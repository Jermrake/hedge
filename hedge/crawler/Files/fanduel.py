
import requests
import json
from datetime import datetime

def pretty(data):
    print(json.dumps(data, indent=3))


def main():
    fd = FanDuel()
   



class FanDuel(object):

    def __init__(self,bet_center):
        self.bet_center = bet_center
        self.site_name = 'FanDuel'
        self.base_url = 'https://sportsbook.fanduel.com/'
        self.current_sport = ''
        self.sport_ids = {
            'College Basketball' : '53474.3',
            'NBA' : '63747.3',
            'NFL' : '62006.3',
            'China BB' : '60284.3',
            'Germany BB' : '60283.3'
        }
        self.bet_type_conversion = {
            'Spread Betting' : 'Spread',
            'Total Points Scored' : 'Total',
            'Moneyline' : 'Moneyline',
            'Alternate Spreads' : 'Spread',
            'Alternate Spread' : 'Spread',
            'Alternate Total Points' : 'Total',
            'Alternate Total' : 'Total'
        }
        self.session = requests.Session()
        self.session.headers = {
            'user-agent' : 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36'
        }

    def get(self,url,data=None,headers=None):
        if headers:
            self.session.headers = headers
        return self.session.get(url,data=data)

    def post(self,url,data=None,headers=None):
        if headers:
            self.session.headers = headers
        return self.session.post(url,data=data)

    def pull_url(self,sport):
        return f'{self.base_url}cache/psmg/UK/{self.sport_ids[sport]}.json'

    def event_url(self,event_id):
        return f'{self.base_url}cache/psevent/UK/1/false/{event_id}.json'
        
    def pull_games(self):
        for sport in self.bet_center.valid_sports:
            self.valid_events = {}
            self.current_sport = sport
            self.sport_pull_games(sport)

    def sport_pull_games(self,sport):
        data = self.get(self.pull_url(sport)).json()
        self.now = datetime.now()
        self.ingest_sport_game_data(data)


    def ingest_sport_game_data(self,data):

        if 'events' not in data:
            print('Fanduel Fail')
            return
        for event in data['events']:
            if not self.valid_event(event):
                continue
            
            self.bet_center.ingest_game(self.current_sport,event["participantname_away"],event["participantname_home"],self.site_name,event['idfoevent'])
    
        
    def valid_event(self,event):
        start_time = datetime.strptime(event['tsstart'].split('.')[0],'%Y-%m-%dT%H:%M:%S')
        return (' At ' in event['eventname'] or ' v ' in event['eventname']) and self.now < datetime.strptime(event['tsstart'],'%Y-%m-%dT%H:%M:%S') \
            and (start_time - datetime.now()).days < 1

    def pull_event_lines(self,event_id,current_game):
        self.current_game = current_game
        data = self.get(self.event_url(event_id)).json()
        self.ingest_event(data)

    def ingest_event(self,data):
        self.current_home = data['participantname_home']
        self.current_away = data['participantname_away']
        for group in data['eventmarketgroups']:
            if group['name'] == 'All':
                self.ingest_market_group(group)

    def ingest_market_group(self,group):
        for market in group['markets']:
            if market['name'] in self.bet_type_conversion:
                self.ingest_market(market,market['name'])

    def ingest_market(self,market,name):
        for selection in market['selections']:
            odds, decimal = self.convert_odds(selection['currentpriceup'],selection['currentpricedown'])
            if name in ['Spread Betting','Spread']:
                if selection['hadvalue'] == 'A':
                    selection['currenthandicap'] = -1 * selection['currenthandicap']
            elif 'Spread' in name:# in ['Alternate Spreads','Alternate Spread']:
                selection['currenthandicap'] = selection['name'].split(' ')[-1].strip('(').strip(')')
                selection['name'] = ' '.join(selection['name'].split(' ')[:-1])
            elif 'Total' in name:# name == ['Alternate Total Points','Alternate Total']:
                if 'currenthandicap' not in selection:
                    selection['currenthandicap'] = selection['name'].split(' ')[-1].strip('(').strip(')')
                    selection['name'] = ' '.join(selection['name'].split(' ')[:-1])

            if selection.get('currenthandicap') and type(selection['currenthandicap']) == str:
                if '+' in selection['currenthandicap']:
                    selection['currenthandicap'] = float(selection['currenthandicap'].replace('+',''))
                else:
                    selection['currenthandicap'] = float(selection['currenthandicap'])

            if selection['name'] == self.current_away:
                selection['name'] = self.current_game.away_team
            elif selection['name'] == self.current_home:
                selection['name'] = self.current_game.home_team

            self.current_game.ingest_bet({
                'Bet Type' : self.convert_bet_type(name),
                'Site' : self.site_name,
                'Label' : selection['name'],
                'Line' : selection.get('currenthandicap'),
                'Odds' : odds,
                'Decimal' : decimal
            })


    def convert_odds(self,price_up,price_down):
        frac = price_up / price_down
        if price_up > price_down:
            odds = f'+{round(frac * 100)}'
        elif price_up < price_down:
            odds = f'-{round((price_down / price_up) * 100)}'
        else:
            odds = '+100'

        decimal = round(frac + 1,4) 

        return odds,decimal


    def ingest_sub_category(self,sub_category):
        for offer in sub_category['offerSubcategory']['offers']:
            for bet in offer:
                event = self.valid_events.get(bet['providerEventId'])
                if not event:
                    continue
                if self.valid_bet(bet) and bet.get('label') in ['Point Spread','Total Points','Moneyline']:
                    self.ingest_bet(bet,event)
            
    def valid_bet(self,bet):
        return bet.get('isOpen') and not bet.get('isSuspended')

    def ingest_bet(self,bet,event):
        for outcome in bet['outcomes']:
            self.bet_center.ingest_bet({
                'Sport' : self.current_sport,
                'Home Team' : event['Home Team'],
                'Away Team' : event['Away Team'],
                'Event Id' : bet['providerEventId'],
                'Bet Type' : self.convert_bet_type(bet['label']),
                'Site' : 'DraftKings',
                'Label' : outcome['label'],
                'Line' : outcome.get('line'),
                'Odds' : outcome['oddsAmerican'],
                'Decimal' : outcome['oddsDecimal']
            })

    def convert_bet_type(self,bet_type):
        return self.bet_type_conversion[bet_type]



if __name__ == '__main__':
    main()