
import requests
import json
from datetime import datetime,timedelta

def pretty(data):
    print(json.dumps(data, indent=3))


def main():
    fb = FoxBet()
   



class FoxBet(object):

    def __init__(self,bet_center):
        self.bet_center = bet_center
        self.site_name = 'FoxBet'
        self.base_url = 'https://sports.nj.foxbet.com/'
        self.current_sport = ''
        
        self.sport_ids = {
            'College Basketball' : '8379618',
            'NBA' : '8370429',
            'NFL' : '8565121',
        }
        self.bet_type_conversion = {
            'Spread' : 'Spread',
            'Alternative Spread (Incl. OT)' : 'Spread',
            'Total Points' : 'Total',
            'Alternative Total Points (Incl. OT)' : 'Total',
            'Money Line (Incl. OT)' : 'Moneyline',
        }
        self.session = requests.Session()
        self.session.headers = {
            'user-agent' : 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36'
        }

    def get(self,url,data=None,headers=None):
        if headers:
            self.session.headers = headers
        return self.session.get(url,data=data)

    def post(self,url,data=None,headers=None):
        if headers:
            self.session.headers = headers
        return self.session.post(url,data=data)

    def pull_url(self,sport):
        return f'{self.base_url}sportsbook/v1/api/getCompetitionEvents?competitionId={self.sport_ids[sport]}&siteId=16384&locale=en-us&channelId=14'

    def event_url(self,event_id):
        return f'{self.base_url}sportsbook/v1/api/getEvent?eventId={event_id}&channelId=14&locale=en-us&siteId=16384'
        
    def pull_games(self):
        for sport in self.bet_center.valid_sports:
            self.valid_events = {}
            self.current_sport = sport
            self.sport_pull_games(sport)

    def sport_pull_games(self,sport):
        if sport not in self.sport_ids:
            return
        data = self.get(self.pull_url(sport)).json()
        self.now = datetime.now()
        self.ingest_sport_game_data(data)


    def ingest_sport_game_data(self,data):
        for event in data['event']:
            
            if not self.valid_event(event):
                continue
            home_team = ''
            away_team = ''
            for team in event['participants']['participant']:
                if team['type'] == 'HOME':
                    home_team = team['name']
                elif team['type'] == 'AWAY':
                    away_team = team['name']
            
            self.bet_center.ingest_game(self.current_sport,away_team,home_team,self.site_name,event['id'])
    
        
    def valid_event(self,event):
        timestamp = int(event['eventTime']) / 1000
        start_time = datetime.fromtimestamp(timestamp)
        return ' vs. ' in event['name'] and (start_time - datetime.now()).days < 1 \
            and self.now < start_time
            

    def pull_event_lines(self,event_id,current_game):
        self.current_game = current_game
        data = self.get(self.event_url(event_id)).json()
        self.ingest_event(data)

    def ingest_event(self,data):
        for team in data['participants']['participant']:
            if team['type'] == 'HOME':
                self.current_home = team['name']
            elif team['type'] == 'AWAY':
                self.current_away = team['name']
        # if 'Nets' in self.current_home:
        #     return
        #     pretty(data)
        #     exit()
        for offer in data['markets']:
            if self.valid_offer(offer):
                self.ingest_offer(offer)

    def valid_offer(self,offer):
        return not offer['suspended'] and offer['displayed']

    def ingest_offer(self,offer):
        for selection in offer['selection']:
            if offer['name'] not in ['Spread','Alternative Spread (Incl. OT)',\
                                                    'Total Points','Alternative Total Points (Incl. OT)',\
                                                    'Money Line (Incl. OT)']:
                continue
            try:
                odds, decimal = self.convert_odds(selection['odds'])
            except:
                continue
            
            if offer['name'] == 'Spread':
                selection['name'] = ' '.join(selection['name'].split(' ')[:-1])
                if selection['name'] == self.current_away:
                    selection['line'] = offer['line'] * -1
                elif selection['name'] == self.current_home:
                    selection['line'] = offer['line']
            elif offer['name'] == 'Total Points':
                selection['line'] = offer['line']
                selection['name'] = selection['type']
            elif offer['name'] in ['Alternative Spread (Incl. OT)','Alternative Total Points (Incl. OT)']:
                selection['line'] = float(selection['name'].split(' ')[-1])
                selection['name'] = ' '.join(selection['name'].split(' ')[:-1])
            


            if selection['name'] == self.current_away:
                selection['name'] = self.current_game.away_team
            elif selection['name'] == self.current_home:
                selection['name'] = self.current_game.home_team

            self.current_game.ingest_bet({
                'Bet Type' : self.convert_bet_type(offer['name']),
                'Site' : self.site_name,
                'Label' : selection['name'],
                'Line' : selection.get('line'),
                'Odds' : odds,
                'Decimal' : decimal
            })


    def convert_odds(self,odds):
        numer = float(odds['frac'].split('/')[0])
        denom = float(odds['frac'].split('/')[1])
        odds = round(numer/denom + 1,4)
        if odds >= 2:
            odds_american = f'+{round((odds - 1) * 100)}'
        else:
            odds_american = f'{round(-100 / (odds - 1))}'
       
        return odds_american,odds

    def convert_bet_type(self,bet_type):
        return self.bet_type_conversion[bet_type]



if __name__ == '__main__':
    main()