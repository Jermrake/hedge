import requests
from groupy.client import Client
import hashlib

def main():
    bot = BetBot()


class BetBot(object):

    def __init__(self,bot_id):
        self.bot_id = bot_id
        # self.group_name = 'BetBot'
        # self.group_id = '65098156'
        # self.access_token = '12Tmtwav0qFw5jAOLhl6iEDZXfj8zfshh8FCrP4q'
        # self.client = Client.from_token(self.access_token)

        self.messages_sent = {}

    def send_message(self,message):
        
        hash_message = hashlib.md5(message.encode()).digest()
        if hash_message in self.messages_sent:
            return
        
        self.messages_sent[hash_message] = ''
        data = {
            'text' : message,
            'bot_id' : self.bot_id,
        }

        requests.post('https://api.groupme.com/v3/bots/post',data=data)
        



if __name__ == '__main__':
    main()