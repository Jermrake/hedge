
import requests
import json
from datetime import datetime,timedelta

def pretty(data):
    print(json.dumps(data, indent=3))


def main():
    s8 = Sports888()
   



class Sports888(object):

    def __init__(self,bet_center):
        self.bet_center = bet_center
        self.site_name = 'Sports888'
        self.base_url = 'https://eu-offering.kambicdn.org/'
        self.current_sport = ''
        
        self.sport_ids = {
            'College Basketball' : 'basketball/ncaab',
            'NBA' : 'basketball/nba',
            'NFL' : 'american_football/nfl',
        }
        self.bet_type_conversion = {
            'Point Spread' : 'Spread',
            'Total Points' : 'Total',
            'Moneyline' : 'Moneyline',
        }
        self.session = requests.Session()
        self.session.headers = {
            'user-agent' : 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36'
        }

    def get(self,url,data=None,headers=None):
        if headers:
            self.session.headers = headers
        return self.session.get(url,data=data)

    def post(self,url,data=None,headers=None):
        if headers:
            self.session.headers = headers
        return self.session.post(url,data=data)

    def pull_url(self,sport):
        return f'{self.base_url}offering/v2018/888usnj/listView/{self.sport_ids[sport]}.json?lang=en_US&market=US-NJ&useCombined=true'

    def event_url(self,event_id):
        return f'{self.base_url}offering/v2018/888usnj/betoffer/event/{event_id}.json?lang=en_US&market=US-NJ&includeParticipants=true'
        
    def pull_games(self):
        for sport in self.bet_center.valid_sports:
            self.valid_events = {}
            self.current_sport = sport
            self.sport_pull_games(sport)

    def sport_pull_games(self,sport):
        if sport not in self.sport_ids:
            return
        data = self.get(self.pull_url(sport)).json()
        self.now = datetime.now()
        self.ingest_sport_game_data(data)


    def ingest_sport_game_data(self,data):
        for event in data['events']:
            event = event['event']
            if not self.valid_event(event):
                continue
            # name = f'{event["shortnameaway"]} @ {event["shortnamehome"]}'
        
            if ' BB' in self.current_sport:
                event['awayName'] = event['awayName'].replace('EWE ','').split(' ')[0].capitalize()
                event['homeName'] = event['homeName'].replace('EWE ','').split(' ')[0].capitalize()
            self.bet_center.ingest_game(self.current_sport,event["awayName"],event["homeName"],self.site_name,event['id'])
    
        
    def valid_event(self,event):
        start_time = datetime.strptime(event['start'],'%Y-%m-%dT%H:%M:%SZ') - timedelta(hours=5)
        return (' @ ' in event['name'] or ' v ' in event['name']) and event['state'] == 'NOT_STARTED' \
            and (datetime.now() - start_time).days < 1

    def pull_event_lines(self,event_id,current_game):
        self.current_game = current_game
        data = self.get(self.event_url(event_id)).json()
        self.ingest_event(data)

    def ingest_event(self,data):
        self.current_home = data['events'][0]['homeName']
        self.current_away = data['events'][0]['awayName']
        for offer in data['betOffers']:
            self.ingest_offer(offer)

    def ingest_offer(self,offer):
        for outcome in offer['outcomes']:
            if offer['criterion']['label'] not in ['Point Spread','Total Points','Moneyline']:
                continue
            odds, decimal = self.convert_odds(outcome['oddsAmerican'],outcome['odds'])

            if offer['criterion']['label'] == 'Moneyline':
                outcome['label'] = outcome['participant']


            if outcome.get('line'):
                outcome['line'] = round(float(outcome['line']) / 1000, 4)

            if outcome['label'] == self.current_away:
                outcome['label'] = self.current_game.away_team
            elif outcome['label'] == self.current_home:
                outcome['label'] = self.current_game.home_team

            self.current_game.ingest_bet({
                'Bet Type' : self.convert_bet_type(offer['criterion']['label']),
                'Site' : self.site_name,
                'Label' : outcome['label'],
                'Line' : outcome.get('line'),
                'Odds' : odds,
                'Decimal' : decimal
            })


    def convert_odds(self,odds_american,odds):

        if '-' not in odds_american:
            odds_american = f'+{odds_american}'
        decimal = round(float(odds)/1000,4)

        return odds_american,decimal

    def convert_bet_type(self,bet_type):
        return self.bet_type_conversion[bet_type]



if __name__ == '__main__':
    main()