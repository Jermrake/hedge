
import json
import time
from datetime import datetime 

from draftkings import DraftKings
from fanduel import FanDuel
from sugarhouse import SugarHouse
from sports888 import Sports888
from foxbet import FoxBet
from betmgm import BetMGM
from game import Game
from betbot import BetBot

def pretty(data):
    print(json.dumps(data, indent=3))

def main():
    while True:
        bet_center = BetCenter()
        bet_center.run()
        print(f'Found {bet_center.total} Games at {datetime.now()}\n')
        time.sleep(120)

class BetCenter(object):

    def __init__(self):
        self.valid_sports = ['NBA']#,'College Basketball','NFL']
        # self.valid_sports = ['China BB',]
        # self.valid_sites = [BetMGM(self),]
        self.valid_sites = [DraftKings(self),FanDuel(self),FoxBet(self)]#,BetMGM(self)]#,SugarHouse(self),Sports888(self)]
        
        self.games = {x:{} for x in self.valid_sports}
        

    def run(self):
        
        for site in self.valid_sites:
            site.pull_games()

        self.total = 0
        for sport,games in self.games.items():
            for game_name,game in games.items():
                if game.valid(): 
                    if 'Northern Illinois' in [game.home_team,game.away_team]:
                        continue
                    try:
                        game.pull_lines(self.valid_sites)
                        self.total += 1
                    except KeyError:
                        pass
                    for difference, data in game.highest_profit.items():
                        bet_bot.send_message(data['Phrase'])
                        # print(f'Guarentee: {data["Guarentee"]}')
                        print(data['Phrase'])
  
        # print(self.games)
        # exit()

    def ingest_game(self,sport,away_team,home_team,site,event_id):
        game = self.get_or_create_game(sport,away_team,home_team)
        game.ingest_site_id(site,event_id)

    def ingest_bet(self,bet):
        game = self.get_or_create_game(bet['Sport'],bet['Game Name'])
        game.ingest_bet(bet)

    def create_game_name(self,away_team,home_team):
        return f'{away_team} @ {home_team}'

    def get_or_create_game(self,sport,away_team,home_team):
        game_name = self.create_game_name(away_team,home_team)
        return self.games[sport].get(game_name) or self.create_game(sport,away_team,home_team)
        
    def create_game(self,sport,away_team,home_team):
        game = Game(sport,away_team,home_team)
        self.games[sport][self.create_game_name(away_team,home_team)] = game
        return game
        



if __name__ == '__main__':
    main()

