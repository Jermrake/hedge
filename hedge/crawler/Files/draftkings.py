
import requests
import json
from datetime import datetime,timedelta


def pretty(data):
    print(json.dumps(data, indent=3))


def main():
    dk = DraftKings(valid_bet_categories,{})

class DraftKings(object):

    def __init__(self,bet_center):
        self.bet_center = bet_center
        self.site_name = 'DraftKings'
        self.base_url = 'https://sportsbook.draftkings.com/'
        self.api_url = 'sites/US-SB/api/v1/'
        self.current_sport = ''
        self.sport_ids = {
            'College Basketball' : '3230960',
            'NBA' : '103',
            'NFL' : '3',
            'China BB' : '78164864',
            'Germany BB' : '74984075',
        }
        self.bet_type_conversion = {
            'Point Spread' : 'Spread',
            'Total Points' : 'Total',
            'Moneyline' : 'Moneyline'
        }
        self.session = requests.Session()
        self.session.headers = {
            'user-agent' : 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36'
        }

    def get(self,url,data=None,headers=None):
        if headers:
            self.session.headers = headers
        return self.session.get(url,data=data)

    def post(self,url,data=None,headers=None):
        if headers:
            self.session.headers = headers
        return self.session.post(url,data=data)

    def eventgroup_url(self,sport):
        return f'{self.base_url}{self.api_url}/eventgroup/{self.sport_ids[sport]}/full?format=json'

    def event_url(self,event_id):
        return f'{self.base_url}{self.api_url}/event/{event_id}?format=json'

    def pull_games(self):
        for sport in self.bet_center.valid_sports:
            self.valid_events = {}
            self.current_sport = sport
            self.sport_pull_game_lines(sport)

    def sport_pull_game_lines(self,sport):
        data = self.get(self.eventgroup_url(sport)).json()
        self.ingest_sport_game_data(data)

    def ingest_sport_game_data(self,data):
        for event in data['eventGroup']['events']:
            if self.valid_event(event):
                
                if self.current_sport == 'College Basketball':
                    away_team_name = event['teamName1']
                    home_team_name = event['teamName2']
                else:
                    away_team_name = event['name'].split(' @ ')[0]
                    home_team_name = event['name'].split(' @ ')[1]
                

                
                self.bet_center.ingest_game(self.current_sport,away_team_name, home_team_name,self.site_name,event['eventId'])
        # for category in data['eventGroup']['offerCategories']:
        #     if category['name'] in ['Game Lines']:
        #         self.ingest_category(category)

    def valid_event(self,event):
        start_time = datetime.strptime(event['startDate'].split('.')[0],'%Y-%m-%dT%H:%M:%S') - timedelta(hours=5)
        
        return event['eventStatus']['state'] == 'NOT_STARTED' and '@' in event['name'] \
                and (start_time - datetime.now()).days < 1


    def pull_event_lines(self,event_id,current_game):
        self.current_game = current_game
        data = self.get(self.event_url(event_id)).json()
        self.ingest_event(data)

    def ingest_event(self,data):
        # if 'eventCategories' not in data:
        #     pretty(data)
        #     exit()
        self.current_home = data['event']['teamName2']
        self.current_away = data['event']['teamName1']
        # if self.current_away == 'Zhejiang Golden Bulls':
        #     pretty(data)
        #     exit()
        for category in data['eventCategories']:
            if category['name'] in ['Game Lines']:
                self.ingest_category(category['componentizedOffers'])




    def ingest_category(self,category):
        for sub_category in category:
            if sub_category['subcategoryName'] in ['Game','Alternate Point Spread','Alternate Total Points']:
                self.ingest_sub_category(sub_category)

    def ingest_sub_category(self,sub_category):
        for offer in sub_category['offers']:
            for bet in offer:
                if self.valid_bet(bet) and bet.get('label') in ['Point Spread','Total Points','Moneyline']:
                    self.ingest_bet(bet)
            
    def valid_bet(self,bet):
        return bet.get('isOpen') and not bet.get('isSuspended')

    def ingest_bet(self,bet):
        bet_type = self.convert_bet_type(bet['label'])
        for outcome in bet['outcomes']:
            
            # line = outcome.get('line')
            # if line is not None and bet_type != 'Total':
            #     line = str(line)
            #     if '-' not in line:
            #         line = f'+{line}'
            if outcome['label'] == self.current_home:
                outcome['label'] = self.current_game.home_team
            elif outcome['label'] == self.current_away:
                outcome['label'] = self.current_game.away_team


            self.current_game.ingest_bet({
                'Bet Type' : bet_type,
                'Site' : self.site_name,
                'Label' : outcome['label'],
                'Line' : outcome.get('line'),
                'Odds' : outcome['oddsAmerican'],
                'Decimal' : outcome['oddsDecimal']
            })

    def convert_bet_type(self,bet_type):
        return self.bet_type_conversion[bet_type]



if __name__ == '__main__':
    main()