import json

def pretty(data):
    print(json.dumps(data, indent=3))

# site_amount_available = {
#     'DraftKings' : 64,
#     'FanDuel' : 960,
#     'FoxBet' : 500,
#     'BetMGM' : 500
# }
site_amount_available = {
    'DraftKings' : 500,
    'FanDuel' : 500,
    'FoxBet' : 500,
    'BetMGM' : 500
}

class Game(object):

    sport = None
    home_team = None
    away_team = None
    bets = None
    site_event_ids = None

    def __init__(self,sport,away_team,home_team):
        self.sport = sport
        self.away_team = away_team
        self.home_team = home_team
        self.game_name = f'{away_team} @ {home_team}'
        self.bets = {}
        self.site_event_ids = {}
        self.profit_worth = {
            0.0 : 5,
            0.5 : 20,
            1.0 : 20
        }
        self.highest_profit = {}

    def ingest_site_id(self,site,event_id):
        self.site_event_ids[site] = event_id

    def pull_lines(self,valid_sites):
        
        for site, event_id in self.site_event_ids.items():
            for valid_site in valid_sites:
                if valid_site.site_name == site:
                    valid_site.pull_event_lines(event_id,self)
        for bet_type, bets in self.bets.items():
            self.compare_bet_type(bet_type,bets)

    def compare_bet_type(self,bet_type,bets):
        if bet_type == 'Moneyline':
            for label, bet in bets.items():
                # if 'Zhejiang' in label:
                #     bet.site_odds['DraftKings'] = '+230 (3.3)'

                for label2, bet2 in bets.items():
                    if bet == bet2:
                        continue
                    self.find_hedge(bet,bet2)
            return
        difference = 1
        for label, bet in bets.items():
            if bet.line < 0 or 'Under' in label:
                continue
            # if bet.line != 4.5:
            #     continue 
            for label2, bet2 in bets.items():
                if bet == bet2 or bet.label == bet2.label:
                    continue
                if abs(bet.line + bet2.line) <= difference and abs(bet2.line) >= abs(bet.line):
                    
                    if self.sport == 'NFL' and abs(bet.line + bet2.line) > 0:
                        if (bet.line > 2 and bet2.line < 4) or (bet.line > 6 and bet2.line < 8):
                            continue
                    self.find_hedge(bet,bet2)

    def find_hedge(self,bet,bet2):

        for site,odds in bet.site_odds.items():
            # if site == 'DraftKings':
            #     odds = '+230 (3.3)'
            for site2,odds2 in bet2.site_odds.items():
                if site == site2:
                    continue
                if self.has_profit(odds,odds2):
                    self.calculate_profit(odds,odds2,site,site2,bet,bet2)
                elif self.has_profit(odds2,odds):
                    self.calculate_profit(odds2,odds,site2,site,bet2,bet)
            
    def has_profit(self,odds,odds2):
        if '+' in odds:
            if '+' in odds2:
                return True
            else:
                odds_num = int(odds.strip('+').split(' ')[0])
                odds2_num = int(odds2.strip('-').split(' ')[0])
                if odds_num > odds2_num:
                    return True
                    
        return False

    def calculate_profit(self,odds,odds2,site,site2,bet,bet2):
        odds_num = int(odds.strip('+').split(' ')[0])
        odds2_num = int(odds2.strip('-').split(' ')[0])
        decimal = float(odds.split('(')[1].strip(')')) - 1
        decimal2 = float(odds2.split('(')[1].strip(')')) - 1

        
        base = 200 if site_amount_available[site2] > 200 else site_amount_available[site2]
        odds2_risk = base
        odds2_profit = round(odds2_risk * decimal2,2)


        # odds2_profit - odds_risk = odds_proit - odds2_risk
        # odds_profit = odds_risk * decimal

        # odds2_profit - odds_risk = (odds_risk * decimal) - odds2_risk
        # (odds_risk * decimal) + odds_risk = (odds2_profit + odd2_risk)
        # odds_risk = (odds2_profit + odd2_risk) / (1 + decimal)

        odds_risk = round((odds2_profit + odds2_risk) / (1 + decimal),2)
        if odds_risk > site_amount_available[site]:
            return
        odds_profit = round(odds_risk * decimal,2)
        difference = 0 if bet.line is None else abs(bet.line + bet2.line)
        risk_free_risk = round(odds2_risk / decimal,2)
        guarentee = round(odds_profit - odds2_risk,2)

        if guarentee < self.profit_worth[difference]:
            return

        s = f'Guarentee: ${guarentee}'
        s += f'\n{self.sport}: {self.game_name}'
        s += f'\n{site} - {bet.label} {bet.line} {odds.split(" ")[0]} - ${odds_risk} to win ${odds_profit}'
        s += f'\n{site2} - {bet2.label} {bet2.line} {odds2.split(" ")[0]} - ${odds2_risk} to win ${odds2_profit}'
        # s += '\n'
        # s += f'\nGuarentee ${guarentee}:'
        # s += f'\n    - {site}: ${odds_risk} to win ${odds_profit}'
        # s += f'\n    - {site2}: ${odds2_risk} to win ${odds2_profit}'
        # s += f'\nRisk Free ${round(odds2_profit - risk_free_risk,2)}:'
        # s += f'\n    - {site2}: ${odds2_risk} to win ${odds2_profit}'
        # s += f'\n    - {site}: ${risk_free_risk} to win ${odds2_risk}'
        
        if difference not in self.highest_profit:
            self.highest_profit[difference] = {}

        if not self.highest_profit[difference]:
            self.highest_profit[difference] = {
                'Guarentee' : guarentee,
                'Phrase' : s
            }
        else:
            if guarentee > self.highest_profit[difference]['Guarentee']:
                self.highest_profit[difference] = {
                'Guarentee' : guarentee,
                'Phrase' : s
            }
        
       
    def ingest_bet(self,bet_data):
        bet = self.get_or_create_bet(bet_data['Bet Type'],bet_data['Label'],bet_data['Line'])
        bet.ingest_site(bet_data['Site'],bet_data['Odds'],bet_data['Decimal'])
        # self.bets.append(Bet(bet['Site'],bet['Odds'],bet['Decimal']))
  
    def get_or_create_bet(self,bet_type,label,line):
        if bet_type not in self.bets:
            self.bets[bet_type] = {}
        # if abs_val not in self.bets[bet_type]:

        return self.bets[bet_type].get(self.bet_key(label,line)) or self.create_bet(bet_type,label,line)

    def create_bet(self,bet_type,label,line):
        bet = Bet(bet_type,label,line)
        self.bets[bet_type][self.bet_key(label,line)] = bet
        return bet

    def bet_key(self,label,line):
        return f'{label}|{line}'
        
    def valid(self):
        return len(self.site_event_ids) > 1

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        s = f'{self.sport}: {self.game_name}\n'
        s += '\n'.join([f'{x} - {y}'for x,y in self.site_event_ids.items()])
        s += '\n'
        for bet_type, bets in self.bets.items():
            s += f'{bet_type}\n'
            s += '\n'.join([f'{x.__repr__()}\n' for x in bets.values()])
        s += '\n\n'
        return s 

class Bet(object):

    bet_type = None
    label = None
    line = None

    def __init__(self,bet_type,label,line):
        # self.site = site
        self.bet_type = bet_type
        self.label = label
        self.line = line
        self.site_odds = {}
        # self.odds = odds
        # self.decimal = decimal

    def ingest_site(self,site,odds,decimal):
        self.site_odds[site] = f'{odds} ({decimal})'

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        s = f'{self.bet_type} {self.label} {self.line}'
        for site,odds in self.site_odds.items():
            s += f'\n{site}: {odds}'
        return s

