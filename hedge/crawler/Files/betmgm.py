
import requests
import json
from datetime import datetime,timedelta

def pretty(data):
    print(json.dumps(data, indent=3))


def main():
    mgm = BetMGM(None)



class BetMGM(object):

    def __init__(self,bet_center):
        self.bet_center = bet_center
        self.site_name = 'BetMGM'
        self.base_url = 'https://cds-api.nj.betmgm.com/'
        self.current_sport = ''
        
        self.sport_ids = {
            'College Basketball' : {
                'comp_id' : '264',
                'region' : '9',
                'id' : '7',
            },
            'NBA' : {
                'comp_id' : '6004',
                'region' : '9',
                'id' : '7',
            },
            'NFL' :  {
                'comp_id' : '35',
                'region' : '9',
                'id' : '11',
            },
        }
        self.bet_type_conversion = {
            'Spread' : 'Spread',
            'Totals' : 'Total',
            'Money Line' : 'Moneyline',
        }
        self.session = requests.Session()
        self.session.headers = {
            'user-agent' : 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36',
            'content-type' : 'application/json'
        }
        self.set_config()

    def set_config(self):
        url = 'https://sports.nj.betmgm.com/en/api/clientconfig'
        data = self.get(url).json()
        self.access_token = data['msConnection']['pushAccessId']

    def get(self,url,data=None,headers=None):
        if headers:
            self.session.headers = headers
        return self.session.get(url,data=data)

    def post(self,url,data=None,headers=None):
        if headers:
            self.session.headers = headers
        return self.session.post(url,data=data)

    def pull_url(self,sport):
        return f'{self.base_url}bettingoffer/fixtures?x-bwin-accessid={self.access_token}&'\
                f'lang=en-us&country=US&userCountry=US&fixtureTypes=Standard&state=Latest&'\
                f'offerMapping=Filtered&offerCategories=Gridable&fixtureCategories=Gridable,NonGridable,Other&'\
                f'sportIds={sport["id"]}&regionIds={sport["region"]}&competitionIds={sport["comp_id"]}&'\
                f'skip=0&take=50&sortBy=Tags'

    def event_url(self,event_id):
        return f'{self.base_url}bettingoffer/fixture-view?x-bwin-accessid={self.access_token}&'\
                f'lang=en-us&country=US&userCountry=US&offerMapping=All&scoreboardMode=Full&'\
                f'fixtureIds={event_id}&state=Latest&includePrecreatedBetBuilder=true&supportVirtual=false'
  
    def pull_games(self):
        for sport in self.bet_center.valid_sports:
            self.valid_events = {}
            self.current_sport = sport
            self.sport_pull_games(sport)

    def sport_pull_games(self,sport):
        if sport not in self.sport_ids:
            return
        r = self.get(self.pull_url(self.sport_ids[sport]))

        data = self.get(self.pull_url(self.sport_ids[sport])).json()
        self.now = datetime.now()
        self.ingest_sport_game_data(data)


    def ingest_sport_game_data(self,data):
        for event in data['fixtures']:
            
            if not self.valid_event(event):
                continue
            away_team = event['name']['value'].split(' at ')[0]
            home_team = event['name']['value'].split(' at ')[1]
           
            self.bet_center.ingest_game(self.current_sport,away_team,home_team,self.site_name,event['id'])
    
        
    def valid_event(self,event):
        start_time = datetime.strptime(event['startDate'],'%Y-%m-%dT%H:%M:%SZ') -timedelta(hours=5)
        return ' at ' in event['name']['value'] and event['stage'] == 'PreMatch'# \
            # and (start_time - datetime.now()).days < 1
            

    def pull_event_lines(self,event_id,current_game):
        self.current_game = current_game
        data = self.get(self.event_url(event_id)).json()
        self.ingest_event(data)

    def ingest_event(self,data):
        data = data['fixture']
        self.current_away = data['name']['value'].split(' at ')[0]
        self.current_home = data['name']['value'].split(' at ')[1]
        for team in data['participants']:
            if team['name']['value'] == self.current_away:
                self.current_away_short = team['name']['short']
            elif team['name']['value'] == self.current_home:
                self.current_home_short = team['name']['short']
                
        for offer in data['games']:
            self.ingest_offer(offer)

    def ingest_offer(self,offer):
        if offer['name']['value'] not in ['Spread','Totals','Money Line']:
            return
        for result in offer['results']:
            odds = result['americanOdds']
            decimal = result['odds']
            if odds > 0:
                odds = f'+{odds}'
            else:
                odds = f'{odds}'
            
            if offer['name']['value'] == 'Money Line':
                if result['name']['value'] == self.current_away_short:
                    result['name'] = self.current_away
                elif result['name']['value'] == self.current_home_short:
                    result['name'] = self.current_home 

            else:
                result['name']['value'] = result['name']['value'].strip()
                result['line'] = float(result['name']['value'].split(' ')[-1].replace(',','.'))
                result['name'] = ' '.join(result['name']['value'].split(' ')[:-1])
            


            if result['name'] == self.current_away:
                result['name'] = self.current_game.away_team
            elif result['name'] == self.current_home:
                result['name'] = self.current_game.home_team

            self.current_game.ingest_bet({
                'Bet Type' : self.convert_bet_type(offer['name']['value']),
                'Site' : self.site_name,
                'Label' : result['name'],
                'Line' : result.get('line'),
                'Odds' : odds,
                'Decimal' : decimal
            })


    def convert_odds(self,odds):
        numer = float(odds['frac'].split('/')[0])
        denom = float(odds['frac'].split('/')[1])
        odds = round(numer/denom + 1,4)
        if odds >= 2:
            odds_american = f'+{round((odds - 1) * 100)}'
        else:
            odds_american = f'{round(-100 / (odds - 1))}'
       
        return odds_american,odds

    def convert_bet_type(self,bet_type):
        return self.bet_type_conversion[bet_type]



if __name__ == '__main__':
    main()