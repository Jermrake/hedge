{'NBA': {'Philadelphia 76ers @ Brooklyn Nets': NBA: Philadelphia 76ers @ Brooklyn Nets
DraftKings - 179653580
FanDuel - 949395.3
FoxBet - 8715840
BetMGM - 10987582
Spread
Spread Philadelphia 76ers -1.5
DraftKings: -112 (1.9)
FanDuel: -110 (1.9091)
FoxBet: -105 (1.95)
BetMGM: -105 (1.95)

Spread Brooklyn Nets 1.5
DraftKings: -110 (1.91)
FanDuel: -110 (1.9091)
FoxBet: -125 (1.8)
BetMGM: -115 (1.87)

Spread Philadelphia 76ers -14.5
DraftKings: +460 (5.6)
FanDuel: +580 (6.8)
FoxBet: +475 (5.75)

Spread Brooklyn Nets 14.5
DraftKings: -670 (1.15)
FanDuel: -1050 (1.0952)
FoxBet: -900 (1.1111)

Spread Philadelphia 76ers -14.0
DraftKings: +460 (5.6)
FoxBet: +475 (5.75)

Spread Brooklyn Nets 14.0
DraftKings: -670 (1.15)
FoxBet: -900 (1.1111)

Spread Philadelphia 76ers -13.5
DraftKings: +400 (5.0)
FanDuel: +520 (6.2)
FoxBet: +425 (5.25)

Spread Brooklyn Nets 13.5
DraftKings: -560 (1.18)
FanDuel: -900 (1.1111)
FoxBet: -700 (1.1429)

Spread Philadelphia 76ers -13.0
DraftKings: +400 (5.0)
FoxBet: +425 (5.25)

Spread Brooklyn Nets 13.0
DraftKings: -560 (1.18)
FoxBet: -700 (1.1429)

Spread Philadelphia 76ers -12.5
DraftKings: +360 (4.6)
FanDuel: +440 (5.4)
FoxBet: +380 (4.8)

Spread Brooklyn Nets 12.5
DraftKings: -500 (1.2)
FanDuel: -700 (1.1429)
FoxBet: -650 (1.1538)

Spread Philadelphia 76ers -12.0
DraftKings: +350 (4.5)
FoxBet: +380 (4.8)

Spread Brooklyn Nets 12.0
DraftKings: -480 (1.21)
FoxBet: -650 (1.1538)

Spread Philadelphia 76ers -11.5
DraftKings: +320 (4.2)
FanDuel: +385 (4.85)
FoxBet: +333 (4.3333)
BetMGM: +450 (5.5)

Spread Brooklyn Nets 11.5
DraftKings: -420 (1.24)
FanDuel: -590 (1.1695)
FoxBet: -550 (1.1818)
BetMGM: -650 (1.16)

Spread Philadelphia 76ers -11.0
DraftKings: +310 (4.1)
FoxBet: +333 (4.3333)

Spread Brooklyn Nets 11.0
DraftKings: -420 (1.24)
FoxBet: -550 (1.1818)

Spread Philadelphia 76ers -10.5
DraftKings: +285 (3.85)
FanDuel: +320 (4.2)
FoxBet: +290 (3.9)
BetMGM: +375 (4.75)

Spread Brooklyn Nets 10.5
DraftKings: -375 (1.27)
FanDuel: -460 (1.2174)
FoxBet: -450 (1.2222)
BetMGM: -500 (1.2)

Spread Philadelphia 76ers -10.0
DraftKings: +275 (3.75)
FoxBet: +290 (3.9)

Spread Brooklyn Nets 10.0
DraftKings: -360 (1.28)
FoxBet: -450 (1.2222)

Spread Philadelphia 76ers -9.5
DraftKings: +245 (3.45)
FanDuel: +270 (3.7)
FoxBet: +275 (3.75)
BetMGM: +310 (4.1)

Spread Brooklyn Nets 9.5
DraftKings: -315 (1.32)
FanDuel: -375 (1.2667)
FoxBet: -400 (1.25)
BetMGM: -400 (1.25)

Spread Philadelphia 76ers -9.0
DraftKings: +245 (3.45)
FoxBet: +275 (3.75)

Spread Brooklyn Nets 9.0
DraftKings: -315 (1.32)
FoxBet: -400 (1.25)

Spread Philadelphia 76ers -8.5
DraftKings: +220 (3.2)
FanDuel: +235 (3.35)
FoxBet: +230 (3.3)
BetMGM: +275 (3.75)

Spread Brooklyn Nets 8.5
DraftKings: -278 (1.36)
FanDuel: -320 (1.3125)
FoxBet: -333 (1.3)
BetMGM: -350 (1.28)

Spread Philadelphia 76ers -8.0
DraftKings: +215 (3.15)
FoxBet: +230 (3.3)

Spread Brooklyn Nets 8.0
DraftKings: -275 (1.37)
FoxBet: -300 (1.3333)

Spread Philadelphia 76ers -7.5
DraftKings: +195 (2.95)
FanDuel: +210 (3.1)
FoxBet: +205 (3.05)
BetMGM: +230 (3.3)

Spread Brooklyn Nets 7.5
DraftKings: -245 (1.41)
FanDuel: -280 (1.3571)
FoxBet: -286 (1.35)
BetMGM: -300 (1.34)

Spread Philadelphia 76ers -7.0
DraftKings: +188 (2.88)
FoxBet: +200 (3.0)

Spread Brooklyn Nets 7.0
DraftKings: -235 (1.43)
FoxBet: -275 (1.3636)

Spread Philadelphia 76ers -6.5
DraftKings: +170 (2.7)
FanDuel: +182 (2.82)
FoxBet: +170 (2.7)
BetMGM: +195 (2.95)

Spread Brooklyn Nets 6.5
DraftKings: -215 (1.47)
FanDuel: -240 (1.4167)
FoxBet: -225 (1.4444)
BetMGM: -250 (1.42)

Spread Philadelphia 76ers -6.0
DraftKings: +163 (2.63)
FoxBet: +165 (2.65)

Spread Brooklyn Nets 6.0
DraftKings: -200 (1.5)
FoxBet: -225 (1.4444)

Spread Philadelphia 76ers -5.5
DraftKings: +150 (2.5)
FanDuel: +162 (2.62)
FoxBet: +155 (2.55)
BetMGM: +165 (2.65)

Spread Brooklyn Nets 5.5
DraftKings: -186 (1.54)
FanDuel: -210 (1.4762)
FoxBet: -212 (1.4706)
BetMGM: -200 (1.5)

Spread Philadelphia 76ers -5.0
DraftKings: +143 (2.43)
FoxBet: +145 (2.45)

Spread Brooklyn Nets 5.0
DraftKings: -177 (1.57)
FoxBet: -200 (1.5)

Spread Philadelphia 76ers -4.5
DraftKings: +130 (2.3)
FanDuel: +136 (2.36)
FoxBet: +130 (2.3)
BetMGM: +145 (2.45)

Spread Brooklyn Nets 4.5
DraftKings: -159 (1.63)
FanDuel: -174 (1.5747)
FoxBet: -175 (1.5714)
BetMGM: -175 (1.57)

Spread Philadelphia 76ers -4.0
DraftKings: +123 (2.23)
FoxBet: +125 (2.25)

Spread Brooklyn Nets 4.0
DraftKings: -152 (1.66)
FoxBet: -162 (1.6154)

Spread Philadelphia 76ers -3.5
DraftKings: +115 (2.15)
FanDuel: +116 (2.16)
FoxBet: +115 (2.15)
BetMGM: +125 (2.25)

Spread Brooklyn Nets 3.5
DraftKings: -141 (1.71)
FanDuel: -142 (1.7042)
FoxBet: -150 (1.6667)
BetMGM: -150 (1.67)

Spread Philadelphia 76ers -3.0
DraftKings: +108 (2.08)
FoxBet: +110 (2.1)

Spread Brooklyn Nets 3.0
DraftKings: -132 (1.76)
FoxBet: -143 (1.7)

Spread Philadelphia 76ers -2.5
DraftKings: +102 (2.02)
FanDuel: +102 (2.02)
FoxBet: +105 (2.05)
BetMGM: +105 (2.05)

Spread Brooklyn Nets 2.5
DraftKings: -124 (1.81)
FanDuel: -124 (1.8065)
FoxBet: -137 (1.7273)
BetMGM: -130 (1.78)

Spread Philadelphia 76ers -2.0
DraftKings: -105 (1.96)
FoxBet: -105 (1.95)

Spread Brooklyn Nets 2.0
DraftKings: -117 (1.86)
FoxBet: -125 (1.8)

Spread Philadelphia 76ers -1.0
DraftKings: -117 (1.86)
FoxBet: -110 (1.9091)

Spread Brooklyn Nets 1.0
DraftKings: -105 (1.96)
FoxBet: -110 (1.9091)

Spread Philadelphia 76ers -0.5
DraftKings: -122 (1.82)
FanDuel: -122 (1.8197)

Spread Brooklyn Nets 0.5
DraftKings: +100 (2.0)
FanDuel: +100 (2.0)

Spread Philadelphia 76ers 0.0
DraftKings: -122 (1.82)
FoxBet: -125 (1.8)

Spread Brooklyn Nets 0.0
DraftKings: +100 (2.0)
FoxBet: -105 (1.95)

Spread Philadelphia 76ers 0.5
DraftKings: -122 (1.82)
FanDuel: -122 (1.8197)

Spread Brooklyn Nets -0.5
DraftKings: +100 (2.0)
FanDuel: +100 (2.0)

Spread Philadelphia 76ers 1.0
DraftKings: -129 (1.78)
FoxBet: -133 (1.75)

Spread Brooklyn Nets -1.0
DraftKings: +105 (2.05)
FoxBet: +100 (2.0)

Spread Philadelphia 76ers 1.5
DraftKings: -134 (1.75)
FanDuel: -134 (1.7463)
FoxBet: -137 (1.7273)
BetMGM: -135 (1.75)

Spread Brooklyn Nets -1.5
DraftKings: +110 (2.1)
FanDuel: +110 (2.1)
FoxBet: +105 (2.05)
BetMGM: +110 (2.1)

Spread Philadelphia 76ers 2.0
DraftKings: -143 (1.7)
FoxBet: -143 (1.7)

Spread Brooklyn Nets -2.0
DraftKings: +117 (2.17)
FoxBet: +110 (2.1)

Spread Philadelphia 76ers 2.5
DraftKings: -150 (1.67)
FanDuel: -154 (1.6494)
FoxBet: -150 (1.6667)
BetMGM: -150 (1.67)

Spread Brooklyn Nets -2.5
DraftKings: +123 (2.23)
FanDuel: +124 (2.24)
FoxBet: +115 (2.15)
BetMGM: +125 (2.25)

Spread Philadelphia 76ers 3.0
DraftKings: -162 (1.62)
FoxBet: -162 (1.6154)

Spread Brooklyn Nets -3.0
DraftKings: +133 (2.33)
FoxBet: +125 (2.25)

Spread Philadelphia 76ers 3.5
DraftKings: -175 (1.58)
FanDuel: -174 (1.5747)
FoxBet: -175 (1.5714)
BetMGM: -175 (1.57)

Spread Brooklyn Nets -3.5
DraftKings: +140 (2.4)
FanDuel: +136 (2.36)
FoxBet: +130 (2.3)
BetMGM: +145 (2.45)

Spread Philadelphia 76ers 4.0
DraftKings: -186 (1.54)
FoxBet: -188 (1.5333)

Spread Brooklyn Nets -4.0
DraftKings: +150 (2.5)
FoxBet: +140 (2.4)

Spread Philadelphia 76ers 4.5
DraftKings: -195 (1.52)
FanDuel: -215 (1.4651)
FoxBet: -200 (1.5)
BetMGM: -200 (1.5)

Spread Brooklyn Nets -4.5
DraftKings: +155 (2.55)
FanDuel: +164 (2.64)
FoxBet: +145 (2.45)
BetMGM: +165 (2.65)

Spread Philadelphia 76ers 5.0
DraftKings: -215 (1.47)
FoxBet: -212 (1.4706)

Spread Brooklyn Nets -5.0
DraftKings: +175 (2.75)
FoxBet: +165 (2.65)

Spread Philadelphia 76ers 5.5
DraftKings: -230 (1.44)
FanDuel: -250 (1.4)
FoxBet: -225 (1.4444)
BetMGM: -250 (1.42)

Spread Brooklyn Nets -5.5
DraftKings: +180 (2.8)
FanDuel: +188 (2.88)
FoxBet: +170 (2.7)
BetMGM: +195 (2.95)

Spread Philadelphia 76ers 6.0
DraftKings: -250 (1.4)
FoxBet: -275 (1.3636)

Spread Brooklyn Nets -6.0
DraftKings: +200 (3.0)
FoxBet: +190 (2.9)

Spread Philadelphia 76ers 6.5
DraftKings: -265 (1.38)
FanDuel: -290 (1.3448)
FoxBet: -275 (1.3636)
BetMGM: -275 (1.35)

Spread Brooklyn Nets -6.5
DraftKings: +210 (3.1)
FanDuel: +215 (3.15)
FoxBet: +200 (3.0)
BetMGM: +225 (3.25)

Spread Philadelphia 76ers 7.0
DraftKings: -286 (1.35)
FoxBet: -300 (1.3333)

Spread Brooklyn Nets -7.0
DraftKings: +225 (3.25)
FoxBet: +230 (3.3)

Spread Philadelphia 76ers 7.5
DraftKings: -305 (1.33)
FanDuel: -330 (1.303)
FoxBet: -300 (1.3333)
BetMGM: -350 (1.28)

Spread Brooklyn Nets -7.5
DraftKings: +235 (3.35)
FanDuel: +240 (3.4)
FoxBet: +230 (3.3)
BetMGM: +275 (3.75)

Spread Philadelphia 76ers 8.0
DraftKings: -335 (1.3)
FoxBet: -400 (1.25)

Spread Brooklyn Nets -8.0
DraftKings: +260 (3.6)
FoxBet: +275 (3.75)

Spread Philadelphia 76ers 8.5
DraftKings: -345 (1.29)
FanDuel: -375 (1.2667)
FoxBet: -400 (1.25)
BetMGM: -400 (1.25)

Spread Brooklyn Nets -8.5
DraftKings: +270 (3.7)
FanDuel: +270 (3.7)
FoxBet: +275 (3.75)
BetMGM: +310 (4.1)

Spread Philadelphia 76ers 9.0
DraftKings: -385 (1.26)
FoxBet: -450 (1.2222)

Spread Brooklyn Nets -9.0
DraftKings: +295 (3.95)
FoxBet: +290 (3.9)

Spread Philadelphia 76ers 9.5
DraftKings: -400 (1.25)
FanDuel: -430 (1.2326)
FoxBet: -450 (1.2222)
BetMGM: -500 (1.2)

Spread Brooklyn Nets -9.5
DraftKings: +310 (4.1)
FanDuel: +300 (4.0)
FoxBet: +290 (3.9)
BetMGM: +375 (4.75)

Spread Philadelphia 76ers 10.0
DraftKings: -455 (1.22)
FoxBet: -550 (1.1818)

Spread Brooklyn Nets -10.0
DraftKings: +335 (4.35)
FoxBet: +333 (4.3333)

Spread Philadelphia 76ers 10.5
DraftKings: -455 (1.22)
FanDuel: -580 (1.1724)
FoxBet: -550 (1.1818)

Spread Brooklyn Nets -10.5
DraftKings: +340 (4.4)
FanDuel: +380 (4.8)
FoxBet: +333 (4.3333)

Spread Philadelphia 76ers 11.0
DraftKings: -530 (1.19)
FoxBet: -650 (1.1538)

Spread Brooklyn Nets -11.0
DraftKings: +380 (4.8)
FoxBet: +380 (4.8)

Spread Philadelphia 76ers 11.5
DraftKings: -530 (1.19)
FanDuel: -700 (1.1429)
FoxBet: -650 (1.1538)

Spread Brooklyn Nets -11.5
DraftKings: +390 (4.9)
FanDuel: +440 (5.4)
FoxBet: +380 (4.8)

Spread Philadelphia 76ers 21.5
FanDuel: -5000 (1.02)

Spread Philadelphia 76ers 20.5
FanDuel: -4500 (1.0222)

Spread Philadelphia 76ers 19.5
FanDuel: -3500 (1.0286)

Spread Philadelphia 76ers 18.5
FanDuel: -3000 (1.0333)

Spread Philadelphia 76ers 17.5
FanDuel: -3000 (1.0333)

Spread Philadelphia 76ers 16.5
FanDuel: -2400 (1.0417)

Spread Philadelphia 76ers 15.5
FanDuel: -1800 (1.0556)

Spread Philadelphia 76ers 14.5
FanDuel: -1400 (1.0714)

Spread Philadelphia 76ers 13.5
FanDuel: -1050 (1.0952)

Spread Philadelphia 76ers 12.5
FanDuel: -900 (1.1111)
FoxBet: -700 (1.1429)

Spread Philadelphia 76ers -15.5
FanDuel: +670 (7.7)

Spread Philadelphia 76ers -16.5
FanDuel: +780 (8.8)

Spread Philadelphia 76ers -17.5
FanDuel: +870 (9.7)

Spread Philadelphia 76ers -18.5
FanDuel: +920 (10.2)

Spread Philadelphia 76ers -19.5
FanDuel: +1000 (11.0)

Spread Philadelphia 76ers -20.5
FanDuel: +1100 (12.0)

Spread Philadelphia 76ers -21.5
FanDuel: +1300 (14.0)

Spread Philadelphia 76ers -22.5
FanDuel: +1300 (14.0)

Spread Philadelphia 76ers -23.5
FanDuel: +1400 (15.0)

Spread Philadelphia 76ers -24.5
FanDuel: +1800 (19.0)

Spread Philadelphia 76ers -25.5
FanDuel: +2200 (23.0)

Spread Philadelphia 76ers -26.5
FanDuel: +2200 (23.0)

Spread Brooklyn Nets -22.5
FanDuel: +1800 (19.0)

Spread Brooklyn Nets -21.5
FanDuel: +1400 (15.0)

Spread Brooklyn Nets -20.5
FanDuel: +1300 (14.0)

Spread Brooklyn Nets -19.5
FanDuel: +1100 (12.0)

Spread Brooklyn Nets -18.5
FanDuel: +1000 (11.0)

Spread Brooklyn Nets -17.5
FanDuel: +980 (10.8)

Spread Brooklyn Nets -16.5
FanDuel: +900 (10.0)

Spread Brooklyn Nets -15.5
FanDuel: +790 (8.9)

Spread Brooklyn Nets -14.5
FanDuel: +690 (7.9)

Spread Brooklyn Nets -13.5
FanDuel: +580 (6.8)

Spread Brooklyn Nets -12.5
FanDuel: +520 (6.2)
FoxBet: +425 (5.25)

Spread Brooklyn Nets 15.5
FanDuel: -1350 (1.0741)

Spread Brooklyn Nets 16.5
FanDuel: -1800 (1.0556)

Spread Brooklyn Nets 17.5
FanDuel: -2200 (1.0455)

Spread Brooklyn Nets 18.5
FanDuel: -2500 (1.04)

Spread Brooklyn Nets 19.5
FanDuel: -3000 (1.0333)

Spread Brooklyn Nets 20.5
FanDuel: -3500 (1.0286)

Spread Brooklyn Nets 21.5
FanDuel: -4500 (1.0222)

Spread Brooklyn Nets 22.5
FanDuel: -4500 (1.0222)

Spread Philadelphia 76ers -15.0
FoxBet: +500 (6.0)

Spread Brooklyn Nets 15.0
FoxBet: -1000 (1.1)

Spread Philadelphia 76ers 12.0
FoxBet: -700 (1.1429)

Spread Brooklyn Nets -12.0
FoxBet: +450 (5.5)

Spread Philadelphia 76ers 13.0
FoxBet: -900 (1.1111)

Spread Brooklyn Nets -13.0
FoxBet: +475 (5.75)
Total
Total Over 226.0
DraftKings: -112 (1.9)
FanDuel: -105 (1.9524)
FoxBet: -125 (1.8)

Total Under 226.0
DraftKings: -109 (1.92)
FanDuel: -115 (1.8696)
FoxBet: -105 (1.95)

Total Over 207.5
DraftKings: -715 (1.14)
FanDuel: -1200 (1.0833)

Total Under 207.5
DraftKings: +480 (5.8)
FanDuel: +630 (7.3)

Total Over 208.0
DraftKings: -670 (1.15)

Total Under 208.0
DraftKings: +460 (5.6)

Total Over 208.5
DraftKings: -625 (1.16)
FanDuel: -1000 (1.1)

Total Under 208.5
DraftKings: +440 (5.4)
FanDuel: +560 (6.6)

Total Over 209.0
DraftKings: -625 (1.16)

Total Under 209.0
DraftKings: +430 (5.3)

Total Over 209.5
DraftKings: -560 (1.18)
FanDuel: -800 (1.125)

Total Under 209.5
DraftKings: +400 (5.0)
FanDuel: +480 (5.8)

Total Over 210.0
DraftKings: -560 (1.18)

Total Under 210.0
DraftKings: +390 (4.9)

Total Over 210.5
DraftKings: -500 (1.2)
FanDuel: -670 (1.1493)

Total Under 210.5
DraftKings: +360 (4.6)
FanDuel: +430 (5.3)

Total Over 211.0
DraftKings: -480 (1.21)

Total Under 211.0
DraftKings: +360 (4.6)

Total Over 211.5
DraftKings: -455 (1.22)
FanDuel: -580 (1.1724)

Total Under 211.5
DraftKings: +330 (4.3)
FanDuel: +380 (4.8)

Total Over 212.0
DraftKings: -435 (1.23)

Total Under 212.0
DraftKings: +325 (4.25)

Total Over 212.5
DraftKings: -400 (1.25)
FanDuel: -500 (1.2)

Total Under 212.5
DraftKings: +300 (4.0)
FanDuel: +340 (4.4)

Total Over 213.0
DraftKings: -385 (1.26)
FoxBet: -450 (1.2222)

Total Under 213.0
DraftKings: +295 (3.95)
FoxBet: +290 (3.9)

Total Over 213.5
DraftKings: -360 (1.28)
FanDuel: -430 (1.2326)
FoxBet: -400 (1.25)

Total Under 213.5
DraftKings: +275 (3.75)
FanDuel: +300 (4.0)
FoxBet: +275 (3.75)

Total Over 214.0
DraftKings: -360 (1.28)
FoxBet: -400 (1.25)

Total Under 214.0
DraftKings: +270 (3.7)
FoxBet: +275 (3.75)

Total Over 214.5
DraftKings: -335 (1.3)
FanDuel: -370 (1.2703)
FoxBet: -350 (1.2857)
BetMGM: -275 (1.35)

Total Under 214.5
DraftKings: +255 (3.55)
FanDuel: +265 (3.65)
FoxBet: +250 (3.5)
BetMGM: +230 (3.3)

Total Over 215.0
DraftKings: -315 (1.32)
FoxBet: -350 (1.2857)

Total Under 215.0
DraftKings: +245 (3.45)
FoxBet: +250 (3.5)

Total Over 215.5
DraftKings: -295 (1.34)
FanDuel: -320 (1.3125)
FoxBet: -300 (1.3333)
BetMGM: -275 (1.36)

Total Under 215.5
DraftKings: +230 (3.3)
FanDuel: +235 (3.35)
FoxBet: +230 (3.3)
BetMGM: +220 (3.2)

Total Over 216.0
DraftKings: -286 (1.35)
FoxBet: -300 (1.3333)

Total Under 216.0
DraftKings: +225 (3.25)
FoxBet: +230 (3.3)

Total Over 216.5
DraftKings: -265 (1.38)
FanDuel: -290 (1.3448)
FoxBet: -286 (1.35)
BetMGM: -250 (1.42)

Total Under 216.5
DraftKings: +210 (3.1)
FanDuel: +215 (3.15)
FoxBet: +205 (3.05)
BetMGM: +195 (2.95)

Total Over 217.0
DraftKings: -265 (1.38)
FoxBet: -286 (1.35)

Total Under 217.0
DraftKings: +205 (3.05)
FoxBet: +205 (3.05)

Total Over 217.5
DraftKings: -245 (1.41)
FanDuel: -265 (1.3774)
FoxBet: -275 (1.3636)
BetMGM: -225 (1.45)

Total Under 217.5
DraftKings: +195 (2.95)
FanDuel: +200 (3.0)
FoxBet: +190 (2.9)
BetMGM: +180 (2.8)

Total Over 218.0
DraftKings: -235 (1.43)
FoxBet: -275 (1.3636)

Total Under 218.0
DraftKings: +188 (2.88)
FoxBet: +190 (2.9)

Total Over 218.5
DraftKings: -225 (1.45)
FanDuel: -240 (1.4167)
FoxBet: -225 (1.4444)
BetMGM: -200 (1.5)

Total Under 218.5
DraftKings: +180 (2.8)
FanDuel: +182 (2.82)
FoxBet: +170 (2.7)
BetMGM: +165 (2.65)

Total Over 219.0
DraftKings: -215 (1.47)
FoxBet: -225 (1.4444)

Total Under 219.0
DraftKings: +170 (2.7)
FoxBet: +165 (2.65)

Total Over 219.5
DraftKings: -200 (1.5)
FanDuel: -205 (1.4878)
FoxBet: -212 (1.4706)
BetMGM: -190 (1.53)

Total Under 219.5
DraftKings: +163 (2.63)
FanDuel: +162 (2.62)
FoxBet: +155 (2.55)
BetMGM: +155 (2.55)

Total Over 220.0
DraftKings: -195 (1.52)
FoxBet: -212 (1.4706)

Total Under 220.0
DraftKings: +155 (2.55)
FoxBet: +155 (2.55)

Total Over 220.5
DraftKings: -182 (1.55)
FanDuel: -188 (1.5319)
FoxBet: -200 (1.5)
BetMGM: -175 (1.57)

Total Under 220.5
DraftKings: +150 (2.5)
FanDuel: +150 (2.5)
FoxBet: +145 (2.45)
BetMGM: +145 (2.45)

Total Over 221.0
DraftKings: -177 (1.57)
FoxBet: -188 (1.5333)

Total Under 221.0
DraftKings: +145 (2.45)
FoxBet: +140 (2.4)

Total Over 221.5
DraftKings: -167 (1.6)
FanDuel: -170 (1.5882)
FoxBet: -182 (1.55)
BetMGM: -160 (1.62)

Total Under 221.5
DraftKings: +138 (2.38)
FanDuel: +136 (2.36)
FoxBet: +135 (2.35)
BetMGM: +135 (2.35)

Total Over 222.0
DraftKings: -162 (1.62)
FoxBet: -175 (1.5714)

Total Under 222.0
DraftKings: +132 (2.32)
FoxBet: +130 (2.3)

Total Over 222.5
DraftKings: -152 (1.66)
FanDuel: -156 (1.641)
FoxBet: -162 (1.6154)
BetMGM: -145 (1.7)

Total Under 222.5
DraftKings: +125 (2.25)
FanDuel: +126 (2.26)
FoxBet: +125 (2.25)
BetMGM: +120 (2.2)

Total Over 223.0
DraftKings: -148 (1.68)
FoxBet: -154 (1.65)

Total Under 223.0
DraftKings: +120 (2.2)
FoxBet: +120 (2.2)

Total Over 223.5
DraftKings: -139 (1.72)
FanDuel: -140 (1.7143)
FoxBet: -143 (1.7)
BetMGM: -135 (1.75)

Total Under 223.5
DraftKings: +115 (2.15)
FanDuel: +114 (2.14)
FoxBet: +110 (2.1)
BetMGM: +110 (2.1)

Total Over 224.0
DraftKings: -134 (1.75)
FoxBet: -137 (1.7273)

Total Under 224.0
DraftKings: +110 (2.1)
FoxBet: +105 (2.05)

Total Over 224.5
DraftKings: -127 (1.79)
FanDuel: -128 (1.7812)
FoxBet: -137 (1.7273)
BetMGM: -125 (1.8)

Total Under 224.5
DraftKings: +105 (2.05)
FanDuel: +104 (2.04)
FoxBet: +105 (2.05)
BetMGM: +105 (2.05)

Total Over 225.0
DraftKings: -122 (1.82)
FoxBet: -133 (1.75)

Total Under 225.0
DraftKings: +100 (2.0)
FoxBet: +100 (2.0)

Total Over 225.5
DraftKings: -117 (1.86)
FanDuel: -116 (1.8621)
FoxBet: -125 (1.8)
BetMGM: -115 (1.87)

Total Under 225.5
DraftKings: -105 (1.96)
FanDuel: -106 (1.9434)
FoxBet: -105 (1.95)
BetMGM: -105 (1.95)

Total Over 226.5
DraftKings: -107 (1.94)
FanDuel: -104 (1.9615)
FoxBet: -110 (1.9091)
BetMGM: -110 (1.91)

Total Under 226.5
DraftKings: -114 (1.88)
FanDuel: -118 (1.8475)
FoxBet: -110 (1.9091)
BetMGM: -110 (1.91)

Total Over 227.0
DraftKings: -103 (1.98)
FoxBet: -105 (1.95)

Total Under 227.0
DraftKings: -120 (1.84)
FoxBet: -125 (1.8)

Total Over 227.5
DraftKings: +102 (2.02)
FanDuel: +104 (2.04)
FoxBet: +100 (2.0)
BetMGM: +100 (2.0)

Total Under 227.5
DraftKings: -124 (1.81)
FanDuel: -128 (1.7812)
FoxBet: -133 (1.75)
BetMGM: -120 (1.83)

Total Over 228.0
DraftKings: +107 (2.07)
FoxBet: +105 (2.05)

Total Under 228.0
DraftKings: -130 (1.77)
FoxBet: -137 (1.7273)

Total Over 228.5
DraftKings: +112 (2.12)
FanDuel: +114 (2.14)
FoxBet: +105 (2.05)
BetMGM: +110 (2.1)

Total Under 228.5
DraftKings: -136 (1.74)
FanDuel: -140 (1.7143)
FoxBet: -137 (1.7273)
BetMGM: -135 (1.75)

Total Over 229.0
DraftKings: +117 (2.17)
FoxBet: +110 (2.1)

Total Under 229.0
DraftKings: -143 (1.7)
FoxBet: -143 (1.7)

Total Over 229.5
DraftKings: +120 (2.2)
FanDuel: +128 (2.28)
FoxBet: +115 (2.15)
BetMGM: +120 (2.2)

Total Under 229.5
DraftKings: -148 (1.68)
FanDuel: -158 (1.6329)
FoxBet: -150 (1.6667)
BetMGM: -145 (1.7)

Total Over 230.0
DraftKings: +128 (2.28)
FoxBet: +120 (2.2)

Total Under 230.0
DraftKings: -157 (1.64)
FoxBet: -154 (1.65)

Total Over 230.5
DraftKings: +132 (2.32)
FanDuel: +136 (2.36)
FoxBet: +125 (2.25)
BetMGM: +125 (2.25)

Total Under 230.5
DraftKings: -162 (1.62)
FanDuel: -172 (1.5814)
FoxBet: -162 (1.6154)
BetMGM: -155 (1.65)

Total Over 231.0
DraftKings: +140 (2.4)
FoxBet: +130 (2.3)

Total Under 231.0
DraftKings: -175 (1.58)
FoxBet: -175 (1.5714)

Total Over 231.5
DraftKings: +145 (2.45)
FanDuel: +148 (2.48)
FoxBet: +135 (2.35)
BetMGM: +140 (2.4)

Total Under 231.5
DraftKings: -177 (1.57)
FanDuel: -186 (1.5376)
FoxBet: -182 (1.55)
BetMGM: -165 (1.6)

Total Over 232.0
DraftKings: +150 (2.5)
FoxBet: +145 (2.45)

Total Under 232.0
DraftKings: -186 (1.54)
FoxBet: -200 (1.5)

Total Over 232.5
DraftKings: +155 (2.55)
FanDuel: +162 (2.62)
FoxBet: +155 (2.55)
BetMGM: +150 (2.5)

Total Under 232.5
DraftKings: -195 (1.52)
FanDuel: -205 (1.4878)
FoxBet: -200 (1.5)
BetMGM: -185 (1.55)

Total Over 233.0
DraftKings: +165 (2.65)
FoxBet: +155 (2.55)

Total Under 233.0
DraftKings: -205 (1.49)
FoxBet: -212 (1.4706)

Total Over 233.5
DraftKings: +170 (2.7)
FanDuel: +178 (2.78)
FoxBet: +165 (2.65)
BetMGM: +155 (2.55)

Total Under 233.5
DraftKings: -215 (1.47)
FanDuel: -235 (1.4255)
FoxBet: -225 (1.4444)
BetMGM: -190 (1.53)

Total Over 234.0
DraftKings: +180 (2.8)
FoxBet: +170 (2.7)

Total Under 234.0
DraftKings: -225 (1.45)
FoxBet: -225 (1.4444)

Total Over 234.5
DraftKings: +185 (2.85)
FanDuel: +196 (2.96)
FoxBet: +185 (2.85)

Total Under 234.5
DraftKings: -235 (1.43)
FanDuel: -260 (1.3846)
FoxBet: -250 (1.4)

Total Over 235.0
DraftKings: +195 (2.95)
FoxBet: +190 (2.9)

Total Under 235.0
DraftKings: -245 (1.41)
FoxBet: -275 (1.3636)

Total Over 235.5
DraftKings: +200 (3.0)
FanDuel: +210 (3.1)
FoxBet: +190 (2.9)

Total Under 235.5
DraftKings: -250 (1.4)
FanDuel: -280 (1.3571)
FoxBet: -275 (1.3636)

Total Over 236.0
DraftKings: +215 (3.15)
FoxBet: +205 (3.05)

Total Under 236.0
DraftKings: -275 (1.37)
FoxBet: -286 (1.35)

Total Over 236.5
DraftKings: +220 (3.2)
FanDuel: +230 (3.3)
FoxBet: +205 (3.05)

Total Under 236.5
DraftKings: -278 (1.36)
FanDuel: -310 (1.3226)
FoxBet: -286 (1.35)

Total Over 237.0
DraftKings: +235 (3.35)
FoxBet: +230 (3.3)

Total Under 237.0
DraftKings: -295 (1.34)
FoxBet: -300 (1.3333)

Total Over 237.5
DraftKings: +240 (3.4)
FanDuel: +260 (3.6)
FoxBet: +230 (3.3)

Total Under 237.5
DraftKings: -305 (1.33)
FanDuel: -360 (1.2778)
FoxBet: -300 (1.3333)

Total Over 238.0
DraftKings: +255 (3.55)
FoxBet: +230 (3.3)

Total Under 238.0
DraftKings: -335 (1.3)
FoxBet: -333 (1.3)

Total Over 238.5
DraftKings: +260 (3.6)
FanDuel: +280 (3.8)
FoxBet: +250 (3.5)

Total Under 238.5
DraftKings: -335 (1.3)
FanDuel: -390 (1.2564)
FoxBet: -350 (1.2857)

Total Over 239.0
DraftKings: +280 (3.8)
FoxBet: +275 (3.75)

Total Under 239.0
DraftKings: -360 (1.28)
FoxBet: -400 (1.25)

Total Over 239.5
DraftKings: +285 (3.85)
FanDuel: +320 (4.2)
FoxBet: +280 (3.8)

Total Under 239.5
DraftKings: -375 (1.27)
FanDuel: -460 (1.2174)
FoxBet: -400 (1.25)

Total Over 240.0
DraftKings: +300 (4.0)
FoxBet: +290 (3.9)

Total Under 240.0
DraftKings: -400 (1.25)
FoxBet: -450 (1.2222)

Total Over 240.5
DraftKings: +310 (4.1)
FanDuel: +350 (4.5)

Total Under 240.5
DraftKings: -400 (1.25)
FanDuel: -520 (1.1923)

Total Over 241.0
DraftKings: +330 (4.3)

Total Under 241.0
DraftKings: -435 (1.23)

Total Over 241.5
DraftKings: +335 (4.35)
FanDuel: +390 (4.9)

Total Under 241.5
DraftKings: -455 (1.22)
FanDuel: -590 (1.1695)

Total Over 242.0
DraftKings: +360 (4.6)

Total Under 242.0
DraftKings: -480 (1.21)

Total Over 242.5
DraftKings: +370 (4.7)
FanDuel: +440 (5.4)

Total Under 242.5
DraftKings: -500 (1.2)
FanDuel: -700 (1.1429)

Total Over 243.0
DraftKings: +390 (4.9)

Total Under 243.0
DraftKings: -530 (1.19)

Total Over 243.5
DraftKings: +400 (5.0)
FanDuel: +500 (6.0)

Total Under 243.5
DraftKings: -560 (1.18)
FanDuel: -850 (1.1176)

Total Over 200.5
FanDuel: -5000 (1.02)

Total Over 201.5
FanDuel: -4000 (1.025)

Total Over 202.5
FanDuel: -3500 (1.0286)

Total Over 203.5
FanDuel: -3000 (1.0333)

Total Over 204.5
FanDuel: -2300 (1.0435)

Total Over 205.5
FanDuel: -1900 (1.0526)

Total Over 206.5
FanDuel: -1450 (1.069)

Total Over 244.5
FanDuel: +560 (6.6)

Total Over 245.5
FanDuel: +630 (7.3)

Total Over 246.5
FanDuel: +700 (8.0)

Total Over 247.5
FanDuel: +800 (9.0)

Total Over 248.5
FanDuel: +880 (9.8)

Total Over 249.5
FanDuel: +940 (10.4)

Total Over 250.5
FanDuel: +1000 (11.0)

Total Under 200.5
FanDuel: +1400 (15.0)

Total Under 201.5
FanDuel: +1300 (14.0)

Total Under 202.5
FanDuel: +1120 (12.2)

Total Under 203.5
FanDuel: +1100 (12.0)

Total Under 204.5
FanDuel: +940 (10.4)

Total Under 205.5
FanDuel: +820 (9.2)

Total Under 206.5
FanDuel: +700 (8.0)

Total Under 244.5
FanDuel: -1000 (1.1)

Total Under 245.5
FanDuel: -1200 (1.0833)

Total Under 246.5
FanDuel: -1450 (1.069)

Total Under 247.5
FanDuel: -1800 (1.0556)

Total Under 248.5
FanDuel: -2200 (1.0455)

Total Under 249.5
FanDuel: -2300 (1.0435)

Total Under 250.5
FanDuel: -2400 (1.0417)
Moneyline
Moneyline Philadelphia 76ers None
DraftKings: -121 (1.83)
FanDuel: -118 (1.8475)
FoxBet: -120 (1.8333)
BetMGM: -120 (1.83)

Moneyline Brooklyn Nets None
DraftKings: +102 (2.02)
FanDuel: +100 (2.0)
FoxBet: +100 (2.0)
BetMGM: +100 (2.0)


, 'Cleveland Cavaliers @ Memphis Grizzlies': NBA: Cleveland Cavaliers @ Memphis Grizzlies
DraftKings - 179653579
FanDuel - 949396.3
FoxBet - 8715834
BetMGM - 10987583
Moneyline
Moneyline Cleveland Cavaliers None
FanDuel: +190 (2.9)
FoxBet: +180 (2.8)
BetMGM: +195 (2.95)

Moneyline Memphis Grizzlies None
FanDuel: -230 (1.4348)
FoxBet: -225 (1.4444)
BetMGM: -250 (1.42)
Spread
Spread Cleveland Cavaliers 5.5
FanDuel: -110 (1.9091)
FoxBet: -110 (1.9091)
BetMGM: -105 (1.95)

Spread Memphis Grizzlies -5.5
FanDuel: -110 (1.9091)
FoxBet: -110 (1.9091)
BetMGM: -115 (1.87)

Spread Cleveland Cavaliers 27.5
FanDuel: -5000 (1.02)

Spread Cleveland Cavaliers 26.5
FanDuel: -4500 (1.0222)

Spread Cleveland Cavaliers 25.5
FanDuel: -3500 (1.0286)

Spread Cleveland Cavaliers 24.5
FanDuel: -3000 (1.0333)

Spread Cleveland Cavaliers 23.5
FanDuel: -3000 (1.0333)

Spread Cleveland Cavaliers 22.5
FanDuel: -2400 (1.0417)

Spread Cleveland Cavaliers 21.5
FanDuel: -2000 (1.05)

Spread Cleveland Cavaliers 20.5
FanDuel: -1450 (1.069)

Spread Cleveland Cavaliers 19.5
FanDuel: -1200 (1.0833)

Spread Cleveland Cavaliers 18.5
FanDuel: -950 (1.1053)
FoxBet: -900 (1.1111)

Spread Cleveland Cavaliers 17.5
FanDuel: -800 (1.125)
FoxBet: -700 (1.1429)

Spread Cleveland Cavaliers 16.5
FanDuel: -650 (1.1538)
FoxBet: -650 (1.1538)

Spread Cleveland Cavaliers 15.5
FanDuel: -550 (1.1818)
FoxBet: -550 (1.1818)

Spread Cleveland Cavaliers 14.5
FanDuel: -460 (1.2174)
FoxBet: -450 (1.2222)

Spread Cleveland Cavaliers 13.5
FanDuel: -385 (1.2597)
FoxBet: -400 (1.25)
BetMGM: -375 (1.26)

Spread Cleveland Cavaliers 12.5
FanDuel: -330 (1.303)
FoxBet: -350 (1.2857)
BetMGM: -350 (1.3)

Spread Cleveland Cavaliers 11.5
FanDuel: -290 (1.3448)
FoxBet: -300 (1.3333)
BetMGM: -275 (1.36)

Spread Cleveland Cavaliers 10.5
FanDuel: -245 (1.4082)
FoxBet: -275 (1.3636)
BetMGM: -250 (1.42)

Spread Cleveland Cavaliers 9.5
FanDuel: -200 (1.5)
FoxBet: -225 (1.4444)
BetMGM: -200 (1.48)

Spread Cleveland Cavaliers 8.5
FanDuel: -170 (1.5882)
FoxBet: -200 (1.5)
BetMGM: -175 (1.57)

Spread Cleveland Cavaliers 7.5
FanDuel: -148 (1.6757)
FoxBet: -162 (1.6154)
BetMGM: -150 (1.67)

Spread Cleveland Cavaliers 6.5
FanDuel: -130 (1.7692)
FoxBet: -137 (1.7273)
BetMGM: -125 (1.8)

Spread Cleveland Cavaliers 4.5
FanDuel: +106 (2.06)
FoxBet: +105 (2.05)
BetMGM: +110 (2.1)

Spread Cleveland Cavaliers 3.5
FanDuel: +126 (2.26)
FoxBet: +120 (2.2)
BetMGM: +125 (2.25)

Spread Cleveland Cavaliers 2.5
FanDuel: +140 (2.4)
FoxBet: +140 (2.4)
BetMGM: +145 (2.45)

Spread Cleveland Cavaliers 1.5
FanDuel: +162 (2.62)
FoxBet: +155 (2.55)
BetMGM: +165 (2.65)

Spread Cleveland Cavaliers 0.5
FanDuel: +178 (2.78)

Spread Cleveland Cavaliers -0.5
FanDuel: +178 (2.78)

Spread Cleveland Cavaliers -1.5
FanDuel: +192 (2.92)
FoxBet: +190 (2.9)
BetMGM: +200 (3.0)

Spread Cleveland Cavaliers -2.5
FanDuel: +215 (3.15)
FoxBet: +205 (3.05)
BetMGM: +230 (3.3)

Spread Cleveland Cavaliers -3.5
FanDuel: +235 (3.35)
FoxBet: +230 (3.3)
BetMGM: +260 (3.6)

Spread Cleveland Cavaliers -4.5
FanDuel: +280 (3.8)
FoxBet: +275 (3.75)
BetMGM: +310 (4.1)

Spread Cleveland Cavaliers -5.5
FanDuel: +340 (4.4)
FoxBet: +290 (3.9)
BetMGM: +360 (4.6)

Spread Cleveland Cavaliers -6.5
FanDuel: +390 (4.9)
FoxBet: +333 (4.3333)
BetMGM: +425 (5.25)

Spread Cleveland Cavaliers -7.5
FanDuel: +460 (5.6)
FoxBet: +425 (5.25)
BetMGM: +525 (6.25)

Spread Cleveland Cavaliers -8.5
FanDuel: +520 (6.2)
FoxBet: +475 (5.75)

Spread Cleveland Cavaliers -9.5
FanDuel: +600 (7.0)

Spread Cleveland Cavaliers -10.5
FanDuel: +730 (8.3)

Spread Cleveland Cavaliers -11.5
FanDuel: +870 (9.7)

Spread Cleveland Cavaliers -12.5
FanDuel: +920 (10.2)

Spread Cleveland Cavaliers -13.5
FanDuel: +1000 (11.0)

Spread Cleveland Cavaliers -14.5
FanDuel: +1100 (12.0)

Spread Cleveland Cavaliers -15.5
FanDuel: +1300 (14.0)

Spread Cleveland Cavaliers -16.5
FanDuel: +1400 (15.0)

Spread Cleveland Cavaliers -17.5
FanDuel: +1800 (19.0)

Spread Cleveland Cavaliers -18.5
FanDuel: +2000 (21.0)

Spread Cleveland Cavaliers -19.5
FanDuel: +2200 (23.0)

Spread Cleveland Cavaliers -20.5
FanDuel: +3000 (31.0)

Spread Memphis Grizzlies -28.5
FanDuel: +1800 (19.0)

Spread Memphis Grizzlies -27.5
FanDuel: +1400 (15.0)

Spread Memphis Grizzlies -26.5
FanDuel: +1300 (14.0)

Spread Memphis Grizzlies -25.5
FanDuel: +1200 (13.0)

Spread Memphis Grizzlies -24.5
FanDuel: +1000 (11.0)

Spread Memphis Grizzlies -23.5
FanDuel: +1000 (11.0)

Spread Memphis Grizzlies -22.5
FanDuel: +900 (10.0)

Spread Memphis Grizzlies -21.5
FanDuel: +830 (9.3)

Spread Memphis Grizzlies -20.5
FanDuel: +700 (8.0)

Spread Memphis Grizzlies -19.5
FanDuel: +630 (7.3)

Spread Memphis Grizzlies -18.5
FanDuel: +540 (6.4)
FoxBet: +500 (6.0)

Spread Memphis Grizzlies -17.5
FanDuel: +480 (5.8)
FoxBet: +425 (5.25)

Spread Memphis Grizzlies -16.5
FanDuel: +420 (5.2)
FoxBet: +380 (4.8)

Spread Memphis Grizzlies -15.5
FanDuel: +360 (4.6)
FoxBet: +333 (4.3333)

Spread Memphis Grizzlies -14.5
FanDuel: +320 (4.2)
FoxBet: +290 (3.9)

Spread Memphis Grizzlies -13.5
FanDuel: +275 (3.75)
FoxBet: +275 (3.75)
BetMGM: +290 (3.9)

Spread Memphis Grizzlies -12.5
FanDuel: +240 (3.4)
FoxBet: +250 (3.5)
BetMGM: +260 (3.6)

Spread Memphis Grizzlies -11.5
FanDuel: +215 (3.15)
FoxBet: +230 (3.3)
BetMGM: +225 (3.25)

Spread Memphis Grizzlies -10.5
FanDuel: +186 (2.86)
FoxBet: +200 (3.0)
BetMGM: +195 (2.95)

Spread Memphis Grizzlies -9.5
FanDuel: +154 (2.54)
FoxBet: +170 (2.7)
BetMGM: +170 (2.7)

Spread Memphis Grizzlies -8.5
FanDuel: +134 (2.34)
FoxBet: +145 (2.45)
BetMGM: +145 (2.45)

Spread Memphis Grizzlies -7.5
FanDuel: +122 (2.22)
FoxBet: +125 (2.25)
BetMGM: +125 (2.25)

Spread Memphis Grizzlies -6.5
FanDuel: +106 (2.06)
FoxBet: +105 (2.05)
BetMGM: +105 (2.05)

Spread Memphis Grizzlies -4.5
FanDuel: -130 (1.7692)
FoxBet: -137 (1.7273)
BetMGM: -135 (1.75)

Spread Memphis Grizzlies -3.5
FanDuel: -156 (1.641)
FoxBet: -154 (1.65)
BetMGM: -155 (1.65)

Spread Memphis Grizzlies -2.5
FanDuel: -180 (1.5556)
FoxBet: -188 (1.5333)
BetMGM: -175 (1.57)

Spread Memphis Grizzlies -1.5
FanDuel: -210 (1.4762)
FoxBet: -212 (1.4706)
BetMGM: -200 (1.5)

Spread Memphis Grizzlies -0.5
FanDuel: -235 (1.4255)

Spread Memphis Grizzlies 0.5
FanDuel: -235 (1.4255)

Spread Memphis Grizzlies 1.5
FanDuel: -255 (1.3922)
FoxBet: -275 (1.3636)
BetMGM: -250 (1.4)

Spread Memphis Grizzlies 2.5
FanDuel: -290 (1.3448)
FoxBet: -286 (1.35)
BetMGM: -300 (1.34)

Spread Memphis Grizzlies 3.5
FanDuel: -320 (1.3125)
FoxBet: -333 (1.3)
BetMGM: -350 (1.3)

Spread Memphis Grizzlies 4.5
FanDuel: -390 (1.2564)
FoxBet: -400 (1.25)
BetMGM: -400 (1.25)

Spread Memphis Grizzlies 5.5
FanDuel: -500 (1.2)
FoxBet: -450 (1.2222)
BetMGM: -500 (1.2)

Spread Memphis Grizzlies 6.5
FanDuel: -600 (1.1667)
FoxBet: -550 (1.1818)
BetMGM: -600 (1.17)

Spread Memphis Grizzlies 7.5
FanDuel: -750 (1.1333)
FoxBet: -650 (1.1538)
BetMGM: -750 (1.13)

Spread Memphis Grizzlies 8.5
FanDuel: -900 (1.1111)
FoxBet: -800 (1.125)

Spread Memphis Grizzlies 9.5
FanDuel: -1100 (1.0909)

Spread Memphis Grizzlies 10.5
FanDuel: -1600 (1.0625)

Spread Memphis Grizzlies 11.5
FanDuel: -2200 (1.0455)

Spread Memphis Grizzlies 12.5
FanDuel: -3000 (1.0333)

Spread Memphis Grizzlies 13.5
FanDuel: -3000 (1.0333)

Spread Memphis Grizzlies 14.5
FanDuel: -3500 (1.0286)

Spread Memphis Grizzlies 15.5
FanDuel: -4500 (1.0222)

Spread Memphis Grizzlies 16.5
FanDuel: -5000 (1.02)

Spread Memphis Grizzlies -12.0
FoxBet: +230 (3.3)

Spread Cleveland Cavaliers 12.0
FoxBet: -333 (1.3)

Spread Memphis Grizzlies -11.0
FoxBet: +215 (3.15)

Spread Cleveland Cavaliers 11.0
FoxBet: -300 (1.3333)

Spread Memphis Grizzlies -14.0
FoxBet: +290 (3.9)

Spread Cleveland Cavaliers 14.0
FoxBet: -450 (1.2222)

Spread Memphis Grizzlies -13.0
FoxBet: +275 (3.75)

Spread Cleveland Cavaliers 13.0
FoxBet: -400 (1.25)

Spread Cleveland Cavaliers 16.0
FoxBet: -650 (1.1538)

Spread Memphis Grizzlies -16.0
FoxBet: +380 (4.8)

Spread Memphis Grizzlies -15.0
FoxBet: +333 (4.3333)

Spread Cleveland Cavaliers 15.0
FoxBet: -550 (1.1818)

Spread Memphis Grizzlies -18.0
FoxBet: +475 (5.75)

Spread Cleveland Cavaliers 18.0
FoxBet: -900 (1.1111)

Spread Memphis Grizzlies -17.0
FoxBet: +425 (5.25)

Spread Cleveland Cavaliers 17.0
FoxBet: -700 (1.1429)

Spread Memphis Grizzlies -19.0
FoxBet: +500 (6.0)

Spread Cleveland Cavaliers 19.0
FoxBet: -1000 (1.1)

Spread Cleveland Cavaliers -9.0
FoxBet: +475 (5.75)

Spread Memphis Grizzlies 9.0
FoxBet: -900 (1.1111)

Spread Memphis Grizzlies 6.0
FoxBet: -550 (1.1818)

Spread Cleveland Cavaliers -6.0
FoxBet: +333 (4.3333)

Spread Cleveland Cavaliers -5.0
FoxBet: +290 (3.9)

Spread Memphis Grizzlies 5.0
FoxBet: -450 (1.2222)

Spread Memphis Grizzlies 8.0
FoxBet: -800 (1.125)

Spread Cleveland Cavaliers -8.0
FoxBet: +475 (5.75)

Spread Memphis Grizzlies 7.0
FoxBet: -650 (1.1538)

Spread Cleveland Cavaliers -7.0
FoxBet: +380 (4.8)

Spread Cleveland Cavaliers -2.0
FoxBet: +205 (3.05)

Spread Memphis Grizzlies 2.0
FoxBet: -286 (1.35)

Spread Memphis Grizzlies 1.0
FoxBet: -275 (1.3636)

Spread Cleveland Cavaliers -1.0
FoxBet: +190 (2.9)

Spread Cleveland Cavaliers -4.0
FoxBet: +275 (3.75)

Spread Memphis Grizzlies 4.0
FoxBet: -400 (1.25)

Spread Memphis Grizzlies 3.0
FoxBet: -333 (1.3)

Spread Cleveland Cavaliers -3.0
FoxBet: +230 (3.3)

Spread Memphis Grizzlies 0.0
FoxBet: -225 (1.4444)

Spread Cleveland Cavaliers 0.0
FoxBet: +170 (2.7)

Spread Cleveland Cavaliers 1.0
FoxBet: +165 (2.65)

Spread Memphis Grizzlies -1.0
FoxBet: -225 (1.4444)

Spread Cleveland Cavaliers 7.0
FoxBet: -150 (1.6667)

Spread Memphis Grizzlies -7.0
FoxBet: +115 (2.15)

Spread Memphis Grizzlies -6.0
FoxBet: -105 (1.95)

Spread Cleveland Cavaliers 6.0
FoxBet: -125 (1.8)

Spread Cleveland Cavaliers 9.0
FoxBet: -212 (1.4706)

Spread Memphis Grizzlies -9.0
FoxBet: +155 (2.55)

Spread Cleveland Cavaliers 8.0
FoxBet: -182 (1.55)

Spread Memphis Grizzlies -8.0
FoxBet: +135 (2.35)

Spread Memphis Grizzlies -3.0
FoxBet: -175 (1.5714)

Spread Cleveland Cavaliers 3.0
FoxBet: +130 (2.3)

Spread Memphis Grizzlies -2.0
FoxBet: -200 (1.5)

Spread Cleveland Cavaliers 2.0
FoxBet: +155 (2.55)

Spread Memphis Grizzlies -5.0
FoxBet: -133 (1.75)

Spread Cleveland Cavaliers 5.0
FoxBet: +100 (2.0)

Spread Memphis Grizzlies -4.0
FoxBet: -150 (1.6667)

Spread Cleveland Cavaliers 4.0
FoxBet: +115 (2.15)

Spread Memphis Grizzlies -10.0
FoxBet: +190 (2.9)

Spread Cleveland Cavaliers 10.0
FoxBet: -275 (1.3636)
Total
Total Over 208.5
FanDuel: -110 (1.9091)
FoxBet: -105 (1.95)
BetMGM: -115 (1.87)

Total Under 208.5
FanDuel: -110 (1.9091)
FoxBet: -125 (1.8)
BetMGM: -105 (1.95)

Total Over 186.5
FanDuel: -2400 (1.0417)

Total Over 187.5
FanDuel: -2200 (1.0455)

Total Over 188.5
FanDuel: -1800 (1.0556)

Total Over 189.5
FanDuel: -1500 (1.0667)

Total Over 190.5
FanDuel: -1200 (1.0833)

Total Over 191.5
FanDuel: -1000 (1.1)

Total Over 192.5
FanDuel: -800 (1.125)

Total Over 193.5
FanDuel: -670 (1.1493)

Total Over 194.5
FanDuel: -550 (1.1818)

Total Over 195.5
FanDuel: -460 (1.2174)
FoxBet: -400 (1.25)

Total Over 196.5
FanDuel: -400 (1.25)
FoxBet: -400 (1.25)

Total Over 197.5
FanDuel: -360 (1.2778)
FoxBet: -333 (1.3)

Total Over 198.5
FanDuel: -310 (1.3226)
FoxBet: -300 (1.3333)

Total Over 199.5
FanDuel: -280 (1.3571)
FoxBet: -275 (1.3636)

Total Over 200.5
FanDuel: -250 (1.4)
FoxBet: -250 (1.4)

Total Over 201.5
FanDuel: -225 (1.4444)
FoxBet: -212 (1.4706)

Total Over 202.5
FanDuel: -198 (1.5051)
FoxBet: -200 (1.5)

Total Over 203.5
FanDuel: -180 (1.5556)
FoxBet: -182 (1.55)
BetMGM: -165 (1.6)

Total Over 204.5
FanDuel: -166 (1.6024)
FoxBet: -162 (1.6154)
BetMGM: -160 (1.62)

Total Over 205.5
FanDuel: -154 (1.6494)
FoxBet: -143 (1.7)
BetMGM: -145 (1.7)

Total Over 206.5
FanDuel: -136 (1.7353)
FoxBet: -137 (1.7273)
BetMGM: -135 (1.75)

Total Over 207.5
FanDuel: -122 (1.8197)
FoxBet: -125 (1.8)
BetMGM: -120 (1.83)

Total Over 209.5
FanDuel: +100 (2.0)
FoxBet: +100 (2.0)
BetMGM: -105 (1.95)

Total Over 210.5
FanDuel: +110 (2.1)
FoxBet: +110 (2.1)
BetMGM: +105 (2.05)

Total Over 211.5
FanDuel: +122 (2.22)
FoxBet: +120 (2.2)
BetMGM: +110 (2.1)

Total Over 212.5
FanDuel: +134 (2.34)
FoxBet: +130 (2.3)
BetMGM: +120 (2.2)

Total Over 213.5
FanDuel: +146 (2.46)
FoxBet: +140 (2.4)
BetMGM: +135 (2.35)

Total Over 214.5
FanDuel: +158 (2.58)
FoxBet: +155 (2.55)
BetMGM: +145 (2.45)

Total Over 215.5
FanDuel: +174 (2.74)
FoxBet: +170 (2.7)
BetMGM: +155 (2.55)

Total Over 216.5
FanDuel: +190 (2.9)
FoxBet: +190 (2.9)
BetMGM: +165 (2.65)

Total Over 217.5
FanDuel: +210 (3.1)
FoxBet: +205 (3.05)
BetMGM: +180 (2.8)

Total Over 218.5
FanDuel: +230 (3.3)
FoxBet: +230 (3.3)
BetMGM: +195 (2.95)

Total Over 219.5
FanDuel: +260 (3.6)
FoxBet: +230 (3.3)
BetMGM: +200 (3.0)

Total Over 220.5
FanDuel: +280 (3.8)
FoxBet: +275 (3.75)
BetMGM: +225 (3.25)

Total Over 221.5
FanDuel: +310 (4.1)
FoxBet: +290 (3.9)
BetMGM: +240 (3.4)

Total Over 222.5
FanDuel: +350 (4.5)

Total Over 223.5
FanDuel: +390 (4.9)

Total Over 224.5
FanDuel: +450 (5.5)

Total Over 225.5
FanDuel: +500 (6.0)

Total Over 226.5
FanDuel: +580 (6.8)

Total Over 227.5
FanDuel: +630 (7.3)

Total Over 228.5
FanDuel: +700 (8.0)

Total Over 229.5
FanDuel: +780 (8.8)

Total Over 230.5
FanDuel: +850 (9.5)

Total Over 231.5
FanDuel: +940 (10.4)

Total Over 232.5
FanDuel: +1100 (12.0)

Total Over 233.5
FanDuel: +1120 (12.2)

Total Over 234.5
FanDuel: +1200 (13.0)

Total Over 235.5
FanDuel: +1300 (14.0)

Total Over 236.5
FanDuel: +1500 (16.0)

Total Under 186.5
FanDuel: +1000 (11.0)

Total Under 187.5
FanDuel: +900 (10.0)

Total Under 188.5
FanDuel: +800 (9.0)

Total Under 189.5
FanDuel: +720 (8.2)

Total Under 190.5
FanDuel: +630 (7.3)

Total Under 191.5
FanDuel: +560 (6.6)

Total Under 192.5
FanDuel: +480 (5.8)

Total Under 193.5
FanDuel: +430 (5.3)

Total Under 194.5
FanDuel: +370 (4.7)

Total Under 195.5
FanDuel: +320 (4.2)
FoxBet: +280 (3.8)

Total Under 196.5
FanDuel: +285 (3.85)
FoxBet: +275 (3.75)

Total Under 197.5
FanDuel: +260 (3.6)
FoxBet: +230 (3.3)

Total Under 198.5
FanDuel: +230 (3.3)
FoxBet: +215 (3.15)

Total Under 199.5
FanDuel: +210 (3.1)
FoxBet: +200 (3.0)

Total Under 200.5
FanDuel: +190 (2.9)
FoxBet: +185 (2.85)

Total Under 201.5
FanDuel: +174 (2.74)
FoxBet: +155 (2.55)

Total Under 202.5
FanDuel: +158 (2.58)
FoxBet: +145 (2.45)

Total Under 203.5
FanDuel: +144 (2.44)
FoxBet: +135 (2.35)
BetMGM: +140 (2.4)

Total Under 204.5
FanDuel: +134 (2.34)
FoxBet: +125 (2.25)
BetMGM: +135 (2.35)

Total Under 205.5
FanDuel: +124 (2.24)
FoxBet: +110 (2.1)
BetMGM: +120 (2.2)

Total Under 206.5
FanDuel: +110 (2.1)
FoxBet: +105 (2.05)
BetMGM: +110 (2.1)

Total Under 207.5
FanDuel: +100 (2.0)
FoxBet: -105 (1.95)
BetMGM: +100 (2.0)

Total Under 209.5
FanDuel: -122 (1.8197)
FoxBet: -133 (1.75)
BetMGM: -115 (1.87)

Total Under 210.5
FanDuel: -136 (1.7353)
FoxBet: -143 (1.7)
BetMGM: -125 (1.8)

Total Under 211.5
FanDuel: -150 (1.6667)
FoxBet: -154 (1.65)
BetMGM: -135 (1.75)

Total Under 212.5
FanDuel: -166 (1.6024)
FoxBet: -175 (1.5714)
BetMGM: -145 (1.7)

Total Under 213.5
FanDuel: -182 (1.5495)
FoxBet: -188 (1.5333)
BetMGM: -160 (1.62)

Total Under 214.5
FanDuel: -198 (1.5051)
FoxBet: -212 (1.4706)
BetMGM: -175 (1.57)

Total Under 215.5
FanDuel: -225 (1.4444)
FoxBet: -225 (1.4444)
BetMGM: -190 (1.53)

Total Under 216.5
FanDuel: -250 (1.4)
FoxBet: -275 (1.3636)
BetMGM: -200 (1.5)

Total Under 217.5
FanDuel: -280 (1.3571)
FoxBet: -286 (1.35)
BetMGM: -225 (1.45)

Total Under 218.5
FanDuel: -310 (1.3226)
FoxBet: -300 (1.3333)
BetMGM: -250 (1.42)

Total Under 219.5
FanDuel: -360 (1.2778)
FoxBet: -333 (1.3)
BetMGM: -250 (1.4)

Total Under 220.5
FanDuel: -390 (1.2564)
FoxBet: -400 (1.25)
BetMGM: -275 (1.35)

Total Under 221.5
FanDuel: -440 (1.2273)
FoxBet: -450 (1.2222)
BetMGM: -300 (1.33)

Total Under 222.5
FanDuel: -520 (1.1923)

Total Under 223.5
FanDuel: -590 (1.1695)

Total Under 224.5
FanDuel: -720 (1.1389)

Total Under 225.5
FanDuel: -850 (1.1176)

Total Under 226.5
FanDuel: -1050 (1.0952)

Total Under 227.5
FanDuel: -1200 (1.0833)

Total Under 228.5
FanDuel: -1450 (1.069)

Total Under 229.5
FanDuel: -1700 (1.0588)

Total Under 230.5
FanDuel: -2000 (1.05)

Total Under 231.5
FanDuel: -2300 (1.0435)

Total Under 232.5
FanDuel: -3000 (1.0333)

Total Under 233.5
FanDuel: -3500 (1.0286)

Total Under 234.5
FanDuel: -4000 (1.025)

Total Under 235.5
FanDuel: -4500 (1.0222)

Total Under 236.5
FanDuel: -6000 (1.0167)

Total Under 195.0
FoxBet: +290 (3.9)

Total Over 195.0
FoxBet: -450 (1.2222)

Total Under 196.0
FoxBet: +275 (3.75)

Total Over 196.0
FoxBet: -400 (1.25)

Total Over 197.0
FoxBet: -350 (1.2857)

Total Under 197.0
FoxBet: +250 (3.5)

Total Under 198.0
FoxBet: +230 (3.3)

Total Over 198.0
FoxBet: -300 (1.3333)

Total Under 199.0
FoxBet: +205 (3.05)

Total Over 199.0
FoxBet: -286 (1.35)

Total Under 200.0
FoxBet: +190 (2.9)

Total Over 200.0
FoxBet: -275 (1.3636)

Total Under 201.0
FoxBet: +170 (2.7)

Total Over 201.0
FoxBet: -225 (1.4444)

Total Over 202.0
FoxBet: -212 (1.4706)

Total Under 202.0
FoxBet: +155 (2.55)

Total Under 203.0
FoxBet: +140 (2.4)

Total Over 203.0
FoxBet: -188 (1.5333)

Total Under 204.0
FoxBet: +130 (2.3)

Total Over 204.0
FoxBet: -175 (1.5714)

Total Under 205.0
FoxBet: +115 (2.15)

Total Over 205.0
FoxBet: -150 (1.6667)

Total Under 206.0
FoxBet: +105 (2.05)

Total Over 206.0
FoxBet: -137 (1.7273)

Total Under 207.0
FoxBet: +100 (2.0)

Total Over 207.0
FoxBet: -133 (1.75)

Total Under 208.0
FoxBet: -110 (1.9091)

Total Over 208.0
FoxBet: -110 (1.9091)

Total Over 209.0
FoxBet: -105 (1.95)

Total Under 209.0
FoxBet: -125 (1.8)

Total Under 210.0
FoxBet: -137 (1.7273)

Total Over 210.0
FoxBet: +105 (2.05)

Total Under 211.0
FoxBet: -150 (1.6667)

Total Over 211.0
FoxBet: +115 (2.15)

Total Under 212.0
FoxBet: -162 (1.6154)

Total Over 212.0
FoxBet: +125 (2.25)

Total Under 213.0
FoxBet: -182 (1.55)

Total Over 213.0
FoxBet: +135 (2.35)

Total Under 214.0
FoxBet: -212 (1.4706)

Total Over 214.0
FoxBet: +155 (2.55)

Total Under 215.0
FoxBet: -225 (1.4444)

Total Over 215.0
FoxBet: +165 (2.65)

Total Over 216.0
FoxBet: +185 (2.85)

Total Under 216.0
FoxBet: -250 (1.4)

Total Over 217.0
FoxBet: +200 (3.0)

Total Under 217.0
FoxBet: -275 (1.3636)

Total Under 218.0
FoxBet: -300 (1.3333)

Total Over 218.0
FoxBet: +215 (3.15)

Total Under 219.0
FoxBet: -333 (1.3)

Total Over 219.0
FoxBet: +230 (3.3)

Total Under 220.0
FoxBet: -400 (1.25)

Total Over 220.0
FoxBet: +275 (3.75)

Total Under 221.0
FoxBet: -400 (1.25)

Total Over 221.0
FoxBet: +275 (3.75)

Total Over 222.0
FoxBet: +290 (3.9)

Total Under 222.0
FoxBet: -450 (1.2222)

Total Over 202.2
BetMGM: -190 (1.53)

Total Under 202.2
BetMGM: +155 (2.55)


, 'Dallas Mavericks @ Denver Nuggets': NBA: Dallas Mavericks @ Denver Nuggets
DraftKings - 179653577
FanDuel - 949398.3
FoxBet - 8715836
BetMGM - 10987585
Spread
Spread Dallas Mavericks 3.0
DraftKings: -109 (1.92)
FanDuel: -114 (1.8772)
FoxBet: -105 (1.95)

Spread Denver Nuggets -3.0
DraftKings: -112 (1.9)
FanDuel: -106 (1.9434)
FoxBet: -125 (1.8)

Spread Dallas Mavericks -10.5
DraftKings: +460 (5.6)
FanDuel: +480 (5.8)
FoxBet: +425 (5.25)

Spread Denver Nuggets 10.5
DraftKings: -670 (1.15)
FanDuel: -800 (1.125)
FoxBet: -700 (1.1429)

Spread Dallas Mavericks -10.0
DraftKings: +450 (5.5)
FoxBet: +425 (5.25)

Spread Denver Nuggets 10.0
DraftKings: -625 (1.16)
FoxBet: -700 (1.1429)

Spread Dallas Mavericks -9.5
DraftKings: +400 (5.0)
FanDuel: +390 (4.9)
FoxBet: +380 (4.8)

Spread Denver Nuggets 9.5
DraftKings: -560 (1.18)
FanDuel: -600 (1.1667)
FoxBet: -650 (1.1538)

Spread Dallas Mavericks -9.0
DraftKings: +390 (4.9)
FoxBet: +380 (4.8)

Spread Denver Nuggets 9.0
DraftKings: -530 (1.19)
FoxBet: -650 (1.1538)

Spread Dallas Mavericks -8.5
DraftKings: +350 (4.5)
FanDuel: +340 (4.4)
FoxBet: +333 (4.3333)

Spread Denver Nuggets 8.5
DraftKings: -480 (1.21)
FanDuel: -500 (1.2)
FoxBet: -500 (1.2)

Spread Dallas Mavericks -8.0
DraftKings: +340 (4.4)
FoxBet: +320 (4.2)

Spread Denver Nuggets 8.0
DraftKings: -455 (1.22)
FoxBet: -500 (1.2)

Spread Dallas Mavericks -7.5
DraftKings: +300 (4.0)
FanDuel: +285 (3.85)
FoxBet: +290 (3.9)
BetMGM: +340 (4.4)

Spread Denver Nuggets 7.5
DraftKings: -400 (1.25)
FanDuel: -400 (1.25)
FoxBet: -450 (1.2222)
BetMGM: -450 (1.22)

Spread Dallas Mavericks -7.0
DraftKings: +295 (3.95)
FoxBet: +290 (3.9)

Spread Denver Nuggets 7.0
DraftKings: -385 (1.26)
FoxBet: -450 (1.2222)

Spread Dallas Mavericks -6.5
DraftKings: +265 (3.65)
FanDuel: +255 (3.55)
FoxBet: +275 (3.75)
BetMGM: +280 (3.8)

Spread Denver Nuggets 6.5
DraftKings: -345 (1.29)
FanDuel: -350 (1.2857)
FoxBet: -400 (1.25)
BetMGM: -350 (1.28)

Spread Dallas Mavericks -6.0
DraftKings: +255 (3.55)
FoxBet: +250 (3.5)

Spread Denver Nuggets 6.0
DraftKings: -335 (1.3)
FoxBet: -350 (1.2857)

Spread Dallas Mavericks -5.5
DraftKings: +230 (3.3)
FanDuel: +230 (3.3)
FoxBet: +230 (3.3)
BetMGM: +240 (3.4)

Spread Denver Nuggets 5.5
DraftKings: -295 (1.34)
FanDuel: -310 (1.3226)
FoxBet: -300 (1.3333)
BetMGM: -300 (1.33)

Spread Dallas Mavericks -5.0
DraftKings: +220 (3.2)
FoxBet: +215 (3.15)

Spread Denver Nuggets 5.0
DraftKings: -278 (1.36)
FoxBet: -300 (1.3333)

Spread Dallas Mavericks -4.5
DraftKings: +200 (3.0)
FanDuel: +196 (2.96)
FoxBet: +190 (2.9)
BetMGM: +200 (3.0)

Spread Denver Nuggets 4.5
DraftKings: -250 (1.4)
FanDuel: -260 (1.3846)
FoxBet: -275 (1.3636)
BetMGM: -250 (1.4)

Spread Dallas Mavericks -4.0
DraftKings: +190 (2.9)
FoxBet: +190 (2.9)

Spread Denver Nuggets 4.0
DraftKings: -240 (1.42)
FoxBet: -275 (1.3636)

Spread Dallas Mavericks -3.5
DraftKings: +175 (2.75)
FanDuel: +168 (2.68)
FoxBet: +170 (2.7)
BetMGM: +180 (2.8)

Spread Denver Nuggets 3.5
DraftKings: -220 (1.46)
FanDuel: -220 (1.4545)
FoxBet: -225 (1.4444)
BetMGM: -225 (1.45)

Spread Dallas Mavericks -3.0
DraftKings: +165 (2.65)
FoxBet: +165 (2.65)

Spread Denver Nuggets 3.0
DraftKings: -205 (1.49)
FoxBet: -225 (1.4444)

Spread Dallas Mavericks -2.5
DraftKings: +150 (2.5)
FanDuel: +146 (2.46)
FoxBet: +155 (2.55)
BetMGM: +155 (2.55)

Spread Denver Nuggets 2.5
DraftKings: -190 (1.53)
FanDuel: -188 (1.5319)
FoxBet: -212 (1.4706)
BetMGM: -190 (1.53)

Spread Dallas Mavericks -2.0
DraftKings: +145 (2.45)
FoxBet: +145 (2.45)

Spread Denver Nuggets 2.0
DraftKings: -180 (1.56)
FoxBet: -200 (1.5)

Spread Dallas Mavericks -1.5
DraftKings: +135 (2.35)
FanDuel: +130 (2.3)
FoxBet: +135 (2.35)
BetMGM: +135 (2.35)

Spread Denver Nuggets 1.5
DraftKings: -165 (1.61)
FanDuel: -164 (1.6098)
FoxBet: -182 (1.55)
BetMGM: -165 (1.6)

Spread Dallas Mavericks -1.0
DraftKings: +130 (2.3)
FoxBet: +130 (2.3)

Spread Denver Nuggets 1.0
DraftKings: -159 (1.63)
FoxBet: -175 (1.5714)

Spread Dallas Mavericks -0.5
DraftKings: +123 (2.23)
FanDuel: +122 (2.22)

Spread Denver Nuggets 0.5
DraftKings: -150 (1.67)
FanDuel: -150 (1.6667)

Spread Dallas Mavericks 0.0
DraftKings: +123 (2.23)
FoxBet: +125 (2.25)

Spread Denver Nuggets 0.0
DraftKings: -150 (1.67)
FoxBet: -162 (1.6154)

Spread Dallas Mavericks 0.5
DraftKings: +123 (2.23)
FanDuel: +122 (2.22)

Spread Denver Nuggets -0.5
DraftKings: -150 (1.67)
FanDuel: -150 (1.6667)

Spread Dallas Mavericks 1.0
DraftKings: +117 (2.17)
FoxBet: +120 (2.2)

Spread Denver Nuggets -1.0
DraftKings: -143 (1.7)
FoxBet: -154 (1.65)

Spread Dallas Mavericks 1.5
DraftKings: +112 (2.12)
FanDuel: +112 (2.12)
FoxBet: +110 (2.1)
BetMGM: +110 (2.1)

Spread Denver Nuggets -1.5
DraftKings: -136 (1.74)
FanDuel: -138 (1.7246)
FoxBet: -143 (1.7)
BetMGM: -135 (1.75)

Spread Dallas Mavericks 2.0
DraftKings: +105 (2.05)
FoxBet: +105 (2.05)

Spread Denver Nuggets -2.0
DraftKings: -127 (1.79)
FoxBet: -137 (1.7273)

Spread Dallas Mavericks 2.5
DraftKings: -103 (1.98)
FanDuel: -104 (1.9615)
FoxBet: +100 (2.0)
BetMGM: -105 (1.95)

Spread Denver Nuggets -2.5
DraftKings: -120 (1.84)
FanDuel: -118 (1.8475)
FoxBet: -133 (1.75)
BetMGM: -115 (1.87)

Spread Dallas Mavericks 3.5
DraftKings: -117 (1.86)
FanDuel: -116 (1.8621)
FoxBet: -110 (1.9091)
BetMGM: -120 (1.83)

Spread Denver Nuggets -3.5
DraftKings: -105 (1.96)
FanDuel: -106 (1.9434)
FoxBet: -110 (1.9091)
BetMGM: +100 (2.0)

Spread Dallas Mavericks 4.0
DraftKings: -125 (1.8)
FoxBet: -125 (1.8)

Spread Denver Nuggets -4.0
DraftKings: +104 (2.04)
FoxBet: -105 (1.95)

Spread Dallas Mavericks 4.5
DraftKings: -134 (1.75)
FanDuel: -142 (1.7042)
FoxBet: -133 (1.75)
BetMGM: -140 (1.72)

Spread Denver Nuggets -4.5
DraftKings: +110 (2.1)
FanDuel: +116 (2.16)
FoxBet: +100 (2.0)
BetMGM: +115 (2.15)

Spread Dallas Mavericks 5.0
DraftKings: -148 (1.68)
FoxBet: -143 (1.7)

Spread Denver Nuggets -5.0
DraftKings: +120 (2.2)
FoxBet: +110 (2.1)

Spread Dallas Mavericks 5.5
DraftKings: -157 (1.64)
FanDuel: -170 (1.5882)
FoxBet: -154 (1.65)
BetMGM: -160 (1.62)

Spread Denver Nuggets -5.5
DraftKings: +128 (2.28)
FanDuel: +134 (2.34)
FoxBet: +120 (2.2)
BetMGM: +135 (2.35)

Spread Dallas Mavericks 6.0
DraftKings: -175 (1.58)
FoxBet: -175 (1.5714)

Spread Denver Nuggets -6.0
DraftKings: +138 (2.38)
FoxBet: +130 (2.3)

Spread Dallas Mavericks 6.5
DraftKings: -180 (1.56)
FanDuel: -194 (1.5155)
FoxBet: -182 (1.55)
BetMGM: -190 (1.53)

Spread Denver Nuggets -6.5
DraftKings: +148 (2.48)
FanDuel: +150 (2.5)
FoxBet: +135 (2.35)
BetMGM: +155 (2.55)

Spread Dallas Mavericks 7.0
DraftKings: -200 (1.5)
FoxBet: -212 (1.4706)

Spread Denver Nuggets -7.0
DraftKings: +160 (2.6)
FoxBet: +155 (2.55)

Spread Dallas Mavericks 7.5
DraftKings: -210 (1.48)
FanDuel: -225 (1.4444)
FoxBet: -225 (1.4444)
BetMGM: -225 (1.44)

Spread Denver Nuggets -7.5
DraftKings: +170 (2.7)
FanDuel: +172 (2.72)
FoxBet: +165 (2.65)
BetMGM: +185 (2.85)

Spread Dallas Mavericks 8.0
DraftKings: -230 (1.44)
FoxBet: -250 (1.4)

Spread Denver Nuggets -8.0
DraftKings: +185 (2.85)
FoxBet: +185 (2.85)

Spread Dallas Mavericks 8.5
DraftKings: -240 (1.42)
FanDuel: -260 (1.3846)
FoxBet: -275 (1.3636)
BetMGM: -275 (1.36)

Spread Denver Nuggets -8.5
DraftKings: +190 (2.9)
FanDuel: +196 (2.96)
FoxBet: +190 (2.9)
BetMGM: +220 (3.2)

Spread Dallas Mavericks 9.0
DraftKings: -265 (1.38)
FoxBet: -286 (1.35)

Spread Denver Nuggets -9.0
DraftKings: +210 (3.1)
FoxBet: +205 (3.05)

Spread Dallas Mavericks 9.5
DraftKings: -278 (1.36)
FanDuel: -300 (1.3333)
FoxBet: -300 (1.3333)
BetMGM: -350 (1.3)

Spread Denver Nuggets -9.5
DraftKings: +220 (3.2)
FanDuel: +220 (3.2)
FoxBet: +215 (3.15)
BetMGM: +260 (3.6)

Spread Dallas Mavericks 10.0
DraftKings: -305 (1.33)
FoxBet: -333 (1.3)

Spread Denver Nuggets -10.0
DraftKings: +240 (3.4)
FoxBet: +230 (3.3)

Spread Dallas Mavericks 10.5
DraftKings: -315 (1.32)
FanDuel: -370 (1.2703)
FoxBet: -350 (1.2857)
BetMGM: -375 (1.26)

Spread Denver Nuggets -10.5
DraftKings: +245 (3.45)
FanDuel: +265 (3.65)
FoxBet: +250 (3.5)
BetMGM: +300 (4.0)

Spread Dallas Mavericks 11.0
DraftKings: -360 (1.28)
FoxBet: -400 (1.25)

Spread Denver Nuggets -11.0
DraftKings: +275 (3.75)
FoxBet: +275 (3.75)

Spread Dallas Mavericks 11.5
DraftKings: -375 (1.27)
FanDuel: -410 (1.2439)
FoxBet: -450 (1.2222)
BetMGM: -450 (1.22)

Spread Denver Nuggets -11.5
DraftKings: +285 (3.85)
FanDuel: +290 (3.9)
FoxBet: +290 (3.9)
BetMGM: +340 (4.4)

Spread Dallas Mavericks 12.0
DraftKings: -420 (1.24)
FoxBet: -450 (1.2222)

Spread Denver Nuggets -12.0
DraftKings: +320 (4.2)
FoxBet: +330 (4.3)

Spread Dallas Mavericks 12.5
DraftKings: -435 (1.23)
FanDuel: -520 (1.1923)
FoxBet: -500 (1.2)
BetMGM: -550 (1.18)

Spread Denver Nuggets -12.5
DraftKings: +325 (4.25)
FanDuel: +350 (4.5)
FoxBet: +320 (4.2)
BetMGM: +400 (5.0)

Spread Dallas Mavericks 13.0
DraftKings: -480 (1.21)
FoxBet: -550 (1.1818)

Spread Denver Nuggets -13.0
DraftKings: +360 (4.6)
FoxBet: +333 (4.3333)

Spread Dallas Mavericks 13.5
DraftKings: -500 (1.2)
FanDuel: -620 (1.1613)
FoxBet: -550 (1.1818)
BetMGM: -650 (1.16)

Spread Denver Nuggets -13.5
DraftKings: +370 (4.7)
FanDuel: +400 (5.0)
FoxBet: +350 (4.5)
BetMGM: +450 (5.5)

Spread Dallas Mavericks 14.0
DraftKings: -560 (1.18)
FoxBet: -650 (1.1538)

Spread Denver Nuggets -14.0
DraftKings: +400 (5.0)
FoxBet: +380 (4.8)

Spread Dallas Mavericks 14.5
DraftKings: -590 (1.17)
FanDuel: -750 (1.1333)
FoxBet: -650 (1.1538)

Spread Denver Nuggets -14.5
DraftKings: +420 (5.2)
FanDuel: +460 (5.6)
FoxBet: +400 (5.0)

Spread Dallas Mavericks 24.5
FanDuel: -5000 (1.02)

Spread Dallas Mavericks 23.5
FanDuel: -4500 (1.0222)

Spread Dallas Mavericks 22.5
FanDuel: -3500 (1.0286)

Spread Dallas Mavericks 21.5
FanDuel: -3000 (1.0333)

Spread Dallas Mavericks 20.5
FanDuel: -3000 (1.0333)

Spread Dallas Mavericks 19.5
FanDuel: -2400 (1.0417)

Spread Dallas Mavericks 18.5
FanDuel: -1800 (1.0556)

Spread Dallas Mavericks 17.5
FanDuel: -1400 (1.0714)

Spread Dallas Mavericks 16.5
FanDuel: -1100 (1.0909)
FoxBet: -900 (1.1111)

Spread Dallas Mavericks 15.5
FanDuel: -900 (1.1111)
FoxBet: -700 (1.1429)

Spread Dallas Mavericks -11.5
FanDuel: +560 (6.6)

Spread Dallas Mavericks -12.5
FanDuel: +670 (7.7)

Spread Dallas Mavericks -13.5
FanDuel: +790 (8.9)

Spread Dallas Mavericks -14.5
FanDuel: +870 (9.7)

Spread Dallas Mavericks -15.5
FanDuel: +920 (10.2)

Spread Dallas Mavericks -16.5
FanDuel: +1000 (11.0)

Spread Dallas Mavericks -17.5
FanDuel: +1100 (12.0)

Spread Dallas Mavericks -18.5
FanDuel: +1300 (14.0)

Spread Dallas Mavericks -19.5
FanDuel: +1400 (15.0)

Spread Dallas Mavericks -20.5
FanDuel: +1800 (19.0)

Spread Dallas Mavericks -21.5
FanDuel: +2000 (21.0)

Spread Denver Nuggets -28.5
FanDuel: +3300 (34.0)

Spread Denver Nuggets -27.5
FanDuel: +2200 (23.0)

Spread Denver Nuggets -26.5
FanDuel: +2000 (21.0)

Spread Denver Nuggets -25.5
FanDuel: +1800 (19.0)

Spread Denver Nuggets -24.5
FanDuel: +1400 (15.0)

Spread Denver Nuggets -23.5
FanDuel: +1300 (14.0)

Spread Denver Nuggets -22.5
FanDuel: +1200 (13.0)

Spread Denver Nuggets -21.5
FanDuel: +1000 (11.0)

Spread Denver Nuggets -20.5
FanDuel: +980 (10.8)

Spread Denver Nuggets -19.5
FanDuel: +900 (10.0)

Spread Denver Nuggets -18.5
FanDuel: +790 (8.9)

Spread Denver Nuggets -17.5
FanDuel: +690 (7.9)

Spread Denver Nuggets -16.5
FanDuel: +580 (6.8)
FoxBet: +475 (5.75)

Spread Denver Nuggets -15.5
FanDuel: +520 (6.2)
FoxBet: +425 (5.25)

Spread Denver Nuggets 11.5
FanDuel: -1000 (1.1)

Spread Denver Nuggets 12.5
FanDuel: -1350 (1.0741)

Spread Denver Nuggets 13.5
FanDuel: -1800 (1.0556)

Spread Denver Nuggets 14.5
FanDuel: -2200 (1.0455)

Spread Denver Nuggets 15.5
FanDuel: -3000 (1.0333)

Spread Denver Nuggets 16.5
FanDuel: -3000 (1.0333)

Spread Denver Nuggets 17.5
FanDuel: -3500 (1.0286)

Spread Denver Nuggets 18.5
FanDuel: -4500 (1.0222)

Spread Denver Nuggets 19.5
FanDuel: -5000 (1.02)

Spread Dallas Mavericks -11.0
FoxBet: +475 (5.75)

Spread Denver Nuggets 11.0
FoxBet: -900 (1.1111)

Spread Denver Nuggets -16.0
FoxBet: +475 (5.75)

Spread Dallas Mavericks 16.0
FoxBet: -900 (1.1111)

Spread Denver Nuggets -15.0
FoxBet: +425 (5.25)

Spread Dallas Mavericks 15.0
FoxBet: -700 (1.1429)

Spread Dallas Mavericks 17.0
FoxBet: -1000 (1.1)

Spread Denver Nuggets -17.0
FoxBet: +550 (6.5)
Total
Total Over 224.0
DraftKings: -108 (1.93)
FoxBet: -110 (1.9091)

Total Under 224.0
DraftKings: -113 (1.89)
FoxBet: -110 (1.9091)

Total Over 204.5
DraftKings: -770 (1.13)
FanDuel: -1200 (1.0833)

Total Under 204.5
DraftKings: +500 (6.0)
FanDuel: +630 (7.3)

Total Over 205.0
DraftKings: -715 (1.14)

Total Under 205.0
DraftKings: +500 (6.0)

Total Over 205.5
DraftKings: -670 (1.15)
FanDuel: -950 (1.1053)

Total Under 205.5
DraftKings: +460 (5.6)
FanDuel: +540 (6.4)

Total Over 206.0
DraftKings: -625 (1.16)

Total Under 206.0
DraftKings: +450 (5.5)

Total Over 206.5
DraftKings: -590 (1.17)
FanDuel: -800 (1.125)

Total Under 206.5
DraftKings: +420 (5.2)
FanDuel: +480 (5.8)

Total Over 207.0
DraftKings: -590 (1.17)

Total Under 207.0
DraftKings: +410 (5.1)

Total Over 207.5
DraftKings: -530 (1.19)
FanDuel: -670 (1.1493)

Total Under 207.5
DraftKings: +380 (4.8)
FanDuel: +430 (5.3)

Total Over 208.0
DraftKings: -530 (1.19)

Total Under 208.0
DraftKings: +375 (4.75)

Total Over 208.5
DraftKings: -480 (1.21)
FanDuel: -580 (1.1724)

Total Under 208.5
DraftKings: +350 (4.5)
FanDuel: +380 (4.8)

Total Over 209.0
DraftKings: -455 (1.22)

Total Under 209.0
DraftKings: +340 (4.4)

Total Over 209.5
DraftKings: -420 (1.24)
FanDuel: -500 (1.2)

Total Under 209.5
DraftKings: +320 (4.2)
FanDuel: +340 (4.4)

Total Over 210.0
DraftKings: -420 (1.24)
FoxBet: -450 (1.2222)

Total Under 210.0
DraftKings: +310 (4.1)
FoxBet: +290 (3.9)

Total Over 210.5
DraftKings: -385 (1.26)
FanDuel: -410 (1.2439)
FoxBet: -450 (1.2222)

Total Under 210.5
DraftKings: +290 (3.9)
FanDuel: +290 (3.9)
FoxBet: +290 (3.9)

Total Over 211.0
DraftKings: -375 (1.27)
FoxBet: -400 (1.25)

Total Under 211.0
DraftKings: +285 (3.85)
FoxBet: +275 (3.75)

Total Over 211.5
DraftKings: -345 (1.29)
FanDuel: -370 (1.2703)
FoxBet: -400 (1.25)

Total Under 211.5
DraftKings: +265 (3.65)
FanDuel: +265 (3.65)
FoxBet: +275 (3.75)

Total Over 212.0
DraftKings: -335 (1.3)
FoxBet: -400 (1.25)

Total Under 212.0
DraftKings: +260 (3.6)
FoxBet: +275 (3.75)

Total Over 212.5
DraftKings: -315 (1.32)
FanDuel: -320 (1.3125)
FoxBet: -333 (1.3)

Total Under 212.5
DraftKings: +245 (3.45)
FanDuel: +235 (3.35)
FoxBet: +230 (3.3)

Total Over 213.0
DraftKings: -305 (1.33)
FoxBet: -333 (1.3)

Total Under 213.0
DraftKings: +240 (3.4)
FoxBet: +230 (3.3)

Total Over 213.5
DraftKings: -278 (1.36)
FanDuel: -290 (1.3448)
FoxBet: -300 (1.3333)

Total Under 213.5
DraftKings: +220 (3.2)
FanDuel: +215 (3.15)
FoxBet: +215 (3.15)

Total Over 214.0
DraftKings: -275 (1.37)
FoxBet: -300 (1.3333)

Total Under 214.0
DraftKings: +215 (3.15)
FoxBet: +215 (3.15)

Total Over 214.5
DraftKings: -250 (1.4)
FanDuel: -265 (1.3774)
FoxBet: -275 (1.3636)

Total Under 214.5
DraftKings: +200 (3.0)
FanDuel: +200 (3.0)
FoxBet: +200 (3.0)

Total Over 215.0
DraftKings: -250 (1.4)
FoxBet: -275 (1.3636)

Total Under 215.0
DraftKings: +200 (3.0)
FoxBet: +200 (3.0)

Total Over 215.5
DraftKings: -235 (1.43)
FanDuel: -240 (1.4167)
FoxBet: -250 (1.4)

Total Under 215.5
DraftKings: +185 (2.85)
FanDuel: +182 (2.82)
FoxBet: +185 (2.85)

Total Over 216.0
DraftKings: -225 (1.45)
FoxBet: -250 (1.4)

Total Under 216.0
DraftKings: +180 (2.8)
FoxBet: +185 (2.85)

Total Over 216.5
DraftKings: -215 (1.47)
FanDuel: -205 (1.4878)
FoxBet: -225 (1.4444)
BetMGM: -190 (1.53)

Total Under 216.5
DraftKings: +170 (2.7)
FanDuel: +162 (2.62)
FoxBet: +165 (2.65)
BetMGM: +155 (2.55)

Total Over 217.0
DraftKings: -205 (1.49)
FoxBet: -212 (1.4706)

Total Under 217.0
DraftKings: +165 (2.65)
FoxBet: +155 (2.55)

Total Over 217.5
DraftKings: -195 (1.52)
FanDuel: -192 (1.5208)
FoxBet: -212 (1.4706)
BetMGM: -175 (1.57)

Total Under 217.5
DraftKings: +155 (2.55)
FanDuel: +154 (2.54)
FoxBet: +155 (2.55)
BetMGM: +145 (2.45)

Total Over 218.0
DraftKings: -186 (1.54)
FoxBet: -200 (1.5)

Total Under 218.0
DraftKings: +150 (2.5)
FoxBet: +145 (2.45)

Total Over 218.5
DraftKings: -177 (1.57)
FanDuel: -174 (1.5747)
FoxBet: -188 (1.5333)
BetMGM: -160 (1.62)

Total Under 218.5
DraftKings: +143 (2.43)
FanDuel: +140 (2.4)
FoxBet: +140 (2.4)
BetMGM: +135 (2.35)

Total Over 219.0
DraftKings: -167 (1.6)
FoxBet: -182 (1.55)

Total Under 219.0
DraftKings: +138 (2.38)
FoxBet: +135 (2.35)

Total Over 219.5
DraftKings: -159 (1.63)
FanDuel: -158 (1.6329)
FoxBet: -175 (1.5714)
BetMGM: -150 (1.67)

Total Under 219.5
DraftKings: +132 (2.32)
FanDuel: +128 (2.28)
FoxBet: +130 (2.3)
BetMGM: +125 (2.25)

Total Over 220.0
DraftKings: -155 (1.65)
FoxBet: -162 (1.6154)

Total Under 220.0
DraftKings: +125 (2.25)
FoxBet: +125 (2.25)

Total Over 220.5
DraftKings: -148 (1.68)
FanDuel: -142 (1.7042)
FoxBet: -150 (1.6667)
BetMGM: -140 (1.72)

Total Under 220.5
DraftKings: +120 (2.2)
FanDuel: +116 (2.16)
FoxBet: +115 (2.15)
BetMGM: +115 (2.15)

Total Over 221.0
DraftKings: -141 (1.71)
FoxBet: -150 (1.6667)

Total Under 221.0
DraftKings: +116 (2.16)
FoxBet: +115 (2.15)

Total Over 221.5
DraftKings: -134 (1.75)
FanDuel: -128 (1.7812)
FoxBet: -137 (1.7273)
BetMGM: -125 (1.8)

Total Under 221.5
DraftKings: +110 (2.1)
FanDuel: +104 (2.04)
FoxBet: +105 (2.05)
BetMGM: +105 (2.05)

Total Over 222.0
DraftKings: -129 (1.78)
FoxBet: -137 (1.7273)

Total Under 222.0
DraftKings: +106 (2.06)
FoxBet: +105 (2.05)

Total Over 222.5
DraftKings: -122 (1.82)
FanDuel: -116 (1.8621)
FoxBet: -133 (1.75)
BetMGM: -115 (1.87)

Total Under 222.5
DraftKings: +100 (2.0)
FanDuel: -106 (1.9434)
FoxBet: +100 (2.0)
BetMGM: -105 (1.95)

Total Over 223.0
DraftKings: -118 (1.85)
FanDuel: -110 (1.9091)
FoxBet: -125 (1.8)

Total Under 223.0
DraftKings: -104 (1.97)
FanDuel: -110 (1.9091)
FoxBet: -105 (1.95)

Total Over 223.5
DraftKings: -113 (1.89)
FanDuel: -106 (1.9434)
FoxBet: -125 (1.8)
BetMGM: -110 (1.91)

Total Under 223.5
DraftKings: -108 (1.93)
FanDuel: -116 (1.8621)
FoxBet: -105 (1.95)
BetMGM: -110 (1.91)

Total Over 224.5
DraftKings: -104 (1.97)
FanDuel: +104 (2.04)
FoxBet: -105 (1.95)
BetMGM: +100 (2.0)

Total Under 224.5
DraftKings: -118 (1.85)
FanDuel: -128 (1.7812)
FoxBet: -125 (1.8)
BetMGM: -120 (1.83)

Total Over 225.0
DraftKings: +102 (2.02)
FoxBet: +100 (2.0)

Total Under 225.0
DraftKings: -124 (1.81)
FoxBet: -133 (1.75)

Total Over 225.5
DraftKings: +106 (2.06)
FanDuel: +114 (2.14)
FoxBet: +105 (2.05)
BetMGM: +110 (2.1)

Total Under 225.5
DraftKings: -129 (1.78)
FanDuel: -140 (1.7143)
FoxBet: -137 (1.7273)
BetMGM: -135 (1.75)

Total Over 226.0
DraftKings: +112 (2.12)
FoxBet: +105 (2.05)

Total Under 226.0
DraftKings: -136 (1.74)
FoxBet: -137 (1.7273)

Total Over 226.5
DraftKings: +116 (2.16)
FanDuel: +126 (2.26)
FoxBet: +110 (2.1)
BetMGM: +115 (2.15)

Total Under 226.5
DraftKings: -141 (1.71)
FanDuel: -156 (1.641)
FoxBet: -143 (1.7)
BetMGM: -140 (1.72)

Total Over 227.0
DraftKings: +120 (2.2)
FoxBet: +115 (2.15)

Total Under 227.0
DraftKings: -148 (1.68)
FoxBet: -150 (1.6667)

Total Over 227.5
DraftKings: +125 (2.25)
FanDuel: +136 (2.36)
FoxBet: +120 (2.2)
BetMGM: +125 (2.25)

Total Under 227.5
DraftKings: -155 (1.65)
FanDuel: -170 (1.5882)
FoxBet: -154 (1.65)
BetMGM: -155 (1.65)

Total Over 228.0
DraftKings: +133 (2.33)
FoxBet: +130 (2.3)

Total Under 228.0
DraftKings: -162 (1.62)
FoxBet: -175 (1.5714)

Total Over 228.5
DraftKings: +138 (2.38)
FanDuel: +148 (2.48)
FoxBet: +130 (2.3)
BetMGM: +135 (2.35)

Total Under 228.5
DraftKings: -167 (1.6)
FanDuel: -186 (1.5376)
FoxBet: -175 (1.5714)
BetMGM: -165 (1.6)

Total Over 229.0
DraftKings: +145 (2.45)
FoxBet: +140 (2.4)

Total Under 229.0
DraftKings: -180 (1.56)
FoxBet: -188 (1.5333)

Total Over 229.5
DraftKings: +150 (2.5)
FanDuel: +160 (2.6)
FoxBet: +145 (2.45)
BetMGM: +150 (2.5)

Total Under 229.5
DraftKings: -182 (1.55)
FanDuel: -200 (1.5)
FoxBet: -200 (1.5)
BetMGM: -185 (1.55)

Total Over 230.0
DraftKings: +155 (2.55)
FoxBet: +155 (2.55)

Total Under 230.0
DraftKings: -195 (1.52)
FoxBet: -212 (1.4706)

Total Over 230.5
DraftKings: +163 (2.63)
FanDuel: +176 (2.76)
FoxBet: +155 (2.55)
BetMGM: +155 (2.55)

Total Under 230.5
DraftKings: -200 (1.5)
FanDuel: -230 (1.4348)
FoxBet: -212 (1.4706)
BetMGM: -190 (1.53)

Total Over 231.0
DraftKings: +170 (2.7)
FoxBet: +165 (2.65)

Total Under 231.0
DraftKings: -215 (1.47)
FoxBet: -225 (1.4444)

Total Over 231.5
DraftKings: +175 (2.75)
FanDuel: +190 (2.9)
FoxBet: +170 (2.7)
BetMGM: +170 (2.7)

Total Under 231.5
DraftKings: -220 (1.46)
FanDuel: -250 (1.4)
FoxBet: -225 (1.4444)
BetMGM: -200 (1.48)

Total Over 232.0
DraftKings: +188 (2.88)
FoxBet: +185 (2.85)

Total Under 232.0
DraftKings: -235 (1.43)
FoxBet: -250 (1.4)

Total Over 232.5
DraftKings: +190 (2.9)
FanDuel: +205 (3.05)
FoxBet: +190 (2.9)
BetMGM: +185 (2.85)

Total Under 232.5
DraftKings: -240 (1.42)
FanDuel: -275 (1.3636)
FoxBet: -275 (1.3636)
BetMGM: -225 (1.44)

Total Over 233.0
DraftKings: +200 (3.0)
FoxBet: +200 (3.0)

Total Under 233.0
DraftKings: -250 (1.4)
FoxBet: -275 (1.3636)

Total Over 233.5
DraftKings: +210 (3.1)
FanDuel: +225 (3.25)
FoxBet: +200 (3.0)
BetMGM: +200 (3.0)

Total Under 233.5
DraftKings: -265 (1.38)
FanDuel: -300 (1.3333)
FoxBet: -275 (1.3636)
BetMGM: -250 (1.4)

Total Over 234.0
DraftKings: +220 (3.2)
FoxBet: +215 (3.15)

Total Under 234.0
DraftKings: -278 (1.36)
FoxBet: -300 (1.3333)

Total Over 234.5
DraftKings: +225 (3.25)
FanDuel: +260 (3.6)
FoxBet: +215 (3.15)
BetMGM: +220 (3.2)

Total Under 234.5
DraftKings: -286 (1.35)
FanDuel: -350 (1.2857)
FoxBet: -300 (1.3333)
BetMGM: -275 (1.36)

Total Over 235.0
DraftKings: +245 (3.45)
FoxBet: +230 (3.3)

Total Under 235.0
DraftKings: -315 (1.32)
FoxBet: -333 (1.3)

Total Over 235.5
DraftKings: +245 (3.45)
FanDuel: +280 (3.8)
FoxBet: +230 (3.3)
BetMGM: +230 (3.3)

Total Under 235.5
DraftKings: -315 (1.32)
FanDuel: -390 (1.2564)
FoxBet: -333 (1.3)
BetMGM: -300 (1.34)

Total Over 236.0
DraftKings: +265 (3.65)
FoxBet: +275 (3.75)

Total Under 236.0
DraftKings: -345 (1.29)
FoxBet: -400 (1.25)

Total Over 236.5
DraftKings: +270 (3.7)
FanDuel: +300 (4.0)
FoxBet: +275 (3.75)

Total Under 236.5
DraftKings: -345 (1.29)
FanDuel: -430 (1.2326)
FoxBet: -400 (1.25)

Total Over 237.0
DraftKings: +290 (3.9)
FoxBet: +275 (3.75)

Total Under 237.0
DraftKings: -375 (1.27)
FoxBet: -400 (1.25)

Total Over 237.5
DraftKings: +295 (3.95)
FanDuel: +350 (4.5)

Total Under 237.5
DraftKings: -385 (1.26)
FanDuel: -520 (1.1923)

Total Over 238.0
DraftKings: +310 (4.1)

Total Under 238.0
DraftKings: -420 (1.24)

Total Over 238.5
DraftKings: +320 (4.2)
FanDuel: +380 (4.8)

Total Under 238.5
DraftKings: -420 (1.24)
FanDuel: -580 (1.1724)

Total Over 239.0
DraftKings: +340 (4.4)

Total Under 239.0
DraftKings: -455 (1.22)

Total Over 239.5
DraftKings: +350 (4.5)
FanDuel: +430 (5.3)

Total Under 239.5
DraftKings: -480 (1.21)
FanDuel: -670 (1.1493)

Total Over 240.0
DraftKings: +370 (4.7)

Total Under 240.0
DraftKings: -500 (1.2)

Total Over 240.5
DraftKings: +375 (4.75)
FanDuel: +480 (5.8)

Total Under 240.5
DraftKings: -530 (1.19)
FanDuel: -800 (1.125)

Total Over 241.0
DraftKings: +400 (5.0)

Total Under 241.0
DraftKings: -560 (1.18)

Total Over 241.5
DraftKings: +400 (5.0)
FanDuel: +540 (6.4)

Total Under 241.5
DraftKings: -560 (1.18)
FanDuel: -950 (1.1053)

Total Over 200.5
FanDuel: -2400 (1.0417)

Total Over 201.5
FanDuel: -2200 (1.0455)

Total Over 202.5
FanDuel: -1800 (1.0556)

Total Over 203.5
FanDuel: -1450 (1.069)

Total Over 242.5
FanDuel: +600 (7.0)

Total Over 243.5
FanDuel: +650 (7.5)

Total Over 244.5
FanDuel: +750 (8.5)

Total Over 245.5
FanDuel: +820 (9.2)

Total Over 246.5
FanDuel: +900 (10.0)

Total Over 247.5
FanDuel: +940 (10.4)

Total Over 248.5
FanDuel: +1100 (12.0)

Total Over 249.5
FanDuel: +1200 (13.0)

Total Over 250.5
FanDuel: +1300 (14.0)

Total Under 200.5
FanDuel: +1000 (11.0)

Total Under 201.5
FanDuel: +900 (10.0)

Total Under 202.5
FanDuel: +800 (9.0)

Total Under 203.5
FanDuel: +700 (8.0)

Total Under 242.5
FanDuel: -1100 (1.0909)

Total Under 243.5
FanDuel: -1300 (1.0769)

Total Under 244.5
FanDuel: -1600 (1.0625)

Total Under 245.5
FanDuel: -1900 (1.0526)

Total Under 246.5
FanDuel: -2200 (1.0455)

Total Under 247.5
FanDuel: -2300 (1.0435)

Total Under 248.5
FanDuel: -3000 (1.0333)

Total Under 249.5
FanDuel: -4000 (1.025)

Total Under 250.5
FanDuel: -4500 (1.0222)
Moneyline
Moneyline Dallas Mavericks None
DraftKings: +123 (2.23)
FanDuel: +124 (2.24)
FoxBet: +130 (2.3)
BetMGM: +125 (2.25)

Moneyline Denver Nuggets None
DraftKings: -148 (1.68)
FanDuel: -146 (1.6849)
FoxBet: -162 (1.6154)
BetMGM: -150 (1.67)


, 'Minnesota Timberwolves @ Portland Trail Blazers': NBA: Minnesota Timberwolves @ Portland Trail Blazers
DraftKings - 179653578
FanDuel - 949397.3
FoxBet - 8715838
BetMGM - 10987584
Spread
Spread Minnesota Timberwolves 9.5
DraftKings: -110 (1.91)
FanDuel: +100 (2.0)
FoxBet: -110 (1.9091)
BetMGM: -105 (1.95)

Spread Portland Trail Blazers -9.5
DraftKings: -110 (1.91)
FanDuel: -122 (1.8197)
FoxBet: -110 (1.9091)
BetMGM: -115 (1.87)

Spread Minnesota Timberwolves -2.5
DraftKings: +480 (5.8)
FanDuel: +420 (5.2)
FoxBet: +475 (5.75)

Spread Portland Trail Blazers 2.5
DraftKings: -715 (1.14)
FanDuel: -650 (1.1538)
FoxBet: -800 (1.125)

Spread Minnesota Timberwolves -2.0
DraftKings: +480 (5.8)
FoxBet: +500 (6.0)

Spread Portland Trail Blazers 2.0
DraftKings: -715 (1.14)
FoxBet: -800 (1.125)

Spread Minnesota Timberwolves -1.5
DraftKings: +425 (5.25)
FanDuel: +360 (4.6)
FoxBet: +425 (5.25)

Spread Portland Trail Blazers 1.5
DraftKings: -590 (1.17)
FanDuel: -550 (1.1818)
FoxBet: -700 (1.1429)

Spread Minnesota Timberwolves -1.0
DraftKings: +410 (5.1)
FoxBet: +425 (5.25)

Spread Portland Trail Blazers 1.0
DraftKings: -590 (1.17)
FoxBet: -700 (1.1429)

Spread Minnesota Timberwolves -0.5
DraftKings: +380 (4.8)
FanDuel: +320 (4.2)

Spread Portland Trail Blazers 0.5
DraftKings: -530 (1.19)
FanDuel: -460 (1.2174)

Spread Minnesota Timberwolves 0.0
DraftKings: +380 (4.8)
FoxBet: +380 (4.8)

Spread Portland Trail Blazers 0.0
DraftKings: -530 (1.19)
FoxBet: -650 (1.1538)

Spread Minnesota Timberwolves 0.5
DraftKings: +380 (4.8)
FanDuel: +320 (4.2)

Spread Portland Trail Blazers -0.5
DraftKings: -530 (1.19)
FanDuel: -460 (1.2174)

Spread Minnesota Timberwolves 1.0
DraftKings: +370 (4.7)
FoxBet: +380 (4.8)

Spread Portland Trail Blazers -1.0
DraftKings: -500 (1.2)
FoxBet: -650 (1.1538)

Spread Minnesota Timberwolves 1.5
DraftKings: +335 (4.35)
FanDuel: +280 (3.8)
FoxBet: +333 (4.3333)
BetMGM: +333 (4.33)

Spread Portland Trail Blazers -1.5
DraftKings: -455 (1.22)
FanDuel: -390 (1.2564)
FoxBet: -550 (1.1818)
BetMGM: -450 (1.22)

Spread Minnesota Timberwolves 2.0
DraftKings: +325 (4.25)
FoxBet: +333 (4.3333)

Spread Portland Trail Blazers -2.0
DraftKings: -435 (1.23)
FoxBet: -550 (1.1818)

Spread Minnesota Timberwolves 2.5
DraftKings: +285 (3.85)
FanDuel: +245 (3.45)
FoxBet: +290 (3.9)
BetMGM: +280 (3.8)

Spread Portland Trail Blazers -2.5
DraftKings: -375 (1.27)
FanDuel: -335 (1.2985)
FoxBet: -450 (1.2222)
BetMGM: -350 (1.28)

Spread Minnesota Timberwolves 3.0
DraftKings: +275 (3.75)
FoxBet: +290 (3.9)

Spread Portland Trail Blazers -3.0
DraftKings: -360 (1.28)
FoxBet: -450 (1.2222)

Spread Minnesota Timberwolves 3.5
DraftKings: +245 (3.45)
FanDuel: +230 (3.3)
FoxBet: +275 (3.75)
BetMGM: +240 (3.4)

Spread Portland Trail Blazers -3.5
DraftKings: -315 (1.32)
FanDuel: -310 (1.3226)
FoxBet: -400 (1.25)
BetMGM: -300 (1.33)

Spread Minnesota Timberwolves 4.0
DraftKings: +230 (3.3)
FoxBet: +250 (3.5)

Spread Portland Trail Blazers -4.0
DraftKings: -295 (1.34)
FoxBet: -350 (1.2857)

Spread Minnesota Timberwolves 4.5
DraftKings: +205 (3.05)
FanDuel: +188 (2.88)
FoxBet: +230 (3.3)
BetMGM: +220 (3.2)

Spread Portland Trail Blazers -4.5
DraftKings: -265 (1.38)
FanDuel: -250 (1.4)
FoxBet: -300 (1.3333)
BetMGM: -275 (1.36)

Spread Minnesota Timberwolves 5.0
DraftKings: +195 (2.95)
FoxBet: +205 (3.05)

Spread Portland Trail Blazers -5.0
DraftKings: -245 (1.41)
FoxBet: -286 (1.35)

Spread Minnesota Timberwolves 5.5
DraftKings: +175 (2.75)
FanDuel: +164 (2.64)
FoxBet: +190 (2.9)
BetMGM: +180 (2.8)

Spread Portland Trail Blazers -5.5
DraftKings: -215 (1.47)
FanDuel: -215 (1.4651)
FoxBet: -275 (1.3636)
BetMGM: -225 (1.45)

Spread Minnesota Timberwolves 6.0
DraftKings: +163 (2.63)
FoxBet: +170 (2.7)

Spread Portland Trail Blazers -6.0
DraftKings: -200 (1.5)
FoxBet: -225 (1.4444)

Spread Minnesota Timberwolves 6.5
DraftKings: +145 (2.45)
FanDuel: +142 (2.42)
FoxBet: +155 (2.55)
BetMGM: +155 (2.55)

Spread Portland Trail Blazers -6.5
DraftKings: -180 (1.56)
FanDuel: -182 (1.5495)
FoxBet: -212 (1.4706)
BetMGM: -190 (1.53)

Spread Minnesota Timberwolves 7.0
DraftKings: +138 (2.38)
FoxBet: +145 (2.45)

Spread Portland Trail Blazers -7.0
DraftKings: -167 (1.6)
FoxBet: -200 (1.5)

Spread Minnesota Timberwolves 7.5
DraftKings: +125 (2.25)
FanDuel: +128 (2.28)
FoxBet: +130 (2.3)
BetMGM: +125 (2.25)

Spread Portland Trail Blazers -7.5
DraftKings: -152 (1.66)
FanDuel: -160 (1.625)
FoxBet: -175 (1.5714)
BetMGM: -155 (1.65)

Spread Minnesota Timberwolves 8.0
DraftKings: +116 (2.16)
FoxBet: +120 (2.2)

Spread Portland Trail Blazers -8.0
DraftKings: -141 (1.71)
FoxBet: -154 (1.65)

Spread Minnesota Timberwolves 8.5
DraftKings: +106 (2.06)
FanDuel: +114 (2.14)
FoxBet: +105 (2.05)
BetMGM: +110 (2.1)

Spread Portland Trail Blazers -8.5
DraftKings: -129 (1.78)
FanDuel: -140 (1.7143)
FoxBet: -137 (1.7273)
BetMGM: -135 (1.75)

Spread Minnesota Timberwolves 9.0
DraftKings: -103 (1.98)
FoxBet: +100 (2.0)

Spread Portland Trail Blazers -9.0
DraftKings: -120 (1.84)
FoxBet: -133 (1.75)

Spread Minnesota Timberwolves 10.0
DraftKings: -120 (1.84)
FanDuel: -114 (1.8772)
FoxBet: -125 (1.8)

Spread Portland Trail Blazers -10.0
DraftKings: -103 (1.98)
FanDuel: -105 (1.9524)
FoxBet: -105 (1.95)

Spread Minnesota Timberwolves 10.5
DraftKings: -129 (1.78)
FanDuel: -122 (1.8197)
FoxBet: -133 (1.75)
BetMGM: -125 (1.8)

Spread Portland Trail Blazers -10.5
DraftKings: +106 (2.06)
FanDuel: +100 (2.0)
FoxBet: +100 (2.0)
BetMGM: +105 (2.05)

Spread Minnesota Timberwolves 11.0
DraftKings: -141 (1.71)
FoxBet: -137 (1.7273)

Spread Portland Trail Blazers -11.0
DraftKings: +115 (2.15)
FoxBet: +105 (2.05)

Spread Minnesota Timberwolves 11.5
DraftKings: -150 (1.67)
FanDuel: -146 (1.6849)
FoxBet: -143 (1.7)
BetMGM: -145 (1.7)

Spread Portland Trail Blazers -11.5
DraftKings: +123 (2.23)
FanDuel: +118 (2.18)
FoxBet: +110 (2.1)
BetMGM: +120 (2.2)

Spread Minnesota Timberwolves 12.0
DraftKings: -165 (1.61)
FoxBet: -154 (1.65)

Spread Portland Trail Blazers -12.0
DraftKings: +135 (2.35)
FoxBet: +120 (2.2)

Spread Minnesota Timberwolves 12.5
DraftKings: -177 (1.57)
FanDuel: -164 (1.6098)
FoxBet: -162 (1.6154)
BetMGM: -160 (1.62)

Spread Portland Trail Blazers -12.5
DraftKings: +143 (2.43)
FanDuel: +130 (2.3)
FoxBet: +125 (2.25)
BetMGM: +135 (2.35)

Spread Minnesota Timberwolves 13.0
DraftKings: -195 (1.52)
FoxBet: -175 (1.5714)

Spread Portland Trail Blazers -13.0
DraftKings: +155 (2.55)
FoxBet: +130 (2.3)

Spread Minnesota Timberwolves 13.5
DraftKings: -205 (1.49)
FanDuel: -186 (1.5376)
FoxBet: -188 (1.5333)
BetMGM: -190 (1.53)

Spread Portland Trail Blazers -13.5
DraftKings: +165 (2.65)
FanDuel: +144 (2.44)
FoxBet: +140 (2.4)
BetMGM: +155 (2.55)

Spread Minnesota Timberwolves 14.0
DraftKings: -230 (1.44)
FoxBet: -200 (1.5)

Spread Portland Trail Blazers -14.0
DraftKings: +185 (2.85)
FoxBet: +145 (2.45)

Spread Minnesota Timberwolves 14.5
DraftKings: -245 (1.41)
FanDuel: -215 (1.4651)
FoxBet: -212 (1.4706)
BetMGM: -200 (1.48)

Spread Portland Trail Blazers -14.5
DraftKings: +195 (2.95)
FanDuel: +164 (2.64)
FoxBet: +155 (2.55)
BetMGM: +170 (2.7)

Spread Minnesota Timberwolves 15.0
DraftKings: -275 (1.37)
FoxBet: -225 (1.4444)

Spread Portland Trail Blazers -15.0
DraftKings: +215 (3.15)
FoxBet: +165 (2.65)

Spread Minnesota Timberwolves 15.5
DraftKings: -286 (1.35)
FanDuel: -245 (1.4082)
FoxBet: -225 (1.4444)
BetMGM: -250 (1.42)

Spread Portland Trail Blazers -15.5
DraftKings: +225 (3.25)
FanDuel: +186 (2.86)
FoxBet: +170 (2.7)
BetMGM: +195 (2.95)

Spread Minnesota Timberwolves 16.0
DraftKings: -315 (1.32)
FoxBet: -275 (1.3636)

Spread Portland Trail Blazers -16.0
DraftKings: +245 (3.45)
FoxBet: +190 (2.9)

Spread Minnesota Timberwolves 16.5
DraftKings: -335 (1.3)
FanDuel: -275 (1.3636)
FoxBet: -275 (1.3636)
BetMGM: -275 (1.36)

Spread Portland Trail Blazers -16.5
DraftKings: +260 (3.6)
FanDuel: +205 (3.05)
FoxBet: +200 (3.0)
BetMGM: +220 (3.2)

Spread Minnesota Timberwolves 17.0
DraftKings: -385 (1.26)
FoxBet: -300 (1.3333)

Spread Portland Trail Blazers -17.0
DraftKings: +295 (3.95)
FoxBet: +215 (3.15)

Spread Minnesota Timberwolves 17.5
DraftKings: -400 (1.25)
FanDuel: -310 (1.3226)
FoxBet: -300 (1.3333)
BetMGM: -350 (1.3)

Spread Portland Trail Blazers -17.5
DraftKings: +300 (4.0)
FanDuel: +230 (3.3)
FoxBet: +230 (3.3)
BetMGM: +260 (3.6)

Spread Minnesota Timberwolves 18.0
DraftKings: -455 (1.22)
FoxBet: -350 (1.2857)

Spread Portland Trail Blazers -18.0
DraftKings: +340 (4.4)
FoxBet: +250 (3.5)

Spread Minnesota Timberwolves 18.5
DraftKings: -480 (1.21)
FanDuel: -350 (1.2857)
FoxBet: -400 (1.25)
BetMGM: -350 (1.28)

Spread Portland Trail Blazers -18.5
DraftKings: +350 (4.5)
FanDuel: +255 (3.55)
FoxBet: +275 (3.75)
BetMGM: +280 (3.8)

Spread Minnesota Timberwolves 19.0
DraftKings: -560 (1.18)
FoxBet: -400 (1.25)

Spread Portland Trail Blazers -19.0
DraftKings: +400 (5.0)
FoxBet: +275 (3.75)

Spread Minnesota Timberwolves 19.5
DraftKings: -560 (1.18)
FanDuel: -385 (1.2597)
FoxBet: -450 (1.2222)
BetMGM: -400 (1.25)

Spread Portland Trail Blazers -19.5
DraftKings: +400 (5.0)
FanDuel: +275 (3.75)
FoxBet: +290 (3.9)
BetMGM: +310 (4.1)

Spread Minnesota Timberwolves 33.5
FanDuel: -5000 (1.02)

Spread Minnesota Timberwolves 32.5
FanDuel: -4500 (1.0222)

Spread Minnesota Timberwolves 31.5
FanDuel: -3500 (1.0286)

Spread Minnesota Timberwolves 30.5
FanDuel: -3000 (1.0333)

Spread Minnesota Timberwolves 29.5
FanDuel: -3000 (1.0333)

Spread Minnesota Timberwolves 28.5
FanDuel: -2500 (1.04)

Spread Minnesota Timberwolves 27.5
FanDuel: -2000 (1.05)

Spread Minnesota Timberwolves 26.5
FanDuel: -1450 (1.069)

Spread Minnesota Timberwolves 25.5
FanDuel: -1100 (1.0909)

Spread Minnesota Timberwolves 24.5
FanDuel: -950 (1.1053)

Spread Minnesota Timberwolves 23.5
FanDuel: -750 (1.1333)

Spread Minnesota Timberwolves 22.5
FanDuel: -620 (1.1613)
FoxBet: -700 (1.1429)

Spread Minnesota Timberwolves 21.5
FanDuel: -550 (1.1818)
FoxBet: -550 (1.1818)

Spread Minnesota Timberwolves 20.5
FanDuel: -450 (1.2222)
FoxBet: -500 (1.2)
BetMGM: -500 (1.2)

Spread Minnesota Timberwolves -3.5
FanDuel: +460 (5.6)
FoxBet: +500 (6.0)

Spread Minnesota Timberwolves -4.5
FanDuel: +560 (6.6)
FoxBet: +600 (7.0)

Spread Minnesota Timberwolves -5.5
FanDuel: +690 (7.9)

Spread Minnesota Timberwolves -6.5
FanDuel: +790 (8.9)

Spread Minnesota Timberwolves -7.5
FanDuel: +900 (10.0)

Spread Minnesota Timberwolves -8.5
FanDuel: +1000 (11.0)

Spread Minnesota Timberwolves -9.5
FanDuel: +1100 (12.0)

Spread Minnesota Timberwolves -10.5
FanDuel: +1300 (14.0)

Spread Minnesota Timberwolves -11.5
FanDuel: +1400 (15.0)

Spread Minnesota Timberwolves -12.5
FanDuel: +1800 (19.0)

Spread Minnesota Timberwolves -13.5
FanDuel: +2000 (21.0)

Spread Minnesota Timberwolves -14.5
FanDuel: +2200 (23.0)

Spread Portland Trail Blazers -34.5
FanDuel: +1800 (19.0)

Spread Portland Trail Blazers -33.5
FanDuel: +1400 (15.0)

Spread Portland Trail Blazers -32.5
FanDuel: +1300 (14.0)

Spread Portland Trail Blazers -31.5
FanDuel: +1200 (13.0)

Spread Portland Trail Blazers -30.5
FanDuel: +1000 (11.0)

Spread Portland Trail Blazers -29.5
FanDuel: +1000 (11.0)

Spread Portland Trail Blazers -28.5
FanDuel: +900 (10.0)

Spread Portland Trail Blazers -27.5
FanDuel: +830 (9.3)

Spread Portland Trail Blazers -26.5
FanDuel: +700 (8.0)

Spread Portland Trail Blazers -25.5
FanDuel: +600 (7.0)

Spread Portland Trail Blazers -24.5
FanDuel: +540 (6.4)

Spread Portland Trail Blazers -23.5
FanDuel: +460 (5.6)

Spread Portland Trail Blazers -22.5
FanDuel: +400 (5.0)
FoxBet: +425 (5.25)

Spread Portland Trail Blazers -21.5
FanDuel: +360 (4.6)
FoxBet: +350 (4.5)

Spread Portland Trail Blazers -20.5
FanDuel: +310 (4.1)
FoxBet: +320 (4.2)
BetMGM: +360 (4.6)

Spread Portland Trail Blazers 3.5
FanDuel: -750 (1.1333)
FoxBet: -1000 (1.1)

Spread Portland Trail Blazers 4.5
FanDuel: -1000 (1.1)
FoxBet: -1100 (1.0909)

Spread Portland Trail Blazers 5.5
FanDuel: -1400 (1.0714)

Spread Portland Trail Blazers 6.5
FanDuel: -1800 (1.0556)

Spread Portland Trail Blazers 7.5
FanDuel: -2400 (1.0417)

Spread Portland Trail Blazers 8.5
FanDuel: -3000 (1.0333)

Spread Portland Trail Blazers 9.5
FanDuel: -3500 (1.0286)

Spread Portland Trail Blazers 10.5
FanDuel: -4500 (1.0222)

Spread Portland Trail Blazers 11.5
FanDuel: -5000 (1.02)

Spread Portland Trail Blazers -21.0
FoxBet: +350 (4.5)

Spread Minnesota Timberwolves 21.0
FoxBet: -550 (1.1818)

Spread Portland Trail Blazers -20.0
FoxBet: +320 (4.2)

Spread Minnesota Timberwolves 20.0
FoxBet: -500 (1.2)

Spread Minnesota Timberwolves -5.0
FoxBet: +650 (7.5)

Spread Portland Trail Blazers 5.0
FoxBet: -1401 (1.0714)

Spread Portland Trail Blazers 4.0
FoxBet: -1100 (1.0909)

Spread Minnesota Timberwolves -4.0
FoxBet: +550 (6.5)

Spread Portland Trail Blazers 3.0
FoxBet: -1000 (1.1)

Spread Minnesota Timberwolves -3.0
FoxBet: +500 (6.0)

Spread Portland Trail Blazers -23.0
FoxBet: +475 (5.75)

Spread Minnesota Timberwolves 23.0
FoxBet: -800 (1.125)

Spread Portland Trail Blazers -22.0
FoxBet: +400 (5.0)

Spread Minnesota Timberwolves 22.0
FoxBet: -650 (1.1538)
Total
Total Over 233.0
DraftKings: -109 (1.92)
FanDuel: -110 (1.9091)
FoxBet: -110 (1.9091)

Total Under 233.0
DraftKings: -113 (1.89)
FanDuel: -110 (1.9091)
FoxBet: -110 (1.9091)

Total Over 214.0
DraftKings: -770 (1.13)

Total Under 214.0
DraftKings: +510 (6.1)

Total Over 214.5
DraftKings: -670 (1.15)
FanDuel: -1200 (1.0833)

Total Under 214.5
DraftKings: +460 (5.6)
FanDuel: +630 (7.3)

Total Over 215.0
DraftKings: -670 (1.15)

Total Under 215.0
DraftKings: +460 (5.6)

Total Over 215.5
DraftKings: -590 (1.17)
FanDuel: -950 (1.1053)

Total Under 215.5
DraftKings: +425 (5.25)
FanDuel: +540 (6.4)

Total Over 216.0
DraftKings: -590 (1.17)

Total Under 216.0
DraftKings: +420 (5.2)

Total Over 216.5
DraftKings: -530 (1.19)
FanDuel: -750 (1.1333)

Total Under 216.5
DraftKings: +390 (4.9)
FanDuel: +460 (5.6)

Total Over 217.0
DraftKings: -530 (1.19)

Total Under 217.0
DraftKings: +380 (4.8)

Total Over 217.5
DraftKings: -480 (1.21)
FanDuel: -650 (1.1538)

Total Under 217.5
DraftKings: +360 (4.6)
FanDuel: +420 (5.2)

Total Over 218.0
DraftKings: -480 (1.21)

Total Under 218.0
DraftKings: +350 (4.5)

Total Over 218.5
DraftKings: -435 (1.23)
FanDuel: -580 (1.1724)

Total Under 218.5
DraftKings: +325 (4.25)
FanDuel: +380 (4.8)

Total Over 219.0
DraftKings: -420 (1.24)

Total Under 219.0
DraftKings: +320 (4.2)

Total Over 219.5
DraftKings: -385 (1.26)
FanDuel: -500 (1.2)

Total Under 219.5
DraftKings: +295 (3.95)
FanDuel: +340 (4.4)

Total Over 220.0
DraftKings: -385 (1.26)
FoxBet: -450 (1.2222)

Total Under 220.0
DraftKings: +290 (3.9)
FoxBet: +290 (3.9)

Total Over 220.5
DraftKings: -345 (1.29)
FanDuel: -410 (1.2439)
FoxBet: -400 (1.25)

Total Under 220.5
DraftKings: +270 (3.7)
FanDuel: +290 (3.9)
FoxBet: +275 (3.75)

Total Over 221.0
DraftKings: -345 (1.29)
FoxBet: -400 (1.25)

Total Under 221.0
DraftKings: +265 (3.65)
FoxBet: +275 (3.75)

Total Over 221.5
DraftKings: -315 (1.32)
FanDuel: -370 (1.2703)
FoxBet: -350 (1.2857)

Total Under 221.5
DraftKings: +240 (3.4)
FanDuel: +265 (3.65)
FoxBet: +250 (3.5)

Total Over 222.0
DraftKings: -305 (1.33)
FoxBet: -333 (1.3)

Total Under 222.0
DraftKings: +240 (3.4)
FoxBet: +230 (3.3)

Total Over 222.5
DraftKings: -286 (1.35)
FanDuel: -320 (1.3125)
FoxBet: -300 (1.3333)

Total Under 222.5
DraftKings: +225 (3.25)
FanDuel: +235 (3.35)
FoxBet: +230 (3.3)

Total Over 223.0
DraftKings: -278 (1.36)
FoxBet: -300 (1.3333)

Total Under 223.0
DraftKings: +220 (3.2)
FoxBet: +215 (3.15)

Total Over 223.5
DraftKings: -265 (1.38)
FanDuel: -290 (1.3448)
FoxBet: -286 (1.35)
BetMGM: -250 (1.42)

Total Under 223.5
DraftKings: +205 (3.05)
FanDuel: +215 (3.15)
FoxBet: +205 (3.05)
BetMGM: +195 (2.95)

Total Over 224.0
DraftKings: -250 (1.4)
FoxBet: -275 (1.3636)

Total Under 224.0
DraftKings: +200 (3.0)
FoxBet: +200 (3.0)

Total Over 224.5
DraftKings: -240 (1.42)
FanDuel: -265 (1.3774)
FoxBet: -275 (1.3636)
BetMGM: -225 (1.45)

Total Under 224.5
DraftKings: +190 (2.9)
FanDuel: +200 (3.0)
FoxBet: +190 (2.9)
BetMGM: +180 (2.8)

Total Over 225.0
DraftKings: -230 (1.44)
FoxBet: -250 (1.4)

Total Under 225.0
DraftKings: +185 (2.85)
FoxBet: +185 (2.85)

Total Over 225.5
DraftKings: -215 (1.47)
FanDuel: -240 (1.4167)
FoxBet: -225 (1.4444)
BetMGM: -200 (1.48)

Total Under 225.5
DraftKings: +175 (2.75)
FanDuel: +182 (2.82)
FoxBet: +170 (2.7)
BetMGM: +170 (2.7)

Total Over 226.0
DraftKings: -210 (1.48)
FoxBet: -225 (1.4444)

Total Under 226.0
DraftKings: +170 (2.7)
FoxBet: +165 (2.65)

Total Over 226.5
DraftKings: -195 (1.52)
FanDuel: -205 (1.4878)
FoxBet: -212 (1.4706)
BetMGM: -190 (1.53)

Total Under 226.5
DraftKings: +155 (2.55)
FanDuel: +162 (2.62)
FoxBet: +155 (2.55)
BetMGM: +155 (2.55)

Total Over 227.0
DraftKings: -190 (1.53)
FoxBet: -212 (1.4706)

Total Under 227.0
DraftKings: +155 (2.55)
FoxBet: +155 (2.55)

Total Over 227.5
DraftKings: -180 (1.56)
FanDuel: -190 (1.5263)
FoxBet: -200 (1.5)
BetMGM: -175 (1.57)

Total Under 227.5
DraftKings: +145 (2.45)
FanDuel: +152 (2.52)
FoxBet: +145 (2.45)
BetMGM: +145 (2.45)

Total Over 228.0
DraftKings: -175 (1.58)
FoxBet: -188 (1.5333)

Total Under 228.0
DraftKings: +140 (2.4)
FoxBet: +140 (2.4)

Total Over 228.5
DraftKings: -162 (1.62)
FanDuel: -174 (1.5747)
FoxBet: -175 (1.5714)
BetMGM: -160 (1.62)

Total Under 228.5
DraftKings: +133 (2.33)
FanDuel: +140 (2.4)
FoxBet: +130 (2.3)
BetMGM: +135 (2.35)

Total Over 229.0
DraftKings: -157 (1.64)
FoxBet: -175 (1.5714)

Total Under 229.0
DraftKings: +128 (2.28)
FoxBet: +130 (2.3)

Total Over 229.5
DraftKings: -148 (1.68)
FanDuel: -158 (1.6329)
FoxBet: -154 (1.65)
BetMGM: -150 (1.67)

Total Under 229.5
DraftKings: +120 (2.2)
FanDuel: +128 (2.28)
FoxBet: +120 (2.2)
BetMGM: +125 (2.25)

Total Over 230.0
DraftKings: -143 (1.7)
FoxBet: -150 (1.6667)

Total Under 230.0
DraftKings: +117 (2.17)
FoxBet: +115 (2.15)

Total Over 230.5
DraftKings: -136 (1.74)
FanDuel: -142 (1.7042)
FoxBet: -143 (1.7)
BetMGM: -140 (1.72)

Total Under 230.5
DraftKings: +112 (2.12)
FanDuel: +116 (2.16)
FoxBet: +110 (2.1)
BetMGM: +115 (2.15)

Total Over 231.0
DraftKings: -130 (1.77)
FoxBet: -137 (1.7273)

Total Under 231.0
DraftKings: +107 (2.07)
FoxBet: +105 (2.05)

Total Over 231.5
DraftKings: -124 (1.81)
FanDuel: -128 (1.7812)
FoxBet: -137 (1.7273)
BetMGM: -125 (1.8)

Total Under 231.5
DraftKings: +102 (2.02)
FanDuel: +104 (2.04)
FoxBet: +105 (2.05)
BetMGM: +105 (2.05)

Total Over 232.0
DraftKings: -120 (1.84)
FoxBet: -133 (1.75)

Total Under 232.0
DraftKings: -103 (1.98)
FoxBet: +100 (2.0)

Total Over 232.5
DraftKings: -114 (1.88)
FanDuel: -118 (1.8475)
FoxBet: -125 (1.8)
BetMGM: -115 (1.87)

Total Under 232.5
DraftKings: -108 (1.93)
FanDuel: -104 (1.9615)
FoxBet: -105 (1.95)
BetMGM: -105 (1.95)

Total Over 233.5
DraftKings: -105 (1.96)
FanDuel: -106 (1.9434)
FoxBet: -105 (1.95)
BetMGM: -110 (1.91)

Total Under 233.5
DraftKings: -117 (1.86)
FanDuel: -116 (1.8621)
FoxBet: -125 (1.8)
BetMGM: -110 (1.91)

Total Over 234.0
DraftKings: +100 (2.0)
FoxBet: -105 (1.95)

Total Under 234.0
DraftKings: -122 (1.82)
FoxBet: -125 (1.8)

Total Over 234.5
DraftKings: +105 (2.05)
FanDuel: +106 (2.06)
FoxBet: +100 (2.0)
BetMGM: +100 (2.0)

Total Under 234.5
DraftKings: -129 (1.78)
FanDuel: -130 (1.7692)
FoxBet: -133 (1.75)
BetMGM: -120 (1.83)

Total Over 235.0
DraftKings: +110 (2.1)
FoxBet: +105 (2.05)

Total Under 235.0
DraftKings: -134 (1.75)
FoxBet: -137 (1.7273)

Total Over 235.5
DraftKings: +115 (2.15)
FanDuel: +116 (2.16)
FoxBet: +105 (2.05)
BetMGM: +110 (2.1)

Total Under 235.5
DraftKings: -139 (1.72)
FanDuel: -142 (1.7042)
FoxBet: -137 (1.7273)
BetMGM: -135 (1.75)

Total Over 236.0
DraftKings: +120 (2.2)
FoxBet: +110 (2.1)

Total Under 236.0
DraftKings: -148 (1.68)
FoxBet: -143 (1.7)

Total Over 236.5
DraftKings: +125 (2.25)
FanDuel: +128 (2.28)
FoxBet: +115 (2.15)
BetMGM: +115 (2.15)

Total Under 236.5
DraftKings: -152 (1.66)
FanDuel: -158 (1.6329)
FoxBet: -150 (1.6667)
BetMGM: -140 (1.72)

Total Over 237.0
DraftKings: +132 (2.32)
FoxBet: +125 (2.25)

Total Under 237.0
DraftKings: -162 (1.62)
FoxBet: -162 (1.6154)

Total Over 237.5
DraftKings: +135 (2.35)
FanDuel: +140 (2.4)
FoxBet: +130 (2.3)
BetMGM: +125 (2.25)

Total Under 237.5
DraftKings: -167 (1.6)
FanDuel: -174 (1.5747)
FoxBet: -175 (1.5714)
BetMGM: -155 (1.65)

Total Over 238.0
DraftKings: +143 (2.43)
FoxBet: +135 (2.35)

Total Under 238.0
DraftKings: -177 (1.57)
FoxBet: -182 (1.55)

Total Over 238.5
DraftKings: +148 (2.48)
FanDuel: +152 (2.52)
FoxBet: +140 (2.4)
BetMGM: +135 (2.35)

Total Under 238.5
DraftKings: -182 (1.55)
FanDuel: -190 (1.5263)
FoxBet: -188 (1.5333)
BetMGM: -165 (1.6)

Total Over 239.0
DraftKings: +155 (2.55)
FoxBet: +145 (2.45)

Total Under 239.0
DraftKings: -195 (1.52)
FoxBet: -200 (1.5)

Total Over 239.5
DraftKings: +163 (2.63)
FanDuel: +162 (2.62)
FoxBet: +155 (2.55)
BetMGM: +150 (2.5)

Total Under 239.5
DraftKings: -200 (1.5)
FanDuel: -205 (1.4878)
FoxBet: -212 (1.4706)
BetMGM: -185 (1.55)

Total Over 240.0
DraftKings: +170 (2.7)
FoxBet: +155 (2.55)

Total Under 240.0
DraftKings: -215 (1.47)
FoxBet: -212 (1.4706)

Total Over 240.5
DraftKings: +175 (2.75)
FanDuel: +178 (2.78)
FoxBet: +165 (2.65)
BetMGM: +155 (2.55)

Total Under 240.5
DraftKings: -220 (1.46)
FanDuel: -235 (1.4255)
FoxBet: -225 (1.4444)
BetMGM: -190 (1.53)

Total Over 241.0
DraftKings: +188 (2.88)
FoxBet: +170 (2.7)

Total Under 241.0
DraftKings: -235 (1.43)
FoxBet: -225 (1.4444)

Total Over 241.5
DraftKings: +190 (2.9)
FanDuel: +200 (3.0)
FoxBet: +185 (2.85)
BetMGM: +170 (2.7)

Total Under 241.5
DraftKings: -240 (1.42)
FanDuel: -265 (1.3774)
FoxBet: -250 (1.4)
BetMGM: -200 (1.48)

Total Over 242.0
DraftKings: +200 (3.0)
FoxBet: +190 (2.9)

Total Under 242.0
DraftKings: -250 (1.4)
FoxBet: -275 (1.3636)

Total Over 242.5
DraftKings: +210 (3.1)
FanDuel: +220 (3.2)
FoxBet: +200 (3.0)
BetMGM: +185 (2.85)

Total Under 242.5
DraftKings: -265 (1.38)
FanDuel: -295 (1.339)
FoxBet: -275 (1.3636)
BetMGM: -225 (1.44)

Total Over 243.0
DraftKings: +220 (3.2)
FoxBet: +205 (3.05)

Total Under 243.0
DraftKings: -278 (1.36)
FoxBet: -286 (1.35)

Total Over 243.5
DraftKings: +225 (3.25)
FanDuel: +235 (3.35)
FoxBet: +205 (3.05)

Total Under 243.5
DraftKings: -286 (1.35)
FanDuel: -320 (1.3125)
FoxBet: -286 (1.35)

Total Over 244.0
DraftKings: +245 (3.45)
FoxBet: +230 (3.3)

Total Under 244.0
DraftKings: -315 (1.32)
FoxBet: -333 (1.3)

Total Over 244.5
DraftKings: +245 (3.45)
FanDuel: +265 (3.65)
FoxBet: +230 (3.3)

Total Under 244.5
DraftKings: -315 (1.32)
FanDuel: -370 (1.2703)
FoxBet: -300 (1.3333)

Total Over 245.0
DraftKings: +265 (3.65)
FoxBet: +250 (3.5)

Total Under 245.0
DraftKings: -345 (1.29)
FoxBet: -350 (1.2857)

Total Over 245.5
DraftKings: +270 (3.7)
FanDuel: +285 (3.85)
FoxBet: +250 (3.5)

Total Under 245.5
DraftKings: -345 (1.29)
FanDuel: -410 (1.2439)
FoxBet: -350 (1.2857)

Total Over 246.0
DraftKings: +290 (3.9)
FoxBet: +275 (3.75)

Total Under 246.0
DraftKings: -375 (1.27)
FoxBet: -400 (1.25)

Total Over 246.5
DraftKings: +295 (3.95)
FanDuel: +330 (4.3)
FoxBet: +275 (3.75)

Total Under 246.5
DraftKings: -385 (1.26)
FanDuel: -480 (1.2083)
FoxBet: -400 (1.25)

Total Over 247.0
DraftKings: +310 (4.1)
FoxBet: +290 (3.9)

Total Under 247.0
DraftKings: -420 (1.24)
FoxBet: -450 (1.2222)

Total Over 247.5
DraftKings: +320 (4.2)
FanDuel: +370 (4.7)

Total Under 247.5
DraftKings: -420 (1.24)
FanDuel: -550 (1.1818)

Total Over 248.0
DraftKings: +340 (4.4)

Total Under 248.0
DraftKings: -455 (1.22)

Total Over 248.5
DraftKings: +350 (4.5)
FanDuel: +400 (5.0)

Total Under 248.5
DraftKings: -480 (1.21)
FanDuel: -620 (1.1613)

Total Over 249.0
DraftKings: +375 (4.75)

Total Under 249.0
DraftKings: -500 (1.2)

Total Over 249.5
DraftKings: +380 (4.8)
FanDuel: +450 (5.5)

Total Under 249.5
DraftKings: -530 (1.19)
FanDuel: -720 (1.1389)

Total Over 250.0
DraftKings: +390 (4.9)

Total Under 250.0
DraftKings: -560 (1.18)

Total Over 250.5
DraftKings: +410 (5.1)
FanDuel: +520 (6.2)

Total Under 250.5
DraftKings: -590 (1.17)
FanDuel: -900 (1.1111)

Total Over 207.5
FanDuel: -4500 (1.0222)

Total Over 208.5
FanDuel: -4000 (1.025)

Total Over 209.5
FanDuel: -3000 (1.0333)

Total Over 210.5
FanDuel: -2300 (1.0435)

Total Over 211.5
FanDuel: -2200 (1.0455)

Total Over 212.5
FanDuel: -1700 (1.0588)

Total Over 213.5
FanDuel: -1450 (1.069)

Total Over 251.5
FanDuel: +580 (6.8)

Total Over 252.5
FanDuel: +630 (7.3)

Total Over 253.5
FanDuel: +720 (8.2)

Total Over 254.5
FanDuel: +800 (9.0)

Total Over 255.5
FanDuel: +900 (10.0)

Total Over 256.5
FanDuel: +940 (10.4)

Total Over 257.5
FanDuel: +1100 (12.0)

Total Under 207.5
FanDuel: +1300 (14.0)

Total Under 208.5
FanDuel: +1200 (13.0)

Total Under 209.5
FanDuel: +1100 (12.0)

Total Under 210.5
FanDuel: +940 (10.4)

Total Under 211.5
FanDuel: +880 (9.8)

Total Under 212.5
FanDuel: +780 (8.8)

Total Under 213.5
FanDuel: +700 (8.0)

Total Under 251.5
FanDuel: -1050 (1.0952)

Total Under 252.5
FanDuel: -1200 (1.0833)

Total Under 253.5
FanDuel: -1500 (1.0667)

Total Under 254.5
FanDuel: -1800 (1.0556)

Total Under 255.5
FanDuel: -2200 (1.0455)

Total Under 256.5
FanDuel: -2300 (1.0435)

Total Under 257.5
FanDuel: -3000 (1.0333)
Moneyline
Moneyline Minnesota Timberwolves None
DraftKings: +380 (4.8)
FanDuel: +380 (4.8)
FoxBet: +380 (4.8)
BetMGM: +375 (4.75)

Moneyline Portland Trail Blazers None
DraftKings: -500 (1.2)
FanDuel: -480 (1.2083)
FoxBet: -650 (1.1538)
BetMGM: -500 (1.2)


, 'San Antonio Spurs @ Los Angeles Lakers': NBA: San Antonio Spurs @ Los Angeles Lakers
DraftKings - 179653709
FanDuel - 949400.3
FoxBet - 8711018
BetMGM - 10987586
Spread
Spread San Antonio Spurs 6.0
DraftKings: -110 (1.91)
FoxBet: -125 (1.8)

Spread Los Angeles Lakers -6.0
DraftKings: -110 (1.91)
FoxBet: -105 (1.95)

Spread San Antonio Spurs -7.5
DraftKings: +460 (5.6)
FanDuel: +480 (5.8)
FoxBet: +400 (5.0)

Spread Los Angeles Lakers 7.5
DraftKings: -670 (1.15)
FanDuel: -800 (1.125)
FoxBet: -650 (1.1538)

Spread San Antonio Spurs -7.0
DraftKings: +460 (5.6)
FoxBet: +380 (4.8)

Spread Los Angeles Lakers 7.0
DraftKings: -670 (1.15)
FoxBet: -650 (1.1538)

Spread San Antonio Spurs -6.5
DraftKings: +390 (4.9)
FanDuel: +430 (5.3)
FoxBet: +350 (4.5)

Spread Los Angeles Lakers 6.5
DraftKings: -560 (1.18)
FanDuel: -670 (1.1493)
FoxBet: -550 (1.1818)

Spread San Antonio Spurs -6.0
DraftKings: +390 (4.9)
FoxBet: +333 (4.3333)

Spread Los Angeles Lakers 6.0
DraftKings: -560 (1.18)
FoxBet: -550 (1.1818)

Spread San Antonio Spurs -5.5
DraftKings: +350 (4.5)
FanDuel: +380 (4.8)
FoxBet: +320 (4.2)

Spread Los Angeles Lakers 5.5
DraftKings: -480 (1.21)
FanDuel: -580 (1.1724)
FoxBet: -500 (1.2)

Spread San Antonio Spurs -5.0
DraftKings: +340 (4.4)
FoxBet: +290 (3.9)

Spread Los Angeles Lakers 5.0
DraftKings: -455 (1.22)
FoxBet: -450 (1.2222)

Spread San Antonio Spurs -4.5
DraftKings: +300 (4.0)
FanDuel: +310 (4.1)
FoxBet: +275 (3.75)

Spread Los Angeles Lakers 4.5
DraftKings: -400 (1.25)
FanDuel: -450 (1.2222)
FoxBet: -400 (1.25)

Spread San Antonio Spurs -4.0
DraftKings: +295 (3.95)
FoxBet: +275 (3.75)

Spread Los Angeles Lakers 4.0
DraftKings: -385 (1.26)
FoxBet: -400 (1.25)

Spread San Antonio Spurs -3.5
DraftKings: +265 (3.65)
FanDuel: +260 (3.6)
FoxBet: +250 (3.5)
BetMGM: +260 (3.6)

Spread Los Angeles Lakers 3.5
DraftKings: -345 (1.29)
FanDuel: -360 (1.2778)
FoxBet: -350 (1.2857)
BetMGM: -350 (1.3)

Spread San Antonio Spurs -3.0
DraftKings: +255 (3.55)
FoxBet: +230 (3.3)

Spread Los Angeles Lakers 3.0
DraftKings: -335 (1.3)
FoxBet: -333 (1.3)

Spread San Antonio Spurs -2.5
DraftKings: +230 (3.3)
FanDuel: +230 (3.3)
FoxBet: +215 (3.15)
BetMGM: +230 (3.3)

Spread Los Angeles Lakers 2.5
DraftKings: -295 (1.34)
FanDuel: -310 (1.3226)
FoxBet: -300 (1.3333)
BetMGM: -275 (1.35)

Spread San Antonio Spurs -2.0
DraftKings: +225 (3.25)
FoxBet: +205 (3.05)

Spread Los Angeles Lakers 2.0
DraftKings: -286 (1.35)
FoxBet: -286 (1.35)

Spread San Antonio Spurs -1.5
DraftKings: +205 (3.05)
FanDuel: +205 (3.05)
FoxBet: +200 (3.0)
BetMGM: +200 (3.0)

Spread Los Angeles Lakers 1.5
DraftKings: -265 (1.38)
FanDuel: -275 (1.3636)
FoxBet: -275 (1.3636)
BetMGM: -250 (1.4)

Spread San Antonio Spurs -1.0
DraftKings: +200 (3.0)
FoxBet: +190 (2.9)

Spread Los Angeles Lakers 1.0
DraftKings: -250 (1.4)
FoxBet: -275 (1.3636)

Spread San Antonio Spurs -0.5
DraftKings: +188 (2.88)
FanDuel: +186 (2.86)

Spread Los Angeles Lakers 0.5
DraftKings: -235 (1.43)
FanDuel: -245 (1.4082)

Spread San Antonio Spurs 0.0
DraftKings: +188 (2.88)
FoxBet: +185 (2.85)

Spread Los Angeles Lakers 0.0
DraftKings: -235 (1.43)
FoxBet: -250 (1.4)

Spread San Antonio Spurs 0.5
DraftKings: +188 (2.88)
FanDuel: +186 (2.86)

Spread Los Angeles Lakers -0.5
DraftKings: -235 (1.43)
FanDuel: -245 (1.4082)

Spread San Antonio Spurs 1.0
DraftKings: +180 (2.8)
FoxBet: +170 (2.7)

Spread Los Angeles Lakers -1.0
DraftKings: -225 (1.45)
FoxBet: -225 (1.4444)

Spread San Antonio Spurs 1.5
DraftKings: +170 (2.7)
FanDuel: +168 (2.68)
FoxBet: +155 (2.55)
BetMGM: +165 (2.65)

Spread Los Angeles Lakers -1.5
DraftKings: -210 (1.48)
FanDuel: -220 (1.4545)
FoxBet: -212 (1.4706)
BetMGM: -200 (1.5)

Spread San Antonio Spurs 2.0
DraftKings: +160 (2.6)
FoxBet: +155 (2.55)

Spread Los Angeles Lakers -2.0
DraftKings: -200 (1.5)
FoxBet: -212 (1.4706)

Spread San Antonio Spurs 2.5
DraftKings: +148 (2.48)
FanDuel: +144 (2.44)
FoxBet: +140 (2.4)
BetMGM: +145 (2.45)

Spread Los Angeles Lakers -2.5
DraftKings: -182 (1.55)
FanDuel: -186 (1.5376)
FoxBet: -188 (1.5333)
BetMGM: -175 (1.57)

Spread San Antonio Spurs 3.0
DraftKings: +140 (2.4)
FoxBet: +135 (2.35)

Spread Los Angeles Lakers -3.0
DraftKings: -175 (1.58)
FoxBet: -182 (1.55)

Spread San Antonio Spurs 3.5
DraftKings: +130 (2.3)
FanDuel: +128 (2.28)
FoxBet: +125 (2.25)
BetMGM: +125 (2.25)

Spread Los Angeles Lakers -3.5
DraftKings: -159 (1.63)
FanDuel: -160 (1.625)
FoxBet: -162 (1.6154)

Spread San Antonio Spurs 4.0
DraftKings: +123 (2.23)
FoxBet: +115 (2.15)

Spread Los Angeles Lakers -4.0
DraftKings: -150 (1.67)
FoxBet: -150 (1.6667)

Spread San Antonio Spurs 4.5
DraftKings: +114 (2.14)
FanDuel: +108 (2.08)
FoxBet: +105 (2.05)
BetMGM: +105 (2.05)

Spread Los Angeles Lakers -4.5
DraftKings: -137 (1.73)
FanDuel: -132 (1.7576)
FoxBet: -137 (1.7273)
BetMGM: -130 (1.78)

Spread San Antonio Spurs 5.0
DraftKings: +106 (2.06)
FoxBet: +100 (2.0)

Spread Los Angeles Lakers -5.0
DraftKings: -129 (1.78)
FoxBet: -133 (1.75)

Spread San Antonio Spurs 5.5
DraftKings: -103 (1.98)
FanDuel: -110 (1.9091)
FoxBet: +100 (2.0)
BetMGM: -110 (1.91)

Spread Los Angeles Lakers -5.5
DraftKings: -118 (1.85)
FanDuel: -110 (1.9091)
FoxBet: -118 (1.85)
BetMGM: -110 (1.91)

Spread San Antonio Spurs 6.5
DraftKings: -118 (1.85)
FanDuel: -128 (1.7812)
FoxBet: -133 (1.75)
BetMGM: -130 (1.78)

Spread Los Angeles Lakers -6.5
DraftKings: -103 (1.98)
FanDuel: +104 (2.04)
FoxBet: +100 (2.0)
BetMGM: +105 (2.05)

Spread San Antonio Spurs 7.0
DraftKings: -129 (1.78)
FoxBet: -143 (1.7)

Spread Los Angeles Lakers -7.0
DraftKings: +105 (2.05)
FoxBet: +110 (2.1)

Spread San Antonio Spurs 7.5
DraftKings: -137 (1.73)
FanDuel: -148 (1.6757)
FoxBet: -154 (1.65)
BetMGM: -155 (1.65)

Spread Los Angeles Lakers -7.5
DraftKings: +112 (2.12)
FanDuel: +120 (2.2)
FoxBet: +120 (2.2)
BetMGM: +125 (2.25)

Spread San Antonio Spurs 8.0
DraftKings: -148 (1.68)
FoxBet: -182 (1.55)

Spread Los Angeles Lakers -8.0
DraftKings: +120 (2.2)
FoxBet: +135 (2.35)

Spread San Antonio Spurs 8.5
DraftKings: -157 (1.64)
FanDuel: -166 (1.6024)
FoxBet: -188 (1.5333)
BetMGM: -185 (1.55)

Spread Los Angeles Lakers -8.5
DraftKings: +128 (2.28)
FanDuel: +132 (2.32)
FoxBet: +140 (2.4)
BetMGM: +150 (2.5)

Spread San Antonio Spurs 9.0
DraftKings: -175 (1.58)
FoxBet: -212 (1.4706)

Spread Los Angeles Lakers -9.0
DraftKings: +140 (2.4)
FoxBet: +155 (2.55)

Spread San Antonio Spurs 9.5
DraftKings: -180 (1.56)
FanDuel: -194 (1.5155)
FoxBet: -225 (1.4444)
BetMGM: -225 (1.45)

Spread Los Angeles Lakers -9.5
DraftKings: +148 (2.48)
FanDuel: +150 (2.5)
FoxBet: +165 (2.65)
BetMGM: +180 (2.8)

Spread San Antonio Spurs 10.0
DraftKings: -195 (1.52)
FoxBet: -250 (1.4)

Spread Los Angeles Lakers -10.0
DraftKings: +160 (2.6)
FoxBet: +185 (2.85)

Spread San Antonio Spurs 10.5
DraftKings: -205 (1.49)
FanDuel: -235 (1.4255)
FoxBet: -275 (1.3636)
BetMGM: -250 (1.4)

Spread Los Angeles Lakers -10.5
DraftKings: +165 (2.65)
FanDuel: +178 (2.78)
FoxBet: +190 (2.9)
BetMGM: +200 (3.0)

Spread San Antonio Spurs 11.0
DraftKings: -230 (1.44)
FoxBet: -286 (1.35)

Spread Los Angeles Lakers -11.0
DraftKings: +180 (2.8)
FoxBet: +205 (3.05)

Spread San Antonio Spurs 11.5
DraftKings: -240 (1.42)
FanDuel: -275 (1.3636)
FoxBet: -300 (1.3333)
BetMGM: -300 (1.34)

Spread Los Angeles Lakers -11.5
DraftKings: +190 (2.9)
FanDuel: +205 (3.05)
FoxBet: +215 (3.15)
BetMGM: +230 (3.3)

Spread San Antonio Spurs 12.0
DraftKings: -265 (1.38)
FoxBet: -333 (1.3)

Spread Los Angeles Lakers -12.0
DraftKings: +210 (3.1)
FoxBet: +230 (3.3)

Spread San Antonio Spurs 12.5
DraftKings: -275 (1.37)
FanDuel: -310 (1.3226)
FoxBet: -333 (1.3)
BetMGM: -350 (1.3)

Spread Los Angeles Lakers -12.5
DraftKings: +215 (3.15)
FanDuel: +230 (3.3)
FoxBet: +230 (3.3)
BetMGM: +260 (3.6)

Spread San Antonio Spurs 13.0
DraftKings: -305 (1.33)
FoxBet: -400 (1.25)

Spread Los Angeles Lakers -13.0
DraftKings: +235 (3.35)
FoxBet: +275 (3.75)

Spread San Antonio Spurs 13.5
DraftKings: -315 (1.32)
FanDuel: -350 (1.2857)
FoxBet: -400 (1.25)
BetMGM: -400 (1.25)

Spread Los Angeles Lakers -13.5
DraftKings: +245 (3.45)
FanDuel: +255 (3.55)
FoxBet: +275 (3.75)
BetMGM: +310 (4.1)

Spread San Antonio Spurs 14.0
DraftKings: -345 (1.29)
FoxBet: -450 (1.2222)

Spread Los Angeles Lakers -14.0
DraftKings: +270 (3.7)
FoxBet: +290 (3.9)

Spread San Antonio Spurs 14.5
DraftKings: -360 (1.28)
FanDuel: -390 (1.2564)
FoxBet: -450 (1.2222)
BetMGM: -450 (1.22)

Spread Los Angeles Lakers -14.5
DraftKings: +280 (3.8)
FanDuel: +280 (3.8)
FoxBet: +290 (3.9)
BetMGM: +350 (4.5)

Spread San Antonio Spurs 15.0
DraftKings: -400 (1.25)
FoxBet: -500 (1.2)

Spread Los Angeles Lakers -15.0
DraftKings: +310 (4.1)
FoxBet: +320 (4.2)

Spread San Antonio Spurs 15.5
DraftKings: -420 (1.24)
FanDuel: -480 (1.2083)
FoxBet: -550 (1.1818)
BetMGM: -550 (1.18)

Spread Los Angeles Lakers -15.5
DraftKings: +320 (4.2)
FanDuel: +330 (4.3)
FoxBet: +333 (4.3333)
BetMGM: +400 (5.0)

Spread San Antonio Spurs 16.0
DraftKings: -480 (1.21)
FoxBet: -550 (1.1818)

Spread Los Angeles Lakers -16.0
DraftKings: +350 (4.5)
FoxBet: +350 (4.5)

Spread San Antonio Spurs 16.5
DraftKings: -500 (1.2)
FanDuel: -560 (1.1786)
FoxBet: -650 (1.1538)
BetMGM: -650 (1.16)

Spread Los Angeles Lakers -16.5
DraftKings: +360 (4.6)
FanDuel: +370 (4.7)
FoxBet: +380 (4.8)
BetMGM: +450 (5.5)

Spread San Antonio Spurs 17.0
DraftKings: -560 (1.18)
FoxBet: -700 (1.1429)

Spread Los Angeles Lakers -17.0
DraftKings: +400 (5.0)
FoxBet: +425 (5.25)

Spread San Antonio Spurs 17.5
DraftKings: -560 (1.18)
FanDuel: -650 (1.1538)
FoxBet: -700 (1.1429)
BetMGM: -700 (1.14)

Spread Los Angeles Lakers -17.5
DraftKings: +400 (5.0)
FanDuel: +430 (5.3)
FoxBet: +450 (5.5)
BetMGM: +500 (6.0)

Spread San Antonio Spurs 28.5
FanDuel: -5000 (1.02)

Spread San Antonio Spurs 27.5
FanDuel: -4500 (1.0222)

Spread San Antonio Spurs 26.5
FanDuel: -4500 (1.0222)

Spread San Antonio Spurs 25.5
FanDuel: -3500 (1.0286)

Spread San Antonio Spurs 24.5
FanDuel: -3000 (1.0333)

Spread San Antonio Spurs 23.5
FanDuel: -2500 (1.04)

Spread San Antonio Spurs 22.5
FanDuel: -2000 (1.05)

Spread San Antonio Spurs 21.5
FanDuel: -1700 (1.0588)

Spread San Antonio Spurs 20.5
FanDuel: -1300 (1.0769)

Spread San Antonio Spurs 19.5
FanDuel: -950 (1.1053)

Spread San Antonio Spurs 18.5
FanDuel: -800 (1.125)
FoxBet: -900 (1.1111)

Spread San Antonio Spurs -8.5
FanDuel: +540 (6.4)
FoxBet: +475 (5.75)

Spread San Antonio Spurs -9.5
FanDuel: +630 (7.3)

Spread San Antonio Spurs -10.5
FanDuel: +790 (8.9)

Spread San Antonio Spurs -11.5
FanDuel: +900 (10.0)

Spread San Antonio Spurs -12.5
FanDuel: +1000 (11.0)

Spread San Antonio Spurs -13.5
FanDuel: +1100 (12.0)

Spread San Antonio Spurs -14.5
FanDuel: +1200 (13.0)

Spread San Antonio Spurs -15.5
FanDuel: +1300 (14.0)

Spread San Antonio Spurs -16.5
FanDuel: +1400 (15.0)

Spread San Antonio Spurs -17.5
FanDuel: +1800 (19.0)

Spread San Antonio Spurs -18.5
FanDuel: +2000 (21.0)

Spread Los Angeles Lakers -31.5
FanDuel: +2200 (23.0)

Spread Los Angeles Lakers -30.5
FanDuel: +2000 (21.0)

Spread Los Angeles Lakers -29.5
FanDuel: +1800 (19.0)

Spread Los Angeles Lakers -28.5
FanDuel: +1400 (15.0)

Spread Los Angeles Lakers -27.5
FanDuel: +1300 (14.0)

Spread Los Angeles Lakers -26.5
FanDuel: +1300 (14.0)

Spread Los Angeles Lakers -25.5
FanDuel: +1100 (12.0)

Spread Los Angeles Lakers -24.5
FanDuel: +1000 (11.0)

Spread Los Angeles Lakers -23.5
FanDuel: +920 (10.2)

Spread Los Angeles Lakers -22.5
FanDuel: +830 (9.3)

Spread Los Angeles Lakers -21.5
FanDuel: +760 (8.6)

Spread Los Angeles Lakers -20.5
FanDuel: +650 (7.5)

Spread Los Angeles Lakers -19.5
FanDuel: +540 (6.4)

Spread Los Angeles Lakers -18.5
FanDuel: +480 (5.8)
FoxBet: +475 (5.75)

Spread Los Angeles Lakers 8.5
FanDuel: -950 (1.1053)
FoxBet: -800 (1.125)

Spread Los Angeles Lakers 9.5
FanDuel: -1200 (1.0833)

Spread Los Angeles Lakers 10.5
FanDuel: -1800 (1.0556)

Spread Los Angeles Lakers 11.5
FanDuel: -2500 (1.04)

Spread Los Angeles Lakers 12.5
FanDuel: -3000 (1.0333)

Spread Los Angeles Lakers 13.5
FanDuel: -3500 (1.0286)

Spread Los Angeles Lakers 14.5
FanDuel: -3500 (1.0286)

Spread Los Angeles Lakers 15.5
FanDuel: -4500 (1.0222)

Spread Los Angeles Lakers 16.5
FanDuel: -5000 (1.02)

Spread San Antonio Spurs 18.0
FoxBet: -900 (1.1111)

Spread Los Angeles Lakers -18.0
FoxBet: +475 (5.75)

Spread San Antonio Spurs 19.0
FoxBet: -1000 (1.1)

Spread Los Angeles Lakers -19.0
FoxBet: +500 (6.0)

Spread San Antonio Spurs -9.0
FoxBet: +500 (6.0)

Spread Los Angeles Lakers 9.0
FoxBet: -900 (1.1111)

Spread Los Angeles Lakers 8.0
FoxBet: -800 (1.125)

Spread San Antonio Spurs -8.0
FoxBet: +475 (5.75)

Spread L.A. Lakers -3.5
BetMGM: -150 (1.67)
Total
Total Over 222.5
DraftKings: -112 (1.9)
FanDuel: -116 (1.8621)
FoxBet: -125 (1.8)
BetMGM: -110 (1.91)

Total Under 222.5
DraftKings: -109 (1.92)
FanDuel: -106 (1.9434)
FoxBet: -105 (1.95)
BetMGM: -110 (1.91)

Total Over 203.5
DraftKings: -770 (1.13)
FanDuel: -1450 (1.069)

Total Under 203.5
DraftKings: +500 (6.0)
FanDuel: +700 (8.0)

Total Over 204.0
DraftKings: -715 (1.14)

Total Under 204.0
DraftKings: +500 (6.0)

Total Over 204.5
DraftKings: -670 (1.15)
FanDuel: -1200 (1.0833)

Total Under 204.5
DraftKings: +460 (5.6)
FanDuel: +630 (7.3)

Total Over 205.0
DraftKings: -625 (1.16)

Total Under 205.0
DraftKings: +450 (5.5)

Total Over 205.5
DraftKings: -590 (1.17)
FanDuel: -950 (1.1053)

Total Under 205.5
DraftKings: +420 (5.2)
FanDuel: +540 (6.4)

Total Over 206.0
DraftKings: -560 (1.18)

Total Under 206.0
DraftKings: +400 (5.0)

Total Over 206.5
DraftKings: -530 (1.19)
FanDuel: -800 (1.125)

Total Under 206.5
DraftKings: +380 (4.8)
FanDuel: +480 (5.8)

Total Over 207.0
DraftKings: -500 (1.2)

Total Under 207.0
DraftKings: +375 (4.75)

Total Over 207.5
DraftKings: -480 (1.21)
FanDuel: -670 (1.1493)

Total Under 207.5
DraftKings: +350 (4.5)
FanDuel: +430 (5.3)

Total Over 208.0
DraftKings: -455 (1.22)

Total Under 208.0
DraftKings: +340 (4.4)

Total Over 208.5
DraftKings: -420 (1.24)
FanDuel: -580 (1.1724)

Total Under 208.5
DraftKings: +320 (4.2)
FanDuel: +380 (4.8)

Total Over 209.0
DraftKings: -400 (1.25)
FoxBet: -450 (1.2222)

Total Under 209.0
DraftKings: +310 (4.1)
FoxBet: +330 (4.3)

Total Over 209.5
DraftKings: -385 (1.26)
FanDuel: -480 (1.2083)
FoxBet: -450 (1.2222)

Total Under 209.5
DraftKings: +290 (3.9)
FanDuel: +330 (4.3)
FoxBet: +290 (3.9)

Total Over 210.0
DraftKings: -375 (1.27)
FoxBet: -450 (1.2222)

Total Under 210.0
DraftKings: +285 (3.85)
FoxBet: +290 (3.9)

Total Over 210.5
DraftKings: -345 (1.29)
FanDuel: -410 (1.2439)
FoxBet: -400 (1.25)

Total Under 210.5
DraftKings: +265 (3.65)
FanDuel: +290 (3.9)
FoxBet: +275 (3.75)

Total Over 211.0
DraftKings: -335 (1.3)
FoxBet: -400 (1.25)

Total Under 211.0
DraftKings: +260 (3.6)
FoxBet: +275 (3.75)

Total Over 211.5
DraftKings: -305 (1.33)
FanDuel: -370 (1.2703)
FoxBet: -333 (1.3)

Total Under 211.5
DraftKings: +240 (3.4)
FanDuel: +265 (3.65)
FoxBet: +230 (3.3)

Total Over 212.0
DraftKings: -305 (1.33)
FoxBet: -333 (1.3)

Total Under 212.0
DraftKings: +235 (3.35)
FoxBet: +230 (3.3)

Total Over 212.5
DraftKings: -278 (1.36)
FanDuel: -310 (1.3226)
FoxBet: -300 (1.3333)

Total Under 212.5
DraftKings: +220 (3.2)
FanDuel: +230 (3.3)
FoxBet: +230 (3.3)

Total Over 213.0
DraftKings: -275 (1.37)
FoxBet: -300 (1.3333)

Total Under 213.0
DraftKings: +215 (3.15)
FoxBet: +215 (3.15)

Total Over 213.5
DraftKings: -250 (1.4)
FanDuel: -280 (1.3571)
FoxBet: -275 (1.3636)

Total Under 213.5
DraftKings: +200 (3.0)
FanDuel: +210 (3.1)
FoxBet: +200 (3.0)

Total Over 214.0
DraftKings: -245 (1.41)
FoxBet: -275 (1.3636)

Total Under 214.0
DraftKings: +195 (2.95)
FoxBet: +200 (3.0)

Total Over 214.5
DraftKings: -235 (1.43)
FanDuel: -260 (1.3846)
FoxBet: -250 (1.4)
BetMGM: -225 (1.45)

Total Under 214.5
DraftKings: +185 (2.85)
FanDuel: +196 (2.96)
FoxBet: +185 (2.85)
BetMGM: +180 (2.8)

Total Over 215.0
DraftKings: -225 (1.45)
FoxBet: -250 (1.4)

Total Under 215.0
DraftKings: +180 (2.8)
FoxBet: +185 (2.85)

Total Over 215.5
DraftKings: -210 (1.48)
FanDuel: -230 (1.4348)
FoxBet: -225 (1.4444)
BetMGM: -200 (1.5)

Total Under 215.5
DraftKings: +170 (2.7)
FanDuel: +176 (2.76)
FoxBet: +165 (2.65)
BetMGM: +165 (2.65)

Total Over 216.0
DraftKings: -205 (1.49)
FoxBet: -225 (1.4444)

Total Under 216.0
DraftKings: +165 (2.65)
FoxBet: +165 (2.65)

Total Over 216.5
DraftKings: -195 (1.52)
FanDuel: -200 (1.5)
FoxBet: -212 (1.4706)
BetMGM: -185 (1.55)

Total Under 216.5
DraftKings: +155 (2.55)
FanDuel: +160 (2.6)
FoxBet: +155 (2.55)
BetMGM: +150 (2.5)

Total Over 217.0
DraftKings: -186 (1.54)
FoxBet: -200 (1.5)

Total Under 217.0
DraftKings: +150 (2.5)
FoxBet: +145 (2.45)

Total Over 217.5
DraftKings: -175 (1.58)
FanDuel: -188 (1.5319)
FoxBet: -188 (1.5333)
BetMGM: -165 (1.6)

Total Under 217.5
DraftKings: +143 (2.43)
FanDuel: +150 (2.5)
FoxBet: +140 (2.4)
BetMGM: +140 (2.4)

Total Over 218.0
DraftKings: -167 (1.6)
FoxBet: -182 (1.55)

Total Under 218.0
DraftKings: +138 (2.38)
FoxBet: +135 (2.35)

Total Over 218.5
DraftKings: -159 (1.63)
FanDuel: -172 (1.5814)
FoxBet: -175 (1.5714)
BetMGM: -155 (1.65)

Total Under 218.5
DraftKings: +130 (2.3)
FanDuel: +138 (2.38)
FoxBet: +130 (2.3)
BetMGM: +125 (2.25)

Total Over 219.0
DraftKings: -155 (1.65)
FoxBet: -162 (1.6154)

Total Under 219.0
DraftKings: +125 (2.25)
FoxBet: +125 (2.25)

Total Over 219.5
DraftKings: -148 (1.68)
FanDuel: -158 (1.6329)
FoxBet: -154 (1.65)
BetMGM: -145 (1.7)

Total Under 219.5
DraftKings: +120 (2.2)
FanDuel: +128 (2.28)
FoxBet: +120 (2.2)
BetMGM: +120 (2.2)

Total Over 220.0
DraftKings: -139 (1.72)
FoxBet: -150 (1.6667)

Total Under 220.0
DraftKings: +115 (2.15)
FoxBet: +115 (2.15)

Total Over 220.5
DraftKings: -134 (1.75)
FanDuel: -140 (1.7143)
FoxBet: -137 (1.7273)
BetMGM: -135 (1.75)

Total Under 220.5
DraftKings: +110 (2.1)
FanDuel: +114 (2.14)
FoxBet: +105 (2.05)
BetMGM: +110 (2.1)

Total Over 221.0
DraftKings: -127 (1.79)
FoxBet: -137 (1.7273)

Total Under 221.0
DraftKings: +105 (2.05)
FoxBet: +105 (2.05)

Total Over 221.5
DraftKings: -122 (1.82)
FanDuel: -128 (1.7812)
FoxBet: -133 (1.75)
BetMGM: -120 (1.83)

Total Under 221.5
DraftKings: +100 (2.0)
FanDuel: +104 (2.04)
FoxBet: +100 (2.0)
BetMGM: +100 (2.0)

Total Over 222.0
DraftKings: -117 (1.86)
FoxBet: -125 (1.8)

Total Under 222.0
DraftKings: -105 (1.96)
FoxBet: -105 (1.95)

Total Over 223.0
DraftKings: -107 (1.94)
FanDuel: -110 (1.9091)
FoxBet: -110 (1.9091)

Total Under 223.0
DraftKings: -114 (1.88)
FanDuel: -110 (1.9091)
FoxBet: -110 (1.9091)

Total Over 223.5
DraftKings: -103 (1.98)
FanDuel: -106 (1.9434)
FoxBet: -105 (1.95)
BetMGM: -105 (1.95)

Total Under 223.5
DraftKings: -120 (1.84)
FanDuel: -116 (1.8621)
FoxBet: -125 (1.8)
BetMGM: -115 (1.87)

Total Over 224.0
DraftKings: +102 (2.02)
FoxBet: +100 (2.0)

Total Under 224.0
DraftKings: -125 (1.8)
FoxBet: -133 (1.75)

Total Over 224.5
DraftKings: +107 (2.07)
FanDuel: +104 (2.04)
FoxBet: +105 (2.05)
BetMGM: +105 (2.05)

Total Under 224.5
DraftKings: -130 (1.77)
FanDuel: -128 (1.7812)
FoxBet: -137 (1.7273)
BetMGM: -125 (1.8)

Total Over 225.0
DraftKings: +112 (2.12)
FoxBet: +105 (2.05)

Total Under 225.0
DraftKings: -137 (1.73)
FoxBet: -137 (1.7273)

Total Over 225.5
DraftKings: +117 (2.17)
FanDuel: +116 (2.16)
FoxBet: +110 (2.1)
BetMGM: +115 (2.15)

Total Under 225.5
DraftKings: -143 (1.7)
FanDuel: -140 (1.7143)
FoxBet: -143 (1.7)
BetMGM: -140 (1.72)

Total Over 226.0
DraftKings: +123 (2.23)
FoxBet: +115 (2.15)

Total Under 226.0
DraftKings: -150 (1.67)
FoxBet: -150 (1.6667)

Total Over 226.5
DraftKings: +128 (2.28)
FanDuel: +128 (2.28)
FoxBet: +120 (2.2)
BetMGM: +125 (2.25)

Total Under 226.5
DraftKings: -157 (1.64)
FanDuel: -158 (1.6329)
FoxBet: -154 (1.65)
BetMGM: -150 (1.67)

Total Over 227.0
DraftKings: +133 (2.33)
FoxBet: +125 (2.25)

Total Under 227.0
DraftKings: -165 (1.61)
FoxBet: -162 (1.6154)

Total Over 227.5
DraftKings: +138 (2.38)
FanDuel: +142 (2.42)
FoxBet: +130 (2.3)
BetMGM: +135 (2.35)

Total Under 227.5
DraftKings: -175 (1.58)
FanDuel: -176 (1.5682)
FoxBet: -175 (1.5714)
BetMGM: -160 (1.62)

Total Over 228.0
DraftKings: +145 (2.45)
FoxBet: +140 (2.4)

Total Under 228.0
DraftKings: -180 (1.56)
FoxBet: -188 (1.5333)

Total Over 228.5
DraftKings: +150 (2.5)
FanDuel: +154 (2.54)
FoxBet: +140 (2.4)
BetMGM: +145 (2.45)

Total Under 228.5
DraftKings: -186 (1.54)
FanDuel: -192 (1.5208)
FoxBet: -188 (1.5333)
BetMGM: -175 (1.57)

Total Over 229.0
DraftKings: +160 (2.6)
FoxBet: +155 (2.55)

Total Under 229.0
DraftKings: -200 (1.5)
FoxBet: -212 (1.4706)

Total Over 229.5
DraftKings: +165 (2.65)
FanDuel: +160 (2.6)
FoxBet: +155 (2.55)
BetMGM: +155 (2.55)

Total Under 229.5
DraftKings: -205 (1.49)
FanDuel: -200 (1.5)
FoxBet: -212 (1.4706)
BetMGM: -190 (1.53)

Total Over 230.0
DraftKings: +175 (2.75)
FoxBet: +165 (2.65)

Total Under 230.0
DraftKings: -220 (1.46)
FoxBet: -225 (1.4444)

Total Over 230.5
DraftKings: +180 (2.8)
FanDuel: +178 (2.78)
FoxBet: +170 (2.7)
BetMGM: +165 (2.65)

Total Under 230.5
DraftKings: -225 (1.45)
FanDuel: -235 (1.4255)
FoxBet: -225 (1.4444)
BetMGM: -200 (1.5)

Total Over 231.0
DraftKings: +190 (2.9)
FoxBet: +185 (2.85)

Total Under 231.0
DraftKings: -240 (1.42)
FoxBet: -250 (1.4)

Total Over 231.5
DraftKings: +195 (2.95)
FanDuel: +200 (3.0)
FoxBet: +190 (2.9)
BetMGM: +180 (2.8)

Total Under 231.5
DraftKings: -245 (1.41)
FanDuel: -265 (1.3774)
FoxBet: -275 (1.3636)
BetMGM: -225 (1.45)

Total Over 232.0
DraftKings: +205 (3.05)
FoxBet: +200 (3.0)

Total Under 232.0
DraftKings: -265 (1.38)
FoxBet: -275 (1.3636)

Total Over 232.5
DraftKings: +215 (3.15)
FanDuel: +220 (3.2)
FoxBet: +200 (3.0)
BetMGM: +195 (2.95)

Total Under 232.5
DraftKings: -275 (1.37)
FanDuel: -295 (1.339)
FoxBet: -275 (1.3636)
BetMGM: -250 (1.42)

Total Over 233.0
DraftKings: +225 (3.25)
FoxBet: +215 (3.15)

Total Under 233.0
DraftKings: -286 (1.35)
FoxBet: -300 (1.3333)

Total Over 233.5
DraftKings: +230 (3.3)
FanDuel: +235 (3.35)
FoxBet: +215 (3.15)
BetMGM: +200 (3.0)

Total Under 233.5
DraftKings: -295 (1.34)
FanDuel: -320 (1.3125)
FoxBet: -300 (1.3333)
BetMGM: -250 (1.4)

Total Over 234.0
DraftKings: +245 (3.45)
FoxBet: +230 (3.3)

Total Under 234.0
DraftKings: -315 (1.32)
FoxBet: -333 (1.3)

Total Over 234.5
DraftKings: +245 (3.45)
FanDuel: +265 (3.65)
FoxBet: +230 (3.3)

Total Under 234.5
DraftKings: -315 (1.32)
FanDuel: -370 (1.2703)
FoxBet: -333 (1.3)

Total Over 235.0
DraftKings: +270 (3.7)
FoxBet: +275 (3.75)

Total Under 235.0
DraftKings: -345 (1.29)
FoxBet: -400 (1.25)

Total Over 235.5
DraftKings: +275 (3.75)
FanDuel: +285 (3.85)
FoxBet: +275 (3.75)

Total Under 235.5
DraftKings: -360 (1.28)
FanDuel: -400 (1.25)
FoxBet: -400 (1.25)

Total Over 236.0
DraftKings: +290 (3.9)
FoxBet: +275 (3.75)

Total Under 236.0
DraftKings: -385 (1.26)
FoxBet: -400 (1.25)

Total Over 236.5
DraftKings: +290 (3.9)
FanDuel: +320 (4.2)

Total Under 236.5
DraftKings: -385 (1.26)
FanDuel: -460 (1.2174)

Total Over 237.0
DraftKings: +320 (4.2)

Total Under 237.0
DraftKings: -420 (1.24)

Total Over 237.5
DraftKings: +325 (4.25)
FanDuel: +370 (4.7)

Total Under 237.5
DraftKings: -435 (1.23)
FanDuel: -550 (1.1818)

Total Over 238.0
DraftKings: +350 (4.5)

Total Under 238.0
DraftKings: -480 (1.21)

Total Over 238.5
DraftKings: +350 (4.5)
FanDuel: +400 (5.0)

Total Under 238.5
DraftKings: -480 (1.21)
FanDuel: -620 (1.1613)

Total Over 239.0
DraftKings: +375 (4.75)

Total Under 239.0
DraftKings: -530 (1.19)

Total Over 239.5
DraftKings: +380 (4.8)
FanDuel: +450 (5.5)

Total Under 239.5
DraftKings: -530 (1.19)
FanDuel: -720 (1.1389)

Total Over 240.0
DraftKings: +400 (5.0)

Total Under 240.0
DraftKings: -560 (1.18)

Total Over 240.5
DraftKings: +420 (5.2)
FanDuel: +500 (6.0)

Total Under 240.5
DraftKings: -590 (1.17)
FanDuel: -850 (1.1176)

Total Over 197.5
FanDuel: -4500 (1.0222)

Total Over 198.5
FanDuel: -4000 (1.025)

Total Over 199.5
FanDuel: -3000 (1.0333)

Total Over 200.5
FanDuel: -2300 (1.0435)

Total Over 201.5
FanDuel: -2200 (1.0455)

Total Over 202.5
FanDuel: -1800 (1.0556)

Total Over 241.5
FanDuel: +560 (6.6)

Total Over 242.5
FanDuel: +630 (7.3)

Total Over 243.5
FanDuel: +680 (7.8)

Total Over 244.5
FanDuel: +750 (8.5)

Total Over 245.5
FanDuel: +850 (9.5)

Total Over 246.5
FanDuel: +900 (10.0)

Total Over 247.5
FanDuel: +1000 (11.0)

Total Under 197.5
FanDuel: +1300 (14.0)

Total Under 198.5
FanDuel: +1200 (13.0)

Total Under 199.5
FanDuel: +1100 (12.0)

Total Under 200.5
FanDuel: +940 (10.4)

Total Under 201.5
FanDuel: +900 (10.0)

Total Under 202.5
FanDuel: +800 (9.0)

Total Under 241.5
FanDuel: -1000 (1.1)

Total Under 242.5
FanDuel: -1200 (1.0833)

Total Under 243.5
FanDuel: -1400 (1.0714)

Total Under 244.5
FanDuel: -1600 (1.0625)

Total Under 245.5
FanDuel: -2000 (1.05)

Total Under 246.5
FanDuel: -2200 (1.0455)

Total Under 247.5
FanDuel: -2400 (1.0417)
Moneyline
Moneyline San Antonio Spurs None
DraftKings: +188 (2.88)
FanDuel: +200 (3.0)
FoxBet: +200 (3.0)
BetMGM: +180 (2.8)

Moneyline Los Angeles Lakers None
DraftKings: -230 (1.44)
FanDuel: -245 (1.4082)
FoxBet: -250 (1.4)
BetMGM: -225 (1.45)


}, 'College Basketball': {"Mt. St. Mary's @ St. Francis (NY)": College Basketball: Mt. St. Mary's @ St. Francis (NY)
DraftKings - 179653585


, 'Central Connecticut State @ Bryant': College Basketball: Central Connecticut State @ Bryant
DraftKings - 179653584


, 'Northeastern @ Hofstra': College Basketball: Northeastern @ Hofstra
DraftKings - 179653587
FanDuel - 949356.3
FoxBet - 8715788
BetMGM - 10973258
Spread
Spread Northeastern 4.5
DraftKings: -108 (1.93)
FanDuel: -112 (1.8929)
FoxBet: -110 (1.9091)
BetMGM: -110 (1.91)

Spread Hofstra -4.5
DraftKings: -112 (1.9)
FanDuel: -110 (1.9091)
FoxBet: -110 (1.9091)
BetMGM: -110 (1.91)

Spread Northeastern -0.5
DraftKings: +165 (2.65)

Spread Hofstra 0.5
DraftKings: -205 (1.49)

Spread Northeastern 0.0
DraftKings: +165 (2.65)

Spread Hofstra 0.0
DraftKings: -205 (1.49)

Spread Northeastern 0.5
DraftKings: +165 (2.65)

Spread Hofstra -0.5
DraftKings: -205 (1.49)

Spread Northeastern 4.0
DraftKings: +100 (2.0)
FoxBet: -105 (1.95)

Spread Hofstra -4.0
DraftKings: -122 (1.82)
FoxBet: -125 (1.8)

Spread Northeastern 8.5
DraftKings: -215 (1.47)
BetMGM: -200 (1.48)

Spread Hofstra -8.5
DraftKings: +175 (2.75)
BetMGM: +170 (2.7)

Spread Northeastern 9.0
DraftKings: -240 (1.42)

Spread Hofstra -9.0
DraftKings: +190 (2.9)

Spread Northeastern 9.5
DraftKings: -250 (1.4)
BetMGM: -250 (1.4)

Spread Hofstra -9.5
DraftKings: +200 (3.0)
BetMGM: +200 (3.0)

Spread Hofstra -1.5
FoxBet: -188 (1.5333)
BetMGM: -185 (1.55)

Spread Northeastern 1.5
FoxBet: +140 (2.4)
BetMGM: +150 (2.5)

Spread Northeastern 7.5
FoxBet: -188 (1.5333)
BetMGM: -175 (1.57)

Spread Hofstra -7.5
FoxBet: +140 (2.4)
BetMGM: +145 (2.45)

Spread Hofstra -3.5
FoxBet: -137 (1.7273)
BetMGM: -135 (1.75)

Spread Northeastern 3.5
FoxBet: +105 (2.05)
BetMGM: +110 (2.1)

Spread Northeastern 5.5
FoxBet: -137 (1.7273)
BetMGM: -125 (1.8)

Spread Hofstra -5.5
FoxBet: +105 (2.05)
BetMGM: +105 (2.05)

Spread Northeastern 1.0
FoxBet: +155 (2.55)

Spread Hofstra -1.0
FoxBet: -200 (1.5)

Spread Northeastern 7.0
FoxBet: -175 (1.5714)

Spread Hofstra -7.0
FoxBet: +130 (2.3)

Spread Hofstra -6.0
FoxBet: +115 (2.15)

Spread Northeastern 6.0
FoxBet: -150 (1.6667)

Spread Northeastern 8.0
FoxBet: -212 (1.4706)

Spread Hofstra -8.0
FoxBet: +155 (2.55)

Spread Hofstra -3.0
FoxBet: -143 (1.7)

Spread Northeastern 3.0
FoxBet: +110 (2.1)

Spread Northeastern 2.0
FoxBet: +130 (2.3)

Spread Hofstra -2.0
FoxBet: -175 (1.5714)

Spread Northeastern 5.0
FoxBet: -125 (1.8)

Spread Hofstra -5.0
FoxBet: -105 (1.95)

Spread Northeastern 2.5
FoxBet: +120 (2.2)
BetMGM: +125 (2.25)

Spread Hofstra -2.5
FoxBet: -154 (1.65)
BetMGM: -155 (1.65)

Spread Hofstra -6.5
FoxBet: +120 (2.2)
BetMGM: +125 (2.25)

Spread Northeastern 6.5
FoxBet: -154 (1.65)
BetMGM: -150 (1.67)

Spread Northeastern 14.5
BetMGM: -600 (1.17)

Spread Hofstra -14.5
BetMGM: +425 (5.25)

Spread Northeastern 13.5
BetMGM: -500 (1.2)

Spread Hofstra -13.5
BetMGM: +375 (4.75)

Spread Northeastern 12.5
BetMGM: -400 (1.25)

Spread Hofstra -12.5
BetMGM: +310 (4.1)

Spread Northeastern 11.5
BetMGM: -350 (1.28)

Spread Hofstra -11.5
BetMGM: +275 (3.75)

Spread Northeastern 10.5
BetMGM: -300 (1.34)

Spread Hofstra -10.5
BetMGM: +230 (3.3)

Spread Northeastern -1.5
BetMGM: +195 (2.95)

Spread Hofstra 1.5
BetMGM: -250 (1.42)

Spread Northeastern -2.5
BetMGM: +225 (3.25)

Spread Hofstra 2.5
BetMGM: -275 (1.35)

Spread Northeastern -3.5
BetMGM: +260 (3.6)

Spread Hofstra 3.5
BetMGM: -350 (1.3)

Spread Northeastern -4.5
BetMGM: +310 (4.1)

Spread Hofstra 4.5
BetMGM: -400 (1.25)

Spread Northeastern -5.5
BetMGM: +360 (4.6)

Spread Hofstra 5.5
BetMGM: -500 (1.2)

Spread Northeastern -6.5
BetMGM: +425 (5.25)

Spread Hofstra 6.5
BetMGM: -600 (1.17)
Total
Total Over 138.5
DraftKings: -108 (1.93)
FanDuel: -106 (1.9434)
FoxBet: -105 (1.95)
BetMGM: -110 (1.91)

Total Under 138.5
DraftKings: -113 (1.89)
FanDuel: -114 (1.8772)
FoxBet: -125 (1.8)
BetMGM: -110 (1.91)

Total Over 133.5
DraftKings: -186 (1.54)
BetMGM: -185 (1.55)

Total Under 133.5
DraftKings: +150 (2.5)
BetMGM: +150 (2.5)

Total Over 134.0
DraftKings: -180 (1.56)

Total Under 134.0
DraftKings: +148 (2.48)

Total Over 134.5
DraftKings: -167 (1.6)
FoxBet: -154 (1.65)
BetMGM: -160 (1.62)

Total Under 134.5
DraftKings: +138 (2.38)
FoxBet: +120 (2.2)
BetMGM: +135 (2.35)

Total Over 138.0
DraftKings: -114 (1.88)
FoxBet: -110 (1.9091)

Total Under 138.0
DraftKings: -107 (1.94)
FoxBet: -110 (1.9091)

Total Over 142.5
DraftKings: +143 (2.43)
BetMGM: +135 (2.35)

Total Under 142.5
DraftKings: -175 (1.58)
BetMGM: -160 (1.62)

Total Over 143.0
DraftKings: +150 (2.5)

Total Under 143.0
DraftKings: -186 (1.54)

Total Over 143.5
DraftKings: +155 (2.55)
BetMGM: +145 (2.45)

Total Under 143.5
DraftKings: -195 (1.52)
BetMGM: -175 (1.57)

Total Over 141.5
FoxBet: +120 (2.2)
BetMGM: +120 (2.2)

Total Under 141.5
FoxBet: -154 (1.65)
BetMGM: -145 (1.7)

Total Under 136.5
FoxBet: +105 (2.05)
BetMGM: +110 (2.1)

Total Over 136.5
FoxBet: -137 (1.7273)
BetMGM: -135 (1.75)

Total Under 135.5
FoxBet: +110 (2.1)
BetMGM: +125 (2.25)

Total Over 135.5
FoxBet: -143 (1.7)
BetMGM: -150 (1.67)

Total Under 135.0
FoxBet: +115 (2.15)

Total Over 135.0
FoxBet: -150 (1.6667)

Total Under 136.0
FoxBet: +105 (2.05)

Total Over 136.0
FoxBet: -137 (1.7273)

Total Over 137.0
FoxBet: -133 (1.75)

Total Under 137.0
FoxBet: +100 (2.0)

Total Under 139.0
FoxBet: -125 (1.8)

Total Over 139.0
FoxBet: -105 (1.95)

Total Under 140.5
FoxBet: -143 (1.7)
BetMGM: -135 (1.75)

Total Over 140.5
FoxBet: +110 (2.1)
BetMGM: +110 (2.1)

Total Under 140.0
FoxBet: -137 (1.7273)

Total Over 140.0
FoxBet: +105 (2.05)

Total Over 141.0
FoxBet: +115 (2.15)

Total Under 141.0
FoxBet: -150 (1.6667)

Total Over 137.5
FoxBet: -125 (1.8)
BetMGM: -120 (1.83)

Total Under 137.5
FoxBet: -105 (1.95)
BetMGM: +100 (2.0)

Total Under 139.5
FoxBet: -133 (1.75)
BetMGM: -120 (1.83)

Total Over 139.5
FoxBet: +100 (2.0)
BetMGM: +100 (2.0)

Total Over 128.5
BetMGM: -300 (1.33)

Total Under 128.5
BetMGM: +240 (3.4)

Total Over 129.5
BetMGM: -275 (1.36)

Total Under 129.5
BetMGM: +220 (3.2)

Total Over 130.5
BetMGM: -250 (1.4)

Total Under 130.5
BetMGM: +200 (3.0)

Total Over 131.5
BetMGM: -225 (1.45)

Total Under 131.5
BetMGM: +180 (2.8)

Total Over 132.5
BetMGM: -200 (1.5)

Total Under 132.5
BetMGM: +165 (2.65)

Total Over 144.5
BetMGM: +155 (2.55)

Total Under 144.5
BetMGM: -190 (1.53)

Total Over 145.5
BetMGM: +170 (2.7)

Total Under 145.5
BetMGM: -200 (1.48)

Total Over 146.5
BetMGM: +190 (2.9)

Total Under 146.5
BetMGM: -250 (1.42)

Total Over 147.5
BetMGM: +200 (3.0)

Total Under 147.5
BetMGM: -250 (1.4)
Moneyline
Moneyline Northeastern None
DraftKings: +165 (2.65)
FanDuel: +168 (2.68)
FoxBet: +170 (2.7)
BetMGM: +170 (2.7)

Moneyline Hofstra None
DraftKings: -205 (1.49)
FanDuel: -200 (1.5)
FoxBet: -212 (1.4706)
BetMGM: -200 (1.48)


, 'Oregon @ Colorado': College Basketball: Oregon @ Colorado
DraftKings - 179653588
FanDuel - 949377.3
BetMGM - 10989429
Spread
Spread Oregon 3.5
DraftKings: -108 (1.93)
FanDuel: -110 (1.9091)
BetMGM: -110 (1.91)

Spread Colorado -3.5
DraftKings: -112 (1.9)
FanDuel: -110 (1.9091)
BetMGM: -110 (1.91)

Spread Oregon -1.5
DraftKings: +160 (2.6)
BetMGM: +165 (2.65)

Spread Colorado 1.5
DraftKings: -200 (1.5)
BetMGM: -200 (1.5)

Spread Oregon -1.0
DraftKings: +155 (2.55)

Spread Colorado 1.0
DraftKings: -190 (1.53)

Spread Oregon -0.5
DraftKings: +143 (2.43)

Spread Colorado 0.5
DraftKings: -175 (1.58)

Spread Oregon 3.0
DraftKings: +102 (2.02)

Spread Colorado -3.0
DraftKings: -122 (1.82)

Spread Oregon 7.5
DraftKings: -220 (1.46)
BetMGM: -200 (1.48)

Spread Colorado -7.5
DraftKings: +175 (2.75)
BetMGM: +170 (2.7)

Spread Oregon 8.0
DraftKings: -250 (1.4)

Spread Colorado -8.0
DraftKings: +200 (3.0)

Spread Oregon 8.5
DraftKings: -265 (1.38)
BetMGM: -250 (1.4)

Spread Colorado -8.5
DraftKings: +210 (3.1)
BetMGM: +200 (3.0)

Spread Oregon 12.5
BetMGM: -500 (1.2)

Spread Colorado -12.5
BetMGM: +375 (4.75)

Spread Oregon 11.5
BetMGM: -400 (1.25)

Spread Colorado -11.5
BetMGM: +310 (4.1)

Spread Oregon 10.5
BetMGM: -350 (1.28)

Spread Colorado -10.5
BetMGM: +275 (3.75)

Spread Oregon 9.5
BetMGM: -300 (1.33)

Spread Colorado -9.5
BetMGM: +240 (3.4)

Spread Oregon 6.5
BetMGM: -175 (1.57)

Spread Colorado -6.5
BetMGM: +145 (2.45)

Spread Oregon 5.5
BetMGM: -150 (1.67)

Spread Colorado -5.5
BetMGM: +125 (2.25)

Spread Oregon 4.5
BetMGM: -125 (1.8)

Spread Colorado -4.5
BetMGM: +105 (2.05)

Spread Oregon 2.5
BetMGM: +110 (2.1)

Spread Colorado -2.5
BetMGM: -135 (1.75)

Spread Oregon 1.5
BetMGM: +125 (2.25)

Spread Colorado -1.5
BetMGM: -155 (1.65)

Spread Oregon -2.5
BetMGM: +190 (2.9)

Spread Colorado 2.5
BetMGM: -250 (1.42)

Spread Oregon -3.5
BetMGM: +225 (3.25)

Spread Colorado 3.5
BetMGM: -275 (1.36)

Spread Oregon -4.5
BetMGM: +260 (3.6)

Spread Colorado 4.5
BetMGM: -350 (1.3)

Spread Oregon -5.5
BetMGM: +310 (4.1)

Spread Colorado 5.5
BetMGM: -400 (1.25)

Spread Oregon -6.5
BetMGM: +360 (4.6)

Spread Colorado 6.5
BetMGM: -500 (1.2)

Spread Oregon -7.5
BetMGM: +425 (5.25)

Spread Colorado 7.5
BetMGM: -600 (1.17)

Spread Oregon -8.5
BetMGM: +500 (6.0)

Spread Colorado 8.5
BetMGM: -700 (1.14)
Total
Total Over 141.5
DraftKings: -109 (1.92)
FanDuel: -110 (1.9091)
BetMGM: -110 (1.91)

Total Under 141.5
DraftKings: -110 (1.91)
FanDuel: -110 (1.9091)
BetMGM: -110 (1.91)

Total Over 136.5
DraftKings: -186 (1.54)
BetMGM: -185 (1.55)

Total Under 136.5
DraftKings: +150 (2.5)
BetMGM: +150 (2.5)

Total Over 137.0
DraftKings: -180 (1.56)

Total Under 137.0
DraftKings: +148 (2.48)

Total Over 137.5
DraftKings: -167 (1.6)
BetMGM: -160 (1.62)

Total Under 137.5
DraftKings: +138 (2.38)
BetMGM: +135 (2.35)

Total Over 142.0
DraftKings: -104 (1.97)

Total Under 142.0
DraftKings: -117 (1.86)

Total Over 145.5
DraftKings: +138 (2.38)
BetMGM: +135 (2.35)

Total Under 145.5
DraftKings: -167 (1.6)
BetMGM: -160 (1.62)

Total Over 146.0
DraftKings: +145 (2.45)

Total Under 146.0
DraftKings: -180 (1.56)

Total Over 146.5
DraftKings: +150 (2.5)
BetMGM: +145 (2.45)

Total Under 146.5
DraftKings: -186 (1.54)
BetMGM: -175 (1.57)

Total Over 131.5
BetMGM: -300 (1.33)

Total Under 131.5
BetMGM: +240 (3.4)

Total Over 132.5
BetMGM: -275 (1.36)

Total Under 132.5
BetMGM: +220 (3.2)

Total Over 133.5
BetMGM: -250 (1.42)

Total Under 133.5
BetMGM: +195 (2.95)

Total Over 134.5
BetMGM: -225 (1.45)

Total Under 134.5
BetMGM: +180 (2.8)

Total Over 135.5
BetMGM: -200 (1.5)

Total Under 135.5
BetMGM: +165 (2.65)

Total Over 138.5
BetMGM: -150 (1.67)

Total Under 138.5
BetMGM: +125 (2.25)

Total Over 139.5
BetMGM: -135 (1.75)

Total Under 139.5
BetMGM: +110 (2.1)

Total Over 140.5
BetMGM: -120 (1.83)

Total Under 140.5
BetMGM: +100 (2.0)

Total Over 142.5
BetMGM: +100 (2.0)

Total Under 142.5
BetMGM: -120 (1.83)

Total Over 143.5
BetMGM: +110 (2.1)

Total Under 143.5
BetMGM: -135 (1.75)

Total Over 144.5
BetMGM: +120 (2.2)

Total Under 144.5
BetMGM: -145 (1.7)

Total Over 147.5
BetMGM: +155 (2.55)

Total Under 147.5
BetMGM: -190 (1.53)

Total Over 148.5
BetMGM: +170 (2.7)

Total Under 148.5
BetMGM: -200 (1.48)

Total Over 149.5
BetMGM: +190 (2.9)

Total Under 149.5
BetMGM: -250 (1.42)

Total Over 150.5
BetMGM: +200 (3.0)

Total Under 150.5
BetMGM: -250 (1.4)
Moneyline
Moneyline Oregon None
DraftKings: +143 (2.43)
FanDuel: +146 (2.46)
BetMGM: +145 (2.45)

Moneyline Colorado None
DraftKings: -175 (1.58)
FanDuel: -174 (1.5747)
BetMGM: -175 (1.57)


, 'Indiana @ Wisconsin': College Basketball: Indiana @ Wisconsin
DraftKings - 179653600
FanDuel - 949358.3
Spread
Spread Indiana 9.0
DraftKings: -110 (1.91)

Spread Wisconsin -9.0
DraftKings: -110 (1.91)

Spread Indiana 4.0
DraftKings: +220 (3.2)

Spread Wisconsin -4.0
DraftKings: -278 (1.36)

Spread Indiana 4.5
DraftKings: +195 (2.95)

Spread Wisconsin -4.5
DraftKings: -245 (1.41)

Spread Indiana 5.0
DraftKings: +185 (2.85)

Spread Wisconsin -5.0
DraftKings: -230 (1.44)

Spread Indiana 8.5
DraftKings: -103 (1.98)
FanDuel: -104 (1.9615)

Spread Wisconsin -8.5
DraftKings: -120 (1.84)
FanDuel: -118 (1.8475)

Spread Indiana 13.0
DraftKings: -215 (1.47)

Spread Wisconsin -13.0
DraftKings: +175 (2.75)

Spread Indiana 13.5
DraftKings: -230 (1.44)

Spread Wisconsin -13.5
DraftKings: +185 (2.85)

Spread Indiana 14.0
DraftKings: -250 (1.4)

Spread Wisconsin -14.0
DraftKings: +200 (3.0)
Total
Total Over 127.5
DraftKings: -112 (1.9)
FanDuel: -112 (1.8929)

Total Under 127.5
DraftKings: -108 (1.93)
FanDuel: -108 (1.9259)

Total Over 122.5
DraftKings: -200 (1.5)

Total Under 122.5
DraftKings: +163 (2.63)

Total Over 123.0
DraftKings: -190 (1.53)

Total Under 123.0
DraftKings: +155 (2.55)

Total Over 123.5
DraftKings: -177 (1.57)

Total Under 123.5
DraftKings: +145 (2.45)

Total Over 128.0
DraftKings: -106 (1.95)

Total Under 128.0
DraftKings: -114 (1.88)

Total Over 131.5
DraftKings: +138 (2.38)

Total Under 131.5
DraftKings: -167 (1.6)

Total Over 132.0
DraftKings: +150 (2.5)

Total Under 132.0
DraftKings: -182 (1.55)

Total Over 132.5
DraftKings: +155 (2.55)

Total Under 132.5
DraftKings: -190 (1.53)
Moneyline
Moneyline Indiana None
DraftKings: +370 (4.7)
FanDuel: +360 (4.6)

Moneyline Wisconsin None
DraftKings: -500 (1.2)
FanDuel: -480 (1.2083)


, 'Tennessee Tech @ Morehead State': College Basketball: Tennessee Tech @ Morehead State
DraftKings - 179653597
FanDuel - 949360.3
FoxBet - 8715792
BetMGM - 10973256
Spread
Spread Tennessee Tech 4.0
DraftKings: -113 (1.89)
FoxBet: -110 (1.9091)

Spread Morehead State -4.0
DraftKings: -107 (1.94)
FoxBet: -110 (1.9091)

Spread Tennessee Tech -1.0
DraftKings: +155 (2.55)

Spread Morehead State 1.0
DraftKings: -195 (1.52)

Spread Tennessee Tech -0.5
DraftKings: +148 (2.48)

Spread Morehead State 0.5
DraftKings: -180 (1.56)

Spread Tennessee Tech 0.0
DraftKings: +148 (2.48)

Spread Morehead State 0.0
DraftKings: -180 (1.56)

Spread Tennessee Tech 3.5
DraftKings: -105 (1.96)
FanDuel: -102 (1.9804)
FoxBet: -105 (1.95)
BetMGM: +100 (2.0)

Spread Morehead State -3.5
DraftKings: -117 (1.86)
FanDuel: -120 (1.8333)
FoxBet: -125 (1.8)
BetMGM: -120 (1.83)

Spread Tennessee Tech 8.0
DraftKings: -235 (1.43)
FoxBet: -225 (1.4444)

Spread Morehead State -8.0
DraftKings: +190 (2.9)
FoxBet: +165 (2.65)

Spread Tennessee Tech 8.5
DraftKings: -245 (1.41)
BetMGM: -250 (1.42)

Spread Morehead State -8.5
DraftKings: +195 (2.95)
BetMGM: +190 (2.9)

Spread Tennessee Tech 9.0
DraftKings: -278 (1.36)

Spread Morehead State -9.0
DraftKings: +220 (3.2)

Spread Morehead State -1.5
FoxBet: -175 (1.5714)
BetMGM: -165 (1.6)

Spread Tennessee Tech 1.5
FoxBet: +130 (2.3)
BetMGM: +135 (2.35)

Spread Tennessee Tech 7.5
FoxBet: -200 (1.5)
BetMGM: -200 (1.5)

Spread Morehead State -7.5
FoxBet: +155 (2.55)
BetMGM: +165 (2.65)

Spread Tennessee Tech 5.5
FoxBet: -143 (1.7)
BetMGM: -140 (1.72)

Spread Morehead State -5.5
FoxBet: +110 (2.1)
BetMGM: +115 (2.15)

Spread Tennessee Tech 1.0
FoxBet: +140 (2.4)

Spread Morehead State -1.0
FoxBet: -188 (1.5333)

Spread Tennessee Tech 7.0
FoxBet: -188 (1.5333)

Spread Morehead State -7.0
FoxBet: +140 (2.4)

Spread Tennessee Tech 6.0
FoxBet: -154 (1.65)

Spread Morehead State -6.0
FoxBet: +120 (2.2)

Spread Tennessee Tech 3.0
FoxBet: +105 (2.05)

Spread Morehead State -3.0
FoxBet: -137 (1.7273)

Spread Tennessee Tech 2.0
FoxBet: +120 (2.2)

Spread Morehead State -2.0
FoxBet: -154 (1.65)

Spread Morehead State -5.0
FoxBet: +105 (2.05)

Spread Tennessee Tech 5.0
FoxBet: -137 (1.7273)

Spread Tennessee Tech 2.5
FoxBet: +110 (2.1)
BetMGM: +120 (2.2)

Spread Morehead State -2.5
FoxBet: -143 (1.7)
BetMGM: -145 (1.7)

Spread Tennessee Tech 4.5
FoxBet: -125 (1.8)
BetMGM: -115 (1.87)

Spread Morehead State -4.5
FoxBet: -105 (1.95)
BetMGM: -105 (1.95)

Spread Tennessee Tech 6.5
FoxBet: -175 (1.5714)
BetMGM: -165 (1.6)

Spread Morehead State -6.5
FoxBet: +130 (2.3)
BetMGM: +135 (2.35)

Spread Tennessee Tech 15.5
BetMGM: -800 (1.12)

Spread Morehead State -15.5
BetMGM: +550 (6.5)

Spread Tennessee Tech 14.5
BetMGM: -650 (1.15)

Spread Morehead State -14.5
BetMGM: +475 (5.75)

Spread Tennessee Tech 13.5
BetMGM: -550 (1.18)

Spread Morehead State -13.5
BetMGM: +400 (5.0)

Spread Tennessee Tech 12.5
BetMGM: -450 (1.22)

Spread Morehead State -12.5
BetMGM: +350 (4.5)

Spread Tennessee Tech 11.5
BetMGM: -375 (1.26)

Spread Morehead State -11.5
BetMGM: +300 (4.0)

Spread Tennessee Tech 10.5
BetMGM: -350 (1.3)

Spread Morehead State -10.5
BetMGM: +260 (3.6)

Spread Tennessee Tech 9.5
BetMGM: -275 (1.36)

Spread Morehead State -9.5
BetMGM: +220 (3.2)

Spread Tennessee Tech -1.5
BetMGM: +180 (2.8)

Spread Morehead State 1.5
BetMGM: -225 (1.45)

Spread Tennessee Tech -2.5
BetMGM: +200 (3.0)

Spread Morehead State 2.5
BetMGM: -250 (1.4)

Spread Tennessee Tech -3.5
BetMGM: +240 (3.4)

Spread Morehead State 3.5
BetMGM: -300 (1.33)

Spread Tennessee Tech -4.5
BetMGM: +290 (3.9)

Spread Morehead State 4.5
BetMGM: -375 (1.26)

Spread Tennessee Tech -5.5
BetMGM: +333 (4.33)

Spread Morehead State 5.5
BetMGM: -450 (1.22)
Total
Total Over 133.0
DraftKings: -107 (1.94)
FoxBet: -125 (1.8)

Total Under 133.0
DraftKings: -113 (1.89)
FoxBet: -105 (1.95)

Total Over 128.0
DraftKings: -195 (1.52)

Total Under 128.0
DraftKings: +155 (2.55)

Total Over 128.5
DraftKings: -180 (1.56)
BetMGM: -165 (1.6)

Total Under 128.5
DraftKings: +145 (2.45)
BetMGM: +135 (2.35)

Total Over 129.0
DraftKings: -167 (1.6)

Total Under 129.0
DraftKings: +138 (2.38)

Total Over 132.5
DraftKings: -113 (1.89)
FoxBet: -125 (1.8)
BetMGM: -110 (1.91)

Total Under 132.5
DraftKings: -107 (1.94)
FoxBet: -105 (1.95)
BetMGM: -110 (1.91)

Total Over 137.0
DraftKings: +145 (2.45)
FoxBet: +125 (2.25)

Total Under 137.0
DraftKings: -177 (1.57)
FoxBet: -162 (1.6154)

Total Over 137.5
DraftKings: +150 (2.5)
BetMGM: +145 (2.45)

Total Under 137.5
DraftKings: -186 (1.54)
BetMGM: -175 (1.57)

Total Over 138.0
DraftKings: +163 (2.63)

Total Under 138.0
DraftKings: -200 (1.5)

Total Over 133.5
FanDuel: -108 (1.9259)
FoxBet: -110 (1.9091)
BetMGM: +100 (2.0)

Total Under 133.5
FanDuel: -112 (1.8929)
FoxBet: -125 (1.8)
BetMGM: -120 (1.83)

Total Under 134.5
FoxBet: -133 (1.75)
BetMGM: -135 (1.75)

Total Over 134.5
FoxBet: +100 (2.0)
BetMGM: +110 (2.1)

Total Under 130.5
FoxBet: +115 (2.15)
BetMGM: +110 (2.1)

Total Over 130.5
FoxBet: -150 (1.6667)
BetMGM: -135 (1.75)

Total Under 136.5
FoxBet: -150 (1.6667)
BetMGM: -160 (1.62)

Total Over 136.5
FoxBet: +115 (2.15)
BetMGM: +135 (2.35)

Total Under 135.5
FoxBet: -137 (1.7273)
BetMGM: -145 (1.7)

Total Over 135.5
FoxBet: +105 (2.05)
BetMGM: +120 (2.2)

Total Under 131.5
FoxBet: +105 (2.05)
BetMGM: +100 (2.0)

Total Over 131.5
FoxBet: -137 (1.7273)
BetMGM: -120 (1.83)

Total Under 130.0
FoxBet: +120 (2.2)

Total Over 130.0
FoxBet: -154 (1.65)

Total Under 131.0
FoxBet: +110 (2.1)

Total Over 131.0
FoxBet: -143 (1.7)

Total Under 132.0
FoxBet: +100 (2.0)

Total Over 132.0
FoxBet: -133 (1.75)

Total Under 134.0
FoxBet: -125 (1.8)

Total Over 134.0
FoxBet: -105 (1.95)

Total Under 135.0
FoxBet: -137 (1.7273)

Total Over 135.0
FoxBet: +105 (2.05)

Total Over 136.0
FoxBet: +110 (2.1)

Total Under 136.0
FoxBet: -143 (1.7)

Total Over 124.5
BetMGM: -250 (1.4)

Total Under 124.5
BetMGM: +200 (3.0)

Total Over 125.5
BetMGM: -225 (1.45)

Total Under 125.5
BetMGM: +180 (2.8)

Total Over 126.5
BetMGM: -200 (1.5)

Total Under 126.5
BetMGM: +165 (2.65)

Total Over 127.5
BetMGM: -185 (1.55)

Total Under 127.5
BetMGM: +150 (2.5)

Total Over 129.5
BetMGM: -150 (1.67)

Total Under 129.5
BetMGM: +125 (2.25)

Total Over 138.5
BetMGM: +155 (2.55)

Total Under 138.5
BetMGM: -190 (1.53)

Total Over 139.5
BetMGM: +180 (2.8)

Total Under 139.5
BetMGM: -225 (1.45)

Total Over 140.5
BetMGM: +190 (2.9)

Total Under 140.5
BetMGM: -250 (1.42)

Total Over 141.5
BetMGM: +220 (3.2)

Total Under 141.5
BetMGM: -275 (1.36)

Total Over 142.5
BetMGM: +230 (3.3)

Total Under 142.5
BetMGM: -300 (1.34)

Total Over 143.5
BetMGM: +260 (3.6)

Total Under 143.5
BetMGM: -350 (1.3)
Moneyline
Moneyline Tennessee Tech None
DraftKings: +148 (2.48)
FanDuel: +158 (2.58)
FoxBet: +150 (2.5)
BetMGM: +155 (2.55)

Moneyline Morehead State None
DraftKings: -180 (1.56)
FanDuel: -188 (1.5319)
FoxBet: -188 (1.5333)
BetMGM: -190 (1.53)


, 'Southeast Missouri State @ Belmont': College Basketball: Southeast Missouri State @ Belmont
DraftKings - 179653591
FoxBet - 8715794
Spread
Spread Southeast Missouri State 13.5
DraftKings: -109 (1.92)
FoxBet: -105 (1.95)

Spread Belmont -13.5
DraftKings: -110 (1.91)
FoxBet: -125 (1.8)

Spread Southeast Missouri State 8.5
DraftKings: +195 (2.95)

Spread Belmont -8.5
DraftKings: -245 (1.41)

Spread Southeast Missouri State 9.0
DraftKings: +185 (2.85)

Spread Belmont -9.0
DraftKings: -230 (1.44)

Spread Southeast Missouri State 9.5
DraftKings: +165 (2.65)

Spread Belmont -9.5
DraftKings: -205 (1.49)

Spread Southeast Missouri State 14.0
DraftKings: -118 (1.85)
FoxBet: -110 (1.9091)

Spread Belmont -14.0
DraftKings: -103 (1.98)
FoxBet: -110 (1.9091)

Spread Southeast Missouri State 17.5
DraftKings: -200 (1.5)
FoxBet: -188 (1.5333)

Spread Belmont -17.5
DraftKings: +165 (2.65)
FoxBet: +140 (2.4)

Spread Southeast Missouri State 18.0
DraftKings: -225 (1.45)

Spread Belmont -18.0
DraftKings: +180 (2.8)

Spread Southeast Missouri State 18.5
DraftKings: -240 (1.42)

Spread Belmont -18.5
DraftKings: +190 (2.9)

Spread Southeast Missouri State 12.0
FoxBet: +120 (2.2)

Spread Belmont -12.0
FoxBet: -154 (1.65)

Spread Southeast Missouri State 11.0
FoxBet: +140 (2.4)

Spread Belmont -11.0
FoxBet: -188 (1.5333)

Spread Southeast Missouri State 13.0
FoxBet: +105 (2.05)

Spread Belmont -13.0
FoxBet: -137 (1.7273)

Spread Belmont -16.0
FoxBet: +115 (2.15)

Spread Southeast Missouri State 16.0
FoxBet: -150 (1.6667)

Spread Southeast Missouri State 15.0
FoxBet: -137 (1.7273)

Spread Belmont -15.0
FoxBet: +105 (2.05)

Spread Southeast Missouri State 17.0
FoxBet: -182 (1.55)

Spread Belmont -17.0
FoxBet: +135 (2.35)

Spread Southeast Missouri State 11.5
FoxBet: +130 (2.3)

Spread Belmont -11.5
FoxBet: -175 (1.5714)

Spread Southeast Missouri State 15.5
FoxBet: -137 (1.7273)

Spread Belmont -15.5
FoxBet: +105 (2.05)

Spread Belmont -10.5
FoxBet: -200 (1.5)

Spread Southeast Missouri State 10.5
FoxBet: +145 (2.45)

Spread Southeast Missouri State 16.5
FoxBet: -162 (1.6154)

Spread Belmont -16.5
FoxBet: +125 (2.25)

Spread Southeast Missouri State 12.5
FoxBet: +110 (2.1)

Spread Belmont -12.5
FoxBet: -143 (1.7)

Spread Southeast Missouri State 14.5
FoxBet: -125 (1.8)

Spread Belmont -14.5
FoxBet: -105 (1.95)
Total
Total Over 145.0
DraftKings: -109 (1.92)
FoxBet: -105 (1.95)

Total Under 145.0
DraftKings: -110 (1.91)
FoxBet: -125 (1.8)

Total Over 140.0
DraftKings: -195 (1.52)

Total Under 140.0
DraftKings: +160 (2.6)

Total Over 140.5
DraftKings: -182 (1.55)

Total Under 140.5
DraftKings: +150 (2.5)

Total Over 141.0
DraftKings: -175 (1.58)

Total Under 141.0
DraftKings: +143 (2.43)

Total Over 144.5
DraftKings: -115 (1.87)
FoxBet: -110 (1.9091)

Total Under 144.5
DraftKings: -105 (1.96)
FoxBet: -110 (1.9091)

Total Over 149.0
DraftKings: +145 (2.45)

Total Under 149.0
DraftKings: -177 (1.57)

Total Over 149.5
DraftKings: +150 (2.5)

Total Under 149.5
DraftKings: -182 (1.55)

Total Over 150.0
DraftKings: +160 (2.6)

Total Under 150.0
DraftKings: -195 (1.52)

Total Under 143.5
FoxBet: -105 (1.95)

Total Over 143.5
FoxBet: -125 (1.8)

Total Under 145.5
FoxBet: -125 (1.8)

Total Over 145.5
FoxBet: -105 (1.95)

Total Under 143.0
FoxBet: +100 (2.0)

Total Over 143.0
FoxBet: -133 (1.75)

Total Under 144.0
FoxBet: -105 (1.95)

Total Over 144.0
FoxBet: -125 (1.8)

Total Under 146.0
FoxBet: -133 (1.75)

Total Over 146.0
FoxBet: +100 (2.0)
Moneyline
Moneyline Southeast Missouri State None
DraftKings: +750 (8.5)
FoxBet: +750 (8.5)

Moneyline Belmont None
DraftKings: -1250 (1.08)
FoxBet: -1600 (1.0625)


, 'Cincinnati @ SMU': College Basketball: Cincinnati @ SMU
DraftKings - 179653598
FoxBet - 8715796
BetMGM - 10973250
Spread
Spread Cincinnati 5.5
DraftKings: -109 (1.92)
FoxBet: -125 (1.8)
BetMGM: -105 (1.95)

Spread SMU -5.5
DraftKings: -110 (1.91)
FoxBet: -105 (1.95)
BetMGM: -115 (1.87)

Spread Cincinnati 0.5
DraftKings: +195 (2.95)

Spread SMU -0.5
DraftKings: -245 (1.41)

Spread Cincinnati 1.0
DraftKings: +190 (2.9)

Spread SMU -1.0
DraftKings: -235 (1.43)

Spread Cincinnati 1.5
DraftKings: +175 (2.75)
BetMGM: +185 (2.85)

Spread SMU -1.5
DraftKings: -215 (1.47)
BetMGM: -225 (1.44)

Spread Cincinnati 6.0
DraftKings: -120 (1.84)
FoxBet: -133 (1.75)

Spread SMU -6.0
DraftKings: -103 (1.98)
FoxBet: +100 (2.0)

Spread Cincinnati 9.5
DraftKings: -215 (1.47)
BetMGM: -200 (1.5)

Spread SMU -9.5
DraftKings: +175 (2.75)
BetMGM: +165 (2.65)

Spread Cincinnati 10.0
DraftKings: -240 (1.42)

Spread SMU -10.0
DraftKings: +190 (2.9)

Spread Cincinnati 10.5
DraftKings: -250 (1.4)
BetMGM: -250 (1.42)

Spread SMU -10.5
DraftKings: +200 (3.0)
BetMGM: +190 (2.9)

Spread SMU -7.5
FoxBet: +125 (2.25)
BetMGM: +120 (2.2)

Spread Cincinnati 7.5
FoxBet: -162 (1.6154)
BetMGM: -145 (1.7)

Spread Cincinnati 3.5
FoxBet: +115 (2.15)
BetMGM: +135 (2.35)

Spread SMU -3.5
FoxBet: -150 (1.6667)
BetMGM: -160 (1.62)

Spread SMU -7.0
FoxBet: +115 (2.15)

Spread Cincinnati 7.0
FoxBet: -150 (1.6667)

Spread Cincinnati 9.0
FoxBet: -212 (1.4706)

Spread SMU -9.0
FoxBet: +155 (2.55)

Spread SMU -8.0
FoxBet: +135 (2.35)

Spread Cincinnati 8.0
FoxBet: -182 (1.55)

Spread SMU -3.0
FoxBet: -175 (1.5714)

Spread Cincinnati 3.0
FoxBet: +130 (2.3)

Spread SMU -2.0
FoxBet: -200 (1.5)

Spread Cincinnati 2.0
FoxBet: +155 (2.55)

Spread SMU -5.0
FoxBet: -118 (1.85)

Spread Cincinnati 5.0
FoxBet: +100 (2.0)

Spread Cincinnati 4.0
FoxBet: +105 (2.05)

Spread SMU -4.0
FoxBet: -137 (1.7273)

Spread SMU -2.5
FoxBet: -182 (1.55)
BetMGM: -190 (1.53)

Spread Cincinnati 2.5
FoxBet: +135 (2.35)
BetMGM: +155 (2.55)

Spread SMU -8.5
FoxBet: +140 (2.4)
BetMGM: +140 (2.4)

Spread Cincinnati 8.5
FoxBet: -188 (1.5333)
BetMGM: -165 (1.6)

Spread SMU -4.5
FoxBet: -133 (1.75)
BetMGM: -140 (1.72)

Spread Cincinnati 4.5
FoxBet: +100 (2.0)
BetMGM: +115 (2.15)

Spread SMU -6.5
FoxBet: +105 (2.05)
BetMGM: +100 (2.0)

Spread Cincinnati 6.5
FoxBet: -137 (1.7273)
BetMGM: -120 (1.83)

Spread Cincinnati 15.5
BetMGM: -550 (1.18)

Spread SMU -15.5
BetMGM: +400 (5.0)

Spread Cincinnati 14.5
BetMGM: -450 (1.22)

Spread SMU -14.5
BetMGM: +350 (4.5)

Spread Cincinnati 13.5
BetMGM: -400 (1.25)

Spread SMU -13.5
BetMGM: +300 (4.0)

Spread Cincinnati 12.5
BetMGM: -350 (1.3)

Spread SMU -12.5
BetMGM: +260 (3.6)

Spread Cincinnati 11.5
BetMGM: -275 (1.36)

Spread SMU -11.5
BetMGM: +225 (3.25)

Spread Cincinnati -1.5
BetMGM: +240 (3.4)

Spread SMU 1.5
BetMGM: -300 (1.33)

Spread Cincinnati -2.5
BetMGM: +275 (3.75)

Spread SMU 2.5
BetMGM: -350 (1.28)

Spread Cincinnati -3.5
BetMGM: +310 (4.1)

Spread SMU 3.5
BetMGM: -400 (1.25)

Spread Cincinnati -4.5
BetMGM: +375 (4.75)

Spread SMU 4.5
BetMGM: -500 (1.2)

Spread Cincinnati -5.5
BetMGM: +450 (5.5)

Spread SMU 5.5
BetMGM: -650 (1.16)
Total
Total Over 142.0
DraftKings: -107 (1.94)
FoxBet: -105 (1.95)

Total Under 142.0
DraftKings: -113 (1.89)
FoxBet: -125 (1.8)

Total Over 137.0
DraftKings: -190 (1.53)

Total Under 137.0
DraftKings: +155 (2.55)

Total Over 137.5
DraftKings: -177 (1.57)
BetMGM: -165 (1.6)

Total Under 137.5
DraftKings: +145 (2.45)
BetMGM: +140 (2.4)

Total Over 138.0
DraftKings: -167 (1.6)
FoxBet: -154 (1.65)

Total Under 138.0
DraftKings: +138 (2.38)
FoxBet: +120 (2.2)

Total Over 141.5
DraftKings: -113 (1.89)
FoxBet: -110 (1.9091)
BetMGM: -115 (1.87)

Total Under 141.5
DraftKings: -107 (1.94)
FoxBet: -110 (1.9091)
BetMGM: -105 (1.95)

Total Over 146.0
DraftKings: +145 (2.45)

Total Under 146.0
DraftKings: -177 (1.57)

Total Over 146.5
DraftKings: +150 (2.5)
BetMGM: +135 (2.35)

Total Under 146.5
DraftKings: -182 (1.55)
BetMGM: -165 (1.6)

Total Over 147.0
DraftKings: +163 (2.63)

Total Under 147.0
DraftKings: -200 (1.5)

Total Under 143.5
FoxBet: -137 (1.7273)
BetMGM: -125 (1.8)

Total Over 143.5
FoxBet: +105 (2.05)
BetMGM: +105 (2.05)

Total Under 138.5
FoxBet: +115 (2.15)
BetMGM: +125 (2.25)

Total Over 138.5
FoxBet: -150 (1.6667)
BetMGM: -155 (1.65)

Total Under 139.0
FoxBet: +110 (2.1)

Total Over 139.0
FoxBet: -143 (1.7)

Total Under 144.5
FoxBet: -150 (1.6667)
BetMGM: -140 (1.72)

Total Over 144.5
FoxBet: +115 (2.15)
BetMGM: +115 (2.15)

Total Under 140.5
FoxBet: -105 (1.95)
BetMGM: +105 (2.05)

Total Over 140.5
FoxBet: -125 (1.8)
BetMGM: -130 (1.78)

Total Under 142.5
FoxBet: -125 (1.8)
BetMGM: -110 (1.91)

Total Over 142.5
FoxBet: -105 (1.95)
BetMGM: -110 (1.91)

Total Under 140.0
FoxBet: +100 (2.0)

Total Over 140.0
FoxBet: -133 (1.75)

Total Over 141.0
FoxBet: -125 (1.8)

Total Under 141.0
FoxBet: -105 (1.95)

Total Under 143.0
FoxBet: -133 (1.75)

Total Over 143.0
FoxBet: +100 (2.0)

Total Under 144.0
FoxBet: -143 (1.7)

Total Over 144.0
FoxBet: +110 (2.1)

Total Under 145.0
FoxBet: -154 (1.65)

Total Over 145.0
FoxBet: +120 (2.2)

Total Under 139.5
FoxBet: +105 (2.05)
BetMGM: +115 (2.15)

Total Over 139.5
FoxBet: -137 (1.7273)
BetMGM: -140 (1.72)

Total Over 134.5
BetMGM: -225 (1.44)

Total Under 134.5
BetMGM: +185 (2.85)

Total Over 135.5
BetMGM: -200 (1.48)

Total Under 135.5
BetMGM: +170 (2.7)

Total Over 136.5
BetMGM: -190 (1.53)

Total Under 136.5
BetMGM: +155 (2.55)

Total Over 145.5
BetMGM: +125 (2.25)

Total Under 145.5
BetMGM: -150 (1.67)

Total Over 147.5
BetMGM: +150 (2.5)

Total Under 147.5
BetMGM: -185 (1.55)

Total Over 148.5
BetMGM: +165 (2.65)

Total Under 148.5
BetMGM: -200 (1.5)

Total Over 149.5
BetMGM: +180 (2.8)

Total Under 149.5
BetMGM: -225 (1.45)

Total Over 150.5
BetMGM: +195 (2.95)

Total Under 150.5
BetMGM: -250 (1.42)

Total Over 151.5
BetMGM: +220 (3.2)

Total Under 151.5
BetMGM: -275 (1.36)

Total Over 152.5
BetMGM: +230 (3.3)

Total Under 152.5
BetMGM: -300 (1.34)

Total Over 153.5
BetMGM: +260 (3.6)

Total Under 153.5
BetMGM: -350 (1.3)
Moneyline
Moneyline Cincinnati None
DraftKings: +195 (2.95)
FoxBet: +200 (3.0)
BetMGM: +200 (3.0)

Moneyline SMU None
DraftKings: -245 (1.41)
FoxBet: -250 (1.4)
BetMGM: -250 (1.4)


, 'St. Francis (PA) @ LIU Sharks': College Basketball: St. Francis (PA) @ LIU Sharks
DraftKings - 179653592


, 'Sacred Heart @ Merrimack': College Basketball: Sacred Heart @ Merrimack
DraftKings - 179653590
FoxBet - 8715828
BetMGM - 10973254
Spread
Spread Sacred Heart 3.5
DraftKings: -108 (1.93)
FoxBet: -110 (1.9091)
BetMGM: -110 (1.91)

Spread Merrimack -3.5
DraftKings: -112 (1.9)
FoxBet: -110 (1.9091)
BetMGM: -110 (1.91)

Spread Sacred Heart -1.5
DraftKings: +160 (2.6)
BetMGM: +165 (2.65)

Spread Merrimack 1.5
DraftKings: -195 (1.52)
BetMGM: -200 (1.5)

Spread Sacred Heart -1.0
DraftKings: +150 (2.5)

Spread Merrimack 1.0
DraftKings: -186 (1.54)

Spread Sacred Heart -0.5
DraftKings: +143 (2.43)

Spread Merrimack 0.5
DraftKings: -175 (1.58)

Spread Sacred Heart 3.0
DraftKings: +100 (2.0)
FoxBet: -105 (1.95)

Spread Merrimack -3.0
DraftKings: -121 (1.83)
FoxBet: -125 (1.8)

Spread Sacred Heart 7.5
DraftKings: -220 (1.46)
FoxBet: -225 (1.4444)
BetMGM: -200 (1.48)

Spread Merrimack -7.5
DraftKings: +175 (2.75)
FoxBet: +165 (2.65)
BetMGM: +170 (2.7)

Spread Sacred Heart 8.0
DraftKings: -245 (1.41)

Spread Merrimack -8.0
DraftKings: +195 (2.95)

Spread Sacred Heart 8.5
DraftKings: -265 (1.38)
BetMGM: -250 (1.4)

Spread Merrimack -8.5
DraftKings: +210 (3.1)
BetMGM: +200 (3.0)

Spread Sacred Heart 1.5
FoxBet: +120 (2.2)
BetMGM: +125 (2.25)

Spread Merrimack -1.5
FoxBet: -154 (1.65)
BetMGM: -155 (1.65)

Spread Sacred Heart 0.0
FoxBet: +135 (2.35)

Spread Merrimack 0.0
FoxBet: -182 (1.55)

Spread Merrimack -5.5
FoxBet: +120 (2.2)
BetMGM: +125 (2.25)

Spread Sacred Heart 5.5
FoxBet: -154 (1.65)
BetMGM: -150 (1.67)

Spread Sacred Heart 1.0
FoxBet: +130 (2.3)

Spread Merrimack -1.0
FoxBet: -175 (1.5714)

Spread Sacred Heart 7.0
FoxBet: -212 (1.4706)

Spread Merrimack -7.0
FoxBet: +155 (2.55)

Spread Sacred Heart 6.0
FoxBet: -175 (1.5714)

Spread Merrimack -6.0
FoxBet: +130 (2.3)

Spread Sacred Heart 2.0
FoxBet: +110 (2.1)

Spread Merrimack -2.0
FoxBet: -143 (1.7)

Spread Merrimack -5.0
FoxBet: +115 (2.15)

Spread Sacred Heart 5.0
FoxBet: -150 (1.6667)

Spread Sacred Heart 4.0
FoxBet: -125 (1.8)

Spread Merrimack -4.0
FoxBet: -105 (1.95)

Spread Sacred Heart 2.5
FoxBet: +105 (2.05)
BetMGM: +110 (2.1)

Spread Merrimack -2.5
FoxBet: -137 (1.7273)
BetMGM: -135 (1.75)

Spread Merrimack -4.5
FoxBet: +105 (2.05)
BetMGM: +105 (2.05)

Spread Sacred Heart 4.5
FoxBet: -137 (1.7273)
BetMGM: -130 (1.78)

Spread Merrimack -6.5
FoxBet: +140 (2.4)
BetMGM: +150 (2.5)

Spread Sacred Heart 6.5
FoxBet: -188 (1.5333)
BetMGM: -185 (1.55)

Spread Sacred Heart 13.5
BetMGM: -650 (1.16)

Spread Merrimack -13.5
BetMGM: +450 (5.5)

Spread Sacred Heart 12.5
BetMGM: -500 (1.2)

Spread Merrimack -12.5
BetMGM: +375 (4.75)

Spread Sacred Heart 11.5
BetMGM: -450 (1.22)

Spread Merrimack -11.5
BetMGM: +333 (4.33)

Spread Sacred Heart 10.5
BetMGM: -350 (1.28)

Spread Merrimack -10.5
BetMGM: +280 (3.8)

Spread Sacred Heart 9.5
BetMGM: -300 (1.33)

Spread Merrimack -9.5
BetMGM: +240 (3.4)

Spread Sacred Heart -2.5
BetMGM: +195 (2.95)

Spread Merrimack 2.5
BetMGM: -250 (1.42)

Spread Sacred Heart -3.5
BetMGM: +225 (3.25)

Spread Merrimack 3.5
BetMGM: -275 (1.35)

Spread Sacred Heart -4.5
BetMGM: +260 (3.6)

Spread Merrimack 4.5
BetMGM: -350 (1.3)

Spread Sacred Heart -5.5
BetMGM: +310 (4.1)

Spread Merrimack 5.5
BetMGM: -400 (1.25)

Spread Sacred Heart -6.5
BetMGM: +375 (4.75)

Spread Merrimack 6.5
BetMGM: -500 (1.2)

Spread Sacred Heart -7.5
BetMGM: +450 (5.5)

Spread Merrimack 7.5
BetMGM: -650 (1.16)
Total
Total Over 129.5
DraftKings: -107 (1.94)
FoxBet: -105 (1.95)
BetMGM: -110 (1.91)

Total Under 129.5
DraftKings: -113 (1.89)
FoxBet: -125 (1.8)
BetMGM: -110 (1.91)

Total Over 124.5
DraftKings: -190 (1.53)
BetMGM: -185 (1.55)

Total Under 124.5
DraftKings: +155 (2.55)
BetMGM: +150 (2.5)

Total Over 125.0
DraftKings: -180 (1.56)

Total Under 125.0
DraftKings: +148 (2.48)

Total Over 125.5
DraftKings: -167 (1.6)
FoxBet: -154 (1.65)
BetMGM: -160 (1.62)

Total Under 125.5
DraftKings: +138 (2.38)
FoxBet: +120 (2.2)
BetMGM: +135 (2.35)

Total Over 129.0
DraftKings: -113 (1.89)
FoxBet: -110 (1.9091)

Total Under 129.0
DraftKings: -107 (1.94)
FoxBet: -125 (1.8)

Total Over 133.5
DraftKings: +145 (2.45)
BetMGM: +135 (2.35)

Total Under 133.5
DraftKings: -177 (1.57)
BetMGM: -160 (1.62)

Total Over 134.0
DraftKings: +155 (2.55)

Total Under 134.0
DraftKings: -190 (1.53)

Total Over 134.5
DraftKings: +160 (2.6)
BetMGM: +145 (2.45)

Total Under 134.5
DraftKings: -195 (1.52)
BetMGM: -175 (1.57)

Total Over 132.5
FoxBet: +125 (2.25)
BetMGM: +120 (2.2)

Total Under 132.5
FoxBet: -162 (1.6154)
BetMGM: -145 (1.7)

Total Under 130.5
FoxBet: -137 (1.7273)
BetMGM: -120 (1.83)

Total Over 130.5
FoxBet: +105 (2.05)
BetMGM: +100 (2.0)

Total Under 127.5
FoxBet: +100 (2.0)
BetMGM: +110 (2.1)

Total Over 127.5
FoxBet: -133 (1.75)
BetMGM: -135 (1.75)

Total Under 126.0
FoxBet: +115 (2.15)

Total Over 126.0
FoxBet: -150 (1.6667)

Total Over 127.0
FoxBet: -137 (1.7273)

Total Under 127.0
FoxBet: +105 (2.05)

Total Over 128.0
FoxBet: -125 (1.8)

Total Under 128.0
FoxBet: -105 (1.95)

Total Under 131.5
FoxBet: -143 (1.7)
BetMGM: -135 (1.75)

Total Over 131.5
FoxBet: +110 (2.1)
BetMGM: +110 (2.1)

Total Under 130.0
FoxBet: -133 (1.75)

Total Over 130.0
FoxBet: +100 (2.0)

Total Under 131.0
FoxBet: -137 (1.7273)

Total Over 131.0
FoxBet: +105 (2.05)

Total Over 132.0
FoxBet: +120 (2.2)

Total Under 132.0
FoxBet: -154 (1.65)

Total Over 126.5
FoxBet: -143 (1.7)
BetMGM: -150 (1.67)

Total Under 126.5
FoxBet: +110 (2.1)
BetMGM: +125 (2.25)

Total Under 128.5
FoxBet: -105 (1.95)
BetMGM: +100 (2.0)

Total Over 128.5
FoxBet: -125 (1.8)
BetMGM: -120 (1.83)

Total Over 120.5
BetMGM: -275 (1.36)

Total Under 120.5
BetMGM: +220 (3.2)

Total Over 121.5
BetMGM: -250 (1.4)

Total Under 121.5
BetMGM: +200 (3.0)

Total Over 122.5
BetMGM: -225 (1.45)

Total Under 122.5
BetMGM: +180 (2.8)

Total Over 123.5
BetMGM: -200 (1.5)

Total Under 123.5
BetMGM: +165 (2.65)

Total Over 135.5
BetMGM: +165 (2.65)

Total Under 135.5
BetMGM: -200 (1.5)

Total Over 136.5
BetMGM: +180 (2.8)

Total Under 136.5
BetMGM: -225 (1.45)

Total Over 137.5
BetMGM: +195 (2.95)

Total Under 137.5
BetMGM: -250 (1.42)

Total Over 138.5
BetMGM: +220 (3.2)

Total Under 138.5
BetMGM: -275 (1.36)

Total Over 139.5
BetMGM: +230 (3.3)

Total Under 139.5
BetMGM: -300 (1.34)
Moneyline
Moneyline Sacred Heart None
DraftKings: +143 (2.43)
FoxBet: +140 (2.4)
BetMGM: +145 (2.45)

Moneyline Merrimack None
DraftKings: -175 (1.58)
FoxBet: -175 (1.5714)
BetMGM: -175 (1.57)


, 'Iowa @ Maryland': College Basketball: Iowa @ Maryland
DraftKings - 179653593
FanDuel - 949357.3
BetMGM - 10973249
Spread
Spread Iowa -5.5
DraftKings: -110 (1.91)
FanDuel: -106 (1.9434)
BetMGM: -105 (1.95)

Spread Maryland 5.5
DraftKings: -109 (1.92)
FanDuel: -114 (1.8772)
BetMGM: -115 (1.87)

Spread Iowa -10.5
DraftKings: +200 (3.0)
BetMGM: +220 (3.2)

Spread Maryland 10.5
DraftKings: -250 (1.4)
BetMGM: -275 (1.36)

Spread Iowa -10.0
DraftKings: +190 (2.9)

Spread Maryland 10.0
DraftKings: -235 (1.43)

Spread Iowa -9.5
DraftKings: +175 (2.75)
BetMGM: +185 (2.85)

Spread Maryland 9.5
DraftKings: -215 (1.47)
BetMGM: -225 (1.44)

Spread Iowa -6.0
DraftKings: -103 (1.98)

Spread Maryland 6.0
DraftKings: -120 (1.84)

Spread Iowa -1.5
DraftKings: -215 (1.47)
BetMGM: -200 (1.5)

Spread Maryland 1.5
DraftKings: +175 (2.75)
BetMGM: +165 (2.65)

Spread Iowa -1.0
DraftKings: -235 (1.43)

Spread Maryland 1.0
DraftKings: +188 (2.88)

Spread Iowa -0.5
DraftKings: -240 (1.42)

Spread Maryland 0.5
DraftKings: +190 (2.9)

Spread Iowa 5.5
BetMGM: -500 (1.2)

Spread Maryland -5.5
BetMGM: +375 (4.75)

Spread Iowa 4.5
BetMGM: -450 (1.22)

Spread Maryland -4.5
BetMGM: +333 (4.33)

Spread Iowa 3.5
BetMGM: -350 (1.28)

Spread Maryland -3.5
BetMGM: +280 (3.8)

Spread Iowa 2.5
BetMGM: -300 (1.33)

Spread Maryland -2.5
BetMGM: +240 (3.4)

Spread Iowa 1.5
BetMGM: -275 (1.36)

Spread Maryland -1.5
BetMGM: +220 (3.2)

Spread Iowa -2.5
BetMGM: -165 (1.6)

Spread Maryland 2.5
BetMGM: +140 (2.4)

Spread Iowa -3.5
BetMGM: -145 (1.7)

Spread Maryland 3.5
BetMGM: +120 (2.2)

Spread Iowa -4.5
BetMGM: -120 (1.83)

Spread Maryland 4.5
BetMGM: +100 (2.0)

Spread Iowa -6.5
BetMGM: +115 (2.15)

Spread Maryland 6.5
BetMGM: -140 (1.72)

Spread Iowa -7.5
BetMGM: +135 (2.35)

Spread Maryland 7.5
BetMGM: -165 (1.6)

Spread Iowa -8.5
BetMGM: +155 (2.55)

Spread Maryland 8.5
BetMGM: -190 (1.53)

Spread Iowa -11.5
BetMGM: +260 (3.6)

Spread Maryland 11.5
BetMGM: -350 (1.3)

Spread Iowa -12.5
BetMGM: +290 (3.9)

Spread Maryland 12.5
BetMGM: -375 (1.26)

Spread Iowa -13.5
BetMGM: +340 (4.4)

Spread Maryland 13.5
BetMGM: -450 (1.22)

Spread Iowa -14.5
BetMGM: +400 (5.0)

Spread Maryland 14.5
BetMGM: -550 (1.18)

Spread Iowa -15.5
BetMGM: +475 (5.75)

Spread Maryland 15.5
BetMGM: -650 (1.15)
Total
Total Over 152.0
DraftKings: -108 (1.93)

Total Under 152.0
DraftKings: -112 (1.9)

Total Over 147.0
DraftKings: -190 (1.53)

Total Under 147.0
DraftKings: +155 (2.55)

Total Over 147.5
DraftKings: -177 (1.57)
BetMGM: -175 (1.57)

Total Under 147.5
DraftKings: +145 (2.45)
BetMGM: +145 (2.45)

Total Over 148.0
DraftKings: -167 (1.6)

Total Under 148.0
DraftKings: +138 (2.38)

Total Over 151.5
DraftKings: -114 (1.88)
FanDuel: -114 (1.8772)
BetMGM: -120 (1.83)

Total Under 151.5
DraftKings: -107 (1.94)
FanDuel: -106 (1.9434)
BetMGM: +100 (2.0)

Total Over 156.0
DraftKings: +143 (2.43)

Total Under 156.0
DraftKings: -175 (1.58)

Total Over 156.5
DraftKings: +150 (2.5)
BetMGM: +125 (2.25)

Total Under 156.5
DraftKings: -182 (1.55)
BetMGM: -155 (1.65)

Total Over 157.0
DraftKings: +155 (2.55)

Total Under 157.0
DraftKings: -195 (1.52)

Total Over 145.5
BetMGM: -225 (1.45)

Total Under 145.5
BetMGM: +180 (2.8)

Total Over 146.5
BetMGM: -200 (1.5)

Total Under 146.5
BetMGM: +165 (2.65)

Total Over 148.5
BetMGM: -160 (1.62)

Total Under 148.5
BetMGM: +135 (2.35)

Total Over 149.5
BetMGM: -150 (1.67)

Total Under 149.5
BetMGM: +125 (2.25)

Total Over 150.5
BetMGM: -135 (1.75)

Total Under 150.5
BetMGM: +110 (2.1)

Total Over 152.5
BetMGM: -110 (1.91)

Total Under 152.5
BetMGM: -110 (1.91)

Total Over 153.5
BetMGM: +100 (2.0)

Total Under 153.5
BetMGM: -120 (1.83)

Total Over 154.5
BetMGM: +105 (2.05)

Total Under 154.5
BetMGM: -130 (1.78)

Total Over 155.5
BetMGM: +120 (2.2)

Total Under 155.5
BetMGM: -145 (1.7)

Total Over 157.5
BetMGM: +140 (2.4)

Total Under 157.5
BetMGM: -165 (1.6)

Total Over 158.5
BetMGM: +155 (2.55)

Total Under 158.5
BetMGM: -190 (1.53)

Total Over 159.5
BetMGM: +170 (2.7)

Total Under 159.5
BetMGM: -200 (1.48)

Total Over 160.5
BetMGM: +180 (2.8)

Total Under 160.5
BetMGM: -225 (1.45)

Total Over 161.5
BetMGM: +200 (3.0)

Total Under 161.5
BetMGM: -250 (1.4)

Total Over 162.5
BetMGM: +220 (3.2)

Total Under 162.5
BetMGM: -275 (1.36)

Total Over 163.5
BetMGM: +240 (3.4)

Total Under 163.5
BetMGM: -300 (1.33)

Total Over 164.5
BetMGM: +260 (3.6)

Total Under 164.5
BetMGM: -350 (1.3)
Moneyline
Moneyline Iowa None
DraftKings: -240 (1.42)
FanDuel: -240 (1.4167)
BetMGM: -225 (1.44)

Moneyline Maryland None
DraftKings: +195 (2.95)
FanDuel: +198 (2.98)
BetMGM: +185 (2.85)


, 'Idaho State @ Northern Arizona': College Basketball: Idaho State @ Northern Arizona
DraftKings - 179653606
FanDuel - 949365.3
FoxBet - 8715800
BetMGM - 10973262
Spread
Spread Idaho State 2.5
DraftKings: -109 (1.92)
FanDuel: -106 (1.9434)
FoxBet: +100 (2.0)
BetMGM: -110 (1.91)

Spread Northern Arizona -2.5
DraftKings: -112 (1.9)
FanDuel: -114 (1.8772)
FoxBet: -118 (1.85)
BetMGM: -110 (1.91)

Spread Idaho State -2.5
DraftKings: +155 (2.55)
BetMGM: +165 (2.65)

Spread Northern Arizona 2.5
DraftKings: -190 (1.53)
BetMGM: -200 (1.5)

Spread Idaho State -2.0
DraftKings: +148 (2.48)

Spread Northern Arizona 2.0
DraftKings: -180 (1.56)

Spread Idaho State -1.5
DraftKings: +135 (2.35)
FoxBet: +135 (2.35)
BetMGM: +140 (2.4)

Spread Northern Arizona 1.5
DraftKings: -165 (1.61)
FoxBet: -182 (1.55)
BetMGM: -165 (1.6)

Spread Idaho State 2.0
DraftKings: +100 (2.0)
FoxBet: +100 (2.0)

Spread Northern Arizona -2.0
DraftKings: -120 (1.84)
FoxBet: -133 (1.75)

Spread Idaho State 6.5
DraftKings: -220 (1.46)
FoxBet: -212 (1.4706)
BetMGM: -200 (1.48)

Spread Northern Arizona -6.5
DraftKings: +175 (2.75)
FoxBet: +155 (2.55)
BetMGM: +170 (2.7)

Spread Idaho State 7.0
DraftKings: -245 (1.41)

Spread Northern Arizona -7.0
DraftKings: +195 (2.95)

Spread Idaho State 7.5
DraftKings: -265 (1.38)
BetMGM: -250 (1.4)

Spread Northern Arizona -7.5
DraftKings: +205 (3.05)
BetMGM: +200 (3.0)

Spread Idaho State 1.5
FoxBet: +105 (2.05)
BetMGM: +105 (2.05)

Spread Northern Arizona -1.5
FoxBet: -137 (1.7273)
BetMGM: -130 (1.78)

Spread Northern Arizona 1.0
FoxBet: -175 (1.5714)

Spread Idaho State -1.0
FoxBet: +130 (2.3)

Spread Northern Arizona -3.5
FoxBet: +100 (2.0)
BetMGM: +105 (2.05)

Spread Idaho State 3.5
FoxBet: -133 (1.75)
BetMGM: -130 (1.78)

Spread Northern Arizona 0.0
FoxBet: -154 (1.65)

Spread Idaho State 0.0
FoxBet: +120 (2.2)

Spread Northern Arizona -5.5
FoxBet: +135 (2.35)
BetMGM: +145 (2.45)

Spread Idaho State 5.5
FoxBet: -182 (1.55)
BetMGM: -175 (1.57)

Spread Idaho State 1.0
FoxBet: +110 (2.1)

Spread Northern Arizona -1.0
FoxBet: -143 (1.7)

Spread Idaho State 6.0
FoxBet: -200 (1.5)

Spread Northern Arizona -6.0
FoxBet: +155 (2.55)

Spread Idaho State 3.0
FoxBet: -125 (1.8)

Spread Northern Arizona -3.0
FoxBet: -105 (1.95)

Spread Northern Arizona -5.0
FoxBet: +130 (2.3)

Spread Idaho State 5.0
FoxBet: -175 (1.5714)

Spread Northern Arizona -4.0
FoxBet: +110 (2.1)

Spread Idaho State 4.0
FoxBet: -143 (1.7)

Spread Idaho State 4.5
FoxBet: -150 (1.6667)
BetMGM: -150 (1.67)

Spread Northern Arizona -4.5
FoxBet: +115 (2.15)
BetMGM: +125 (2.25)

Spread Idaho State 11.5
BetMGM: -500 (1.2)

Spread Northern Arizona -11.5
BetMGM: +375 (4.75)

Spread Idaho State 10.5
BetMGM: -450 (1.22)

Spread Northern Arizona -10.5
BetMGM: +333 (4.33)

Spread Idaho State 9.5
BetMGM: -350 (1.28)

Spread Northern Arizona -9.5
BetMGM: +280 (3.8)

Spread Idaho State 8.5
BetMGM: -300 (1.33)

Spread Northern Arizona -8.5
BetMGM: +240 (3.4)

Spread Idaho State -3.5
BetMGM: +190 (2.9)

Spread Northern Arizona 3.5
BetMGM: -250 (1.42)

Spread Idaho State -4.5
BetMGM: +225 (3.25)

Spread Northern Arizona 4.5
BetMGM: -275 (1.36)

Spread Idaho State -5.5
BetMGM: +260 (3.6)

Spread Northern Arizona 5.5
BetMGM: -350 (1.3)

Spread Idaho State -6.5
BetMGM: +310 (4.1)

Spread Northern Arizona 6.5
BetMGM: -400 (1.25)

Spread Idaho State -7.5
BetMGM: +375 (4.75)

Spread Northern Arizona 7.5
BetMGM: -500 (1.2)

Spread Idaho State -8.5
BetMGM: +425 (5.25)

Spread Northern Arizona 8.5
BetMGM: -600 (1.17)

Spread Idaho State -9.5
BetMGM: +500 (6.0)

Spread Northern Arizona 9.5
BetMGM: -700 (1.14)
Total
Total Over 132.5
DraftKings: -108 (1.93)
FoxBet: -125 (1.8)
BetMGM: -110 (1.91)

Total Under 132.5
DraftKings: -112 (1.9)
FoxBet: -105 (1.95)
BetMGM: -110 (1.91)

Total Over 127.5
DraftKings: -190 (1.53)
BetMGM: -185 (1.55)

Total Under 127.5
DraftKings: +155 (2.55)
BetMGM: +150 (2.5)

Total Over 128.0
DraftKings: -182 (1.55)

Total Under 128.0
DraftKings: +148 (2.48)

Total Over 128.5
DraftKings: -167 (1.6)
BetMGM: -160 (1.62)

Total Under 128.5
DraftKings: +138 (2.38)
BetMGM: +135 (2.35)

Total Over 132.0
DraftKings: -114 (1.88)
FoxBet: -133 (1.75)

Total Under 132.0
DraftKings: -106 (1.95)
FoxBet: +100 (2.0)

Total Over 136.5
DraftKings: +143 (2.43)
FoxBet: +120 (2.2)
BetMGM: +135 (2.35)

Total Under 136.5
DraftKings: -175 (1.58)
FoxBet: -154 (1.65)
BetMGM: -160 (1.62)

Total Over 137.0
DraftKings: +150 (2.5)

Total Under 137.0
DraftKings: -186 (1.54)

Total Over 137.5
DraftKings: +155 (2.55)
BetMGM: +145 (2.45)

Total Under 137.5
DraftKings: -195 (1.52)
BetMGM: -175 (1.57)

Total Over 133.5
FanDuel: -106 (1.9434)
FoxBet: -105 (1.95)
BetMGM: +100 (2.0)

Total Under 133.5
FanDuel: -114 (1.8772)
FoxBet: -125 (1.8)
BetMGM: -120 (1.83)

Total Over 134.5
FoxBet: +100 (2.0)
BetMGM: +110 (2.1)

Total Under 134.5
FoxBet: -133 (1.75)
BetMGM: -135 (1.75)

Total Over 130.5
FoxBet: -143 (1.7)
BetMGM: -135 (1.75)

Total Under 130.5
FoxBet: +110 (2.1)
BetMGM: +110 (2.1)

Total Under 129.5
FoxBet: +120 (2.2)
BetMGM: +125 (2.25)

Total Over 129.5
FoxBet: -154 (1.65)
BetMGM: -150 (1.67)

Total Over 135.5
FoxBet: +110 (2.1)
BetMGM: +120 (2.2)

Total Under 135.5
FoxBet: -143 (1.7)
BetMGM: -145 (1.7)

Total Under 131.5
FoxBet: +105 (2.05)
BetMGM: +100 (2.0)

Total Over 131.5
FoxBet: -137 (1.7273)
BetMGM: -120 (1.83)

Total Under 130.0
FoxBet: +115 (2.15)

Total Over 130.0
FoxBet: -150 (1.6667)

Total Over 131.0
FoxBet: -137 (1.7273)

Total Under 131.0
FoxBet: +105 (2.05)

Total Under 133.0
FoxBet: -110 (1.9091)

Total Over 133.0
FoxBet: -110 (1.9091)

Total Over 134.0
FoxBet: -105 (1.95)

Total Under 134.0
FoxBet: -125 (1.8)

Total Under 135.0
FoxBet: -137 (1.7273)

Total Over 135.0
FoxBet: +105 (2.05)

Total Over 136.0
FoxBet: +115 (2.15)

Total Under 136.0
FoxBet: -150 (1.6667)

Total Over 123.5
BetMGM: -275 (1.36)

Total Under 123.5
BetMGM: +220 (3.2)

Total Over 124.5
BetMGM: -250 (1.4)

Total Under 124.5
BetMGM: +200 (3.0)

Total Over 125.5
BetMGM: -225 (1.45)

Total Under 125.5
BetMGM: +180 (2.8)

Total Over 126.5
BetMGM: -200 (1.5)

Total Under 126.5
BetMGM: +165 (2.65)

Total Over 138.5
BetMGM: +155 (2.55)

Total Under 138.5
BetMGM: -190 (1.53)

Total Over 139.5
BetMGM: +180 (2.8)

Total Under 139.5
BetMGM: -225 (1.45)

Total Over 140.5
BetMGM: +190 (2.9)

Total Under 140.5
BetMGM: -250 (1.42)

Total Over 141.5
BetMGM: +220 (3.2)

Total Under 141.5
BetMGM: -275 (1.36)

Total Over 142.5
BetMGM: +230 (3.3)

Total Under 142.5
BetMGM: -300 (1.34)
Moneyline
Moneyline Idaho State None
DraftKings: +120 (2.2)
FanDuel: +128 (2.28)
FoxBet: +125 (2.25)
BetMGM: +125 (2.25)

Moneyline Northern Arizona None
DraftKings: -148 (1.68)
FanDuel: -152 (1.6579)
FoxBet: -154 (1.65)
BetMGM: -150 (1.67)


, 'UNLV @ Colorado State': College Basketball: UNLV @ Colorado State
DraftKings - 179653601
FanDuel - 949364.3
FoxBet - 8715802
BetMGM - 10973259
Spread
Spread UNLV 10.0
DraftKings: -110 (1.91)
FoxBet: -125 (1.8)

Spread Colorado State -10.0
DraftKings: -110 (1.91)
FoxBet: -110 (1.9091)

Spread UNLV 5.0
DraftKings: +210 (3.1)
FoxBet: +155 (2.55)

Spread Colorado State -5.0
DraftKings: -265 (1.38)
FoxBet: -212 (1.4706)

Spread UNLV 5.5
DraftKings: +188 (2.88)
FoxBet: +140 (2.4)
BetMGM: +190 (2.9)

Spread Colorado State -5.5
DraftKings: -235 (1.43)
FoxBet: -188 (1.5333)
BetMGM: -250 (1.42)

Spread UNLV 6.0
DraftKings: +175 (2.75)

Spread Colorado State -6.0
DraftKings: -220 (1.46)

Spread UNLV 9.5
DraftKings: -103 (1.98)
FanDuel: -104 (1.9615)
FoxBet: -105 (1.95)
BetMGM: +100 (2.0)

Spread Colorado State -9.5
DraftKings: -118 (1.85)
FanDuel: -118 (1.8475)
FoxBet: -125 (1.8)
BetMGM: -120 (1.83)

Spread UNLV 14.0
DraftKings: -210 (1.48)

Spread Colorado State -14.0
DraftKings: +170 (2.7)

Spread UNLV 14.5
DraftKings: -225 (1.45)
BetMGM: -225 (1.45)

Spread Colorado State -14.5
DraftKings: +180 (2.8)
BetMGM: +180 (2.8)

Spread UNLV 15.0
DraftKings: -250 (1.4)

Spread Colorado State -15.0
DraftKings: +200 (3.0)

Spread Colorado State -12.0
FoxBet: +125 (2.25)

Spread UNLV 12.0
FoxBet: -162 (1.6154)

Spread UNLV 11.0
FoxBet: -137 (1.7273)

Spread Colorado State -11.0
FoxBet: +105 (2.05)

Spread UNLV 13.0
FoxBet: -200 (1.5)

Spread Colorado State -13.0
FoxBet: +145 (2.45)

Spread Colorado State -11.5
FoxBet: +110 (2.1)
BetMGM: +115 (2.15)

Spread UNLV 11.5
FoxBet: -143 (1.7)
BetMGM: -140 (1.72)

Spread Colorado State -7.5
FoxBet: -175 (1.5714)
BetMGM: -165 (1.6)

Spread UNLV 7.5
FoxBet: +130 (2.3)
BetMGM: +135 (2.35)

Spread UNLV 13.5
FoxBet: -212 (1.4706)
BetMGM: -190 (1.53)

Spread Colorado State -13.5
FoxBet: +155 (2.55)
BetMGM: +155 (2.55)

Spread Colorado State -7.0
FoxBet: -188 (1.5333)

Spread UNLV 7.0
FoxBet: +140 (2.4)

Spread UNLV 9.0
FoxBet: +105 (2.05)

Spread Colorado State -9.0
FoxBet: -137 (1.7273)

Spread UNLV 8.0
FoxBet: +120 (2.2)

Spread Colorado State -8.0
FoxBet: -154 (1.65)

Spread Colorado State -10.5
FoxBet: +100 (2.0)
BetMGM: -105 (1.95)

Spread UNLV 10.5
FoxBet: -133 (1.75)
BetMGM: -115 (1.87)

Spread Colorado State -8.5
FoxBet: -143 (1.7)
BetMGM: -140 (1.72)

Spread UNLV 8.5
FoxBet: +110 (2.1)
BetMGM: +115 (2.15)

Spread UNLV 12.5
FoxBet: -175 (1.5714)
BetMGM: -160 (1.62)

Spread Colorado State -12.5
FoxBet: +130 (2.3)
BetMGM: +135 (2.35)

Spread UNLV 6.5
FoxBet: +145 (2.45)
BetMGM: +165 (2.65)

Spread Colorado State -6.5
FoxBet: -200 (1.5)
BetMGM: -200 (1.5)

Spread UNLV 18.5
BetMGM: -400 (1.25)

Spread Colorado State -18.5
BetMGM: +310 (4.1)

Spread UNLV 17.5
BetMGM: -350 (1.28)

Spread Colorado State -17.5
BetMGM: +275 (3.75)

Spread UNLV 16.5
BetMGM: -300 (1.33)

Spread Colorado State -16.5
BetMGM: +240 (3.4)

Spread UNLV 15.5
BetMGM: -250 (1.4)

Spread Colorado State -15.5
BetMGM: +200 (3.0)

Spread UNLV 4.5
BetMGM: +225 (3.25)

Spread Colorado State -4.5
BetMGM: -275 (1.35)

Spread UNLV 3.5
BetMGM: +260 (3.6)

Spread Colorado State -3.5
BetMGM: -350 (1.3)

Spread UNLV 2.5
BetMGM: +310 (4.1)

Spread Colorado State -2.5
BetMGM: -400 (1.25)

Spread UNLV 1.5
BetMGM: +375 (4.75)

Spread Colorado State -1.5
BetMGM: -500 (1.2)

Spread UNLV -1.5
BetMGM: +475 (5.75)

Spread Colorado State 1.5
BetMGM: -650 (1.15)

Spread UNLV -2.5
BetMGM: +550 (6.5)

Spread Colorado State 2.5
BetMGM: -800 (1.12)
Total
Total Over 146.0
DraftKings: -113 (1.89)
FoxBet: -125 (1.8)

Total Under 146.0
DraftKings: -107 (1.94)
FoxBet: -105 (1.95)

Total Over 141.5
DraftKings: -186 (1.54)
BetMGM: -190 (1.53)

Total Under 141.5
DraftKings: +150 (2.5)
BetMGM: +155 (2.55)

Total Over 142.0
DraftKings: -180 (1.56)

Total Under 142.0
DraftKings: +148 (2.48)

Total Over 142.5
DraftKings: -167 (1.6)
BetMGM: -175 (1.57)

Total Under 142.5
DraftKings: +138 (2.38)
BetMGM: +145 (2.45)

Total Over 146.5
DraftKings: -107 (1.94)
FanDuel: -110 (1.9091)
FoxBet: -110 (1.9091)
BetMGM: -120 (1.83)

Total Under 146.5
DraftKings: -113 (1.89)
FanDuel: -110 (1.9091)
FoxBet: -110 (1.9091)
BetMGM: +100 (2.0)

Total Over 150.5
DraftKings: +143 (2.43)
BetMGM: +120 (2.2)

Total Under 150.5
DraftKings: -177 (1.57)
BetMGM: -145 (1.7)

Total Over 151.0
DraftKings: +150 (2.5)

Total Under 151.0
DraftKings: -186 (1.54)

Total Over 151.5
DraftKings: +160 (2.6)
BetMGM: +135 (2.35)

Total Under 151.5
DraftKings: -195 (1.52)
BetMGM: -160 (1.62)

Total Under 150.0
FoxBet: -154 (1.65)

Total Over 150.0
FoxBet: +120 (2.2)

Total Under 148.5
FoxBet: -137 (1.7273)
BetMGM: -120 (1.83)

Total Over 148.5
FoxBet: +105 (2.05)
BetMGM: +100 (2.0)

Total Under 143.5
FoxBet: +115 (2.15)
BetMGM: +135 (2.35)

Total Over 143.5
FoxBet: -150 (1.6667)
BetMGM: -160 (1.62)

Total Under 145.5
FoxBet: +100 (2.0)
BetMGM: +110 (2.1)

Total Over 145.5
FoxBet: -133 (1.75)
BetMGM: -135 (1.75)

Total Under 147.5
FoxBet: -125 (1.8)
BetMGM: -110 (1.91)

Total Over 147.5
FoxBet: -105 (1.95)
BetMGM: -110 (1.91)

Total Under 149.5
FoxBet: -150 (1.6667)
BetMGM: -135 (1.75)

Total Over 149.5
FoxBet: +115 (2.15)
BetMGM: +110 (2.1)

Total Over 144.5
FoxBet: -137 (1.7273)
BetMGM: -145 (1.7)

Total Under 144.5
FoxBet: +105 (2.05)
BetMGM: +120 (2.2)

Total Under 143.0
FoxBet: +125 (2.25)

Total Over 143.0
FoxBet: -162 (1.6154)

Total Under 144.0
FoxBet: +110 (2.1)

Total Over 144.0
FoxBet: -143 (1.7)

Total Over 145.0
FoxBet: -137 (1.7273)

Total Under 145.0
FoxBet: +105 (2.05)

Total Under 147.0
FoxBet: -125 (1.8)

Total Over 147.0
FoxBet: -105 (1.95)

Total Under 148.0
FoxBet: -133 (1.75)

Total Over 148.0
FoxBet: +100 (2.0)

Total Under 149.0
FoxBet: -143 (1.7)

Total Over 149.0
FoxBet: +110 (2.1)

Total Over 136.5
BetMGM: -350 (1.3)

Total Under 136.5
BetMGM: +260 (3.6)

Total Over 137.5
BetMGM: -300 (1.34)

Total Under 137.5
BetMGM: +230 (3.3)

Total Over 138.5
BetMGM: -275 (1.36)

Total Under 138.5
BetMGM: +220 (3.2)

Total Over 139.5
BetMGM: -250 (1.42)

Total Under 139.5
BetMGM: +195 (2.95)

Total Over 140.5
BetMGM: -225 (1.45)

Total Under 140.5
BetMGM: +180 (2.8)

Total Over 152.5
BetMGM: +145 (2.45)

Total Under 152.5
BetMGM: -175 (1.57)

Total Over 153.5
BetMGM: +155 (2.55)

Total Under 153.5
BetMGM: -190 (1.53)

Total Over 154.5
BetMGM: +170 (2.7)

Total Under 154.5
BetMGM: -200 (1.48)

Total Over 155.5
BetMGM: +190 (2.9)

Total Under 155.5
BetMGM: -250 (1.42)
Moneyline
Moneyline UNLV None
DraftKings: +420 (5.2)
FanDuel: +450 (5.5)
FoxBet: +425 (5.25)
BetMGM: +425 (5.25)

Moneyline Colorado State None
DraftKings: -590 (1.17)
FanDuel: -600 (1.1667)
FoxBet: -650 (1.1538)
BetMGM: -600 (1.17)


, 'Jacksonville State @ Eastern Kentucky': College Basketball: Jacksonville State @ Eastern Kentucky
DraftKings - 179653599
FanDuel - 949370.3
FoxBet - 8715808
BetMGM - 10973260
Spread
Spread Jacksonville State 3.0
DraftKings: -108 (1.93)
FoxBet: -125 (1.8)

Spread Eastern Kentucky -3.0
DraftKings: -112 (1.9)
FoxBet: -105 (1.95)

Spread Jacksonville State -2.0
DraftKings: +155 (2.55)

Spread Eastern Kentucky 2.0
DraftKings: -195 (1.52)

Spread Jacksonville State -1.5
DraftKings: +145 (2.45)
BetMGM: +150 (2.5)

Spread Eastern Kentucky 1.5
DraftKings: -177 (1.57)
BetMGM: -185 (1.55)

Spread Jacksonville State -1.0
DraftKings: +138 (2.38)
FoxBet: +130 (2.3)

Spread Eastern Kentucky 1.0
DraftKings: -167 (1.6)
FoxBet: -175 (1.5714)

Spread Jacksonville State 3.5
DraftKings: -117 (1.86)
FoxBet: -133 (1.75)
BetMGM: -115 (1.87)

Spread Eastern Kentucky -3.5
DraftKings: -104 (1.97)
FoxBet: +100 (2.0)
BetMGM: -105 (1.95)

Spread Jacksonville State 7.0
DraftKings: -220 (1.46)
FoxBet: -225 (1.4444)

Spread Eastern Kentucky -7.0
DraftKings: +175 (2.75)
FoxBet: +165 (2.65)

Spread Jacksonville State 7.5
DraftKings: -235 (1.43)
BetMGM: -225 (1.44)

Spread Eastern Kentucky -7.5
DraftKings: +188 (2.88)
BetMGM: +185 (2.85)

Spread Jacksonville State 8.0
DraftKings: -265 (1.38)

Spread Eastern Kentucky -8.0
DraftKings: +210 (3.1)

Spread Jacksonville State 2.5
FanDuel: -105 (1.9524)
FoxBet: -105 (1.95)
BetMGM: +100 (2.0)

Spread Eastern Kentucky -2.5
FanDuel: -115 (1.8696)
FoxBet: -125 (1.8)
BetMGM: -120 (1.83)

Spread Jacksonville State 1.5
FoxBet: +105 (2.05)
BetMGM: +115 (2.15)

Spread Eastern Kentucky -1.5
FoxBet: -137 (1.7273)
BetMGM: -140 (1.72)

Spread Eastern Kentucky 0.0
FoxBet: -162 (1.6154)

Spread Jacksonville State 0.0
FoxBet: +125 (2.25)

Spread Jacksonville State 5.5
FoxBet: -175 (1.5714)
BetMGM: -160 (1.62)

Spread Eastern Kentucky -5.5
FoxBet: +130 (2.3)
BetMGM: +135 (2.35)

Spread Eastern Kentucky -1.0
FoxBet: -150 (1.6667)

Spread Jacksonville State 1.0
FoxBet: +115 (2.15)

Spread Eastern Kentucky -6.0
FoxBet: +145 (2.45)

Spread Jacksonville State 6.0
FoxBet: -200 (1.5)

Spread Jacksonville State 2.0
FoxBet: +100 (2.0)

Spread Eastern Kentucky -2.0
FoxBet: -133 (1.75)

Spread Jacksonville State 5.0
FoxBet: -162 (1.6154)

Spread Eastern Kentucky -5.0
FoxBet: +125 (2.25)

Spread Eastern Kentucky -4.0
FoxBet: +105 (2.05)

Spread Jacksonville State 4.0
FoxBet: -137 (1.7273)

Spread Eastern Kentucky -4.5
FoxBet: +115 (2.15)
BetMGM: +115 (2.15)

Spread Jacksonville State 4.5
FoxBet: -150 (1.6667)
BetMGM: -140 (1.72)

Spread Jacksonville State 6.5
FoxBet: -212 (1.4706)
BetMGM: -190 (1.53)

Spread Eastern Kentucky -6.5
FoxBet: +155 (2.55)
BetMGM: +155 (2.55)

Spread Jacksonville State 15.5
BetMGM: -1000 (1.1)

Spread Eastern Kentucky -15.5
BetMGM: +625 (7.25)

Spread Jacksonville State 14.5
BetMGM: -800 (1.12)

Spread Eastern Kentucky -14.5
BetMGM: +550 (6.5)

Spread Jacksonville State 13.5
BetMGM: -650 (1.15)

Spread Eastern Kentucky -13.5
BetMGM: +475 (5.75)

Spread Jacksonville State 12.5
BetMGM: -550 (1.18)

Spread Eastern Kentucky -12.5
BetMGM: +400 (5.0)

Spread Jacksonville State 11.5
BetMGM: -450 (1.22)

Spread Eastern Kentucky -11.5
BetMGM: +350 (4.5)

Spread Jacksonville State 10.5
BetMGM: -375 (1.26)

Spread Eastern Kentucky -10.5
BetMGM: +300 (4.0)

Spread Jacksonville State 9.5
BetMGM: -350 (1.3)

Spread Eastern Kentucky -9.5
BetMGM: +260 (3.6)

Spread Jacksonville State 8.5
BetMGM: -275 (1.36)

Spread Eastern Kentucky -8.5
BetMGM: +220 (3.2)

Spread Jacksonville State -2.5
BetMGM: +180 (2.8)

Spread Eastern Kentucky 2.5
BetMGM: -225 (1.45)

Spread Jacksonville State -3.5
BetMGM: +200 (3.0)

Spread Eastern Kentucky 3.5
BetMGM: -250 (1.4)

Spread Jacksonville State -4.5
BetMGM: +240 (3.4)

Spread Eastern Kentucky 4.5
BetMGM: -300 (1.33)

Spread Jacksonville State -5.5
BetMGM: +280 (3.8)

Spread Eastern Kentucky 5.5
BetMGM: -350 (1.28)
Total
Total Over 148.0
DraftKings: -109 (1.92)
FoxBet: +100 (2.0)

Total Under 148.0
DraftKings: -112 (1.9)
FoxBet: -133 (1.75)

Total Over 143.0
DraftKings: -190 (1.53)
FoxBet: -154 (1.65)

Total Under 143.0
DraftKings: +155 (2.55)
FoxBet: +120 (2.2)

Total Over 143.5
DraftKings: -180 (1.56)
FoxBet: -150 (1.6667)
BetMGM: -160 (1.62)

Total Under 143.5
DraftKings: +145 (2.45)
FoxBet: +115 (2.15)
BetMGM: +135 (2.35)

Total Over 144.0
DraftKings: -167 (1.6)
FoxBet: -143 (1.7)

Total Under 144.0
DraftKings: +138 (2.38)
FoxBet: +110 (2.1)

Total Over 147.5
DraftKings: -114 (1.88)
FoxBet: -105 (1.95)
BetMGM: -110 (1.91)

Total Under 147.5
DraftKings: -106 (1.95)
FoxBet: -125 (1.8)
BetMGM: -110 (1.91)

Total Over 152.0
DraftKings: +143 (2.43)

Total Under 152.0
DraftKings: -175 (1.58)

Total Over 152.5
DraftKings: +148 (2.48)
BetMGM: +140 (2.4)

Total Under 152.5
DraftKings: -180 (1.56)
BetMGM: -165 (1.6)

Total Over 153.0
DraftKings: +155 (2.55)

Total Under 153.0
DraftKings: -195 (1.52)

Total Over 146.5
FanDuel: -110 (1.9091)
FoxBet: -110 (1.9091)
BetMGM: -120 (1.83)

Total Under 146.5
FanDuel: -110 (1.9091)
FoxBet: -110 (1.9091)
BetMGM: +100 (2.0)

Total Under 150.0
FoxBet: -154 (1.65)

Total Over 150.0
FoxBet: +120 (2.2)

Total Over 148.5
FoxBet: +105 (2.05)
BetMGM: +100 (2.0)

Total Under 148.5
FoxBet: -137 (1.7273)
BetMGM: -120 (1.83)

Total Over 145.5
FoxBet: -133 (1.75)
BetMGM: -135 (1.75)

Total Under 145.5
FoxBet: +100 (2.0)
BetMGM: +110 (2.1)

Total Under 149.5
FoxBet: -143 (1.7)
BetMGM: -130 (1.78)

Total Over 149.5
FoxBet: +110 (2.1)
BetMGM: +105 (2.05)

Total Under 144.5
FoxBet: +105 (2.05)
BetMGM: +125 (2.25)

Total Over 144.5
FoxBet: -137 (1.7273)
BetMGM: -150 (1.67)

Total Over 145.0
FoxBet: -137 (1.7273)

Total Under 145.0
FoxBet: +105 (2.05)

Total Under 146.0
FoxBet: -105 (1.95)

Total Over 146.0
FoxBet: -125 (1.8)

Total Under 147.0
FoxBet: -125 (1.8)

Total Over 147.0
FoxBet: -105 (1.95)

Total Under 149.0
FoxBet: -137 (1.7273)

Total Over 149.0
FoxBet: +105 (2.05)

Total Over 137.5
BetMGM: -300 (1.34)

Total Under 137.5
BetMGM: +230 (3.3)

Total Over 138.5
BetMGM: -275 (1.36)

Total Under 138.5
BetMGM: +220 (3.2)

Total Over 139.5
BetMGM: -250 (1.42)

Total Under 139.5
BetMGM: +195 (2.95)

Total Over 140.5
BetMGM: -225 (1.45)

Total Under 140.5
BetMGM: +180 (2.8)

Total Over 141.5
BetMGM: -200 (1.5)

Total Under 141.5
BetMGM: +165 (2.65)

Total Over 142.5
BetMGM: -175 (1.57)

Total Under 142.5
BetMGM: +145 (2.45)

Total Over 150.5
BetMGM: +120 (2.2)

Total Under 150.5
BetMGM: -145 (1.7)

Total Over 151.5
BetMGM: +125 (2.25)

Total Under 151.5
BetMGM: -155 (1.65)

Total Over 153.5
BetMGM: +155 (2.55)

Total Under 153.5
BetMGM: -190 (1.53)

Total Over 154.5
BetMGM: +170 (2.7)

Total Under 154.5
BetMGM: -200 (1.48)

Total Over 155.5
BetMGM: +185 (2.85)

Total Under 155.5
BetMGM: -225 (1.44)

Total Over 156.5
BetMGM: +200 (3.0)

Total Under 156.5
BetMGM: -250 (1.4)
Moneyline
Moneyline Jacksonville State None
DraftKings: +130 (2.3)
FanDuel: +132 (2.32)
FoxBet: +130 (2.3)
BetMGM: +135 (2.35)

Moneyline Eastern Kentucky None
DraftKings: -157 (1.64)
FanDuel: -154 (1.6494)
FoxBet: -162 (1.6154)
BetMGM: -160 (1.62)


, 'BYU @ Gonzaga': College Basketball: BYU @ Gonzaga
DraftKings - 179653605
BetMGM - 10989430
Spread
Spread BYU 17.5
DraftKings: -109 (1.92)
BetMGM: -115 (1.87)

Spread Gonzaga -17.5
DraftKings: -110 (1.91)
BetMGM: -105 (1.95)

Spread BYU 12.5
DraftKings: +180 (2.8)
BetMGM: +180 (2.8)

Spread Gonzaga -12.5
DraftKings: -225 (1.45)
BetMGM: -225 (1.45)

Spread BYU 13.0
DraftKings: +175 (2.75)

Spread Gonzaga -13.0
DraftKings: -215 (1.47)

Spread BYU 13.5
DraftKings: +155 (2.55)
BetMGM: +155 (2.55)

Spread Gonzaga -13.5
DraftKings: -195 (1.52)
BetMGM: -190 (1.53)

Spread BYU 18.0
DraftKings: -118 (1.85)

Spread Gonzaga -18.0
DraftKings: -103 (1.98)

Spread BYU 21.5
DraftKings: -195 (1.52)
BetMGM: -225 (1.45)

Spread Gonzaga -21.5
DraftKings: +155 (2.55)
BetMGM: +180 (2.8)

Spread BYU 22.0
DraftKings: -210 (1.48)

Spread Gonzaga -22.0
DraftKings: +170 (2.7)

Spread BYU 22.5
DraftKings: -225 (1.45)
BetMGM: -250 (1.4)

Spread Gonzaga -22.5
DraftKings: +180 (2.8)
BetMGM: +200 (3.0)

Spread BYU 27.5
BetMGM: -600 (1.17)

Spread Gonzaga -27.5
BetMGM: +425 (5.25)

Spread BYU 26.5
BetMGM: -500 (1.2)

Spread Gonzaga -26.5
BetMGM: +360 (4.6)

Spread BYU 25.5
BetMGM: -400 (1.25)

Spread Gonzaga -25.5
BetMGM: +310 (4.1)

Spread BYU 24.5
BetMGM: -350 (1.28)

Spread Gonzaga -24.5
BetMGM: +275 (3.75)

Spread BYU 23.5
BetMGM: -300 (1.34)

Spread Gonzaga -23.5
BetMGM: +230 (3.3)

Spread BYU 20.5
BetMGM: -185 (1.55)

Spread Gonzaga -20.5
BetMGM: +150 (2.5)

Spread BYU 19.5
BetMGM: -160 (1.62)

Spread Gonzaga -19.5
BetMGM: +135 (2.35)

Spread BYU 18.5
BetMGM: -140 (1.72)

Spread Gonzaga -18.5
BetMGM: +115 (2.15)

Spread BYU 16.5
BetMGM: +100 (2.0)

Spread Gonzaga -16.5
BetMGM: -120 (1.83)

Spread BYU 15.5
BetMGM: +115 (2.15)

Spread Gonzaga -15.5
BetMGM: -140 (1.72)

Spread BYU 14.5
BetMGM: +135 (2.35)

Spread Gonzaga -14.5
BetMGM: -160 (1.62)

Spread BYU 11.5
BetMGM: +200 (3.0)

Spread Gonzaga -11.5
BetMGM: -250 (1.4)

Spread BYU 10.5
BetMGM: +240 (3.4)

Spread Gonzaga -10.5
BetMGM: -300 (1.33)

Spread BYU 9.5
BetMGM: +280 (3.8)

Spread Gonzaga -9.5
BetMGM: -350 (1.28)

Spread BYU 8.5
BetMGM: +333 (4.33)

Spread Gonzaga -8.5
BetMGM: -450 (1.22)
Total
Total Over 163.5
DraftKings: -110 (1.91)
BetMGM: -110 (1.91)

Total Under 163.5
DraftKings: -109 (1.92)
BetMGM: -110 (1.91)

Total Over 158.5
DraftKings: -195 (1.52)
BetMGM: -165 (1.6)

Total Under 158.5
DraftKings: +155 (2.55)
BetMGM: +140 (2.4)

Total Over 159.0
DraftKings: -186 (1.54)

Total Under 159.0
DraftKings: +150 (2.5)

Total Over 159.5
DraftKings: -175 (1.58)
BetMGM: -155 (1.65)

Total Under 159.5
DraftKings: +143 (2.43)
BetMGM: +125 (2.25)

Total Over 164.0
DraftKings: -105 (1.96)

Total Under 164.0
DraftKings: -115 (1.87)

Total Over 167.5
DraftKings: +138 (2.38)
BetMGM: +135 (2.35)

Total Under 167.5
DraftKings: -167 (1.6)
BetMGM: -160 (1.62)

Total Over 168.0
DraftKings: +150 (2.5)

Total Under 168.0
DraftKings: -182 (1.55)

Total Over 168.5
DraftKings: +155 (2.55)
BetMGM: +145 (2.45)

Total Under 168.5
DraftKings: -190 (1.53)
BetMGM: -175 (1.57)

Total Over 154.5
BetMGM: -250 (1.4)

Total Under 154.5
BetMGM: +200 (3.0)

Total Over 155.5
BetMGM: -225 (1.44)

Total Under 155.5
BetMGM: +185 (2.85)

Total Over 156.5
BetMGM: -200 (1.48)

Total Under 156.5
BetMGM: +170 (2.7)

Total Over 157.5
BetMGM: -190 (1.53)

Total Under 157.5
BetMGM: +155 (2.55)

Total Over 160.5
BetMGM: -145 (1.7)

Total Under 160.5
BetMGM: +120 (2.2)

Total Over 161.5
BetMGM: -130 (1.78)

Total Under 161.5
BetMGM: +105 (2.05)

Total Over 162.5
BetMGM: -120 (1.83)

Total Under 162.5
BetMGM: +100 (2.0)

Total Over 164.5
BetMGM: +100 (2.0)

Total Under 164.5
BetMGM: -120 (1.83)

Total Over 165.5
BetMGM: +110 (2.1)

Total Under 165.5
BetMGM: -135 (1.75)

Total Over 166.5
BetMGM: +120 (2.2)

Total Under 166.5
BetMGM: -145 (1.7)

Total Over 169.5
BetMGM: +155 (2.55)

Total Under 169.5
BetMGM: -190 (1.53)

Total Over 170.5
BetMGM: +170 (2.7)

Total Under 170.5
BetMGM: -200 (1.48)

Total Over 171.5
BetMGM: +185 (2.85)

Total Under 171.5
BetMGM: -225 (1.44)

Total Over 172.5
BetMGM: +200 (3.0)

Total Under 172.5
BetMGM: -250 (1.4)

Total Over 173.5
BetMGM: +225 (3.25)

Total Under 173.5
BetMGM: -275 (1.36)
Moneyline
Moneyline BYU None
DraftKings: +1100 (12.0)
BetMGM: +1200 (13.0)

Moneyline Gonzaga None
DraftKings: -2500 (1.04)
BetMGM: -3000 (1.03)


, 'Murray State @ Eastern Illinois': College Basketball: Murray State @ Eastern Illinois
DraftKings - 179653607
FanDuel - 949369.3
BetMGM - 10973263
Spread
Spread Murray State -3.5
DraftKings: -112 (1.9)
FanDuel: -114 (1.8772)
BetMGM: -115 (1.87)

Spread Eastern Illinois 3.5
DraftKings: -108 (1.93)
FanDuel: -106 (1.9434)
BetMGM: -105 (1.95)

Spread Murray State -8.5
DraftKings: +200 (3.0)
BetMGM: +195 (2.95)

Spread Eastern Illinois 8.5
DraftKings: -250 (1.4)
BetMGM: -250 (1.42)

Spread Murray State -8.0
DraftKings: +190 (2.9)

Spread Eastern Illinois 8.0
DraftKings: -240 (1.42)

Spread Murray State -7.5
DraftKings: +175 (2.75)
BetMGM: +165 (2.65)

Spread Eastern Illinois 7.5
DraftKings: -215 (1.47)
BetMGM: -200 (1.5)

Spread Murray State -3.0
DraftKings: -121 (1.83)

Spread Eastern Illinois 3.0
DraftKings: +100 (2.0)

Spread Murray State 0.5
DraftKings: -175 (1.58)

Spread Eastern Illinois -0.5
DraftKings: +143 (2.43)

Spread Murray State 1.0
DraftKings: -186 (1.54)

Spread Eastern Illinois -1.0
DraftKings: +150 (2.5)

Spread Murray State 1.5
DraftKings: -195 (1.52)
BetMGM: -200 (1.48)

Spread Eastern Illinois -1.5
DraftKings: +155 (2.55)
BetMGM: +170 (2.7)

Spread Murray State 8.5
BetMGM: -750 (1.13)

Spread Eastern Illinois -8.5
BetMGM: +525 (6.25)

Spread Murray State 7.5
BetMGM: -650 (1.16)

Spread Eastern Illinois -7.5
BetMGM: +450 (5.5)

Spread Murray State 6.5
BetMGM: -500 (1.2)

Spread Eastern Illinois -6.5
BetMGM: +375 (4.75)

Spread Murray State 5.5
BetMGM: -400 (1.25)

Spread Eastern Illinois -5.5
BetMGM: +310 (4.1)

Spread Murray State 4.5
BetMGM: -350 (1.28)

Spread Eastern Illinois -4.5
BetMGM: +275 (3.75)

Spread Murray State 3.5
BetMGM: -300 (1.34)

Spread Eastern Illinois -3.5
BetMGM: +230 (3.3)

Spread Murray State 2.5
BetMGM: -250 (1.4)

Spread Eastern Illinois -2.5
BetMGM: +200 (3.0)

Spread Murray State -1.5
BetMGM: -160 (1.62)

Spread Eastern Illinois 1.5
BetMGM: +135 (2.35)

Spread Murray State -2.5
BetMGM: -140 (1.72)

Spread Eastern Illinois 2.5
BetMGM: +115 (2.15)

Spread Murray State -4.5
BetMGM: +100 (2.0)

Spread Eastern Illinois 4.5
BetMGM: -120 (1.83)

Spread Murray State -5.5
BetMGM: +120 (2.2)

Spread Eastern Illinois 5.5
BetMGM: -145 (1.7)

Spread Murray State -6.5
BetMGM: +140 (2.4)

Spread Eastern Illinois 6.5
BetMGM: -165 (1.6)

Spread Murray State -9.5
BetMGM: +225 (3.25)

Spread Eastern Illinois 9.5
BetMGM: -275 (1.35)

Spread Murray State -10.5
BetMGM: +260 (3.6)

Spread Eastern Illinois 10.5
BetMGM: -350 (1.3)

Spread Murray State -11.5
BetMGM: +310 (4.1)

Spread Eastern Illinois 11.5
BetMGM: -400 (1.25)

Spread Murray State -12.5
BetMGM: +360 (4.6)

Spread Eastern Illinois 12.5
BetMGM: -500 (1.2)
Total
Total Over 140.0
DraftKings: -108 (1.93)

Total Under 140.0
DraftKings: -112 (1.9)

Total Over 135.0
DraftKings: -190 (1.53)

Total Under 135.0
DraftKings: +155 (2.55)

Total Over 135.5
DraftKings: -177 (1.57)
BetMGM: -185 (1.55)

Total Under 135.5
DraftKings: +145 (2.45)
BetMGM: +150 (2.5)

Total Over 136.0
DraftKings: -167 (1.6)

Total Under 136.0
DraftKings: +138 (2.38)

Total Over 139.5
DraftKings: -114 (1.88)
BetMGM: -120 (1.83)

Total Under 139.5
DraftKings: -106 (1.95)
BetMGM: +100 (2.0)

Total Over 144.0
DraftKings: +143 (2.43)

Total Under 144.0
DraftKings: -177 (1.57)

Total Over 144.5
DraftKings: +150 (2.5)
BetMGM: +135 (2.35)

Total Under 144.5
DraftKings: -182 (1.55)
BetMGM: -160 (1.62)

Total Over 145.0
DraftKings: +160 (2.6)

Total Under 145.0
DraftKings: -195 (1.52)

Total Over 140.5
FanDuel: -106 (1.9434)
BetMGM: -110 (1.91)

Total Under 140.5
FanDuel: -114 (1.8772)
BetMGM: -110 (1.91)

Total Over 131.5
BetMGM: -275 (1.36)

Total Under 131.5
BetMGM: +220 (3.2)

Total Over 132.5
BetMGM: -250 (1.42)

Total Under 132.5
BetMGM: +195 (2.95)

Total Over 133.5
BetMGM: -225 (1.45)

Total Under 133.5
BetMGM: +180 (2.8)

Total Over 134.5
BetMGM: -200 (1.5)

Total Under 134.5
BetMGM: +165 (2.65)

Total Over 136.5
BetMGM: -160 (1.62)

Total Under 136.5
BetMGM: +135 (2.35)

Total Over 137.5
BetMGM: -150 (1.67)

Total Under 137.5
BetMGM: +125 (2.25)

Total Over 138.5
BetMGM: -135 (1.75)

Total Under 138.5
BetMGM: +110 (2.1)

Total Over 141.5
BetMGM: +100 (2.0)

Total Under 141.5
BetMGM: -120 (1.83)

Total Over 142.5
BetMGM: +110 (2.1)

Total Under 142.5
BetMGM: -135 (1.75)

Total Over 143.5
BetMGM: +120 (2.2)

Total Under 143.5
BetMGM: -145 (1.7)

Total Over 145.5
BetMGM: +145 (2.45)

Total Under 145.5
BetMGM: -175 (1.57)

Total Over 146.5
BetMGM: +155 (2.55)

Total Under 146.5
BetMGM: -190 (1.53)

Total Over 147.5
BetMGM: +170 (2.7)

Total Under 147.5
BetMGM: -200 (1.48)

Total Over 148.5
BetMGM: +190 (2.9)

Total Under 148.5
BetMGM: -250 (1.42)

Total Over 149.5
BetMGM: +200 (3.0)

Total Under 149.5
BetMGM: -250 (1.4)

Total Over 150.5
BetMGM: +225 (3.25)

Total Under 150.5
BetMGM: -275 (1.35)
Moneyline
Moneyline Murray State None
DraftKings: -175 (1.58)
FanDuel: -178 (1.5618)
BetMGM: -185 (1.55)

Moneyline Eastern Illinois None
DraftKings: +143 (2.43)
FanDuel: +150 (2.5)
BetMGM: +150 (2.5)


, 'Montana State @ Northern Colorado': College Basketball: Montana State @ Northern Colorado
DraftKings - 179653608
FanDuel - 949363.3
FoxBet - 8715798
BetMGM - 10973264
Spread
Spread Montana State 3.5
DraftKings: -108 (1.93)
FoxBet: -105 (1.95)
BetMGM: +100 (2.0)

Spread Northern Colorado -3.5
DraftKings: -112 (1.9)
FoxBet: -125 (1.8)
BetMGM: -120 (1.83)

Spread Montana State -1.5
DraftKings: +155 (2.55)
BetMGM: +180 (2.8)

Spread Northern Colorado 1.5
DraftKings: -195 (1.52)
BetMGM: -225 (1.45)

Spread Montana State -1.0
DraftKings: +150 (2.5)

Spread Northern Colorado 1.0
DraftKings: -186 (1.54)

Spread Montana State -0.5
DraftKings: +143 (2.43)

Spread Northern Colorado 0.5
DraftKings: -175 (1.58)

Spread Montana State 3.0
DraftKings: +100 (2.0)
FoxBet: +105 (2.05)

Spread Northern Colorado -3.0
DraftKings: -121 (1.83)
FoxBet: -137 (1.7273)

Spread Montana State 7.5
DraftKings: -215 (1.47)
FoxBet: -200 (1.5)
BetMGM: -190 (1.53)

Spread Northern Colorado -7.5
DraftKings: +175 (2.75)
FoxBet: +145 (2.45)
BetMGM: +155 (2.55)

Spread Montana State 8.0
DraftKings: -245 (1.41)
FoxBet: -212 (1.4706)

Spread Northern Colorado -8.0
DraftKings: +195 (2.95)
FoxBet: +155 (2.55)

Spread Montana State 8.5
DraftKings: -250 (1.4)
BetMGM: -225 (1.44)

Spread Northern Colorado -8.5
DraftKings: +200 (3.0)
BetMGM: +185 (2.85)

Spread Montana State 4.5
FanDuel: -118 (1.8475)
FoxBet: -125 (1.8)
BetMGM: -115 (1.87)

Spread Northern Colorado -4.5
FanDuel: -104 (1.9615)
FoxBet: -105 (1.95)
BetMGM: -105 (1.95)

Spread Northern Colorado -1.5
FoxBet: -175 (1.5714)
BetMGM: -165 (1.6)

Spread Montana State 1.5
FoxBet: +130 (2.3)
BetMGM: +140 (2.4)

Spread Montana State 5.5
FoxBet: -143 (1.7)
BetMGM: -140 (1.72)

Spread Northern Colorado -5.5
FoxBet: +110 (2.1)
BetMGM: +115 (2.15)

Spread Northern Colorado -1.0
FoxBet: -188 (1.5333)

Spread Montana State 1.0
FoxBet: +140 (2.4)

Spread Montana State 7.0
FoxBet: -188 (1.5333)

Spread Northern Colorado -7.0
FoxBet: +140 (2.4)

Spread Northern Colorado -6.0
FoxBet: +120 (2.2)

Spread Montana State 6.0
FoxBet: -154 (1.65)

Spread Northern Colorado -2.0
FoxBet: -162 (1.6154)

Spread Montana State 2.0
FoxBet: +125 (2.25)

Spread Montana State 5.0
FoxBet: -137 (1.7273)

Spread Northern Colorado -5.0
FoxBet: +105 (2.05)

Spread Northern Colorado -4.0
FoxBet: -110 (1.9091)

Spread Montana State 4.0
FoxBet: -110 (1.9091)

Spread Northern Colorado -2.5
FoxBet: -143 (1.7)
BetMGM: -145 (1.7)

Spread Montana State 2.5
FoxBet: +110 (2.1)
BetMGM: +120 (2.2)

Spread Montana State 6.5
FoxBet: -175 (1.5714)
BetMGM: -165 (1.6)

Spread Northern Colorado -6.5
FoxBet: +130 (2.3)
BetMGM: +135 (2.35)

Spread Montana State 13.5
BetMGM: -550 (1.18)

Spread Northern Colorado -13.5
BetMGM: +400 (5.0)

Spread Montana State 12.5
BetMGM: -450 (1.22)

Spread Northern Colorado -12.5
BetMGM: +350 (4.5)

Spread Montana State 11.5
BetMGM: -375 (1.26)

Spread Northern Colorado -11.5
BetMGM: +300 (4.0)

Spread Montana State 10.5
BetMGM: -350 (1.3)

Spread Northern Colorado -10.5
BetMGM: +260 (3.6)

Spread Montana State 9.5
BetMGM: -275 (1.36)

Spread Northern Colorado -9.5
BetMGM: +220 (3.2)

Spread Montana State -2.5
BetMGM: +200 (3.0)

Spread Northern Colorado 2.5
BetMGM: -250 (1.4)

Spread Montana State -3.5
BetMGM: +240 (3.4)

Spread Northern Colorado 3.5
BetMGM: -300 (1.33)

Spread Montana State -4.5
BetMGM: +280 (3.8)

Spread Northern Colorado 4.5
BetMGM: -350 (1.28)

Spread Montana State -5.5
BetMGM: +333 (4.33)

Spread Northern Colorado 5.5
BetMGM: -450 (1.22)

Spread Montana State -6.5
BetMGM: +400 (5.0)

Spread Northern Colorado 6.5
BetMGM: -550 (1.18)

Spread Montana State -7.5
BetMGM: +475 (5.75)

Spread Northern Colorado 7.5
BetMGM: -650 (1.15)
Total
Total Over 137.0
DraftKings: -108 (1.93)
FoxBet: -105 (1.95)

Total Under 137.0
DraftKings: -112 (1.9)
FoxBet: -125 (1.8)

Total Over 132.0
DraftKings: -190 (1.53)

Total Under 132.0
DraftKings: +155 (2.55)

Total Over 132.5
DraftKings: -180 (1.56)
BetMGM: -185 (1.55)

Total Under 132.5
DraftKings: +145 (2.45)
BetMGM: +150 (2.5)

Total Over 133.0
DraftKings: -167 (1.6)
FoxBet: -154 (1.65)

Total Under 133.0
DraftKings: +138 (2.38)
FoxBet: +120 (2.2)

Total Over 136.5
DraftKings: -114 (1.88)
FanDuel: -110 (1.9091)
FoxBet: -110 (1.9091)
BetMGM: -120 (1.83)

Total Under 136.5
DraftKings: -106 (1.95)
FanDuel: -110 (1.9091)
FoxBet: -110 (1.9091)
BetMGM: +100 (2.0)

Total Over 141.0
DraftKings: +145 (2.45)

Total Under 141.0
DraftKings: -177 (1.57)

Total Over 141.5
DraftKings: +150 (2.5)
BetMGM: +135 (2.35)

Total Under 141.5
DraftKings: -182 (1.55)
BetMGM: -160 (1.62)

Total Over 142.0
DraftKings: +160 (2.6)

Total Under 142.0
DraftKings: -195 (1.52)

Total Over 134.5
FoxBet: -137 (1.7273)
BetMGM: -150 (1.67)

Total Under 134.5
FoxBet: +105 (2.05)
BetMGM: +125 (2.25)

Total Over 138.5
FoxBet: +105 (2.05)
BetMGM: +100 (2.0)

Total Under 138.5
FoxBet: -137 (1.7273)
BetMGM: -120 (1.83)

Total Over 133.5
FoxBet: -150 (1.6667)
BetMGM: -160 (1.62)

Total Under 133.5
FoxBet: +115 (2.15)
BetMGM: +135 (2.35)

Total Under 135.5
FoxBet: -105 (1.95)
BetMGM: +110 (2.1)

Total Over 135.5
FoxBet: -125 (1.8)
BetMGM: -135 (1.75)

Total Over 134.0
FoxBet: -143 (1.7)

Total Under 134.0
FoxBet: +110 (2.1)

Total Over 135.0
FoxBet: -133 (1.75)

Total Under 135.0
FoxBet: +100 (2.0)

Total Over 136.0
FoxBet: -125 (1.8)

Total Under 136.0
FoxBet: -105 (1.95)

Total Over 138.0
FoxBet: +100 (2.0)

Total Under 138.0
FoxBet: -133 (1.75)

Total Under 139.0
FoxBet: -143 (1.7)

Total Over 139.0
FoxBet: +110 (2.1)

Total Under 140.0
FoxBet: -154 (1.65)

Total Over 140.0
FoxBet: +120 (2.2)

Total Over 137.5
FoxBet: -105 (1.95)
BetMGM: -110 (1.91)

Total Under 137.5
FoxBet: -125 (1.8)
BetMGM: -110 (1.91)

Total Over 139.5
FoxBet: +115 (2.15)
BetMGM: +110 (2.1)

Total Under 139.5
FoxBet: -150 (1.6667)
BetMGM: -135 (1.75)

Total Over 126.5
BetMGM: -350 (1.3)

Total Under 126.5
BetMGM: +260 (3.6)

Total Over 127.5
BetMGM: -300 (1.33)

Total Under 127.5
BetMGM: +240 (3.4)

Total Over 128.5
BetMGM: -275 (1.36)

Total Under 128.5
BetMGM: +220 (3.2)

Total Over 129.5
BetMGM: -250 (1.4)

Total Under 129.5
BetMGM: +200 (3.0)

Total Over 130.5
BetMGM: -225 (1.45)

Total Under 130.5
BetMGM: +180 (2.8)

Total Over 131.5
BetMGM: -200 (1.5)

Total Under 131.5
BetMGM: +165 (2.65)

Total Over 140.5
BetMGM: +120 (2.2)

Total Under 140.5
BetMGM: -145 (1.7)

Total Over 142.5
BetMGM: +145 (2.45)

Total Under 142.5
BetMGM: -175 (1.57)

Total Over 143.5
BetMGM: +155 (2.55)

Total Under 143.5
BetMGM: -190 (1.53)

Total Over 144.5
BetMGM: +170 (2.7)

Total Under 144.5
BetMGM: -200 (1.48)

Total Over 145.5
BetMGM: +190 (2.9)

Total Under 145.5
BetMGM: -250 (1.42)
Moneyline
Moneyline Montana State None
DraftKings: +143 (2.43)
FanDuel: +160 (2.6)
FoxBet: +150 (2.5)
BetMGM: +155 (2.55)

Moneyline Northern Colorado None
DraftKings: -175 (1.58)
FanDuel: -190 (1.5263)
FoxBet: -188 (1.5333)
BetMGM: -190 (1.53)


, 'Tennessee-Martin @ Tennessee State': College Basketball: Tennessee-Martin @ Tennessee State
DraftKings - 179653618


, 'USC @ Arizona': College Basketball: USC @ Arizona
DraftKings - 179653609
FanDuel - 949367.3
FoxBet - 8715804
BetMGM - 10973267
Spread
Spread USC 4.5
DraftKings: -108 (1.93)
FanDuel: -115 (1.8696)
FoxBet: -125 (1.8)
BetMGM: -110 (1.91)

Spread Arizona -4.5
DraftKings: -112 (1.9)
FanDuel: -105 (1.9524)
FoxBet: -105 (1.95)
BetMGM: -110 (1.91)

Spread USC -0.5
DraftKings: +165 (2.65)

Spread Arizona 0.5
DraftKings: -205 (1.49)

Spread USC 0.0
DraftKings: +165 (2.65)

Spread Arizona 0.0
DraftKings: -205 (1.49)

Spread USC 0.5
DraftKings: +165 (2.65)

Spread Arizona -0.5
DraftKings: -205 (1.49)

Spread USC 4.0
DraftKings: +100 (2.0)
FoxBet: -105 (1.95)

Spread Arizona -4.0
DraftKings: -122 (1.82)
FoxBet: -125 (1.8)

Spread USC 8.5
DraftKings: -215 (1.47)
BetMGM: -200 (1.48)

Spread Arizona -8.5
DraftKings: +175 (2.75)
BetMGM: +170 (2.7)

Spread USC 9.0
DraftKings: -240 (1.42)

Spread Arizona -9.0
DraftKings: +190 (2.9)

Spread USC 9.5
DraftKings: -250 (1.4)
BetMGM: -250 (1.4)

Spread Arizona -9.5
DraftKings: +200 (3.0)
BetMGM: +200 (3.0)

Spread USC 1.5
FoxBet: +135 (2.35)
BetMGM: +150 (2.5)

Spread Arizona -1.5
FoxBet: -182 (1.55)
BetMGM: -185 (1.55)

Spread USC 7.5
FoxBet: -200 (1.5)
BetMGM: -185 (1.55)

Spread Arizona -7.5
FoxBet: +145 (2.45)
BetMGM: +150 (2.5)

Spread USC 3.5
FoxBet: +100 (2.0)
BetMGM: +110 (2.1)

Spread Arizona -3.5
FoxBet: -133 (1.75)
BetMGM: -135 (1.75)

Spread USC 5.5
FoxBet: -137 (1.7273)
BetMGM: -125 (1.8)

Spread Arizona -5.5
FoxBet: +105 (2.05)
BetMGM: +105 (2.05)

Spread USC 1.0
FoxBet: +145 (2.45)

Spread Arizona -1.0
FoxBet: -200 (1.5)

Spread USC 7.0
FoxBet: -182 (1.55)

Spread Arizona -7.0
FoxBet: +135 (2.35)

Spread USC 6.0
FoxBet: -150 (1.6667)

Spread Arizona -6.0
FoxBet: +115 (2.15)

Spread USC 8.0
FoxBet: -212 (1.4706)

Spread Arizona -8.0
FoxBet: +155 (2.55)

Spread USC 3.0
FoxBet: +105 (2.05)

Spread Arizona -3.0
FoxBet: -137 (1.7273)

Spread Arizona -2.0
FoxBet: -162 (1.6154)

Spread USC 2.0
FoxBet: +125 (2.25)

Spread USC 5.0
FoxBet: -133 (1.75)

Spread Arizona -5.0
FoxBet: +100 (2.0)

Spread USC 2.5
FoxBet: +115 (2.15)
BetMGM: +125 (2.25)

Spread Arizona -2.5
FoxBet: -150 (1.6667)
BetMGM: -155 (1.65)

Spread USC 6.5
FoxBet: -162 (1.6154)
BetMGM: -150 (1.67)

Spread Arizona -6.5
FoxBet: +125 (2.25)
BetMGM: +125 (2.25)

Spread USC 13.5
BetMGM: -500 (1.2)

Spread Arizona -13.5
BetMGM: +375 (4.75)

Spread USC 12.5
BetMGM: -400 (1.25)

Spread Arizona -12.5
BetMGM: +310 (4.1)

Spread USC 11.5
BetMGM: -350 (1.28)

Spread Arizona -11.5
BetMGM: +275 (3.75)

Spread USC 10.5
BetMGM: -300 (1.34)

Spread Arizona -10.5
BetMGM: +230 (3.3)

Spread USC -1.5
BetMGM: +195 (2.95)

Spread Arizona 1.5
BetMGM: -250 (1.42)

Spread USC -2.5
BetMGM: +225 (3.25)

Spread Arizona 2.5
BetMGM: -275 (1.35)

Spread USC -3.5
BetMGM: +260 (3.6)

Spread Arizona 3.5
BetMGM: -350 (1.3)

Spread USC -4.5
BetMGM: +310 (4.1)

Spread Arizona 4.5
BetMGM: -400 (1.25)

Spread USC -5.5
BetMGM: +360 (4.6)

Spread Arizona 5.5
BetMGM: -500 (1.2)

Spread USC -6.5
BetMGM: +425 (5.25)

Spread Arizona 6.5
BetMGM: -600 (1.17)

Spread USC -7.5
BetMGM: +525 (6.25)

Spread Arizona 7.5
BetMGM: -750 (1.13)
Total
Total Over 138.5
DraftKings: -108 (1.93)
FanDuel: -110 (1.9091)
FoxBet: -110 (1.9091)
BetMGM: -105 (1.95)

Total Under 138.5
DraftKings: -113 (1.89)
FanDuel: -110 (1.9091)
FoxBet: -110 (1.9091)
BetMGM: -115 (1.87)

Total Over 133.5
DraftKings: -186 (1.54)
BetMGM: -165 (1.6)

Total Under 133.5
DraftKings: +150 (2.5)
BetMGM: +140 (2.4)

Total Over 134.0
DraftKings: -180 (1.56)

Total Under 134.0
DraftKings: +148 (2.48)

Total Over 134.5
DraftKings: -167 (1.6)
BetMGM: -155 (1.65)

Total Under 134.5
DraftKings: +138 (2.38)
BetMGM: +125 (2.25)

Total Over 138.0
DraftKings: -114 (1.88)
FoxBet: -125 (1.8)

Total Under 138.0
DraftKings: -107 (1.94)
FoxBet: -105 (1.95)

Total Over 142.5
DraftKings: +143 (2.43)
BetMGM: +135 (2.35)

Total Under 142.5
DraftKings: -175 (1.58)
BetMGM: -165 (1.6)

Total Over 143.0
DraftKings: +150 (2.5)

Total Under 143.0
DraftKings: -186 (1.54)

Total Over 143.5
DraftKings: +155 (2.55)
BetMGM: +150 (2.5)

Total Under 143.5
DraftKings: -195 (1.52)
BetMGM: -185 (1.55)

Total Over 141.5
FoxBet: +115 (2.15)
BetMGM: +125 (2.25)

Total Under 141.5
FoxBet: -150 (1.6667)
BetMGM: -150 (1.67)

Total Under 136.5
FoxBet: +105 (2.05)
BetMGM: +105 (2.05)

Total Over 136.5
FoxBet: -137 (1.7273)
BetMGM: -130 (1.78)

Total Under 135.5
FoxBet: +115 (2.15)
BetMGM: +115 (2.15)

Total Over 135.5
FoxBet: -150 (1.6667)
BetMGM: -140 (1.72)

Total Under 135.0
FoxBet: +125 (2.25)

Total Over 135.0
FoxBet: -162 (1.6154)

Total Under 136.0
FoxBet: +110 (2.1)

Total Over 136.0
FoxBet: -143 (1.7)

Total Under 137.0
FoxBet: +105 (2.05)

Total Over 137.0
FoxBet: -137 (1.7273)

Total Under 139.0
FoxBet: -125 (1.8)

Total Over 139.0
FoxBet: -105 (1.95)

Total Under 140.5
FoxBet: -137 (1.7273)
BetMGM: -140 (1.72)

Total Over 140.5
FoxBet: +105 (2.05)
BetMGM: +115 (2.15)

Total Under 140.0
FoxBet: -133 (1.75)

Total Over 140.0
FoxBet: +100 (2.0)

Total Under 141.0
FoxBet: -143 (1.7)

Total Over 141.0
FoxBet: +110 (2.1)

Total Under 142.0
FoxBet: -154 (1.65)

Total Over 142.0
FoxBet: +120 (2.2)

Total Over 137.5
FoxBet: -133 (1.75)
BetMGM: -115 (1.87)

Total Under 137.5
FoxBet: +100 (2.0)
BetMGM: -105 (1.95)

Total Over 139.5
FoxBet: -105 (1.95)
BetMGM: +105 (2.05)

Total Under 139.5
FoxBet: -125 (1.8)
BetMGM: -125 (1.8)

Total Over 131.5
BetMGM: -200 (1.48)

Total Under 131.5
BetMGM: +170 (2.7)

Total Over 132.5
BetMGM: -190 (1.53)

Total Under 132.5
BetMGM: +155 (2.55)

Total Over 144.5
BetMGM: +165 (2.65)

Total Under 144.5
BetMGM: -200 (1.5)

Total Over 145.5
BetMGM: +180 (2.8)

Total Under 145.5
BetMGM: -225 (1.45)

Total Over 146.5
BetMGM: +200 (3.0)

Total Under 146.5
BetMGM: -250 (1.4)

Total Over 147.5
BetMGM: +220 (3.2)

Total Under 147.5
BetMGM: -275 (1.36)

Total Over 148.5
BetMGM: +240 (3.4)

Total Under 148.5
BetMGM: -300 (1.33)

Total Over 149.5
BetMGM: +260 (3.6)

Total Under 149.5
BetMGM: -350 (1.3)

Total Over 150.5
BetMGM: +290 (3.9)

Total Under 150.5
BetMGM: -375 (1.26)
Moneyline
Moneyline USC None
DraftKings: +165 (2.65)
FanDuel: +164 (2.64)
FoxBet: +160 (2.6)
BetMGM: +170 (2.7)

Moneyline Arizona None
DraftKings: -205 (1.49)
FanDuel: -198 (1.5051)
FoxBet: -200 (1.5)
BetMGM: -200 (1.48)


, 'Idaho @ Southern Utah': College Basketball: Idaho @ Southern Utah
DraftKings - 179653614
FanDuel - 949371.3
FoxBet - 8715812
BetMGM - 10973270
Spread
Spread Idaho 14.5
DraftKings: -109 (1.92)
FanDuel: -112 (1.8929)
FoxBet: -125 (1.8)
BetMGM: -120 (1.83)

Spread Southern Utah -14.5
DraftKings: -110 (1.91)
FanDuel: -108 (1.9259)
FoxBet: -105 (1.95)
BetMGM: +100 (2.0)

Spread Idaho 9.5
DraftKings: +190 (2.9)
BetMGM: +180 (2.8)

Spread Southern Utah -9.5
DraftKings: -240 (1.42)
BetMGM: -225 (1.45)

Spread Idaho 10.0
DraftKings: +185 (2.85)

Spread Southern Utah -10.0
DraftKings: -230 (1.44)

Spread Idaho 10.5
DraftKings: +165 (2.65)
BetMGM: +155 (2.55)

Spread Southern Utah -10.5
DraftKings: -205 (1.49)
BetMGM: -190 (1.53)

Spread Idaho 15.0
DraftKings: -118 (1.85)
FoxBet: -133 (1.75)

Spread Southern Utah -15.0
DraftKings: -103 (1.98)
FoxBet: +100 (2.0)

Spread Idaho 18.5
DraftKings: -200 (1.5)
BetMGM: -225 (1.45)

Spread Southern Utah -18.5
DraftKings: +165 (2.65)
BetMGM: +180 (2.8)

Spread Idaho 19.0
DraftKings: -225 (1.45)

Spread Southern Utah -19.0
DraftKings: +180 (2.8)

Spread Idaho 19.5
DraftKings: -240 (1.42)
BetMGM: -250 (1.4)

Spread Southern Utah -19.5
DraftKings: +190 (2.9)
BetMGM: +200 (3.0)

Spread Southern Utah -17.5
FoxBet: +135 (2.35)
BetMGM: +155 (2.55)

Spread Idaho 17.5
FoxBet: -182 (1.55)
BetMGM: -190 (1.53)

Spread Southern Utah -13.5
FoxBet: -133 (1.75)
BetMGM: -120 (1.83)

Spread Idaho 13.5
FoxBet: +100 (2.0)
BetMGM: +100 (2.0)

Spread Southern Utah -12.0
FoxBet: -162 (1.6154)

Spread Idaho 12.0
FoxBet: +125 (2.25)

Spread Southern Utah -11.0
FoxBet: -200 (1.5)

Spread Idaho 11.0
FoxBet: +145 (2.45)

Spread Southern Utah -14.0
FoxBet: -125 (1.8)

Spread Idaho 14.0
FoxBet: -105 (1.95)

Spread Southern Utah -13.0
FoxBet: -137 (1.7273)

Spread Idaho 13.0
FoxBet: +105 (2.05)

Spread Southern Utah -16.0
FoxBet: +110 (2.1)

Spread Idaho 16.0
FoxBet: -143 (1.7)

Spread Southern Utah -18.0
FoxBet: +145 (2.45)

Spread Idaho 18.0
FoxBet: -200 (1.5)

Spread Southern Utah -17.0
FoxBet: +130 (2.3)

Spread Idaho 17.0
FoxBet: -175 (1.5714)

Spread Southern Utah -11.5
FoxBet: -182 (1.55)
BetMGM: -160 (1.62)

Spread Idaho 11.5
FoxBet: +135 (2.35)
BetMGM: +135 (2.35)

Spread Idaho 15.5
FoxBet: -137 (1.7273)
BetMGM: -140 (1.72)

Spread Southern Utah -15.5
FoxBet: +105 (2.05)
BetMGM: +115 (2.15)

Spread Idaho 16.5
FoxBet: -154 (1.65)
BetMGM: -160 (1.62)

Spread Southern Utah -16.5
FoxBet: +120 (2.2)
BetMGM: +135 (2.35)

Spread Idaho 12.5
FoxBet: +115 (2.15)
BetMGM: +115 (2.15)

Spread Southern Utah -12.5
FoxBet: -150 (1.6667)
BetMGM: -140 (1.72)

Spread Idaho 23.5
BetMGM: -500 (1.2)

Spread Southern Utah -23.5
BetMGM: +375 (4.75)

Spread Idaho 22.5
BetMGM: -400 (1.25)

Spread Southern Utah -22.5
BetMGM: +310 (4.1)

Spread Idaho 21.5
BetMGM: -350 (1.28)

Spread Southern Utah -21.5
BetMGM: +275 (3.75)

Spread Idaho 20.5
BetMGM: -300 (1.33)

Spread Southern Utah -20.5
BetMGM: +240 (3.4)

Spread Idaho 8.5
BetMGM: +220 (3.2)

Spread Southern Utah -8.5
BetMGM: -275 (1.36)

Spread Idaho 7.5
BetMGM: +240 (3.4)

Spread Southern Utah -7.5
BetMGM: -300 (1.33)

Spread Idaho 6.5
BetMGM: +290 (3.9)

Spread Southern Utah -6.5
BetMGM: -375 (1.26)

Spread Idaho 5.5
BetMGM: +350 (4.5)

Spread Southern Utah -5.5
BetMGM: -450 (1.22)

Spread Idaho 4.5
BetMGM: +425 (5.25)

Spread Southern Utah -4.5
BetMGM: -600 (1.17)
Total
Total Over 142.5
DraftKings: -109 (1.92)
FanDuel: -108 (1.9259)
FoxBet: -110 (1.9091)
BetMGM: -110 (1.91)

Total Under 142.5
DraftKings: -110 (1.91)
FanDuel: -112 (1.8929)
FoxBet: -125 (1.8)
BetMGM: -110 (1.91)

Total Over 137.5
DraftKings: -195 (1.52)
BetMGM: -175 (1.57)

Total Under 137.5
DraftKings: +155 (2.55)
BetMGM: +145 (2.45)

Total Over 138.0
DraftKings: -186 (1.54)

Total Under 138.0
DraftKings: +150 (2.5)

Total Over 138.5
DraftKings: -175 (1.58)
BetMGM: -160 (1.62)

Total Under 138.5
DraftKings: +143 (2.43)
BetMGM: +135 (2.35)

Total Over 142.0
DraftKings: -115 (1.87)
FoxBet: -125 (1.8)

Total Under 142.0
DraftKings: -105 (1.96)
FoxBet: -105 (1.95)

Total Over 146.5
DraftKings: +143 (2.43)
BetMGM: +135 (2.35)

Total Under 146.5
DraftKings: -175 (1.58)
BetMGM: -160 (1.62)

Total Over 147.0
DraftKings: +150 (2.5)

Total Under 147.0
DraftKings: -186 (1.54)

Total Over 147.5
DraftKings: +155 (2.55)
BetMGM: +145 (2.45)

Total Under 147.5
DraftKings: -195 (1.52)
BetMGM: -175 (1.57)

Total Under 145.5
FoxBet: -150 (1.6667)
BetMGM: -145 (1.7)

Total Over 145.5
FoxBet: +115 (2.15)
BetMGM: +120 (2.2)

Total Over 141.5
FoxBet: -125 (1.8)
BetMGM: -120 (1.83)

Total Under 141.5
FoxBet: -105 (1.95)
BetMGM: +100 (2.0)

Total Under 139.0
FoxBet: +120 (2.2)

Total Over 139.0
FoxBet: -154 (1.65)

Total Under 140.0
FoxBet: +110 (2.1)

Total Over 140.0
FoxBet: -143 (1.7)

Total Under 141.0
FoxBet: +100 (2.0)

Total Over 141.0
FoxBet: -133 (1.75)

Total Under 143.0
FoxBet: -125 (1.8)

Total Over 143.0
FoxBet: -105 (1.95)

Total Under 144.0
FoxBet: -137 (1.7273)

Total Over 144.0
FoxBet: +105 (2.05)

Total Over 145.0
FoxBet: +110 (2.1)

Total Under 145.0
FoxBet: -143 (1.7)

Total Under 146.0
FoxBet: -162 (1.6154)

Total Over 146.0
FoxBet: +125 (2.25)

Total Under 139.5
FoxBet: +115 (2.15)
BetMGM: +120 (2.2)

Total Over 139.5
FoxBet: -150 (1.6667)
BetMGM: -145 (1.7)

Total Under 143.5
FoxBet: -133 (1.75)
BetMGM: -120 (1.83)

Total Over 143.5
FoxBet: +100 (2.0)
BetMGM: +100 (2.0)

Total Under 144.5
FoxBet: -137 (1.7273)
BetMGM: -135 (1.75)

Total Over 144.5
FoxBet: +105 (2.05)
BetMGM: +110 (2.1)

Total Under 140.5
FoxBet: +105 (2.05)
BetMGM: +110 (2.1)

Total Over 140.5
FoxBet: -137 (1.7273)
BetMGM: -135 (1.75)

Total Over 136.5
BetMGM: -200 (1.5)

Total Under 136.5
BetMGM: +165 (2.65)

Total Over 148.5
BetMGM: +165 (2.65)

Total Under 148.5
BetMGM: -200 (1.5)

Total Over 149.5
BetMGM: +180 (2.8)

Total Under 149.5
BetMGM: -225 (1.45)

Total Over 150.5
BetMGM: +195 (2.95)

Total Under 150.5
BetMGM: -250 (1.42)

Total Over 151.5
BetMGM: +220 (3.2)

Total Under 151.5
BetMGM: -275 (1.36)

Total Over 152.5
BetMGM: +230 (3.3)

Total Under 152.5
BetMGM: -300 (1.34)

Total Over 153.5
BetMGM: +260 (3.6)

Total Under 153.5
BetMGM: -350 (1.3)

Total Over 154.5
BetMGM: +280 (3.8)

Total Under 154.5
BetMGM: -350 (1.28)

Total Over 155.5
BetMGM: +310 (4.1)

Total Under 155.5
BetMGM: -400 (1.25)
Moneyline
Moneyline Idaho None
DraftKings: +850 (9.5)
FanDuel: +980 (10.8)
FoxBet: +800 (9.0)
BetMGM: +800 (9.0)

Moneyline Southern Utah None
DraftKings: -1667 (1.06)
FanDuel: -1800 (1.0556)
FoxBet: -1600 (1.0625)
BetMGM: -1400 (1.07)


, 'Illinois @ Northwestern': College Basketball: Illinois @ Northwestern
DraftKings - 179653612
FanDuel - 949368.3
BetMGM - 10973266
Spread
Spread Illinois -6.5
DraftKings: -110 (1.91)
FanDuel: -106 (1.9434)
BetMGM: -110 (1.91)

Spread Northwestern 6.5
DraftKings: -110 (1.91)
FanDuel: -114 (1.8772)
BetMGM: -110 (1.91)

Spread Illinois -11.5
DraftKings: +200 (3.0)
BetMGM: +200 (3.0)

Spread Northwestern 11.5
DraftKings: -250 (1.4)
BetMGM: -250 (1.4)

Spread Illinois -11.0
DraftKings: +190 (2.9)

Spread Northwestern 11.0
DraftKings: -235 (1.43)

Spread Illinois -10.5
DraftKings: +170 (2.7)
BetMGM: +170 (2.7)

Spread Northwestern 10.5
DraftKings: -210 (1.48)
BetMGM: -200 (1.48)

Spread Illinois -7.0
DraftKings: +100 (2.0)

Spread Northwestern 7.0
DraftKings: -120 (1.84)

Spread Illinois -2.5
DraftKings: -220 (1.46)
BetMGM: -225 (1.45)

Spread Northwestern 2.5
DraftKings: +175 (2.75)
BetMGM: +180 (2.8)

Spread Illinois -2.0
DraftKings: -240 (1.42)

Spread Northwestern 2.0
DraftKings: +190 (2.9)

Spread Illinois -1.5
DraftKings: -250 (1.4)
BetMGM: -250 (1.4)

Spread Northwestern 1.5
DraftKings: +200 (3.0)
BetMGM: +200 (3.0)

Spread Illinois 3.5
BetMGM: -500 (1.2)

Spread Northwestern -3.5
BetMGM: +360 (4.6)

Spread Illinois 2.5
BetMGM: -400 (1.25)

Spread Northwestern -2.5
BetMGM: +310 (4.1)

Spread Illinois 1.5
BetMGM: -350 (1.3)

Spread Northwestern -1.5
BetMGM: +260 (3.6)

Spread Illinois -3.5
BetMGM: -185 (1.55)

Spread Northwestern 3.5
BetMGM: +150 (2.5)

Spread Illinois -4.5
BetMGM: -155 (1.65)

Spread Northwestern 4.5
BetMGM: +125 (2.25)

Spread Illinois -5.5
BetMGM: -135 (1.75)

Spread Northwestern 5.5
BetMGM: +110 (2.1)

Spread Illinois -7.5
BetMGM: +105 (2.05)

Spread Northwestern 7.5
BetMGM: -130 (1.78)

Spread Illinois -8.5
BetMGM: +125 (2.25)

Spread Northwestern 8.5
BetMGM: -150 (1.67)

Spread Illinois -9.5
BetMGM: +145 (2.45)

Spread Northwestern 9.5
BetMGM: -175 (1.57)

Spread Illinois -12.5
BetMGM: +230 (3.3)

Spread Northwestern 12.5
BetMGM: -275 (1.35)

Spread Illinois -13.5
BetMGM: +260 (3.6)

Spread Northwestern 13.5
BetMGM: -350 (1.3)

Spread Illinois -14.5
BetMGM: +310 (4.1)

Spread Northwestern 14.5
BetMGM: -400 (1.25)

Spread Illinois -15.5
BetMGM: +360 (4.6)

Spread Northwestern 15.5
BetMGM: -500 (1.2)

Spread Illinois -16.5
BetMGM: +425 (5.25)

Spread Northwestern 16.5
BetMGM: -600 (1.17)

Spread Illinois -17.5
BetMGM: +500 (6.0)

Spread Northwestern 17.5
BetMGM: -700 (1.14)
Total
Total Over 149.0
DraftKings: -107 (1.94)

Total Under 149.0
DraftKings: -113 (1.89)

Total Over 143.5
DraftKings: -195 (1.52)
BetMGM: -165 (1.6)

Total Under 143.5
DraftKings: +160 (2.6)
BetMGM: +140 (2.4)

Total Over 144.0
DraftKings: -190 (1.53)

Total Under 144.0
DraftKings: +155 (2.55)

Total Over 144.5
DraftKings: -177 (1.57)
BetMGM: -155 (1.65)

Total Under 144.5
DraftKings: +145 (2.45)
BetMGM: +125 (2.25)

Total Over 148.5
DraftKings: -113 (1.89)
BetMGM: -110 (1.91)

Total Under 148.5
DraftKings: -107 (1.94)
BetMGM: -110 (1.91)

Total Over 152.5
DraftKings: +135 (2.35)
BetMGM: +135 (2.35)

Total Under 152.5
DraftKings: -165 (1.61)
BetMGM: -160 (1.62)

Total Over 153.0
DraftKings: +145 (2.45)

Total Under 153.0
DraftKings: -177 (1.57)

Total Over 153.5
DraftKings: +150 (2.5)
BetMGM: +150 (2.5)

Total Under 153.5
DraftKings: -182 (1.55)
BetMGM: -185 (1.55)

Total Over 147.5
FanDuel: -114 (1.8772)
BetMGM: -115 (1.87)

Total Under 147.5
FanDuel: -108 (1.9259)
BetMGM: -105 (1.95)

Total Over 141.5
BetMGM: -200 (1.48)

Total Under 141.5
BetMGM: +170 (2.7)

Total Over 142.5
BetMGM: -190 (1.53)

Total Under 142.5
BetMGM: +155 (2.55)

Total Over 145.5
BetMGM: -140 (1.72)

Total Under 145.5
BetMGM: +115 (2.15)

Total Over 146.5
BetMGM: -130 (1.78)

Total Under 146.5
BetMGM: +105 (2.05)

Total Over 149.5
BetMGM: +105 (2.05)

Total Under 149.5
BetMGM: -125 (1.8)

Total Over 150.5
BetMGM: +115 (2.15)

Total Under 150.5
BetMGM: -140 (1.72)

Total Over 151.5
BetMGM: +125 (2.25)

Total Under 151.5
BetMGM: -150 (1.67)

Total Over 154.5
BetMGM: +165 (2.65)

Total Under 154.5
BetMGM: -200 (1.5)

Total Over 155.5
BetMGM: +180 (2.8)

Total Under 155.5
BetMGM: -225 (1.45)

Total Over 156.5
BetMGM: +195 (2.95)

Total Under 156.5
BetMGM: -250 (1.42)

Total Over 157.5
BetMGM: +220 (3.2)

Total Under 157.5
BetMGM: -275 (1.36)

Total Over 158.5
BetMGM: +230 (3.3)

Total Under 158.5
BetMGM: -300 (1.34)

Total Over 159.5
BetMGM: +260 (3.6)

Total Under 159.5
BetMGM: -350 (1.3)

Total Over 160.5
BetMGM: +275 (3.75)

Total Under 160.5
BetMGM: -350 (1.28)
Moneyline
Moneyline Illinois None
DraftKings: -295 (1.34)
FanDuel: -290 (1.3448)
BetMGM: -300 (1.33)

Moneyline Northwestern None
DraftKings: +230 (3.3)
FanDuel: +235 (3.35)
BetMGM: +240 (3.4)


, 'Washington @ Stanford': College Basketball: Washington @ Stanford
DraftKings - 179653616
FanDuel - 949388.3
FoxBet - 8715870
Spread
Spread Washington 10.5
DraftKings: -110 (1.91)
FanDuel: -115 (1.8696)
FoxBet: -125 (1.8)

Spread Stanford -10.5
DraftKings: -110 (1.91)
FanDuel: -105 (1.9524)
FoxBet: -105 (1.95)

Spread Washington 5.5
DraftKings: +200 (3.0)

Spread Stanford -5.5
DraftKings: -250 (1.4)

Spread Washington 6.0
DraftKings: +190 (2.9)

Spread Stanford -6.0
DraftKings: -240 (1.42)

Spread Washington 6.5
DraftKings: +175 (2.75)
FoxBet: +155 (2.55)

Spread Stanford -6.5
DraftKings: -215 (1.47)
FoxBet: -212 (1.4706)

Spread Washington 11.0
DraftKings: -120 (1.84)
FoxBet: -133 (1.75)

Spread Stanford -11.0
DraftKings: -103 (1.98)
FoxBet: +100 (2.0)

Spread Washington 14.5
DraftKings: -205 (1.49)

Spread Stanford -14.5
DraftKings: +165 (2.65)

Spread Washington 15.0
DraftKings: -230 (1.44)

Spread Stanford -15.0
DraftKings: +185 (2.85)

Spread Washington 15.5
DraftKings: -245 (1.41)

Spread Stanford -15.5
DraftKings: +195 (2.95)

Spread Washington 12.0
FoxBet: -150 (1.6667)

Spread Stanford -12.0
FoxBet: +115 (2.15)

Spread Stanford -13.0
FoxBet: +135 (2.35)

Spread Washington 13.0
FoxBet: -182 (1.55)

Spread Stanford -11.5
FoxBet: +105 (2.05)

Spread Washington 11.5
FoxBet: -137 (1.7273)

Spread Stanford -7.5
FoxBet: -182 (1.55)

Spread Washington 7.5
FoxBet: +135 (2.35)

Spread Washington 9.5
FoxBet: +100 (2.0)

Spread Stanford -9.5
FoxBet: -133 (1.75)

Spread Stanford -13.5
FoxBet: +145 (2.45)

Spread Washington 13.5
FoxBet: -200 (1.5)

Spread Washington 10.0
FoxBet: -105 (1.95)

Spread Stanford -10.0
FoxBet: -125 (1.8)

Spread Washington 7.0
FoxBet: +145 (2.45)

Spread Stanford -7.0
FoxBet: -200 (1.5)

Spread Stanford -9.0
FoxBet: -143 (1.7)

Spread Washington 9.0
FoxBet: +110 (2.1)

Spread Washington 8.0
FoxBet: +130 (2.3)

Spread Stanford -8.0
FoxBet: -175 (1.5714)

Spread Stanford -8.5
FoxBet: -150 (1.6667)

Spread Washington 8.5
FoxBet: +115 (2.15)

Spread Washington 12.5
FoxBet: -162 (1.6154)

Spread Stanford -12.5
FoxBet: +125 (2.25)
Total
Total Over 137.5
DraftKings: -108 (1.93)
FanDuel: -106 (1.9434)
FoxBet: -105 (1.95)

Total Under 137.5
DraftKings: -113 (1.89)
FanDuel: -114 (1.8772)
FoxBet: -125 (1.8)

Total Over 132.5
DraftKings: -190 (1.53)

Total Under 132.5
DraftKings: +155 (2.55)

Total Over 133.0
DraftKings: -182 (1.55)

Total Under 133.0
DraftKings: +150 (2.5)

Total Over 133.5
DraftKings: -167 (1.6)
FoxBet: -162 (1.6154)

Total Under 133.5
DraftKings: +138 (2.38)
FoxBet: +125 (2.25)

Total Over 137.0
DraftKings: -114 (1.88)
FoxBet: -118 (1.85)

Total Under 137.0
DraftKings: -106 (1.95)
FoxBet: -110 (1.9091)

Total Over 141.5
DraftKings: +145 (2.45)

Total Under 141.5
DraftKings: -177 (1.57)

Total Over 142.0
DraftKings: +155 (2.55)

Total Under 142.0
DraftKings: -190 (1.53)

Total Over 142.5
DraftKings: +160 (2.6)

Total Under 142.5
DraftKings: -195 (1.52)

Total Under 134.5
FoxBet: +110 (2.1)

Total Over 134.5
FoxBet: -143 (1.7)

Total Under 136.5
FoxBet: -105 (1.95)

Total Over 136.5
FoxBet: -125 (1.8)

Total Under 138.5
FoxBet: -133 (1.75)

Total Over 138.5
FoxBet: +100 (2.0)

Total Under 135.5
FoxBet: +105 (2.05)

Total Over 135.5
FoxBet: -137 (1.7273)

Total Under 134.0
FoxBet: +120 (2.2)

Total Over 134.0
FoxBet: -154 (1.65)

Total Under 135.0
FoxBet: +105 (2.05)

Total Over 135.0
FoxBet: -137 (1.7273)

Total Over 136.0
FoxBet: -133 (1.75)

Total Under 136.0
FoxBet: +100 (2.0)

Total Under 138.0
FoxBet: -125 (1.8)

Total Over 138.0
FoxBet: -105 (1.95)

Total Under 139.0
FoxBet: -137 (1.7273)

Total Over 139.0
FoxBet: +105 (2.05)

Total Under 140.5
FoxBet: -154 (1.65)

Total Over 140.5
FoxBet: +120 (2.2)

Total Over 140.0
FoxBet: +115 (2.15)

Total Under 140.0
FoxBet: -150 (1.6667)

Total Under 139.5
FoxBet: -137 (1.7273)

Total Over 139.5
FoxBet: +105 (2.05)
Moneyline
Moneyline Washington None
DraftKings: +460 (5.6)
FanDuel: +480 (5.8)
FoxBet: +450 (5.5)

Moneyline Stanford None
DraftKings: -670 (1.15)
FanDuel: -650 (1.1538)
FoxBet: -650 (1.1538)


, 'Portland @ San Francisco': College Basketball: Portland @ San Francisco
DraftKings - 179653613
FanDuel - 949408.3
FoxBet - 8716434
Spread
Spread Portland 17.5
DraftKings: -109 (1.92)
FanDuel: -114 (1.8772)
FoxBet: -125 (1.8)

Spread San Francisco -17.5
DraftKings: -110 (1.91)
FanDuel: -106 (1.9434)
FoxBet: -105 (1.95)

Spread Portland 12.5
DraftKings: +180 (2.8)

Spread San Francisco -12.5
DraftKings: -225 (1.45)

Spread Portland 13.0
DraftKings: +175 (2.75)

Spread San Francisco -13.0
DraftKings: -215 (1.47)

Spread Portland 13.5
DraftKings: +155 (2.55)

Spread San Francisco -13.5
DraftKings: -195 (1.52)

Spread Portland 18.0
DraftKings: -118 (1.85)
FoxBet: -133 (1.75)

Spread San Francisco -18.0
DraftKings: -103 (1.98)
FoxBet: +100 (2.0)

Spread Portland 21.5
DraftKings: -195 (1.52)

Spread San Francisco -21.5
DraftKings: +155 (2.55)

Spread Portland 22.0
DraftKings: -215 (1.47)

Spread San Francisco -22.0
DraftKings: +175 (2.75)

Spread Portland 22.5
DraftKings: -225 (1.45)

Spread San Francisco -22.5
DraftKings: +180 (2.8)

Spread San Francisco -20.5
FoxBet: +135 (2.35)

Spread Portland 20.5
FoxBet: -182 (1.55)

Spread Portland 21.0
FoxBet: -200 (1.5)

Spread San Francisco -21.0
FoxBet: +145 (2.45)

Spread San Francisco -20.0
FoxBet: +130 (2.3)

Spread Portland 20.0
FoxBet: -175 (1.5714)

Spread San Francisco -14.0
FoxBet: -188 (1.5333)

Spread Portland 14.0
FoxBet: +140 (2.4)

Spread San Francisco -16.0
FoxBet: -137 (1.7273)

Spread Portland 16.0
FoxBet: +105 (2.05)

Spread San Francisco -15.0
FoxBet: -154 (1.65)

Spread Portland 15.0
FoxBet: +120 (2.2)

Spread San Francisco -17.0
FoxBet: -125 (1.8)

Spread Portland 17.0
FoxBet: -105 (1.95)

Spread San Francisco -19.0
FoxBet: +110 (2.1)

Spread Portland 19.0
FoxBet: -143 (1.7)

Spread San Francisco -19.5
FoxBet: +120 (2.2)

Spread Portland 19.5
FoxBet: -154 (1.65)

Spread San Francisco -15.5
FoxBet: -143 (1.7)

Spread Portland 15.5
FoxBet: +110 (2.1)

Spread San Francisco -16.5
FoxBet: -133 (1.75)

Spread Portland 16.5
FoxBet: +100 (2.0)

Spread San Francisco -18.5
FoxBet: +105 (2.05)

Spread Portland 18.5
FoxBet: -137 (1.7273)

Spread Portland 14.5
FoxBet: +130 (2.3)

Spread San Francisco -14.5
FoxBet: -175 (1.5714)
Total
Total Over 147.5
DraftKings: -110 (1.91)
FanDuel: -110 (1.9091)
FoxBet: -110 (1.9091)

Total Under 147.5
DraftKings: -110 (1.91)
FanDuel: -110 (1.9091)
FoxBet: -110 (1.9091)

Total Over 142.5
DraftKings: -195 (1.52)

Total Under 142.5
DraftKings: +155 (2.55)

Total Over 143.0
DraftKings: -186 (1.54)

Total Under 143.0
DraftKings: +150 (2.5)

Total Over 143.5
DraftKings: -175 (1.58)

Total Under 143.5
DraftKings: +143 (2.43)

Total Over 148.0
DraftKings: -104 (1.97)
FoxBet: -105 (1.95)

Total Under 148.0
DraftKings: -117 (1.86)
FoxBet: -125 (1.8)

Total Over 151.5
DraftKings: +143 (2.43)

Total Under 151.5
DraftKings: -175 (1.58)

Total Over 152.0
DraftKings: +150 (2.5)

Total Under 152.0
DraftKings: -186 (1.54)

Total Over 152.5
DraftKings: +155 (2.55)

Total Under 152.5
DraftKings: -195 (1.52)

Total Under 150.0
FoxBet: -143 (1.7)

Total Over 150.0
FoxBet: +110 (2.1)

Total Under 151.0
FoxBet: -154 (1.65)

Total Over 151.0
FoxBet: +120 (2.2)

Total Over 148.5
FoxBet: -105 (1.95)

Total Under 148.5
FoxBet: -125 (1.8)

Total Under 145.5
FoxBet: +105 (2.05)

Total Over 145.5
FoxBet: -137 (1.7273)

Total Under 150.5
FoxBet: -150 (1.6667)

Total Over 150.5
FoxBet: +115 (2.15)

Total Under 149.5
FoxBet: -137 (1.7273)

Total Over 149.5
FoxBet: +105 (2.05)

Total Under 144.5
FoxBet: +115 (2.15)

Total Over 144.5
FoxBet: -150 (1.6667)

Total Over 146.5
FoxBet: -125 (1.8)

Total Under 146.5
FoxBet: -105 (1.95)

Total Under 144.0
FoxBet: +120 (2.2)

Total Over 144.0
FoxBet: -154 (1.65)

Total Under 145.0
FoxBet: +110 (2.1)

Total Over 145.0
FoxBet: -143 (1.7)

Total Under 146.0
FoxBet: +100 (2.0)

Total Over 146.0
FoxBet: -133 (1.75)

Total Under 147.0
FoxBet: -105 (1.95)

Total Over 147.0
FoxBet: -125 (1.8)

Total Under 149.0
FoxBet: -137 (1.7273)

Total Over 149.0
FoxBet: +100 (2.0)
Moneyline
Moneyline Portland None
DraftKings: +1150 (12.5)
FanDuel: +1500 (16.0)
FoxBet: +1200 (13.0)

Moneyline San Francisco None
DraftKings: -2500 (1.04)
FanDuel: -4000 (1.025)
FoxBet: -3300 (1.0303)


, 'Washington State @ California': College Basketball: Washington State @ California
DraftKings - 179653620
FanDuel - 949376.3
FoxBet - 8715830
BetMGM - 10989428
Spread
Spread Washington State -2.5
DraftKings: -112 (1.9)
FanDuel: -106 (1.9434)
FoxBet: -105 (1.95)
BetMGM: -110 (1.91)

Spread California 2.5
DraftKings: -109 (1.92)
FanDuel: -114 (1.8772)
FoxBet: -125 (1.8)
BetMGM: -110 (1.91)

Spread Washington State -7.5
DraftKings: +210 (3.1)
BetMGM: +220 (3.2)

Spread California 7.5
DraftKings: -265 (1.38)
BetMGM: -275 (1.36)

Spread Washington State -7.0
DraftKings: +195 (2.95)

Spread California 7.0
DraftKings: -245 (1.41)

Spread Washington State -6.5
DraftKings: +175 (2.75)
FoxBet: +165 (2.65)
BetMGM: +180 (2.8)

Spread California 6.5
DraftKings: -220 (1.46)
FoxBet: -225 (1.4444)
BetMGM: -225 (1.45)

Spread Washington State -2.0
DraftKings: -121 (1.83)
FoxBet: -125 (1.8)

Spread California 2.0
DraftKings: +100 (2.0)
FoxBet: -105 (1.95)

Spread Washington State 1.5
DraftKings: -165 (1.61)
FoxBet: -162 (1.6154)
BetMGM: -160 (1.62)

Spread California -1.5
DraftKings: +135 (2.35)
FoxBet: +125 (2.25)
BetMGM: +135 (2.35)

Spread Washington State 2.0
DraftKings: -180 (1.56)

Spread California -2.0
DraftKings: +148 (2.48)

Spread Washington State 2.5
DraftKings: -195 (1.52)
BetMGM: -190 (1.53)

Spread California -2.5
DraftKings: +155 (2.55)
BetMGM: +155 (2.55)

Spread Washington State -6.0
FoxBet: +155 (2.55)

Spread California 6.0
FoxBet: -212 (1.4706)

Spread California 5.0
FoxBet: -182 (1.55)

Spread Washington State -5.0
FoxBet: +135 (2.35)

Spread California 1.0
FoxBet: +105 (2.05)

Spread Washington State -1.0
FoxBet: -137 (1.7273)

Spread Washington State -4.0
FoxBet: +115 (2.15)

Spread California 4.0
FoxBet: -150 (1.6667)

Spread Washington State -3.0
FoxBet: +100 (2.0)

Spread California 3.0
FoxBet: -133 (1.75)

Spread Washington State 0.0
FoxBet: -143 (1.7)

Spread California 0.0
FoxBet: +110 (2.1)

Spread Washington State -1.5
FoxBet: -133 (1.75)
BetMGM: -125 (1.8)

Spread California 1.5
FoxBet: +100 (2.0)
BetMGM: +105 (2.05)

Spread California 3.5
FoxBet: -137 (1.7273)
BetMGM: -135 (1.75)

Spread Washington State -3.5
FoxBet: +105 (2.05)
BetMGM: +110 (2.1)

Spread Washington State -5.5
FoxBet: +145 (2.45)
BetMGM: +155 (2.55)

Spread California 5.5
FoxBet: -200 (1.5)
BetMGM: -190 (1.53)

Spread California -1.0
FoxBet: +120 (2.2)

Spread Washington State 1.0
FoxBet: -154 (1.65)

Spread Washington State -4.5
FoxBet: +125 (2.25)
BetMGM: +125 (2.25)

Spread California 4.5
FoxBet: -162 (1.6154)
BetMGM: -155 (1.65)

Spread Washington State 11.5
BetMGM: -1100 (1.09)

Spread California -11.5
BetMGM: +675 (7.75)

Spread Washington State 10.5
BetMGM: -900 (1.11)

Spread California -10.5
BetMGM: +575 (6.75)

Spread Washington State 9.5
BetMGM: -700 (1.14)

Spread California -9.5
BetMGM: +500 (6.0)

Spread Washington State 8.5
BetMGM: -600 (1.17)

Spread California -8.5
BetMGM: +425 (5.25)

Spread Washington State 7.5
BetMGM: -450 (1.22)

Spread California -7.5
BetMGM: +350 (4.5)

Spread Washington State 6.5
BetMGM: -375 (1.26)

Spread California -6.5
BetMGM: +300 (4.0)

Spread Washington State 5.5
BetMGM: -350 (1.3)

Spread California -5.5
BetMGM: +260 (3.6)

Spread Washington State 4.5
BetMGM: -275 (1.36)

Spread California -4.5
BetMGM: +220 (3.2)

Spread Washington State 3.5
BetMGM: -225 (1.44)

Spread California -3.5
BetMGM: +185 (2.85)

Spread Washington State -8.5
BetMGM: +260 (3.6)

Spread California 8.5
BetMGM: -350 (1.3)

Spread Washington State -9.5
BetMGM: +290 (3.9)

Spread California 9.5
BetMGM: -375 (1.26)
Total
Total Over 128
DraftKings: -107 (1.94)

Total Under 128
DraftKings: -113 (1.89)

Total Over 123
DraftKings: -195 (1.52)

Total Under 123
DraftKings: +155 (2.55)

Total Over 123.5
DraftKings: -180 (1.56)
BetMGM: -190 (1.53)

Total Under 123.5
DraftKings: +148 (2.48)
BetMGM: +155 (2.55)

Total Over 124
DraftKings: -175 (1.58)

Total Under 124
DraftKings: +140 (2.4)

Total Over 127.5
DraftKings: -114 (1.88)
FoxBet: -125 (1.8)
BetMGM: -130 (1.78)

Total Under 127.5
DraftKings: -107 (1.94)
FoxBet: -105 (1.95)
BetMGM: +105 (2.05)

Total Over 132
DraftKings: +145 (2.45)

Total Under 132
DraftKings: -177 (1.57)

Total Over 132.5
DraftKings: +150 (2.5)
BetMGM: +125 (2.25)

Total Under 132.5
DraftKings: -186 (1.54)
BetMGM: -155 (1.65)

Total Over 133
DraftKings: +163 (2.63)

Total Under 133
DraftKings: -200 (1.5)

Total Over 128.5
FanDuel: -110 (1.9091)
FoxBet: -110 (1.9091)
BetMGM: -115 (1.87)

Total Under 128.5
FanDuel: -110 (1.9091)
FoxBet: -110 (1.9091)
BetMGM: -105 (1.95)

Total Under 130.5
FoxBet: -137 (1.7273)
BetMGM: -125 (1.8)

Total Over 130.5
FoxBet: +105 (2.05)
BetMGM: +105 (2.05)

Total Under 129.5
FoxBet: -133 (1.75)
BetMGM: -115 (1.87)

Total Over 129.5
FoxBet: +100 (2.0)
BetMGM: -105 (1.95)

Total Under 125.5
FoxBet: +115 (2.15)
BetMGM: +125 (2.25)

Total Over 125.5
FoxBet: -150 (1.6667)
BetMGM: -155 (1.65)

Total Under 125.0
FoxBet: +120 (2.2)

Total Over 125.0
FoxBet: -154 (1.65)

Total Under 126.0
FoxBet: +110 (2.1)

Total Over 126.0
FoxBet: -143 (1.7)

Total Under 127.0
FoxBet: +100 (2.0)

Total Over 127.0
FoxBet: -133 (1.75)

Total Under 128.0
FoxBet: -105 (1.95)

Total Over 128.0
FoxBet: -125 (1.8)

Total Under 129.0
FoxBet: -125 (1.8)

Total Over 129.0
FoxBet: -105 (1.95)

Total Under 131.5
FoxBet: -150 (1.6667)
BetMGM: -140 (1.72)

Total Over 131.5
FoxBet: +115 (2.15)
BetMGM: +115 (2.15)

Total Over 130.0
FoxBet: +105 (2.05)

Total Under 130.0
FoxBet: -137 (1.7273)

Total Under 131.0
FoxBet: -143 (1.7)

Total Over 131.0
FoxBet: +110 (2.1)

Total Over 132.0
FoxBet: +125 (2.25)

Total Under 132.0
FoxBet: -162 (1.6154)

Total Under 126.5
FoxBet: +105 (2.05)
BetMGM: +115 (2.15)

Total Over 126.5
FoxBet: -137 (1.7273)
BetMGM: -140 (1.72)

Total Over 120.5
BetMGM: -275 (1.36)

Total Under 120.5
BetMGM: +220 (3.2)

Total Over 121.5
BetMGM: -250 (1.42)

Total Under 121.5
BetMGM: +190 (2.9)

Total Over 122.5
BetMGM: -200 (1.48)

Total Under 122.5
BetMGM: +170 (2.7)

Total Over 124.5
BetMGM: -175 (1.57)

Total Under 124.5
BetMGM: +145 (2.45)

Total Over 133.5
BetMGM: +140 (2.4)

Total Under 133.5
BetMGM: -165 (1.6)

Total Over 134.5
BetMGM: +155 (2.55)

Total Under 134.5
BetMGM: -190 (1.53)

Total Over 135.5
BetMGM: +170 (2.7)

Total Under 135.5
BetMGM: -200 (1.48)

Total Over 136.5
BetMGM: +185 (2.85)

Total Under 136.5
BetMGM: -225 (1.44)

Total Over 137.5
BetMGM: +200 (3.0)

Total Under 137.5
BetMGM: -250 (1.4)

Total Over 138.5
BetMGM: +225 (3.25)

Total Under 138.5
BetMGM: -275 (1.36)

Total Over 139.5
BetMGM: +240 (3.4)

Total Under 139.5
BetMGM: -300 (1.33)
Moneyline
Moneyline Washington State None
DraftKings: -148 (1.68)
FanDuel: -142 (1.7042)
FoxBet: -137 (1.7273)
BetMGM: -140 (1.72)

Moneyline California None
DraftKings: +120 (2.2)
FanDuel: +120 (2.2)
FoxBet: +115 (2.15)
BetMGM: +115 (2.15)


, 'Nevada @ San Diego State': College Basketball: Nevada @ San Diego State
DraftKings - 179653619
FanDuel - 949372.3
FoxBet - 8715814
BetMGM - 10973272
Spread
Spread Nevada 13.0
DraftKings: -109 (1.92)
FoxBet: -105 (1.95)

Spread San Diego State -13.0
DraftKings: -110 (1.91)
FoxBet: -125 (1.8)

Spread Nevada 8.0
DraftKings: +200 (3.0)

Spread San Diego State -8.0
DraftKings: -250 (1.4)

Spread Nevada 8.5
DraftKings: +185 (2.85)
BetMGM: +195 (2.95)

Spread San Diego State -8.5
DraftKings: -230 (1.44)
BetMGM: -250 (1.42)

Spread Nevada 9.0
DraftKings: +175 (2.75)

Spread San Diego State -9.0
DraftKings: -215 (1.47)

Spread Nevada 13.5
DraftKings: -118 (1.85)
FanDuel: -114 (1.8772)
FoxBet: -125 (1.8)
BetMGM: -110 (1.91)

Spread San Diego State -13.5
DraftKings: -103 (1.98)
FanDuel: -106 (1.9434)
FoxBet: -105 (1.95)
BetMGM: -110 (1.91)

Spread Nevada 17.0
DraftKings: -210 (1.48)
FoxBet: -212 (1.4706)

Spread San Diego State -17.0
DraftKings: +170 (2.7)
FoxBet: +155 (2.55)

Spread Nevada 17.5
DraftKings: -225 (1.45)
BetMGM: -200 (1.5)

Spread San Diego State -17.5
DraftKings: +180 (2.8)
BetMGM: +165 (2.65)

Spread Nevada 18.0
DraftKings: -250 (1.4)

Spread San Diego State -18.0
DraftKings: +200 (3.0)

Spread Nevada 12.0
FoxBet: +105 (2.05)

Spread San Diego State -12.0
FoxBet: -137 (1.7273)

Spread San Diego State -11.0
FoxBet: -162 (1.6154)

Spread Nevada 11.0
FoxBet: +125 (2.25)

Spread Nevada 14.0
FoxBet: -133 (1.75)

Spread San Diego State -14.0
FoxBet: +100 (2.0)

Spread San Diego State -16.0
FoxBet: +130 (2.3)

Spread Nevada 16.0
FoxBet: -175 (1.5714)

Spread San Diego State -15.0
FoxBet: +115 (2.15)

Spread Nevada 15.0
FoxBet: -150 (1.6667)

Spread San Diego State -11.5
FoxBet: -150 (1.6667)
BetMGM: -150 (1.67)

Spread Nevada 11.5
FoxBet: +115 (2.15)
BetMGM: +125 (2.25)

Spread Nevada 15.5
FoxBet: -154 (1.65)
BetMGM: -150 (1.67)

Spread San Diego State -15.5
FoxBet: +120 (2.2)
BetMGM: +125 (2.25)

Spread San Diego State -10.5
FoxBet: -182 (1.55)
BetMGM: -175 (1.57)

Spread Nevada 10.5
FoxBet: +135 (2.35)
BetMGM: +145 (2.45)

Spread San Diego State -16.5
FoxBet: +140 (2.4)
BetMGM: +145 (2.45)

Spread Nevada 16.5
FoxBet: -188 (1.5333)
BetMGM: -175 (1.57)

Spread Nevada 12.5
FoxBet: +100 (2.0)
BetMGM: +105 (2.05)

Spread San Diego State -12.5
FoxBet: -133 (1.75)
BetMGM: -130 (1.78)

Spread San Diego State -10.0
FoxBet: -200 (1.5)

Spread Nevada 10.0
FoxBet: +145 (2.45)

Spread San Diego State -14.5
FoxBet: +105 (2.05)
BetMGM: +105 (2.05)

Spread Nevada 14.5
FoxBet: -137 (1.7273)
BetMGM: -130 (1.78)

Spread Nevada 22.5
BetMGM: -450 (1.22)

Spread San Diego State -22.5
BetMGM: +350 (4.5)

Spread Nevada 21.5
BetMGM: -375 (1.26)

Spread San Diego State -21.5
BetMGM: +300 (4.0)

Spread Nevada 20.5
BetMGM: -350 (1.3)

Spread San Diego State -20.5
BetMGM: +260 (3.6)

Spread Nevada 19.5
BetMGM: -275 (1.36)

Spread San Diego State -19.5
BetMGM: +220 (3.2)

Spread Nevada 18.5
BetMGM: -250 (1.42)

Spread San Diego State -18.5
BetMGM: +190 (2.9)

Spread Nevada 9.5
BetMGM: +170 (2.7)

Spread San Diego State -9.5
BetMGM: -200 (1.48)

Spread Nevada 7.5
BetMGM: +230 (3.3)

Spread San Diego State -7.5
BetMGM: -300 (1.34)

Spread Nevada 6.5
BetMGM: +275 (3.75)

Spread San Diego State -6.5
BetMGM: -350 (1.28)

Spread Nevada 5.5
BetMGM: +333 (4.33)

Spread San Diego State -5.5
BetMGM: -450 (1.22)

Spread Nevada 4.5
BetMGM: +375 (4.75)

Spread San Diego State -4.5
BetMGM: -500 (1.2)

Spread Nevada 3.5
BetMGM: +475 (5.75)

Spread San Diego State -3.5
BetMGM: -650 (1.15)
Total
Total Over 136.5
DraftKings: -108 (1.93)
FoxBet: -125 (1.8)
BetMGM: -115 (1.87)

Total Under 136.5
DraftKings: -113 (1.89)
FoxBet: -105 (1.95)
BetMGM: -105 (1.95)

Total Over 131.5
DraftKings: -190 (1.53)
BetMGM: -190 (1.53)

Total Under 131.5
DraftKings: +155 (2.55)
BetMGM: +155 (2.55)

Total Over 132.0
DraftKings: -182 (1.55)

Total Under 132.0
DraftKings: +150 (2.5)

Total Over 132.5
DraftKings: -167 (1.6)
BetMGM: -165 (1.6)

Total Under 132.5
DraftKings: +138 (2.38)
BetMGM: +140 (2.4)

Total Over 136.0
DraftKings: -114 (1.88)
FoxBet: -133 (1.75)

Total Under 136.0
DraftKings: -106 (1.95)
FoxBet: +100 (2.0)

Total Over 140.5
DraftKings: +145 (2.45)
FoxBet: +120 (2.2)
BetMGM: +125 (2.25)

Total Under 140.5
DraftKings: -177 (1.57)
FoxBet: -154 (1.65)
BetMGM: -155 (1.65)

Total Over 141.0
DraftKings: +155 (2.55)

Total Under 141.0
DraftKings: -190 (1.53)

Total Over 141.5
DraftKings: +163 (2.63)
BetMGM: +140 (2.4)

Total Under 141.5
DraftKings: -200 (1.5)
BetMGM: -165 (1.6)

Total Over 137.5
FanDuel: -106 (1.9434)
FoxBet: -105 (1.95)
BetMGM: -105 (1.95)

Total Under 137.5
FanDuel: -114 (1.8772)
FoxBet: -125 (1.8)
BetMGM: -115 (1.87)

Total Under 134.5
FoxBet: +110 (2.1)
BetMGM: +115 (2.15)

Total Over 134.5
FoxBet: -143 (1.7)
BetMGM: -140 (1.72)

Total Under 138.5
FoxBet: -133 (1.75)
BetMGM: -130 (1.78)

Total Over 138.5
FoxBet: +100 (2.0)
BetMGM: +105 (2.05)

Total Under 133.5
FoxBet: +125 (2.25)
BetMGM: +125 (2.25)

Total Over 133.5
FoxBet: -162 (1.6154)
BetMGM: -155 (1.65)

Total Under 135.5
FoxBet: +105 (2.05)
BetMGM: +105 (2.05)

Total Over 135.5
FoxBet: -137 (1.7273)
BetMGM: -125 (1.8)

Total Under 134.0
FoxBet: +120 (2.2)

Total Over 134.0
FoxBet: -154 (1.65)

Total Under 135.0
FoxBet: +105 (2.05)

Total Over 135.0
FoxBet: -137 (1.7273)

Total Over 137.0
FoxBet: -110 (1.9091)

Total Under 137.0
FoxBet: -110 (1.9091)

Total Under 138.0
FoxBet: -125 (1.8)

Total Over 138.0
FoxBet: -105 (1.95)

Total Under 139.0
FoxBet: -137 (1.7273)

Total Over 139.0
FoxBet: +105 (2.05)

Total Under 140.0
FoxBet: -150 (1.6667)

Total Over 140.0
FoxBet: +115 (2.15)

Total Under 139.5
FoxBet: -143 (1.7)
BetMGM: -140 (1.72)

Total Over 139.5
FoxBet: +110 (2.1)
BetMGM: +115 (2.15)

Total Over 128.5
BetMGM: -250 (1.4)

Total Under 128.5
BetMGM: +200 (3.0)

Total Over 129.5
BetMGM: -225 (1.44)

Total Under 129.5
BetMGM: +185 (2.85)

Total Over 130.5
BetMGM: -200 (1.48)

Total Under 130.5
BetMGM: +170 (2.7)

Total Over 142.5
BetMGM: +155 (2.55)

Total Under 142.5
BetMGM: -190 (1.53)

Total Over 143.5
BetMGM: +170 (2.7)

Total Under 143.5
BetMGM: -200 (1.48)

Total Over 144.5
BetMGM: +185 (2.85)

Total Under 144.5
BetMGM: -225 (1.44)

Total Over 145.5
BetMGM: +200 (3.0)

Total Under 145.5
BetMGM: -250 (1.4)

Total Over 146.5
BetMGM: +225 (3.25)

Total Under 146.5
BetMGM: -275 (1.35)

Total Over 147.5
BetMGM: +240 (3.4)

Total Under 147.5
BetMGM: -300 (1.33)
Moneyline
Moneyline Nevada None
DraftKings: +700 (8.0)
FanDuel: +830 (9.3)
FoxBet: +650 (7.5)
BetMGM: +750 (8.5)

Moneyline San Diego State None
DraftKings: -1115 (1.09)
FanDuel: -1400 (1.0714)
FoxBet: -1200 (1.0833)
BetMGM: -1200 (1.08)


, 'UCLA @ Arizona State': College Basketball: UCLA @ Arizona State
DraftKings - 179653617
FanDuel - 949379.3
FoxBet - 8715826
BetMGM - 10973273
Spread
Spread UCLA -2.5
DraftKings: -112 (1.9)
FanDuel: -110 (1.9091)
FoxBet: -110 (1.9091)
BetMGM: +105 (2.05)

Spread Arizona State 2.5
DraftKings: -109 (1.92)
FanDuel: -110 (1.9091)
FoxBet: -110 (1.9091)
BetMGM: -130 (1.78)

Spread UCLA -7.5
DraftKings: +200 (3.0)
BetMGM: +240 (3.4)

Spread Arizona State 7.5
DraftKings: -250 (1.4)
BetMGM: -300 (1.33)

Spread UCLA -7.0
DraftKings: +190 (2.9)

Spread Arizona State 7.0
DraftKings: -240 (1.42)

Spread UCLA -6.5
DraftKings: +175 (2.75)
FoxBet: +155 (2.55)
BetMGM: +200 (3.0)

Spread Arizona State 6.5
DraftKings: -215 (1.47)
FoxBet: -212 (1.4706)
BetMGM: -250 (1.4)

Spread UCLA -2.0
DraftKings: -120 (1.84)
FoxBet: -125 (1.8)

Spread Arizona State 2.0
DraftKings: +100 (2.0)
FoxBet: -105 (1.95)

Spread UCLA 1.5
DraftKings: -162 (1.62)
FoxBet: -175 (1.5714)
BetMGM: -145 (1.7)

Spread Arizona State -1.5
DraftKings: +133 (2.33)
FoxBet: +130 (2.3)
BetMGM: +120 (2.2)

Spread UCLA 2.0
DraftKings: -180 (1.56)

Spread Arizona State -2.0
DraftKings: +148 (2.48)

Spread UCLA 2.5
DraftKings: -190 (1.53)
BetMGM: -165 (1.6)

Spread Arizona State -2.5
DraftKings: +155 (2.55)
BetMGM: +140 (2.4)

Spread Arizona State 6.0
FoxBet: -212 (1.4706)

Spread UCLA -6.0
FoxBet: +155 (2.55)

Spread UCLA -5.0
FoxBet: +130 (2.3)

Spread Arizona State 5.0
FoxBet: -175 (1.5714)

Spread UCLA -1.0
FoxBet: -143 (1.7)

Spread Arizona State 1.0
FoxBet: +110 (2.1)

Spread UCLA -4.0
FoxBet: +110 (2.1)

Spread Arizona State 4.0
FoxBet: -143 (1.7)

Spread UCLA -3.0
FoxBet: -105 (1.95)

Spread Arizona State 3.0
FoxBet: -125 (1.8)

Spread Arizona State 0.0
FoxBet: +115 (2.15)

Spread UCLA 0.0
FoxBet: -150 (1.6667)

Spread Arizona State 1.5
FoxBet: +105 (2.05)
BetMGM: -110 (1.91)

Spread UCLA -1.5
FoxBet: -137 (1.7273)
BetMGM: -110 (1.91)

Spread UCLA -3.5
FoxBet: +105 (2.05)
BetMGM: +125 (2.25)

Spread Arizona State 3.5
FoxBet: -137 (1.7273)
BetMGM: -150 (1.67)

Spread UCLA -5.5
FoxBet: +140 (2.4)
BetMGM: +170 (2.7)

Spread Arizona State 5.5
FoxBet: -188 (1.5333)
BetMGM: -200 (1.48)

Spread Arizona State -1.0
FoxBet: +125 (2.25)

Spread UCLA 1.0
FoxBet: -162 (1.6154)

Spread UCLA -4.5
FoxBet: +120 (2.2)
BetMGM: +145 (2.45)

Spread Arizona State 4.5
FoxBet: -154 (1.65)
BetMGM: -175 (1.57)

Spread UCLA 9.5
BetMGM: -600 (1.17)

Spread Arizona State -9.5
BetMGM: +425 (5.25)

Spread UCLA 8.5
BetMGM: -500 (1.2)

Spread Arizona State -8.5
BetMGM: +360 (4.6)

Spread UCLA 7.5
BetMGM: -400 (1.25)

Spread Arizona State -7.5
BetMGM: +310 (4.1)

Spread UCLA 6.5
BetMGM: -350 (1.3)

Spread Arizona State -6.5
BetMGM: +260 (3.6)

Spread UCLA 5.5
BetMGM: -275 (1.36)

Spread Arizona State -5.5
BetMGM: +220 (3.2)

Spread UCLA 4.5
BetMGM: -250 (1.42)

Spread Arizona State -4.5
BetMGM: +190 (2.9)

Spread UCLA 3.5
BetMGM: -200 (1.5)

Spread Arizona State -3.5
BetMGM: +165 (2.65)

Spread UCLA -8.5
BetMGM: +280 (3.8)

Spread Arizona State 8.5
BetMGM: -350 (1.28)

Spread UCLA -9.5
BetMGM: +333 (4.33)

Spread Arizona State 9.5
BetMGM: -450 (1.22)

Spread UCLA -10.5
BetMGM: +375 (4.75)

Spread Arizona State 10.5
BetMGM: -500 (1.2)

Spread UCLA -11.5
BetMGM: +450 (5.5)

Spread Arizona State 11.5
BetMGM: -650 (1.16)
Total
Total Over 144.0
DraftKings: -109 (1.92)
FoxBet: -110 (1.9091)

Total Under 144.0
DraftKings: -112 (1.9)
FoxBet: -110 (1.9091)

Total Over 139.0
DraftKings: -195 (1.52)

Total Under 139.0
DraftKings: +155 (2.55)

Total Over 139.5
DraftKings: -180 (1.56)
BetMGM: -175 (1.57)

Total Under 139.5
DraftKings: +148 (2.48)
BetMGM: +145 (2.45)

Total Over 140.0
DraftKings: -175 (1.58)

Total Under 140.0
DraftKings: +140 (2.4)

Total Over 143.5
DraftKings: -114 (1.88)
FoxBet: -125 (1.8)
BetMGM: -120 (1.83)

Total Under 143.5
DraftKings: -106 (1.95)
FoxBet: -105 (1.95)
BetMGM: +100 (2.0)

Total Over 148.0
DraftKings: +143 (2.43)

Total Under 148.0
DraftKings: -175 (1.58)

Total Over 148.5
DraftKings: +148 (2.48)
BetMGM: +135 (2.35)

Total Under 148.5
DraftKings: -180 (1.56)
BetMGM: -160 (1.62)

Total Over 149.0
DraftKings: +155 (2.55)

Total Under 149.0
DraftKings: -195 (1.52)

Total Over 144.5
FanDuel: -105 (1.9524)
FoxBet: -105 (1.95)
BetMGM: -110 (1.91)

Total Under 144.5
FanDuel: -115 (1.8696)
FoxBet: -125 (1.8)
BetMGM: -110 (1.91)

Total Under 145.5
FoxBet: -133 (1.75)
BetMGM: -120 (1.83)

Total Over 145.5
FoxBet: +100 (2.0)
BetMGM: +100 (2.0)

Total Over 141.5
FoxBet: -143 (1.7)
BetMGM: -145 (1.7)

Total Under 141.5
FoxBet: +110 (2.1)
BetMGM: +120 (2.2)

Total Over 147.5
FoxBet: +120 (2.2)
BetMGM: +120 (2.2)

Total Under 147.5
FoxBet: -154 (1.65)
BetMGM: -145 (1.7)

Total Under 146.5
FoxBet: -143 (1.7)
BetMGM: -135 (1.75)

Total Over 146.5
FoxBet: +110 (2.1)
BetMGM: +110 (2.1)

Total Over 140.5
FoxBet: -154 (1.65)
BetMGM: -160 (1.62)

Total Under 140.5
FoxBet: +120 (2.2)
BetMGM: +135 (2.35)

Total Under 142.5
FoxBet: +100 (2.0)
BetMGM: +110 (2.1)

Total Over 142.5
FoxBet: -133 (1.75)
BetMGM: -135 (1.75)

Total Over 141.0
FoxBet: -150 (1.6667)

Total Under 141.0
FoxBet: +115 (2.15)

Total Under 142.0
FoxBet: +105 (2.05)

Total Over 142.0
FoxBet: -137 (1.7273)

Total Under 143.0
FoxBet: -105 (1.95)

Total Over 143.0
FoxBet: -125 (1.8)

Total Under 145.0
FoxBet: -125 (1.8)

Total Over 145.0
FoxBet: -105 (1.95)

Total Under 146.0
FoxBet: -137 (1.7273)

Total Over 146.0
FoxBet: +105 (2.05)

Total Over 147.0
FoxBet: +115 (2.15)

Total Under 147.0
FoxBet: -150 (1.6667)

Total Over 149.5
BetMGM: +145 (2.45)

Total Under 149.5
BetMGM: -175 (1.57)

Total Over 150.5
BetMGM: +155 (2.55)

Total Under 150.5
BetMGM: -190 (1.53)

Total Over 151.5
BetMGM: +170 (2.7)

Total Under 151.5
BetMGM: -200 (1.48)

Total Over 152.5
BetMGM: +185 (2.85)

Total Under 152.5
BetMGM: -225 (1.44)

Total Over 153.5
BetMGM: +200 (3.0)

Total Under 153.5
BetMGM: -250 (1.4)

Total Over 154.5
BetMGM: +225 (3.25)

Total Under 154.5
BetMGM: -275 (1.36)

Total Over 155.5
BetMGM: +240 (3.4)

Total Under 155.5
BetMGM: -300 (1.33)

Total Over 156.5
BetMGM: +260 (3.6)

Total Under 156.5
BetMGM: -350 (1.3)

Total Over 157.5
BetMGM: +290 (3.9)

Total Under 157.5
BetMGM: -375 (1.26)

Total Over 158.5
BetMGM: +310 (4.1)

Total Under 158.5
BetMGM: -400 (1.25)
Moneyline
Moneyline UCLA None
DraftKings: -143 (1.7)
FanDuel: -146 (1.6849)
FoxBet: -150 (1.6667)
BetMGM: -125 (1.8)

Moneyline Arizona State None
DraftKings: +120 (2.2)
FanDuel: +124 (2.24)
FoxBet: +120 (2.2)
BetMGM: +105 (2.05)


, 'Mount St. Marys @ St Francis Brooklyn': College Basketball: Mount St. Marys @ St Francis Brooklyn
FanDuel - 949374.3


, 'Central Conn. State @ Bryant': College Basketball: Central Conn. State @ Bryant
FanDuel - 949381.3


, 'SE Missouri State @ Belmont': College Basketball: SE Missouri State @ Belmont
FanDuel - 949361.3
BetMGM - 10973261
Moneyline
Moneyline SE Missouri State None
FanDuel: +980 (10.8)
BetMGM: +725 (8.25)

Moneyline Belmont None
FanDuel: -1800 (1.0556)
BetMGM: -1200 (1.08)
Spread
Spread SE Missouri State 14.5
FanDuel: -118 (1.8475)
BetMGM: -130 (1.78)

Spread Belmont -14.5
FanDuel: -104 (1.9615)
BetMGM: +105 (2.05)

Spread SE Missouri State 27.5
BetMGM: -1200 (1.08)

Spread Belmont -27.5
BetMGM: +725 (8.25)

Spread SE Missouri State 26.5
BetMGM: -1000 (1.1)

Spread Belmont -26.5
BetMGM: +625 (7.25)

Spread SE Missouri State 25.5
BetMGM: -800 (1.12)

Spread Belmont -25.5
BetMGM: +550 (6.5)

Spread SE Missouri State 24.5
BetMGM: -650 (1.15)

Spread Belmont -24.5
BetMGM: +475 (5.75)

Spread SE Missouri State 23.5
BetMGM: -550 (1.18)

Spread Belmont -23.5
BetMGM: +400 (5.0)

Spread SE Missouri State 22.5
BetMGM: -450 (1.22)

Spread Belmont -22.5
BetMGM: +350 (4.5)

Spread SE Missouri State 21.5
BetMGM: -375 (1.26)

Spread Belmont -21.5
BetMGM: +300 (4.0)

Spread SE Missouri State 20.5
BetMGM: -350 (1.3)

Spread Belmont -20.5
BetMGM: +260 (3.6)

Spread SE Missouri State 19.5
BetMGM: -275 (1.36)

Spread Belmont -19.5
BetMGM: +220 (3.2)

Spread SE Missouri State 18.5
BetMGM: -250 (1.42)

Spread Belmont -18.5
BetMGM: +190 (2.9)

Spread SE Missouri State 17.5
BetMGM: -200 (1.5)

Spread Belmont -17.5
BetMGM: +165 (2.65)

Spread SE Missouri State 16.5
BetMGM: -175 (1.57)

Spread Belmont -16.5
BetMGM: +145 (2.45)

Spread SE Missouri State 15.5
BetMGM: -150 (1.67)

Spread Belmont -15.5
BetMGM: +125 (2.25)

Spread SE Missouri State 13.5
BetMGM: -110 (1.91)

Spread Belmont -13.5
BetMGM: -110 (1.91)

Spread SE Missouri State 12.5
BetMGM: +105 (2.05)

Spread Belmont -12.5
BetMGM: -130 (1.78)

Spread SE Missouri State 11.5
BetMGM: +125 (2.25)

Spread Belmont -11.5
BetMGM: -150 (1.67)

Spread SE Missouri State 10.5
BetMGM: +145 (2.45)

Spread Belmont -10.5
BetMGM: -175 (1.57)

Spread SE Missouri State 9.5
BetMGM: +170 (2.7)

Spread Belmont -9.5
BetMGM: -200 (1.48)

Spread SE Missouri State 8.5
BetMGM: +195 (2.95)

Spread Belmont -8.5
BetMGM: -250 (1.42)
Total
Total Over 144.5
FanDuel: -114 (1.8772)
BetMGM: -115 (1.87)

Total Under 144.5
FanDuel: -106 (1.9434)
BetMGM: -105 (1.95)

Total Over 136.5
BetMGM: -250 (1.4)

Total Under 136.5
BetMGM: +200 (3.0)

Total Over 137.5
BetMGM: -225 (1.44)

Total Under 137.5
BetMGM: +185 (2.85)

Total Over 138.5
BetMGM: -200 (1.48)

Total Under 138.5
BetMGM: +170 (2.7)

Total Over 139.5
BetMGM: -185 (1.55)

Total Under 139.5
BetMGM: +150 (2.5)

Total Over 140.5
BetMGM: -165 (1.6)

Total Under 140.5
BetMGM: +140 (2.4)

Total Over 141.5
BetMGM: -155 (1.65)

Total Under 141.5
BetMGM: +125 (2.25)

Total Over 142.5
BetMGM: -140 (1.72)

Total Under 142.5
BetMGM: +115 (2.15)

Total Over 143.5
BetMGM: -125 (1.8)

Total Under 143.5
BetMGM: +105 (2.05)

Total Over 145.5
BetMGM: -105 (1.95)

Total Under 145.5
BetMGM: -115 (1.87)

Total Over 146.5
BetMGM: +105 (2.05)

Total Under 146.5
BetMGM: -130 (1.78)

Total Over 147.5
BetMGM: +115 (2.15)

Total Under 147.5
BetMGM: -140 (1.72)

Total Over 148.5
BetMGM: +125 (2.25)

Total Under 148.5
BetMGM: -155 (1.65)

Total Over 149.5
BetMGM: +140 (2.4)

Total Under 149.5
BetMGM: -165 (1.6)

Total Over 150.5
BetMGM: +155 (2.55)

Total Under 150.5
BetMGM: -190 (1.53)

Total Over 151.5
BetMGM: +170 (2.7)

Total Under 151.5
BetMGM: -200 (1.48)

Total Over 152.5
BetMGM: +185 (2.85)

Total Under 152.5
BetMGM: -225 (1.44)

Total Over 153.5
BetMGM: +200 (3.0)

Total Under 153.5
BetMGM: -250 (1.4)

Total Over 154.5
BetMGM: +220 (3.2)

Total Under 154.5
BetMGM: -275 (1.36)

Total Over 155.5
BetMGM: +240 (3.4)

Total Under 155.5
BetMGM: -300 (1.33)


, 'St. Francis (PA) @ LIU': College Basketball: St. Francis (PA) @ LIU
FanDuel - 949378.3


, 'Sacred Heart @ Merrimack ': College Basketball: Sacred Heart @ Merrimack 
FanDuel - 949380.3


, 'Cincinnati @ Southern Methodist': College Basketball: Cincinnati @ Southern Methodist
FanDuel - 949362.3


, 'Brigham Young @ Gonzaga': College Basketball: Brigham Young @ Gonzaga
FanDuel - 949373.3


, 'UT Martin @ Tennessee State': College Basketball: UT Martin @ Tennessee State
FanDuel - 949375.3


, 'IUPUI @ UW Milwaukee': College Basketball: IUPUI @ UW Milwaukee
FanDuel - 949735.3


, 'Central Connecticut @ Bryant': College Basketball: Central Connecticut @ Bryant
FoxBet - 8716432


, "Mt St Mary's @ St. Francis (BKN)": College Basketball: Mt St Mary's @ St. Francis (BKN)
FoxBet - 8715818


, '#17 Oregon @ Colorado': College Basketball: #17 Oregon @ Colorado
FoxBet - 8715824


, 'Indiana @ #8 Wisconsin': College Basketball: Indiana @ #8 Wisconsin
FoxBet - 8715790


, '#5 Iowa @ Maryland': College Basketball: #5 Iowa @ Maryland
FoxBet - 8715786


, 'St Francis (PA) @ LIU': College Basketball: St Francis (PA) @ LIU
FoxBet - 8715822


, 'BYU @ #1 Gonzaga': College Basketball: BYU @ #1 Gonzaga
FoxBet - 8715816


, 'Murray Sate @ Eastern Illinois': College Basketball: Murray Sate @ Eastern Illinois
FoxBet - 8715810


, '#12 Illinois @ Northwestern': College Basketball: #12 Illinois @ Northwestern
FoxBet - 8715806


, 'Tennessee Martin @ Tennessee State': College Basketball: Tennessee Martin @ Tennessee State
FoxBet - 8715820


, 'Central Conn @ Bryant': College Basketball: Central Conn @ Bryant
BetMGM - 10973252


, 'Mount St. Marys @ St. Francis NY': College Basketball: Mount St. Marys @ St. Francis NY
BetMGM - 10973253


, 'St Francis PA @ Long Island': College Basketball: St Francis PA @ Long Island
BetMGM - 10973255


, 'Indiana U. @ Wisconsin': College Basketball: Indiana U. @ Wisconsin
BetMGM - 10989426


, 'Tenn Martin @ Tennessee State': College Basketball: Tenn Martin @ Tennessee State
BetMGM - 10973268


, 'Portland U @ San Francisco': College Basketball: Portland U @ San Francisco
BetMGM - 10989427


}, 'NFL': {'Indianapolis Colts @ Buffalo Bills': NFL: Indianapolis Colts @ Buffalo Bills
BetMGM - 10977405


, 'Los Angeles Rams @ Seattle Seahawks': NFL: Los Angeles Rams @ Seattle Seahawks
BetMGM - 10977409


, 'Tampa Bay Buccaneers @ Washington Football Team': NFL: Tampa Bay Buccaneers @ Washington Football Team
BetMGM - 10977432


, 'Baltimore Ravens @ Tennessee Titans': NFL: Baltimore Ravens @ Tennessee Titans
BetMGM - 10977407


, 'Chicago Bears @ New Orleans Saints': NFL: Chicago Bears @ New Orleans Saints
BetMGM - 10977408


, 'Cleveland Browns @ Pittsburgh Steelers': NFL: Cleveland Browns @ Pittsburgh Steelers
BetMGM - 10977406


}}
Found 28 at 2021-01-07 15:20:27.608162

