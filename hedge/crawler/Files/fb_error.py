{
   "attributes": {
      "attrib": [
         {
            "value": "sr:match:24750814",
            "key": "betradarId"
         },
         {
            "value": "true",
            "key": "cashoutAvailable"
         },
         {
            "value": "TNT",
            "key": "TvDataChannel"
         },
         {
            "value": "1610066100000",
            "key": "TvDataStartTime"
         },
         {
            "value": "1610073300000",
            "key": "TvDataEndTime"
         }
      ]
   },
   "markets": [
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 113.5",
                  "shortName": "Under 113.5"
               },
               "wasPrice": [],
               "id": 203989337,
               "name": "Under 113.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 113.5",
                  "shortName": "Over 113.5"
               },
               "wasPrice": [],
               "id": 203989335,
               "name": "Over 113.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Half Total Points",
            "shortName": "1st Half Total Points"
         },
         "id": 70190074,
         "name": "1st Half Total Points",
         "type": "BASKETBALL:P:OU",
         "subtype": "H1#113.5",
         "line": 113.5,
         "displayOrder": -365,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "H1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.72",
                  "frac": "8/11",
                  "rootIdx": 60
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets +0.5",
                  "shortName": "Brooklyn Nets +0.5"
               },
               "wasPrice": [],
               "id": 203935803,
               "name": "Brooklyn Nets +0.5",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2",
                  "frac": "1/1",
                  "rootIdx": 83
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers -0.5",
                  "shortName": "Philadelphia 76ers -0.5"
               },
               "wasPrice": [],
               "id": 203935801,
               "name": "Philadelphia 76ers -0.5",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "4th Quarter Spread",
            "shortName": "4th Quarter Spread"
         },
         "id": 70176650,
         "name": "4th Quarter Spread",
         "type": "BASKETBALL:P:PSPRD",
         "subtype": "Q4#0.5",
         "line": 0.5,
         "displayOrder": -294,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q4"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "9",
                  "frac": "8/1",
                  "rootIdx": 192
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Shake Milton 4+",
                  "shortName": "Shake Milton 4+"
               },
               "wasPrice": [],
               "id": 204141095,
               "name": "Shake Milton 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "5.25",
                  "frac": "17/4",
                  "rootIdx": 163
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Shake Milton 3+",
                  "shortName": "Shake Milton 3+"
               },
               "wasPrice": [],
               "id": 204141093,
               "name": "Shake Milton 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.7",
                  "frac": "17/10",
                  "rootIdx": 113
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "Shake Milton 2+",
                  "shortName": "Shake Milton 2+"
               },
               "wasPrice": [],
               "id": 204141091,
               "name": "Shake Milton 2+",
               "type": "2",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.53",
                  "frac": "8/15",
                  "rootIdx": 50
               },
               "pos": {
                  "row": 101,
                  "col": 1
               },
               "names": {
                  "longName": "Shake Milton 1+",
                  "shortName": "Shake Milton 1+"
               },
               "wasPrice": [],
               "id": 204141089,
               "name": "Shake Milton 1+",
               "type": "1",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Shake Milton steals (incl. overtime)",
            "shortName": "Shake Milton steals (incl. overtime)"
         },
         "id": 70230352,
         "name": "Shake Milton steals (incl. overtime)",
         "type": "BASKETBALL:FT:PLSTA",
         "subtype": "M#pre:playerprops:24750814:1495351",
         "displayOrder": -270,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "5.5",
                  "frac": "9/2",
                  "rootIdx": 166
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "Seth Curry 2+",
                  "shortName": "Seth Curry 2+"
               },
               "wasPrice": [],
               "id": 204141187,
               "name": "Seth Curry 2+",
               "type": "2",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.85",
                  "frac": "37/20",
                  "rootIdx": 116
               },
               "pos": {
                  "row": 101,
                  "col": 1
               },
               "names": {
                  "longName": "Seth Curry 1+",
                  "shortName": "Seth Curry 1+"
               },
               "wasPrice": [],
               "id": 204141185,
               "name": "Seth Curry 1+",
               "type": "1",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "10",
                  "frac": "9/1",
                  "rootIdx": 196
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Seth Curry 3+",
                  "shortName": "Seth Curry 3+"
               },
               "wasPrice": [],
               "id": 204141189,
               "name": "Seth Curry 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "30",
                  "frac": "29/1",
                  "rootIdx": 221
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Seth Curry 4+",
                  "shortName": "Seth Curry 4+"
               },
               "wasPrice": [],
               "id": 204141191,
               "name": "Seth Curry 4+",
               "type": "4",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Seth Curry blocks (incl. overtime)",
            "shortName": "Seth Curry blocks (incl. overtime)"
         },
         "id": 70230370,
         "name": "Seth Curry blocks (incl. overtime)",
         "type": "BASKETBALL:FT:PLBLOC",
         "subtype": "M#pre:playerprops:24750814:607326",
         "displayOrder": -269,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.9",
                  "frac": "19/10",
                  "rootIdx": 118
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 217.5",
                  "shortName": "Under 217.5"
               },
               "wasPrice": [],
               "id": 203826129,
               "name": "Under 217.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.36",
                  "frac": "4/11",
                  "rootIdx": 39
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 217.5",
                  "shortName": "Over 217.5"
               },
               "wasPrice": [],
               "id": 203826116,
               "name": "Over 217.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132037,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#217.5",
         "line": 217.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "5.9",
                  "frac": "49/10",
                  "rootIdx": 171
               },
               "names": {
                  "longName": "Kyrie Irving 41+",
                  "shortName": "Kyrie Irving 41+"
               },
               "wasPrice": [],
               "id": 204141035,
               "name": "Kyrie Irving 41+",
               "type": "41",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4.65",
                  "frac": "73/20",
                  "rootIdx": 154
               },
               "names": {
                  "longName": "Kyrie Irving 39+",
                  "shortName": "Kyrie Irving 39+"
               },
               "wasPrice": [],
               "id": 204141033,
               "name": "Kyrie Irving 39+",
               "type": "39",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.75",
                  "frac": "11/4",
                  "rootIdx": 135
               },
               "names": {
                  "longName": "Kyrie Irving 37+",
                  "shortName": "Kyrie Irving 37+"
               },
               "wasPrice": [],
               "id": 204141031,
               "name": "Kyrie Irving 37+",
               "type": "37",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.5",
                  "frac": "3/2",
                  "rootIdx": 108
               },
               "names": {
                  "longName": "Kyrie Irving 33+",
                  "shortName": "Kyrie Irving 33+"
               },
               "wasPrice": [],
               "id": 204141027,
               "name": "Kyrie Irving 33+",
               "type": "33",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.1",
                  "frac": "11/10",
                  "rootIdx": 93
               },
               "names": {
                  "longName": "Kyrie Irving 31+",
                  "shortName": "Kyrie Irving 31+"
               },
               "wasPrice": [],
               "id": 204141025,
               "name": "Kyrie Irving 31+",
               "type": "31",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.78",
                  "frac": "39/50",
                  "rootIdx": 65
               },
               "names": {
                  "longName": "Kyrie Irving 29+",
                  "shortName": "Kyrie Irving 29+"
               },
               "wasPrice": [],
               "id": 204141023,
               "name": "Kyrie Irving 29+",
               "type": "29",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.62",
                  "frac": "8/13",
                  "rootIdx": 54
               },
               "names": {
                  "longName": "Kyrie Irving 27+",
                  "shortName": "Kyrie Irving 27+"
               },
               "wasPrice": [],
               "id": 204141021,
               "name": "Kyrie Irving 27+",
               "type": "27",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.05",
                  "frac": "41/20",
                  "rootIdx": 121
               },
               "names": {
                  "longName": "Kyrie Irving 35+",
                  "shortName": "Kyrie Irving 35+"
               },
               "wasPrice": [],
               "id": 204141029,
               "name": "Kyrie Irving 35+",
               "type": "35",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "9.25",
                  "frac": "33/4",
                  "rootIdx": 193
               },
               "names": {
                  "longName": "Kyrie Irving 43+",
                  "shortName": "Kyrie Irving 43+"
               },
               "wasPrice": [],
               "id": 204141037,
               "name": "Kyrie Irving 43+",
               "type": "43",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Kyrie Irving points (incl. overtime)",
            "shortName": "Kyrie Irving points (incl. overtime)"
         },
         "id": 70230338,
         "name": "Kyrie Irving points (incl. overtime)",
         "type": "BASKETBALL:FT:PLPOI",
         "subtype": "M#pre:playerprops:24750814:608268",
         "displayOrder": -275,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.45",
                  "frac": "29/20",
                  "rootIdx": 107
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 220.5",
                  "shortName": "Under 220.5"
               },
               "wasPrice": [],
               "id": 203826186,
               "name": "Under 220.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.5",
                  "frac": "1/2",
                  "rootIdx": 47
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 220.5",
                  "shortName": "Over 220.5"
               },
               "wasPrice": [],
               "id": 203826179,
               "name": "Over 220.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132065,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#220.5",
         "line": 220.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 110.5",
                  "shortName": "Under 110.5"
               },
               "wasPrice": [],
               "id": 203827141,
               "name": "Under 110.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 110.5",
                  "shortName": "Over 110.5"
               },
               "wasPrice": [],
               "id": 203827139,
               "name": "Over 110.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Brooklyn Nets Total Points (Incl. OT)",
            "shortName": "Brooklyn Nets Total Points (Incl. OT)"
         },
         "id": 70132474,
         "name": "Brooklyn Nets Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:A:OU",
         "subtype": "M#110.5",
         "line": 110.5,
         "displayOrder": -445,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3.9",
                  "frac": "29/10",
                  "rootIdx": 138
               },
               "pos": {
                  "row": 2,
                  "col": 2
               },
               "names": {
                  "longName": "Brooklyn Nets & Over 223.5",
                  "shortName": "Brooklyn Nets & Over 223.5"
               },
               "wasPrice": [],
               "id": 203844463,
               "name": "Brooklyn Nets & Over 223.5",
               "type": "AOver",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.3",
                  "frac": "23/10",
                  "rootIdx": 126
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Philadelphia 76ers & Under 223.5",
                  "shortName": "Philadelphia 76ers & Under 223.5"
               },
               "wasPrice": [],
               "id": 203844461,
               "name": "Philadelphia 76ers & Under 223.5",
               "type": "BUnder",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.9",
                  "frac": "29/10",
                  "rootIdx": 138
               },
               "pos": {
                  "row": 2,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets & Under 223.5",
                  "shortName": "Brooklyn Nets & Under 223.5"
               },
               "wasPrice": [],
               "id": 203844459,
               "name": "Brooklyn Nets & Under 223.5",
               "type": "AUnder",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.3",
                  "frac": "23/10",
                  "rootIdx": 126
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers & Over 223.5",
                  "shortName": "Philadelphia 76ers & Over 223.5"
               },
               "wasPrice": [],
               "id": 203844457,
               "name": "Philadelphia 76ers & Over 223.5",
               "type": "BOver",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Matchbet and Total Points (Incl. OT)",
            "shortName": "Matchbet and Total Points (Incl. OT)"
         },
         "id": 70139242,
         "name": "Matchbet and Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:AXB$OU",
         "subtype": "M#223.5",
         "line": 223.5,
         "displayOrder": -425,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "13",
                  "frac": "12/1",
                  "rootIdx": 202
               },
               "names": {
                  "longName": "Tobias Harris 13+",
                  "shortName": "Tobias Harris 13+"
               },
               "wasPrice": [],
               "id": 204140635,
               "name": "Tobias Harris 13+",
               "type": "13",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "6.6",
                  "frac": "28/5",
                  "rootIdx": 179
               },
               "names": {
                  "longName": "Tobias Harris 12+",
                  "shortName": "Tobias Harris 12+"
               },
               "wasPrice": [],
               "id": 204140633,
               "name": "Tobias Harris 12+",
               "type": "12",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4.1",
                  "frac": "31/10",
                  "rootIdx": 142
               },
               "names": {
                  "longName": "Tobias Harris 11+",
                  "shortName": "Tobias Harris 11+"
               },
               "wasPrice": [],
               "id": 204140631,
               "name": "Tobias Harris 11+",
               "type": "11",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.45",
                  "frac": "9/20",
                  "rootIdx": 44
               },
               "pos": {
                  "row": 103,
                  "col": 1
               },
               "names": {
                  "longName": "Tobias Harris 7+",
                  "shortName": "Tobias Harris 7+"
               },
               "wasPrice": [],
               "id": 204140641,
               "name": "Tobias Harris 7+",
               "type": "7",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.75",
                  "frac": "7/4",
                  "rootIdx": 114
               },
               "names": {
                  "longName": "Tobias Harris 10+",
                  "shortName": "Tobias Harris 10+"
               },
               "wasPrice": [],
               "id": 204140629,
               "name": "Tobias Harris 10+",
               "type": "10",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2",
                  "frac": "1/1",
                  "rootIdx": 83
               },
               "names": {
                  "longName": "Tobias Harris 9+",
                  "shortName": "Tobias Harris 9+"
               },
               "wasPrice": [],
               "id": 204140645,
               "name": "Tobias Harris 9+",
               "type": "9",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.7",
                  "frac": "7/10",
                  "rootIdx": 59
               },
               "pos": {
                  "row": 203,
                  "col": 1
               },
               "names": {
                  "longName": "Tobias Harris 8+",
                  "shortName": "Tobias Harris 8+"
               },
               "wasPrice": [],
               "id": 204140643,
               "name": "Tobias Harris 8+",
               "type": "8",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "32",
                  "frac": "31/1",
                  "rootIdx": 223
               },
               "names": {
                  "longName": "Tobias Harris 15+",
                  "shortName": "Tobias Harris 15+"
               },
               "wasPrice": [],
               "id": 204140639,
               "name": "Tobias Harris 15+",
               "type": "15",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "21",
                  "frac": "20/1",
                  "rootIdx": 212
               },
               "names": {
                  "longName": "Tobias Harris 14+",
                  "shortName": "Tobias Harris 14+"
               },
               "wasPrice": [],
               "id": 204140637,
               "name": "Tobias Harris 14+",
               "type": "14",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Tobias Harris rebounds (incl. overtime)",
            "shortName": "Tobias Harris rebounds (incl. overtime)"
         },
         "id": 70230254,
         "name": "Tobias Harris rebounds (incl. overtime)",
         "type": "BASKETBALL:FT:PLREB",
         "subtype": "M#pre:playerprops:24750814:608250",
         "displayOrder": -274,
         "columnCount": 1,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "12",
                  "frac": "11/1",
                  "rootIdx": 200
               },
               "pos": {
                  "row": 302,
                  "col": 1
               },
               "names": {
                  "longName": "DeAndre Jordan 6+",
                  "shortName": "DeAndre Jordan 6+"
               },
               "wasPrice": [],
               "id": 204140989,
               "name": "DeAndre Jordan 6+",
               "type": "6",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "6.2",
                  "frac": "26/5",
                  "rootIdx": 174
               },
               "pos": {
                  "row": 202,
                  "col": 1
               },
               "names": {
                  "longName": "DeAndre Jordan 5+",
                  "shortName": "DeAndre Jordan 5+"
               },
               "wasPrice": [],
               "id": 204140987,
               "name": "DeAndre Jordan 5+",
               "type": "5",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.5",
                  "frac": "5/2",
                  "rootIdx": 130
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "DeAndre Jordan 4+",
                  "shortName": "DeAndre Jordan 4+"
               },
               "wasPrice": [],
               "id": 204140985,
               "name": "DeAndre Jordan 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.2",
                  "frac": "6/5",
                  "rootIdx": 99
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "DeAndre Jordan 3+",
                  "shortName": "DeAndre Jordan 3+"
               },
               "wasPrice": [],
               "id": 204140983,
               "name": "DeAndre Jordan 3+",
               "type": "3",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "DeAndre Jordan assists (incl. overtime)",
            "shortName": "DeAndre Jordan assists (incl. overtime)"
         },
         "id": 70230328,
         "name": "DeAndre Jordan assists (incl. overtime)",
         "type": "BASKETBALL:FT:PLASS",
         "subtype": "M#pre:playerprops:24750814:607508",
         "displayOrder": -273,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.75",
                  "frac": "3/4",
                  "rootIdx": 63
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 58.5",
                  "shortName": "Under 58.5"
               },
               "wasPrice": [],
               "id": 204512913,
               "name": "Under 58.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 58.5",
                  "shortName": "Over 58.5"
               },
               "wasPrice": [],
               "id": 204512911,
               "name": "Over 58.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Quarter Total Points",
            "shortName": "1st Quarter Total Points"
         },
         "id": 70307674,
         "name": "1st Quarter Total Points",
         "type": "BASKETBALL:P:OU",
         "subtype": "Q1#58.5",
         "line": 58.5,
         "displayOrder": -323,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.25",
                  "frac": "1/4",
                  "rootIdx": 32
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets +9.5",
                  "shortName": "Brooklyn Nets +9.5"
               },
               "wasPrice": [],
               "id": 203825945,
               "name": "Brooklyn Nets +9.5",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.75",
                  "frac": "11/4",
                  "rootIdx": 135
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers -9.5",
                  "shortName": "Philadelphia 76ers -9.5"
               },
               "wasPrice": [],
               "id": 203825951,
               "name": "Philadelphia 76ers -9.5",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131954,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#9.5",
         "line": 9.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.3",
                  "frac": "13/10",
                  "rootIdx": 103
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 27.5",
                  "shortName": "Under 27.5"
               },
               "wasPrice": [],
               "id": 203826075,
               "name": "Under 27.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.55",
                  "frac": "11/20",
                  "rootIdx": 51
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 27.5",
                  "shortName": "Over 27.5"
               },
               "wasPrice": [],
               "id": 203826066,
               "name": "Over 27.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Quarter Brooklyn Nets Total Points",
            "shortName": "1st Quarter Brooklyn Nets Total Points"
         },
         "id": 70132007,
         "name": "1st Quarter Brooklyn Nets Total Points",
         "type": "BASKETBALL:P:A:OU",
         "subtype": "Q1#27.5",
         "line": 27.5,
         "displayOrder": -318,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "4.5",
                  "frac": "7/2",
                  "rootIdx": 151
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers -11.5",
                  "shortName": "Philadelphia 76ers -11.5"
               },
               "wasPrice": [],
               "id": 203825987,
               "name": "Philadelphia 76ers -11.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.18",
                  "frac": "2/11",
                  "rootIdx": 26
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets +11.5",
                  "shortName": "Brooklyn Nets +11.5"
               },
               "wasPrice": [],
               "id": 203825977,
               "name": "Brooklyn Nets +11.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131947,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#11.5",
         "line": 11.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "5.5",
                  "frac": "9/2",
                  "rootIdx": 166
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets 1-5",
                  "shortName": "Brooklyn Nets 1-5"
               },
               "wasPrice": [],
               "id": 203826296,
               "name": "Brooklyn Nets 1-5",
               "type": "HT 1-5",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "4.45",
                  "frac": "69/20",
                  "rootIdx": 150
               },
               "pos": {
                  "row": 3,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets 11+",
                  "shortName": "Brooklyn Nets 11+"
               },
               "wasPrice": [],
               "id": 203826293,
               "name": "Brooklyn Nets 11+",
               "type": "HT > 10",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "5.5",
                  "frac": "9/2",
                  "rootIdx": 166
               },
               "pos": {
                  "row": 2,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers 6-10",
                  "shortName": "Philadelphia 76ers 6-10"
               },
               "wasPrice": [],
               "id": 203826291,
               "name": "Philadelphia 76ers 6-10",
               "type": "AT 6-10",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "5.7",
                  "frac": "47/10",
                  "rootIdx": 168
               },
               "pos": {
                  "row": 2,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets 6-10",
                  "shortName": "Brooklyn Nets 6-10"
               },
               "wasPrice": [],
               "id": 203826308,
               "name": "Brooklyn Nets 6-10",
               "type": "HT 6-10",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "5.4",
                  "frac": "22/5",
                  "rootIdx": 165
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers 1-5",
                  "shortName": "Philadelphia 76ers 1-5"
               },
               "wasPrice": [],
               "id": 203826286,
               "name": "Philadelphia 76ers 1-5",
               "type": "AT 1-5",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "4.3",
                  "frac": "33/10",
                  "rootIdx": 146
               },
               "pos": {
                  "row": 3,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers 11+",
                  "shortName": "Philadelphia 76ers 11+"
               },
               "wasPrice": [],
               "id": 203826281,
               "name": "Philadelphia 76ers 11+",
               "type": "AT > 10",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Winning Margin 6-Way(Incl. OT)",
            "shortName": "Winning Margin 6-Way(Incl. OT)"
         },
         "id": 70132103,
         "name": "Winning Margin 6-Way(Incl. OT)",
         "type": "BASKETBALL:FTOT:WM",
         "subtype": "M#sr:winning_margin_no_draw:11+",
         "displayOrder": -405,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "14",
                  "frac": "13/1",
                  "rootIdx": 204
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Timothe Luwawu-Cabarrot 4+",
                  "shortName": "Timothe Luwawu-Cabarrot 4+"
               },
               "wasPrice": [],
               "id": 204141377,
               "name": "Timothe Luwawu-Cabarrot 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "6",
                  "frac": "5/1",
                  "rootIdx": 172
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Timothe Luwawu-Cabarrot 3+",
                  "shortName": "Timothe Luwawu-Cabarrot 3+"
               },
               "wasPrice": [],
               "id": 204141375,
               "name": "Timothe Luwawu-Cabarrot 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.35",
                  "frac": "47/20",
                  "rootIdx": 127
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "Timothe Luwawu-Cabarrot 2+",
                  "shortName": "Timothe Luwawu-Cabarrot 2+"
               },
               "wasPrice": [],
               "id": 204141373,
               "name": "Timothe Luwawu-Cabarrot 2+",
               "type": "2",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.75",
                  "frac": "3/4",
                  "rootIdx": 63
               },
               "pos": {
                  "row": 101,
                  "col": 1
               },
               "names": {
                  "longName": "Timothe Luwawu-Cabarrot 1+",
                  "shortName": "Timothe Luwawu-Cabarrot 1+"
               },
               "wasPrice": [],
               "id": 204141371,
               "name": "Timothe Luwawu-Cabarrot 1+",
               "type": "1",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Timothe Luwawu-Cabarrot steals (incl. overtime)",
            "shortName": "Timothe Luwawu-Cabarrot steals (incl. overtime)"
         },
         "id": 70230412,
         "name": "Timothe Luwawu-Cabarrot steals (incl. overtime)",
         "type": "BASKETBALL:FT:PLSTA",
         "subtype": "M#pre:playerprops:24750814:852058",
         "displayOrder": -270,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.12",
                  "frac": "1/8",
                  "rootIdx": 19
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers +13.5",
                  "shortName": "Philadelphia 76ers +13.5"
               },
               "wasPrice": [],
               "id": 204605409,
               "name": "Philadelphia 76ers +13.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "5.75",
                  "frac": "19/4",
                  "rootIdx": 169
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets -13.5",
                  "shortName": "Brooklyn Nets -13.5"
               },
               "wasPrice": [],
               "id": 204605407,
               "name": "Brooklyn Nets -13.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70331798,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-13.5",
         "line": -13.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.62",
                  "frac": "8/13",
                  "rootIdx": 54
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers +3.5",
                  "shortName": "Philadelphia 76ers +3.5"
               },
               "wasPrice": [],
               "id": 203825823,
               "name": "Philadelphia 76ers +3.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.25",
                  "frac": "5/4",
                  "rootIdx": 101
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets -3.5",
                  "shortName": "Brooklyn Nets -3.5"
               },
               "wasPrice": [],
               "id": 203825810,
               "name": "Brooklyn Nets -3.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131890,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-3.5",
         "line": -3.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.67",
                  "frac": "4/6",
                  "rootIdx": 57
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 229.5",
                  "shortName": "Under 229.5"
               },
               "wasPrice": [],
               "id": 203826287,
               "name": "Under 229.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.15",
                  "frac": "23/20",
                  "rootIdx": 96
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 229.5",
                  "shortName": "Over 229.5"
               },
               "wasPrice": [],
               "id": 203826280,
               "name": "Over 229.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132102,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#229.5",
         "line": 229.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "11.5",
                  "frac": "21/2",
                  "rootIdx": 199
               },
               "names": {
                  "longName": "Kyrie Irving 10+",
                  "shortName": "Kyrie Irving 10+"
               },
               "wasPrice": [],
               "id": 204140647,
               "name": "Kyrie Irving 10+",
               "type": "10",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "28",
                  "frac": "27/1",
                  "rootIdx": 219
               },
               "names": {
                  "longName": "Kyrie Irving 11+",
                  "shortName": "Kyrie Irving 11+"
               },
               "wasPrice": [],
               "id": 204140649,
               "name": "Kyrie Irving 11+",
               "type": "11",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "6.6",
                  "frac": "28/5",
                  "rootIdx": 179
               },
               "names": {
                  "longName": "Kyrie Irving 9+",
                  "shortName": "Kyrie Irving 9+"
               },
               "wasPrice": [],
               "id": 204140659,
               "name": "Kyrie Irving 9+",
               "type": "9",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4",
                  "frac": "3/1",
                  "rootIdx": 140
               },
               "pos": {
                  "row": 203,
                  "col": 1
               },
               "names": {
                  "longName": "Kyrie Irving 8+",
                  "shortName": "Kyrie Irving 8+"
               },
               "wasPrice": [],
               "id": 204140657,
               "name": "Kyrie Irving 8+",
               "type": "8",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.65",
                  "frac": "33/20",
                  "rootIdx": 112
               },
               "pos": {
                  "row": 103,
                  "col": 1
               },
               "names": {
                  "longName": "Kyrie Irving 7+",
                  "shortName": "Kyrie Irving 7+"
               },
               "wasPrice": [],
               "id": 204140655,
               "name": "Kyrie Irving 7+",
               "type": "7",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.05",
                  "frac": "21/20",
                  "rootIdx": 88
               },
               "pos": {
                  "row": 302,
                  "col": 1
               },
               "names": {
                  "longName": "Kyrie Irving 6+",
                  "shortName": "Kyrie Irving 6+"
               },
               "wasPrice": [],
               "id": 204140653,
               "name": "Kyrie Irving 6+",
               "type": "6",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.53",
                  "frac": "8/15",
                  "rootIdx": 50
               },
               "pos": {
                  "row": 202,
                  "col": 1
               },
               "names": {
                  "longName": "Kyrie Irving 5+",
                  "shortName": "Kyrie Irving 5+"
               },
               "wasPrice": [],
               "id": 204140651,
               "name": "Kyrie Irving 5+",
               "type": "5",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Kyrie Irving rebounds (incl. overtime)",
            "shortName": "Kyrie Irving rebounds (incl. overtime)"
         },
         "id": 70230256,
         "name": "Kyrie Irving rebounds (incl. overtime)",
         "type": "BASKETBALL:FT:PLREB",
         "subtype": "M#pre:playerprops:24750814:608268",
         "displayOrder": -274,
         "columnCount": 1,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.5",
                  "frac": "1/2",
                  "rootIdx": 47
               },
               "names": {
                  "longName": "Tobias Harris 18+",
                  "shortName": "Tobias Harris 18+"
               },
               "wasPrice": [],
               "id": 204140933,
               "name": "Tobias Harris 18+",
               "type": "18",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "17",
                  "frac": "16/1",
                  "rootIdx": 208
               },
               "names": {
                  "longName": "Tobias Harris 34+",
                  "shortName": "Tobias Harris 34+"
               },
               "wasPrice": [],
               "id": 204140949,
               "name": "Tobias Harris 34+",
               "type": "34",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "9.5",
                  "frac": "17/2",
                  "rootIdx": 194
               },
               "names": {
                  "longName": "Tobias Harris 32+",
                  "shortName": "Tobias Harris 32+"
               },
               "wasPrice": [],
               "id": 204140947,
               "name": "Tobias Harris 32+",
               "type": "32",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "7",
                  "frac": "6/1",
                  "rootIdx": 184
               },
               "names": {
                  "longName": "Tobias Harris 30+",
                  "shortName": "Tobias Harris 30+"
               },
               "wasPrice": [],
               "id": 204140945,
               "name": "Tobias Harris 30+",
               "type": "30",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.85",
                  "frac": "57/20",
                  "rootIdx": 137
               },
               "names": {
                  "longName": "Tobias Harris 26+",
                  "shortName": "Tobias Harris 26+"
               },
               "wasPrice": [],
               "id": 204140941,
               "name": "Tobias Harris 26+",
               "type": "26",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "5.3",
                  "frac": "43/10",
                  "rootIdx": 164
               },
               "names": {
                  "longName": "Tobias Harris 28+",
                  "shortName": "Tobias Harris 28+"
               },
               "wasPrice": [],
               "id": 204140943,
               "name": "Tobias Harris 28+",
               "type": "28",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.85",
                  "frac": "37/20",
                  "rootIdx": 116
               },
               "names": {
                  "longName": "Tobias Harris 24+",
                  "shortName": "Tobias Harris 24+"
               },
               "wasPrice": [],
               "id": 204140939,
               "name": "Tobias Harris 24+",
               "type": "24",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.15",
                  "frac": "23/20",
                  "rootIdx": 96
               },
               "names": {
                  "longName": "Tobias Harris 22+",
                  "shortName": "Tobias Harris 22+"
               },
               "wasPrice": [],
               "id": 204140937,
               "name": "Tobias Harris 22+",
               "type": "22",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.87",
                  "frac": "87/100",
                  "rootIdx": 71
               },
               "names": {
                  "longName": "Tobias Harris 20+",
                  "shortName": "Tobias Harris 20+"
               },
               "wasPrice": [],
               "id": 204140935,
               "name": "Tobias Harris 20+",
               "type": "20",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Tobias Harris points (incl. overtime)",
            "shortName": "Tobias Harris points (incl. overtime)"
         },
         "id": 70230322,
         "name": "Tobias Harris points (incl. overtime)",
         "type": "BASKETBALL:FT:PLPOI",
         "subtype": "M#pre:playerprops:24750814:608250",
         "displayOrder": -275,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.55",
                  "frac": "31/20",
                  "rootIdx": 109
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 232.5",
                  "shortName": "Over 232.5"
               },
               "wasPrice": [],
               "id": 203826309,
               "name": "Over 232.5",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.5",
                  "frac": "1/2",
                  "rootIdx": 47
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 232.5",
                  "shortName": "Under 232.5"
               },
               "wasPrice": [],
               "id": 203826321,
               "name": "Under 232.5",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132116,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#232.5",
         "line": 232.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers -1.5",
                  "shortName": "Philadelphia 76ers -1.5"
               },
               "wasPrice": [],
               "id": 203825884,
               "name": "Philadelphia 76ers -1.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets +1.5",
                  "shortName": "Brooklyn Nets +1.5"
               },
               "wasPrice": [],
               "id": 203825880,
               "name": "Brooklyn Nets +1.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131917,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#1.5",
         "line": 1.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 56.5",
                  "shortName": "Under 56.5"
               },
               "wasPrice": [],
               "id": 203825690,
               "name": "Under 56.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.75",
                  "frac": "3/4",
                  "rootIdx": 63
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 56.5",
                  "shortName": "Over 56.5"
               },
               "wasPrice": [],
               "id": 203825687,
               "name": "Over 56.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Half Brooklyn Nets Total Points",
            "shortName": "1st Half Brooklyn Nets Total Points"
         },
         "id": 70131847,
         "name": "1st Half Brooklyn Nets Total Points",
         "type": "BASKETBALL:P:A:OU",
         "subtype": "H1#56.5",
         "line": 56.5,
         "displayOrder": -355,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "H1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "5.75",
                  "frac": "19/4",
                  "rootIdx": 169
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers -13",
                  "shortName": "Philadelphia 76ers -13"
               },
               "wasPrice": [],
               "id": 203825850,
               "name": "Philadelphia 76ers -13",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.12",
                  "frac": "1/8",
                  "rootIdx": 19
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets +13",
                  "shortName": "Brooklyn Nets +13"
               },
               "wasPrice": [],
               "id": 203825841,
               "name": "Brooklyn Nets +13",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131900,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#13",
         "line": 13.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.15",
                  "frac": "2/13",
                  "rootIdx": 23
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets +12",
                  "shortName": "Brooklyn Nets +12"
               },
               "wasPrice": [],
               "id": 203825979,
               "name": "Brooklyn Nets +12",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "5.25",
                  "frac": "17/4",
                  "rootIdx": 163
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers -12",
                  "shortName": "Philadelphia 76ers -12"
               },
               "wasPrice": [],
               "id": 203825988,
               "name": "Philadelphia 76ers -12",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131964,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#12",
         "line": 12.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "6",
                  "frac": "5/1",
                  "rootIdx": 172
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers -15",
                  "shortName": "Philadelphia 76ers -15"
               },
               "wasPrice": [],
               "id": 203825913,
               "name": "Philadelphia 76ers -15",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.1",
                  "frac": "1/10",
                  "rootIdx": 17
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets +15",
                  "shortName": "Brooklyn Nets +15"
               },
               "wasPrice": [],
               "id": 203825900,
               "name": "Brooklyn Nets +15",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131932,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#15",
         "line": 15.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 114.5",
                  "shortName": "Under 114.5"
               },
               "wasPrice": [],
               "id": 203849763,
               "name": "Under 114.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 114.5",
                  "shortName": "Over 114.5"
               },
               "wasPrice": [],
               "id": 203849761,
               "name": "Over 114.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Half Total Points",
            "shortName": "1st Half Total Points"
         },
         "id": 70141568,
         "name": "1st Half Total Points",
         "type": "BASKETBALL:P:OU",
         "subtype": "H1#114.5",
         "line": 114.5,
         "displayOrder": -365,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "H1"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.45",
                  "frac": "29/20",
                  "rootIdx": 107
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Timothe Luwawu-Cabarrot 3+",
                  "shortName": "Timothe Luwawu-Cabarrot 3+"
               },
               "wasPrice": [],
               "id": 204140907,
               "name": "Timothe Luwawu-Cabarrot 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.45",
                  "frac": "9/20",
                  "rootIdx": 44
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "Timothe Luwawu-Cabarrot 2+",
                  "shortName": "Timothe Luwawu-Cabarrot 2+"
               },
               "wasPrice": [],
               "id": 204140905,
               "name": "Timothe Luwawu-Cabarrot 2+",
               "type": "2",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4.1",
                  "frac": "31/10",
                  "rootIdx": 142
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Timothe Luwawu-Cabarrot 4+",
                  "shortName": "Timothe Luwawu-Cabarrot 4+"
               },
               "wasPrice": [],
               "id": 204140909,
               "name": "Timothe Luwawu-Cabarrot 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "30",
                  "frac": "29/1",
                  "rootIdx": 221
               },
               "pos": {
                  "row": 103,
                  "col": 1
               },
               "names": {
                  "longName": "Timothe Luwawu-Cabarrot 7+",
                  "shortName": "Timothe Luwawu-Cabarrot 7+"
               },
               "wasPrice": [],
               "id": 204140915,
               "name": "Timothe Luwawu-Cabarrot 7+",
               "type": "7",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "16",
                  "frac": "15/1",
                  "rootIdx": 207
               },
               "pos": {
                  "row": 302,
                  "col": 1
               },
               "names": {
                  "longName": "Timothe Luwawu-Cabarrot 6+",
                  "shortName": "Timothe Luwawu-Cabarrot 6+"
               },
               "wasPrice": [],
               "id": 204140913,
               "name": "Timothe Luwawu-Cabarrot 6+",
               "type": "6",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "9.25",
                  "frac": "33/4",
                  "rootIdx": 193
               },
               "pos": {
                  "row": 202,
                  "col": 1
               },
               "names": {
                  "longName": "Timothe Luwawu-Cabarrot 5+",
                  "shortName": "Timothe Luwawu-Cabarrot 5+"
               },
               "wasPrice": [],
               "id": 204140911,
               "name": "Timothe Luwawu-Cabarrot 5+",
               "type": "5",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Timothe Luwawu-Cabarrot 3-point field goals (incl. overtime)",
            "shortName": "Timothe Luwawu-Cabarrot 3-point field goals (incl. overtime)"
         },
         "id": 70230316,
         "name": "Timothe Luwawu-Cabarrot 3-point field goals (incl. overtime)",
         "type": "BASKETBALL:FT:3PFG",
         "subtype": "M#pre:playerprops:24750814:852058",
         "displayOrder": -272,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.11",
                  "frac": "1/9",
                  "rootIdx": 18
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets +14",
                  "shortName": "Brooklyn Nets +14"
               },
               "wasPrice": [],
               "id": 203825897,
               "name": "Brooklyn Nets +14",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "5.75",
                  "frac": "19/4",
                  "rootIdx": 169
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers -14",
                  "shortName": "Philadelphia 76ers -14"
               },
               "wasPrice": [],
               "id": 203825908,
               "name": "Philadelphia 76ers -14",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131928,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#14",
         "line": 14.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.18",
                  "frac": "2/11",
                  "rootIdx": 26
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets +11",
                  "shortName": "Brooklyn Nets +11"
               },
               "wasPrice": [],
               "id": 203825961,
               "name": "Brooklyn Nets +11",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "4.33",
                  "frac": "10/3",
                  "rootIdx": 147
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers -11",
                  "shortName": "Philadelphia 76ers -11"
               },
               "wasPrice": [],
               "id": 203825966,
               "name": "Philadelphia 76ers -11",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131967,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#11",
         "line": 11.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.2",
                  "frac": "6/5",
                  "rootIdx": 99
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "Caris LeVert 2+",
                  "shortName": "Caris LeVert 2+"
               },
               "wasPrice": [],
               "id": 204141381,
               "name": "Caris LeVert 2+",
               "type": "2",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "6.1",
                  "frac": "51/10",
                  "rootIdx": 173
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Caris LeVert 4+",
                  "shortName": "Caris LeVert 4+"
               },
               "wasPrice": [],
               "id": 204141385,
               "name": "Caris LeVert 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4.75",
                  "frac": "15/4",
                  "rootIdx": 156
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Caris LeVert 3+",
                  "shortName": "Caris LeVert 3+"
               },
               "wasPrice": [],
               "id": 204141383,
               "name": "Caris LeVert 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.33",
                  "frac": "1/3",
                  "rootIdx": 37
               },
               "pos": {
                  "row": 101,
                  "col": 1
               },
               "names": {
                  "longName": "Caris LeVert 1+",
                  "shortName": "Caris LeVert 1+"
               },
               "wasPrice": [],
               "id": 204141379,
               "name": "Caris LeVert 1+",
               "type": "1",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Caris LeVert steals (incl. overtime)",
            "shortName": "Caris LeVert steals (incl. overtime)"
         },
         "id": 70230414,
         "name": "Caris LeVert steals (incl. overtime)",
         "type": "BASKETBALL:FT:PLSTA",
         "subtype": "M#pre:playerprops:24750814:996291",
         "displayOrder": -270,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3.9",
                  "frac": "29/10",
                  "rootIdx": 138
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers -10",
                  "shortName": "Philadelphia 76ers -10"
               },
               "wasPrice": [],
               "id": 203825858,
               "name": "Philadelphia 76ers -10",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.22",
                  "frac": "2/9",
                  "rootIdx": 30
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets +10",
                  "shortName": "Brooklyn Nets +10"
               },
               "wasPrice": [],
               "id": 203825851,
               "name": "Brooklyn Nets +10",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131901,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#10",
         "line": 10.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "names": {
                  "longName": "DeAndre Jordan 9+",
                  "shortName": "DeAndre Jordan 9+"
               },
               "wasPrice": [],
               "id": 204141325,
               "name": "DeAndre Jordan 9+",
               "type": "9",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 203,
                  "col": 1
               },
               "names": {
                  "longName": "DeAndre Jordan 8+",
                  "shortName": "DeAndre Jordan 8+"
               },
               "wasPrice": [],
               "id": 204141323,
               "name": "DeAndre Jordan 8+",
               "type": "8",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.11",
                  "frac": "1/9",
                  "rootIdx": 18
               },
               "pos": {
                  "row": 103,
                  "col": 1
               },
               "names": {
                  "longName": "DeAndre Jordan 7+",
                  "shortName": "DeAndre Jordan 7+"
               },
               "wasPrice": [],
               "id": 204141321,
               "name": "DeAndre Jordan 7+",
               "type": "7",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.6",
                  "frac": "8/5",
                  "rootIdx": 110
               },
               "names": {
                  "longName": "DeAndre Jordan 14+",
                  "shortName": "DeAndre Jordan 14+"
               },
               "wasPrice": [],
               "id": 204141317,
               "name": "DeAndre Jordan 14+",
               "type": "14",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.45",
                  "frac": "29/20",
                  "rootIdx": 107
               },
               "names": {
                  "longName": "DeAndre Jordan 13+",
                  "shortName": "DeAndre Jordan 13+"
               },
               "wasPrice": [],
               "id": 204141315,
               "name": "DeAndre Jordan 13+",
               "type": "13",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.05",
                  "frac": "21/20",
                  "rootIdx": 88
               },
               "names": {
                  "longName": "DeAndre Jordan 10+",
                  "shortName": "DeAndre Jordan 10+"
               },
               "wasPrice": [],
               "id": 204141309,
               "name": "DeAndre Jordan 10+",
               "type": "10",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.3",
                  "frac": "13/10",
                  "rootIdx": 103
               },
               "names": {
                  "longName": "DeAndre Jordan 12+",
                  "shortName": "DeAndre Jordan 12+"
               },
               "wasPrice": [],
               "id": 204141313,
               "name": "DeAndre Jordan 12+",
               "type": "12",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.15",
                  "frac": "23/20",
                  "rootIdx": 96
               },
               "names": {
                  "longName": "DeAndre Jordan 11+",
                  "shortName": "DeAndre Jordan 11+"
               },
               "wasPrice": [],
               "id": 204141311,
               "name": "DeAndre Jordan 11+",
               "type": "11",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.55",
                  "frac": "51/20",
                  "rootIdx": 131
               },
               "names": {
                  "longName": "DeAndre Jordan 15+",
                  "shortName": "DeAndre Jordan 15+"
               },
               "wasPrice": [],
               "id": 204141319,
               "name": "DeAndre Jordan 15+",
               "type": "15",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "DeAndre Jordan rebounds (incl. overtime)",
            "shortName": "DeAndre Jordan rebounds (incl. overtime)"
         },
         "id": 70230398,
         "name": "DeAndre Jordan rebounds (incl. overtime)",
         "type": "BASKETBALL:FT:PLREB",
         "subtype": "M#pre:playerprops:24750814:607508",
         "displayOrder": -274,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.85",
                  "frac": "57/20",
                  "rootIdx": 137
               },
               "names": {
                  "longName": "Kyrie Irving 9+",
                  "shortName": "Kyrie Irving 9+"
               },
               "wasPrice": [],
               "id": 204140531,
               "name": "Kyrie Irving 9+",
               "type": "9",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 103,
                  "col": 1
               },
               "names": {
                  "longName": "Kyrie Irving 7+",
                  "shortName": "Kyrie Irving 7+"
               },
               "wasPrice": [],
               "id": 204140527,
               "name": "Kyrie Irving 7+",
               "type": "7",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.53",
                  "frac": "8/15",
                  "rootIdx": 50
               },
               "pos": {
                  "row": 302,
                  "col": 1
               },
               "names": {
                  "longName": "Kyrie Irving 6+",
                  "shortName": "Kyrie Irving 6+"
               },
               "wasPrice": [],
               "id": 204140525,
               "name": "Kyrie Irving 6+",
               "type": "6",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.2",
                  "frac": "1/5",
                  "rootIdx": 28
               },
               "pos": {
                  "row": 202,
                  "col": 1
               },
               "names": {
                  "longName": "Kyrie Irving 5+",
                  "shortName": "Kyrie Irving 5+"
               },
               "wasPrice": [],
               "id": 204140523,
               "name": "Kyrie Irving 5+",
               "type": "5",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "11",
                  "frac": "10/1",
                  "rootIdx": 198
               },
               "names": {
                  "longName": "Kyrie Irving 11+",
                  "shortName": "Kyrie Irving 11+"
               },
               "wasPrice": [],
               "id": 204140521,
               "name": "Kyrie Irving 11+",
               "type": "11",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "6.3",
                  "frac": "53/10",
                  "rootIdx": 176
               },
               "names": {
                  "longName": "Kyrie Irving 10+",
                  "shortName": "Kyrie Irving 10+"
               },
               "wasPrice": [],
               "id": 204140519,
               "name": "Kyrie Irving 10+",
               "type": "10",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.63",
                  "frac": "13/8",
                  "rootIdx": 111
               },
               "pos": {
                  "row": 203,
                  "col": 1
               },
               "names": {
                  "longName": "Kyrie Irving 8+",
                  "shortName": "Kyrie Irving 8+"
               },
               "wasPrice": [],
               "id": 204140529,
               "name": "Kyrie Irving 8+",
               "type": "8",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Kyrie Irving assists (incl. overtime)",
            "shortName": "Kyrie Irving assists (incl. overtime)"
         },
         "id": 70230228,
         "name": "Kyrie Irving assists (incl. overtime)",
         "type": "BASKETBALL:FT:PLASS",
         "subtype": "M#pre:playerprops:24750814:608268",
         "displayOrder": -273,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.7",
                  "frac": "17/10",
                  "rootIdx": 113
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 218.5",
                  "shortName": "Under 218.5"
               },
               "wasPrice": [],
               "id": 203826067,
               "name": "Under 218.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.44",
                  "frac": "4/9",
                  "rootIdx": 43
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 218.5",
                  "shortName": "Over 218.5"
               },
               "wasPrice": [],
               "id": 203826060,
               "name": "Over 218.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132005,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#218.5",
         "line": 218.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 56.5",
                  "shortName": "Over 56.5"
               },
               "wasPrice": [],
               "id": 203844473,
               "name": "Over 56.5",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 56.5",
                  "shortName": "Under 56.5"
               },
               "wasPrice": [],
               "id": 203844475,
               "name": "Under 56.5",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Half Philadelphia 76ers Total Points",
            "shortName": "1st Half Philadelphia 76ers Total Points"
         },
         "id": 70139248,
         "name": "1st Half Philadelphia 76ers Total Points",
         "type": "BASKETBALL:P:B:OU",
         "subtype": "H1#56.5",
         "line": 56.5,
         "displayOrder": -350,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "H1"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "19",
                  "frac": "18/1",
                  "rootIdx": 210
               },
               "pos": {
                  "row": 203,
                  "col": 1
               },
               "names": {
                  "longName": "Danny Green 8+",
                  "shortName": "Danny Green 8+"
               },
               "wasPrice": [],
               "id": 204141121,
               "name": "Danny Green 8+",
               "type": "8",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "5.5",
                  "frac": "9/2",
                  "rootIdx": 166
               },
               "pos": {
                  "row": 302,
                  "col": 1
               },
               "names": {
                  "longName": "Danny Green 6+",
                  "shortName": "Danny Green 6+"
               },
               "wasPrice": [],
               "id": 204141117,
               "name": "Danny Green 6+",
               "type": "6",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.85",
                  "frac": "37/20",
                  "rootIdx": 116
               },
               "pos": {
                  "row": 202,
                  "col": 1
               },
               "names": {
                  "longName": "Danny Green 5+",
                  "shortName": "Danny Green 5+"
               },
               "wasPrice": [],
               "id": 204141115,
               "name": "Danny Green 5+",
               "type": "5",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.83",
                  "frac": "5/6",
                  "rootIdx": 69
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Danny Green 4+",
                  "shortName": "Danny Green 4+"
               },
               "wasPrice": [],
               "id": 204141113,
               "name": "Danny Green 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "11",
                  "frac": "10/1",
                  "rootIdx": 198
               },
               "pos": {
                  "row": 103,
                  "col": 1
               },
               "names": {
                  "longName": "Danny Green 7+",
                  "shortName": "Danny Green 7+"
               },
               "wasPrice": [],
               "id": 204141119,
               "name": "Danny Green 7+",
               "type": "7",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.22",
                  "frac": "2/9",
                  "rootIdx": 30
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Danny Green 3+",
                  "shortName": "Danny Green 3+"
               },
               "wasPrice": [],
               "id": 204141111,
               "name": "Danny Green 3+",
               "type": "3",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Danny Green rebounds (incl. overtime)",
            "shortName": "Danny Green rebounds (incl. overtime)"
         },
         "id": 70230358,
         "name": "Danny Green rebounds (incl. overtime)",
         "type": "BASKETBALL:FT:PLREB",
         "subtype": "M#pre:playerprops:24750814:607862",
         "displayOrder": -274,
         "columnCount": 1,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 225.5",
                  "shortName": "Under 225.5"
               },
               "wasPrice": [],
               "id": 203826253,
               "name": "Under 225.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 225.5",
                  "shortName": "Over 225.5"
               },
               "wasPrice": [],
               "id": 203826236,
               "name": "Over 225.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Total Points (Excl. OT)",
            "shortName": "Total Points (Excl. OT)"
         },
         "id": 70132082,
         "name": "Total Points (Excl. OT)",
         "type": "BASKETBALL:FT:OU",
         "subtype": "M#225.5",
         "line": 225.5,
         "displayOrder": -450,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 111.5",
                  "shortName": "Over 111.5"
               },
               "wasPrice": [],
               "id": 203825807,
               "name": "Over 111.5",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 111.5",
                  "shortName": "Under 111.5"
               },
               "wasPrice": [],
               "id": 203825828,
               "name": "Under 111.5",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Brooklyn Nets Total Points (Incl. OT)",
            "shortName": "Brooklyn Nets Total Points (Incl. OT)"
         },
         "id": 70131881,
         "name": "Brooklyn Nets Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:A:OU",
         "subtype": "M#111.5",
         "line": 111.5,
         "displayOrder": -445,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.35",
                  "frac": "27/20",
                  "rootIdx": 104
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 221.5",
                  "shortName": "Under 221.5"
               },
               "wasPrice": [],
               "id": 203826135,
               "name": "Under 221.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.55",
                  "frac": "11/20",
                  "rootIdx": 51
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 221.5",
                  "shortName": "Over 221.5"
               },
               "wasPrice": [],
               "id": 203826122,
               "name": "Over 221.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132035,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#221.5",
         "line": 221.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.95",
                  "frac": "59/20",
                  "rootIdx": 139
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Ben Simmons 4+",
                  "shortName": "Ben Simmons 4+"
               },
               "wasPrice": [],
               "id": 204140517,
               "name": "Ben Simmons 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.35",
                  "frac": "47/20",
                  "rootIdx": 127
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Ben Simmons 3+",
                  "shortName": "Ben Simmons 3+"
               },
               "wasPrice": [],
               "id": 204140515,
               "name": "Ben Simmons 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.65",
                  "frac": "13/20",
                  "rootIdx": 56
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "Ben Simmons 2+",
                  "shortName": "Ben Simmons 2+"
               },
               "wasPrice": [],
               "id": 204140513,
               "name": "Ben Simmons 2+",
               "type": "2",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.1",
                  "frac": "1/10",
                  "rootIdx": 17
               },
               "pos": {
                  "row": 101,
                  "col": 1
               },
               "names": {
                  "longName": "Ben Simmons 1+",
                  "shortName": "Ben Simmons 1+"
               },
               "wasPrice": [],
               "id": 204140511,
               "name": "Ben Simmons 1+",
               "type": "1",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Ben Simmons steals (incl. overtime)",
            "shortName": "Ben Simmons steals (incl. overtime)"
         },
         "id": 70230226,
         "name": "Ben Simmons steals (incl. overtime)",
         "type": "BASKETBALL:FT:PLSTA",
         "subtype": "M#pre:playerprops:24750814:996289",
         "displayOrder": -270,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "6.5",
                  "frac": "11/2",
                  "rootIdx": 178
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers -17",
                  "shortName": "Philadelphia 76ers -17"
               },
               "wasPrice": [],
               "id": 203825962,
               "name": "Philadelphia 76ers -17",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.09",
                  "frac": "1/11",
                  "rootIdx": 16
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets +17",
                  "shortName": "Brooklyn Nets +17"
               },
               "wasPrice": [],
               "id": 203825955,
               "name": "Brooklyn Nets +17",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131948,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#17",
         "line": 17.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "6.5",
                  "frac": "11/2",
                  "rootIdx": 178
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers -16",
                  "shortName": "Philadelphia 76ers -16"
               },
               "wasPrice": [],
               "id": 203825901,
               "name": "Philadelphia 76ers -16",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.09",
                  "frac": "1/11",
                  "rootIdx": 16
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets +16",
                  "shortName": "Brooklyn Nets +16"
               },
               "wasPrice": [],
               "id": 203825892,
               "name": "Brooklyn Nets +16",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131931,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#16",
         "line": 16.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Odd",
                  "shortName": "Odd"
               },
               "wasPrice": [],
               "id": 203826148,
               "name": "Odd",
               "type": "Odd",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Even",
                  "shortName": "Even"
               },
               "wasPrice": [],
               "id": 203826127,
               "name": "Even",
               "type": "Even",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Odd/even (incl. overtime)",
            "shortName": "Odd/even (incl. overtime)"
         },
         "id": 70132031,
         "name": "Odd/even (incl. overtime)",
         "type": "BASKETBALL:FTOT:OE",
         "subtype": "M",
         "displayOrder": -259,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "46",
                  "frac": "45/1",
                  "rootIdx": 237
               },
               "names": {
                  "longName": "DeAndre Jordan 22+",
                  "shortName": "DeAndre Jordan 22+"
               },
               "wasPrice": [],
               "id": 204140459,
               "name": "DeAndre Jordan 22+",
               "type": "22",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "19",
                  "frac": "18/1",
                  "rootIdx": 210
               },
               "names": {
                  "longName": "DeAndre Jordan 20+",
                  "shortName": "DeAndre Jordan 20+"
               },
               "wasPrice": [],
               "id": 204140457,
               "name": "DeAndre Jordan 20+",
               "type": "20",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "10",
                  "frac": "9/1",
                  "rootIdx": 196
               },
               "names": {
                  "longName": "DeAndre Jordan 18+",
                  "shortName": "DeAndre Jordan 18+"
               },
               "wasPrice": [],
               "id": 204140455,
               "name": "DeAndre Jordan 18+",
               "type": "18",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.4",
                  "frac": "2/5",
                  "rootIdx": 41
               },
               "pos": {
                  "row": 302,
                  "col": 1
               },
               "names": {
                  "longName": "DeAndre Jordan 6+",
                  "shortName": "DeAndre Jordan 6+"
               },
               "wasPrice": [],
               "id": 204140461,
               "name": "DeAndre Jordan 6+",
               "type": "6",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "7.5",
                  "frac": "13/2",
                  "rootIdx": 186
               },
               "names": {
                  "longName": "DeAndre Jordan 16+",
                  "shortName": "DeAndre Jordan 16+"
               },
               "wasPrice": [],
               "id": 204140453,
               "name": "DeAndre Jordan 16+",
               "type": "16",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "5.1",
                  "frac": "41/10",
                  "rootIdx": 161
               },
               "names": {
                  "longName": "DeAndre Jordan 14+",
                  "shortName": "DeAndre Jordan 14+"
               },
               "wasPrice": [],
               "id": 204140451,
               "name": "DeAndre Jordan 14+",
               "type": "14",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.35",
                  "frac": "27/20",
                  "rootIdx": 104
               },
               "names": {
                  "longName": "DeAndre Jordan 10+",
                  "shortName": "DeAndre Jordan 10+"
               },
               "wasPrice": [],
               "id": 204140447,
               "name": "DeAndre Jordan 10+",
               "type": "10",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.85",
                  "frac": "17/20",
                  "rootIdx": 70
               },
               "pos": {
                  "row": 203,
                  "col": 1
               },
               "names": {
                  "longName": "DeAndre Jordan 8+",
                  "shortName": "DeAndre Jordan 8+"
               },
               "wasPrice": [],
               "id": 204140463,
               "name": "DeAndre Jordan 8+",
               "type": "8",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.45",
                  "frac": "49/20",
                  "rootIdx": 129
               },
               "names": {
                  "longName": "DeAndre Jordan 12+",
                  "shortName": "DeAndre Jordan 12+"
               },
               "wasPrice": [],
               "id": 204140449,
               "name": "DeAndre Jordan 12+",
               "type": "12",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "DeAndre Jordan points (incl. overtime)",
            "shortName": "DeAndre Jordan points (incl. overtime)"
         },
         "id": 70230214,
         "name": "DeAndre Jordan points (incl. overtime)",
         "type": "BASKETBALL:FT:PLPOI",
         "subtype": "M#pre:playerprops:24750814:607508",
         "displayOrder": -275,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.3",
                  "frac": "13/10",
                  "rootIdx": 103
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 30.5",
                  "shortName": "Over 30.5"
               },
               "wasPrice": [],
               "id": 203933357,
               "name": "Over 30.5",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.55",
                  "frac": "11/20",
                  "rootIdx": 51
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 30.5",
                  "shortName": "Under 30.5"
               },
               "wasPrice": [],
               "id": 203933355,
               "name": "Under 30.5",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Quarter Philadelphia 76ers Total Points",
            "shortName": "1st Quarter Philadelphia 76ers Total Points"
         },
         "id": 70175508,
         "name": "1st Quarter Philadelphia 76ers Total Points",
         "type": "BASKETBALL:P:B:OU",
         "subtype": "Q1#30.5",
         "line": 30.5,
         "displayOrder": -317,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "5",
                  "frac": "4/1",
                  "rootIdx": 160
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers -12.5",
                  "shortName": "Philadelphia 76ers -12.5"
               },
               "wasPrice": [],
               "id": 203825885,
               "name": "Philadelphia 76ers -12.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.15",
                  "frac": "2/13",
                  "rootIdx": 23
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets +12.5",
                  "shortName": "Brooklyn Nets +12.5"
               },
               "wasPrice": [],
               "id": 203825881,
               "name": "Brooklyn Nets +12.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131919,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#12.5",
         "line": 12.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.4",
                  "frac": "7/5",
                  "rootIdx": 106
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets -4.5",
                  "shortName": "Brooklyn Nets -4.5"
               },
               "wasPrice": [],
               "id": 203825797,
               "name": "Brooklyn Nets -4.5",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.53",
                  "frac": "8/15",
                  "rootIdx": 50
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers +4.5",
                  "shortName": "Philadelphia 76ers +4.5"
               },
               "wasPrice": [],
               "id": 203825814,
               "name": "Philadelphia 76ers +4.5",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131880,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-4.5",
         "line": -4.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.5",
                  "frac": "3/2",
                  "rootIdx": 108
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Danny Green 3+",
                  "shortName": "Danny Green 3+"
               },
               "wasPrice": [],
               "id": 204140827,
               "name": "Danny Green 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4.45",
                  "frac": "69/20",
                  "rootIdx": 150
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Danny Green 4+",
                  "shortName": "Danny Green 4+"
               },
               "wasPrice": [],
               "id": 204140829,
               "name": "Danny Green 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "20",
                  "frac": "19/1",
                  "rootIdx": 211
               },
               "pos": {
                  "row": 103,
                  "col": 1
               },
               "names": {
                  "longName": "Danny Green 7+",
                  "shortName": "Danny Green 7+"
               },
               "wasPrice": [],
               "id": 204140835,
               "name": "Danny Green 7+",
               "type": "7",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "13.5",
                  "frac": "25/2",
                  "rootIdx": 203
               },
               "pos": {
                  "row": 302,
                  "col": 1
               },
               "names": {
                  "longName": "Danny Green 6+",
                  "shortName": "Danny Green 6+"
               },
               "wasPrice": [],
               "id": 204140833,
               "name": "Danny Green 6+",
               "type": "6",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "8.5",
                  "frac": "15/2",
                  "rootIdx": 190
               },
               "pos": {
                  "row": 202,
                  "col": 1
               },
               "names": {
                  "longName": "Danny Green 5+",
                  "shortName": "Danny Green 5+"
               },
               "wasPrice": [],
               "id": 204140831,
               "name": "Danny Green 5+",
               "type": "5",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Danny Green assists (incl. overtime)",
            "shortName": "Danny Green assists (incl. overtime)"
         },
         "id": 70230298,
         "name": "Danny Green assists (incl. overtime)",
         "type": "BASKETBALL:FT:PLASS",
         "subtype": "M#pre:playerprops:24750814:607862",
         "displayOrder": -273,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.44",
                  "frac": "4/9",
                  "rootIdx": 43
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 233.5",
                  "shortName": "Under 233.5"
               },
               "wasPrice": [],
               "id": 203826317,
               "name": "Under 233.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.65",
                  "frac": "33/20",
                  "rootIdx": 112
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 233.5",
                  "shortName": "Over 233.5"
               },
               "wasPrice": [],
               "id": 203826307,
               "name": "Over 233.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132114,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#233.5",
         "line": 233.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3.75",
                  "frac": "11/4",
                  "rootIdx": 135
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 210.5",
                  "shortName": "Under 210.5"
               },
               "wasPrice": [],
               "id": 203844465,
               "name": "Under 210.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.25",
                  "frac": "1/4",
                  "rootIdx": 32
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 210.5",
                  "shortName": "Over 210.5"
               },
               "wasPrice": [],
               "id": 203844467,
               "name": "Over 210.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70139244,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#210.5",
         "line": 210.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "11",
                  "frac": "10/1",
                  "rootIdx": 198
               },
               "names": {
                  "longName": "Danny Green 21+",
                  "shortName": "Danny Green 21+"
               },
               "wasPrice": [],
               "id": 204140609,
               "name": "Danny Green 21+",
               "type": "21",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "9.25",
                  "frac": "33/4",
                  "rootIdx": 193
               },
               "names": {
                  "longName": "Danny Green 19+",
                  "shortName": "Danny Green 19+"
               },
               "wasPrice": [],
               "id": 204140607,
               "name": "Danny Green 19+",
               "type": "19",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "6.8",
                  "frac": "29/5",
                  "rootIdx": 182
               },
               "names": {
                  "longName": "Danny Green 17+",
                  "shortName": "Danny Green 17+"
               },
               "wasPrice": [],
               "id": 204140605,
               "name": "Danny Green 17+",
               "type": "17",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4.65",
                  "frac": "73/20",
                  "rootIdx": 154
               },
               "names": {
                  "longName": "Danny Green 15+",
                  "shortName": "Danny Green 15+"
               },
               "wasPrice": [],
               "id": 204140603,
               "name": "Danny Green 15+",
               "type": "15",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.1",
                  "frac": "21/10",
                  "rootIdx": 122
               },
               "names": {
                  "longName": "Danny Green 13+",
                  "shortName": "Danny Green 13+"
               },
               "wasPrice": [],
               "id": 204140601,
               "name": "Danny Green 13+",
               "type": "13",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "-",
                  "frac": "-",
                  "rootIdx": -1
               },
               "names": {
                  "longName": "Danny Green 16+",
                  "shortName": "Danny Green 16+"
               },
               "wasPrice": [],
               "id": 204584799,
               "name": "Danny Green 16+",
               "type": "16",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "-",
                  "frac": "-",
                  "rootIdx": -1
               },
               "names": {
                  "longName": "Danny Green 12+",
                  "shortName": "Danny Green 12+"
               },
               "wasPrice": [],
               "id": 204584795,
               "name": "Danny Green 12+",
               "type": "12",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "-",
                  "frac": "-",
                  "rootIdx": -1
               },
               "names": {
                  "longName": "Danny Green 10+",
                  "shortName": "Danny Green 10+"
               },
               "wasPrice": [],
               "id": 204584793,
               "name": "Danny Green 10+",
               "type": "10",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.78",
                  "frac": "39/50",
                  "rootIdx": 65
               },
               "names": {
                  "longName": "Danny Green 9+",
                  "shortName": "Danny Green 9+"
               },
               "wasPrice": [],
               "id": 204140615,
               "name": "Danny Green 9+",
               "type": "9",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.4",
                  "frac": "2/5",
                  "rootIdx": 41
               },
               "pos": {
                  "row": 103,
                  "col": 1
               },
               "names": {
                  "longName": "Danny Green 7+",
                  "shortName": "Danny Green 7+"
               },
               "wasPrice": [],
               "id": 204140613,
               "name": "Danny Green 7+",
               "type": "7",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "-",
                  "frac": "-",
                  "rootIdx": -1
               },
               "names": {
                  "longName": "Danny Green 18+",
                  "shortName": "Danny Green 18+"
               },
               "wasPrice": [],
               "id": 204584801,
               "name": "Danny Green 18+",
               "type": "18",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.25",
                  "frac": "5/4",
                  "rootIdx": 101
               },
               "names": {
                  "longName": "Danny Green 11+",
                  "shortName": "Danny Green 11+"
               },
               "wasPrice": [],
               "id": 204140599,
               "name": "Danny Green 11+",
               "type": "11",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "-",
                  "frac": "-",
                  "rootIdx": -1
               },
               "names": {
                  "longName": "Danny Green 8+",
                  "shortName": "Danny Green 8+"
               },
               "wasPrice": [],
               "id": 204584809,
               "name": "Danny Green 8+",
               "type": "8",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "-",
                  "frac": "-",
                  "rootIdx": -1
               },
               "names": {
                  "longName": "Danny Green 24+",
                  "shortName": "Danny Green 24+"
               },
               "wasPrice": [],
               "id": 204584807,
               "name": "Danny Green 24+",
               "type": "24",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "-",
                  "frac": "-",
                  "rootIdx": -1
               },
               "names": {
                  "longName": "Danny Green 22+",
                  "shortName": "Danny Green 22+"
               },
               "wasPrice": [],
               "id": 204584805,
               "name": "Danny Green 22+",
               "type": "22",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "-",
                  "frac": "-",
                  "rootIdx": -1
               },
               "names": {
                  "longName": "Danny Green 20+",
                  "shortName": "Danny Green 20+"
               },
               "wasPrice": [],
               "id": 204584803,
               "name": "Danny Green 20+",
               "type": "20",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "-",
                  "frac": "-",
                  "rootIdx": -1
               },
               "names": {
                  "longName": "Danny Green 14+",
                  "shortName": "Danny Green 14+"
               },
               "wasPrice": [],
               "id": 204584797,
               "name": "Danny Green 14+",
               "type": "14",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "12.5",
                  "frac": "23/2",
                  "rootIdx": 201
               },
               "names": {
                  "longName": "Danny Green 23+",
                  "shortName": "Danny Green 23+"
               },
               "wasPrice": [],
               "id": 204140611,
               "name": "Danny Green 23+",
               "type": "23",
               "suspended": true,
               "displayed": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Danny Green points (incl. overtime)",
            "shortName": "Danny Green points (incl. overtime)"
         },
         "id": 70230246,
         "name": "Danny Green points (incl. overtime)",
         "type": "BASKETBALL:FT:PLPOI",
         "subtype": "M#pre:playerprops:24750814:607862",
         "displayOrder": -275,
         "columnCount": 1,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.72",
                  "frac": "8/11",
                  "rootIdx": 60
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets +2.5",
                  "shortName": "Brooklyn Nets +2.5"
               },
               "wasPrice": [],
               "id": 203825920,
               "name": "Brooklyn Nets +2.5",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.05",
                  "frac": "21/20",
                  "rootIdx": 88
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers -2.5",
                  "shortName": "Philadelphia 76ers -2.5"
               },
               "wasPrice": [],
               "id": 203825929,
               "name": "Philadelphia 76ers -2.5",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131935,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#2.5",
         "line": 2.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.3",
                  "frac": "13/10",
                  "rootIdx": 103
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 26.5",
                  "shortName": "Under 26.5"
               },
               "wasPrice": [],
               "id": 203826000,
               "name": "Under 26.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.55",
                  "frac": "11/20",
                  "rootIdx": 51
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 26.5",
                  "shortName": "Over 26.5"
               },
               "wasPrice": [],
               "id": 203825985,
               "name": "Over 26.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Quarter Brooklyn Nets Total Points",
            "shortName": "1st Quarter Brooklyn Nets Total Points"
         },
         "id": 70131966,
         "name": "1st Quarter Brooklyn Nets Total Points",
         "type": "BASKETBALL:P:A:OU",
         "subtype": "Q1#26.5",
         "line": 26.5,
         "displayOrder": -318,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.87",
                  "frac": "87/100",
                  "rootIdx": 71
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets +3",
                  "shortName": "Brooklyn Nets +3"
               },
               "wasPrice": [],
               "id": 203827143,
               "name": "Brooklyn Nets +3",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2",
                  "frac": "1/1",
                  "rootIdx": 83
               },
               "pos": {
                  "row": 1,
                  "col": 3
               },
               "names": {
                  "longName": "Philadelphia 76ers -3",
                  "shortName": "Philadelphia 76ers -3"
               },
               "wasPrice": [],
               "id": 203827147,
               "name": "Philadelphia 76ers -3",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "20",
                  "frac": "19/1",
                  "rootIdx": 211
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Tie +3",
                  "shortName": "Tie +3"
               },
               "wasPrice": [],
               "id": 203827145,
               "name": "Tie +3",
               "type": "DH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Handicap (Excl. OT)",
            "shortName": "Handicap (Excl. OT)"
         },
         "id": 70132476,
         "name": "Handicap (Excl. OT)",
         "type": "BASKETBALL:FT:3HCP",
         "subtype": "M#+3",
         "line": 3.0,
         "displayOrder": -465,
         "columnCount": 3,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "Joel Embiid 2+",
                  "shortName": "Joel Embiid 2+"
               },
               "wasPrice": [],
               "id": 204140887,
               "name": "Joel Embiid 2+",
               "type": "2",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.17",
                  "frac": "17/100",
                  "rootIdx": 25
               },
               "pos": {
                  "row": 101,
                  "col": 1
               },
               "names": {
                  "longName": "Joel Embiid 1+",
                  "shortName": "Joel Embiid 1+"
               },
               "wasPrice": [],
               "id": 204140885,
               "name": "Joel Embiid 1+",
               "type": "1",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.95",
                  "frac": "59/20",
                  "rootIdx": 139
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Joel Embiid 3+",
                  "shortName": "Joel Embiid 3+"
               },
               "wasPrice": [],
               "id": 204140889,
               "name": "Joel Embiid 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "6",
                  "frac": "5/1",
                  "rootIdx": 172
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Joel Embiid 4+",
                  "shortName": "Joel Embiid 4+"
               },
               "wasPrice": [],
               "id": 204140891,
               "name": "Joel Embiid 4+",
               "type": "4",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Joel Embiid blocks (incl. overtime)",
            "shortName": "Joel Embiid blocks (incl. overtime)"
         },
         "id": 70230312,
         "name": "Joel Embiid blocks (incl. overtime)",
         "type": "BASKETBALL:FT:PLBLOC",
         "subtype": "M#pre:playerprops:24750814:607528",
         "displayOrder": -269,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "22",
                  "frac": "21/1",
                  "rootIdx": 213
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Tie +2",
                  "shortName": "Tie +2"
               },
               "wasPrice": [],
               "id": 203825733,
               "name": "Tie +2",
               "type": "DH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 3
               },
               "names": {
                  "longName": "Philadelphia 76ers -2",
                  "shortName": "Philadelphia 76ers -2"
               },
               "wasPrice": [],
               "id": 203825749,
               "name": "Philadelphia 76ers -2",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets +2",
                  "shortName": "Brooklyn Nets +2"
               },
               "wasPrice": [],
               "id": 203825719,
               "name": "Brooklyn Nets +2",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Handicap (Excl. OT)",
            "shortName": "Handicap (Excl. OT)"
         },
         "id": 70131857,
         "name": "Handicap (Excl. OT)",
         "type": "BASKETBALL:FT:3HCP",
         "subtype": "M#+2",
         "line": 2.0,
         "displayOrder": -465,
         "columnCount": 3,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.05",
                  "frac": "21/20",
                  "rootIdx": 88
               },
               "pos": {
                  "row": 1,
                  "col": 3
               },
               "names": {
                  "longName": "Philadelphia 76ers -1",
                  "shortName": "Philadelphia 76ers -1"
               },
               "wasPrice": [],
               "id": 203840891,
               "name": "Philadelphia 76ers -1",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "27",
                  "frac": "26/1",
                  "rootIdx": 218
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Tie +1",
                  "shortName": "Tie +1"
               },
               "wasPrice": [],
               "id": 203840887,
               "name": "Tie +1",
               "type": "DH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets +1",
                  "shortName": "Brooklyn Nets +1"
               },
               "wasPrice": [],
               "id": 203840889,
               "name": "Brooklyn Nets +1",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Handicap (Excl. OT)",
            "shortName": "Handicap (Excl. OT)"
         },
         "id": 70138232,
         "name": "Handicap (Excl. OT)",
         "type": "BASKETBALL:FT:3HCP",
         "subtype": "M#+1",
         "line": 1.0,
         "displayOrder": -465,
         "columnCount": 3,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.57",
                  "frac": "4/7",
                  "rootIdx": 52
               },
               "names": {
                  "longName": "Joel Embiid 12+",
                  "shortName": "Joel Embiid 12+"
               },
               "wasPrice": [],
               "id": 204140485,
               "name": "Joel Embiid 12+",
               "type": "12",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "19",
                  "frac": "18/1",
                  "rootIdx": 210
               },
               "names": {
                  "longName": "Joel Embiid 19+",
                  "shortName": "Joel Embiid 19+"
               },
               "wasPrice": [],
               "id": 204140499,
               "name": "Joel Embiid 19+",
               "type": "19",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "12.5",
                  "frac": "23/2",
                  "rootIdx": 201
               },
               "names": {
                  "longName": "Joel Embiid 18+",
                  "shortName": "Joel Embiid 18+"
               },
               "wasPrice": [],
               "id": 204140497,
               "name": "Joel Embiid 18+",
               "type": "18",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "6.75",
                  "frac": "23/4",
                  "rootIdx": 181
               },
               "names": {
                  "longName": "Joel Embiid 17+",
                  "shortName": "Joel Embiid 17+"
               },
               "wasPrice": [],
               "id": 204140495,
               "name": "Joel Embiid 17+",
               "type": "17",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4.35",
                  "frac": "67/20",
                  "rootIdx": 148
               },
               "names": {
                  "longName": "Joel Embiid 16+",
                  "shortName": "Joel Embiid 16+"
               },
               "wasPrice": [],
               "id": 204140493,
               "name": "Joel Embiid 16+",
               "type": "16",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.05",
                  "frac": "41/20",
                  "rootIdx": 121
               },
               "names": {
                  "longName": "Joel Embiid 15+",
                  "shortName": "Joel Embiid 15+"
               },
               "wasPrice": [],
               "id": 204140491,
               "name": "Joel Embiid 15+",
               "type": "15",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.7",
                  "frac": "7/10",
                  "rootIdx": 59
               },
               "names": {
                  "longName": "Joel Embiid 13+",
                  "shortName": "Joel Embiid 13+"
               },
               "wasPrice": [],
               "id": 204140487,
               "name": "Joel Embiid 13+",
               "type": "13",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.3",
                  "frac": "13/10",
                  "rootIdx": 103
               },
               "names": {
                  "longName": "Joel Embiid 14+",
                  "shortName": "Joel Embiid 14+"
               },
               "wasPrice": [],
               "id": 204140489,
               "name": "Joel Embiid 14+",
               "type": "14",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "28",
                  "frac": "27/1",
                  "rootIdx": 219
               },
               "names": {
                  "longName": "Joel Embiid 20+",
                  "shortName": "Joel Embiid 20+"
               },
               "wasPrice": [],
               "id": 204140501,
               "name": "Joel Embiid 20+",
               "type": "20",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Joel Embiid rebounds (incl. overtime)",
            "shortName": "Joel Embiid rebounds (incl. overtime)"
         },
         "id": 70230222,
         "name": "Joel Embiid rebounds (incl. overtime)",
         "type": "BASKETBALL:FT:PLREB",
         "subtype": "M#pre:playerprops:24750814:607528",
         "displayOrder": -274,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.55",
                  "frac": "31/20",
                  "rootIdx": 109
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 219.5",
                  "shortName": "Under 219.5"
               },
               "wasPrice": [],
               "id": 203826251,
               "name": "Under 219.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.47",
                  "frac": "40/85",
                  "rootIdx": 45
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 219.5",
                  "shortName": "Over 219.5"
               },
               "wasPrice": [],
               "id": 203826240,
               "name": "Over 219.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132092,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#219.5",
         "line": 219.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.4",
                  "frac": "12/5",
                  "rootIdx": 128
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Joe Harris 3+",
                  "shortName": "Joe Harris 3+"
               },
               "wasPrice": [],
               "id": 204140591,
               "name": "Joe Harris 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "20",
                  "frac": "19/1",
                  "rootIdx": 211
               },
               "pos": {
                  "row": 302,
                  "col": 1
               },
               "names": {
                  "longName": "Joe Harris 6+",
                  "shortName": "Joe Harris 6+"
               },
               "wasPrice": [],
               "id": 204140597,
               "name": "Joe Harris 6+",
               "type": "6",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "12.5",
                  "frac": "23/2",
                  "rootIdx": 201
               },
               "pos": {
                  "row": 202,
                  "col": 1
               },
               "names": {
                  "longName": "Joe Harris 5+",
                  "shortName": "Joe Harris 5+"
               },
               "wasPrice": [],
               "id": 204140595,
               "name": "Joe Harris 5+",
               "type": "5",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "6.75",
                  "frac": "23/4",
                  "rootIdx": 181
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Joe Harris 4+",
                  "shortName": "Joe Harris 4+"
               },
               "wasPrice": [],
               "id": 204140593,
               "name": "Joe Harris 4+",
               "type": "4",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Joe Harris assists (incl. overtime)",
            "shortName": "Joe Harris assists (incl. overtime)"
         },
         "id": 70230244,
         "name": "Joe Harris assists (incl. overtime)",
         "type": "BASKETBALL:FT:PLASS",
         "subtype": "M#pre:playerprops:24750814:607950",
         "displayOrder": -273,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "16",
                  "frac": "15/1",
                  "rootIdx": 207
               },
               "pos": {
                  "row": 103,
                  "col": 1
               },
               "names": {
                  "longName": "Jarrett Allen 7+",
                  "shortName": "Jarrett Allen 7+"
               },
               "wasPrice": [],
               "id": 204140727,
               "name": "Jarrett Allen 7+",
               "type": "7",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "8.5",
                  "frac": "15/2",
                  "rootIdx": 190
               },
               "pos": {
                  "row": 302,
                  "col": 1
               },
               "names": {
                  "longName": "Jarrett Allen 6+",
                  "shortName": "Jarrett Allen 6+"
               },
               "wasPrice": [],
               "id": 204140725,
               "name": "Jarrett Allen 6+",
               "type": "6",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4.5",
                  "frac": "7/2",
                  "rootIdx": 151
               },
               "pos": {
                  "row": 202,
                  "col": 1
               },
               "names": {
                  "longName": "Jarrett Allen 5+",
                  "shortName": "Jarrett Allen 5+"
               },
               "wasPrice": [],
               "id": 204140723,
               "name": "Jarrett Allen 5+",
               "type": "5",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.55",
                  "frac": "31/20",
                  "rootIdx": 109
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Jarrett Allen 4+",
                  "shortName": "Jarrett Allen 4+"
               },
               "wasPrice": [],
               "id": 204140721,
               "name": "Jarrett Allen 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.83",
                  "frac": "5/6",
                  "rootIdx": 69
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Jarrett Allen 3+",
                  "shortName": "Jarrett Allen 3+"
               },
               "wasPrice": [],
               "id": 204140719,
               "name": "Jarrett Allen 3+",
               "type": "3",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Jarrett Allen assists (incl. overtime)",
            "shortName": "Jarrett Allen assists (incl. overtime)"
         },
         "id": 70230274,
         "name": "Jarrett Allen assists (incl. overtime)",
         "type": "BASKETBALL:FT:PLASS",
         "subtype": "M#pre:playerprops:24750814:1124733",
         "displayOrder": -273,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.33",
                  "frac": "1/3",
                  "rootIdx": 37
               },
               "names": {
                  "longName": "Caris LeVert 3+",
                  "shortName": "Caris LeVert 3+"
               },
               "wasPrice": [],
               "id": 204587829,
               "name": "Caris LeVert 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "12.5",
                  "frac": "23/2",
                  "rootIdx": 201
               },
               "names": {
                  "longName": "Caris LeVert 9+",
                  "shortName": "Caris LeVert 9+"
               },
               "wasPrice": [],
               "id": 204141063,
               "name": "Caris LeVert 9+",
               "type": "9",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "6.9",
                  "frac": "59/10",
                  "rootIdx": 183
               },
               "pos": {
                  "row": 203,
                  "col": 1
               },
               "names": {
                  "longName": "Caris LeVert 8+",
                  "shortName": "Caris LeVert 8+"
               },
               "wasPrice": [],
               "id": 204141061,
               "name": "Caris LeVert 8+",
               "type": "8",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4",
                  "frac": "3/1",
                  "rootIdx": 140
               },
               "pos": {
                  "row": 103,
                  "col": 1
               },
               "names": {
                  "longName": "Caris LeVert 7+",
                  "shortName": "Caris LeVert 7+"
               },
               "wasPrice": [],
               "id": 204141059,
               "name": "Caris LeVert 7+",
               "type": "7",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.6",
                  "frac": "8/5",
                  "rootIdx": 110
               },
               "pos": {
                  "row": 302,
                  "col": 1
               },
               "names": {
                  "longName": "Caris LeVert 6+",
                  "shortName": "Caris LeVert 6+"
               },
               "wasPrice": [],
               "id": 204141057,
               "name": "Caris LeVert 6+",
               "type": "6",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 202,
                  "col": 1
               },
               "names": {
                  "longName": "Caris LeVert 5+",
                  "shortName": "Caris LeVert 5+"
               },
               "wasPrice": [],
               "id": 204141055,
               "name": "Caris LeVert 5+",
               "type": "5",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Caris LeVert 4+",
                  "shortName": "Caris LeVert 4+"
               },
               "wasPrice": [],
               "id": 204141053,
               "name": "Caris LeVert 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "31",
                  "frac": "30/1",
                  "rootIdx": 222
               },
               "names": {
                  "longName": "Caris LeVert 10+",
                  "shortName": "Caris LeVert 10+"
               },
               "wasPrice": [],
               "id": 204141051,
               "name": "Caris LeVert 10+",
               "type": "10",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Caris LeVert rebounds (incl. overtime)",
            "shortName": "Caris LeVert rebounds (incl. overtime)"
         },
         "id": 70230344,
         "name": "Caris LeVert rebounds (incl. overtime)",
         "type": "BASKETBALL:FT:PLREB",
         "subtype": "M#pre:playerprops:24750814:996291",
         "displayOrder": -274,
         "columnCount": 1,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.25",
                  "frac": "5/4",
                  "rootIdx": 101
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 222.5",
                  "shortName": "Under 222.5"
               },
               "wasPrice": [],
               "id": 203826141,
               "name": "Under 222.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.62",
                  "frac": "8/13",
                  "rootIdx": 54
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 222.5",
                  "shortName": "Over 222.5"
               },
               "wasPrice": [],
               "id": 203826131,
               "name": "Over 222.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132039,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#222.5",
         "line": 222.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3.9",
                  "frac": "29/10",
                  "rootIdx": 138
               },
               "pos": {
                  "row": 2,
                  "col": 2
               },
               "names": {
                  "longName": "Brooklyn Nets & Over 225.5",
                  "shortName": "Brooklyn Nets & Over 225.5"
               },
               "wasPrice": [],
               "id": 203840885,
               "name": "Brooklyn Nets & Over 225.5",
               "type": "AOver",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.3",
                  "frac": "23/10",
                  "rootIdx": 126
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Philadelphia 76ers & Under 225.5",
                  "shortName": "Philadelphia 76ers & Under 225.5"
               },
               "wasPrice": [],
               "id": 203840883,
               "name": "Philadelphia 76ers & Under 225.5",
               "type": "BUnder",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.35",
                  "frac": "47/20",
                  "rootIdx": 127
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers & Over 225.5",
                  "shortName": "Philadelphia 76ers & Over 225.5"
               },
               "wasPrice": [],
               "id": 203840879,
               "name": "Philadelphia 76ers & Over 225.5",
               "type": "BOver",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.85",
                  "frac": "57/20",
                  "rootIdx": 137
               },
               "pos": {
                  "row": 2,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets & Under 225.5",
                  "shortName": "Brooklyn Nets & Under 225.5"
               },
               "wasPrice": [],
               "id": 203840881,
               "name": "Brooklyn Nets & Under 225.5",
               "type": "AUnder",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Matchbet and Total Points (Incl. OT)",
            "shortName": "Matchbet and Total Points (Incl. OT)"
         },
         "id": 70138230,
         "name": "Matchbet and Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:AXB$OU",
         "subtype": "M#225.5",
         "line": 225.5,
         "displayOrder": -425,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 56.5",
                  "shortName": "Under 56.5"
               },
               "wasPrice": [],
               "id": 203849759,
               "name": "Under 56.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.75",
                  "frac": "3/4",
                  "rootIdx": 63
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 56.5",
                  "shortName": "Over 56.5"
               },
               "wasPrice": [],
               "id": 203849757,
               "name": "Over 56.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Quarter Total Points",
            "shortName": "1st Quarter Total Points"
         },
         "id": 70141566,
         "name": "1st Quarter Total Points",
         "type": "BASKETBALL:P:OU",
         "subtype": "Q1#56.5",
         "line": 56.5,
         "displayOrder": -323,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "12",
                  "frac": "11/1",
                  "rootIdx": 200
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Tie",
                  "shortName": "Tie"
               },
               "wasPrice": [],
               "id": 203825702,
               "name": "Tie",
               "type": "Draw",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2",
                  "frac": "1/1",
                  "rootIdx": 83
               },
               "pos": {
                  "row": 1,
                  "col": 3
               },
               "names": {
                  "longName": "Philadelphia 76ers",
                  "shortName": "Philadelphia 76ers"
               },
               "wasPrice": [],
               "id": 203825723,
               "name": "Philadelphia 76ers",
               "type": "B",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.05",
                  "frac": "21/20",
                  "rootIdx": 88
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets",
                  "shortName": "Brooklyn Nets"
               },
               "wasPrice": [],
               "id": 203825692,
               "name": "Brooklyn Nets",
               "type": "A",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Money Line 3Way",
            "shortName": "Money Line 3Way"
         },
         "id": 70131854,
         "name": "Money Line 3Way",
         "type": "BASKETBALL:FT:AXB",
         "subtype": "M",
         "displayOrder": -481,
         "columnCount": 3,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3.15",
                  "frac": "43/20",
                  "rootIdx": 123
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers -7.5",
                  "shortName": "Philadelphia 76ers -7.5"
               },
               "wasPrice": [],
               "id": 203826023,
               "name": "Philadelphia 76ers -7.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.33",
                  "frac": "1/3",
                  "rootIdx": 37
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets +7.5",
                  "shortName": "Brooklyn Nets +7.5"
               },
               "wasPrice": [],
               "id": 203826015,
               "name": "Brooklyn Nets +7.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131984,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#7.5",
         "line": 7.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.7",
                  "frac": "7/10",
                  "rootIdx": 59
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 29.5",
                  "shortName": "Under 29.5"
               },
               "wasPrice": [],
               "id": 203840877,
               "name": "Under 29.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.05",
                  "frac": "21/20",
                  "rootIdx": 88
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 29.5",
                  "shortName": "Over 29.5"
               },
               "wasPrice": [],
               "id": 203840875,
               "name": "Over 29.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Quarter Brooklyn Nets Total Points",
            "shortName": "1st Quarter Brooklyn Nets Total Points"
         },
         "id": 70138228,
         "name": "1st Quarter Brooklyn Nets Total Points",
         "type": "BASKETBALL:P:A:OU",
         "subtype": "Q1#29.5",
         "line": 29.5,
         "displayOrder": -318,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.12",
                  "frac": "1/8",
                  "rootIdx": 19
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets +13.5",
                  "shortName": "Brooklyn Nets +13.5"
               },
               "wasPrice": [],
               "id": 203825891,
               "name": "Brooklyn Nets +13.5",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "5.75",
                  "frac": "19/4",
                  "rootIdx": 169
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers -13.5",
                  "shortName": "Philadelphia 76ers -13.5"
               },
               "wasPrice": [],
               "id": 203825905,
               "name": "Philadelphia 76ers -13.5",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131927,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#13.5",
         "line": 13.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "6.2",
                  "frac": "26/5",
                  "rootIdx": 174
               },
               "pos": {
                  "row": 302,
                  "col": 1
               },
               "names": {
                  "longName": "Tobias Harris 6+",
                  "shortName": "Tobias Harris 6+"
               },
               "wasPrice": [],
               "id": 204140437,
               "name": "Tobias Harris 6+",
               "type": "6",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "11",
                  "frac": "10/1",
                  "rootIdx": 198
               },
               "pos": {
                  "row": 103,
                  "col": 1
               },
               "names": {
                  "longName": "Tobias Harris 7+",
                  "shortName": "Tobias Harris 7+"
               },
               "wasPrice": [],
               "id": 204140439,
               "name": "Tobias Harris 7+",
               "type": "7",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.45",
                  "frac": "49/20",
                  "rootIdx": 129
               },
               "pos": {
                  "row": 202,
                  "col": 1
               },
               "names": {
                  "longName": "Tobias Harris 5+",
                  "shortName": "Tobias Harris 5+"
               },
               "wasPrice": [],
               "id": 204140435,
               "name": "Tobias Harris 5+",
               "type": "5",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.1",
                  "frac": "11/10",
                  "rootIdx": 93
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Tobias Harris 4+",
                  "shortName": "Tobias Harris 4+"
               },
               "wasPrice": [],
               "id": 204140433,
               "name": "Tobias Harris 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.45",
                  "frac": "9/20",
                  "rootIdx": 44
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Tobias Harris 3+",
                  "shortName": "Tobias Harris 3+"
               },
               "wasPrice": [],
               "id": 204140431,
               "name": "Tobias Harris 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "19",
                  "frac": "18/1",
                  "rootIdx": 210
               },
               "pos": {
                  "row": 203,
                  "col": 1
               },
               "names": {
                  "longName": "Tobias Harris 8+",
                  "shortName": "Tobias Harris 8+"
               },
               "wasPrice": [],
               "id": 204140441,
               "name": "Tobias Harris 8+",
               "type": "8",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Tobias Harris assists (incl. overtime)",
            "shortName": "Tobias Harris assists (incl. overtime)"
         },
         "id": 70230210,
         "name": "Tobias Harris assists (incl. overtime)",
         "type": "BASKETBALL:FT:PLASS",
         "subtype": "M#pre:playerprops:24750814:608250",
         "displayOrder": -273,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4.65",
                  "frac": "73/20",
                  "rootIdx": 154
               },
               "names": {
                  "longName": "Ben Simmons 13+",
                  "shortName": "Ben Simmons 13+"
               },
               "wasPrice": [],
               "id": 204141165,
               "name": "Ben Simmons 13+",
               "type": "13",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.2",
                  "frac": "11/5",
                  "rootIdx": 124
               },
               "names": {
                  "longName": "Ben Simmons 12+",
                  "shortName": "Ben Simmons 12+"
               },
               "wasPrice": [],
               "id": 204141163,
               "name": "Ben Simmons 12+",
               "type": "12",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.35",
                  "frac": "27/20",
                  "rootIdx": 104
               },
               "names": {
                  "longName": "Ben Simmons 11+",
                  "shortName": "Ben Simmons 11+"
               },
               "wasPrice": [],
               "id": 204141161,
               "name": "Ben Simmons 11+",
               "type": "11",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2",
                  "frac": "1/1",
                  "rootIdx": 83
               },
               "names": {
                  "longName": "Ben Simmons 10+",
                  "shortName": "Ben Simmons 10+"
               },
               "wasPrice": [],
               "id": 204141159,
               "name": "Ben Simmons 10+",
               "type": "10",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "14.5",
                  "frac": "27/2",
                  "rootIdx": 205
               },
               "names": {
                  "longName": "Ben Simmons 15+",
                  "shortName": "Ben Simmons 15+"
               },
               "wasPrice": [],
               "id": 204141169,
               "name": "Ben Simmons 15+",
               "type": "15",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.57",
                  "frac": "4/7",
                  "rootIdx": 52
               },
               "names": {
                  "longName": "Ben Simmons 9+",
                  "shortName": "Ben Simmons 9+"
               },
               "wasPrice": [],
               "id": 204141175,
               "name": "Ben Simmons 9+",
               "type": "9",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "32",
                  "frac": "31/1",
                  "rootIdx": 223
               },
               "names": {
                  "longName": "Ben Simmons 17+",
                  "shortName": "Ben Simmons 17+"
               },
               "wasPrice": [],
               "id": 204141173,
               "name": "Ben Simmons 17+",
               "type": "17",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "23",
                  "frac": "22/1",
                  "rootIdx": 214
               },
               "names": {
                  "longName": "Ben Simmons 16+",
                  "shortName": "Ben Simmons 16+"
               },
               "wasPrice": [],
               "id": 204141171,
               "name": "Ben Simmons 16+",
               "type": "16",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "7.25",
                  "frac": "25/4",
                  "rootIdx": 185
               },
               "names": {
                  "longName": "Ben Simmons 14+",
                  "shortName": "Ben Simmons 14+"
               },
               "wasPrice": [],
               "id": 204141167,
               "name": "Ben Simmons 14+",
               "type": "14",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Ben Simmons rebounds (incl. overtime)",
            "shortName": "Ben Simmons rebounds (incl. overtime)"
         },
         "id": 70230364,
         "name": "Ben Simmons rebounds (incl. overtime)",
         "type": "BASKETBALL:FT:PLREB",
         "subtype": "M#pre:playerprops:24750814:996289",
         "displayOrder": -274,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.1",
                  "frac": "11/10",
                  "rootIdx": 93
               },
               "pos": {
                  "row": 101,
                  "col": 1
               },
               "names": {
                  "longName": "Shake Milton 1+",
                  "shortName": "Shake Milton 1+"
               },
               "wasPrice": [],
               "id": 204141039,
               "name": "Shake Milton 1+",
               "type": "1",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "25",
                  "frac": "24/1",
                  "rootIdx": 216
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Shake Milton 4+",
                  "shortName": "Shake Milton 4+"
               },
               "wasPrice": [],
               "id": 204141045,
               "name": "Shake Milton 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "7",
                  "frac": "6/1",
                  "rootIdx": 184
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Shake Milton 3+",
                  "shortName": "Shake Milton 3+"
               },
               "wasPrice": [],
               "id": 204141043,
               "name": "Shake Milton 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4.25",
                  "frac": "13/4",
                  "rootIdx": 145
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "Shake Milton 2+",
                  "shortName": "Shake Milton 2+"
               },
               "wasPrice": [],
               "id": 204141041,
               "name": "Shake Milton 2+",
               "type": "2",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Shake Milton blocks (incl. overtime)",
            "shortName": "Shake Milton blocks (incl. overtime)"
         },
         "id": 70230340,
         "name": "Shake Milton blocks (incl. overtime)",
         "type": "BASKETBALL:FT:PLBLOC",
         "subtype": "M#pre:playerprops:24750814:1495351",
         "displayOrder": -269,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "5.9",
                  "frac": "49/10",
                  "rootIdx": 171
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Ben Simmons 3+",
                  "shortName": "Ben Simmons 3+"
               },
               "wasPrice": [],
               "id": 204140541,
               "name": "Ben Simmons 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "18",
                  "frac": "17/1",
                  "rootIdx": 209
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Ben Simmons 4+",
                  "shortName": "Ben Simmons 4+"
               },
               "wasPrice": [],
               "id": 204140543,
               "name": "Ben Simmons 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3",
                  "frac": "2/1",
                  "rootIdx": 120
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "Ben Simmons 2+",
                  "shortName": "Ben Simmons 2+"
               },
               "wasPrice": [],
               "id": 204140539,
               "name": "Ben Simmons 2+",
               "type": "2",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.5",
                  "frac": "1/2",
                  "rootIdx": 47
               },
               "pos": {
                  "row": 101,
                  "col": 1
               },
               "names": {
                  "longName": "Ben Simmons 1+",
                  "shortName": "Ben Simmons 1+"
               },
               "wasPrice": [],
               "id": 204140537,
               "name": "Ben Simmons 1+",
               "type": "1",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Ben Simmons blocks (incl. overtime)",
            "shortName": "Ben Simmons blocks (incl. overtime)"
         },
         "id": 70230232,
         "name": "Ben Simmons blocks (incl. overtime)",
         "type": "BASKETBALL:FT:PLBLOC",
         "subtype": "M#pre:playerprops:24750814:996289",
         "displayOrder": -269,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.44",
                  "frac": "4/9",
                  "rootIdx": 43
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers +5.5",
                  "shortName": "Philadelphia 76ers +5.5"
               },
               "wasPrice": [],
               "id": 203825803,
               "name": "Philadelphia 76ers +5.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.65",
                  "frac": "33/20",
                  "rootIdx": 112
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets -5.5",
                  "shortName": "Brooklyn Nets -5.5"
               },
               "wasPrice": [],
               "id": 203825792,
               "name": "Brooklyn Nets -5.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131884,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-5.5",
         "line": -5.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.85",
                  "frac": "37/20",
                  "rootIdx": 116
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 234.5",
                  "shortName": "Over 234.5"
               },
               "wasPrice": [],
               "id": 203826352,
               "name": "Over 234.5",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.4",
                  "frac": "2/5",
                  "rootIdx": 41
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 234.5",
                  "shortName": "Under 234.5"
               },
               "wasPrice": [],
               "id": 203826360,
               "name": "Under 234.5",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132132,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#234.5",
         "line": 234.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3.75",
                  "frac": "11/4",
                  "rootIdx": 135
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 211.5",
                  "shortName": "Under 211.5"
               },
               "wasPrice": [],
               "id": 203826108,
               "name": "Under 211.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.25",
                  "frac": "1/4",
                  "rootIdx": 32
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 211.5",
                  "shortName": "Over 211.5"
               },
               "wasPrice": [],
               "id": 203826085,
               "name": "Over 211.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132013,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#211.5",
         "line": 211.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.75",
                  "frac": "3/4",
                  "rootIdx": 63
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 58.5",
                  "shortName": "Under 58.5"
               },
               "wasPrice": [],
               "id": 204529989,
               "name": "Under 58.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 58.5",
                  "shortName": "Over 58.5"
               },
               "wasPrice": [],
               "id": 204529991,
               "name": "Over 58.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Half Brooklyn Nets Total Points",
            "shortName": "1st Half Brooklyn Nets Total Points"
         },
         "id": 70313078,
         "name": "1st Half Brooklyn Nets Total Points",
         "type": "BASKETBALL:P:A:OU",
         "subtype": "H1#58.5",
         "line": 58.5,
         "displayOrder": -355,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "H1"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "21",
                  "frac": "20/1",
                  "rootIdx": 212
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Caris LeVert 4+",
                  "shortName": "Caris LeVert 4+"
               },
               "wasPrice": [],
               "id": 204141369,
               "name": "Caris LeVert 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "6.2",
                  "frac": "26/5",
                  "rootIdx": 174
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Caris LeVert 3+",
                  "shortName": "Caris LeVert 3+"
               },
               "wasPrice": [],
               "id": 204141367,
               "name": "Caris LeVert 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.5",
                  "frac": "5/2",
                  "rootIdx": 130
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "Caris LeVert 2+",
                  "shortName": "Caris LeVert 2+"
               },
               "wasPrice": [],
               "id": 204141365,
               "name": "Caris LeVert 2+",
               "type": "2",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 101,
                  "col": 1
               },
               "names": {
                  "longName": "Caris LeVert 1+",
                  "shortName": "Caris LeVert 1+"
               },
               "wasPrice": [],
               "id": 204141363,
               "name": "Caris LeVert 1+",
               "type": "1",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Caris LeVert blocks (incl. overtime)",
            "shortName": "Caris LeVert blocks (incl. overtime)"
         },
         "id": 70230410,
         "name": "Caris LeVert blocks (incl. overtime)",
         "type": "BASKETBALL:FT:PLBLOC",
         "subtype": "M#pre:playerprops:24750814:996291",
         "displayOrder": -269,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "10",
                  "frac": "9/1",
                  "rootIdx": 196
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Timothe Luwawu-Cabarrot 3+",
                  "shortName": "Timothe Luwawu-Cabarrot 3+"
               },
               "wasPrice": [],
               "id": 204141347,
               "name": "Timothe Luwawu-Cabarrot 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "5.5",
                  "frac": "9/2",
                  "rootIdx": 166
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "Timothe Luwawu-Cabarrot 2+",
                  "shortName": "Timothe Luwawu-Cabarrot 2+"
               },
               "wasPrice": [],
               "id": 204141345,
               "name": "Timothe Luwawu-Cabarrot 2+",
               "type": "2",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.85",
                  "frac": "37/20",
                  "rootIdx": 116
               },
               "pos": {
                  "row": 101,
                  "col": 1
               },
               "names": {
                  "longName": "Timothe Luwawu-Cabarrot 1+",
                  "shortName": "Timothe Luwawu-Cabarrot 1+"
               },
               "wasPrice": [],
               "id": 204141343,
               "name": "Timothe Luwawu-Cabarrot 1+",
               "type": "1",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "30",
                  "frac": "29/1",
                  "rootIdx": 221
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Timothe Luwawu-Cabarrot 4+",
                  "shortName": "Timothe Luwawu-Cabarrot 4+"
               },
               "wasPrice": [],
               "id": 204141349,
               "name": "Timothe Luwawu-Cabarrot 4+",
               "type": "4",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Timothe Luwawu-Cabarrot blocks (incl. overtime)",
            "shortName": "Timothe Luwawu-Cabarrot blocks (incl. overtime)"
         },
         "id": 70230404,
         "name": "Timothe Luwawu-Cabarrot blocks (incl. overtime)",
         "type": "BASKETBALL:FT:PLBLOC",
         "subtype": "M#pre:playerprops:24750814:852058",
         "displayOrder": -269,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.6",
                  "frac": "8/5",
                  "rootIdx": 110
               },
               "names": {
                  "longName": "Joel Embiid 32+",
                  "shortName": "Joel Embiid 32+"
               },
               "wasPrice": [],
               "id": 204140973,
               "name": "Joel Embiid 32+",
               "type": "32",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.25",
                  "frac": "5/4",
                  "rootIdx": 101
               },
               "names": {
                  "longName": "Joel Embiid 30+",
                  "shortName": "Joel Embiid 30+"
               },
               "wasPrice": [],
               "id": 204140971,
               "name": "Joel Embiid 30+",
               "type": "30",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "-",
                  "frac": "-",
                  "rootIdx": -1
               },
               "names": {
                  "longName": "{outcomeName}",
                  "shortName": "{outcomeName}"
               },
               "wasPrice": [],
               "id": 204162763,
               "name": "{outcomeName}",
               "type": "39",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "-",
                  "frac": "-",
                  "rootIdx": -1
               },
               "names": {
                  "longName": "{outcomeName}",
                  "shortName": "{outcomeName}"
               },
               "wasPrice": [],
               "id": 204162761,
               "name": "{outcomeName}",
               "type": "37",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "-",
                  "frac": "-",
                  "rootIdx": -1
               },
               "names": {
                  "longName": "{outcomeName}",
                  "shortName": "{outcomeName}"
               },
               "wasPrice": [],
               "id": 204162749,
               "name": "{outcomeName}",
               "type": "25",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "-",
                  "frac": "-",
                  "rootIdx": -1
               },
               "names": {
                  "longName": "{outcomeName}",
                  "shortName": "{outcomeName}"
               },
               "wasPrice": [],
               "id": 204162747,
               "name": "{outcomeName}",
               "type": "41",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "6.7",
                  "frac": "57/10",
                  "rootIdx": 180
               },
               "names": {
                  "longName": "Joel Embiid 40+",
                  "shortName": "Joel Embiid 40+"
               },
               "wasPrice": [],
               "id": 204140981,
               "name": "Joel Embiid 40+",
               "type": "40",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2",
                  "frac": "1/1",
                  "rootIdx": 83
               },
               "names": {
                  "longName": "Joel Embiid 28+",
                  "shortName": "Joel Embiid 28+"
               },
               "wasPrice": [],
               "id": 204140969,
               "name": "Joel Embiid 28+",
               "type": "28",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "names": {
                  "longName": "Joel Embiid 26+",
                  "shortName": "Joel Embiid 26+"
               },
               "wasPrice": [],
               "id": 204140967,
               "name": "Joel Embiid 26+",
               "type": "26",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.42",
                  "frac": "21/50",
                  "rootIdx": 42
               },
               "names": {
                  "longName": "Joel Embiid 24+",
                  "shortName": "Joel Embiid 24+"
               },
               "wasPrice": [],
               "id": 204140965,
               "name": "Joel Embiid 24+",
               "type": "24",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "-",
                  "frac": "-",
                  "rootIdx": -1
               },
               "names": {
                  "longName": "{outcomeName}",
                  "shortName": "{outcomeName}"
               },
               "wasPrice": [],
               "id": 204162759,
               "name": "{outcomeName}",
               "type": "35",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "-",
                  "frac": "-",
                  "rootIdx": -1
               },
               "names": {
                  "longName": "{outcomeName}",
                  "shortName": "{outcomeName}"
               },
               "wasPrice": [],
               "id": 204162757,
               "name": "{outcomeName}",
               "type": "33",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "-",
                  "frac": "-",
                  "rootIdx": -1
               },
               "names": {
                  "longName": "{outcomeName}",
                  "shortName": "{outcomeName}"
               },
               "wasPrice": [],
               "id": 204162755,
               "name": "{outcomeName}",
               "type": "31",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "-",
                  "frac": "-",
                  "rootIdx": -1
               },
               "names": {
                  "longName": "{outcomeName}",
                  "shortName": "{outcomeName}"
               },
               "wasPrice": [],
               "id": 204162753,
               "name": "{outcomeName}",
               "type": "29",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "-",
                  "frac": "-",
                  "rootIdx": -1
               },
               "names": {
                  "longName": "{outcomeName}",
                  "shortName": "{outcomeName}"
               },
               "wasPrice": [],
               "id": 204162751,
               "name": "{outcomeName}",
               "type": "27",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "5.2",
                  "frac": "21/5",
                  "rootIdx": 162
               },
               "names": {
                  "longName": "Joel Embiid 38+",
                  "shortName": "Joel Embiid 38+"
               },
               "wasPrice": [],
               "id": 204140979,
               "name": "Joel Embiid 38+",
               "type": "38",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4.1",
                  "frac": "31/10",
                  "rootIdx": 142
               },
               "names": {
                  "longName": "Joel Embiid 36+",
                  "shortName": "Joel Embiid 36+"
               },
               "wasPrice": [],
               "id": 204140977,
               "name": "Joel Embiid 36+",
               "type": "36",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.25",
                  "frac": "9/4",
                  "rootIdx": 125
               },
               "names": {
                  "longName": "Joel Embiid 34+",
                  "shortName": "Joel Embiid 34+"
               },
               "wasPrice": [],
               "id": 204140975,
               "name": "Joel Embiid 34+",
               "type": "34",
               "suspended": true,
               "displayed": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Joel Embiid points (incl. overtime)",
            "shortName": "Joel Embiid points (incl. overtime)"
         },
         "id": 70230326,
         "name": "Joel Embiid points (incl. overtime)",
         "type": "BASKETBALL:FT:PLPOI",
         "subtype": "M#pre:playerprops:24750814:607528",
         "displayOrder": -275,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.95",
                  "frac": "39/20",
                  "rootIdx": 119
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "Seth Curry 2+",
                  "shortName": "Seth Curry 2+"
               },
               "wasPrice": [],
               "id": 204141337,
               "name": "Seth Curry 2+",
               "type": "2",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.6",
                  "frac": "3/5",
                  "rootIdx": 53
               },
               "pos": {
                  "row": 101,
                  "col": 1
               },
               "names": {
                  "longName": "Seth Curry 1+",
                  "shortName": "Seth Curry 1+"
               },
               "wasPrice": [],
               "id": 204141335,
               "name": "Seth Curry 1+",
               "type": "1",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "10",
                  "frac": "9/1",
                  "rootIdx": 196
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Seth Curry 4+",
                  "shortName": "Seth Curry 4+"
               },
               "wasPrice": [],
               "id": 204141341,
               "name": "Seth Curry 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "5.5",
                  "frac": "9/2",
                  "rootIdx": 166
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Seth Curry 3+",
                  "shortName": "Seth Curry 3+"
               },
               "wasPrice": [],
               "id": 204141339,
               "name": "Seth Curry 3+",
               "type": "3",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Seth Curry steals (incl. overtime)",
            "shortName": "Seth Curry steals (incl. overtime)"
         },
         "id": 70230402,
         "name": "Seth Curry steals (incl. overtime)",
         "type": "BASKETBALL:FT:PLSTA",
         "subtype": "M#pre:playerprops:24750814:607326",
         "displayOrder": -270,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "8",
                  "frac": "7/1",
                  "rootIdx": 188
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "DeAndre Jordan 4+",
                  "shortName": "DeAndre Jordan 4+"
               },
               "wasPrice": [],
               "id": 204141261,
               "name": "DeAndre Jordan 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4.5",
                  "frac": "7/2",
                  "rootIdx": 151
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "DeAndre Jordan 3+",
                  "shortName": "DeAndre Jordan 3+"
               },
               "wasPrice": [],
               "id": 204141259,
               "name": "DeAndre Jordan 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.1",
                  "frac": "11/10",
                  "rootIdx": 93
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "DeAndre Jordan 2+",
                  "shortName": "DeAndre Jordan 2+"
               },
               "wasPrice": [],
               "id": 204141257,
               "name": "DeAndre Jordan 2+",
               "type": "2",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.22",
                  "frac": "2/9",
                  "rootIdx": 30
               },
               "pos": {
                  "row": 101,
                  "col": 1
               },
               "names": {
                  "longName": "DeAndre Jordan 1+",
                  "shortName": "DeAndre Jordan 1+"
               },
               "wasPrice": [],
               "id": 204141255,
               "name": "DeAndre Jordan 1+",
               "type": "1",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "DeAndre Jordan blocks (incl. overtime)",
            "shortName": "DeAndre Jordan blocks (incl. overtime)"
         },
         "id": 70230386,
         "name": "DeAndre Jordan blocks (incl. overtime)",
         "type": "BASKETBALL:FT:PLBLOC",
         "subtype": "M#pre:playerprops:24750814:607508",
         "displayOrder": -269,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.36",
                  "frac": "4/11",
                  "rootIdx": 39
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "Joe Harris 2+",
                  "shortName": "Joe Harris 2+"
               },
               "wasPrice": [],
               "id": 204140921,
               "name": "Joe Harris 2+",
               "type": "2",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "14.5",
                  "frac": "27/2",
                  "rootIdx": 205
               },
               "pos": {
                  "row": 302,
                  "col": 1
               },
               "names": {
                  "longName": "Joe Harris 6+",
                  "shortName": "Joe Harris 6+"
               },
               "wasPrice": [],
               "id": 204140929,
               "name": "Joe Harris 6+",
               "type": "6",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "27",
                  "frac": "26/1",
                  "rootIdx": 218
               },
               "pos": {
                  "row": 103,
                  "col": 1
               },
               "names": {
                  "longName": "Joe Harris 7+",
                  "shortName": "Joe Harris 7+"
               },
               "wasPrice": [],
               "id": 204140931,
               "name": "Joe Harris 7+",
               "type": "7",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "5.9",
                  "frac": "49/10",
                  "rootIdx": 171
               },
               "pos": {
                  "row": 202,
                  "col": 1
               },
               "names": {
                  "longName": "Joe Harris 5+",
                  "shortName": "Joe Harris 5+"
               },
               "wasPrice": [],
               "id": 204140927,
               "name": "Joe Harris 5+",
               "type": "5",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.9",
                  "frac": "19/10",
                  "rootIdx": 118
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Joe Harris 4+",
                  "shortName": "Joe Harris 4+"
               },
               "wasPrice": [],
               "id": 204140925,
               "name": "Joe Harris 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.2",
                  "frac": "6/5",
                  "rootIdx": 99
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Joe Harris 3+",
                  "shortName": "Joe Harris 3+"
               },
               "wasPrice": [],
               "id": 204140923,
               "name": "Joe Harris 3+",
               "type": "3",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Joe Harris 3-point field goals (incl. overtime)",
            "shortName": "Joe Harris 3-point field goals (incl. overtime)"
         },
         "id": 70230320,
         "name": "Joe Harris 3-point field goals (incl. overtime)",
         "type": "BASKETBALL:FT:3PFG",
         "subtype": "M#pre:playerprops:24750814:607950",
         "displayOrder": -272,
         "columnCount": 1,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.85",
                  "frac": "17/20",
                  "rootIdx": 70
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets",
                  "shortName": "Brooklyn Nets"
               },
               "wasPrice": [],
               "id": 203825699,
               "name": "Brooklyn Nets",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.85",
                  "frac": "17/20",
                  "rootIdx": 70
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers",
                  "shortName": "Philadelphia 76ers"
               },
               "wasPrice": [],
               "id": 203825715,
               "name": "Philadelphia 76ers",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Half Money Line",
            "shortName": "1st Half Money Line"
         },
         "id": 70131855,
         "name": "1st Half Money Line",
         "type": "BASKETBALL:P:DNB",
         "subtype": "H1",
         "displayOrder": -375,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "H1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.36",
                  "frac": "4/11",
                  "rootIdx": 39
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 235.5",
                  "shortName": "Under 235.5"
               },
               "wasPrice": [],
               "id": 203826387,
               "name": "Under 235.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.9",
                  "frac": "19/10",
                  "rootIdx": 118
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 235.5",
                  "shortName": "Over 235.5"
               },
               "wasPrice": [],
               "id": 203826385,
               "name": "Over 235.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132138,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#235.5",
         "line": 235.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.5",
                  "frac": "5/2",
                  "rootIdx": 130
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "Danny Green 2+",
                  "shortName": "Danny Green 2+"
               },
               "wasPrice": [],
               "id": 204140505,
               "name": "Danny Green 2+",
               "type": "2",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 101,
                  "col": 1
               },
               "names": {
                  "longName": "Danny Green 1+",
                  "shortName": "Danny Green 1+"
               },
               "wasPrice": [],
               "id": 204140503,
               "name": "Danny Green 1+",
               "type": "1",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "21",
                  "frac": "20/1",
                  "rootIdx": 212
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Danny Green 4+",
                  "shortName": "Danny Green 4+"
               },
               "wasPrice": [],
               "id": 204140509,
               "name": "Danny Green 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "6.2",
                  "frac": "26/5",
                  "rootIdx": 174
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Danny Green 3+",
                  "shortName": "Danny Green 3+"
               },
               "wasPrice": [],
               "id": 204140507,
               "name": "Danny Green 3+",
               "type": "3",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Danny Green blocks (incl. overtime)",
            "shortName": "Danny Green blocks (incl. overtime)"
         },
         "id": 70230224,
         "name": "Danny Green blocks (incl. overtime)",
         "type": "BASKETBALL:FT:PLBLOC",
         "subtype": "M#pre:playerprops:24750814:607862",
         "displayOrder": -269,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.85",
                  "frac": "17/20",
                  "rootIdx": 70
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets",
                  "shortName": "Brooklyn Nets"
               },
               "wasPrice": [],
               "id": 203825703,
               "name": "Brooklyn Nets",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.85",
                  "frac": "17/20",
                  "rootIdx": 70
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers",
                  "shortName": "Philadelphia 76ers"
               },
               "wasPrice": [],
               "id": 203825724,
               "name": "Philadelphia 76ers",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "2nd Half Money Line (Excl. OT)",
            "shortName": "2nd Half Money Line (Excl. OT)"
         },
         "id": 70131856,
         "name": "2nd Half Money Line (Excl. OT)",
         "type": "BASKETBALL:P:DNB",
         "subtype": "H2",
         "displayOrder": -345,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "H2"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.7",
                  "frac": "7/10",
                  "rootIdx": 59
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 223.5",
                  "shortName": "Over 223.5"
               },
               "wasPrice": [],
               "id": 203826180,
               "name": "Over 223.5",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.1",
                  "frac": "11/10",
                  "rootIdx": 93
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 223.5",
                  "shortName": "Under 223.5"
               },
               "wasPrice": [],
               "id": 203826189,
               "name": "Under 223.5",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132057,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#223.5",
         "line": 223.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3.35",
                  "frac": "47/20",
                  "rootIdx": 127
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Philadelphia 76ers & Under 224.5",
                  "shortName": "Philadelphia 76ers & Under 224.5"
               },
               "wasPrice": [],
               "id": 203826274,
               "name": "Philadelphia 76ers & Under 224.5",
               "type": "BUnder",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.8",
                  "frac": "14/5",
                  "rootIdx": 136
               },
               "pos": {
                  "row": 2,
                  "col": 2
               },
               "names": {
                  "longName": "Brooklyn Nets & Over 224.5",
                  "shortName": "Brooklyn Nets & Over 224.5"
               },
               "wasPrice": [],
               "id": 203826239,
               "name": "Brooklyn Nets & Over 224.5",
               "type": "AOver",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.35",
                  "frac": "47/20",
                  "rootIdx": 127
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers & Over 224.5",
                  "shortName": "Philadelphia 76ers & Over 224.5"
               },
               "wasPrice": [],
               "id": 203826267,
               "name": "Philadelphia 76ers & Over 224.5",
               "type": "BOver",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.9",
                  "frac": "29/10",
                  "rootIdx": 138
               },
               "pos": {
                  "row": 2,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets & Under 224.5",
                  "shortName": "Brooklyn Nets & Under 224.5"
               },
               "wasPrice": [],
               "id": 203826257,
               "name": "Brooklyn Nets & Under 224.5",
               "type": "AUnder",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Matchbet and Total Points (Incl. OT)",
            "shortName": "Matchbet and Total Points (Incl. OT)"
         },
         "id": 70132093,
         "name": "Matchbet and Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:AXB$OU",
         "subtype": "M#224.5",
         "line": 224.5,
         "displayOrder": -425,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "7.5",
                  "frac": "13/2",
                  "rootIdx": 186
               },
               "names": {
                  "longName": "Shake Milton 24+",
                  "shortName": "Shake Milton 24+"
               },
               "wasPrice": [],
               "id": 204140851,
               "name": "Shake Milton 24+",
               "type": "24",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4.5",
                  "frac": "7/2",
                  "rootIdx": 151
               },
               "names": {
                  "longName": "Shake Milton 20+",
                  "shortName": "Shake Milton 20+"
               },
               "wasPrice": [],
               "id": 204140847,
               "name": "Shake Milton 20+",
               "type": "20",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.4",
                  "frac": "12/5",
                  "rootIdx": 128
               },
               "names": {
                  "longName": "Shake Milton 18+",
                  "shortName": "Shake Milton 18+"
               },
               "wasPrice": [],
               "id": 204140845,
               "name": "Shake Milton 18+",
               "type": "18",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.6",
                  "frac": "8/5",
                  "rootIdx": 110
               },
               "names": {
                  "longName": "Shake Milton 16+",
                  "shortName": "Shake Milton 16+"
               },
               "wasPrice": [],
               "id": 204140843,
               "name": "Shake Milton 16+",
               "type": "16",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.05",
                  "frac": "21/20",
                  "rootIdx": 88
               },
               "names": {
                  "longName": "Shake Milton 14+",
                  "shortName": "Shake Milton 14+"
               },
               "wasPrice": [],
               "id": 204140841,
               "name": "Shake Milton 14+",
               "type": "14",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.5",
                  "frac": "1/2",
                  "rootIdx": 47
               },
               "names": {
                  "longName": "Shake Milton 10+",
                  "shortName": "Shake Milton 10+"
               },
               "wasPrice": [],
               "id": 204140837,
               "name": "Shake Milton 10+",
               "type": "10",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "names": {
                  "longName": "Shake Milton 12+",
                  "shortName": "Shake Milton 12+"
               },
               "wasPrice": [],
               "id": 204140839,
               "name": "Shake Milton 12+",
               "type": "12",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "6",
                  "frac": "5/1",
                  "rootIdx": 172
               },
               "names": {
                  "longName": "Shake Milton 22+",
                  "shortName": "Shake Milton 22+"
               },
               "wasPrice": [],
               "id": 204140849,
               "name": "Shake Milton 22+",
               "type": "22",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "9.25",
                  "frac": "33/4",
                  "rootIdx": 193
               },
               "names": {
                  "longName": "Shake Milton 26+",
                  "shortName": "Shake Milton 26+"
               },
               "wasPrice": [],
               "id": 204140853,
               "name": "Shake Milton 26+",
               "type": "26",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Shake Milton points (incl. overtime)",
            "shortName": "Shake Milton points (incl. overtime)"
         },
         "id": 70230300,
         "name": "Shake Milton points (incl. overtime)",
         "type": "BASKETBALL:FT:PLPOI",
         "subtype": "M#pre:playerprops:24750814:1495351",
         "displayOrder": -275,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 57.5",
                  "shortName": "Under 57.5"
               },
               "wasPrice": [],
               "id": 203924543,
               "name": "Under 57.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.75",
                  "frac": "3/4",
                  "rootIdx": 63
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 57.5",
                  "shortName": "Over 57.5"
               },
               "wasPrice": [],
               "id": 203924541,
               "name": "Over 57.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Quarter Total Points",
            "shortName": "1st Quarter Total Points"
         },
         "id": 70171744,
         "name": "1st Quarter Total Points",
         "type": "BASKETBALL:P:OU",
         "subtype": "Q1#57.5",
         "line": 57.5,
         "displayOrder": -323,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q1"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.45",
                  "frac": "9/20",
                  "rootIdx": 44
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "Tobias Harris 2+",
                  "shortName": "Tobias Harris 2+"
               },
               "wasPrice": [],
               "id": 204140749,
               "name": "Tobias Harris 2+",
               "type": "2",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "46",
                  "frac": "45/1",
                  "rootIdx": 237
               },
               "pos": {
                  "row": 103,
                  "col": 1
               },
               "names": {
                  "longName": "Tobias Harris 7+",
                  "shortName": "Tobias Harris 7+"
               },
               "wasPrice": [],
               "id": 204140759,
               "name": "Tobias Harris 7+",
               "type": "7",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "22",
                  "frac": "21/1",
                  "rootIdx": 213
               },
               "pos": {
                  "row": 302,
                  "col": 1
               },
               "names": {
                  "longName": "Tobias Harris 6+",
                  "shortName": "Tobias Harris 6+"
               },
               "wasPrice": [],
               "id": 204140757,
               "name": "Tobias Harris 6+",
               "type": "6",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "10",
                  "frac": "9/1",
                  "rootIdx": 196
               },
               "pos": {
                  "row": 202,
                  "col": 1
               },
               "names": {
                  "longName": "Tobias Harris 5+",
                  "shortName": "Tobias Harris 5+"
               },
               "wasPrice": [],
               "id": 204140755,
               "name": "Tobias Harris 5+",
               "type": "5",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4.35",
                  "frac": "67/20",
                  "rootIdx": 148
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Tobias Harris 4+",
                  "shortName": "Tobias Harris 4+"
               },
               "wasPrice": [],
               "id": 204140753,
               "name": "Tobias Harris 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.3",
                  "frac": "13/10",
                  "rootIdx": 103
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Tobias Harris 3+",
                  "shortName": "Tobias Harris 3+"
               },
               "wasPrice": [],
               "id": 204140751,
               "name": "Tobias Harris 3+",
               "type": "3",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Tobias Harris 3-point field goals (incl. overtime)",
            "shortName": "Tobias Harris 3-point field goals (incl. overtime)"
         },
         "id": 70230282,
         "name": "Tobias Harris 3-point field goals (incl. overtime)",
         "type": "BASKETBALL:FT:3PFG",
         "subtype": "M#pre:playerprops:24750814:608250",
         "displayOrder": -272,
         "columnCount": 1,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "10.5",
                  "frac": "19/2",
                  "rootIdx": 197
               },
               "names": {
                  "longName": "Ben Simmons 30+",
                  "shortName": "Ben Simmons 30+"
               },
               "wasPrice": [],
               "id": 204159767,
               "name": "Ben Simmons 30+",
               "type": "30",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "7.5",
                  "frac": "13/2",
                  "rootIdx": 186
               },
               "names": {
                  "longName": "Ben Simmons 27+",
                  "shortName": "Ben Simmons 27+"
               },
               "wasPrice": [],
               "id": 204140581,
               "name": "Ben Simmons 27+",
               "type": "27",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "6.6",
                  "frac": "28/5",
                  "rootIdx": 179
               },
               "names": {
                  "longName": "Ben Simmons 26+",
                  "shortName": "Ben Simmons 26+"
               },
               "wasPrice": [],
               "id": 204159763,
               "name": "Ben Simmons 26+",
               "type": "26",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4.9",
                  "frac": "39/10",
                  "rootIdx": 159
               },
               "names": {
                  "longName": "Ben Simmons 24+",
                  "shortName": "Ben Simmons 24+"
               },
               "wasPrice": [],
               "id": 204159761,
               "name": "Ben Simmons 24+",
               "type": "24",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "5.7",
                  "frac": "47/10",
                  "rootIdx": 168
               },
               "names": {
                  "longName": "Ben Simmons 25+",
                  "shortName": "Ben Simmons 25+"
               },
               "wasPrice": [],
               "id": 204140579,
               "name": "Ben Simmons 25+",
               "type": "25",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4.2",
                  "frac": "16/5",
                  "rootIdx": 144
               },
               "names": {
                  "longName": "Ben Simmons 23+",
                  "shortName": "Ben Simmons 23+"
               },
               "wasPrice": [],
               "id": 204140577,
               "name": "Ben Simmons 23+",
               "type": "23",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.1",
                  "frac": "21/10",
                  "rootIdx": 122
               },
               "names": {
                  "longName": "Ben Simmons 21+",
                  "shortName": "Ben Simmons 21+"
               },
               "wasPrice": [],
               "id": 204140575,
               "name": "Ben Simmons 21+",
               "type": "21",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.3",
                  "frac": "13/10",
                  "rootIdx": 103
               },
               "names": {
                  "longName": "Ben Simmons 19+",
                  "shortName": "Ben Simmons 19+"
               },
               "wasPrice": [],
               "id": 204140573,
               "name": "Ben Simmons 19+",
               "type": "19",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "names": {
                  "longName": "Ben Simmons 17+",
                  "shortName": "Ben Simmons 17+"
               },
               "wasPrice": [],
               "id": 204140571,
               "name": "Ben Simmons 17+",
               "type": "17",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.65",
                  "frac": "33/20",
                  "rootIdx": 112
               },
               "names": {
                  "longName": "Ben Simmons 20+",
                  "shortName": "Ben Simmons 20+"
               },
               "wasPrice": [],
               "id": 204159757,
               "name": "Ben Simmons 20+",
               "type": "20",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "8.5",
                  "frac": "15/2",
                  "rootIdx": 190
               },
               "names": {
                  "longName": "Ben Simmons 28+",
                  "shortName": "Ben Simmons 28+"
               },
               "wasPrice": [],
               "id": 204159765,
               "name": "Ben Simmons 28+",
               "type": "28",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.6",
                  "frac": "13/5",
                  "rootIdx": 132
               },
               "names": {
                  "longName": "Ben Simmons 22+",
                  "shortName": "Ben Simmons 22+"
               },
               "wasPrice": [],
               "id": 204159759,
               "name": "Ben Simmons 22+",
               "type": "22",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "11.5",
                  "frac": "21/2",
                  "rootIdx": 199
               },
               "names": {
                  "longName": "Ben Simmons 31+",
                  "shortName": "Ben Simmons 31+"
               },
               "wasPrice": [],
               "id": 204140585,
               "name": "Ben Simmons 31+",
               "type": "31",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.1",
                  "frac": "11/10",
                  "rootIdx": 93
               },
               "names": {
                  "longName": "Ben Simmons 18+",
                  "shortName": "Ben Simmons 18+"
               },
               "wasPrice": [],
               "id": 204159755,
               "name": "Ben Simmons 18+",
               "type": "18",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "names": {
                  "longName": "Ben Simmons 16+",
                  "shortName": "Ben Simmons 16+"
               },
               "wasPrice": [],
               "id": 204159753,
               "name": "Ben Simmons 16+",
               "type": "16",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.47",
                  "frac": "40/85",
                  "rootIdx": 45
               },
               "names": {
                  "longName": "Ben Simmons 14+",
                  "shortName": "Ben Simmons 14+"
               },
               "wasPrice": [],
               "id": 204159751,
               "name": "Ben Simmons 14+",
               "type": "14",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.53",
                  "frac": "8/15",
                  "rootIdx": 50
               },
               "names": {
                  "longName": "Ben Simmons 15+",
                  "shortName": "Ben Simmons 15+"
               },
               "wasPrice": [],
               "id": 204140569,
               "name": "Ben Simmons 15+",
               "type": "15",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "9.25",
                  "frac": "33/4",
                  "rootIdx": 193
               },
               "names": {
                  "longName": "Ben Simmons 29+",
                  "shortName": "Ben Simmons 29+"
               },
               "wasPrice": [],
               "id": 204140583,
               "name": "Ben Simmons 29+",
               "type": "29",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Ben Simmons points (incl. overtime)",
            "shortName": "Ben Simmons points (incl. overtime)"
         },
         "id": 70230240,
         "name": "Ben Simmons points (incl. overtime)",
         "type": "BASKETBALL:FT:PLPOI",
         "subtype": "M#pre:playerprops:24750814:996289",
         "displayOrder": -275,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "11.5",
                  "frac": "21/2",
                  "rootIdx": 199
               },
               "pos": {
                  "row": 203,
                  "col": 1
               },
               "names": {
                  "longName": "Timothe Luwawu-Cabarrot 8+",
                  "shortName": "Timothe Luwawu-Cabarrot 8+"
               },
               "wasPrice": [],
               "id": 204140715,
               "name": "Timothe Luwawu-Cabarrot 8+",
               "type": "8",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "6.5",
                  "frac": "11/2",
                  "rootIdx": 178
               },
               "pos": {
                  "row": 103,
                  "col": 1
               },
               "names": {
                  "longName": "Timothe Luwawu-Cabarrot 7+",
                  "shortName": "Timothe Luwawu-Cabarrot 7+"
               },
               "wasPrice": [],
               "id": 204140713,
               "name": "Timothe Luwawu-Cabarrot 7+",
               "type": "7",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.85",
                  "frac": "57/20",
                  "rootIdx": 137
               },
               "pos": {
                  "row": 302,
                  "col": 1
               },
               "names": {
                  "longName": "Timothe Luwawu-Cabarrot 6+",
                  "shortName": "Timothe Luwawu-Cabarrot 6+"
               },
               "wasPrice": [],
               "id": 204140711,
               "name": "Timothe Luwawu-Cabarrot 6+",
               "type": "6",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2",
                  "frac": "1/1",
                  "rootIdx": 83
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Timothe Luwawu-Cabarrot 4+",
                  "shortName": "Timothe Luwawu-Cabarrot 4+"
               },
               "wasPrice": [],
               "id": 204140707,
               "name": "Timothe Luwawu-Cabarrot 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.45",
                  "frac": "9/20",
                  "rootIdx": 44
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Timothe Luwawu-Cabarrot 3+",
                  "shortName": "Timothe Luwawu-Cabarrot 3+"
               },
               "wasPrice": [],
               "id": 204140705,
               "name": "Timothe Luwawu-Cabarrot 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.55",
                  "frac": "31/20",
                  "rootIdx": 109
               },
               "pos": {
                  "row": 202,
                  "col": 1
               },
               "names": {
                  "longName": "Timothe Luwawu-Cabarrot 5+",
                  "shortName": "Timothe Luwawu-Cabarrot 5+"
               },
               "wasPrice": [],
               "id": 204140709,
               "name": "Timothe Luwawu-Cabarrot 5+",
               "type": "5",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "28",
                  "frac": "27/1",
                  "rootIdx": 219
               },
               "names": {
                  "longName": "Timothe Luwawu-Cabarrot 9+",
                  "shortName": "Timothe Luwawu-Cabarrot 9+"
               },
               "wasPrice": [],
               "id": 204140717,
               "name": "Timothe Luwawu-Cabarrot 9+",
               "type": "9",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Timothe Luwawu-Cabarrot rebounds (incl. overtime)",
            "shortName": "Timothe Luwawu-Cabarrot rebounds (incl. overtime)"
         },
         "id": 70230272,
         "name": "Timothe Luwawu-Cabarrot rebounds (incl. overtime)",
         "type": "BASKETBALL:FT:PLREB",
         "subtype": "M#pre:playerprops:24750814:852058",
         "displayOrder": -274,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "5.75",
                  "frac": "19/4",
                  "rootIdx": 169
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers -14.5",
                  "shortName": "Philadelphia 76ers -14.5"
               },
               "wasPrice": [],
               "id": 203825878,
               "name": "Philadelphia 76ers -14.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.11",
                  "frac": "1/9",
                  "rootIdx": 18
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets +14.5",
                  "shortName": "Brooklyn Nets +14.5"
               },
               "wasPrice": [],
               "id": 203825872,
               "name": "Brooklyn Nets +14.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131908,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#14.5",
         "line": 14.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3.5",
                  "frac": "5/2",
                  "rootIdx": 130
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers -8.5",
                  "shortName": "Philadelphia 76ers -8.5"
               },
               "wasPrice": [],
               "id": 203826019,
               "name": "Philadelphia 76ers -8.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.28",
                  "frac": "2/7",
                  "rootIdx": 34
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets +8.5",
                  "shortName": "Brooklyn Nets +8.5"
               },
               "wasPrice": [],
               "id": 203826004,
               "name": "Brooklyn Nets +8.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131982,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#8.5",
         "line": 8.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.72",
                  "frac": "8/11",
                  "rootIdx": 60
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 28.5",
                  "shortName": "Over 28.5"
               },
               "wasPrice": [],
               "id": 203826005,
               "name": "Over 28.5",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2",
                  "frac": "1/1",
                  "rootIdx": 83
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 28.5",
                  "shortName": "Under 28.5"
               },
               "wasPrice": [],
               "id": 203826018,
               "name": "Under 28.5",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Quarter Brooklyn Nets Total Points",
            "shortName": "1st Quarter Brooklyn Nets Total Points"
         },
         "id": 70131980,
         "name": "1st Quarter Brooklyn Nets Total Points",
         "type": "BASKETBALL:P:A:OU",
         "subtype": "Q1#28.5",
         "line": 28.5,
         "displayOrder": -318,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.05",
                  "frac": "21/20",
                  "rootIdx": 88
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 224.5",
                  "shortName": "Under 224.5"
               },
               "wasPrice": [],
               "id": 203826110,
               "name": "Under 224.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.72",
                  "frac": "8/11",
                  "rootIdx": 60
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 224.5",
                  "shortName": "Over 224.5"
               },
               "wasPrice": [],
               "id": 203826106,
               "name": "Over 224.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132028,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#224.5",
         "line": 224.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.36",
                  "frac": "4/11",
                  "rootIdx": 39
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers +6.5",
                  "shortName": "Philadelphia 76ers +6.5"
               },
               "wasPrice": [],
               "id": 203825866,
               "name": "Philadelphia 76ers +6.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.9",
                  "frac": "19/10",
                  "rootIdx": 118
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets -6.5",
                  "shortName": "Brooklyn Nets -6.5"
               },
               "wasPrice": [],
               "id": 203825861,
               "name": "Brooklyn Nets -6.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131904,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-6.5",
         "line": -6.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "8",
                  "frac": "7/1",
                  "rootIdx": 188
               },
               "names": {
                  "longName": "Timothe Luwawu-Cabarrot 25+",
                  "shortName": "Timothe Luwawu-Cabarrot 25+"
               },
               "wasPrice": [],
               "id": 204141211,
               "name": "Timothe Luwawu-Cabarrot 25+",
               "type": "25",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4.65",
                  "frac": "73/20",
                  "rootIdx": 154
               },
               "names": {
                  "longName": "Timothe Luwawu-Cabarrot 21+",
                  "shortName": "Timothe Luwawu-Cabarrot 21+"
               },
               "wasPrice": [],
               "id": 204141207,
               "name": "Timothe Luwawu-Cabarrot 21+",
               "type": "21",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.5",
                  "frac": "5/2",
                  "rootIdx": 130
               },
               "names": {
                  "longName": "Timothe Luwawu-Cabarrot 19+",
                  "shortName": "Timothe Luwawu-Cabarrot 19+"
               },
               "wasPrice": [],
               "id": 204141205,
               "name": "Timothe Luwawu-Cabarrot 19+",
               "type": "19",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.75",
                  "frac": "7/4",
                  "rootIdx": 114
               },
               "names": {
                  "longName": "Timothe Luwawu-Cabarrot 17+",
                  "shortName": "Timothe Luwawu-Cabarrot 17+"
               },
               "wasPrice": [],
               "id": 204141203,
               "name": "Timothe Luwawu-Cabarrot 17+",
               "type": "17",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.45",
                  "frac": "29/20",
                  "rootIdx": 107
               },
               "names": {
                  "longName": "Timothe Luwawu-Cabarrot 15+",
                  "shortName": "Timothe Luwawu-Cabarrot 15+"
               },
               "wasPrice": [],
               "id": 204141201,
               "name": "Timothe Luwawu-Cabarrot 15+",
               "type": "15",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.15",
                  "frac": "23/20",
                  "rootIdx": 96
               },
               "names": {
                  "longName": "Timothe Luwawu-Cabarrot 13+",
                  "shortName": "Timothe Luwawu-Cabarrot 13+"
               },
               "wasPrice": [],
               "id": 204141199,
               "name": "Timothe Luwawu-Cabarrot 13+",
               "type": "13",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "names": {
                  "longName": "Timothe Luwawu-Cabarrot 11+",
                  "shortName": "Timothe Luwawu-Cabarrot 11+"
               },
               "wasPrice": [],
               "id": 204141197,
               "name": "Timothe Luwawu-Cabarrot 11+",
               "type": "11",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "6.1",
                  "frac": "51/10",
                  "rootIdx": 173
               },
               "names": {
                  "longName": "Timothe Luwawu-Cabarrot 23+",
                  "shortName": "Timothe Luwawu-Cabarrot 23+"
               },
               "wasPrice": [],
               "id": 204141209,
               "name": "Timothe Luwawu-Cabarrot 23+",
               "type": "23",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.33",
                  "frac": "1/3",
                  "rootIdx": 37
               },
               "names": {
                  "longName": "Timothe Luwawu-Cabarrot 9+",
                  "shortName": "Timothe Luwawu-Cabarrot 9+"
               },
               "wasPrice": [],
               "id": 204141213,
               "name": "Timothe Luwawu-Cabarrot 9+",
               "type": "9",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Timothe Luwawu-Cabarrot points (incl. overtime)",
            "shortName": "Timothe Luwawu-Cabarrot points (incl. overtime)"
         },
         "id": 70230374,
         "name": "Timothe Luwawu-Cabarrot points (incl. overtime)",
         "type": "BASKETBALL:FT:PLPOI",
         "subtype": "M#pre:playerprops:24750814:852058",
         "displayOrder": -275,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "18",
                  "frac": "17/1",
                  "rootIdx": 209
               },
               "pos": {
                  "row": 3,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers/Draw",
                  "shortName": "Philadelphia 76ers/Draw"
               },
               "wasPrice": [],
               "id": 203825787,
               "name": "Philadelphia 76ers/Draw",
               "type": "B/X",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.55",
                  "frac": "31/20",
                  "rootIdx": 109
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets/Brooklyn Nets",
                  "shortName": "Brooklyn Nets/Brooklyn Nets"
               },
               "wasPrice": [],
               "id": 203825729,
               "name": "Brooklyn Nets/Brooklyn Nets",
               "type": "A/A",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "23",
                  "frac": "22/1",
                  "rootIdx": 214
               },
               "pos": {
                  "row": 2,
                  "col": 3
               },
               "names": {
                  "longName": "Tie/Philadelphia 76ers",
                  "shortName": "Tie/Philadelphia 76ers"
               },
               "wasPrice": [],
               "id": 203825776,
               "name": "Tie/Philadelphia 76ers",
               "type": "X/B",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "7.5",
                  "frac": "13/2",
                  "rootIdx": 186
               },
               "pos": {
                  "row": 3,
                  "col": 1
               },
               "names": {
                  "longName": "Philadelphia 76ers/Brooklyn Nets",
                  "shortName": "Philadelphia 76ers/Brooklyn Nets"
               },
               "wasPrice": [],
               "id": 203825782,
               "name": "Philadelphia 76ers/Brooklyn Nets",
               "type": "B/A",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "18",
                  "frac": "17/1",
                  "rootIdx": 209
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Brooklyn Nets/Draw",
                  "shortName": "Brooklyn Nets/Draw"
               },
               "wasPrice": [],
               "id": 203825747,
               "name": "Brooklyn Nets/Draw",
               "type": "A/X",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "71",
                  "frac": "70/1",
                  "rootIdx": 246
               },
               "pos": {
                  "row": 2,
                  "col": 2
               },
               "names": {
                  "longName": "Tie/Tie",
                  "shortName": "Tie/Tie"
               },
               "wasPrice": [],
               "id": 203825773,
               "name": "Tie/Tie",
               "type": "X/X",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "7.25",
                  "frac": "25/4",
                  "rootIdx": 185
               },
               "pos": {
                  "row": 1,
                  "col": 3
               },
               "names": {
                  "longName": "Brooklyn Nets/Philadelphia 76ers",
                  "shortName": "Brooklyn Nets/Philadelphia 76ers"
               },
               "wasPrice": [],
               "id": 203825760,
               "name": "Brooklyn Nets/Philadelphia 76ers",
               "type": "A/B",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "24",
                  "frac": "23/1",
                  "rootIdx": 215
               },
               "pos": {
                  "row": 2,
                  "col": 1
               },
               "names": {
                  "longName": "Tie/Brooklyn Nets",
                  "shortName": "Tie/Brooklyn Nets"
               },
               "wasPrice": [],
               "id": 203825769,
               "name": "Tie/Brooklyn Nets",
               "type": "X/A",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.5",
                  "frac": "3/2",
                  "rootIdx": 108
               },
               "pos": {
                  "row": 3,
                  "col": 3
               },
               "names": {
                  "longName": "Philadelphia 76ers/Philadelphia 76ers",
                  "shortName": "Philadelphia 76ers/Philadelphia 76ers"
               },
               "wasPrice": [],
               "id": 203825794,
               "name": "Philadelphia 76ers/Philadelphia 76ers",
               "type": "B/B",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Halftime/Fulltime",
            "shortName": "Halftime/Fulltime"
         },
         "id": 70131864,
         "name": "Halftime/Fulltime",
         "type": "BASKETBALL:FT:HTFT",
         "subtype": "M",
         "displayOrder": -420,
         "columnCount": 3,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "5.3",
                  "frac": "43/10",
                  "rootIdx": 164
               },
               "names": {
                  "longName": "Caris LeVert 32+",
                  "shortName": "Caris LeVert 32+"
               },
               "wasPrice": [],
               "id": 204141295,
               "name": "Caris LeVert 32+",
               "type": "32",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4.33",
                  "frac": "10/3",
                  "rootIdx": 147
               },
               "names": {
                  "longName": "Caris LeVert 30+",
                  "shortName": "Caris LeVert 30+"
               },
               "wasPrice": [],
               "id": 204141293,
               "name": "Caris LeVert 30+",
               "type": "30",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.5",
                  "frac": "5/2",
                  "rootIdx": 130
               },
               "names": {
                  "longName": "Caris LeVert 28+",
                  "shortName": "Caris LeVert 28+"
               },
               "wasPrice": [],
               "id": 204141291,
               "name": "Caris LeVert 28+",
               "type": "28",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.65",
                  "frac": "33/20",
                  "rootIdx": 112
               },
               "names": {
                  "longName": "Caris LeVert 24+",
                  "shortName": "Caris LeVert 24+"
               },
               "wasPrice": [],
               "id": 204141287,
               "name": "Caris LeVert 24+",
               "type": "24",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.38",
                  "frac": "11/8",
                  "rootIdx": 105
               },
               "names": {
                  "longName": "Caris LeVert 22+",
                  "shortName": "Caris LeVert 22+"
               },
               "wasPrice": [],
               "id": 204141285,
               "name": "Caris LeVert 22+",
               "type": "22",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.1",
                  "frac": "11/10",
                  "rootIdx": 93
               },
               "names": {
                  "longName": "Caris LeVert 20+",
                  "shortName": "Caris LeVert 20+"
               },
               "wasPrice": [],
               "id": 204141283,
               "name": "Caris LeVert 20+",
               "type": "20",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.42",
                  "frac": "21/50",
                  "rootIdx": 42
               },
               "names": {
                  "longName": "Caris LeVert 16+",
                  "shortName": "Caris LeVert 16+"
               },
               "wasPrice": [],
               "id": 204141279,
               "name": "Caris LeVert 16+",
               "type": "16",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3",
                  "frac": "2/1",
                  "rootIdx": 120
               },
               "names": {
                  "longName": "Caris LeVert 26+",
                  "shortName": "Caris LeVert 26+"
               },
               "wasPrice": [],
               "id": 204141289,
               "name": "Caris LeVert 26+",
               "type": "26",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.87",
                  "frac": "87/100",
                  "rootIdx": 71
               },
               "names": {
                  "longName": "Caris LeVert 18+",
                  "shortName": "Caris LeVert 18+"
               },
               "wasPrice": [],
               "id": 204141281,
               "name": "Caris LeVert 18+",
               "type": "18",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Caris LeVert points (incl. overtime)",
            "shortName": "Caris LeVert points (incl. overtime)"
         },
         "id": 70230392,
         "name": "Caris LeVert points (incl. overtime)",
         "type": "BASKETBALL:FT:PLPOI",
         "subtype": "M#pre:playerprops:24750814:996291",
         "displayOrder": -275,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3.75",
                  "frac": "11/4",
                  "rootIdx": 135
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 212.5",
                  "shortName": "Under 212.5"
               },
               "wasPrice": [],
               "id": 203826051,
               "name": "Under 212.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.25",
                  "frac": "1/4",
                  "rootIdx": 32
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 212.5",
                  "shortName": "Over 212.5"
               },
               "wasPrice": [],
               "id": 203826044,
               "name": "Over 212.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70131998,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#212.5",
         "line": 212.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 58.5",
                  "shortName": "Over 58.5"
               },
               "wasPrice": [],
               "id": 204512909,
               "name": "Over 58.5",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.75",
                  "frac": "3/4",
                  "rootIdx": 63
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 58.5",
                  "shortName": "Under 58.5"
               },
               "wasPrice": [],
               "id": 204512907,
               "name": "Under 58.5",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "2nd Quarter Total Points",
            "shortName": "2nd Quarter Total Points"
         },
         "id": 70307672,
         "name": "2nd Quarter Total Points",
         "type": "BASKETBALL:P:OU",
         "subtype": "Q2#58.5",
         "line": 58.5,
         "displayOrder": -313,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q2"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 57.5",
                  "shortName": "Under 57.5"
               },
               "wasPrice": [],
               "id": 203927331,
               "name": "Under 57.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.75",
                  "frac": "3/4",
                  "rootIdx": 63
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 57.5",
                  "shortName": "Over 57.5"
               },
               "wasPrice": [],
               "id": 203927329,
               "name": "Over 57.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Half Brooklyn Nets Total Points",
            "shortName": "1st Half Brooklyn Nets Total Points"
         },
         "id": 70173074,
         "name": "1st Half Brooklyn Nets Total Points",
         "type": "BASKETBALL:P:A:OU",
         "subtype": "H1#57.5",
         "line": 57.5,
         "displayOrder": -355,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "H1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3",
                  "frac": "2/1",
                  "rootIdx": 120
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "6 to 10",
                  "shortName": "6 to 10"
               },
               "wasPrice": [],
               "id": 203825849,
               "name": "6 to 10",
               "type": "6 to 10",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "19",
                  "frac": "18/1",
                  "rootIdx": 210
               },
               "pos": {
                  "row": 2,
                  "col": 3
               },
               "names": {
                  "longName": "26 to 30",
                  "shortName": "26 to 30"
               },
               "wasPrice": [],
               "id": 203825821,
               "name": "26 to 30",
               "type": "26 to 30",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "11",
                  "frac": "10/1",
                  "rootIdx": 198
               },
               "pos": {
                  "row": 2,
                  "col": 2
               },
               "names": {
                  "longName": "21 to 25",
                  "shortName": "21 to 25"
               },
               "wasPrice": [],
               "id": 203825798,
               "name": "21 to 25",
               "type": "21 to 25",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "4.85",
                  "frac": "77/20",
                  "rootIdx": 158
               },
               "pos": {
                  "row": 1,
                  "col": 3
               },
               "names": {
                  "longName": "11 to 15",
                  "shortName": "11 to 15"
               },
               "wasPrice": [],
               "id": 203825768,
               "name": "11 to 15",
               "type": "11 to 15",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.9",
                  "frac": "19/10",
                  "rootIdx": 118
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "1 to 5",
                  "shortName": "1 to 5"
               },
               "wasPrice": [],
               "id": 203825786,
               "name": "1 to 5",
               "type": "1 to 5",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "28",
                  "frac": "27/1",
                  "rootIdx": 219
               },
               "pos": {
                  "row": 3,
                  "col": 1
               },
               "names": {
                  "longName": "31 plus",
                  "shortName": "31 plus"
               },
               "wasPrice": [],
               "id": 203825834,
               "name": "31 plus",
               "type": "31 plus",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "6.8",
                  "frac": "29/5",
                  "rootIdx": 182
               },
               "pos": {
                  "row": 2,
                  "col": 1
               },
               "names": {
                  "longName": "16 to 20",
                  "shortName": "16 to 20"
               },
               "wasPrice": [],
               "id": 203825777,
               "name": "16 to 20",
               "type": "16 to 20",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Any team winning margin (incl. OT)",
            "shortName": "Any team winning margin (incl. OT)"
         },
         "id": 70131866,
         "name": "Any team winning margin (incl. OT)",
         "type": "BASKETBALL:FTOT:ATWM",
         "subtype": "M#sr:winning_margin_no_draw_any_team:31+",
         "displayOrder": -399,
         "columnCount": 3,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.75",
                  "frac": "3/4",
                  "rootIdx": 63
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets +0.5",
                  "shortName": "Brooklyn Nets +0.5"
               },
               "wasPrice": [],
               "id": 203825969,
               "name": "Brooklyn Nets +0.5",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers -0.5",
                  "shortName": "Philadelphia 76ers -0.5"
               },
               "wasPrice": [],
               "id": 203825975,
               "name": "Philadelphia 76ers -0.5",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Quarter Spread",
            "shortName": "1st Quarter Spread"
         },
         "id": 70131969,
         "name": "1st Quarter Spread",
         "type": "BASKETBALL:P:PSPRD",
         "subtype": "Q1#0.5",
         "line": 0.5,
         "displayOrder": -324,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 113.5",
                  "shortName": "Over 113.5"
               },
               "wasPrice": [],
               "id": 203826097,
               "name": "Over 113.5",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 113.5",
                  "shortName": "Under 113.5"
               },
               "wasPrice": [],
               "id": 203826104,
               "name": "Under 113.5",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Philadelphia 76ers Total Points (Incl. OT)",
            "shortName": "Philadelphia 76ers Total Points (Incl. OT)"
         },
         "id": 70132021,
         "name": "Philadelphia 76ers Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:B:OU",
         "subtype": "M#113.5",
         "line": 113.5,
         "displayOrder": -440,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "8.5",
                  "frac": "15/2",
                  "rootIdx": 190
               },
               "pos": {
                  "row": 103,
                  "col": 1
               },
               "names": {
                  "longName": "Shake Milton 7+",
                  "shortName": "Shake Milton 7+"
               },
               "wasPrice": [],
               "id": 204141109,
               "name": "Shake Milton 7+",
               "type": "7",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4.85",
                  "frac": "77/20",
                  "rootIdx": 158
               },
               "pos": {
                  "row": 302,
                  "col": 1
               },
               "names": {
                  "longName": "Shake Milton 6+",
                  "shortName": "Shake Milton 6+"
               },
               "wasPrice": [],
               "id": 204141107,
               "name": "Shake Milton 6+",
               "type": "6",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.95",
                  "frac": "39/20",
                  "rootIdx": 119
               },
               "pos": {
                  "row": 202,
                  "col": 1
               },
               "names": {
                  "longName": "Shake Milton 5+",
                  "shortName": "Shake Milton 5+"
               },
               "wasPrice": [],
               "id": 204141105,
               "name": "Shake Milton 5+",
               "type": "5",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.75",
                  "frac": "3/4",
                  "rootIdx": 63
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Shake Milton 4+",
                  "shortName": "Shake Milton 4+"
               },
               "wasPrice": [],
               "id": 204141103,
               "name": "Shake Milton 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.65",
                  "frac": "13/20",
                  "rootIdx": 56
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Shake Milton 3+",
                  "shortName": "Shake Milton 3+"
               },
               "wasPrice": [],
               "id": 204141101,
               "name": "Shake Milton 3+",
               "type": "3",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Shake Milton assists (incl. overtime)",
            "shortName": "Shake Milton assists (incl. overtime)"
         },
         "id": 70230356,
         "name": "Shake Milton assists (incl. overtime)",
         "type": "BASKETBALL:FT:PLASS",
         "subtype": "M#pre:playerprops:24750814:1495351",
         "displayOrder": -273,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "20",
                  "frac": "19/1",
                  "rootIdx": 211
               },
               "pos": {
                  "row": 202,
                  "col": 1
               },
               "names": {
                  "longName": "Timothe Luwawu-Cabarrot 5+",
                  "shortName": "Timothe Luwawu-Cabarrot 5+"
               },
               "wasPrice": [],
               "id": 204140665,
               "name": "Timothe Luwawu-Cabarrot 5+",
               "type": "5",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "12",
                  "frac": "11/1",
                  "rootIdx": 200
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Timothe Luwawu-Cabarrot 4+",
                  "shortName": "Timothe Luwawu-Cabarrot 4+"
               },
               "wasPrice": [],
               "id": 204140663,
               "name": "Timothe Luwawu-Cabarrot 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "5.9",
                  "frac": "49/10",
                  "rootIdx": 171
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Timothe Luwawu-Cabarrot 3+",
                  "shortName": "Timothe Luwawu-Cabarrot 3+"
               },
               "wasPrice": [],
               "id": 204140661,
               "name": "Timothe Luwawu-Cabarrot 3+",
               "type": "3",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Timothe Luwawu-Cabarrot assists (incl. overtime)",
            "shortName": "Timothe Luwawu-Cabarrot assists (incl. overtime)"
         },
         "id": 70230258,
         "name": "Timothe Luwawu-Cabarrot assists (incl. overtime)",
         "type": "BASKETBALL:FT:PLASS",
         "subtype": "M#pre:playerprops:24750814:852058",
         "displayOrder": -273,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "20",
                  "frac": "19/1",
                  "rootIdx": 211
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "DeAndre Jordan 4+",
                  "shortName": "DeAndre Jordan 4+"
               },
               "wasPrice": [],
               "id": 204140471,
               "name": "DeAndre Jordan 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "6.2",
                  "frac": "26/5",
                  "rootIdx": 174
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "DeAndre Jordan 3+",
                  "shortName": "DeAndre Jordan 3+"
               },
               "wasPrice": [],
               "id": 204140469,
               "name": "DeAndre Jordan 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.5",
                  "frac": "5/2",
                  "rootIdx": 130
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "DeAndre Jordan 2+",
                  "shortName": "DeAndre Jordan 2+"
               },
               "wasPrice": [],
               "id": 204140467,
               "name": "DeAndre Jordan 2+",
               "type": "2",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 101,
                  "col": 1
               },
               "names": {
                  "longName": "DeAndre Jordan 1+",
                  "shortName": "DeAndre Jordan 1+"
               },
               "wasPrice": [],
               "id": 204140465,
               "name": "DeAndre Jordan 1+",
               "type": "1",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "DeAndre Jordan steals (incl. overtime)",
            "shortName": "DeAndre Jordan steals (incl. overtime)"
         },
         "id": 70230216,
         "name": "DeAndre Jordan steals (incl. overtime)",
         "type": "BASKETBALL:FT:PLSTA",
         "subtype": "M#pre:playerprops:24750814:607508",
         "displayOrder": -270,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "19",
                  "frac": "18/1",
                  "rootIdx": 210
               },
               "pos": {
                  "row": 103,
                  "col": 1
               },
               "names": {
                  "longName": "Kyrie Irving 7+",
                  "shortName": "Kyrie Irving 7+"
               },
               "wasPrice": [],
               "id": 204140805,
               "name": "Kyrie Irving 7+",
               "type": "7",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "7.75",
                  "frac": "27/4",
                  "rootIdx": 187
               },
               "pos": {
                  "row": 302,
                  "col": 1
               },
               "names": {
                  "longName": "Kyrie Irving 6+",
                  "shortName": "Kyrie Irving 6+"
               },
               "wasPrice": [],
               "id": 204140803,
               "name": "Kyrie Irving 6+",
               "type": "6",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.75",
                  "frac": "11/4",
                  "rootIdx": 135
               },
               "pos": {
                  "row": 202,
                  "col": 1
               },
               "names": {
                  "longName": "Kyrie Irving 5+",
                  "shortName": "Kyrie Irving 5+"
               },
               "wasPrice": [],
               "id": 204140801,
               "name": "Kyrie Irving 5+",
               "type": "5",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.1",
                  "frac": "11/10",
                  "rootIdx": 93
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Kyrie Irving 4+",
                  "shortName": "Kyrie Irving 4+"
               },
               "wasPrice": [],
               "id": 204140799,
               "name": "Kyrie Irving 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.5",
                  "frac": "1/2",
                  "rootIdx": 47
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Kyrie Irving 3+",
                  "shortName": "Kyrie Irving 3+"
               },
               "wasPrice": [],
               "id": 204140797,
               "name": "Kyrie Irving 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "32",
                  "frac": "31/1",
                  "rootIdx": 223
               },
               "pos": {
                  "row": 203,
                  "col": 1
               },
               "names": {
                  "longName": "Kyrie Irving 8+",
                  "shortName": "Kyrie Irving 8+"
               },
               "wasPrice": [],
               "id": 204140807,
               "name": "Kyrie Irving 8+",
               "type": "8",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Kyrie Irving 3-point field goals (incl. overtime)",
            "shortName": "Kyrie Irving 3-point field goals (incl. overtime)"
         },
         "id": 70230294,
         "name": "Kyrie Irving 3-point field goals (incl. overtime)",
         "type": "BASKETBALL:FT:3PFG",
         "subtype": "M#pre:playerprops:24750814:608268",
         "displayOrder": -272,
         "columnCount": 1,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.25",
                  "frac": "1/4",
                  "rootIdx": 32
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 213.5",
                  "shortName": "Over 213.5"
               },
               "wasPrice": [],
               "id": 203826136,
               "name": "Over 213.5",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.75",
                  "frac": "11/4",
                  "rootIdx": 135
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 213.5",
                  "shortName": "Under 213.5"
               },
               "wasPrice": [],
               "id": 203826149,
               "name": "Under 213.5",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132036,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#213.5",
         "line": 213.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.35",
                  "frac": "7/20",
                  "rootIdx": 38
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 236.5",
                  "shortName": "Under 236.5"
               },
               "wasPrice": [],
               "id": 203826325,
               "name": "Under 236.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.05",
                  "frac": "41/20",
                  "rootIdx": 121
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 236.5",
                  "shortName": "Over 236.5"
               },
               "wasPrice": [],
               "id": 203826322,
               "name": "Over 236.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132121,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#236.5",
         "line": 236.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "32",
                  "frac": "31/1",
                  "rootIdx": 223
               },
               "pos": {
                  "row": 203,
                  "col": 1
               },
               "names": {
                  "longName": "Seth Curry 8+",
                  "shortName": "Seth Curry 8+"
               },
               "wasPrice": [],
               "id": 204140743,
               "name": "Seth Curry 8+",
               "type": "8",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "7.25",
                  "frac": "25/4",
                  "rootIdx": 185
               },
               "pos": {
                  "row": 302,
                  "col": 1
               },
               "names": {
                  "longName": "Seth Curry 6+",
                  "shortName": "Seth Curry 6+"
               },
               "wasPrice": [],
               "id": 204140739,
               "name": "Seth Curry 6+",
               "type": "6",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4.3",
                  "frac": "33/10",
                  "rootIdx": 146
               },
               "pos": {
                  "row": 202,
                  "col": 1
               },
               "names": {
                  "longName": "Seth Curry 5+",
                  "shortName": "Seth Curry 5+"
               },
               "wasPrice": [],
               "id": 204140737,
               "name": "Seth Curry 5+",
               "type": "5",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.75",
                  "frac": "7/4",
                  "rootIdx": 114
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Seth Curry 4+",
                  "shortName": "Seth Curry 4+"
               },
               "wasPrice": [],
               "id": 204140735,
               "name": "Seth Curry 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.15",
                  "frac": "23/20",
                  "rootIdx": 96
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Seth Curry 3+",
                  "shortName": "Seth Curry 3+"
               },
               "wasPrice": [],
               "id": 204140733,
               "name": "Seth Curry 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "13",
                  "frac": "12/1",
                  "rootIdx": 202
               },
               "pos": {
                  "row": 103,
                  "col": 1
               },
               "names": {
                  "longName": "Seth Curry 7+",
                  "shortName": "Seth Curry 7+"
               },
               "wasPrice": [],
               "id": 204140741,
               "name": "Seth Curry 7+",
               "type": "7",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Seth Curry rebounds (incl. overtime)",
            "shortName": "Seth Curry rebounds (incl. overtime)"
         },
         "id": 70230278,
         "name": "Seth Curry rebounds (incl. overtime)",
         "type": "BASKETBALL:FT:PLREB",
         "subtype": "M#pre:playerprops:24750814:607326",
         "displayOrder": -274,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "9.5",
                  "frac": "17/2",
                  "rootIdx": 194
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Yes",
                  "shortName": "Yes"
               },
               "wasPrice": [],
               "id": 203826237,
               "name": "Yes",
               "type": "Yes",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.04",
                  "frac": "1/25",
                  "rootIdx": 10
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "No",
                  "shortName": "No"
               },
               "wasPrice": [],
               "id": 203826229,
               "name": "No",
               "type": "No",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Overtime Yes/No",
            "shortName": "Overtime Yes/No"
         },
         "id": 70132083,
         "name": "Overtime Yes/No",
         "type": "BASKETBALL:FT:OTYN",
         "subtype": "M",
         "displayOrder": -395,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3.35",
                  "frac": "47/20",
                  "rootIdx": 127
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Philadelphia 76ers & Under 227.5",
                  "shortName": "Philadelphia 76ers & Under 227.5"
               },
               "wasPrice": [],
               "id": 204405033,
               "name": "Philadelphia 76ers & Under 227.5",
               "type": "BUnder",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.5",
                  "frac": "5/2",
                  "rootIdx": 130
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers & Over 227.5",
                  "shortName": "Philadelphia 76ers & Over 227.5"
               },
               "wasPrice": [],
               "id": 204405031,
               "name": "Philadelphia 76ers & Over 227.5",
               "type": "BOver",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.8",
                  "frac": "14/5",
                  "rootIdx": 136
               },
               "pos": {
                  "row": 2,
                  "col": 2
               },
               "names": {
                  "longName": "Brooklyn Nets & Over 227.5",
                  "shortName": "Brooklyn Nets & Over 227.5"
               },
               "wasPrice": [],
               "id": 204405027,
               "name": "Brooklyn Nets & Over 227.5",
               "type": "AOver",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.65",
                  "frac": "53/20",
                  "rootIdx": 133
               },
               "pos": {
                  "row": 2,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets & Under 227.5",
                  "shortName": "Brooklyn Nets & Under 227.5"
               },
               "wasPrice": [],
               "id": 204405029,
               "name": "Brooklyn Nets & Under 227.5",
               "type": "AUnder",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Matchbet and Total Points (Incl. OT)",
            "shortName": "Matchbet and Total Points (Incl. OT)"
         },
         "id": 70287568,
         "name": "Matchbet and Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:AXB$OU",
         "subtype": "M#227.5",
         "line": 227.5,
         "displayOrder": -425,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.72",
                  "frac": "8/11",
                  "rootIdx": 60
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 29.5",
                  "shortName": "Under 29.5"
               },
               "wasPrice": [],
               "id": 203826009,
               "name": "Under 29.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2",
                  "frac": "1/1",
                  "rootIdx": 83
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 29.5",
                  "shortName": "Over 29.5"
               },
               "wasPrice": [],
               "id": 203825989,
               "name": "Over 29.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Quarter Philadelphia 76ers Total Points",
            "shortName": "1st Quarter Philadelphia 76ers Total Points"
         },
         "id": 70131970,
         "name": "1st Quarter Philadelphia 76ers Total Points",
         "type": "BASKETBALL:P:B:OU",
         "subtype": "Q1#29.5",
         "line": 29.5,
         "displayOrder": -317,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.72",
                  "frac": "8/11",
                  "rootIdx": 60
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 220.5",
                  "shortName": "Over 220.5"
               },
               "wasPrice": [],
               "id": 203844455,
               "name": "Over 220.5",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.05",
                  "frac": "21/20",
                  "rootIdx": 88
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 220.5",
                  "shortName": "Under 220.5"
               },
               "wasPrice": [],
               "id": 203844453,
               "name": "Under 220.5",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Total Points (Excl. OT)",
            "shortName": "Total Points (Excl. OT)"
         },
         "id": 70139240,
         "name": "Total Points (Excl. OT)",
         "type": "BASKETBALL:FT:OU",
         "subtype": "M#220.5",
         "line": 220.5,
         "displayOrder": -450,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.83",
                  "frac": "5/6",
                  "rootIdx": 69
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Shake Milton 3+",
                  "shortName": "Shake Milton 3+"
               },
               "wasPrice": [],
               "id": 204140549,
               "name": "Shake Milton 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "33",
                  "frac": "32/1",
                  "rootIdx": 224
               },
               "pos": {
                  "row": 203,
                  "col": 1
               },
               "names": {
                  "longName": "Shake Milton 8+",
                  "shortName": "Shake Milton 8+"
               },
               "wasPrice": [],
               "id": 204140559,
               "name": "Shake Milton 8+",
               "type": "8",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "13",
                  "frac": "12/1",
                  "rootIdx": 202
               },
               "pos": {
                  "row": 103,
                  "col": 1
               },
               "names": {
                  "longName": "Shake Milton 7+",
                  "shortName": "Shake Milton 7+"
               },
               "wasPrice": [],
               "id": 204140557,
               "name": "Shake Milton 7+",
               "type": "7",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "6.8",
                  "frac": "29/5",
                  "rootIdx": 182
               },
               "pos": {
                  "row": 302,
                  "col": 1
               },
               "names": {
                  "longName": "Shake Milton 6+",
                  "shortName": "Shake Milton 6+"
               },
               "wasPrice": [],
               "id": 204140555,
               "name": "Shake Milton 6+",
               "type": "6",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.85",
                  "frac": "57/20",
                  "rootIdx": 137
               },
               "pos": {
                  "row": 202,
                  "col": 1
               },
               "names": {
                  "longName": "Shake Milton 5+",
                  "shortName": "Shake Milton 5+"
               },
               "wasPrice": [],
               "id": 204140553,
               "name": "Shake Milton 5+",
               "type": "5",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.4",
                  "frac": "7/5",
                  "rootIdx": 106
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Shake Milton 4+",
                  "shortName": "Shake Milton 4+"
               },
               "wasPrice": [],
               "id": 204140551,
               "name": "Shake Milton 4+",
               "type": "4",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Shake Milton rebounds (incl. overtime)",
            "shortName": "Shake Milton rebounds (incl. overtime)"
         },
         "id": 70230236,
         "name": "Shake Milton rebounds (incl. overtime)",
         "type": "BASKETBALL:FT:PLREB",
         "subtype": "M#pre:playerprops:24750814:1495351",
         "displayOrder": -274,
         "columnCount": 1,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.85",
                  "frac": "17/20",
                  "rootIdx": 70
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers",
                  "shortName": "Philadelphia 76ers"
               },
               "wasPrice": [],
               "id": 203933359,
               "name": "Philadelphia 76ers",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.85",
                  "frac": "17/20",
                  "rootIdx": 70
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets",
                  "shortName": "Brooklyn Nets"
               },
               "wasPrice": [],
               "id": 203933361,
               "name": "Brooklyn Nets",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Race to 30 Points",
            "shortName": "Race to 30 Points"
         },
         "id": 70175510,
         "name": "Race to 30 Points",
         "type": "BASKETBALL:FTOT:RX",
         "subtype": "M#30",
         "line": 30.0,
         "displayOrder": -283,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "Jarrett Allen 2+",
                  "shortName": "Jarrett Allen 2+"
               },
               "wasPrice": [],
               "id": 204141249,
               "name": "Jarrett Allen 2+",
               "type": "2",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "6",
                  "frac": "5/1",
                  "rootIdx": 172
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Jarrett Allen 4+",
                  "shortName": "Jarrett Allen 4+"
               },
               "wasPrice": [],
               "id": 204141253,
               "name": "Jarrett Allen 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.95",
                  "frac": "59/20",
                  "rootIdx": 139
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Jarrett Allen 3+",
                  "shortName": "Jarrett Allen 3+"
               },
               "wasPrice": [],
               "id": 204141251,
               "name": "Jarrett Allen 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.17",
                  "frac": "17/100",
                  "rootIdx": 25
               },
               "pos": {
                  "row": 101,
                  "col": 1
               },
               "names": {
                  "longName": "Jarrett Allen 1+",
                  "shortName": "Jarrett Allen 1+"
               },
               "wasPrice": [],
               "id": 204141247,
               "name": "Jarrett Allen 1+",
               "type": "1",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Jarrett Allen blocks (incl. overtime)",
            "shortName": "Jarrett Allen blocks (incl. overtime)"
         },
         "id": 70230384,
         "name": "Jarrett Allen blocks (incl. overtime)",
         "type": "BASKETBALL:FT:PLBLOC",
         "subtype": "M#pre:playerprops:24750814:1124733",
         "displayOrder": -269,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.11",
                  "frac": "1/9",
                  "rootIdx": 18
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets +15.5",
                  "shortName": "Brooklyn Nets +15.5"
               },
               "wasPrice": [],
               "id": 203825873,
               "name": "Brooklyn Nets +15.5",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "5.75",
                  "frac": "19/4",
                  "rootIdx": 169
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers -15.5",
                  "shortName": "Philadelphia 76ers -15.5"
               },
               "wasPrice": [],
               "id": 203825879,
               "name": "Philadelphia 76ers -15.5",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131913,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#15.5",
         "line": 15.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "22",
                  "frac": "21/1",
                  "rootIdx": 213
               },
               "pos": {
                  "row": 302,
                  "col": 1
               },
               "names": {
                  "longName": "Danny Green 6+",
                  "shortName": "Danny Green 6+"
               },
               "wasPrice": [],
               "id": 204140999,
               "name": "Danny Green 6+",
               "type": "6",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "46",
                  "frac": "45/1",
                  "rootIdx": 237
               },
               "names": {
                  "longName": "{outcomeName}",
                  "shortName": "{outcomeName}"
               },
               "wasPrice": [],
               "id": 204591469,
               "name": "{outcomeName}",
               "type": "7",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "10",
                  "frac": "9/1",
                  "rootIdx": 196
               },
               "pos": {
                  "row": 202,
                  "col": 1
               },
               "names": {
                  "longName": "Danny Green 5+",
                  "shortName": "Danny Green 5+"
               },
               "wasPrice": [],
               "id": 204140997,
               "name": "Danny Green 5+",
               "type": "5",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4.35",
                  "frac": "67/20",
                  "rootIdx": 148
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Danny Green 4+",
                  "shortName": "Danny Green 4+"
               },
               "wasPrice": [],
               "id": 204140995,
               "name": "Danny Green 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.38",
                  "frac": "11/8",
                  "rootIdx": 105
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Danny Green 3+",
                  "shortName": "Danny Green 3+"
               },
               "wasPrice": [],
               "id": 204140993,
               "name": "Danny Green 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.45",
                  "frac": "9/20",
                  "rootIdx": 44
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "Danny Green 2+",
                  "shortName": "Danny Green 2+"
               },
               "wasPrice": [],
               "id": 204140991,
               "name": "Danny Green 2+",
               "type": "2",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Danny Green 3-point field goals (incl. overtime)",
            "shortName": "Danny Green 3-point field goals (incl. overtime)"
         },
         "id": 70230330,
         "name": "Danny Green 3-point field goals (incl. overtime)",
         "type": "BASKETBALL:FT:3PFG",
         "subtype": "M#pre:playerprops:24750814:607862",
         "displayOrder": -272,
         "columnCount": 1,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.3",
                  "frac": "3/10",
                  "rootIdx": 35
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers +7.5",
                  "shortName": "Philadelphia 76ers +7.5"
               },
               "wasPrice": [],
               "id": 203825844,
               "name": "Philadelphia 76ers +7.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.3",
                  "frac": "23/10",
                  "rootIdx": 126
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets -7.5",
                  "shortName": "Brooklyn Nets -7.5"
               },
               "wasPrice": [],
               "id": 203825835,
               "name": "Brooklyn Nets -7.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131898,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-7.5",
         "line": -7.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "25",
                  "frac": "24/1",
                  "rootIdx": 216
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Kyrie Irving 4+",
                  "shortName": "Kyrie Irving 4+"
               },
               "wasPrice": [],
               "id": 204141307,
               "name": "Kyrie Irving 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "7",
                  "frac": "6/1",
                  "rootIdx": 184
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Kyrie Irving 3+",
                  "shortName": "Kyrie Irving 3+"
               },
               "wasPrice": [],
               "id": 204141305,
               "name": "Kyrie Irving 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4.25",
                  "frac": "13/4",
                  "rootIdx": 145
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "Kyrie Irving 2+",
                  "shortName": "Kyrie Irving 2+"
               },
               "wasPrice": [],
               "id": 204141303,
               "name": "Kyrie Irving 2+",
               "type": "2",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.1",
                  "frac": "11/10",
                  "rootIdx": 93
               },
               "pos": {
                  "row": 101,
                  "col": 1
               },
               "names": {
                  "longName": "Kyrie Irving 1+",
                  "shortName": "Kyrie Irving 1+"
               },
               "wasPrice": [],
               "id": 204141301,
               "name": "Kyrie Irving 1+",
               "type": "1",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Kyrie Irving blocks (incl. overtime)",
            "shortName": "Kyrie Irving blocks (incl. overtime)"
         },
         "id": 70230396,
         "name": "Kyrie Irving blocks (incl. overtime)",
         "type": "BASKETBALL:FT:PLBLOC",
         "subtype": "M#pre:playerprops:24750814:608268",
         "displayOrder": -269,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 225.5",
                  "shortName": "Under 225.5"
               },
               "wasPrice": [],
               "id": 203826174,
               "name": "Under 225.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 225.5",
                  "shortName": "Over 225.5"
               },
               "wasPrice": [],
               "id": 203826170,
               "name": "Over 225.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132052,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#225.5",
         "line": 225.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.75",
                  "frac": "3/4",
                  "rootIdx": 63
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets +1.5",
                  "shortName": "Brooklyn Nets +1.5"
               },
               "wasPrice": [],
               "id": 203825698,
               "name": "Brooklyn Nets +1.5",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers -1.5",
                  "shortName": "Philadelphia 76ers -1.5"
               },
               "wasPrice": [],
               "id": 203825717,
               "name": "Philadelphia 76ers -1.5",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Half Spread",
            "shortName": "1st Half Spread"
         },
         "id": 70131850,
         "name": "1st Half Spread",
         "type": "BASKETBALL:P:AHCP",
         "subtype": "H1#1.5",
         "line": 1.5,
         "displayOrder": -370,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "H1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.75",
                  "frac": "3/4",
                  "rootIdx": 63
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 57.5",
                  "shortName": "Over 57.5"
               },
               "wasPrice": [],
               "id": 203935799,
               "name": "Over 57.5",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 57.5",
                  "shortName": "Under 57.5"
               },
               "wasPrice": [],
               "id": 203935797,
               "name": "Under 57.5",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "2nd Quarter Total Points",
            "shortName": "2nd Quarter Total Points"
         },
         "id": 70176648,
         "name": "2nd Quarter Total Points",
         "type": "BASKETBALL:P:OU",
         "subtype": "Q2#57.5",
         "line": 57.5,
         "displayOrder": -313,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q2"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.1",
                  "frac": "11/10",
                  "rootIdx": 93
               },
               "names": {
                  "longName": "Seth Curry 17+",
                  "shortName": "Seth Curry 17+"
               },
               "wasPrice": [],
               "id": 204141145,
               "name": "Seth Curry 17+",
               "type": "17",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.85",
                  "frac": "17/20",
                  "rootIdx": 70
               },
               "names": {
                  "longName": "Seth Curry 15+",
                  "shortName": "Seth Curry 15+"
               },
               "wasPrice": [],
               "id": 204141143,
               "name": "Seth Curry 15+",
               "type": "15",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.45",
                  "frac": "9/20",
                  "rootIdx": 44
               },
               "names": {
                  "longName": "Seth Curry 13+",
                  "shortName": "Seth Curry 13+"
               },
               "wasPrice": [],
               "id": 204141141,
               "name": "Seth Curry 13+",
               "type": "13",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.88",
                  "frac": "15/8",
                  "rootIdx": 117
               },
               "names": {
                  "longName": "Seth Curry 21+",
                  "shortName": "Seth Curry 21+"
               },
               "wasPrice": [],
               "id": 204141149,
               "name": "Seth Curry 21+",
               "type": "21",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "7.75",
                  "frac": "27/4",
                  "rootIdx": 187
               },
               "names": {
                  "longName": "Seth Curry 29+",
                  "shortName": "Seth Curry 29+"
               },
               "wasPrice": [],
               "id": 204141157,
               "name": "Seth Curry 29+",
               "type": "29",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "6.25",
                  "frac": "21/4",
                  "rootIdx": 175
               },
               "names": {
                  "longName": "Seth Curry 27+",
                  "shortName": "Seth Curry 27+"
               },
               "wasPrice": [],
               "id": 204141155,
               "name": "Seth Curry 27+",
               "type": "27",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4.85",
                  "frac": "77/20",
                  "rootIdx": 158
               },
               "names": {
                  "longName": "Seth Curry 25+",
                  "shortName": "Seth Curry 25+"
               },
               "wasPrice": [],
               "id": 204141153,
               "name": "Seth Curry 25+",
               "type": "25",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.7",
                  "frac": "27/10",
                  "rootIdx": 134
               },
               "names": {
                  "longName": "Seth Curry 23+",
                  "shortName": "Seth Curry 23+"
               },
               "wasPrice": [],
               "id": 204141151,
               "name": "Seth Curry 23+",
               "type": "23",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.35",
                  "frac": "27/20",
                  "rootIdx": 104
               },
               "names": {
                  "longName": "Seth Curry 19+",
                  "shortName": "Seth Curry 19+"
               },
               "wasPrice": [],
               "id": 204141147,
               "name": "Seth Curry 19+",
               "type": "19",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Seth Curry points (incl. overtime)",
            "shortName": "Seth Curry points (incl. overtime)"
         },
         "id": 70230362,
         "name": "Seth Curry points (incl. overtime)",
         "type": "BASKETBALL:FT:PLPOI",
         "subtype": "M#pre:playerprops:24750814:607326",
         "displayOrder": -275,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers",
                  "shortName": "Philadelphia 76ers"
               },
               "wasPrice": [],
               "id": 203826080,
               "name": "Philadelphia 76ers",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets",
                  "shortName": "Brooklyn Nets"
               },
               "wasPrice": [],
               "id": 203826068,
               "name": "Brooklyn Nets",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Race to 20 Points",
            "shortName": "Race to 20 Points"
         },
         "id": 70132009,
         "name": "Race to 20 Points",
         "type": "BASKETBALL:FTOT:RX",
         "subtype": "M#20",
         "line": 20.0,
         "displayOrder": -283,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.55",
                  "frac": "31/20",
                  "rootIdx": 109
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers -5.5",
                  "shortName": "Philadelphia 76ers -5.5"
               },
               "wasPrice": [],
               "id": 203825949,
               "name": "Philadelphia 76ers -5.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.47",
                  "frac": "40/85",
                  "rootIdx": 45
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets +5.5",
                  "shortName": "Brooklyn Nets +5.5"
               },
               "wasPrice": [],
               "id": 203825944,
               "name": "Brooklyn Nets +5.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131950,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#5.5",
         "line": 5.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.33",
                  "frac": "1/3",
                  "rootIdx": 37
               },
               "pos": {
                  "row": 101,
                  "col": 1
               },
               "names": {
                  "longName": "Danny Green 1+",
                  "shortName": "Danny Green 1+"
               },
               "wasPrice": [],
               "id": 204140561,
               "name": "Danny Green 1+",
               "type": "1",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "6.1",
                  "frac": "51/10",
                  "rootIdx": 173
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Danny Green 4+",
                  "shortName": "Danny Green 4+"
               },
               "wasPrice": [],
               "id": 204140567,
               "name": "Danny Green 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4.75",
                  "frac": "15/4",
                  "rootIdx": 156
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Danny Green 3+",
                  "shortName": "Danny Green 3+"
               },
               "wasPrice": [],
               "id": 204140565,
               "name": "Danny Green 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.2",
                  "frac": "6/5",
                  "rootIdx": 99
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "Danny Green 2+",
                  "shortName": "Danny Green 2+"
               },
               "wasPrice": [],
               "id": 204140563,
               "name": "Danny Green 2+",
               "type": "2",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Danny Green steals (incl. overtime)",
            "shortName": "Danny Green steals (incl. overtime)"
         },
         "id": 70230238,
         "name": "Danny Green steals (incl. overtime)",
         "type": "BASKETBALL:FT:PLSTA",
         "subtype": "M#pre:playerprops:24750814:607862",
         "displayOrder": -270,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.15",
                  "frac": "23/20",
                  "rootIdx": 96
               },
               "names": {
                  "longName": "Each team over 22.5 points in each quarter",
                  "shortName": "Each team over 22.5 points in each quarter"
               },
               "wasPrice": [],
               "id": 203857989,
               "name": "Each team over 22.5 points in each quarter",
               "type": "Each team over 22.5 points in each quarter",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "12",
                  "frac": "11/1",
                  "rootIdx": 200
               },
               "names": {
                  "longName": "Each team over 25.5 points in each quarter",
                  "shortName": "Each team over 25.5 points in each quarter"
               },
               "wasPrice": [],
               "id": 203857987,
               "name": "Each team over 25.5 points in each quarter",
               "type": "Each team over 25.5 points in each quarter",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.35",
                  "frac": "27/20",
                  "rootIdx": 104
               },
               "names": {
                  "longName": "Each quarter over 50.5 points",
                  "shortName": "Each quarter over 50.5 points"
               },
               "wasPrice": [],
               "id": 203857985,
               "name": "Each quarter over 50.5 points",
               "type": "Each quarter over 50.5 points",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "28",
                  "frac": "27/1",
                  "rootIdx": 219
               },
               "names": {
                  "longName": "Each quarter over 60.5 points",
                  "shortName": "Each quarter over 60.5 points"
               },
               "wasPrice": [],
               "id": 203857983,
               "name": "Each quarter over 60.5 points",
               "type": "Each quarter over 60.5 points",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "4.8",
                  "frac": "19/5",
                  "rootIdx": 157
               },
               "names": {
                  "longName": "Each team over 120.5 points",
                  "shortName": "Each team over 120.5 points"
               },
               "wasPrice": [],
               "id": 203857981,
               "name": "Each team over 120.5 points",
               "type": "Each team over 120.5 points",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "10.5",
                  "frac": "19/2",
                  "rootIdx": 197
               },
               "names": {
                  "longName": "Brooklyn Nets to Win and Each Team to Score Over 119.5 Points",
                  "shortName": "Brooklyn Nets to Win and Each Team to Score Over 119.5 Points"
               },
               "wasPrice": [],
               "id": 203858009,
               "name": "Brooklyn Nets to Win and Each Team to Score Over 119.5 Points",
               "type": "Brooklyn Nets to Win and Each Team to Score Over 119.5 Points",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.35",
                  "frac": "27/20",
                  "rootIdx": 104
               },
               "names": {
                  "longName": "Brooklyn Nets to Win and Each Team to Score Over 99.5 Points",
                  "shortName": "Brooklyn Nets to Win and Each Team to Score Over 99.5 Points"
               },
               "wasPrice": [],
               "id": 203858007,
               "name": "Brooklyn Nets to Win and Each Team to Score Over 99.5 Points",
               "type": "Brooklyn Nets to Win and Each Team to Score Over 99.5 Points",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "9.75",
                  "frac": "35/4",
                  "rootIdx": 195
               },
               "names": {
                  "longName": "Philadelphia 76ers to win all 4 quarters",
                  "shortName": "Philadelphia 76ers to win all 4 quarters"
               },
               "wasPrice": [],
               "id": 203858005,
               "name": "Philadelphia 76ers to win all 4 quarters",
               "type": "Philadelphia 76ers to win all 4 quarters",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.15",
                  "frac": "43/20",
                  "rootIdx": 123
               },
               "names": {
                  "longName": "Philadelphia 76ers to win 1st and 2nd quarter",
                  "shortName": "Philadelphia 76ers to win 1st and 2nd quarter"
               },
               "wasPrice": [],
               "id": 203858003,
               "name": "Philadelphia 76ers to win 1st and 2nd quarter",
               "type": "Philadelphia 76ers to win 1st and 2nd quarter",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "22",
                  "frac": "21/1",
                  "rootIdx": 213
               },
               "names": {
                  "longName": "Philadelphia 76ers over 30.5 points in each quarter",
                  "shortName": "Philadelphia 76ers over 30.5 points in each quarter"
               },
               "wasPrice": [],
               "id": 203858001,
               "name": "Philadelphia 76ers over 30.5 points in each quarter",
               "type": "Philadelphia 76ers over 30.5 points in each quarter",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "17",
                  "frac": "16/1",
                  "rootIdx": 208
               },
               "names": {
                  "longName": "Each team over 130.5 points",
                  "shortName": "Each team over 130.5 points"
               },
               "wasPrice": [],
               "id": 203857979,
               "name": "Each team over 130.5 points",
               "type": "Each team over 130.5 points",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.15",
                  "frac": "2/13",
                  "rootIdx": 23
               },
               "names": {
                  "longName": "Each team over 100.5 points",
                  "shortName": "Each team over 100.5 points"
               },
               "wasPrice": [],
               "id": 203857977,
               "name": "Each team over 100.5 points",
               "type": "Each team over 100.5 points",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.15",
                  "frac": "23/20",
                  "rootIdx": 96
               },
               "names": {
                  "longName": "Each team over 110.5 points",
                  "shortName": "Each team over 110.5 points"
               },
               "wasPrice": [],
               "id": 203857975,
               "name": "Each team over 110.5 points",
               "type": "Each team over 110.5 points",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "10.5",
                  "frac": "19/2",
                  "rootIdx": 197
               },
               "names": {
                  "longName": "Philadelphia 76ers To Win the first Three Quarters and lose the Fourth",
                  "shortName": "Philadelphia 76ers To Win the first Three Quarters and lose the Fourth"
               },
               "wasPrice": [],
               "id": 203857999,
               "name": "Philadelphia 76ers To Win the first Three Quarters and lose the Fourth",
               "type": "Philadelphia 76ers To Win the first Three Quarters and lose the Fourth",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "14",
                  "frac": "13/1",
                  "rootIdx": 204
               },
               "names": {
                  "longName": "Brooklyn Nets to win all 4 quarters",
                  "shortName": "Brooklyn Nets to win all 4 quarters"
               },
               "wasPrice": [],
               "id": 203857973,
               "name": "Brooklyn Nets to win all 4 quarters",
               "type": "Brooklyn Nets to win all 4 quarters",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.19",
                  "frac": "19/100",
                  "rootIdx": 27
               },
               "names": {
                  "longName": "Philadelphia 76ers over 20.5 points in each quarter",
                  "shortName": "Philadelphia 76ers over 20.5 points in each quarter"
               },
               "wasPrice": [],
               "id": 203857997,
               "name": "Philadelphia 76ers over 20.5 points in each quarter",
               "type": "Philadelphia 76ers over 20.5 points in each quarter",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.75",
                  "frac": "11/4",
                  "rootIdx": 135
               },
               "names": {
                  "longName": "Brooklyn Nets to win 1st and 2nd quarter",
                  "shortName": "Brooklyn Nets to win 1st and 2nd quarter"
               },
               "wasPrice": [],
               "id": 203857971,
               "name": "Brooklyn Nets to win 1st and 2nd quarter",
               "type": "Brooklyn Nets to win 1st and 2nd quarter",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3",
                  "frac": "2/1",
                  "rootIdx": 120
               },
               "names": {
                  "longName": "Philadelphia 76ers over 25.5 points in each quarter",
                  "shortName": "Philadelphia 76ers over 25.5 points in each quarter"
               },
               "wasPrice": [],
               "id": 203857995,
               "name": "Philadelphia 76ers over 25.5 points in each quarter",
               "type": "Philadelphia 76ers over 25.5 points in each quarter",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "7.5",
                  "frac": "13/2",
                  "rootIdx": 186
               },
               "names": {
                  "longName": "Philadelphia 76ers to Win and Each Team to Score Over 119.5 Points",
                  "shortName": "Philadelphia 76ers to Win and Each Team to Score Over 119.5 Points"
               },
               "wasPrice": [],
               "id": 203857993,
               "name": "Philadelphia 76ers to Win and Each Team to Score Over 119.5 Points",
               "type": "Philadelphia 76ers to Win and Each Team to Score Over 119.5 Points",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.1",
                  "frac": "11/10",
                  "rootIdx": 93
               },
               "names": {
                  "longName": "Philadelphia 76ers to Win and Each Team to Score Over 99.5 Points",
                  "shortName": "Philadelphia 76ers to Win and Each Team to Score Over 99.5 Points"
               },
               "wasPrice": [],
               "id": 203857991,
               "name": "Philadelphia 76ers to Win and Each Team to Score Over 99.5 Points",
               "type": "Philadelphia 76ers to Win and Each Team to Score Over 99.5 Points",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "30",
                  "frac": "29/1",
                  "rootIdx": 221
               },
               "names": {
                  "longName": "Brooklyn Nets over 30.5 points in each quarter",
                  "shortName": "Brooklyn Nets over 30.5 points in each quarter"
               },
               "wasPrice": [],
               "id": 203858017,
               "name": "Brooklyn Nets over 30.5 points in each quarter",
               "type": "Brooklyn Nets over 30.5 points in each quarter",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "12.5",
                  "frac": "23/2",
                  "rootIdx": 201
               },
               "names": {
                  "longName": "Brooklyn Nets To Win the first Three Quarters and lose the Fourth",
                  "shortName": "Brooklyn Nets To Win the first Three Quarters and lose the Fourth"
               },
               "wasPrice": [],
               "id": 203858015,
               "name": "Brooklyn Nets To Win the first Three Quarters and lose the Fourth",
               "type": "Brooklyn Nets To Win the first Three Quarters and lose the Fourth",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.25",
                  "frac": "1/4",
                  "rootIdx": 32
               },
               "names": {
                  "longName": "Brooklyn Nets over 20.5 points in each quarter",
                  "shortName": "Brooklyn Nets over 20.5 points in each quarter"
               },
               "wasPrice": [],
               "id": 203858013,
               "name": "Brooklyn Nets over 20.5 points in each quarter",
               "type": "Brooklyn Nets over 20.5 points in each quarter",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.4",
                  "frac": "12/5",
                  "rootIdx": 128
               },
               "names": {
                  "longName": "Brooklyn Nets over 25.5 points in each quarter",
                  "shortName": "Brooklyn Nets over 25.5 points in each quarter"
               },
               "wasPrice": [],
               "id": 203858011,
               "name": "Brooklyn Nets over 25.5 points in each quarter",
               "type": "Brooklyn Nets over 25.5 points in each quarter",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "CustomBet",
            "shortName": "CustomBet"
         },
         "id": 70144962,
         "name": "CustomBet",
         "type": "BASKETBALL_RAB",
         "displayOrder": -488,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": ""
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 112.5",
                  "shortName": "Over 112.5"
               },
               "wasPrice": [],
               "id": 203832339,
               "name": "Over 112.5",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 112.5",
                  "shortName": "Under 112.5"
               },
               "wasPrice": [],
               "id": 203832341,
               "name": "Under 112.5",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Philadelphia 76ers Total Points (Incl. OT)",
            "shortName": "Philadelphia 76ers Total Points (Incl. OT)"
         },
         "id": 70134674,
         "name": "Philadelphia 76ers Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:B:OU",
         "subtype": "M#112.5",
         "line": 112.5,
         "displayOrder": -440,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over {Line}",
                  "shortName": "Over {Line}"
               },
               "wasPrice": [],
               "id": 203826093,
               "name": "Over {Line}",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under {Line}",
                  "shortName": "Under {Line}"
               },
               "wasPrice": [],
               "id": 203826100,
               "name": "Under {Line}",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Total Points",
            "shortName": "Total Points"
         },
         "id": 70132017,
         "name": "Total Points",
         "type": "BASKETBALL:FTOT:OU_MAIN",
         "subtype": "226.5",
         "line": 226.5,
         "lineType": "TOTAL",
         "displayOrder": -490,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "226.5"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "5",
                  "frac": "4/1",
                  "rootIdx": 160
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Joel Embiid 3+",
                  "shortName": "Joel Embiid 3+"
               },
               "wasPrice": [],
               "id": 204141219,
               "name": "Joel Embiid 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.38",
                  "frac": "11/8",
                  "rootIdx": 105
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "Joel Embiid 2+",
                  "shortName": "Joel Embiid 2+"
               },
               "wasPrice": [],
               "id": 204141217,
               "name": "Joel Embiid 2+",
               "type": "2",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.38",
                  "frac": "19/50",
                  "rootIdx": 40
               },
               "pos": {
                  "row": 101,
                  "col": 1
               },
               "names": {
                  "longName": "Joel Embiid 1+",
                  "shortName": "Joel Embiid 1+"
               },
               "wasPrice": [],
               "id": 204141215,
               "name": "Joel Embiid 1+",
               "type": "1",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "7",
                  "frac": "6/1",
                  "rootIdx": 184
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Joel Embiid 4+",
                  "shortName": "Joel Embiid 4+"
               },
               "wasPrice": [],
               "id": 204141221,
               "name": "Joel Embiid 4+",
               "type": "4",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Joel Embiid steals (incl. overtime)",
            "shortName": "Joel Embiid steals (incl. overtime)"
         },
         "id": 70230376,
         "name": "Joel Embiid steals (incl. overtime)",
         "type": "BASKETBALL:FT:PLSTA",
         "subtype": "M#pre:playerprops:24750814:607528",
         "displayOrder": -270,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.33",
                  "frac": "1/3",
                  "rootIdx": 37
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 237.5",
                  "shortName": "Under 237.5"
               },
               "wasPrice": [],
               "id": 203826227,
               "name": "Under 237.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.3",
                  "frac": "23/10",
                  "rootIdx": 126
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 237.5",
                  "shortName": "Over 237.5"
               },
               "wasPrice": [],
               "id": 203826219,
               "name": "Over 237.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132069,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#237.5",
         "line": 237.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.28",
                  "frac": "2/7",
                  "rootIdx": 34
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 214.5",
                  "shortName": "Over 214.5"
               },
               "wasPrice": [],
               "id": 203826081,
               "name": "Over 214.5",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.5",
                  "frac": "5/2",
                  "rootIdx": 130
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 214.5",
                  "shortName": "Under 214.5"
               },
               "wasPrice": [],
               "id": 203826087,
               "name": "Under 214.5",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132010,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#214.5",
         "line": 214.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "20",
                  "frac": "19/1",
                  "rootIdx": 211
               },
               "pos": {
                  "row": 203,
                  "col": 1
               },
               "names": {
                  "longName": "Seth Curry 8+",
                  "shortName": "Seth Curry 8+"
               },
               "wasPrice": [],
               "id": 204140699,
               "name": "Seth Curry 8+",
               "type": "8",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.42",
                  "frac": "21/50",
                  "rootIdx": 42
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Seth Curry 3+",
                  "shortName": "Seth Curry 3+"
               },
               "wasPrice": [],
               "id": 204140689,
               "name": "Seth Curry 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "12.5",
                  "frac": "23/2",
                  "rootIdx": 201
               },
               "pos": {
                  "row": 103,
                  "col": 1
               },
               "names": {
                  "longName": "Seth Curry 7+",
                  "shortName": "Seth Curry 7+"
               },
               "wasPrice": [],
               "id": 204140697,
               "name": "Seth Curry 7+",
               "type": "7",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "7.25",
                  "frac": "25/4",
                  "rootIdx": 185
               },
               "pos": {
                  "row": 302,
                  "col": 1
               },
               "names": {
                  "longName": "Seth Curry 6+",
                  "shortName": "Seth Curry 6+"
               },
               "wasPrice": [],
               "id": 204140695,
               "name": "Seth Curry 6+",
               "type": "6",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.85",
                  "frac": "57/20",
                  "rootIdx": 137
               },
               "pos": {
                  "row": 202,
                  "col": 1
               },
               "names": {
                  "longName": "Seth Curry 5+",
                  "shortName": "Seth Curry 5+"
               },
               "wasPrice": [],
               "id": 204140693,
               "name": "Seth Curry 5+",
               "type": "5",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.25",
                  "frac": "5/4",
                  "rootIdx": 101
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Seth Curry 4+",
                  "shortName": "Seth Curry 4+"
               },
               "wasPrice": [],
               "id": 204140691,
               "name": "Seth Curry 4+",
               "type": "4",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Seth Curry assists (incl. overtime)",
            "shortName": "Seth Curry assists (incl. overtime)"
         },
         "id": 70230268,
         "name": "Seth Curry assists (incl. overtime)",
         "type": "BASKETBALL:FT:PLASS",
         "subtype": "M#pre:playerprops:24750814:607326",
         "displayOrder": -273,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.25",
                  "frac": "1/4",
                  "rootIdx": 32
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 240.5",
                  "shortName": "Under 240.5"
               },
               "wasPrice": [],
               "id": 204405049,
               "name": "Under 240.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.75",
                  "frac": "11/4",
                  "rootIdx": 135
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 240.5",
                  "shortName": "Over 240.5"
               },
               "wasPrice": [],
               "id": 204405047,
               "name": "Over 240.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70287576,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#240.5",
         "line": 240.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.7",
                  "frac": "17/10",
                  "rootIdx": 113
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets -5.5",
                  "shortName": "Brooklyn Nets -5.5"
               },
               "wasPrice": [],
               "id": 203826330,
               "name": "Brooklyn Nets -5.5",
               "type": "1",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.95",
                  "frac": "39/20",
                  "rootIdx": 119
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Neither Team -5.5",
                  "shortName": "Neither Team -5.5"
               },
               "wasPrice": [],
               "id": 203826343,
               "name": "Neither Team -5.5",
               "type": "Draw",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.63",
                  "frac": "13/8",
                  "rootIdx": 111
               },
               "pos": {
                  "row": 1,
                  "col": 3
               },
               "names": {
                  "longName": "Philadelphia 76ers -5.5",
                  "shortName": "Philadelphia 76ers -5.5"
               },
               "wasPrice": [],
               "id": 203826335,
               "name": "Philadelphia 76ers -5.5",
               "type": "2",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Winning Margin 3-Way(Incl. OT)",
            "shortName": "Winning Margin 3-Way(Incl. OT)"
         },
         "id": 70132128,
         "name": "Winning Margin 3-Way(Incl. OT)",
         "type": "BASKETBALL:FTOT:WM",
         "subtype": "M#sr:winning_margin:6+",
         "displayOrder": -410,
         "columnCount": 3,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3.55",
                  "frac": "51/20",
                  "rootIdx": 131
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers & Over 226.5",
                  "shortName": "Philadelphia 76ers & Over 226.5"
               },
               "wasPrice": [],
               "id": 204220319,
               "name": "Philadelphia 76ers & Over 226.5",
               "type": "BOver",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.55",
                  "frac": "51/20",
                  "rootIdx": 131
               },
               "pos": {
                  "row": 2,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets & Under 226.5",
                  "shortName": "Brooklyn Nets & Under 226.5"
               },
               "wasPrice": [],
               "id": 204220317,
               "name": "Brooklyn Nets & Under 226.5",
               "type": "AUnder",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.5",
                  "frac": "5/2",
                  "rootIdx": 130
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Philadelphia 76ers & Under 226.5",
                  "shortName": "Philadelphia 76ers & Under 226.5"
               },
               "wasPrice": [],
               "id": 204220315,
               "name": "Philadelphia 76ers & Under 226.5",
               "type": "BUnder",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.65",
                  "frac": "53/20",
                  "rootIdx": 133
               },
               "pos": {
                  "row": 2,
                  "col": 2
               },
               "names": {
                  "longName": "Brooklyn Nets & Over 226.5",
                  "shortName": "Brooklyn Nets & Over 226.5"
               },
               "wasPrice": [],
               "id": 204220321,
               "name": "Brooklyn Nets & Over 226.5",
               "type": "AOver",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Matchbet and Total Points (Incl. OT)",
            "shortName": "Matchbet and Total Points (Incl. OT)"
         },
         "id": 70248544,
         "name": "Matchbet and Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:AXB$OU",
         "subtype": "M#226.5",
         "line": 226.5,
         "displayOrder": -425,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3.45",
                  "frac": "49/20",
                  "rootIdx": 129
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Neither Team -2.5",
                  "shortName": "Neither Team -2.5"
               },
               "wasPrice": [],
               "id": 204128627,
               "name": "Neither Team -2.5",
               "type": "other",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.5",
                  "frac": "3/2",
                  "rootIdx": 108
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets -2.5",
                  "shortName": "Brooklyn Nets -2.5"
               },
               "wasPrice": [],
               "id": 204128625,
               "name": "Brooklyn Nets -2.5",
               "type": "A>2",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.5",
                  "frac": "3/2",
                  "rootIdx": 108
               },
               "pos": {
                  "row": 1,
                  "col": 3
               },
               "names": {
                  "longName": "Philadelphia 76ers -2.5",
                  "shortName": "Philadelphia 76ers -2.5"
               },
               "wasPrice": [],
               "id": 204128623,
               "name": "Philadelphia 76ers -2.5",
               "type": "B>2",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "2nd Quarter Winning Margin",
            "shortName": "2nd Quarter Winning Margin"
         },
         "id": 70226618,
         "name": "2nd Quarter Winning Margin",
         "type": "BASKETBALL:P:WM",
         "subtype": "Q2",
         "displayOrder": -311,
         "columnCount": 3,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q2"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.5",
                  "frac": "3/2",
                  "rootIdx": 108
               },
               "pos": {
                  "row": 1,
                  "col": 3
               },
               "names": {
                  "longName": "Philadelphia 76ers -2.5",
                  "shortName": "Philadelphia 76ers -2.5"
               },
               "wasPrice": [],
               "id": 204128615,
               "name": "Philadelphia 76ers -2.5",
               "type": "B>2",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.45",
                  "frac": "49/20",
                  "rootIdx": 129
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Neither Team -2.5",
                  "shortName": "Neither Team -2.5"
               },
               "wasPrice": [],
               "id": 204128613,
               "name": "Neither Team -2.5",
               "type": "other",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.5",
                  "frac": "3/2",
                  "rootIdx": 108
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets -2.5",
                  "shortName": "Brooklyn Nets -2.5"
               },
               "wasPrice": [],
               "id": 204128611,
               "name": "Brooklyn Nets -2.5",
               "type": "A>2",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Quarter Winning Margin",
            "shortName": "1st Quarter Winning Margin"
         },
         "id": 70226614,
         "name": "1st Quarter Winning Margin",
         "type": "BASKETBALL:P:WM",
         "subtype": "Q1",
         "displayOrder": -321,
         "columnCount": 3,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3.35",
                  "frac": "47/20",
                  "rootIdx": 127
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Neither Team -2.5",
                  "shortName": "Neither Team -2.5"
               },
               "wasPrice": [],
               "id": 204128637,
               "name": "Neither Team -2.5",
               "type": "other",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.55",
                  "frac": "31/20",
                  "rootIdx": 109
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets -2.5",
                  "shortName": "Brooklyn Nets -2.5"
               },
               "wasPrice": [],
               "id": 204128635,
               "name": "Brooklyn Nets -2.5",
               "type": "A>2",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.5",
                  "frac": "3/2",
                  "rootIdx": 108
               },
               "pos": {
                  "row": 1,
                  "col": 3
               },
               "names": {
                  "longName": "Philadelphia 76ers -2.5",
                  "shortName": "Philadelphia 76ers -2.5"
               },
               "wasPrice": [],
               "id": 204128633,
               "name": "Philadelphia 76ers -2.5",
               "type": "B>2",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "4th Quarter Winning Margin (Excl. OT)",
            "shortName": "4th Quarter Winning Margin (Excl. OT)"
         },
         "id": 70226622,
         "name": "4th Quarter Winning Margin (Excl. OT)",
         "type": "BASKETBALL:P:WM",
         "subtype": "Q4",
         "displayOrder": -291,
         "columnCount": 3,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q4"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.5",
                  "frac": "3/2",
                  "rootIdx": 108
               },
               "pos": {
                  "row": 1,
                  "col": 3
               },
               "names": {
                  "longName": "Philadelphia 76ers -2.5",
                  "shortName": "Philadelphia 76ers -2.5"
               },
               "wasPrice": [],
               "id": 204128621,
               "name": "Philadelphia 76ers -2.5",
               "type": "B>2",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.4",
                  "frac": "12/5",
                  "rootIdx": 128
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Neither Team -2.5",
                  "shortName": "Neither Team -2.5"
               },
               "wasPrice": [],
               "id": 204128619,
               "name": "Neither Team -2.5",
               "type": "other",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.55",
                  "frac": "31/20",
                  "rootIdx": 109
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets -2.5",
                  "shortName": "Brooklyn Nets -2.5"
               },
               "wasPrice": [],
               "id": 204128617,
               "name": "Brooklyn Nets -2.5",
               "type": "A>2",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "3rd Quarter Winning Margin",
            "shortName": "3rd Quarter Winning Margin"
         },
         "id": 70226616,
         "name": "3rd Quarter Winning Margin",
         "type": "BASKETBALL:P:WM",
         "subtype": "Q3",
         "displayOrder": -301,
         "columnCount": 3,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q3"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 57",
                  "shortName": "Under 57"
               },
               "wasPrice": [],
               "id": 203825763,
               "name": "Under 57",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 57",
                  "shortName": "Over 57"
               },
               "wasPrice": [],
               "id": 203825748,
               "name": "Over 57",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Half Philadelphia 76ers Total Points",
            "shortName": "1st Half Philadelphia 76ers Total Points"
         },
         "id": 70131853,
         "name": "1st Half Philadelphia 76ers Total Points",
         "type": "BASKETBALL:P:B:OU",
         "subtype": "H1#57",
         "line": 57.0,
         "displayOrder": -350,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "H1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 58",
                  "shortName": "Over 58"
               },
               "wasPrice": [],
               "id": 203927597,
               "name": "Over 58",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 58",
                  "shortName": "Under 58"
               },
               "wasPrice": [],
               "id": 203927595,
               "name": "Under 58",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Half Philadelphia 76ers Total Points",
            "shortName": "1st Half Philadelphia 76ers Total Points"
         },
         "id": 70173198,
         "name": "1st Half Philadelphia 76ers Total Points",
         "type": "BASKETBALL:P:B:OU",
         "subtype": "H1#58",
         "line": 58.0,
         "displayOrder": -350,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "H1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "6",
                  "frac": "5/1",
                  "rootIdx": 172
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers -16.5",
                  "shortName": "Philadelphia 76ers -16.5"
               },
               "wasPrice": [],
               "id": 203825865,
               "name": "Philadelphia 76ers -16.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.11",
                  "frac": "1/9",
                  "rootIdx": 18
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets +16.5",
                  "shortName": "Brooklyn Nets +16.5"
               },
               "wasPrice": [],
               "id": 203825860,
               "name": "Brooklyn Nets +16.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131906,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#16.5",
         "line": 16.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.85",
                  "frac": "37/20",
                  "rootIdx": 116
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers -6.5",
                  "shortName": "Philadelphia 76ers -6.5"
               },
               "wasPrice": [],
               "id": 203825938,
               "name": "Philadelphia 76ers -6.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.4",
                  "frac": "2/5",
                  "rootIdx": 41
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets +6.5",
                  "shortName": "Brooklyn Nets +6.5"
               },
               "wasPrice": [],
               "id": 203825936,
               "name": "Brooklyn Nets +6.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131949,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#6.5",
         "line": 6.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers {awayLine}",
                  "shortName": "Philadelphia 76ers {awayLine}"
               },
               "wasPrice": [],
               "id": 203826042,
               "name": "Philadelphia 76ers {awayLine}",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets {homeLine}",
                  "shortName": "Brooklyn Nets {homeLine}"
               },
               "wasPrice": [],
               "id": 203826035,
               "name": "Brooklyn Nets {homeLine}",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Spread",
            "shortName": "Spread"
         },
         "id": 70131990,
         "name": "Spread",
         "type": "BASKETBALL:FTOT:AHCP_MAIN",
         "subtype": "1.0",
         "line": 1.0,
         "lineType": "HANDICAP",
         "displayOrder": -495,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "4.33",
                  "frac": "10/3",
                  "rootIdx": 147
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets -10.5",
                  "shortName": "Brooklyn Nets -10.5"
               },
               "wasPrice": [],
               "id": 203825725,
               "name": "Brooklyn Nets -10.5",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.18",
                  "frac": "2/11",
                  "rootIdx": 26
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers +10.5",
                  "shortName": "Philadelphia 76ers +10.5"
               },
               "wasPrice": [],
               "id": 203825740,
               "name": "Philadelphia 76ers +10.5",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131860,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-10.5",
         "line": -10.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3.75",
                  "frac": "11/4",
                  "rootIdx": 135
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets -8.5",
                  "shortName": "Brooklyn Nets -8.5"
               },
               "wasPrice": [],
               "id": 203825833,
               "name": "Brooklyn Nets -8.5",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.25",
                  "frac": "1/4",
                  "rootIdx": 32
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers +8.5",
                  "shortName": "Philadelphia 76ers +8.5"
               },
               "wasPrice": [],
               "id": 203825842,
               "name": "Philadelphia 76ers +8.5",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131896,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-8.5",
         "line": -8.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 226.5",
                  "shortName": "Over 226.5"
               },
               "wasPrice": [],
               "id": 203826160,
               "name": "Over 226.5",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 226.5",
                  "shortName": "Under 226.5"
               },
               "wasPrice": [],
               "id": 203826169,
               "name": "Under 226.5",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132053,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#226.5",
         "line": 226.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "25",
                  "frac": "24/1",
                  "rootIdx": 216
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Joe Harris 4+",
                  "shortName": "Joe Harris 4+"
               },
               "wasPrice": [],
               "id": 204141083,
               "name": "Joe Harris 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "7",
                  "frac": "6/1",
                  "rootIdx": 184
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Joe Harris 3+",
                  "shortName": "Joe Harris 3+"
               },
               "wasPrice": [],
               "id": 204141081,
               "name": "Joe Harris 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.1",
                  "frac": "11/10",
                  "rootIdx": 93
               },
               "pos": {
                  "row": 101,
                  "col": 1
               },
               "names": {
                  "longName": "Joe Harris 1+",
                  "shortName": "Joe Harris 1+"
               },
               "wasPrice": [],
               "id": 204141077,
               "name": "Joe Harris 1+",
               "type": "1",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4.25",
                  "frac": "13/4",
                  "rootIdx": 145
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "Joe Harris 2+",
                  "shortName": "Joe Harris 2+"
               },
               "wasPrice": [],
               "id": 204141079,
               "name": "Joe Harris 2+",
               "type": "2",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Joe Harris blocks (incl. overtime)",
            "shortName": "Joe Harris blocks (incl. overtime)"
         },
         "id": 70230348,
         "name": "Joe Harris blocks (incl. overtime)",
         "type": "BASKETBALL:FT:PLBLOC",
         "subtype": "M#pre:playerprops:24750814:607950",
         "displayOrder": -269,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "Joel Embiid 2+",
                  "shortName": "Joel Embiid 2+"
               },
               "wasPrice": [],
               "id": 204140679,
               "name": "Joel Embiid 2+",
               "type": "2",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "46",
                  "frac": "45/1",
                  "rootIdx": 237
               },
               "pos": {
                  "row": 302,
                  "col": 1
               },
               "names": {
                  "longName": "Joel Embiid 6+",
                  "shortName": "Joel Embiid 6+"
               },
               "wasPrice": [],
               "id": 204140687,
               "name": "Joel Embiid 6+",
               "type": "6",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "23",
                  "frac": "22/1",
                  "rootIdx": 214
               },
               "pos": {
                  "row": 202,
                  "col": 1
               },
               "names": {
                  "longName": "Joel Embiid 5+",
                  "shortName": "Joel Embiid 5+"
               },
               "wasPrice": [],
               "id": 204140685,
               "name": "Joel Embiid 5+",
               "type": "5",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "12",
                  "frac": "11/1",
                  "rootIdx": 200
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Joel Embiid 4+",
                  "shortName": "Joel Embiid 4+"
               },
               "wasPrice": [],
               "id": 204140683,
               "name": "Joel Embiid 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "6.4",
                  "frac": "27/5",
                  "rootIdx": 177
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Joel Embiid 3+",
                  "shortName": "Joel Embiid 3+"
               },
               "wasPrice": [],
               "id": 204140681,
               "name": "Joel Embiid 3+",
               "type": "3",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Joel Embiid 3-point field goals (incl. overtime)",
            "shortName": "Joel Embiid 3-point field goals (incl. overtime)"
         },
         "id": 70230266,
         "name": "Joel Embiid 3-point field goals (incl. overtime)",
         "type": "BASKETBALL:FT:3PFG",
         "subtype": "M#pre:playerprops:24750814:607528",
         "displayOrder": -272,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers -0.5",
                  "shortName": "Philadelphia 76ers -0.5"
               },
               "wasPrice": [],
               "id": 203935793,
               "name": "Philadelphia 76ers -0.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.75",
                  "frac": "3/4",
                  "rootIdx": 63
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets +0.5",
                  "shortName": "Brooklyn Nets +0.5"
               },
               "wasPrice": [],
               "id": 203935795,
               "name": "Brooklyn Nets +0.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "2nd Quarter Spread",
            "shortName": "2nd Quarter Spread"
         },
         "id": 70176646,
         "name": "2nd Quarter Spread",
         "type": "BASKETBALL:P:PSPRD",
         "subtype": "Q2#0.5",
         "line": 0.5,
         "displayOrder": -314,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q2"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 56.5",
                  "shortName": "Under 56.5"
               },
               "wasPrice": [],
               "id": 203988547,
               "name": "Under 56.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.75",
                  "frac": "3/4",
                  "rootIdx": 63
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 56.5",
                  "shortName": "Over 56.5"
               },
               "wasPrice": [],
               "id": 203988549,
               "name": "Over 56.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "2nd Quarter Total Points",
            "shortName": "2nd Quarter Total Points"
         },
         "id": 70189730,
         "name": "2nd Quarter Total Points",
         "type": "BASKETBALL:P:OU",
         "subtype": "Q2#56.5",
         "line": 56.5,
         "displayOrder": -313,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q2"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "18",
                  "frac": "17/1",
                  "rootIdx": 209
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Tobias Harris 4+",
                  "shortName": "Tobias Harris 4+"
               },
               "wasPrice": [],
               "id": 204141269,
               "name": "Tobias Harris 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "5.9",
                  "frac": "49/10",
                  "rootIdx": 171
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Tobias Harris 3+",
                  "shortName": "Tobias Harris 3+"
               },
               "wasPrice": [],
               "id": 204141267,
               "name": "Tobias Harris 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3",
                  "frac": "2/1",
                  "rootIdx": 120
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "Tobias Harris 2+",
                  "shortName": "Tobias Harris 2+"
               },
               "wasPrice": [],
               "id": 204141265,
               "name": "Tobias Harris 2+",
               "type": "2",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.5",
                  "frac": "1/2",
                  "rootIdx": 47
               },
               "pos": {
                  "row": 101,
                  "col": 1
               },
               "names": {
                  "longName": "Tobias Harris 1+",
                  "shortName": "Tobias Harris 1+"
               },
               "wasPrice": [],
               "id": 204141263,
               "name": "Tobias Harris 1+",
               "type": "1",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Tobias Harris blocks (incl. overtime)",
            "shortName": "Tobias Harris blocks (incl. overtime)"
         },
         "id": 70230388,
         "name": "Tobias Harris blocks (incl. overtime)",
         "type": "BASKETBALL:FT:PLBLOC",
         "subtype": "M#pre:playerprops:24750814:608250",
         "displayOrder": -269,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "6",
                  "frac": "5/1",
                  "rootIdx": 172
               },
               "names": {
                  "longName": "Ben Simmons to Record a Triple Double and 76ers to win",
                  "shortName": "Ben Simmons to Record a Triple Double and 76ers to win"
               },
               "wasPrice": [
                  {
                     "channel": "INTERNET",
                     "decimalOdds": 5.5,
                     "fractionalOdds": "9/2"
                  },
                  {
                     "channel": "IT",
                     "decimalOdds": 5.5,
                     "fractionalOdds": "9/2"
                  },
                  {
                     "channel": "AR",
                     "decimalOdds": 5.5,
                     "fractionalOdds": "9/2"
                  },
                  {
                     "channel": "NJ",
                     "decimalOdds": 5.5,
                     "fractionalOdds": "9/2"
                  },
                  {
                     "channel": "PA",
                     "decimalOdds": 5.5,
                     "fractionalOdds": "9/2"
                  },
                  {
                     "channel": "CO",
                     "decimalOdds": 5.5,
                     "fractionalOdds": "9/2"
                  },
                  {
                     "channel": "DE",
                     "decimalOdds": 5.5,
                     "fractionalOdds": "9/2"
                  },
                  {
                     "channel": "ES",
                     "decimalOdds": 5.5,
                     "fractionalOdds": "9/2"
                  },
                  {
                     "channel": "GR",
                     "decimalOdds": 5.5,
                     "fractionalOdds": "9/2"
                  },
                  {
                     "channel": "MI",
                     "decimalOdds": 5.5,
                     "fractionalOdds": "9/2"
                  }
               ],
               "id": 204128343,
               "name": "Ben Simmons to Record a Triple Double and 76ers to win",
               "type": "Ben Simmons to Record a Triple Double and 76ers to win",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "5.25",
                  "frac": "17/4",
                  "rootIdx": 163
               },
               "names": {
                  "longName": "Each team over 120.5 points",
                  "shortName": "Each team over 120.5 points"
               },
               "wasPrice": [
                  {
                     "channel": "INTERNET",
                     "decimalOdds": 4.8,
                     "fractionalOdds": "19/5"
                  },
                  {
                     "channel": "IT",
                     "decimalOdds": 4.8,
                     "fractionalOdds": "19/5"
                  },
                  {
                     "channel": "AR",
                     "decimalOdds": 4.8,
                     "fractionalOdds": "19/5"
                  },
                  {
                     "channel": "NJ",
                     "decimalOdds": 4.8,
                     "fractionalOdds": "19/5"
                  },
                  {
                     "channel": "PA",
                     "decimalOdds": 4.8,
                     "fractionalOdds": "19/5"
                  },
                  {
                     "channel": "CO",
                     "decimalOdds": 4.8,
                     "fractionalOdds": "19/5"
                  },
                  {
                     "channel": "DE",
                     "decimalOdds": 4.8,
                     "fractionalOdds": "19/5"
                  },
                  {
                     "channel": "ES",
                     "decimalOdds": 4.8,
                     "fractionalOdds": "19/5"
                  },
                  {
                     "channel": "GR",
                     "decimalOdds": 4.8,
                     "fractionalOdds": "19/5"
                  },
                  {
                     "channel": "MI",
                     "decimalOdds": 4.8,
                     "fractionalOdds": "19/5"
                  }
               ],
               "id": 204128341,
               "name": "Each team over 120.5 points",
               "type": "Each team over 120.5 points",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "5",
                  "frac": "4/1",
                  "rootIdx": 160
               },
               "names": {
                  "longName": "5x Your Money - Joel Embiid 30 or More Points and Philadelphia 76ers to Win",
                  "shortName": "",
                  "veryshortName": ""
               },
               "wasPrice": [
                  {
                     "channel": "INTERNET",
                     "decimalOdds": 3.9,
                     "fractionalOdds": "29/10"
                  },
                  {
                     "channel": "IT",
                     "decimalOdds": 3.9,
                     "fractionalOdds": "29/10"
                  },
                  {
                     "channel": "AR",
                     "decimalOdds": 3.9,
                     "fractionalOdds": "29/10"
                  },
                  {
                     "channel": "NJ",
                     "decimalOdds": 3.9,
                     "fractionalOdds": "29/10"
                  },
                  {
                     "channel": "PA",
                     "decimalOdds": 3.9,
                     "fractionalOdds": "29/10"
                  },
                  {
                     "channel": "CO",
                     "decimalOdds": 3.9,
                     "fractionalOdds": "29/10"
                  },
                  {
                     "channel": "DE",
                     "decimalOdds": 3.9,
                     "fractionalOdds": "29/10"
                  },
                  {
                     "channel": "ES",
                     "decimalOdds": 3.9,
                     "fractionalOdds": "29/10"
                  },
                  {
                     "channel": "GR",
                     "decimalOdds": 3.9,
                     "fractionalOdds": "29/10"
                  },
                  {
                     "channel": "MI",
                     "decimalOdds": 3.9,
                     "fractionalOdds": "29/10"
                  }
               ],
               "id": 204128349,
               "name": "5x Your Money - Joel Embiid 30 or More Points and Philadelphia 76ers to Win",
               "type": "Joel Embiid Over 29.5 Points and Philadelphia 76ers to Win",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.8",
                  "frac": "14/5",
                  "rootIdx": 136
               },
               "names": {
                  "longName": "Kyrie Irving Over 29.5 Points and Brooklyn Nets to Win",
                  "shortName": "Kyrie Irving Over 29.5 Points and Brooklyn Nets to Win"
               },
               "wasPrice": [
                  {
                     "channel": "INTERNET",
                     "decimalOdds": 3.4,
                     "fractionalOdds": "12/5"
                  },
                  {
                     "channel": "IT",
                     "decimalOdds": 3.4,
                     "fractionalOdds": "12/5"
                  },
                  {
                     "channel": "AR",
                     "decimalOdds": 3.4,
                     "fractionalOdds": "12/5"
                  },
                  {
                     "channel": "NJ",
                     "decimalOdds": 3.4,
                     "fractionalOdds": "12/5"
                  },
                  {
                     "channel": "PA",
                     "decimalOdds": 3.4,
                     "fractionalOdds": "12/5"
                  },
                  {
                     "channel": "CO",
                     "decimalOdds": 3.4,
                     "fractionalOdds": "12/5"
                  },
                  {
                     "channel": "DE",
                     "decimalOdds": 3.4,
                     "fractionalOdds": "12/5"
                  },
                  {
                     "channel": "ES",
                     "decimalOdds": 3.4,
                     "fractionalOdds": "12/5"
                  },
                  {
                     "channel": "GR",
                     "decimalOdds": 3.4,
                     "fractionalOdds": "12/5"
                  },
                  {
                     "channel": "MI",
                     "decimalOdds": 3.4,
                     "fractionalOdds": "12/5"
                  }
               ],
               "id": 204128347,
               "name": "Kyrie Irving Over 29.5 Points and Brooklyn Nets to Win",
               "type": "Kyrie Irving Over 29.5 Points and Brooklyn Nets to Win",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "5.5",
                  "frac": "9/2",
                  "rootIdx": 166
               },
               "names": {
                  "longName": "Tobias Harris and Joel Embiid to combine for over 60.5 points",
                  "shortName": "Tobias Harris and Joel Embiid to combine for over 60.5 points"
               },
               "wasPrice": [
                  {
                     "channel": "INTERNET",
                     "decimalOdds": 5.0,
                     "fractionalOdds": "4/1"
                  },
                  {
                     "channel": "IT",
                     "decimalOdds": 5.0,
                     "fractionalOdds": "4/1"
                  },
                  {
                     "channel": "AR",
                     "decimalOdds": 5.0,
                     "fractionalOdds": "4/1"
                  },
                  {
                     "channel": "NJ",
                     "decimalOdds": 5.0,
                     "fractionalOdds": "4/1"
                  },
                  {
                     "channel": "PA",
                     "decimalOdds": 5.0,
                     "fractionalOdds": "4/1"
                  },
                  {
                     "channel": "CO",
                     "decimalOdds": 5.0,
                     "fractionalOdds": "4/1"
                  },
                  {
                     "channel": "DE",
                     "decimalOdds": 5.0,
                     "fractionalOdds": "4/1"
                  },
                  {
                     "channel": "ES",
                     "decimalOdds": 5.0,
                     "fractionalOdds": "4/1"
                  },
                  {
                     "channel": "GR",
                     "decimalOdds": 5.0,
                     "fractionalOdds": "4/1"
                  },
                  {
                     "channel": "MI",
                     "decimalOdds": 5.0,
                     "fractionalOdds": "4/1"
                  }
               ],
               "id": 204128345,
               "name": "Tobias Harris and Joel Embiid to combine for over 60.5 points",
               "type": "Tobias Harris and Joel Embiid to combine for over 60.5 points",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Bet Boost",
            "shortName": "",
            "veryshortName": ""
         },
         "id": 70226504,
         "name": "Bet Boost",
         "type": "BASKETBALL_SPEC",
         "displayOrder": -489,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": ""
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.85",
                  "frac": "17/20",
                  "rootIdx": 70
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers",
                  "shortName": "Philadelphia 76ers"
               },
               "wasPrice": [],
               "id": 203927199,
               "name": "Philadelphia 76ers",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.85",
                  "frac": "17/20",
                  "rootIdx": 70
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets",
                  "shortName": "Brooklyn Nets"
               },
               "wasPrice": [],
               "id": 203927197,
               "name": "Brooklyn Nets",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Race to 40 Points",
            "shortName": "Race to 40 Points"
         },
         "id": 70173016,
         "name": "Race to 40 Points",
         "type": "BASKETBALL:FTOT:RX",
         "subtype": "M#40",
         "line": 40.0,
         "displayOrder": -283,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.75",
                  "frac": "3/4",
                  "rootIdx": 63
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets +0.5",
                  "shortName": "Brooklyn Nets +0.5"
               },
               "wasPrice": [],
               "id": 203825734,
               "name": "Brooklyn Nets +0.5",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers -0.5",
                  "shortName": "Philadelphia 76ers -0.5"
               },
               "wasPrice": [],
               "id": 203825757,
               "name": "Philadelphia 76ers -0.5",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "2nd Half Spread (Excl. OT)",
            "shortName": "2nd Half Spread (Excl. OT)"
         },
         "id": 70131852,
         "name": "2nd Half Spread (Excl. OT)",
         "type": "BASKETBALL:P:AHCP",
         "subtype": "H2#0.5",
         "line": 0.5,
         "displayOrder": -340,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "H2"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "14",
                  "frac": "13/1",
                  "rootIdx": 204
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Jarrett Allen 4+",
                  "shortName": "Jarrett Allen 4+"
               },
               "wasPrice": [],
               "id": 204140483,
               "name": "Jarrett Allen 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "6",
                  "frac": "5/1",
                  "rootIdx": 172
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Jarrett Allen 3+",
                  "shortName": "Jarrett Allen 3+"
               },
               "wasPrice": [],
               "id": 204140481,
               "name": "Jarrett Allen 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.75",
                  "frac": "3/4",
                  "rootIdx": 63
               },
               "pos": {
                  "row": 101,
                  "col": 1
               },
               "names": {
                  "longName": "Jarrett Allen 1+",
                  "shortName": "Jarrett Allen 1+"
               },
               "wasPrice": [],
               "id": 204140477,
               "name": "Jarrett Allen 1+",
               "type": "1",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.35",
                  "frac": "47/20",
                  "rootIdx": 127
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "Jarrett Allen 2+",
                  "shortName": "Jarrett Allen 2+"
               },
               "wasPrice": [],
               "id": 204140479,
               "name": "Jarrett Allen 2+",
               "type": "2",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Jarrett Allen steals (incl. overtime)",
            "shortName": "Jarrett Allen steals (incl. overtime)"
         },
         "id": 70230220,
         "name": "Jarrett Allen steals (incl. overtime)",
         "type": "BASKETBALL:FT:PLSTA",
         "subtype": "M#pre:playerprops:24750814:1124733",
         "displayOrder": -270,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3.3",
                  "frac": "23/10",
                  "rootIdx": 126
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 215.5",
                  "shortName": "Under 215.5"
               },
               "wasPrice": [],
               "id": 203826099,
               "name": "Under 215.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.33",
                  "frac": "1/3",
                  "rootIdx": 37
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 215.5",
                  "shortName": "Over 215.5"
               },
               "wasPrice": [],
               "id": 203826092,
               "name": "Over 215.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132018,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#215.5",
         "line": 215.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3.5",
                  "frac": "5/2",
                  "rootIdx": 130
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 238.5",
                  "shortName": "Over 238.5"
               },
               "wasPrice": [],
               "id": 203840893,
               "name": "Over 238.5",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.28",
                  "frac": "2/7",
                  "rootIdx": 34
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 238.5",
                  "shortName": "Under 238.5"
               },
               "wasPrice": [],
               "id": 203840895,
               "name": "Under 238.5",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70138234,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#238.5",
         "line": 238.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.3",
                  "frac": "13/10",
                  "rootIdx": 103
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 27.5",
                  "shortName": "Under 27.5"
               },
               "wasPrice": [],
               "id": 203825999,
               "name": "Under 27.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.55",
                  "frac": "11/20",
                  "rootIdx": 51
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 27.5",
                  "shortName": "Over 27.5"
               },
               "wasPrice": [],
               "id": 203825984,
               "name": "Over 27.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Quarter Philadelphia 76ers Total Points",
            "shortName": "1st Quarter Philadelphia 76ers Total Points"
         },
         "id": 70131968,
         "name": "1st Quarter Philadelphia 76ers Total Points",
         "type": "BASKETBALL:P:B:OU",
         "subtype": "Q1#27.5",
         "line": 27.5,
         "displayOrder": -317,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 115.5",
                  "shortName": "Over 115.5"
               },
               "wasPrice": [],
               "id": 203927601,
               "name": "Over 115.5",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 115.5",
                  "shortName": "Under 115.5"
               },
               "wasPrice": [],
               "id": 203927599,
               "name": "Under 115.5",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Half Total Points",
            "shortName": "1st Half Total Points"
         },
         "id": 70173200,
         "name": "1st Half Total Points",
         "type": "BASKETBALL:P:OU",
         "subtype": "H1#115.5",
         "line": 115.5,
         "displayOrder": -365,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "H1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.85",
                  "frac": "17/20",
                  "rootIdx": 70
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 57.5",
                  "shortName": "Under 57.5"
               },
               "wasPrice": [],
               "id": 203840907,
               "name": "Under 57.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.85",
                  "frac": "17/20",
                  "rootIdx": 70
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 57.5",
                  "shortName": "Over 57.5"
               },
               "wasPrice": [],
               "id": 203840905,
               "name": "Over 57.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Half Philadelphia 76ers Total Points",
            "shortName": "1st Half Philadelphia 76ers Total Points"
         },
         "id": 70138240,
         "name": "1st Half Philadelphia 76ers Total Points",
         "type": "BASKETBALL:P:B:OU",
         "subtype": "H1#57.5",
         "line": 57.5,
         "displayOrder": -350,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "H1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.14",
                  "frac": "1/7",
                  "rootIdx": 21
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers +12",
                  "shortName": "Philadelphia 76ers +12"
               },
               "wasPrice": [],
               "id": 203828483,
               "name": "Philadelphia 76ers +12",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "5",
                  "frac": "4/1",
                  "rootIdx": 160
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets -12",
                  "shortName": "Brooklyn Nets -12"
               },
               "wasPrice": [],
               "id": 203828481,
               "name": "Brooklyn Nets -12",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70133042,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-12",
         "line": -12.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.18",
                  "frac": "2/11",
                  "rootIdx": 26
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers +11",
                  "shortName": "Philadelphia 76ers +11"
               },
               "wasPrice": [],
               "id": 203825784,
               "name": "Philadelphia 76ers +11",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "4.5",
                  "frac": "7/2",
                  "rootIdx": 151
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets -11",
                  "shortName": "Brooklyn Nets -11"
               },
               "wasPrice": [],
               "id": 203825779,
               "name": "Brooklyn Nets -11",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131873,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-11",
         "line": -11.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.1",
                  "frac": "1/10",
                  "rootIdx": 17
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers +14",
                  "shortName": "Philadelphia 76ers +14"
               },
               "wasPrice": [],
               "id": 204605413,
               "name": "Philadelphia 76ers +14",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "6",
                  "frac": "5/1",
                  "rootIdx": 172
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets -14",
                  "shortName": "Brooklyn Nets -14"
               },
               "wasPrice": [],
               "id": 204605411,
               "name": "Brooklyn Nets -14",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70331800,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-14",
         "line": -14.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.12",
                  "frac": "1/8",
                  "rootIdx": 19
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers +13",
                  "shortName": "Philadelphia 76ers +13"
               },
               "wasPrice": [],
               "id": 203841141,
               "name": "Philadelphia 76ers +13",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "5.75",
                  "frac": "19/4",
                  "rootIdx": 169
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets -13",
                  "shortName": "Brooklyn Nets -13"
               },
               "wasPrice": [],
               "id": 203841139,
               "name": "Brooklyn Nets -13",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70138334,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-13",
         "line": -13.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 112.5",
                  "shortName": "Under 112.5"
               },
               "wasPrice": [],
               "id": 203840899,
               "name": "Under 112.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 112.5",
                  "shortName": "Over 112.5"
               },
               "wasPrice": [],
               "id": 203840897,
               "name": "Over 112.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Brooklyn Nets Total Points (Incl. OT)",
            "shortName": "Brooklyn Nets Total Points (Incl. OT)"
         },
         "id": 70138236,
         "name": "Brooklyn Nets Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:A:OU",
         "subtype": "M#112.5",
         "line": 112.5,
         "displayOrder": -445,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.65",
                  "frac": "13/20",
                  "rootIdx": 56
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 230",
                  "shortName": "Under 230"
               },
               "wasPrice": [],
               "id": 203826230,
               "name": "Under 230",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.2",
                  "frac": "6/5",
                  "rootIdx": 99
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 230",
                  "shortName": "Over 230"
               },
               "wasPrice": [],
               "id": 203826226,
               "name": "Over 230",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132089,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#230",
         "line": 230.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.57",
                  "frac": "4/7",
                  "rootIdx": 52
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 231",
                  "shortName": "Under 231"
               },
               "wasPrice": [],
               "id": 203826263,
               "name": "Under 231",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.3",
                  "frac": "13/10",
                  "rootIdx": 103
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 231",
                  "shortName": "Over 231"
               },
               "wasPrice": [],
               "id": 203826250,
               "name": "Over 231",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132094,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#231",
         "line": 231.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.5",
                  "frac": "1/2",
                  "rootIdx": 47
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 232",
                  "shortName": "Under 232"
               },
               "wasPrice": [],
               "id": 203826218,
               "name": "Under 232",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.45",
                  "frac": "29/20",
                  "rootIdx": 107
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 232",
                  "shortName": "Over 232"
               },
               "wasPrice": [],
               "id": 203826210,
               "name": "Over 232",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132078,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#232",
         "line": 232.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.47",
                  "frac": "40/85",
                  "rootIdx": 45
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 233",
                  "shortName": "Under 233"
               },
               "wasPrice": [],
               "id": 203826187,
               "name": "Under 233",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.55",
                  "frac": "31/20",
                  "rootIdx": 109
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 233",
                  "shortName": "Over 233"
               },
               "wasPrice": [],
               "id": 203826181,
               "name": "Over 233",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132062,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#233",
         "line": 233.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.7",
                  "frac": "17/10",
                  "rootIdx": 113
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 234",
                  "shortName": "Over 234"
               },
               "wasPrice": [],
               "id": 203826339,
               "name": "Over 234",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.44",
                  "frac": "4/9",
                  "rootIdx": 43
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 234",
                  "shortName": "Under 234"
               },
               "wasPrice": [],
               "id": 203826345,
               "name": "Under 234",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132134,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#234",
         "line": 234.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.36",
                  "frac": "4/11",
                  "rootIdx": 39
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 235",
                  "shortName": "Under 235"
               },
               "wasPrice": [],
               "id": 203826324,
               "name": "Under 235",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.9",
                  "frac": "19/10",
                  "rootIdx": 118
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 235",
                  "shortName": "Over 235"
               },
               "wasPrice": [],
               "id": 203826312,
               "name": "Over 235",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132119,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#235",
         "line": 235.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.35",
                  "frac": "7/20",
                  "rootIdx": 38
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 236",
                  "shortName": "Under 236"
               },
               "wasPrice": [],
               "id": 203826231,
               "name": "Under 236",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.05",
                  "frac": "41/20",
                  "rootIdx": 121
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 236",
                  "shortName": "Over 236"
               },
               "wasPrice": [],
               "id": 203826223,
               "name": "Over 236",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132081,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#236",
         "line": 236.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.33",
                  "frac": "1/3",
                  "rootIdx": 37
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 237",
                  "shortName": "Under 237"
               },
               "wasPrice": [],
               "id": 203826199,
               "name": "Under 237",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.3",
                  "frac": "23/10",
                  "rootIdx": 126
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 237",
                  "shortName": "Over 237"
               },
               "wasPrice": [],
               "id": 203826194,
               "name": "Over 237",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132070,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#237",
         "line": 237.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3.3",
                  "frac": "23/10",
                  "rootIdx": 126
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 238",
                  "shortName": "Over 238"
               },
               "wasPrice": [],
               "id": 203826297,
               "name": "Over 238",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.3",
                  "frac": "3/10",
                  "rootIdx": 35
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 238",
                  "shortName": "Under 238"
               },
               "wasPrice": [],
               "id": 203826311,
               "name": "Under 238",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132115,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#238",
         "line": 238.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.25",
                  "frac": "1/4",
                  "rootIdx": 32
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 239",
                  "shortName": "Under 239"
               },
               "wasPrice": [],
               "id": 203840903,
               "name": "Under 239",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.75",
                  "frac": "11/4",
                  "rootIdx": 135
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 239",
                  "shortName": "Over 239"
               },
               "wasPrice": [],
               "id": 203840901,
               "name": "Over 239",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70138238,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#239",
         "line": 239.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers -0.5",
                  "shortName": "Philadelphia 76ers -0.5"
               },
               "wasPrice": [],
               "id": 203935819,
               "name": "Philadelphia 76ers -0.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.75",
                  "frac": "3/4",
                  "rootIdx": 63
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets +0.5",
                  "shortName": "Brooklyn Nets +0.5"
               },
               "wasPrice": [],
               "id": 203935817,
               "name": "Brooklyn Nets +0.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "3rd Quarter Spread",
            "shortName": "3rd Quarter Spread"
         },
         "id": 70176658,
         "name": "3rd Quarter Spread",
         "type": "BASKETBALL:P:PSPRD",
         "subtype": "Q3#0.5",
         "line": 0.5,
         "displayOrder": -304,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q3"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "46",
                  "frac": "45/1",
                  "rootIdx": 237
               },
               "pos": {
                  "row": 103,
                  "col": 1
               },
               "names": {
                  "longName": "Caris LeVert 7+",
                  "shortName": "Caris LeVert 7+"
               },
               "wasPrice": [],
               "id": 204141019,
               "name": "Caris LeVert 7+",
               "type": "7",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "22",
                  "frac": "21/1",
                  "rootIdx": 213
               },
               "pos": {
                  "row": 302,
                  "col": 1
               },
               "names": {
                  "longName": "Caris LeVert 6+",
                  "shortName": "Caris LeVert 6+"
               },
               "wasPrice": [],
               "id": 204141017,
               "name": "Caris LeVert 6+",
               "type": "6",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "9",
                  "frac": "8/1",
                  "rootIdx": 192
               },
               "pos": {
                  "row": 202,
                  "col": 1
               },
               "names": {
                  "longName": "Caris LeVert 5+",
                  "shortName": "Caris LeVert 5+"
               },
               "wasPrice": [],
               "id": 204141015,
               "name": "Caris LeVert 5+",
               "type": "5",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4.05",
                  "frac": "61/20",
                  "rootIdx": 141
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Caris LeVert 4+",
                  "shortName": "Caris LeVert 4+"
               },
               "wasPrice": [],
               "id": 204141013,
               "name": "Caris LeVert 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.05",
                  "frac": "21/20",
                  "rootIdx": 88
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Caris LeVert 3+",
                  "shortName": "Caris LeVert 3+"
               },
               "wasPrice": [],
               "id": 204141011,
               "name": "Caris LeVert 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.65",
                  "frac": "13/20",
                  "rootIdx": 56
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "Caris LeVert 2+",
                  "shortName": "Caris LeVert 2+"
               },
               "wasPrice": [],
               "id": 204141009,
               "name": "Caris LeVert 2+",
               "type": "2",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Caris LeVert 3-point field goals (incl. overtime)",
            "shortName": "Caris LeVert 3-point field goals (incl. overtime)"
         },
         "id": 70230336,
         "name": "Caris LeVert 3-point field goals (incl. overtime)",
         "type": "BASKETBALL:FT:3PFG",
         "subtype": "M#pre:playerprops:24750814:996291",
         "displayOrder": -272,
         "columnCount": 1,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "names": {
                  "longName": "Jarrett Allen 13+",
                  "shortName": "Jarrett Allen 13+"
               },
               "wasPrice": [],
               "id": 204140815,
               "name": "Jarrett Allen 13+",
               "type": "13",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "names": {
                  "longName": "Jarrett Allen 12+",
                  "shortName": "Jarrett Allen 12+"
               },
               "wasPrice": [],
               "id": 204140813,
               "name": "Jarrett Allen 12+",
               "type": "12",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.7",
                  "frac": "7/10",
                  "rootIdx": 59
               },
               "names": {
                  "longName": "Jarrett Allen 11+",
                  "shortName": "Jarrett Allen 11+"
               },
               "wasPrice": [],
               "id": 204140811,
               "name": "Jarrett Allen 11+",
               "type": "11",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.4",
                  "frac": "2/5",
                  "rootIdx": 41
               },
               "names": {
                  "longName": "Jarrett Allen 10+",
                  "shortName": "Jarrett Allen 10+"
               },
               "wasPrice": [],
               "id": 204140809,
               "name": "Jarrett Allen 10+",
               "type": "10",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "12",
                  "frac": "11/1",
                  "rootIdx": 200
               },
               "names": {
                  "longName": "Jarrett Allen 18+",
                  "shortName": "Jarrett Allen 18+"
               },
               "wasPrice": [],
               "id": 204140825,
               "name": "Jarrett Allen 18+",
               "type": "18",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4.8",
                  "frac": "19/5",
                  "rootIdx": 157
               },
               "names": {
                  "longName": "Jarrett Allen 16+",
                  "shortName": "Jarrett Allen 16+"
               },
               "wasPrice": [],
               "id": 204140821,
               "name": "Jarrett Allen 16+",
               "type": "16",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "8",
                  "frac": "7/1",
                  "rootIdx": 188
               },
               "names": {
                  "longName": "Jarrett Allen 17+",
                  "shortName": "Jarrett Allen 17+"
               },
               "wasPrice": [],
               "id": 204140823,
               "name": "Jarrett Allen 17+",
               "type": "17",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.35",
                  "frac": "47/20",
                  "rootIdx": 127
               },
               "names": {
                  "longName": "Jarrett Allen 15+",
                  "shortName": "Jarrett Allen 15+"
               },
               "wasPrice": [],
               "id": 204140819,
               "name": "Jarrett Allen 15+",
               "type": "15",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.45",
                  "frac": "29/20",
                  "rootIdx": 107
               },
               "names": {
                  "longName": "Jarrett Allen 14+",
                  "shortName": "Jarrett Allen 14+"
               },
               "wasPrice": [],
               "id": 204140817,
               "name": "Jarrett Allen 14+",
               "type": "14",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Jarrett Allen rebounds (incl. overtime)",
            "shortName": "Jarrett Allen rebounds (incl. overtime)"
         },
         "id": 70230296,
         "name": "Jarrett Allen rebounds (incl. overtime)",
         "type": "BASKETBALL:FT:PLREB",
         "subtype": "M#pre:playerprops:24750814:1124733",
         "displayOrder": -274,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "14",
                  "frac": "13/1",
                  "rootIdx": 204
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Joe Harris 4+",
                  "shortName": "Joe Harris 4+"
               },
               "wasPrice": [],
               "id": 204141361,
               "name": "Joe Harris 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.35",
                  "frac": "47/20",
                  "rootIdx": 127
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "Joe Harris 2+",
                  "shortName": "Joe Harris 2+"
               },
               "wasPrice": [],
               "id": 204141357,
               "name": "Joe Harris 2+",
               "type": "2",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.75",
                  "frac": "3/4",
                  "rootIdx": 63
               },
               "pos": {
                  "row": 101,
                  "col": 1
               },
               "names": {
                  "longName": "Joe Harris 1+",
                  "shortName": "Joe Harris 1+"
               },
               "wasPrice": [],
               "id": 204141355,
               "name": "Joe Harris 1+",
               "type": "1",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "6",
                  "frac": "5/1",
                  "rootIdx": 172
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Joe Harris 3+",
                  "shortName": "Joe Harris 3+"
               },
               "wasPrice": [],
               "id": 204141359,
               "name": "Joe Harris 3+",
               "type": "3",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Joe Harris steals (incl. overtime)",
            "shortName": "Joe Harris steals (incl. overtime)"
         },
         "id": 70230408,
         "name": "Joe Harris steals (incl. overtime)",
         "type": "BASKETBALL:FT:PLSTA",
         "subtype": "M#pre:playerprops:24750814:607950",
         "displayOrder": -270,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3.75",
                  "frac": "11/4",
                  "rootIdx": 135
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers -9",
                  "shortName": "Philadelphia 76ers -9"
               },
               "wasPrice": [],
               "id": 203826013,
               "name": "Philadelphia 76ers -9",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.25",
                  "frac": "1/4",
                  "rootIdx": 32
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets +9",
                  "shortName": "Brooklyn Nets +9"
               },
               "wasPrice": [],
               "id": 203826003,
               "name": "Brooklyn Nets +9",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131973,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#9",
         "line": 9.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "-",
                  "frac": "-",
                  "rootIdx": -1
               },
               "names": {
                  "longName": "{outcomeName}",
                  "shortName": "{outcomeName}"
               },
               "wasPrice": [],
               "id": 204584791,
               "name": "{outcomeName}",
               "type": "25",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "16",
                  "frac": "15/1",
                  "rootIdx": 207
               },
               "names": {
                  "longName": "Jarrett Allen 24+",
                  "shortName": "Jarrett Allen 24+"
               },
               "wasPrice": [],
               "id": 204141245,
               "name": "Jarrett Allen 24+",
               "type": "24",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "8.75",
                  "frac": "31/4",
                  "rootIdx": 191
               },
               "names": {
                  "longName": "Jarrett Allen 22+",
                  "shortName": "Jarrett Allen 22+"
               },
               "wasPrice": [],
               "id": 204141243,
               "name": "Jarrett Allen 22+",
               "type": "22",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "6.25",
                  "frac": "21/4",
                  "rootIdx": 175
               },
               "names": {
                  "longName": "Jarrett Allen 20+",
                  "shortName": "Jarrett Allen 20+"
               },
               "wasPrice": [],
               "id": 204141241,
               "name": "Jarrett Allen 20+",
               "type": "20",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "-",
                  "frac": "-",
                  "rootIdx": -1
               },
               "names": {
                  "longName": "{outcomeName}",
                  "shortName": "{outcomeName}"
               },
               "wasPrice": [],
               "id": 204584779,
               "name": "{outcomeName}",
               "type": "9",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "-",
                  "frac": "-",
                  "rootIdx": -1
               },
               "names": {
                  "longName": "{outcomeName}",
                  "shortName": "{outcomeName}"
               },
               "wasPrice": [],
               "id": 204584777,
               "name": "{outcomeName}",
               "type": "23",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "-",
                  "frac": "-",
                  "rootIdx": -1
               },
               "names": {
                  "longName": "{outcomeName}",
                  "shortName": "{outcomeName}"
               },
               "wasPrice": [],
               "id": 204584775,
               "name": "{outcomeName}",
               "type": "21",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "-",
                  "frac": "-",
                  "rootIdx": -1
               },
               "names": {
                  "longName": "{outcomeName}",
                  "shortName": "{outcomeName}"
               },
               "wasPrice": [],
               "id": 204584789,
               "name": "{outcomeName}",
               "type": "11",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "-",
                  "frac": "-",
                  "rootIdx": -1
               },
               "names": {
                  "longName": "{outcomeName}",
                  "shortName": "{outcomeName}"
               },
               "wasPrice": [],
               "id": 204584787,
               "name": "{outcomeName}",
               "type": "13",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "-",
                  "frac": "-",
                  "rootIdx": -1
               },
               "names": {
                  "longName": "{outcomeName}",
                  "shortName": "{outcomeName}"
               },
               "wasPrice": [],
               "id": 204584785,
               "name": "{outcomeName}",
               "type": "15",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4.5",
                  "frac": "7/2",
                  "rootIdx": 151
               },
               "names": {
                  "longName": "Jarrett Allen 18+",
                  "shortName": "Jarrett Allen 18+"
               },
               "wasPrice": [],
               "id": 204141239,
               "name": "Jarrett Allen 18+",
               "type": "18",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "-",
                  "frac": "-",
                  "rootIdx": -1
               },
               "names": {
                  "longName": "{outcomeName}",
                  "shortName": "{outcomeName}"
               },
               "wasPrice": [],
               "id": 204584783,
               "name": "{outcomeName}",
               "type": "17",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.2",
                  "frac": "11/5",
                  "rootIdx": 124
               },
               "names": {
                  "longName": "Jarrett Allen 16+",
                  "shortName": "Jarrett Allen 16+"
               },
               "wasPrice": [],
               "id": 204141237,
               "name": "Jarrett Allen 16+",
               "type": "16",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "-",
                  "frac": "-",
                  "rootIdx": -1
               },
               "names": {
                  "longName": "{outcomeName}",
                  "shortName": "{outcomeName}"
               },
               "wasPrice": [],
               "id": 204584781,
               "name": "{outcomeName}",
               "type": "19",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.35",
                  "frac": "27/20",
                  "rootIdx": 104
               },
               "names": {
                  "longName": "Jarrett Allen 14+",
                  "shortName": "Jarrett Allen 14+"
               },
               "wasPrice": [],
               "id": 204141235,
               "name": "Jarrett Allen 14+",
               "type": "14",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "names": {
                  "longName": "Jarrett Allen 12+",
                  "shortName": "Jarrett Allen 12+"
               },
               "wasPrice": [],
               "id": 204141233,
               "name": "Jarrett Allen 12+",
               "type": "12",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.53",
                  "frac": "8/15",
                  "rootIdx": 50
               },
               "names": {
                  "longName": "Jarrett Allen 10+",
                  "shortName": "Jarrett Allen 10+"
               },
               "wasPrice": [],
               "id": 204141231,
               "name": "Jarrett Allen 10+",
               "type": "10",
               "suspended": true,
               "displayed": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Jarrett Allen points (incl. overtime)",
            "shortName": "Jarrett Allen points (incl. overtime)"
         },
         "id": 70230382,
         "name": "Jarrett Allen points (incl. overtime)",
         "type": "BASKETBALL:FT:PLPOI",
         "subtype": "M#pre:playerprops:24750814:1124733",
         "displayOrder": -275,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "4.8",
                  "frac": "19/5",
                  "rootIdx": 157
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets -11.5",
                  "shortName": "Brooklyn Nets -11.5"
               },
               "wasPrice": [],
               "id": 203828485,
               "name": "Brooklyn Nets -11.5",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.15",
                  "frac": "2/13",
                  "rootIdx": 23
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers +11.5",
                  "shortName": "Philadelphia 76ers +11.5"
               },
               "wasPrice": [],
               "id": 203828487,
               "name": "Philadelphia 76ers +11.5",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70133044,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-11.5",
         "line": -11.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.75",
                  "frac": "3/4",
                  "rootIdx": 63
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers +1.5",
                  "shortName": "Philadelphia 76ers +1.5"
               },
               "wasPrice": [],
               "id": 203825767,
               "name": "Philadelphia 76ers +1.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2",
                  "frac": "1/1",
                  "rootIdx": 83
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets -1.5",
                  "shortName": "Brooklyn Nets -1.5"
               },
               "wasPrice": [],
               "id": 203825754,
               "name": "Brooklyn Nets -1.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131862,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-1.5",
         "line": -1.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.44",
                  "frac": "4/9",
                  "rootIdx": 43
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets +6",
                  "shortName": "Brooklyn Nets +6"
               },
               "wasPrice": [],
               "id": 203826024,
               "name": "Brooklyn Nets +6",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.7",
                  "frac": "17/10",
                  "rootIdx": 113
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers -6",
                  "shortName": "Philadelphia 76ers -6"
               },
               "wasPrice": [],
               "id": 203826029,
               "name": "Philadelphia 76ers -6",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131986,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#6",
         "line": 6.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.5",
                  "frac": "1/2",
                  "rootIdx": 47
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets +5",
                  "shortName": "Brooklyn Nets +5"
               },
               "wasPrice": [],
               "id": 203825946,
               "name": "Brooklyn Nets +5",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.45",
                  "frac": "29/20",
                  "rootIdx": 107
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers -5",
                  "shortName": "Philadelphia 76ers -5"
               },
               "wasPrice": [],
               "id": 203825953,
               "name": "Philadelphia 76ers -5",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131952,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#5",
         "line": 5.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3.3",
                  "frac": "23/10",
                  "rootIdx": 126
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers -8",
                  "shortName": "Philadelphia 76ers -8"
               },
               "wasPrice": [],
               "id": 203825957,
               "name": "Philadelphia 76ers -8",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.3",
                  "frac": "3/10",
                  "rootIdx": 35
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets +8",
                  "shortName": "Brooklyn Nets +8"
               },
               "wasPrice": [],
               "id": 203825954,
               "name": "Brooklyn Nets +8",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131963,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#8",
         "line": 8.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.75",
                  "frac": "3/4",
                  "rootIdx": 63
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 56.5",
                  "shortName": "Under 56.5"
               },
               "wasPrice": [],
               "id": 204405037,
               "name": "Under 56.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 56.5",
                  "shortName": "Over 56.5"
               },
               "wasPrice": [],
               "id": 204405035,
               "name": "Over 56.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "3rd Quarter Total Points",
            "shortName": "3rd Quarter Total Points"
         },
         "id": 70287570,
         "name": "3rd Quarter Total Points",
         "type": "BASKETBALL:P:OU",
         "subtype": "Q3#56.5",
         "line": 56.5,
         "displayOrder": -303,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q3"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3.05",
                  "frac": "41/20",
                  "rootIdx": 121
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers -7",
                  "shortName": "Philadelphia 76ers -7"
               },
               "wasPrice": [],
               "id": 203825974,
               "name": "Philadelphia 76ers -7",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.35",
                  "frac": "7/20",
                  "rootIdx": 38
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets +7",
                  "shortName": "Brooklyn Nets +7"
               },
               "wasPrice": [],
               "id": 203825968,
               "name": "Brooklyn Nets +7",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131971,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#7",
         "line": 7.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.85",
                  "frac": "17/20",
                  "rootIdx": 70
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers",
                  "shortName": "Philadelphia 76ers"
               },
               "wasPrice": [],
               "id": 203826277,
               "name": "Philadelphia 76ers",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets",
                  "shortName": "Brooklyn Nets"
               },
               "wasPrice": [],
               "id": 203826273,
               "name": "Brooklyn Nets",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Money Line (Incl. OT)",
            "shortName": "Money Line (Incl. OT)"
         },
         "id": 70132100,
         "name": "Money Line (Incl. OT)",
         "type": "BASKETBALL:FTOT:ML",
         "subtype": "M",
         "displayOrder": -500,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2",
                  "frac": "1/1",
                  "rootIdx": 83
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers -2",
                  "shortName": "Philadelphia 76ers -2"
               },
               "wasPrice": [],
               "id": 203825925,
               "name": "Philadelphia 76ers -2",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.75",
                  "frac": "3/4",
                  "rootIdx": 63
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets +2",
                  "shortName": "Brooklyn Nets +2"
               },
               "wasPrice": [],
               "id": 203825917,
               "name": "Brooklyn Nets +2",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131936,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#2",
         "line": 2.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.85",
                  "frac": "17/20",
                  "rootIdx": 70
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets +1",
                  "shortName": "Brooklyn Nets +1"
               },
               "wasPrice": [],
               "id": 203825902,
               "name": "Brooklyn Nets +1",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers -1",
                  "shortName": "Philadelphia 76ers -1"
               },
               "wasPrice": [],
               "id": 203825918,
               "name": "Philadelphia 76ers -1",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131933,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#1",
         "line": 1.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2",
                  "frac": "1/1",
                  "rootIdx": 83
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 227.5",
                  "shortName": "Over 227.5"
               },
               "wasPrice": [],
               "id": 203826265,
               "name": "Over 227.5",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.75",
                  "frac": "3/4",
                  "rootIdx": 63
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 227.5",
                  "shortName": "Under 227.5"
               },
               "wasPrice": [],
               "id": 203826276,
               "name": "Under 227.5",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132084,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#227.5",
         "line": 227.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.57",
                  "frac": "4/7",
                  "rootIdx": 52
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets +4",
                  "shortName": "Brooklyn Nets +4"
               },
               "wasPrice": [],
               "id": 203825939,
               "name": "Brooklyn Nets +4",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.3",
                  "frac": "13/10",
                  "rootIdx": 103
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers -4",
                  "shortName": "Philadelphia 76ers -4"
               },
               "wasPrice": [],
               "id": 203825967,
               "name": "Philadelphia 76ers -4",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131946,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#4",
         "line": 4.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false, 
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.22",
                  "frac": "2/9",
                  "rootIdx": 30
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers +9.5",
                  "shortName": "Philadelphia 76ers +9.5"
               },
               "wasPrice": [],
               "id": 203825912,
               "name": "Philadelphia 76ers +9.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.9",
                  "frac": "29/10",
                  "rootIdx": 138
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets -9.5",
                  "shortName": "Brooklyn Nets -9.5"
               },
               "wasPrice": [],
               "id": 203825889,
               "name": "Brooklyn Nets -9.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131926,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-9.5",
         "line": -9.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.7",
                  "frac": "7/10",
                  "rootIdx": 59
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets +3",
                  "shortName": "Brooklyn Nets +3"
               },
               "wasPrice": [],
               "id": 203825924,
               "name": "Brooklyn Nets +3",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.1",
                  "frac": "11/10",
                  "rootIdx": 93
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers -3",
                  "shortName": "Philadelphia 76ers -3"
               },
               "wasPrice": [],
               "id": 203825932,
               "name": "Philadelphia 76ers -3",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131939,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#3",
         "line": 3.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets 0",
                  "shortName": "Brooklyn Nets 0"
               },
               "wasPrice": [],
               "id": 203825847,
               "name": "Brooklyn Nets 0",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers 0",
                  "shortName": "Philadelphia 76ers 0"
               },
               "wasPrice": [],
               "id": 203825859,
               "name": "Philadelphia 76ers 0",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131899,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#0",
         "line": 0.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.25",
                  "frac": "5/4",
                  "rootIdx": 101
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 230.5",
                  "shortName": "Over 230.5"
               },
               "wasPrice": [],
               "id": 203826254,
               "name": "Over 230.5",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.62",
                  "frac": "8/13",
                  "rootIdx": 54
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 230.5",
                  "shortName": "Under 230.5"
               },
               "wasPrice": [],
               "id": 203826266,
               "name": "Under 230.5",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132096,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#230.5",
         "line": 230.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.22",
                  "frac": "2/9",
                  "rootIdx": 30
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 240",
                  "shortName": "Under 240"
               },
               "wasPrice": [],
               "id": 204212427,
               "name": "Under 240",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.9",
                  "frac": "29/10",
                  "rootIdx": 138
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 240",
                  "shortName": "Over 240"
               },
               "wasPrice": [],
               "id": 204212425,
               "name": "Over 240",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70246800,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#240",
         "line": 240.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3.9",
                  "frac": "29/10",
                  "rootIdx": 138
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 241",
                  "shortName": "Over 241"
               },
               "wasPrice": [],
               "id": 204405039,
               "name": "Over 241",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.22",
                  "frac": "2/9",
                  "rootIdx": 30
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 241",
                  "shortName": "Under 241"
               },
               "wasPrice": [],
               "id": 204405041,
               "name": "Under 241",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70287572,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#241",
         "line": 241.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.2",
                  "frac": "6/5",
                  "rootIdx": 99
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers -3.5",
                  "shortName": "Philadelphia 76ers -3.5"
               },
               "wasPrice": [],
               "id": 203825933,
               "name": "Philadelphia 76ers -3.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.65",
                  "frac": "13/20",
                  "rootIdx": 56
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets +3.5",
                  "shortName": "Brooklyn Nets +3.5"
               },
               "wasPrice": [],
               "id": 203825930,
               "name": "Brooklyn Nets +3.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131940,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#3.5",
         "line": 3.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets -1",
                  "shortName": "Brooklyn Nets -1"
               },
               "wasPrice": [],
               "id": 203825722,
               "name": "Brooklyn Nets -1",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers +1",
                  "shortName": "Philadelphia 76ers +1"
               },
               "wasPrice": [],
               "id": 203825738,
               "name": "Philadelphia 76ers +1",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131858,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-1",
         "line": -1.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.35",
                  "frac": "7/20",
                  "rootIdx": 38
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 216.5",
                  "shortName": "Over 216.5"
               },
               "wasPrice": [],
               "id": 203826147,
               "name": "Over 216.5",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.05",
                  "frac": "41/20",
                  "rootIdx": 121
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 216.5",
                  "shortName": "Under 216.5"
               },
               "wasPrice": [],
               "id": 203826153,
               "name": "Under 216.5",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132041,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#216.5",
         "line": 216.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 58.5",
                  "shortName": "Under 58.5"
               },
               "wasPrice": [],
               "id": 203933837,
               "name": "Under 58.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 58.5",
                  "shortName": "Over 58.5"
               },
               "wasPrice": [],
               "id": 203933835,
               "name": "Over 58.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Half Philadelphia 76ers Total Points",
            "shortName": "1st Half Philadelphia 76ers Total Points"
         },
         "id": 70175744,
         "name": "1st Half Philadelphia 76ers Total Points",
         "type": "BASKETBALL:P:B:OU",
         "subtype": "H1#58.5",
         "line": 58.5,
         "displayOrder": -350,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "H1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.85",
                  "frac": "17/20",
                  "rootIdx": 70
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 116.5",
                  "shortName": "Over 116.5"
               },
               "wasPrice": [],
               "id": 204476491,
               "name": "Over 116.5",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.85",
                  "frac": "17/20",
                  "rootIdx": 70
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 116.5",
                  "shortName": "Under 116.5"
               },
               "wasPrice": [],
               "id": 204476493,
               "name": "Under 116.5",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Half Total Points",
            "shortName": "1st Half Total Points"
         },
         "id": 70295522,
         "name": "1st Half Total Points",
         "type": "BASKETBALL:P:OU",
         "subtype": "H1#116.5",
         "line": 116.5,
         "displayOrder": -365,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "H1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.25",
                  "frac": "1/4",
                  "rootIdx": 32
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 239.5",
                  "shortName": "Under 239.5"
               },
               "wasPrice": [],
               "id": 204212347,
               "name": "Under 239.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.8",
                  "frac": "14/5",
                  "rootIdx": 136
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 239.5",
                  "shortName": "Over 239.5"
               },
               "wasPrice": [],
               "id": 204212342,
               "name": "Over 239.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70246790,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#239.5",
         "line": 239.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.05",
                  "frac": "21/20",
                  "rootIdx": 88
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 28.5",
                  "shortName": "Under 28.5"
               },
               "wasPrice": [],
               "id": 203826031,
               "name": "Under 28.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.7",
                  "frac": "7/10",
                  "rootIdx": 59
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 28.5",
                  "shortName": "Over 28.5"
               },
               "wasPrice": [],
               "id": 203826025,
               "name": "Over 28.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Quarter Philadelphia 76ers Total Points",
            "shortName": "1st Quarter Philadelphia 76ers Total Points"
         },
         "id": 70131987,
         "name": "1st Quarter Philadelphia 76ers Total Points",
         "type": "BASKETBALL:P:B:OU",
         "subtype": "Q1#28.5",
         "line": 28.5,
         "displayOrder": -317,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q1"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "19",
                  "frac": "18/1",
                  "rootIdx": 210
               },
               "pos": {
                  "row": 203,
                  "col": 1
               },
               "names": {
                  "longName": "Joel Embiid 8+",
                  "shortName": "Joel Embiid 8+"
               },
               "wasPrice": [],
               "id": 204141075,
               "name": "Joel Embiid 8+",
               "type": "8",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "11",
                  "frac": "10/1",
                  "rootIdx": 198
               },
               "pos": {
                  "row": 103,
                  "col": 1
               },
               "names": {
                  "longName": "Joel Embiid 7+",
                  "shortName": "Joel Embiid 7+"
               },
               "wasPrice": [],
               "id": 204141073,
               "name": "Joel Embiid 7+",
               "type": "7",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "6.2",
                  "frac": "26/5",
                  "rootIdx": 174
               },
               "pos": {
                  "row": 302,
                  "col": 1
               },
               "names": {
                  "longName": "Joel Embiid 6+",
                  "shortName": "Joel Embiid 6+"
               },
               "wasPrice": [],
               "id": 204141071,
               "name": "Joel Embiid 6+",
               "type": "6",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.3",
                  "frac": "13/10",
                  "rootIdx": 103
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Joel Embiid 4+",
                  "shortName": "Joel Embiid 4+"
               },
               "wasPrice": [],
               "id": 204141067,
               "name": "Joel Embiid 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.45",
                  "frac": "9/20",
                  "rootIdx": 44
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Joel Embiid 3+",
                  "shortName": "Joel Embiid 3+"
               },
               "wasPrice": [],
               "id": 204141065,
               "name": "Joel Embiid 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.45",
                  "frac": "49/20",
                  "rootIdx": 129
               },
               "pos": {
                  "row": 202,
                  "col": 1
               },
               "names": {
                  "longName": "Joel Embiid 5+",
                  "shortName": "Joel Embiid 5+"
               },
               "wasPrice": [],
               "id": 204141069,
               "name": "Joel Embiid 5+",
               "type": "5",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Joel Embiid assists (incl. overtime)",
            "shortName": "Joel Embiid assists (incl. overtime)"
         },
         "id": 70230346,
         "name": "Joel Embiid assists (incl. overtime)",
         "type": "BASKETBALL:FT:PLASS",
         "subtype": "M#pre:playerprops:24750814:607528",
         "displayOrder": -273,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "-",
                  "frac": "-",
                  "rootIdx": -1
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 210",
                  "shortName": "Over 210"
               },
               "wasPrice": [],
               "id": 203844469,
               "name": "Over 210",
               "type": "Over",
               "suspended": true,
               "displayed": false
            },
            {
               "odds": {
                  "dec": "-",
                  "frac": "-",
                  "rootIdx": -1
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 210",
                  "shortName": "Under 210"
               },
               "wasPrice": [],
               "id": 203844471,
               "name": "Under 210",
               "type": "Under",
               "suspended": true,
               "displayed": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70139246,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#210",
         "line": 210.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.22",
                  "frac": "2/9",
                  "rootIdx": 30
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 211",
                  "shortName": "Over 211"
               },
               "wasPrice": [],
               "id": 203826041,
               "name": "Over 211",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.9",
                  "frac": "29/10",
                  "rootIdx": 138
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 211",
                  "shortName": "Under 211"
               },
               "wasPrice": [],
               "id": 203826047,
               "name": "Under 211",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70131994,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#211",
         "line": 211.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3.15",
                  "frac": "43/20",
                  "rootIdx": 123
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets -7",
                  "shortName": "Brooklyn Nets -7"
               },
               "wasPrice": [],
               "id": 203825848,
               "name": "Brooklyn Nets -7",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.33",
                  "frac": "1/3",
                  "rootIdx": 37
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers +7",
                  "shortName": "Philadelphia 76ers +7"
               },
               "wasPrice": [],
               "id": 203825855,
               "name": "Philadelphia 76ers +7",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131903,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-7",
         "line": -7.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.22",
                  "frac": "2/9",
                  "rootIdx": 30
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 212",
                  "shortName": "Over 212"
               },
               "wasPrice": [],
               "id": 203826050,
               "name": "Over 212",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.9",
                  "frac": "29/10",
                  "rootIdx": 138
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 212",
                  "shortName": "Under 212"
               },
               "wasPrice": [],
               "id": 203826055,
               "name": "Under 212",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132002,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#212",
         "line": 212.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.85",
                  "frac": "37/20",
                  "rootIdx": 116
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets -6",
                  "shortName": "Brooklyn Nets -6"
               },
               "wasPrice": [],
               "id": 203825809,
               "name": "Brooklyn Nets -6",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.4",
                  "frac": "2/5",
                  "rootIdx": 41
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers +6",
                  "shortName": "Philadelphia 76ers +6"
               },
               "wasPrice": [],
               "id": 203825825,
               "name": "Philadelphia 76ers +6",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131885,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-6",
         "line": -6.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.22",
                  "frac": "2/9",
                  "rootIdx": 30
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 213",
                  "shortName": "Over 213"
               },
               "wasPrice": [],
               "id": 203826074,
               "name": "Over 213",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.9",
                  "frac": "29/10",
                  "rootIdx": 138
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 213",
                  "shortName": "Under 213"
               },
               "wasPrice": [],
               "id": 203826086,
               "name": "Under 213",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132012,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#213",
         "line": 213.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3.75",
                  "frac": "11/4",
                  "rootIdx": 135
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 214",
                  "shortName": "Under 214"
               },
               "wasPrice": [],
               "id": 203826089,
               "name": "Under 214",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.25",
                  "frac": "1/4",
                  "rootIdx": 32
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 214",
                  "shortName": "Over 214"
               },
               "wasPrice": [],
               "id": 203826084,
               "name": "Over 214",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132015,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#214",
         "line": 214.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.22",
                  "frac": "2/9",
                  "rootIdx": 30
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers +9",
                  "shortName": "Philadelphia 76ers +9"
               },
               "wasPrice": [],
               "id": 203825869,
               "name": "Philadelphia 76ers +9",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.9",
                  "frac": "29/10",
                  "rootIdx": 138
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets -9",
                  "shortName": "Brooklyn Nets -9"
               },
               "wasPrice": [],
               "id": 203825867,
               "name": "Brooklyn Nets -9",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131909,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-9",
         "line": -9.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.28",
                  "frac": "2/7",
                  "rootIdx": 34
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers +8",
                  "shortName": "Philadelphia 76ers +8"
               },
               "wasPrice": [],
               "id": 203825921,
               "name": "Philadelphia 76ers +8",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.5",
                  "frac": "5/2",
                  "rootIdx": 130
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets -8",
                  "shortName": "Brooklyn Nets -8"
               },
               "wasPrice": [],
               "id": 203825907,
               "name": "Brooklyn Nets -8",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131934,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-8",
         "line": -8.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.28",
                  "frac": "2/7",
                  "rootIdx": 34
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 215",
                  "shortName": "Over 215"
               },
               "wasPrice": [],
               "id": 203826158,
               "name": "Over 215",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.5",
                  "frac": "5/2",
                  "rootIdx": 130
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 215",
                  "shortName": "Under 215"
               },
               "wasPrice": [],
               "id": 203826168,
               "name": "Under 215",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132047,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#215",
         "line": 215.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "5.3",
                  "frac": "43/10",
                  "rootIdx": 164
               },
               "names": {
                  "longName": "Joe Harris 23+",
                  "shortName": "Joe Harris 23+"
               },
               "wasPrice": [],
               "id": 204141135,
               "name": "Joe Harris 23+",
               "type": "23",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.38",
                  "frac": "11/8",
                  "rootIdx": 105
               },
               "names": {
                  "longName": "Joe Harris 17+",
                  "shortName": "Joe Harris 17+"
               },
               "wasPrice": [],
               "id": 204141129,
               "name": "Joe Harris 17+",
               "type": "17",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.8",
                  "frac": "14/5",
                  "rootIdx": 136
               },
               "names": {
                  "longName": "Joe Harris 21+",
                  "shortName": "Joe Harris 21+"
               },
               "wasPrice": [],
               "id": 204141133,
               "name": "Joe Harris 21+",
               "type": "21",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.75",
                  "frac": "7/4",
                  "rootIdx": 114
               },
               "names": {
                  "longName": "Joe Harris 19+",
                  "shortName": "Joe Harris 19+"
               },
               "wasPrice": [],
               "id": 204141131,
               "name": "Joe Harris 19+",
               "type": "19",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.1",
                  "frac": "11/10",
                  "rootIdx": 93
               },
               "names": {
                  "longName": "Joe Harris 15+",
                  "shortName": "Joe Harris 15+"
               },
               "wasPrice": [],
               "id": 204141127,
               "name": "Joe Harris 15+",
               "type": "15",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.87",
                  "frac": "87/100",
                  "rootIdx": 71
               },
               "names": {
                  "longName": "Joe Harris 13+",
                  "shortName": "Joe Harris 13+"
               },
               "wasPrice": [],
               "id": 204141125,
               "name": "Joe Harris 13+",
               "type": "13",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.28",
                  "frac": "2/7",
                  "rootIdx": 34
               },
               "names": {
                  "longName": "Joe Harris 11+",
                  "shortName": "Joe Harris 11+"
               },
               "wasPrice": [],
               "id": 204141123,
               "name": "Joe Harris 11+",
               "type": "11",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "9.75",
                  "frac": "35/4",
                  "rootIdx": 195
               },
               "names": {
                  "longName": "Joe Harris 27+",
                  "shortName": "Joe Harris 27+"
               },
               "wasPrice": [],
               "id": 204141139,
               "name": "Joe Harris 27+",
               "type": "27",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "7.25",
                  "frac": "25/4",
                  "rootIdx": 185
               },
               "names": {
                  "longName": "Joe Harris 25+",
                  "shortName": "Joe Harris 25+"
               },
               "wasPrice": [],
               "id": 204141137,
               "name": "Joe Harris 25+",
               "type": "25",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Joe Harris points (incl. overtime)",
            "shortName": "Joe Harris points (incl. overtime)"
         },
         "id": 70230360,
         "name": "Joe Harris points (incl. overtime)",
         "type": "BASKETBALL:FT:PLPOI",
         "subtype": "M#pre:playerprops:24750814:607950",
         "displayOrder": -275,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 113.5",
                  "shortName": "Under 113.5"
               },
               "wasPrice": [],
               "id": 204405045,
               "name": "Under 113.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 113.5",
                  "shortName": "Over 113.5"
               },
               "wasPrice": [],
               "id": 204405043,
               "name": "Over 113.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Brooklyn Nets Total Points (Incl. OT)",
            "shortName": "Brooklyn Nets Total Points (Incl. OT)"
         },
         "id": 70287574,
         "name": "Brooklyn Nets Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:A:OU",
         "subtype": "M#113.5",
         "line": 113.5,
         "displayOrder": -445,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.2",
                  "frac": "6/5",
                  "rootIdx": 99
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets -3",
                  "shortName": "Brooklyn Nets -3"
               },
               "wasPrice": [],
               "id": 203825716,
               "name": "Brooklyn Nets -3",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.65",
                  "frac": "13/20",
                  "rootIdx": 56
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers +3",
                  "shortName": "Philadelphia 76ers +3"
               },
               "wasPrice": [],
               "id": 203825736,
               "name": "Philadelphia 76ers +3",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131863,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-3",
         "line": -3.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.33",
                  "frac": "1/3",
                  "rootIdx": 37
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 216",
                  "shortName": "Over 216"
               },
               "wasPrice": [],
               "id": 203826112,
               "name": "Over 216",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.3",
                  "frac": "23/10",
                  "rootIdx": 126
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 216",
                  "shortName": "Under 216"
               },
               "wasPrice": [],
               "id": 203826125,
               "name": "Under 216",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132025,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#216",
         "line": 216.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "5.4",
                  "frac": "22/5",
                  "rootIdx": 165
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Kyrie Irving 4+",
                  "shortName": "Kyrie Irving 4+"
               },
               "wasPrice": [],
               "id": 204141277,
               "name": "Kyrie Irving 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4.25",
                  "frac": "13/4",
                  "rootIdx": 145
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Kyrie Irving 3+",
                  "shortName": "Kyrie Irving 3+"
               },
               "wasPrice": [],
               "id": 204141275,
               "name": "Kyrie Irving 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "Kyrie Irving 2+",
                  "shortName": "Kyrie Irving 2+"
               },
               "wasPrice": [],
               "id": 204141273,
               "name": "Kyrie Irving 2+",
               "type": "2",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.22",
                  "frac": "2/9",
                  "rootIdx": 30
               },
               "pos": {
                  "row": 101,
                  "col": 1
               },
               "names": {
                  "longName": "Kyrie Irving 1+",
                  "shortName": "Kyrie Irving 1+"
               },
               "wasPrice": [],
               "id": 204141271,
               "name": "Kyrie Irving 1+",
               "type": "1",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Kyrie Irving steals (incl. overtime)",
            "shortName": "Kyrie Irving steals (incl. overtime)"
         },
         "id": 70230390,
         "name": "Kyrie Irving steals (incl. overtime)",
         "type": "BASKETBALL:FT:PLSTA",
         "subtype": "M#pre:playerprops:24750814:608268",
         "displayOrder": -270,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.72",
                  "frac": "8/11",
                  "rootIdx": 60
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers +2",
                  "shortName": "Philadelphia 76ers +2"
               },
               "wasPrice": [],
               "id": 203825752,
               "name": "Philadelphia 76ers +2",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.05",
                  "frac": "21/20",
                  "rootIdx": 88
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets -2",
                  "shortName": "Brooklyn Nets -2"
               },
               "wasPrice": [],
               "id": 203825735,
               "name": "Brooklyn Nets -2",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131865,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-2",
         "line": -2.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3.05",
                  "frac": "41/20",
                  "rootIdx": 121
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 217",
                  "shortName": "Under 217"
               },
               "wasPrice": [],
               "id": 203826061,
               "name": "Under 217",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.35",
                  "frac": "7/20",
                  "rootIdx": 38
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 217",
                  "shortName": "Over 217"
               },
               "wasPrice": [],
               "id": 203826056,
               "name": "Over 217",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132006,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#217",
         "line": 217.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.55",
                  "frac": "31/20",
                  "rootIdx": 109
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets -5",
                  "shortName": "Brooklyn Nets -5"
               },
               "wasPrice": [],
               "id": 203825819,
               "name": "Brooklyn Nets -5",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.47",
                  "frac": "40/85",
                  "rootIdx": 45
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers +5",
                  "shortName": "Philadelphia 76ers +5"
               },
               "wasPrice": [],
               "id": 203825831,
               "name": "Philadelphia 76ers +5",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131894,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-5",
         "line": -5.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.36",
                  "frac": "4/11",
                  "rootIdx": 39
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 218",
                  "shortName": "Over 218"
               },
               "wasPrice": [],
               "id": 203826146,
               "name": "Over 218",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.9",
                  "frac": "19/10",
                  "rootIdx": 118
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 218",
                  "shortName": "Under 218"
               },
               "wasPrice": [],
               "id": 203826159,
               "name": "Under 218",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132038,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#218",
         "line": 218.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.35",
                  "frac": "27/20",
                  "rootIdx": 104
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets -4",
                  "shortName": "Brooklyn Nets -4"
               },
               "wasPrice": [],
               "id": 203825812,
               "name": "Brooklyn Nets -4",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.55",
                  "frac": "11/20",
                  "rootIdx": 51
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers +4",
                  "shortName": "Philadelphia 76ers +4"
               },
               "wasPrice": [],
               "id": 203825827,
               "name": "Philadelphia 76ers +4",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131895,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-4",
         "line": -4.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.44",
                  "frac": "4/9",
                  "rootIdx": 43
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 219",
                  "shortName": "Over 219"
               },
               "wasPrice": [],
               "id": 203826192,
               "name": "Over 219",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.65",
                  "frac": "33/20",
                  "rootIdx": 112
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 219",
                  "shortName": "Under 219"
               },
               "wasPrice": [],
               "id": 203826198,
               "name": "Under 219",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132066,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#219",
         "line": 219.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.85",
                  "frac": "17/20",
                  "rootIdx": 70
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers",
                  "shortName": "Philadelphia 76ers"
               },
               "wasPrice": [],
               "id": 203826008,
               "name": "Philadelphia 76ers",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.85",
                  "frac": "17/20",
                  "rootIdx": 70
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets",
                  "shortName": "Brooklyn Nets"
               },
               "wasPrice": [],
               "id": 203826001,
               "name": "Brooklyn Nets",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Quarter Money Line",
            "shortName": "1st Quarter Money Line"
         },
         "id": 70131983,
         "name": "1st Quarter Money Line",
         "type": "BASKETBALL:P:DNB",
         "subtype": "Q1",
         "displayOrder": -325,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.85",
                  "frac": "17/20",
                  "rootIdx": 70
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets",
                  "shortName": "Brooklyn Nets"
               },
               "wasPrice": [],
               "id": 203935813,
               "name": "Brooklyn Nets",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.85",
                  "frac": "17/20",
                  "rootIdx": 70
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers",
                  "shortName": "Philadelphia 76ers"
               },
               "wasPrice": [],
               "id": 203935815,
               "name": "Philadelphia 76ers",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "2nd Quarter Money Line",
            "shortName": "2nd Quarter Money Line"
         },
         "id": 70176656,
         "name": "2nd Quarter Money Line",
         "type": "BASKETBALL:P:DNB",
         "subtype": "Q2",
         "displayOrder": -315,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q2"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.1",
                  "frac": "11/10",
                  "rootIdx": 93
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets -2.5",
                  "shortName": "Brooklyn Nets -2.5"
               },
               "wasPrice": [],
               "id": 203825808,
               "name": "Brooklyn Nets -2.5",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.7",
                  "frac": "7/10",
                  "rootIdx": 59
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers +2.5",
                  "shortName": "Philadelphia 76ers +2.5"
               },
               "wasPrice": [],
               "id": 203825824,
               "name": "Philadelphia 76ers +2.5",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131892,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-2.5",
         "line": -2.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.2",
                  "frac": "1/5",
                  "rootIdx": 28
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets +10.5",
                  "shortName": "Brooklyn Nets +10.5"
               },
               "wasPrice": [],
               "id": 203825910,
               "name": "Brooklyn Nets +10.5",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "4.3",
                  "frac": "33/10",
                  "rootIdx": 146
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers -10.5",
                  "shortName": "Philadelphia 76ers -10.5"
               },
               "wasPrice": [],
               "id": 203825922,
               "name": "Philadelphia 76ers -10.5",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131930,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#10.5",
         "line": 10.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.85",
                  "frac": "17/20",
                  "rootIdx": 70
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers",
                  "shortName": "Philadelphia 76ers"
               },
               "wasPrice": [],
               "id": 203935823,
               "name": "Philadelphia 76ers",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.85",
                  "frac": "17/20",
                  "rootIdx": 70
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets",
                  "shortName": "Brooklyn Nets"
               },
               "wasPrice": [],
               "id": 203935821,
               "name": "Brooklyn Nets",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "3rd Quarter Money Line",
            "shortName": "3rd Quarter Money Line"
         },
         "id": 70176660,
         "name": "3rd Quarter Money Line",
         "type": "BASKETBALL:P:DNB",
         "subtype": "Q3",
         "displayOrder": -305,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q3"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.85",
                  "frac": "17/20",
                  "rootIdx": 70
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers",
                  "shortName": "Philadelphia 76ers"
               },
               "wasPrice": [],
               "id": 203935809,
               "name": "Philadelphia 76ers",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.85",
                  "frac": "17/20",
                  "rootIdx": 70
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets",
                  "shortName": "Brooklyn Nets"
               },
               "wasPrice": [],
               "id": 203935811,
               "name": "Brooklyn Nets",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "4th Quarter Money Line",
            "shortName": "4th Quarter Money Line"
         },
         "id": 70176654,
         "name": "4th Quarter Money Line",
         "type": "BASKETBALL:P:DNB",
         "subtype": "Q4",
         "displayOrder": -295,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q4"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 55.5",
                  "shortName": "Over 55.5"
               },
               "wasPrice": [],
               "id": 203935807,
               "name": "Over 55.5",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 55.5",
                  "shortName": "Under 55.5"
               },
               "wasPrice": [],
               "id": 203935805,
               "name": "Under 55.5",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "3rd Quarter Total Points",
            "shortName": "3rd Quarter Total Points"
         },
         "id": 70176652,
         "name": "3rd Quarter Total Points",
         "type": "BASKETBALL:P:OU",
         "subtype": "Q3#55.5",
         "line": 55.5,
         "displayOrder": -303,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q3"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.8",
                  "frac": "9/5",
                  "rootIdx": 115
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Seth Curry 4+",
                  "shortName": "Seth Curry 4+"
               },
               "wasPrice": [],
               "id": 204140897,
               "name": "Seth Curry 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.75",
                  "frac": "3/4",
                  "rootIdx": 63
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Seth Curry 3+",
                  "shortName": "Seth Curry 3+"
               },
               "wasPrice": [],
               "id": 204140895,
               "name": "Seth Curry 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.42",
                  "frac": "21/50",
                  "rootIdx": 42
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "Seth Curry 2+",
                  "shortName": "Seth Curry 2+"
               },
               "wasPrice": [],
               "id": 204140893,
               "name": "Seth Curry 2+",
               "type": "2",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "24",
                  "frac": "23/1",
                  "rootIdx": 215
               },
               "pos": {
                  "row": 103,
                  "col": 1
               },
               "names": {
                  "longName": "Seth Curry 7+",
                  "shortName": "Seth Curry 7+"
               },
               "wasPrice": [],
               "id": 204140903,
               "name": "Seth Curry 7+",
               "type": "7",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "11.5",
                  "frac": "21/2",
                  "rootIdx": 199
               },
               "pos": {
                  "row": 302,
                  "col": 1
               },
               "names": {
                  "longName": "Seth Curry 6+",
                  "shortName": "Seth Curry 6+"
               },
               "wasPrice": [],
               "id": 204140901,
               "name": "Seth Curry 6+",
               "type": "6",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "5.25",
                  "frac": "17/4",
                  "rootIdx": 163
               },
               "pos": {
                  "row": 202,
                  "col": 1
               },
               "names": {
                  "longName": "Seth Curry 5+",
                  "shortName": "Seth Curry 5+"
               },
               "wasPrice": [],
               "id": 204140899,
               "name": "Seth Curry 5+",
               "type": "5",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Seth Curry 3-point field goals (incl. overtime)",
            "shortName": "Seth Curry 3-point field goals (incl. overtime)"
         },
         "id": 70230314,
         "name": "Seth Curry 3-point field goals (incl. overtime)",
         "type": "BASKETBALL:FT:3PFG",
         "subtype": "M#pre:playerprops:24750814:607326",
         "displayOrder": -272,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.05",
                  "frac": "21/20",
                  "rootIdx": 88
               },
               "names": {
                  "longName": "Ben Simmons 9+",
                  "shortName": "Ben Simmons 9+"
               },
               "wasPrice": [],
               "id": 204140963,
               "name": "Ben Simmons 9+",
               "type": "9",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.28",
                  "frac": "2/7",
                  "rootIdx": 34
               },
               "pos": {
                  "row": 103,
                  "col": 1
               },
               "names": {
                  "longName": "Ben Simmons 7+",
                  "shortName": "Ben Simmons 7+"
               },
               "wasPrice": [],
               "id": 204140959,
               "name": "Ben Simmons 7+",
               "type": "7",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "7.75",
                  "frac": "27/4",
                  "rootIdx": 187
               },
               "names": {
                  "longName": "Ben Simmons 13+",
                  "shortName": "Ben Simmons 13+"
               },
               "wasPrice": [],
               "id": 204140957,
               "name": "Ben Simmons 13+",
               "type": "13",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4.85",
                  "frac": "77/20",
                  "rootIdx": 158
               },
               "names": {
                  "longName": "Ben Simmons 12+",
                  "shortName": "Ben Simmons 12+"
               },
               "wasPrice": [],
               "id": 204140955,
               "name": "Ben Simmons 12+",
               "type": "12",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.25",
                  "frac": "9/4",
                  "rootIdx": 125
               },
               "names": {
                  "longName": "Ben Simmons 11+",
                  "shortName": "Ben Simmons 11+"
               },
               "wasPrice": [],
               "id": 204140953,
               "name": "Ben Simmons 11+",
               "type": "11",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.38",
                  "frac": "11/8",
                  "rootIdx": 105
               },
               "names": {
                  "longName": "Ben Simmons 10+",
                  "shortName": "Ben Simmons 10+"
               },
               "wasPrice": [],
               "id": 204140951,
               "name": "Ben Simmons 10+",
               "type": "10",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.57",
                  "frac": "4/7",
                  "rootIdx": 52
               },
               "pos": {
                  "row": 203,
                  "col": 1
               },
               "names": {
                  "longName": "Ben Simmons 8+",
                  "shortName": "Ben Simmons 8+"
               },
               "wasPrice": [],
               "id": 204140961,
               "name": "Ben Simmons 8+",
               "type": "8",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Ben Simmons assists (incl. overtime)",
            "shortName": "Ben Simmons assists (incl. overtime)"
         },
         "id": 70230324,
         "name": "Ben Simmons assists (incl. overtime)",
         "type": "BASKETBALL:FT:PLASS",
         "subtype": "M#pre:playerprops:24750814:996289",
         "displayOrder": -273,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.14",
                  "frac": "1/7",
                  "rootIdx": 21
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers +12.5",
                  "shortName": "Philadelphia 76ers +12.5"
               },
               "wasPrice": [],
               "id": 203841145,
               "name": "Philadelphia 76ers +12.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "5.25",
                  "frac": "17/4",
                  "rootIdx": 163
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets -12.5",
                  "shortName": "Brooklyn Nets -12.5"
               },
               "wasPrice": [],
               "id": 203841143,
               "name": "Brooklyn Nets -12.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70138336,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-12.5",
         "line": -12.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.2",
                  "frac": "1/5",
                  "rootIdx": 28
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers +10",
                  "shortName": "Philadelphia 76ers +10"
               },
               "wasPrice": [],
               "id": 203825772,
               "name": "Philadelphia 76ers +10",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "4.2",
                  "frac": "16/5",
                  "rootIdx": 144
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets -10",
                  "shortName": "Brooklyn Nets -10"
               },
               "wasPrice": [],
               "id": 203825766,
               "name": "Brooklyn Nets -10",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131867,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-10",
         "line": -10.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.72",
                  "frac": "8/11",
                  "rootIdx": 60
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 228.5",
                  "shortName": "Under 228.5"
               },
               "wasPrice": [],
               "id": 203826216,
               "name": "Under 228.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.05",
                  "frac": "21/20",
                  "rootIdx": 88
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 228.5",
                  "shortName": "Over 228.5"
               },
               "wasPrice": [],
               "id": 203826209,
               "name": "Over 228.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132071,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#228.5",
         "line": 228.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "5.3",
                  "frac": "43/10",
                  "rootIdx": 164
               },
               "pos": {
                  "row": 203,
                  "col": 1
               },
               "names": {
                  "longName": "Joe Harris 8+",
                  "shortName": "Joe Harris 8+"
               },
               "wasPrice": [],
               "id": 204140783,
               "name": "Joe Harris 8+",
               "type": "8",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.35",
                  "frac": "27/20",
                  "rootIdx": 104
               },
               "pos": {
                  "row": 302,
                  "col": 1
               },
               "names": {
                  "longName": "Joe Harris 6+",
                  "shortName": "Joe Harris 6+"
               },
               "wasPrice": [],
               "id": 204140779,
               "name": "Joe Harris 6+",
               "type": "6",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.87",
                  "frac": "87/100",
                  "rootIdx": 71
               },
               "pos": {
                  "row": 202,
                  "col": 1
               },
               "names": {
                  "longName": "Joe Harris 5+",
                  "shortName": "Joe Harris 5+"
               },
               "wasPrice": [],
               "id": 204140777,
               "name": "Joe Harris 5+",
               "type": "5",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.78",
                  "frac": "39/50",
                  "rootIdx": 65
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Joe Harris 4+",
                  "shortName": "Joe Harris 4+"
               },
               "wasPrice": [],
               "id": 204140775,
               "name": "Joe Harris 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "20",
                  "frac": "19/1",
                  "rootIdx": 211
               },
               "names": {
                  "longName": "Joe Harris 10+",
                  "shortName": "Joe Harris 10+"
               },
               "wasPrice": [],
               "id": 204140773,
               "name": "Joe Harris 10+",
               "type": "10",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.35",
                  "frac": "47/20",
                  "rootIdx": 127
               },
               "pos": {
                  "row": 103,
                  "col": 1
               },
               "names": {
                  "longName": "Joe Harris 7+",
                  "shortName": "Joe Harris 7+"
               },
               "wasPrice": [],
               "id": 204140781,
               "name": "Joe Harris 7+",
               "type": "7",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "32",
                  "frac": "31/1",
                  "rootIdx": 223
               },
               "names": {
                  "longName": "Joe Harris 11+",
                  "shortName": "Joe Harris 11+"
               },
               "wasPrice": [],
               "id": 204587839,
               "name": "Joe Harris 11+",
               "type": "11",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.35",
                  "frac": "7/20",
                  "rootIdx": 38
               },
               "names": {
                  "longName": "Joe Harris 3+",
                  "shortName": "Joe Harris 3+"
               },
               "wasPrice": [],
               "id": 204587841,
               "name": "Joe Harris 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "9.25",
                  "frac": "33/4",
                  "rootIdx": 193
               },
               "names": {
                  "longName": "Joe Harris 9+",
                  "shortName": "Joe Harris 9+"
               },
               "wasPrice": [],
               "id": 204140785,
               "name": "Joe Harris 9+",
               "type": "9",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Joe Harris rebounds (incl. overtime)",
            "shortName": "Joe Harris rebounds (incl. overtime)"
         },
         "id": 70230290,
         "name": "Joe Harris rebounds (incl. overtime)",
         "type": "BASKETBALL:FT:PLREB",
         "subtype": "M#pre:playerprops:24750814:607950",
         "displayOrder": -274,
         "columnCount": 1,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers -0.5",
                  "shortName": "Philadelphia 76ers -0.5"
               },
               "wasPrice": [],
               "id": 203832337,
               "name": "Philadelphia 76ers -0.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.75",
                  "frac": "3/4",
                  "rootIdx": 63
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets +0.5",
                  "shortName": "Brooklyn Nets +0.5"
               },
               "wasPrice": [],
               "id": 203832335,
               "name": "Brooklyn Nets +0.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Half Spread",
            "shortName": "1st Half Spread"
         },
         "id": 70134672,
         "name": "1st Half Spread",
         "type": "BASKETBALL:P:AHCP",
         "subtype": "H1#0.5",
         "line": 0.5,
         "displayOrder": -370,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "H1"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.1",
                  "frac": "11/10",
                  "rootIdx": 93
               },
               "pos": {
                  "row": 302,
                  "col": 1
               },
               "names": {
                  "longName": "Caris LeVert 6+",
                  "shortName": "Caris LeVert 6+"
               },
               "wasPrice": [],
               "id": 204140869,
               "name": "Caris LeVert 6+",
               "type": "6",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.35",
                  "frac": "47/20",
                  "rootIdx": 127
               },
               "names": {
                  "longName": "Caris LeVert 9+",
                  "shortName": "Caris LeVert 9+"
               },
               "wasPrice": [],
               "id": 204140875,
               "name": "Caris LeVert 9+",
               "type": "9",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.4",
                  "frac": "7/5",
                  "rootIdx": 106
               },
               "pos": {
                  "row": 203,
                  "col": 1
               },
               "names": {
                  "longName": "Caris LeVert 8+",
                  "shortName": "Caris LeVert 8+"
               },
               "wasPrice": [],
               "id": 204140873,
               "name": "Caris LeVert 8+",
               "type": "8",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.2",
                  "frac": "6/5",
                  "rootIdx": 99
               },
               "pos": {
                  "row": 103,
                  "col": 1
               },
               "names": {
                  "longName": "Caris LeVert 7+",
                  "shortName": "Caris LeVert 7+"
               },
               "wasPrice": [],
               "id": 204140871,
               "name": "Caris LeVert 7+",
               "type": "7",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.42",
                  "frac": "21/50",
                  "rootIdx": 42
               },
               "pos": {
                  "row": 202,
                  "col": 1
               },
               "names": {
                  "longName": "Caris LeVert 5+",
                  "shortName": "Caris LeVert 5+"
               },
               "wasPrice": [],
               "id": 204140867,
               "name": "Caris LeVert 5+",
               "type": "5",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.18",
                  "frac": "2/11",
                  "rootIdx": 26
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Caris LeVert 4+",
                  "shortName": "Caris LeVert 4+"
               },
               "wasPrice": [],
               "id": 204140865,
               "name": "Caris LeVert 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "5",
                  "frac": "4/1",
                  "rootIdx": 160
               },
               "names": {
                  "longName": "Caris LeVert 10+",
                  "shortName": "Caris LeVert 10+"
               },
               "wasPrice": [],
               "id": 204140863,
               "name": "Caris LeVert 10+",
               "type": "10",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Caris LeVert assists (incl. overtime)",
            "shortName": "Caris LeVert assists (incl. overtime)"
         },
         "id": 70230306,
         "name": "Caris LeVert assists (incl. overtime)",
         "type": "BASKETBALL:FT:PLASS",
         "subtype": "M#pre:playerprops:24750814:996291",
         "displayOrder": -273,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "5.4",
                  "frac": "22/5",
                  "rootIdx": 165
               },
               "pos": {
                  "row": 2,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers 6-10",
                  "shortName": "Philadelphia 76ers 6-10"
               },
               "wasPrice": [],
               "id": 203826370,
               "name": "Philadelphia 76ers 6-10",
               "type": "AT 6-10",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "11",
                  "frac": "10/1",
                  "rootIdx": 198
               },
               "pos": {
                  "row": 4,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers 16-20",
                  "shortName": "Philadelphia 76ers 16-20"
               },
               "wasPrice": [],
               "id": 203826353,
               "name": "Philadelphia 76ers 16-20",
               "type": "AT 16-20",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "11.5",
                  "frac": "21/2",
                  "rootIdx": 199
               },
               "pos": {
                  "row": 4,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets 16-20",
                  "shortName": "Brooklyn Nets 16-20"
               },
               "wasPrice": [],
               "id": 203826377,
               "name": "Brooklyn Nets 16-20",
               "type": "HT 16-20",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "5.5",
                  "frac": "9/2",
                  "rootIdx": 166
               },
               "pos": {
                  "row": 2,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets 6-10",
                  "shortName": "Brooklyn Nets 6-10"
               },
               "wasPrice": [],
               "id": 203826383,
               "name": "Brooklyn Nets 6-10",
               "type": "HT 6-10",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "5.4",
                  "frac": "22/5",
                  "rootIdx": 165
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets 1-5",
                  "shortName": "Brooklyn Nets 1-5"
               },
               "wasPrice": [],
               "id": 203826375,
               "name": "Brooklyn Nets 1-5",
               "type": "HT 1-5",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "18",
                  "frac": "17/1",
                  "rootIdx": 209
               },
               "pos": {
                  "row": 5,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets 21-25",
                  "shortName": "Brooklyn Nets 21-25"
               },
               "wasPrice": [],
               "id": 203826379,
               "name": "Brooklyn Nets 21-25",
               "type": "HT 21-25",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "18",
                  "frac": "17/1",
                  "rootIdx": 209
               },
               "pos": {
                  "row": 5,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers 21-25",
                  "shortName": "Philadelphia 76ers 21-25"
               },
               "wasPrice": [],
               "id": 203826358,
               "name": "Philadelphia 76ers 21-25",
               "type": "AT 21-25",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "5.3",
                  "frac": "43/10",
                  "rootIdx": 164
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers 1-5",
                  "shortName": "Philadelphia 76ers 1-5"
               },
               "wasPrice": [],
               "id": 203826347,
               "name": "Philadelphia 76ers 1-5",
               "type": "AT 1-5",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "8.5",
                  "frac": "15/2",
                  "rootIdx": 190
               },
               "pos": {
                  "row": 3,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers 11-15",
                  "shortName": "Philadelphia 76ers 11-15"
               },
               "wasPrice": [],
               "id": 203826333,
               "name": "Philadelphia 76ers 11-15",
               "type": "AT 11-15",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "22",
                  "frac": "21/1",
                  "rootIdx": 213
               },
               "pos": {
                  "row": 6,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers 26+",
                  "shortName": "Philadelphia 76ers 26+"
               },
               "wasPrice": [],
               "id": 203826364,
               "name": "Philadelphia 76ers 26+",
               "type": "AT > 25",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "8.5",
                  "frac": "15/2",
                  "rootIdx": 190
               },
               "pos": {
                  "row": 3,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets 11-15",
                  "shortName": "Brooklyn Nets 11-15"
               },
               "wasPrice": [],
               "id": 203826373,
               "name": "Brooklyn Nets 11-15",
               "type": "HT 11-15",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "23",
                  "frac": "22/1",
                  "rootIdx": 214
               },
               "pos": {
                  "row": 6,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets 26+",
                  "shortName": "Brooklyn Nets 26+"
               },
               "wasPrice": [],
               "id": 203826381,
               "name": "Brooklyn Nets 26+",
               "type": "HT > 25",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Winning Margin 12-Way (Incl. OT)",
            "shortName": "Winning Margin 12-Way (Incl. OT)"
         },
         "id": 70132125,
         "name": "Winning Margin 12-Way (Incl. OT)",
         "type": "BASKETBALL:FTOT:WM",
         "subtype": "M#sr:winning_margin_no_draw:26+",
         "displayOrder": -400,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.55",
                  "frac": "31/20",
                  "rootIdx": 109
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 220",
                  "shortName": "Under 220"
               },
               "wasPrice": [],
               "id": 203826268,
               "name": "Under 220",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.47",
                  "frac": "40/85",
                  "rootIdx": 45
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 220",
                  "shortName": "Over 220"
               },
               "wasPrice": [],
               "id": 203826255,
               "name": "Over 220",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132097,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#220",
         "line": 220.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.4",
                  "frac": "7/5",
                  "rootIdx": 106
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 221",
                  "shortName": "Under 221"
               },
               "wasPrice": [],
               "id": 203826357,
               "name": "Under 221",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.53",
                  "frac": "8/15",
                  "rootIdx": 50
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 221",
                  "shortName": "Over 221"
               },
               "wasPrice": [],
               "id": 203826349,
               "name": "Over 221",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132131,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#221",
         "line": 221.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.35",
                  "frac": "27/20",
                  "rootIdx": 104
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 231.5",
                  "shortName": "Over 231.5"
               },
               "wasPrice": [],
               "id": 203826178,
               "name": "Over 231.5",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.55",
                  "frac": "11/20",
                  "rootIdx": 51
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 231.5",
                  "shortName": "Under 231.5"
               },
               "wasPrice": [],
               "id": 203826188,
               "name": "Under 231.5",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132058,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#231.5",
         "line": 231.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.3",
                  "frac": "13/10",
                  "rootIdx": 103
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 222",
                  "shortName": "Under 222"
               },
               "wasPrice": [],
               "id": 203826201,
               "name": "Under 222",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.57",
                  "frac": "4/7",
                  "rootIdx": 52
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 222",
                  "shortName": "Over 222"
               },
               "wasPrice": [],
               "id": 203826197,
               "name": "Over 222",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132067,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#222",
         "line": 222.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.38",
                  "frac": "11/8",
                  "rootIdx": 105
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "Tobias Harris 2+",
                  "shortName": "Tobias Harris 2+"
               },
               "wasPrice": [],
               "id": 204141329,
               "name": "Tobias Harris 2+",
               "type": "2",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "7",
                  "frac": "6/1",
                  "rootIdx": 184
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Tobias Harris 4+",
                  "shortName": "Tobias Harris 4+"
               },
               "wasPrice": [],
               "id": 204141333,
               "name": "Tobias Harris 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "5",
                  "frac": "4/1",
                  "rootIdx": 160
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Tobias Harris 3+",
                  "shortName": "Tobias Harris 3+"
               },
               "wasPrice": [],
               "id": 204141331,
               "name": "Tobias Harris 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.38",
                  "frac": "19/50",
                  "rootIdx": 40
               },
               "pos": {
                  "row": 101,
                  "col": 1
               },
               "names": {
                  "longName": "Tobias Harris 1+",
                  "shortName": "Tobias Harris 1+"
               },
               "wasPrice": [],
               "id": 204141327,
               "name": "Tobias Harris 1+",
               "type": "1",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Tobias Harris steals (incl. overtime)",
            "shortName": "Tobias Harris steals (incl. overtime)"
         },
         "id": 70230400,
         "name": "Tobias Harris steals (incl. overtime)",
         "type": "BASKETBALL:FT:PLSTA",
         "subtype": "M#pre:playerprops:24750814:608250",
         "displayOrder": -270,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.2",
                  "frac": "6/5",
                  "rootIdx": 99
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 223",
                  "shortName": "Under 223"
               },
               "wasPrice": [],
               "id": 203826139,
               "name": "Under 223",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.65",
                  "frac": "13/20",
                  "rootIdx": 56
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 223",
                  "shortName": "Over 223"
               },
               "wasPrice": [],
               "id": 203826124,
               "name": "Over 223",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132032,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#223",
         "line": 223.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.05",
                  "frac": "21/20",
                  "rootIdx": 88
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 224",
                  "shortName": "Under 224"
               },
               "wasPrice": [],
               "id": 203826142,
               "name": "Under 224",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.72",
                  "frac": "8/11",
                  "rootIdx": 60
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 224",
                  "shortName": "Over 224"
               },
               "wasPrice": [],
               "id": 203826132,
               "name": "Over 224",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132034,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#224",
         "line": 224.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2",
                  "frac": "1/1",
                  "rootIdx": 83
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 225",
                  "shortName": "Under 225"
               },
               "wasPrice": [],
               "id": 203826140,
               "name": "Under 225",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.75",
                  "frac": "3/4",
                  "rootIdx": 63
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 225",
                  "shortName": "Over 225"
               },
               "wasPrice": [],
               "id": 203826109,
               "name": "Over 225",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132023,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#225",
         "line": 225.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 226",
                  "shortName": "Over 226"
               },
               "wasPrice": [],
               "id": 203826203,
               "name": "Over 226",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 226",
                  "shortName": "Under 226"
               },
               "wasPrice": [],
               "id": 203826214,
               "name": "Under 226",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132072,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#226",
         "line": 226.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 227",
                  "shortName": "Under 227"
               },
               "wasPrice": [],
               "id": 203826167,
               "name": "Under 227",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 227",
                  "shortName": "Over 227"
               },
               "wasPrice": [],
               "id": 203826163,
               "name": "Over 227",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132051,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#227",
         "line": 227.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.72",
                  "frac": "8/11",
                  "rootIdx": 60
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 228",
                  "shortName": "Under 228"
               },
               "wasPrice": [],
               "id": 203826260,
               "name": "Under 228",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.05",
                  "frac": "21/20",
                  "rootIdx": 88
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 228",
                  "shortName": "Over 228"
               },
               "wasPrice": [],
               "id": 203826245,
               "name": "Over 228",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132091,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#228",
         "line": 228.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.7",
                  "frac": "7/10",
                  "rootIdx": 59
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 229",
                  "shortName": "Under 229"
               },
               "wasPrice": [],
               "id": 203826215,
               "name": "Under 229",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.1",
                  "frac": "11/10",
                  "rootIdx": 93
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 229",
                  "shortName": "Over 229"
               },
               "wasPrice": [],
               "id": 203826206,
               "name": "Over 229",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 70132068,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#229",
         "line": 229.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "30",
                  "frac": "29/1",
                  "rootIdx": 221
               },
               "pos": {
                  "row": 302,
                  "col": 1
               },
               "names": {
                  "longName": "Shake Milton 6+",
                  "shortName": "Shake Milton 6+"
               },
               "wasPrice": [],
               "id": 204140795,
               "name": "Shake Milton 6+",
               "type": "6",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "17",
                  "frac": "16/1",
                  "rootIdx": 208
               },
               "pos": {
                  "row": 202,
                  "col": 1
               },
               "names": {
                  "longName": "Shake Milton 5+",
                  "shortName": "Shake Milton 5+"
               },
               "wasPrice": [],
               "id": 204140793,
               "name": "Shake Milton 5+",
               "type": "5",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "6.7",
                  "frac": "57/10",
                  "rootIdx": 180
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Shake Milton 4+",
                  "shortName": "Shake Milton 4+"
               },
               "wasPrice": [],
               "id": 204140791,
               "name": "Shake Milton 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.83",
                  "frac": "5/6",
                  "rootIdx": 69
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "Shake Milton 2+",
                  "shortName": "Shake Milton 2+"
               },
               "wasPrice": [],
               "id": 204140787,
               "name": "Shake Milton 2+",
               "type": "2",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "AWAY",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3",
                  "frac": "2/1",
                  "rootIdx": 120
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Shake Milton 3+",
                  "shortName": "Shake Milton 3+"
               },
               "wasPrice": [],
               "id": 204140789,
               "name": "Shake Milton 3+",
               "type": "3",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Shake Milton 3-point field goals (incl. overtime)",
            "shortName": "Shake Milton 3-point field goals (incl. overtime)"
         },
         "id": 70230292,
         "name": "Shake Milton 3-point field goals (incl. overtime)",
         "type": "BASKETBALL:FT:3PFG",
         "subtype": "M#pre:playerprops:24750814:1495351",
         "displayOrder": -272,
         "columnCount": 1,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.55",
                  "frac": "11/20",
                  "rootIdx": 51
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Brooklyn Nets +4.5",
                  "shortName": "Brooklyn Nets +4.5"
               },
               "wasPrice": [],
               "id": 203825986,
               "name": "Brooklyn Nets +4.5",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.35",
                  "frac": "27/20",
                  "rootIdx": 104
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Philadelphia 76ers -4.5",
                  "shortName": "Philadelphia 76ers -4.5"
               },
               "wasPrice": [],
               "id": 203825997,
               "name": "Philadelphia 76ers -4.5",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 70131977,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#4.5",
         "line": 4.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      }
   ],
   "pastForm": [],
   "participants": {
      "participant": [
         {
            "names": {
               "longName": "Philadelphia 76ers",
               "shortName": "PHI"
            },
            "id": 4062516,
            "name": "Philadelphia 76ers",
            "type": "AWAY"
         },
         {
            "names": {
               "longName": "Brooklyn Nets",
               "shortName": "BKN"
            },
            "id": 4062501,
            "name": "Brooklyn Nets",
            "type": "HOME"
         }
      ]
   },
   "names": {
      "longName": "Brooklyn Nets vs. Philadelphia 76ers",
      "shortName": "BKN vs. PHI",
      "veryshortName": "Brooklyn Nets vs. Philadelphia 76ers"
   },
   "compNames": {
      "longName": "NBA",
      "shortName": "NBA"
   },
   "categoryNames": {
      "longName": "USA",
      "shortName": "USA"
   },
   "id": 8715840,
   "compId": 8370429,
   "compName": "NBA",
   "compWeighting": 100000.0,
   "categoryId": 8370427,
   "categoryName": "USA",
   "path": "226652:8370427:8370429:8715840",
   "name": "Brooklyn Nets vs. Philadelphia 76ers",
   "sport": "BASKETBALL",
   "state": "SUSPENDED",
   "locale": "en-us",
   "displayed": true,
   "offeredInplay": true,
   "isInplay": false,
   "eventTime": 1610065800000,
   "numMarkets": 179,
   "numSpecSelections": 5,
   "numRABSelections": 24,
   "lastUpdatedTime": 1610058440572,
   "outright": false,
   "neutralVenue": false,
   "eventCode": "16608190",
   "winningLegsBonus": false,
   "usDisplay": true
}
