{
   "fixtures": [
      {
         "optionMarkets": [],
         "games": [
            {
               "id": 561393439,
               "name": {
                  "value": "Totals",
                  "sign": "mvsKzw=="
               },
               "results": [
                  {
                     "id": 1630341334,
                     "odds": 1.45,
                     "name": {
                        "value": "Over 241.5",
                        "sign": "cEVwFA==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 5,
                     "denominator": 11,
                     "americanOdds": -225
                  },
                  {
                     "id": 1630341335,
                     "odds": 2.7,
                     "name": {
                        "value": "Under 241.5",
                        "sign": "/zGPXw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 17,
                     "denominator": 10,
                     "americanOdds": 170
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "OtherLiveEvents",
               "combo2": "Single",
               "visibility": "Suspended",
               "balanced": 1,
               "attr": "241.5",
               "spread": 1.25,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "mvsKzw=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561393721,
               "name": {
                  "value": "Totals",
                  "sign": "G66VMg=="
               },
               "results": [
                  {
                     "id": 1630341928,
                     "odds": 1.83,
                     "name": {
                        "value": "Over 242.5",
                        "sign": "mvm0+g==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 5,
                     "denominator": 6,
                     "americanOdds": -120
                  },
                  {
                     "id": 1630341929,
                     "odds": 1.95,
                     "name": {
                        "value": "Under 242.5",
                        "sign": "ku9NDw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 19,
                     "denominator": 20,
                     "americanOdds": -105
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "OtherLiveEvents",
               "combo2": "Single",
               "visibility": "Suspended",
               "balanced": 1,
               "attr": "242.5",
               "spread": 0.12,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "G66VMg=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561393734,
               "name": {
                  "value": "Totals",
                  "sign": "Hk0pBQ=="
               },
               "results": [
                  {
                     "id": 1630341954,
                     "odds": 2.7,
                     "name": {
                        "value": "Over 243.5",
                        "sign": "7CIa8A==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 17,
                     "denominator": 10,
                     "americanOdds": 170
                  },
                  {
                     "id": 1630341955,
                     "odds": 1.45,
                     "name": {
                        "value": "Under 243.5",
                        "sign": "EphfDw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 5,
                     "denominator": 11,
                     "americanOdds": -225
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "OtherLiveEvents",
               "combo2": "Single",
               "visibility": "Suspended",
               "balanced": 1,
               "attr": "243.5",
               "spread": 1.25,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "Hk0pBQ=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561393751,
               "name": {
                  "value": "4th Quarter Totals (only points scored in this quarter/overtime not included)",
                  "sign": "/mS5ZA=="
               },
               "results": [
                  {
                     "id": 1630341988,
                     "odds": 1.8,
                     "name": {
                        "value": "Over 68.5",
                        "sign": "OjFfMg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 4,
                     "denominator": 5,
                     "americanOdds": -125
                  },
                  {
                     "id": 1630341989,
                     "odds": 1.9,
                     "name": {
                        "value": "Under 68.5",
                        "sign": "mfBxEw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 9,
                     "denominator": 10,
                     "americanOdds": -110
                  }
               ],
               "templateId": 2841,
               "categoryId": 731,
               "resultOrder": "Default",
               "combo1": "OtherLiveEvents",
               "combo2": "Single",
               "visibility": "Suspended",
               "balanced": 1,
               "attr": "68.5",
               "spread": 0.1,
               "category": "Gridable",
               "templateCategory": {
                  "id": 731,
                  "name": {
                     "value": "1/4 Totals",
                     "sign": "CWorUg=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "u4a4nn1dl"
                  ],
                  "detailed": [
                     {
                        "group": 1,
                        "index": 4,
                        "subIndex": 1,
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 4,
                        "index": 2,
                        "subIndex": 9,
                        "marketTabId": 7,
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            }
         ],
         "participants": [
            {
               "participantId": 2554200,
               "name": {
                  "value": "Utah Jazz",
                  "sign": "umhvSg==",
                  "short": "Jazz",
                  "shortSign": "mDvWxQ=="
               },
               "image": {
                  "logo": "2554200_logo",
                  "jersey": "2554200_1",
                  "rotateJersey": true
               },
               "options": []
            },
            {
               "participantId": 2554194,
               "name": {
                  "value": "San Antonio Spurs",
                  "sign": "ZuCTbQ==",
                  "short": "Spurs",
                  "shortSign": "cQL5Yg=="
               },
               "image": {
                  "logo": "2554194_logo",
                  "jersey": "2554194_1",
                  "rotateJersey": true
               },
               "options": []
            }
         ],
         "id": "10969346",
         "name": {
            "value": "Utah Jazz at San Antonio Spurs",
            "sign": "jSqGBw=="
         },
         "sourceId": 10969346,
         "source": "V1",
         "fixtureType": "Standard",
         "context": "v1|en-us|10969346",
         "addons": {
            "participantDividend": {}
         },
         "stage": "Live",
         "groupId": 2345469,
         "liveType": "NotSet",
         "liveAlert": false,
         "scoreboard": {
            "totalPoints": {
               "player1": {
                  "1": 33,
                  "3": 32,
                  "5": 33,
                  "7": 32,
                  "255": 130
               },
               "player2": {
                  "1": 23,
                  "3": 28,
                  "5": 25,
                  "7": 31,
                  "255": 107
               }
            },
            "matchType": 2,
            "source": "V1",
            "sportId": 7,
            "scoreboardId": 10969346,
            "period": "Q4",
            "periodId": 7,
            "points": [],
            "turn": "Player02",
            "score": "130:107",
            "indicator": "< 00:30",
            "started": true
         },
         "startDate": "2021-01-04T00:10:00Z",
         "cutOffDate": "2021-01-04T03:10:00Z",
         "sport": {
            "type": "Sport",
            "id": 7,
            "name": {
               "value": "Basketball",
               "sign": "WotGFA=="
            }
         },
         "competition": {
            "parentLeagueId": 6004,
            "statistics": true,
            "sportId": 7,
            "type": "Competition",
            "id": 2541,
            "parentId": 9,
            "name": {
               "value": "NBA",
               "sign": "pL+WdA=="
            }
         },
         "region": {
            "code": "N-A",
            "sportId": 7,
            "type": "Region",
            "id": 9,
            "parentId": 7,
            "name": {
               "value": "USA",
               "sign": "ULdUtw=="
            }
         },
         "viewType": "American",
         "isOpenForBetting": true,
         "isVirtual": false,
         "taggedLocations": [],
         "totalMarketsCount": 19
      },
      {
         "optionMarkets": [],
         "games": [
            {
               "id": 561380375,
               "name": {
                  "value": "Totals",
                  "sign": "DdBILw=="
               },
               "results": [
                  {
                     "id": 1630313220,
                     "odds": 1.95,
                     "name": {
                        "value": "Over 227.5",
                        "sign": "hBxkXQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 19,
                     "denominator": 20,
                     "americanOdds": -105
                  },
                  {
                     "id": 1630313221,
                     "odds": 1.83,
                     "name": {
                        "value": "Under 227.5",
                        "sign": "sZrbbg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 5,
                     "denominator": 6,
                     "americanOdds": -120
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "OtherLiveEvents",
               "combo2": "Single",
               "visibility": "Suspended",
               "balanced": 1,
               "attr": "227.5",
               "spread": 0.12,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "DdBILw=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561380386,
               "name": {
                  "value": "Totals",
                  "sign": "T/+pRQ=="
               },
               "results": [
                  {
                     "id": 1630313260,
                     "odds": 1.6,
                     "name": {
                        "value": "Over 226.5",
                        "sign": "vutahg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 3,
                     "denominator": 5,
                     "americanOdds": -165
                  },
                  {
                     "id": 1630313261,
                     "odds": 2.3,
                     "name": {
                        "value": "Under 226.5",
                        "sign": "pnByEQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 13,
                     "denominator": 10,
                     "americanOdds": 130
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "OtherLiveEvents",
               "combo2": "Single",
               "visibility": "Suspended",
               "balanced": 1,
               "attr": "226.5",
               "spread": 0.7,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "T/+pRQ=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561384151,
               "name": {
                  "value": "Totals",
                  "sign": "bx0B6Q=="
               },
               "results": [
                  {
                     "id": 1630321185,
                     "odds": 2.35,
                     "name": {
                        "value": "Over 229.5",
                        "sign": "pG8DAA==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 27,
                     "denominator": 20,
                     "americanOdds": 135
                  },
                  {
                     "id": 1630321186,
                     "odds": 1.57,
                     "name": {
                        "value": "Under 229.5",
                        "sign": "sbGxYg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 4,
                     "denominator": 7,
                     "americanOdds": -175
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "OtherLiveEvents",
               "combo2": "Single",
               "visibility": "Suspended",
               "balanced": 1,
               "attr": "229.5",
               "spread": 0.78,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "bx0B6Q=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561393801,
               "name": {
                  "value": "4th Quarter Totals (only points scored in this quarter/overtime not included)",
                  "sign": "UQ1WMw=="
               },
               "results": [
                  {
                     "id": 1630342090,
                     "odds": 1.57,
                     "name": {
                        "value": "Over 59.5",
                        "sign": "nXT8ew==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 4,
                     "denominator": 7,
                     "americanOdds": -175
                  },
                  {
                     "id": 1630342091,
                     "odds": 2.25,
                     "name": {
                        "value": "Under 59.5",
                        "sign": "+73htw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 5,
                     "denominator": 4,
                     "americanOdds": 125
                  }
               ],
               "templateId": 2841,
               "categoryId": 731,
               "resultOrder": "Default",
               "combo1": "OtherLiveEvents",
               "combo2": "Single",
               "visibility": "Suspended",
               "balanced": 1,
               "attr": "59.5",
               "spread": 0.68,
               "category": "Gridable",
               "templateCategory": {
                  "id": 731,
                  "name": {
                     "value": "1/4 Totals",
                     "sign": "WG6RBA=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "u4a4nn1dl"
                  ],
                  "detailed": [
                     {
                        "group": 1,
                        "index": 4,
                        "subIndex": 1,
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 4,
                        "index": 2,
                        "subIndex": 9,
                        "marketTabId": 7,
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            }
         ],
         "participants": [
            {
               "participantId": 2554164,
               "name": {
                  "value": "Denver Nuggets",
                  "sign": "0nOZ4w==",
                  "short": "Nuggets",
                  "shortSign": "C3LBwg=="
               },
               "image": {
                  "logo": "2554164_logo",
                  "jersey": "2554164_1",
                  "rotateJersey": true
               },
               "options": []
            },
            {
               "participantId": 2554182,
               "name": {
                  "value": "Minnesota Timberwolves",
                  "sign": "Wqf+cA==",
                  "short": "Timberwolves",
                  "shortSign": "MNrOVg=="
               },
               "image": {
                  "logo": "2554182_logo",
                  "jersey": "2554182_1",
                  "rotateJersey": true
               },
               "options": []
            }
         ],
         "id": "10969347",
         "name": {
            "value": "Denver Nuggets at Minnesota Timberwolves",
            "sign": "M3TuzA=="
         },
         "sourceId": 10969347,
         "source": "V1",
         "fixtureType": "Standard",
         "context": "v1|en-us|10969347",
         "addons": {
            "betRadar": 24750924,
            "statistics": true,
            "participantDividend": {}
         },
         "stage": "Live",
         "groupId": 2345470,
         "liveType": "NotSet",
         "liveAlert": false,
         "scoreboard": {
            "totalPoints": {
               "player1": {
                  "1": 29,
                  "3": 26,
                  "5": 28,
                  "7": 36,
                  "255": 119
               },
               "player2": {
                  "1": 28,
                  "3": 20,
                  "5": 36,
                  "7": 17,
                  "255": 101
               }
            },
            "matchType": 2,
            "source": "V1",
            "sportId": 7,
            "scoreboardId": 10969347,
            "period": "Q4",
            "periodId": 7,
            "points": [],
            "turn": "Player01",
            "score": "119:101",
            "indicator": "< 01:45",
            "started": true
         },
         "startDate": "2021-01-04T00:10:00Z",
         "cutOffDate": "2021-01-04T03:10:00Z",
         "sport": {
            "type": "Sport",
            "id": 7,
            "name": {
               "value": "Basketball",
               "sign": "WotGFA=="
            }
         },
         "competition": {
            "parentLeagueId": 6004,
            "statistics": true,
            "sportId": 7,
            "type": "Competition",
            "id": 2541,
            "parentId": 9,
            "name": {
               "value": "NBA",
               "sign": "pL+WdA=="
            }
         },
         "region": {
            "code": "N-A",
            "sportId": 7,
            "type": "Region",
            "id": 9,
            "parentId": 7,
            "name": {
               "value": "USA",
               "sign": "ULdUtw=="
            }
         },
         "viewType": "American",
         "isOpenForBetting": true,
         "isVirtual": false,
         "taggedLocations": [],
         "totalMarketsCount": 20
      },
      {
         "optionMarkets": [],
         "games": [
            {
               "id": 560594015,
               "name": {
                  "value": "Money Line",
                  "sign": "0xwAJg=="
               },
               "results": [
                  {
                     "id": 1628252394,
                     "odds": 1.05,
                     "name": {
                        "value": "Clippers",
                        "sign": "hMWZeA==",
                        "shortSign": ""
                     },
                     "sourceName": {
                        "value": "1",
                        "sign": "ePLxtA=="
                     },
                     "visibility": "Visible",
                     "numerator": 1,
                     "denominator": 20,
                     "americanOdds": -2000
                  },
                  {
                     "id": 1628252395,
                     "odds": 11.0,
                     "name": {
                        "value": "Suns",
                        "sign": "IWmT3Q==",
                        "shortSign": ""
                     },
                     "sourceName": {
                        "value": "2",
                        "sign": "gWheqg=="
                     },
                     "visibility": "Visible",
                     "numerator": 10,
                     "denominator": 1,
                     "americanOdds": 1000
                  }
               ],
               "templateId": 3450,
               "categoryId": 43,
               "resultOrder": "Default",
               "combo1": "OtherLiveEvents",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 9.95,
               "category": "Gridable",
               "templateCategory": {
                  "id": 43,
                  "name": {
                     "value": "Money Line",
                     "sign": "0xwAJg=="
                  },
                  "category": "Gridable"
               },
               "isMain": true,
               "grouping": {
                  "gridGroups": [
                     "xt229vkj8"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 2,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "displayType": "Regular"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561388867,
               "name": {
                  "value": "Totals",
                  "sign": "VRG5cQ=="
               },
               "results": [
                  {
                     "id": 1630331004,
                     "odds": 1.87,
                     "name": {
                        "value": "Over 213.5",
                        "sign": "T21etg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 20,
                     "denominator": 23,
                     "americanOdds": -115
                  },
                  {
                     "id": 1630331005,
                     "odds": 1.9,
                     "name": {
                        "value": "Under 213.5",
                        "sign": "TVH4vw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 9,
                     "denominator": 10,
                     "americanOdds": -110
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "OtherLiveEvents",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "213.5",
               "spread": 0.03,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "VRG5cQ=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561391066,
               "name": {
                  "value": "Spread",
                  "sign": "OFq8Kw=="
               },
               "results": [
                  {
                     "id": 1630335813,
                     "odds": 1.9,
                     "name": {
                        "value": "Los Angeles Clippers -16.5",
                        "sign": "f9bgxg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-16.5",
                     "numerator": 9,
                     "denominator": 10,
                     "americanOdds": -110
                  },
                  {
                     "id": 1630335814,
                     "odds": 1.87,
                     "name": {
                        "value": "Phoenix Suns +16.5",
                        "sign": "0RDmlg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+16.5",
                     "numerator": 20,
                     "denominator": 23,
                     "americanOdds": -115
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "OtherLiveEvents",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 0.03,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "OFq8Kw=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561391072,
               "name": {
                  "value": "Spread",
                  "sign": "Om3HnQ=="
               },
               "results": [
                  {
                     "id": 1630335825,
                     "odds": 2.25,
                     "name": {
                        "value": "Los Angeles Clippers -18.5",
                        "sign": "YIs9Ng==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-18.5",
                     "numerator": 5,
                     "denominator": 4,
                     "americanOdds": 125
                  },
                  {
                     "id": 1630335826,
                     "odds": 1.62,
                     "name": {
                        "value": "Phoenix Suns +18.5",
                        "sign": "oGDHKw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+18.5",
                     "numerator": 8,
                     "denominator": 13,
                     "americanOdds": -160
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "OtherLiveEvents",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 0.63,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "Om3HnQ=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561391077,
               "name": {
                  "value": "Spread",
                  "sign": "+ppO1g=="
               },
               "results": [
                  {
                     "id": 1630335835,
                     "odds": 2.05,
                     "name": {
                        "value": "Los Angeles Clippers -17.5",
                        "sign": "d/f4rQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-17.5",
                     "numerator": 21,
                     "denominator": 20,
                     "americanOdds": 105
                  },
                  {
                     "id": 1630335836,
                     "odds": 1.75,
                     "name": {
                        "value": "Phoenix Suns +17.5",
                        "sign": "2E71BA==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+17.5",
                     "numerator": 3,
                     "denominator": 4,
                     "americanOdds": -135
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "OtherLiveEvents",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 0.3,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "+ppO1g=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561393496,
               "name": {
                  "value": "3rd quarter totals  (only points scored in this quarter)",
                  "sign": "YCIpkQ=="
               },
               "results": [
                  {
                     "id": 1630341477,
                     "odds": 1.8,
                     "name": {
                        "value": "Over 53.5",
                        "sign": "NC0PUA==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 4,
                     "denominator": 5,
                     "americanOdds": -125
                  },
                  {
                     "id": 1630341478,
                     "odds": 1.9,
                     "name": {
                        "value": "Under 53.5",
                        "sign": "M3ugbw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 9,
                     "denominator": 10,
                     "americanOdds": -110
                  }
               ],
               "templateId": 2840,
               "categoryId": 731,
               "resultOrder": "Default",
               "combo1": "OtherLiveEvents",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "53.5",
               "spread": 0.1,
               "category": "Gridable",
               "templateCategory": {
                  "id": 731,
                  "name": {
                     "value": "1/4 Totals",
                     "sign": "4nSNZQ=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "qdqtjbjls"
                  ],
                  "detailed": [
                     {
                        "group": 1,
                        "index": 3,
                        "subIndex": 1,
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 4,
                        "index": 2,
                        "subIndex": 7,
                        "marketTabId": 6,
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561393501,
               "name": {
                  "value": "Totals",
                  "sign": "3REUKw=="
               },
               "results": [
                  {
                     "id": 1630341487,
                     "odds": 1.7,
                     "name": {
                        "value": "Over 211.5",
                        "sign": "qY1TQw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 7,
                     "denominator": 10,
                     "americanOdds": -145
                  },
                  {
                     "id": 1630341488,
                     "odds": 2.1,
                     "name": {
                        "value": "Under 211.5",
                        "sign": "HSdGfw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 11,
                     "denominator": 10,
                     "americanOdds": 110
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "OtherLiveEvents",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "211.5",
               "spread": 0.4,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "3REUKw=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561393507,
               "name": {
                  "value": "Totals",
                  "sign": "37XOwQ=="
               },
               "results": [
                  {
                     "id": 1630341499,
                     "odds": 1.78,
                     "name": {
                        "value": "Over 212.5",
                        "sign": "Vv7TwQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 4,
                     "denominator": 5,
                     "americanOdds": -130
                  },
                  {
                     "id": 1630341500,
                     "odds": 2.0,
                     "name": {
                        "value": "Under 212.5",
                        "sign": "iyTD3g==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 1,
                     "denominator": 1,
                     "americanOdds": 100
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "OtherLiveEvents",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "212.5",
               "spread": 0.22,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "37XOwQ=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            }
         ],
         "participants": [
            {
               "participantId": 2554178,
               "name": {
                  "value": "Los Angeles Clippers",
                  "sign": "q7qMEQ==",
                  "short": "Clippers",
                  "shortSign": "1kfzeQ=="
               },
               "image": {
                  "logo": "2554178_logo",
                  "jersey": "2554178_1",
                  "rotateJersey": true
               },
               "options": []
            },
            {
               "participantId": 2554189,
               "name": {
                  "value": "Phoenix Suns",
                  "sign": "/kofoQ==",
                  "short": "Suns",
                  "shortSign": "6jzWbw=="
               },
               "image": {
                  "logo": "2554189_logo",
                  "jersey": "2554189_1",
                  "rotateJersey": true
               },
               "options": []
            }
         ],
         "id": "10969348",
         "name": {
            "value": "Los Angeles Clippers at Phoenix Suns",
            "sign": "5filpQ=="
         },
         "sourceId": 10969348,
         "source": "V1",
         "fixtureType": "Standard",
         "context": "v1|en-us|10969348",
         "addons": {
            "participantDividend": {}
         },
         "stage": "Live",
         "groupId": 2345471,
         "liveType": "NotSet",
         "liveAlert": true,
         "scoreboard": {
            "totalPoints": {
               "player1": {
                  "1": 37,
                  "3": 27,
                  "255": 64
               },
               "player2": {
                  "1": 24,
                  "3": 20,
                  "255": 44
               }
            },
            "matchType": 2,
            "source": "V1",
            "sportId": 7,
            "scoreboardId": 10969348,
            "period": "Halftime",
            "periodId": 4,
            "points": [],
            "turn": "Player01",
            "score": "64:44",
            "started": true
         },
         "startDate": "2021-01-04T01:10:00Z",
         "cutOffDate": "2021-01-04T04:10:00Z",
         "sport": {
            "type": "Sport",
            "id": 7,
            "name": {
               "value": "Basketball",
               "sign": "WotGFA=="
            }
         },
         "competition": {
            "parentLeagueId": 6004,
            "statistics": true,
            "sportId": 7,
            "type": "Competition",
            "id": 2541,
            "parentId": 9,
            "name": {
               "value": "NBA",
               "sign": "pL+WdA=="
            }
         },
         "region": {
            "code": "N-A",
            "sportId": 7,
            "type": "Region",
            "id": 9,
            "parentId": 7,
            "name": {
               "value": "USA",
               "sign": "ULdUtw=="
            }
         },
         "viewType": "American",
         "isOpenForBetting": true,
         "isVirtual": false,
         "taggedLocations": [],
         "totalMarketsCount": 26
      },
      {
         "optionMarkets": [],
         "games": [
            {
               "id": 560594036,
               "name": {
                  "value": "Money Line",
                  "sign": "JmyrDg=="
               },
               "results": [
                  {
                     "id": 1628252464,
                     "odds": 1.57,
                     "name": {
                        "value": "Mavericks",
                        "sign": "lEBYvA==",
                        "shortSign": ""
                     },
                     "sourceName": {
                        "value": "1",
                        "sign": "0Ysu2A=="
                     },
                     "visibility": "Visible",
                     "numerator": 4,
                     "denominator": 7,
                     "americanOdds": -175
                  },
                  {
                     "id": 1628252465,
                     "odds": 2.45,
                     "name": {
                        "value": "Bulls",
                        "sign": "DdkD7Q==",
                        "shortSign": ""
                     },
                     "sourceName": {
                        "value": "2",
                        "sign": "KBGBxg=="
                     },
                     "visibility": "Visible",
                     "numerator": 29,
                     "denominator": 20,
                     "americanOdds": 145
                  }
               ],
               "templateId": 3450,
               "categoryId": 43,
               "resultOrder": "Default",
               "combo1": "OtherLiveEvents",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 0.88,
               "category": "Gridable",
               "templateCategory": {
                  "id": 43,
                  "name": {
                     "value": "Money Line",
                     "sign": "JmyrDg=="
                  },
                  "category": "Gridable"
               },
               "isMain": true,
               "grouping": {
                  "gridGroups": [
                     "xt229vkj8"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 2,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "displayType": "Regular"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561386944,
               "name": {
                  "value": "Spread",
                  "sign": "zch0yQ=="
               },
               "results": [
                  {
                     "id": 1630326886,
                     "odds": 2.0,
                     "name": {
                        "value": "Dallas Mavericks -3.5",
                        "sign": "vqY6QA==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-3.5",
                     "numerator": 1,
                     "denominator": 1,
                     "americanOdds": 100
                  },
                  {
                     "id": 1630326887,
                     "odds": 1.78,
                     "name": {
                        "value": "Chicago Bulls +3.5",
                        "sign": "HH0+dA==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+3.5",
                     "numerator": 4,
                     "denominator": 5,
                     "americanOdds": -130
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "OtherLiveEvents",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 0.22,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "zch0yQ=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561388750,
               "name": {
                  "value": "Spread",
                  "sign": "pT7uuA=="
               },
               "results": [
                  {
                     "id": 1630330762,
                     "odds": 1.7,
                     "name": {
                        "value": "Dallas Mavericks -1.5",
                        "sign": "8z+j7Q==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-1.5",
                     "numerator": 7,
                     "denominator": 10,
                     "americanOdds": -145
                  },
                  {
                     "id": 1630330763,
                     "odds": 2.1,
                     "name": {
                        "value": "Chicago Bulls +1.5",
                        "sign": "yI+d3Q==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+1.5",
                     "numerator": 11,
                     "denominator": 10,
                     "americanOdds": 110
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "OtherLiveEvents",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 0.4,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "pT7uuA=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561388753,
               "name": {
                  "value": "Spread",
                  "sign": "pGyDzQ=="
               },
               "results": [
                  {
                     "id": 1630330768,
                     "odds": 1.83,
                     "name": {
                        "value": "Dallas Mavericks -2.5",
                        "sign": "Fljuiw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-2.5",
                     "numerator": 5,
                     "denominator": 6,
                     "americanOdds": -120
                  },
                  {
                     "id": 1630330769,
                     "odds": 1.95,
                     "name": {
                        "value": "Chicago Bulls +2.5",
                        "sign": "rwUKTA==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+2.5",
                     "numerator": 19,
                     "denominator": 20,
                     "americanOdds": -105
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "OtherLiveEvents",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 0.12,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "pGyDzQ=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561389995,
               "name": {
                  "value": "Totals",
                  "sign": "Z/3hLg=="
               },
               "results": [
                  {
                     "id": 1630333275,
                     "odds": 1.87,
                     "name": {
                        "value": "Over 228.5",
                        "sign": "buUodg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 20,
                     "denominator": 23,
                     "americanOdds": -115
                  },
                  {
                     "id": 1630333276,
                     "odds": 1.9,
                     "name": {
                        "value": "Under 228.5",
                        "sign": "gtdJwQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 9,
                     "denominator": 10,
                     "americanOdds": -110
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "OtherLiveEvents",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "228.5",
               "spread": 0.03,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "Z/3hLg=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561390001,
               "name": {
                  "value": "Totals",
                  "sign": "plgFEA=="
               },
               "results": [
                  {
                     "id": 1630333287,
                     "odds": 2.0,
                     "name": {
                        "value": "Over 229.5",
                        "sign": "8t/QLQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 1,
                     "denominator": 1,
                     "americanOdds": 100
                  },
                  {
                     "id": 1630333288,
                     "odds": 1.8,
                     "name": {
                        "value": "Under 229.5",
                        "sign": "DytXQw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 4,
                     "denominator": 5,
                     "americanOdds": -125
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "OtherLiveEvents",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "229.5",
               "spread": 0.2,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "plgFEA=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561390008,
               "name": {
                  "value": "Totals",
                  "sign": "Zhpuvg=="
               },
               "results": [
                  {
                     "id": 1630333301,
                     "odds": 2.1,
                     "name": {
                        "value": "Over 230.5",
                        "sign": "D5OlJQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 11,
                     "denominator": 10,
                     "americanOdds": 110
                  },
                  {
                     "id": 1630333302,
                     "odds": 1.72,
                     "name": {
                        "value": "Under 230.5",
                        "sign": "/VeV1A==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 8,
                     "denominator": 11,
                     "americanOdds": -140
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "OtherLiveEvents",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "230.5",
               "spread": 0.38,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "Zhpuvg=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561393682,
               "name": {
                  "value": "3rd quarter totals  (only points scored in this quarter)",
                  "sign": "gFF/kg=="
               },
               "results": [
                  {
                     "id": 1630341850,
                     "odds": 1.91,
                     "name": {
                        "value": "Over 55.5",
                        "sign": "sM/Uqw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 10,
                     "denominator": 11,
                     "americanOdds": -110
                  },
                  {
                     "id": 1630341851,
                     "odds": 1.8,
                     "name": {
                        "value": "Under 55.5",
                        "sign": "6Y8isQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 4,
                     "denominator": 5,
                     "americanOdds": -125
                  }
               ],
               "templateId": 2840,
               "categoryId": 731,
               "resultOrder": "Default",
               "combo1": "OtherLiveEvents",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "55.5",
               "spread": 0.11,
               "category": "Gridable",
               "templateCategory": {
                  "id": 731,
                  "name": {
                     "value": "1/4 Totals",
                     "sign": "Yz14Ww=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "qdqtjbjls"
                  ],
                  "detailed": [
                     {
                        "group": 1,
                        "index": 3,
                        "subIndex": 1,
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 4,
                        "index": 2,
                        "subIndex": 7,
                        "marketTabId": 6,
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            }
         ],
         "participants": [
            {
               "participantId": 2554163,
               "name": {
                  "value": "Dallas Mavericks",
                  "sign": "SiEByg==",
                  "short": "Mavericks",
                  "shortSign": "YiX7vA=="
               },
               "image": {
                  "logo": "2554163_logo",
                  "jersey": "2554163_1",
                  "rotateJersey": true
               },
               "options": []
            },
            {
               "participantId": 2554160,
               "name": {
                  "value": "Chicago Bulls",
                  "sign": "5bX06g==",
                  "short": "Bulls",
                  "shortSign": "BYL8SA=="
               },
               "image": {
                  "logo": "2554160_logo",
                  "jersey": "2554160_1",
                  "rotateJersey": true
               },
               "options": []
            }
         ],
         "id": "10969349",
         "name": {
            "value": "Dallas Mavericks at Chicago Bulls",
            "sign": "eKfVew=="
         },
         "sourceId": 10969349,
         "source": "V1",
         "fixtureType": "Standard",
         "context": "v1|en-us|10969349",
         "addons": {
            "betRadar": 24750998,
            "participantDividend": {}
         },
         "stage": "Live",
         "groupId": 2345472,
         "liveType": "NotSet",
         "liveAlert": true,
         "scoreboard": {
            "totalPoints": {
               "player1": {
                  "1": 31,
                  "3": 29,
                  "255": 60
               },
               "player2": {
                  "1": 31,
                  "3": 28,
                  "255": 59
               }
            },
            "matchType": 2,
            "source": "V1",
            "sportId": 7,
            "scoreboardId": 10969349,
            "period": "Halftime",
            "periodId": 4,
            "points": [],
            "turn": "Player02",
            "score": "60:59",
            "started": true
         },
         "startDate": "2021-01-04T01:10:00Z",
         "cutOffDate": "2021-01-04T04:10:00Z",
         "sport": {
            "type": "Sport",
            "id": 7,
            "name": {
               "value": "Basketball",
               "sign": "WotGFA=="
            }
         },
         "competition": {
            "parentLeagueId": 6004,
            "statistics": true,
            "sportId": 7,
            "type": "Competition",
            "id": 2541,
            "parentId": 9,
            "name": {
               "value": "NBA",
               "sign": "pL+WdA=="
            }
         },
         "region": {
            "code": "N-A",
            "sportId": 7,
            "type": "Region",
            "id": 9,
            "parentId": 7,
            "name": {
               "value": "USA",
               "sign": "ULdUtw=="
            }
         },
         "viewType": "American",
         "isOpenForBetting": true,
         "isVirtual": false,
         "taggedLocations": [],
         "totalMarketsCount": 29
      },
      {
         "optionMarkets": [],
         "games": [
            {
               "id": 560594057,
               "name": {
                  "value": "Money Line",
                  "sign": "PjWTJQ=="
               },
               "results": [
                  {
                     "id": 1628252534,
                     "odds": 1.95,
                     "name": {
                        "value": "Trail Blazers",
                        "sign": "9LTYQw==",
                        "shortSign": ""
                     },
                     "sourceName": {
                        "value": "1",
                        "sign": "LVlcIg=="
                     },
                     "visibility": "Visible",
                     "numerator": 19,
                     "denominator": 20,
                     "americanOdds": -105
                  },
                  {
                     "id": 1628252535,
                     "odds": 1.87,
                     "name": {
                        "value": "Warriors",
                        "sign": "KwBnQQ==",
                        "shortSign": ""
                     },
                     "sourceName": {
                        "value": "2",
                        "sign": "1MPzPA=="
                     },
                     "visibility": "Visible",
                     "numerator": 20,
                     "denominator": 23,
                     "americanOdds": -115
                  }
               ],
               "templateId": 3450,
               "categoryId": 43,
               "resultOrder": "Default",
               "combo1": "OtherLiveEvents",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 0.08,
               "category": "Gridable",
               "templateCategory": {
                  "id": 43,
                  "name": {
                     "value": "Money Line",
                     "sign": "PjWTJQ=="
                  },
                  "category": "Gridable"
               },
               "isMain": true,
               "grouping": {
                  "gridGroups": [
                     "xt229vkj8"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 2,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "displayType": "Regular"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 560594058,
               "name": {
                  "value": "Spread",
                  "sign": "7KDC4w=="
               },
               "results": [
                  {
                     "id": 1628252536,
                     "odds": 1.83,
                     "name": {
                        "value": "Portland Trail Blazers +1.5",
                        "sign": "SETROQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+1.5",
                     "numerator": 5,
                     "denominator": 6,
                     "americanOdds": -120
                  },
                  {
                     "id": 1628252537,
                     "odds": 1.95,
                     "name": {
                        "value": "Golden State Warriors -1.5",
                        "sign": "oqLIoA==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-1.5",
                     "numerator": 19,
                     "denominator": 20,
                     "americanOdds": -105
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "OtherLiveEvents",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 0.12,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "7KDC4w=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561390152,
               "name": {
                  "value": "1st Quarter Money Line",
                  "sign": "rY8z4A=="
               },
               "results": [
                  {
                     "id": 1630333589,
                     "odds": 34.0,
                     "name": {
                        "value": "Trail Blazers",
                        "sign": "pgPZzA==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "numerator": 33,
                     "denominator": 1,
                     "americanOdds": 3300
                  },
                  {
                     "id": 1630333590,
                     "odds": 1.01,
                     "name": {
                        "value": "Warriors",
                        "sign": "x3D9sg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "numerator": 1,
                     "denominator": 100,
                     "americanOdds": -10000
                  }
               ],
               "templateId": 19503,
               "categoryId": 1272,
               "resultOrder": "Default",
               "combo1": "OtherLiveEvents",
               "combo2": "Single",
               "visibility": "Suspended",
               "category": "Gridable",
               "templateCategory": {
                  "id": 1272,
                  "name": {
                     "value": "Quarter Money Line",
                     "sign": "ytRCpA=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "ohu73we4y"
                  ],
                  "detailed": [
                     {
                        "group": 4,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 4,
                        "name": "Money Line",
                        "displayType": "Regular"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561390196,
               "name": {
                  "value": "1st Half Money Line",
                  "sign": "DQk68g=="
               },
               "results": [
                  {
                     "id": 1630333698,
                     "odds": 3.1,
                     "name": {
                        "value": "Trail Blazers",
                        "sign": "rQegVA==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "numerator": 21,
                     "denominator": 10,
                     "americanOdds": 210
                  },
                  {
                     "id": 1630333699,
                     "odds": 1.33,
                     "name": {
                        "value": "Warriors",
                        "sign": "POt/8w==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "numerator": 1,
                     "denominator": 3,
                     "americanOdds": -300
                  }
               ],
               "templateId": 7763,
               "categoryId": 623,
               "resultOrder": "Default",
               "combo1": "OtherLiveEvents",
               "combo2": "Single",
               "visibility": "Visible",
               "category": "Gridable",
               "templateCategory": {
                  "id": 623,
                  "name": {
                     "value": "1st Half Money Line",
                     "sign": "DQk68g=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "zrzzjn34b"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 2,
                        "subIndex": 1,
                        "marketTabId": 2,
                        "displayType": "Regular"
                     },
                     {
                        "group": 4,
                        "index": 0,
                        "subIndex": 2,
                        "marketTabId": 2,
                        "name": "Money Line",
                        "displayType": "Regular"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561392042,
               "name": {
                  "value": "Spread",
                  "sign": "sKyR9w=="
               },
               "results": [
                  {
                     "id": 1630338134,
                     "odds": 2.2,
                     "name": {
                        "value": "Portland Trail Blazers -2.5",
                        "sign": "AiBPXg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-2.5",
                     "numerator": 6,
                     "denominator": 5,
                     "americanOdds": 120
                  },
                  {
                     "id": 1630338135,
                     "odds": 1.65,
                     "name": {
                        "value": "Golden State Warriors +2.5",
                        "sign": "zepr6w==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+2.5",
                     "numerator": 13,
                     "denominator": 20,
                     "americanOdds": -155
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "OtherLiveEvents",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 0.55,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "sKyR9w=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561392618,
               "name": {
                  "value": "Spread",
                  "sign": "Co4b2Q=="
               },
               "results": [
                  {
                     "id": 1630339328,
                     "odds": 2.05,
                     "name": {
                        "value": "Portland Trail Blazers -1.5",
                        "sign": "1FoPMw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-1.5",
                     "numerator": 21,
                     "denominator": 20,
                     "americanOdds": 105
                  },
                  {
                     "id": 1630339329,
                     "odds": 1.75,
                     "name": {
                        "value": "Golden State Warriors +1.5",
                        "sign": "QjqeXw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+1.5",
                     "numerator": 3,
                     "denominator": 4,
                     "americanOdds": -135
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "OtherLiveEvents",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 0.3,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "Co4b2Q=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561392628,
               "name": {
                  "value": "1st Half Spread",
                  "sign": "Cmfe9g=="
               },
               "results": [
                  {
                     "id": 1630339348,
                     "odds": 2.2,
                     "name": {
                        "value": "Portland Trail Blazers +2.5",
                        "sign": "yhwFeQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+2.5",
                     "numerator": 6,
                     "denominator": 5,
                     "americanOdds": 120
                  },
                  {
                     "id": 1630339349,
                     "odds": 1.65,
                     "name": {
                        "value": "Golden State Warriors -2.5",
                        "sign": "0qvwgg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-2.5",
                     "numerator": 13,
                     "denominator": 20,
                     "americanOdds": -155
                  }
               ],
               "templateId": 7068,
               "categoryId": 728,
               "resultOrder": "Default",
               "combo1": "OtherLiveEvents",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 0.55,
               "category": "Gridable",
               "templateCategory": {
                  "id": 728,
                  "name": {
                     "value": "Halftime Spread",
                     "sign": "IL8lmA=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "jzowoz9hz"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 3,
                        "marketTabId": 2,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 3,
                        "marketTabId": 2,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 4,
                        "index": 1,
                        "subIndex": 2,
                        "marketTabId": 2,
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561392630,
               "name": {
                  "value": "1st Quarter Spread",
                  "sign": "DoGQVQ=="
               },
               "results": [
                  {
                     "id": 1630339352,
                     "odds": 2.05,
                     "name": {
                        "value": "Portland Trail Blazers +5.5",
                        "sign": "+Wp47g==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+5.5",
                     "numerator": 21,
                     "denominator": 20,
                     "americanOdds": 105
                  },
                  {
                     "id": 1630339353,
                     "odds": 1.75,
                     "name": {
                        "value": "Golden State Warriors -5.5",
                        "sign": "m5BRZw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-5.5",
                     "numerator": 3,
                     "denominator": 4,
                     "americanOdds": -135
                  }
               ],
               "templateId": 2834,
               "categoryId": 729,
               "resultOrder": "Default",
               "combo1": "OtherLiveEvents",
               "combo2": "Single",
               "visibility": "Suspended",
               "balanced": 1,
               "spread": 0.3,
               "category": "Gridable",
               "templateCategory": {
                  "id": 729,
                  "name": {
                     "value": "Quarter Spread",
                     "sign": "1H/DRQ=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "pitxb8l13"
                  ],
                  "detailed": [
                     {
                        "group": 2,
                        "index": 2,
                        "subIndex": 0,
                        "displayType": "Spread"
                     },
                     {
                        "group": 4,
                        "index": 1,
                        "subIndex": 0,
                        "marketTabId": 4,
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561392920,
               "name": {
                  "value": "Totals",
                  "sign": "p8QXTg=="
               },
               "results": [
                  {
                     "id": 1630340295,
                     "odds": 1.65,
                     "name": {
                        "value": "Over 240.5",
                        "sign": "5AtLjw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 13,
                     "denominator": 20,
                     "americanOdds": -155
                  },
                  {
                     "id": 1630340296,
                     "odds": 2.2,
                     "name": {
                        "value": "Under 240.5",
                        "sign": "89vTbw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 6,
                     "denominator": 5,
                     "americanOdds": 120
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "OtherLiveEvents",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "240.5",
               "spread": 0.55,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "p8QXTg=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561392923,
               "name": {
                  "value": "Totals",
                  "sign": "52lvdw=="
               },
               "results": [
                  {
                     "id": 1630340301,
                     "odds": 1.72,
                     "name": {
                        "value": "Over 241.5",
                        "sign": "AAfqzg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 8,
                     "denominator": 11,
                     "americanOdds": -140
                  },
                  {
                     "id": 1630340302,
                     "odds": 2.1,
                     "name": {
                        "value": "Under 241.5",
                        "sign": "A+xI0g==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 11,
                     "denominator": 10,
                     "americanOdds": 110
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "OtherLiveEvents",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "241.5",
               "spread": 0.38,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "52lvdw=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561393315,
               "name": {
                  "value": "1st Quarter Totals",
                  "sign": "kQTnVg=="
               },
               "results": [
                  {
                     "id": 1630341085,
                     "odds": 3.5,
                     "name": {
                        "value": "Over 69.5",
                        "sign": "VKIFRA==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 5,
                     "denominator": 2,
                     "americanOdds": 250
                  },
                  {
                     "id": 1630341086,
                     "odds": 1.28,
                     "name": {
                        "value": "Under 69.5",
                        "sign": "aKA5FQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 2,
                     "denominator": 7,
                     "americanOdds": -350
                  }
               ],
               "templateId": 8643,
               "categoryId": 731,
               "resultOrder": "Default",
               "combo1": "OtherLiveEvents",
               "combo2": "Single",
               "visibility": "Suspended",
               "balanced": 1,
               "attr": "69.5",
               "spread": 2.22,
               "category": "Gridable",
               "templateCategory": {
                  "id": 731,
                  "name": {
                     "value": "1/4 Totals",
                     "sign": "RWDg1A=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "obi6qifok"
                  ],
                  "detailed": [
                     {
                        "group": 1,
                        "index": 1,
                        "subIndex": 0,
                        "marketTabId": 4,
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 4,
                        "index": 2,
                        "subIndex": 1,
                        "marketTabId": 4,
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561393575,
               "name": {
                  "value": "Totals",
                  "sign": "10TMGA=="
               },
               "results": [
                  {
                     "id": 1630341635,
                     "odds": 1.78,
                     "name": {
                        "value": "Over 242.5",
                        "sign": "YRmUdw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 4,
                     "denominator": 5,
                     "americanOdds": -130
                  },
                  {
                     "id": 1630341636,
                     "odds": 2.0,
                     "name": {
                        "value": "Under 242.5",
                        "sign": "Ibn2mQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 1,
                     "denominator": 1,
                     "americanOdds": 100
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "OtherLiveEvents",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "242.5",
               "spread": 0.22,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "10TMGA=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561393688,
               "name": {
                  "value": "1st Half Totals",
                  "sign": "m+xLSw=="
               },
               "results": [
                  {
                     "id": 1630341862,
                     "odds": 1.85,
                     "name": {
                        "value": "Over 129.5",
                        "sign": "UPxJzQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 17,
                     "denominator": 20,
                     "americanOdds": -115
                  },
                  {
                     "id": 1630341863,
                     "odds": 1.91,
                     "name": {
                        "value": "Under 129.5",
                        "sign": "2/OAxA==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 10,
                     "denominator": 11,
                     "americanOdds": -110
                  }
               ],
               "templateId": 15038,
               "categoryId": 730,
               "resultOrder": "Default",
               "combo1": "OtherLiveEvents",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "129.5",
               "spread": 0.06,
               "category": "Gridable",
               "templateCategory": {
                  "id": 730,
                  "name": {
                     "value": "Halftime Totals",
                     "sign": "sTSwJQ=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "p28rj0wqu"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 3,
                        "marketTabId": 2,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 3,
                        "marketTabId": 2,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 4,
                        "index": 2,
                        "subIndex": 6,
                        "marketTabId": 2,
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            }
         ],
         "participants": [
            {
               "participantId": 2554190,
               "name": {
                  "value": "Portland Trail Blazers",
                  "sign": "jDrvvQ==",
                  "short": "Trail Blazers",
                  "shortSign": "7iQfcA=="
               },
               "image": {
                  "logo": "2554190_logo",
                  "jersey": "2554190_1",
                  "rotateJersey": true
               },
               "options": []
            },
            {
               "participantId": 2554173,
               "name": {
                  "value": "Golden State Warriors",
                  "sign": "VuQt5Q==",
                  "short": "Warriors",
                  "shortSign": "B1X2lw=="
               },
               "image": {
                  "logo": "2554173_logo",
                  "jersey": "2554173_1",
                  "rotateJersey": true
               },
               "options": []
            }
         ],
         "id": "10969350",
         "name": {
            "value": "Portland Trail Blazers at Golden State Warriors",
            "sign": "aysOwQ=="
         },
         "sourceId": 10969350,
         "source": "V1",
         "fixtureType": "Standard",
         "context": "v1|en-us|10969350",
         "addons": {
            "betRadar": 24751450,
            "participantDividend": {}
         },
         "stage": "Live",
         "groupId": 2345473,
         "liveType": "NotSet",
         "liveAlert": true,
         "scoreboard": {
            "totalPoints": {
               "player1": {
                  "1": 30,
                  "255": 30
               },
               "player2": {
                  "1": 36,
                  "255": 36
               }
            },
            "matchType": 2,
            "source": "V1",
            "sportId": 7,
            "scoreboardId": 10969350,
            "period": "Q1",
            "periodId": 1,
            "points": [],
            "turn": "Player01",
            "score": "30:36",
            "indicator": "< 00:15",
            "started": true
         },
         "startDate": "2021-01-04T01:40:00Z",
         "cutOffDate": "2021-01-04T04:40:00Z",
         "sport": {
            "type": "Sport",
            "id": 7,
            "name": {
               "value": "Basketball",
               "sign": "WotGFA=="
            }
         },
         "competition": {
            "parentLeagueId": 6004,
            "statistics": true,
            "sportId": 7,
            "type": "Competition",
            "id": 2541,
            "parentId": 9,
            "name": {
               "value": "NBA",
               "sign": "pL+WdA=="
            }
         },
         "region": {
            "code": "N-A",
            "sportId": 7,
            "type": "Region",
            "id": 9,
            "parentId": 7,
            "name": {
               "value": "USA",
               "sign": "ULdUtw=="
            }
         },
         "viewType": "American",
         "isOpenForBetting": true,
         "isVirtual": false,
         "taggedLocations": [],
         "totalMarketsCount": 27
      },
      {
         "optionMarkets": [],
         "games": [
            {
               "id": 561066240,
               "name": {
                  "value": "Money Line",
                  "sign": "mdY+DA=="
               },
               "results": [
                  {
                     "id": 1629478871,
                     "odds": 3.6,
                     "name": {
                        "value": "Thunder",
                        "sign": "JvJEUA==",
                        "shortSign": ""
                     },
                     "sourceName": {
                        "value": "1",
                        "sign": "q3I7DQ=="
                     },
                     "visibility": "Visible",
                     "numerator": 13,
                     "denominator": 5,
                     "americanOdds": 260
                  },
                  {
                     "id": 1629478872,
                     "odds": 1.3,
                     "name": {
                        "value": "Heat",
                        "sign": "8eBUiQ==",
                        "shortSign": ""
                     },
                     "sourceName": {
                        "value": "2",
                        "sign": "xBCwUg=="
                     },
                     "visibility": "Visible",
                     "numerator": 3,
                     "denominator": 10,
                     "americanOdds": -350
                  }
               ],
               "templateId": 3450,
               "categoryId": 43,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 2.3,
               "category": "Gridable",
               "templateCategory": {
                  "id": 43,
                  "name": {
                     "value": "Money Line",
                     "sign": "mdY+DA=="
                  },
                  "category": "Gridable"
               },
               "isMain": true,
               "grouping": {
                  "gridGroups": [
                     "xt229vkj8"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 2,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "displayType": "Regular"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561391861,
               "name": {
                  "value": "Spread",
                  "sign": "6F5o1w=="
               },
               "results": [
                  {
                     "id": 1630337772,
                     "odds": 1.26,
                     "name": {
                        "value": "Oklahoma City Thunder +16.5",
                        "sign": "L4VkAQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+16.5",
                     "numerator": 4,
                     "denominator": 15,
                     "americanOdds": -375
                  },
                  {
                     "id": 1630337773,
                     "odds": 3.9,
                     "name": {
                        "value": "Miami Heat -16.5",
                        "sign": "gj6FEg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-16.5",
                     "numerator": 29,
                     "denominator": 10,
                     "americanOdds": 290
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 2.64,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "6F5o1w=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561391864,
               "name": {
                  "value": "Spread",
                  "sign": "KI+iJQ=="
               },
               "results": [
                  {
                     "id": 1630337778,
                     "odds": 1.3,
                     "name": {
                        "value": "Oklahoma City Thunder +15.5",
                        "sign": "AMwutQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+15.5",
                     "numerator": 3,
                     "denominator": 10,
                     "americanOdds": -350
                  },
                  {
                     "id": 1630337779,
                     "odds": 3.6,
                     "name": {
                        "value": "Miami Heat -15.5",
                        "sign": "Wc3Bpw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-15.5",
                     "numerator": 13,
                     "denominator": 5,
                     "americanOdds": 260
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 2.3,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "KI+iJQ=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561391867,
               "name": {
                  "value": "Spread",
                  "sign": "aCLaHA=="
               },
               "results": [
                  {
                     "id": 1630337784,
                     "odds": 1.35,
                     "name": {
                        "value": "Oklahoma City Thunder +14.5",
                        "sign": "2vbHbw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+14.5",
                     "numerator": 7,
                     "denominator": 20,
                     "americanOdds": -275
                  },
                  {
                     "id": 1630337785,
                     "odds": 3.25,
                     "name": {
                        "value": "Miami Heat -14.5",
                        "sign": "L57SfQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-14.5",
                     "numerator": 9,
                     "denominator": 4,
                     "americanOdds": 225
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 1.9,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "aCLaHA=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561391870,
               "name": {
                  "value": "Spread",
                  "sign": "qNVTVw=="
               },
               "results": [
                  {
                     "id": 1630337790,
                     "odds": 1.4,
                     "name": {
                        "value": "Oklahoma City Thunder +13.5",
                        "sign": "BNCRIw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+13.5",
                     "numerator": 2,
                     "denominator": 5,
                     "americanOdds": -250
                  },
                  {
                     "id": 1630337791,
                     "odds": 3.0,
                     "name": {
                        "value": "Miami Heat -13.5",
                        "sign": "lZ/SMQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-13.5",
                     "numerator": 2,
                     "denominator": 1,
                     "americanOdds": 200
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 1.6,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "qNVTVw=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561391873,
               "name": {
                  "value": "Spread",
                  "sign": "/PTYsA=="
               },
               "results": [
                  {
                     "id": 1630337796,
                     "odds": 1.45,
                     "name": {
                        "value": "Oklahoma City Thunder +12.5",
                        "sign": "R8Zx6A==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+12.5",
                     "numerator": 5,
                     "denominator": 11,
                     "americanOdds": -225
                  },
                  {
                     "id": 1630337797,
                     "odds": 2.8,
                     "name": {
                        "value": "Miami Heat -12.5",
                        "sign": "5VtjXQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-12.5",
                     "numerator": 9,
                     "denominator": 5,
                     "americanOdds": 180
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 1.35,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "/PTYsA=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561391876,
               "name": {
                  "value": "Spread",
                  "sign": "PANR+w=="
               },
               "results": [
                  {
                     "id": 1630337802,
                     "odds": 1.53,
                     "name": {
                        "value": "Oklahoma City Thunder +11.5",
                        "sign": "XvUi9g==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+11.5",
                     "numerator": 8,
                     "denominator": 15,
                     "americanOdds": -190
                  },
                  {
                     "id": 1630337803,
                     "odds": 2.55,
                     "name": {
                        "value": "Miami Heat -11.5",
                        "sign": "18Z+9Q==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-11.5",
                     "numerator": 31,
                     "denominator": 20,
                     "americanOdds": 155
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 1.02,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "PANR+w=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561391879,
               "name": {
                  "value": "Spread",
                  "sign": "fK4pwg=="
               },
               "results": [
                  {
                     "id": 1630337808,
                     "odds": 1.62,
                     "name": {
                        "value": "Oklahoma City Thunder +10.5",
                        "sign": "srXShg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+10.5",
                     "numerator": 8,
                     "denominator": 13,
                     "americanOdds": -160
                  },
                  {
                     "id": 1630337809,
                     "odds": 2.35,
                     "name": {
                        "value": "Miami Heat -10.5",
                        "sign": "SPs0Mg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-10.5",
                     "numerator": 27,
                     "denominator": 20,
                     "americanOdds": 135
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 0.73,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "fK4pwg=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561391882,
               "name": {
                  "value": "Spread",
                  "sign": "vH/jMA=="
               },
               "results": [
                  {
                     "id": 1630337814,
                     "odds": 1.72,
                     "name": {
                        "value": "Oklahoma City Thunder +9.5",
                        "sign": "rqdNLg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+9.5",
                     "numerator": 8,
                     "denominator": 11,
                     "americanOdds": -140
                  },
                  {
                     "id": 1630337815,
                     "odds": 2.15,
                     "name": {
                        "value": "Miami Heat -9.5",
                        "sign": "Ug6alQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-9.5",
                     "numerator": 23,
                     "denominator": 20,
                     "americanOdds": 115
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 0.43,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "vH/jMA=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561391885,
               "name": {
                  "value": "Spread",
                  "sign": "/EE6VQ=="
               },
               "results": [
                  {
                     "id": 1630337820,
                     "odds": 1.83,
                     "name": {
                        "value": "Oklahoma City Thunder +8.5",
                        "sign": "s/YWvw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+8.5",
                     "numerator": 5,
                     "denominator": 6,
                     "americanOdds": -120
                  },
                  {
                     "id": 1630337821,
                     "odds": 2.0,
                     "name": {
                        "value": "Miami Heat -8.5",
                        "sign": "T/Oo/g==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-8.5",
                     "numerator": 1,
                     "denominator": 1,
                     "americanOdds": 100
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 0.17,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "/EE6VQ=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561391888,
               "name": {
                  "value": "Spread",
                  "sign": "fdoHDg=="
               },
               "results": [
                  {
                     "id": 1630337826,
                     "odds": 2.0,
                     "name": {
                        "value": "Oklahoma City Thunder +7.5",
                        "sign": "bSj8+A==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+7.5",
                     "numerator": 1,
                     "denominator": 1,
                     "americanOdds": 100
                  },
                  {
                     "id": 1630337827,
                     "odds": 1.83,
                     "name": {
                        "value": "Miami Heat -7.5",
                        "sign": "bRFpTQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-7.5",
                     "numerator": 5,
                     "denominator": 6,
                     "americanOdds": -120
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 0.17,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "fdoHDg=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561391891,
               "name": {
                  "value": "Spread",
                  "sign": "PXd/Nw=="
               },
               "results": [
                  {
                     "id": 1630337832,
                     "odds": 2.15,
                     "name": {
                        "value": "Oklahoma City Thunder +6.5",
                        "sign": "cHmnaQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+6.5",
                     "numerator": 23,
                     "denominator": 20,
                     "americanOdds": 115
                  },
                  {
                     "id": 1630337833,
                     "odds": 1.72,
                     "name": {
                        "value": "Miami Heat -6.5",
                        "sign": "cOxbJg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-6.5",
                     "numerator": 8,
                     "denominator": 11,
                     "americanOdds": -140
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 0.43,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "PXd/Nw=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561391894,
               "name": {
                  "value": "Spread",
                  "sign": "/YD2fA=="
               },
               "results": [
                  {
                     "id": 1630337838,
                     "odds": 2.35,
                     "name": {
                        "value": "Oklahoma City Thunder +5.5",
                        "sign": "z9GWGw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+5.5",
                     "numerator": 27,
                     "denominator": 20,
                     "americanOdds": 135
                  },
                  {
                     "id": 1630337839,
                     "odds": 1.6,
                     "name": {
                        "value": "Miami Heat -5.5",
                        "sign": "zyBNAg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-5.5",
                     "numerator": 3,
                     "denominator": 5,
                     "americanOdds": -165
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 0.75,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "/YD2fA=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561391897,
               "name": {
                  "value": "Spread",
                  "sign": "vZhsoA=="
               },
               "results": [
                  {
                     "id": 1630337844,
                     "odds": 2.55,
                     "name": {
                        "value": "Oklahoma City Thunder +4.5",
                        "sign": "vOn7mQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+4.5",
                     "numerator": 31,
                     "denominator": 20,
                     "americanOdds": 155
                  },
                  {
                     "id": 1630337845,
                     "odds": 1.53,
                     "name": {
                        "value": "Miami Heat -4.5",
                        "sign": "wlABhw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-4.5",
                     "numerator": 8,
                     "denominator": 15,
                     "americanOdds": -190
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 1.02,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "vZhsoA=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561391900,
               "name": {
                  "value": "Spread",
                  "sign": "fW/l6w=="
               },
               "results": [
                  {
                     "id": 1630337850,
                     "odds": 2.85,
                     "name": {
                        "value": "Oklahoma City Thunder +3.5",
                        "sign": "aN1Y5Q==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+3.5",
                     "numerator": 15,
                     "denominator": 8,
                     "americanOdds": 185
                  },
                  {
                     "id": 1630337851,
                     "odds": 1.44,
                     "name": {
                        "value": "Miami Heat -3.5",
                        "sign": "KXIh0w==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-3.5",
                     "numerator": 4,
                     "denominator": 9,
                     "americanOdds": -225
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 1.41,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "fW/l6w=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561391903,
               "name": {
                  "value": "Spread",
                  "sign": "PcKd0g=="
               },
               "results": [
                  {
                     "id": 1630337856,
                     "odds": 3.2,
                     "name": {
                        "value": "Oklahoma City Thunder +2.5",
                        "sign": "f5KADQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+2.5",
                     "numerator": 11,
                     "denominator": 5,
                     "americanOdds": 220
                  },
                  {
                     "id": 1630337857,
                     "odds": 1.36,
                     "name": {
                        "value": "Miami Heat -2.5",
                        "sign": "hyb5Xg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-2.5",
                     "numerator": 4,
                     "denominator": 11,
                     "americanOdds": -275
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 1.84,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "PcKd0g=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561391906,
               "name": {
                  "value": "Spread",
                  "sign": "f8s/AQ=="
               },
               "results": [
                  {
                     "id": 1630337862,
                     "odds": 3.4,
                     "name": {
                        "value": "Oklahoma City Thunder +1.5",
                        "sign": "wDqxfw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+1.5",
                     "numerator": 12,
                     "denominator": 5,
                     "americanOdds": 240
                  },
                  {
                     "id": 1630337863,
                     "odds": 1.33,
                     "name": {
                        "value": "Miami Heat -1.5",
                        "sign": "OOrveg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-1.5",
                     "numerator": 1,
                     "denominator": 3,
                     "americanOdds": -300
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 2.07,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "f8s/AQ=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561391907,
               "name": {
                  "value": "Spread",
                  "sign": "v68XFg=="
               },
               "results": [
                  {
                     "id": 1630337864,
                     "odds": 4.0,
                     "name": {
                        "value": "Oklahoma City Thunder -1.5",
                        "sign": "TUc2Iw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-1.5",
                     "numerator": 3,
                     "denominator": 1,
                     "americanOdds": 300
                  },
                  {
                     "id": 1630337865,
                     "odds": 1.26,
                     "name": {
                        "value": "Miami Heat +1.5",
                        "sign": "ioHrDg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+1.5",
                     "numerator": 4,
                     "denominator": 15,
                     "americanOdds": -375
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 2.74,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "v68XFg=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561391910,
               "name": {
                  "value": "Spread",
                  "sign": "f1ieXQ=="
               },
               "results": [
                  {
                     "id": 1630337870,
                     "odds": 4.4,
                     "name": {
                        "value": "Oklahoma City Thunder -2.5",
                        "sign": "8u8HUQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-2.5",
                     "numerator": 17,
                     "denominator": 5,
                     "americanOdds": 340
                  },
                  {
                     "id": 1630337871,
                     "odds": 1.22,
                     "name": {
                        "value": "Miami Heat +2.5",
                        "sign": "NU39Kg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+2.5",
                     "numerator": 2,
                     "denominator": 9,
                     "americanOdds": -450
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 3.18,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "f1ieXQ=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561391913,
               "name": {
                  "value": "Spread",
                  "sign": "P0AEgQ=="
               },
               "results": [
                  {
                     "id": 1630337876,
                     "odds": 4.75,
                     "name": {
                        "value": "Oklahoma City Thunder -3.5",
                        "sign": "gddq0w==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-3.5",
                     "numerator": 15,
                     "denominator": 4,
                     "americanOdds": 375
                  },
                  {
                     "id": 1630337877,
                     "odds": 1.2,
                     "name": {
                        "value": "Miami Heat +3.5",
                        "sign": "OD2xrw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+3.5",
                     "numerator": 1,
                     "denominator": 5,
                     "americanOdds": -500
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 3.55,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "P0AEgQ=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561391916,
               "name": {
                  "value": "Spread",
                  "sign": "/7eNyg=="
               },
               "results": [
                  {
                     "id": 1630337882,
                     "odds": 5.5,
                     "name": {
                        "value": "Oklahoma City Thunder -4.5",
                        "sign": "VePJrw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-4.5",
                     "numerator": 9,
                     "denominator": 2,
                     "americanOdds": 450
                  },
                  {
                     "id": 1630337883,
                     "odds": 1.16,
                     "name": {
                        "value": "Miami Heat +4.5",
                        "sign": "0x+R+w==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+4.5",
                     "numerator": 2,
                     "denominator": 13,
                     "americanOdds": -650
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 4.34,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "/7eNyg=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561391919,
               "name": {
                  "value": "Totals",
                  "sign": "3iZfGA=="
               },
               "results": [
                  {
                     "id": 1630337888,
                     "odds": 1.4,
                     "name": {
                        "value": "Over 203.5",
                        "sign": "uuj8mg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 2,
                     "denominator": 5,
                     "americanOdds": -250
                  },
                  {
                     "id": 1630337889,
                     "odds": 3.0,
                     "name": {
                        "value": "Under 203.5",
                        "sign": "jrvf9w==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 2,
                     "denominator": 1,
                     "americanOdds": 200
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "203.5",
               "spread": 1.6,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "3iZfGA=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561391922,
               "name": {
                  "value": "Totals",
                  "sign": "X71iQw=="
               },
               "results": [
                  {
                     "id": 1630337894,
                     "odds": 1.42,
                     "name": {
                        "value": "Over 204.5",
                        "sign": "jiNtoA==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 21,
                     "denominator": 50,
                     "americanOdds": -250
                  },
                  {
                     "id": 1630337895,
                     "odds": 2.95,
                     "name": {
                        "value": "Under 204.5",
                        "sign": "zPDJTg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 19,
                     "denominator": 10,
                     "americanOdds": 195
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "204.5",
               "spread": 1.53,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "X71iQw=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561391925,
               "name": {
                  "value": "Totals",
                  "sign": "H4O7Jg=="
               },
               "results": [
                  {
                     "id": 1630337900,
                     "odds": 1.45,
                     "name": {
                        "value": "Over 205.5",
                        "sign": "ai/M4Q==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 5,
                     "denominator": 11,
                     "americanOdds": -225
                  },
                  {
                     "id": 1630337901,
                     "odds": 2.8,
                     "name": {
                        "value": "Under 205.5",
                        "sign": "83qRUA==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 9,
                     "denominator": 5,
                     "americanOdds": 180
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "205.5",
               "spread": 1.35,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "H4O7Jg=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561391928,
               "name": {
                  "value": "Totals",
                  "sign": "31Jx1A=="
               },
               "results": [
                  {
                     "id": 1630337906,
                     "odds": 1.5,
                     "name": {
                        "value": "Over 206.5",
                        "sign": "RjovIw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 1,
                     "denominator": 2,
                     "americanOdds": -200
                  },
                  {
                     "id": 1630337907,
                     "odds": 2.65,
                     "name": {
                        "value": "Under 206.5",
                        "sign": "suR4cg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 33,
                     "denominator": 20,
                     "americanOdds": 165
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "206.5",
               "spread": 1.15,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "31Jx1A=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561391931,
               "name": {
                  "value": "Totals",
                  "sign": "n/8J7Q=="
               },
               "results": [
                  {
                     "id": 1630337912,
                     "odds": 1.55,
                     "name": {
                        "value": "Over 207.5",
                        "sign": "ojaOYg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 11,
                     "denominator": 20,
                     "americanOdds": -185
                  },
                  {
                     "id": 1630337913,
                     "odds": 2.5,
                     "name": {
                        "value": "Under 207.5",
                        "sign": "jW4gbA==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 6,
                     "denominator": 4,
                     "americanOdds": 150
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "207.5",
               "spread": 0.95,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "n/8J7Q=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561391934,
               "name": {
                  "value": "Totals",
                  "sign": "XwiApg=="
               },
               "results": [
                  {
                     "id": 1630337918,
                     "odds": 1.6,
                     "name": {
                        "value": "Over 208.5",
                        "sign": "LqwMVg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 3,
                     "denominator": 5,
                     "americanOdds": -165
                  },
                  {
                     "id": 1630337919,
                     "odds": 2.4,
                     "name": {
                        "value": "Under 208.5",
                        "sign": "d3Ql2w==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 7,
                     "denominator": 5,
                     "americanOdds": 140
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "208.5",
               "spread": 0.8,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "XwiApg=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561391937,
               "name": {
                  "value": "Totals",
                  "sign": "Wus8kQ=="
               },
               "results": [
                  {
                     "id": 1630337924,
                     "odds": 1.65,
                     "name": {
                        "value": "Over 209.5",
                        "sign": "J+Q1pA==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 13,
                     "denominator": 20,
                     "americanOdds": -155
                  },
                  {
                     "id": 1630337925,
                     "odds": 2.25,
                     "name": {
                        "value": "Under 209.5",
                        "sign": "yfh1Gw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 5,
                     "denominator": 4,
                     "americanOdds": 125
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "209.5",
               "spread": 0.6,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "Wus8kQ=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561391940,
               "name": {
                  "value": "Totals",
                  "sign": "mhy12g=="
               },
               "results": [
                  {
                     "id": 1630337930,
                     "odds": 1.7,
                     "name": {
                        "value": "Over 210.5",
                        "sign": "wEiveQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 7,
                     "denominator": 10,
                     "americanOdds": -145
                  },
                  {
                     "id": 1630337931,
                     "odds": 2.2,
                     "name": {
                        "value": "Under 210.5",
                        "sign": "bi/D0g==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 6,
                     "denominator": 5,
                     "americanOdds": 120
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "210.5",
               "spread": 0.5,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "mhy12g=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561391943,
               "name": {
                  "value": "Totals",
                  "sign": "2rHN4w=="
               },
               "results": [
                  {
                     "id": 1630337936,
                     "odds": 1.75,
                     "name": {
                        "value": "Over 211.5",
                        "sign": "XB9ckg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 3,
                     "denominator": 4,
                     "americanOdds": -135
                  },
                  {
                     "id": 1630337937,
                     "odds": 2.1,
                     "name": {
                        "value": "Under 211.5",
                        "sign": "BA7vkg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 11,
                     "denominator": 10,
                     "americanOdds": 110
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "211.5",
               "spread": 0.35,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "2rHN4w=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561391946,
               "name": {
                  "value": "Totals",
                  "sign": "GmAHEQ=="
               },
               "results": [
                  {
                     "id": 1630337942,
                     "odds": 1.83,
                     "name": {
                        "value": "Over 212.5",
                        "sign": "tHzErw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 5,
                     "denominator": 6,
                     "americanOdds": -120
                  },
                  {
                     "id": 1630337943,
                     "odds": 2.0,
                     "name": {
                        "value": "Under 212.5",
                        "sign": "mu3wLA==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 1,
                     "denominator": 1,
                     "americanOdds": 100
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "212.5",
               "spread": 0.17,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "GmAHEQ=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561391949,
               "name": {
                  "value": "Totals",
                  "sign": "Wl7edA=="
               },
               "results": [
                  {
                     "id": 1630337948,
                     "odds": 1.91,
                     "name": {
                        "value": "Over 213.5",
                        "sign": "UHBl7g==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 10,
                     "denominator": 11,
                     "americanOdds": -110
                  },
                  {
                     "id": 1630337949,
                     "odds": 1.91,
                     "name": {
                        "value": "Under 213.5",
                        "sign": "pWeoMg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 10,
                     "denominator": 11,
                     "americanOdds": -110
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "213.5",
               "spread": 0.0,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "Wl7edA=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561391952,
               "name": {
                  "value": "Totals",
                  "sign": "28XjLw=="
               },
               "results": [
                  {
                     "id": 1630337954,
                     "odds": 1.95,
                     "name": {
                        "value": "Over 214.5",
                        "sign": "EX1apA==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 19,
                     "denominator": 20,
                     "americanOdds": -105
                  },
                  {
                     "id": 1630337955,
                     "odds": 1.87,
                     "name": {
                        "value": "Under 214.5",
                        "sign": "kgehqw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 20,
                     "denominator": 23,
                     "americanOdds": -115
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "214.5",
               "spread": 0.08,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "28XjLw=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561391955,
               "name": {
                  "value": "Totals",
                  "sign": "m2ibFg=="
               },
               "results": [
                  {
                     "id": 1630337960,
                     "odds": 2.05,
                     "name": {
                        "value": "Over 215.5",
                        "sign": "9XH75Q==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 21,
                     "denominator": 20,
                     "americanOdds": 105
                  },
                  {
                     "id": 1630337961,
                     "odds": 1.8,
                     "name": {
                        "value": "Under 215.5",
                        "sign": "rY35tQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 4,
                     "denominator": 5,
                     "americanOdds": -125
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "215.5",
               "spread": 0.25,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "m2ibFg=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561391958,
               "name": {
                  "value": "Totals",
                  "sign": "W58SXQ=="
               },
               "results": [
                  {
                     "id": 1630337966,
                     "odds": 2.15,
                     "name": {
                        "value": "Over 216.5",
                        "sign": "HRJj2A==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 23,
                     "denominator": 20,
                     "americanOdds": 115
                  },
                  {
                     "id": 1630337967,
                     "odds": 1.72,
                     "name": {
                        "value": "Under 216.5",
                        "sign": "M27mCw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 8,
                     "denominator": 11,
                     "americanOdds": -140
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "216.5",
               "spread": 0.43,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "W58SXQ=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561391961,
               "name": {
                  "value": "Totals",
                  "sign": "G4eIgQ=="
               },
               "results": [
                  {
                     "id": 1630337972,
                     "odds": 2.25,
                     "name": {
                        "value": "Over 217.5",
                        "sign": "gUWQMw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 5,
                     "denominator": 4,
                     "americanOdds": 125
                  },
                  {
                     "id": 1630337973,
                     "odds": 1.67,
                     "name": {
                        "value": "Under 217.5",
                        "sign": "WU/KSw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 4,
                     "denominator": 6,
                     "americanOdds": -150
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "217.5",
               "spread": 0.58,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "G4eIgQ=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561391964,
               "name": {
                  "value": "Totals",
                  "sign": "23AByg=="
               },
               "results": [
                  {
                     "id": 1630337978,
                     "odds": 2.35,
                     "name": {
                        "value": "Over 218.5",
                        "sign": "sfI7Ug==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 27,
                     "denominator": 20,
                     "americanOdds": 135
                  },
                  {
                     "id": 1630337979,
                     "odds": 1.62,
                     "name": {
                        "value": "Under 218.5",
                        "sign": "KYNNPg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 8,
                     "denominator": 13,
                     "americanOdds": -160
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "218.5",
               "spread": 0.73,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "23AByg=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561391967,
               "name": {
                  "value": "Totals",
                  "sign": "m9158w=="
               },
               "results": [
                  {
                     "id": 1630337984,
                     "odds": 2.45,
                     "name": {
                        "value": "Over 219.5",
                        "sign": "v3LH8g==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 29,
                     "denominator": 20,
                     "americanOdds": 145
                  },
                  {
                     "id": 1630337985,
                     "odds": 1.57,
                     "name": {
                        "value": "Under 219.5",
                        "sign": "/F8rYA==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 4,
                     "denominator": 7,
                     "americanOdds": -175
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "219.5",
               "spread": 0.88,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "m9158w=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561391971,
               "name": {
                  "value": "Totals",
                  "sign": "GbDzNw=="
               },
               "results": [
                  {
                     "id": 1630337992,
                     "odds": 2.55,
                     "name": {
                        "value": "Over 220.5",
                        "sign": "YsuKug==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 31,
                     "denominator": 20,
                     "americanOdds": 155
                  },
                  {
                     "id": 1630337993,
                     "odds": 1.53,
                     "name": {
                        "value": "Under 220.5",
                        "sign": "Fx3Nvw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 8,
                     "denominator": 15,
                     "americanOdds": -190
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "220.5",
               "spread": 1.02,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "GbDzNw=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561391974,
               "name": {
                  "value": "Totals",
                  "sign": "2Ud6fA=="
               },
               "results": [
                  {
                     "id": 1630337998,
                     "odds": 2.7,
                     "name": {
                        "value": "Over 221.5",
                        "sign": "5HyWhA==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 17,
                     "denominator": 10,
                     "americanOdds": 170
                  },
                  {
                     "id": 1630337999,
                     "odds": 1.48,
                     "name": {
                        "value": "Under 221.5",
                        "sign": "5ypWAg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 10,
                     "denominator": 21,
                     "americanOdds": -200
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "221.5",
               "spread": 1.22,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "2Ud6fA=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561391977,
               "name": {
                  "value": "Totals",
                  "sign": "mV/goA=="
               },
               "results": [
                  {
                     "id": 1630338004,
                     "odds": 2.8,
                     "name": {
                        "value": "Over 222.5",
                        "sign": "Fv/hbA==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 9,
                     "denominator": 5,
                     "americanOdds": 180
                  },
                  {
                     "id": 1630338005,
                     "odds": 1.45,
                     "name": {
                        "value": "Under 222.5",
                        "sign": "49/+QQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 5,
                     "denominator": 11,
                     "americanOdds": -225
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "222.5",
               "spread": 1.35,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "mV/goA=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            }
         ],
         "participants": [
            {
               "participantId": 2559027,
               "name": {
                  "value": "Oklahoma City Thunder",
                  "sign": "zLpw7Q==",
                  "short": "Thunder",
                  "shortSign": "bQQy+g=="
               },
               "image": {
                  "logo": "2559027_logo",
                  "jersey": "2559027_1",
                  "rotateJersey": true
               },
               "options": []
            },
            {
               "participantId": 2554180,
               "name": {
                  "value": "Miami Heat",
                  "sign": "/j8W2A==",
                  "short": "Heat",
                  "shortSign": "nCuoFQ=="
               },
               "image": {
                  "logo": "2554180_logo",
                  "jersey": "2554180_1",
                  "rotateJersey": true
               },
               "options": []
            }
         ],
         "id": "10974869",
         "name": {
            "value": "Oklahoma City Thunder at Miami Heat",
            "sign": "mIehww=="
         },
         "sourceId": 10974869,
         "source": "V1",
         "fixtureType": "Standard",
         "context": "v1|en-us|10974869",
         "addons": {
            "participantDividend": {}
         },
         "stage": "PreMatch",
         "groupId": 2346893,
         "liveType": "NotSet",
         "liveAlert": true,
         "startDate": "2021-01-05T00:35:00Z",
         "cutOffDate": "2021-01-05T00:35:00Z",
         "sport": {
            "type": "Sport",
            "id": 7,
            "name": {
               "value": "Basketball",
               "sign": "WotGFA=="
            }
         },
         "competition": {
            "parentLeagueId": 6004,
            "statistics": true,
            "sportId": 7,
            "type": "Competition",
            "id": 6004,
            "parentId": 9,
            "name": {
               "value": "NBA",
               "sign": "pL+WdA=="
            }
         },
         "region": {
            "code": "N-A",
            "sportId": 7,
            "type": "Region",
            "id": 9,
            "parentId": 7,
            "name": {
               "value": "USA",
               "sign": "ULdUtw=="
            }
         },
         "viewType": "American",
         "isOpenForBetting": true,
         "isVirtual": false,
         "taggedLocations": [],
         "totalMarketsCount": 68
      },
      {
         "optionMarkets": [],
         "games": [
            {
               "id": 561066241,
               "name": {
                  "value": "Money Line",
                  "sign": "DnAj6w=="
               },
               "results": [
                  {
                     "id": 1629478873,
                     "odds": 2.95,
                     "name": {
                        "value": "Knicks",
                        "sign": "oIHAmQ==",
                        "shortSign": ""
                     },
                     "sourceName": {
                        "value": "1",
                        "sign": "PYofTA=="
                     },
                     "visibility": "Visible",
                     "numerator": 19,
                     "denominator": 10,
                     "americanOdds": 195
                  },
                  {
                     "id": 1629478874,
                     "odds": 1.42,
                     "name": {
                        "value": "Hawks",
                        "sign": "Fx5qbw==",
                        "shortSign": ""
                     },
                     "sourceName": {
                        "value": "2",
                        "sign": "A4CMhg=="
                     },
                     "visibility": "Visible",
                     "numerator": 21,
                     "denominator": 50,
                     "americanOdds": -250
                  }
               ],
               "templateId": 3450,
               "categoryId": 43,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 1.53,
               "category": "Gridable",
               "templateCategory": {
                  "id": 43,
                  "name": {
                     "value": "Money Line",
                     "sign": "DnAj6w=="
                  },
                  "category": "Gridable"
               },
               "isMain": true,
               "grouping": {
                  "gridGroups": [
                     "xt229vkj8"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 2,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "displayType": "Regular"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561393135,
               "name": {
                  "value": "Spread",
                  "sign": "DHyvZA=="
               },
               "results": [
                  {
                     "id": 1630340725,
                     "odds": 1.25,
                     "name": {
                        "value": "New York Knicks +14.5",
                        "sign": "dphWMA==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+14.5",
                     "numerator": 1,
                     "denominator": 4,
                     "americanOdds": -400
                  },
                  {
                     "id": 1630340726,
                     "odds": 4.1,
                     "name": {
                        "value": "Atlanta Hawks -14.5",
                        "sign": "DJjZTA==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-14.5",
                     "numerator": 31,
                     "denominator": 10,
                     "americanOdds": 310
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 2.85,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "DHyvZA=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561393138,
               "name": {
                  "value": "Spread",
                  "sign": "jeeSPw=="
               },
               "results": [
                  {
                     "id": 1630340731,
                     "odds": 1.28,
                     "name": {
                        "value": "New York Knicks +13.5",
                        "sign": "TV19XQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+13.5",
                     "numerator": 2,
                     "denominator": 7,
                     "americanOdds": -350
                  },
                  {
                     "id": 1630340732,
                     "odds": 3.8,
                     "name": {
                        "value": "Atlanta Hawks -13.5",
                        "sign": "XQaexw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-13.5",
                     "numerator": 14,
                     "denominator": 5,
                     "americanOdds": 280
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 2.52,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "jeeSPw=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561393141,
               "name": {
                  "value": "Spread",
                  "sign": "zdlLWg=="
               },
               "results": [
                  {
                     "id": 1630340737,
                     "odds": 1.33,
                     "name": {
                        "value": "New York Knicks +12.5",
                        "sign": "Y8pMCA==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+12.5",
                     "numerator": 1,
                     "denominator": 3,
                     "americanOdds": -300
                  },
                  {
                     "id": 1630340738,
                     "odds": 3.4,
                     "name": {
                        "value": "Atlanta Hawks -12.5",
                        "sign": "N0QZ5g==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-12.5",
                     "numerator": 12,
                     "denominator": 5,
                     "americanOdds": 240
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 2.07,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "zdlLWg=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561393144,
               "name": {
                  "value": "Spread",
                  "sign": "DQiBqA=="
               },
               "results": [
                  {
                     "id": 1630340743,
                     "odds": 1.36,
                     "name": {
                        "value": "New York Knicks +11.5",
                        "sign": "+grIvg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+11.5",
                     "numerator": 4,
                     "denominator": 11,
                     "americanOdds": -275
                  },
                  {
                     "id": 1630340744,
                     "odds": 3.2,
                     "name": {
                        "value": "Atlanta Hawks -11.5",
                        "sign": "unJXag==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-11.5",
                     "numerator": 11,
                     "denominator": 5,
                     "americanOdds": 220
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 1.84,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "DQiBqA=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561393147,
               "name": {
                  "value": "Spread",
                  "sign": "TaX5kQ=="
               },
               "results": [
                  {
                     "id": 1630340749,
                     "odds": 1.44,
                     "name": {
                        "value": "New York Knicks +10.5",
                        "sign": "zOU9uQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+10.5",
                     "numerator": 4,
                     "denominator": 9,
                     "americanOdds": -225
                  },
                  {
                     "id": 1630340750,
                     "odds": 2.85,
                     "name": {
                        "value": "Atlanta Hawks -10.5",
                        "sign": "/mJCpw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-10.5",
                     "numerator": 15,
                     "denominator": 8,
                     "americanOdds": 185
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 1.41,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "TaX5kQ=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561393150,
               "name": {
                  "value": "Spread",
                  "sign": "jVJw2g=="
               },
               "results": [
                  {
                     "id": 1630340755,
                     "odds": 1.5,
                     "name": {
                        "value": "New York Knicks +9.5",
                        "sign": "YDZNXA==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+9.5",
                     "numerator": 1,
                     "denominator": 2,
                     "americanOdds": -200
                  },
                  {
                     "id": 1630340756,
                     "odds": 2.65,
                     "name": {
                        "value": "Atlanta Hawks -9.5",
                        "sign": "QnzI6Q==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-9.5",
                     "numerator": 33,
                     "denominator": 20,
                     "americanOdds": 165
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 1.15,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "jVJw2g=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561393153,
               "name": {
                  "value": "Spread",
                  "sign": "pHI/2Q=="
               },
               "results": [
                  {
                     "id": 1630340761,
                     "odds": 1.6,
                     "name": {
                        "value": "New York Knicks +8.5",
                        "sign": "EkK/3A==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+8.5",
                     "numerator": 3,
                     "denominator": 5,
                     "americanOdds": -165
                  },
                  {
                     "id": 1630340762,
                     "odds": 2.4,
                     "name": {
                        "value": "Atlanta Hawks -8.5",
                        "sign": "MH9kzA==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-8.5",
                     "numerator": 7,
                     "denominator": 5,
                     "americanOdds": 140
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 0.8,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "pHI/2Q=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561393156,
               "name": {
                  "value": "Spread",
                  "sign": "ZIW2kg=="
               },
               "results": [
                  {
                     "id": 1630340767,
                     "odds": 1.7,
                     "name": {
                        "value": "New York Knicks +7.5",
                        "sign": "0/IjHg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+7.5",
                     "numerator": 7,
                     "denominator": 10,
                     "americanOdds": -145
                  },
                  {
                     "id": 1630340768,
                     "odds": 2.2,
                     "name": {
                        "value": "Atlanta Hawks -7.5",
                        "sign": "dT+FHw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-7.5",
                     "numerator": 6,
                     "denominator": 5,
                     "americanOdds": 120
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 0.5,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "ZIW2kg=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561393159,
               "name": {
                  "value": "Spread",
                  "sign": "JCjOqw=="
               },
               "results": [
                  {
                     "id": 1630340773,
                     "odds": 1.83,
                     "name": {
                        "value": "New York Knicks +6.5",
                        "sign": "BCZjZg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+6.5",
                     "numerator": 5,
                     "denominator": 6,
                     "americanOdds": -120
                  },
                  {
                     "id": 1630340774,
                     "odds": 2.0,
                     "name": {
                        "value": "Atlanta Hawks -6.5",
                        "sign": "9rNKxQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-6.5",
                     "numerator": 1,
                     "denominator": 1,
                     "americanOdds": 100
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 0.17,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "JCjOqw=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561393162,
               "name": {
                  "value": "Spread",
                  "sign": "5PkEWQ=="
               },
               "results": [
                  {
                     "id": 1630340779,
                     "odds": 2.0,
                     "name": {
                        "value": "New York Knicks +5.5",
                        "sign": "j95F3Q==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+5.5",
                     "numerator": 1,
                     "denominator": 1,
                     "americanOdds": 100
                  },
                  {
                     "id": 1630340780,
                     "odds": 1.83,
                     "name": {
                        "value": "Atlanta Hawks -5.5",
                        "sign": "MiBrcQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-5.5",
                     "numerator": 5,
                     "denominator": 6,
                     "americanOdds": -120
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 0.17,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "5PkEWQ=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561393165,
               "name": {
                  "value": "Spread",
                  "sign": "pMfdPA=="
               },
               "results": [
                  {
                     "id": 1630340785,
                     "odds": 2.15,
                     "name": {
                        "value": "New York Knicks +4.5",
                        "sign": "ocj2vA==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+4.5",
                     "numerator": 23,
                     "denominator": 20,
                     "americanOdds": 115
                  },
                  {
                     "id": 1630340786,
                     "odds": 1.72,
                     "name": {
                        "value": "Atlanta Hawks -4.5",
                        "sign": "4zpxcQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-4.5",
                     "numerator": 8,
                     "denominator": 11,
                     "americanOdds": -140
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 0.43,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "pMfdPA=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561393168,
               "name": {
                  "value": "Spread",
                  "sign": "JVzgZw=="
               },
               "results": [
                  {
                     "id": 1630340791,
                     "odds": 2.35,
                     "name": {
                        "value": "New York Knicks +3.5",
                        "sign": "2Cl5cA==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+3.5",
                     "numerator": 27,
                     "denominator": 20,
                     "americanOdds": 135
                  },
                  {
                     "id": 1630340792,
                     "odds": 1.62,
                     "name": {
                        "value": "Atlanta Hawks -3.5",
                        "sign": "+wFZwg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-3.5",
                     "numerator": 8,
                     "denominator": 13,
                     "americanOdds": -160
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 0.73,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "JVzgZw=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561393172,
               "name": {
                  "value": "Spread",
                  "sign": "Jc9BOw=="
               },
               "results": [
                  {
                     "id": 1630340799,
                     "odds": 2.55,
                     "name": {
                        "value": "New York Knicks +2.5",
                        "sign": "wfIbAQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+2.5",
                     "numerator": 31,
                     "denominator": 20,
                     "americanOdds": 155
                  },
                  {
                     "id": 1630340800,
                     "odds": 1.53,
                     "name": {
                        "value": "Atlanta Hawks -2.5",
                        "sign": "VKj6xA==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-2.5",
                     "numerator": 8,
                     "denominator": 15,
                     "americanOdds": -190
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 1.02,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "Jc9BOw=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561393176,
               "name": {
                  "value": "Spread",
                  "sign": "JXqj3g=="
               },
               "results": [
                  {
                     "id": 1630340807,
                     "odds": 2.7,
                     "name": {
                        "value": "New York Knicks +1.5",
                        "sign": "4diouA==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+1.5",
                     "numerator": 17,
                     "denominator": 10,
                     "americanOdds": 170
                  },
                  {
                     "id": 1630340808,
                     "odds": 1.48,
                     "name": {
                        "value": "Atlanta Hawks -1.5",
                        "sign": "/JnfOQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-1.5",
                     "numerator": 10,
                     "denominator": 21,
                     "americanOdds": -200
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 1.22,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "JXqj3g=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561393177,
               "name": {
                  "value": "Spread",
                  "sign": "5R6LyQ=="
               },
               "results": [
                  {
                     "id": 1630340809,
                     "odds": 3.2,
                     "name": {
                        "value": "New York Knicks -1.5",
                        "sign": "78GjJA==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-1.5",
                     "numerator": 11,
                     "denominator": 5,
                     "americanOdds": 220
                  },
                  {
                     "id": 1630340810,
                     "odds": 1.36,
                     "name": {
                        "value": "Atlanta Hawks +1.5",
                        "sign": "TGSwVQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+1.5",
                     "numerator": 4,
                     "denominator": 11,
                     "americanOdds": -275
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 1.84,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "5R6LyQ=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561393181,
               "name": {
                  "value": "Spread",
                  "sign": "5Y0qlQ=="
               },
               "results": [
                  {
                     "id": 1630340817,
                     "odds": 3.4,
                     "name": {
                        "value": "New York Knicks -2.5",
                        "sign": "xKwEtw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-2.5",
                     "numerator": 12,
                     "denominator": 5,
                     "americanOdds": 240
                  },
                  {
                     "id": 1630340818,
                     "odds": 1.33,
                     "name": {
                        "value": "Atlanta Hawks +2.5",
                        "sign": "R0wjjQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+2.5",
                     "numerator": 1,
                     "denominator": 3,
                     "americanOdds": -300
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 2.07,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "5Y0qlQ=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561393184,
               "name": {
                  "value": "Spread",
                  "sign": "p4SIRg=="
               },
               "results": [
                  {
                     "id": 1630340823,
                     "odds": 3.8,
                     "name": {
                        "value": "New York Knicks -3.5",
                        "sign": "DzEGfw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-3.5",
                     "numerator": 14,
                     "denominator": 5,
                     "americanOdds": 280
                  },
                  {
                     "id": 1630340824,
                     "odds": 1.28,
                     "name": {
                        "value": "Atlanta Hawks +3.5",
                        "sign": "7QuGOg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+3.5",
                     "numerator": 2,
                     "denominator": 7,
                     "americanOdds": -350
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 2.52,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "p4SIRg=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561393188,
               "name": {
                  "value": "Spread",
                  "sign": "pxcpGg=="
               },
               "results": [
                  {
                     "id": 1630340831,
                     "odds": 4.33,
                     "name": {
                        "value": "New York Knicks -4.5",
                        "sign": "pJbpCg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-4.5",
                     "numerator": 10,
                     "denominator": 3,
                     "americanOdds": 333
                  },
                  {
                     "id": 1630340832,
                     "odds": 1.22,
                     "name": {
                        "value": "Atlanta Hawks +4.5",
                        "sign": "fLhwrg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+4.5",
                     "numerator": 2,
                     "denominator": 9,
                     "americanOdds": -450
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 3.11,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "pxcpGg=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561393191,
               "name": {
                  "value": "Spread",
                  "sign": "57pRIw=="
               },
               "results": [
                  {
                     "id": 1630340837,
                     "odds": 4.75,
                     "name": {
                        "value": "New York Knicks -5.5",
                        "sign": "c0Kpcg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-5.5",
                     "numerator": 15,
                     "denominator": 4,
                     "americanOdds": 375
                  },
                  {
                     "id": 1630340838,
                     "odds": 1.2,
                     "name": {
                        "value": "Atlanta Hawks +5.5",
                        "sign": "/zS/dA==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+5.5",
                     "numerator": 1,
                     "denominator": 5,
                     "americanOdds": -500
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 3.55,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "57pRIw=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561393194,
               "name": {
                  "value": "Spread",
                  "sign": "J2ub0Q=="
               },
               "results": [
                  {
                     "id": 1630340843,
                     "odds": 5.5,
                     "name": {
                        "value": "New York Knicks -6.5",
                        "sign": "+LqPyQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "-6.5",
                     "numerator": 9,
                     "denominator": 2,
                     "americanOdds": 450
                  },
                  {
                     "id": 1630340844,
                     "odds": 1.16,
                     "name": {
                        "value": "Atlanta Hawks +6.5",
                        "sign": "O6eewA==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "attr": "+6.5",
                     "numerator": 2,
                     "denominator": 13,
                     "americanOdds": -650
                  }
               ],
               "templateId": 8098,
               "categoryId": 44,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "spread": 4.34,
               "category": "Gridable",
               "templateCategory": {
                  "id": 44,
                  "name": {
                     "value": "Spread",
                     "sign": "J2ub0Q=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "1n91ubk9k"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 7,
                        "subIndex": 0,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     },
                     {
                        "group": 2,
                        "index": 1,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Spread",
                        "displayType": "Spread"
                     }
                  ],
                  "parameters": {}
               }
            },
            {
               "id": 561393199,
               "name": {
                  "value": "Totals",
                  "sign": "hqC4cQ=="
               },
               "results": [
                  {
                     "id": 1630340853,
                     "odds": 1.36,
                     "name": {
                        "value": "Over 211.5",
                        "sign": "Ghf8GQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 4,
                     "denominator": 11,
                     "americanOdds": -275
                  },
                  {
                     "id": 1630340854,
                     "odds": 3.2,
                     "name": {
                        "value": "Under 211.5",
                        "sign": "K3zD0A==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 11,
                     "denominator": 5,
                     "americanOdds": 220
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "211.5",
               "spread": 1.84,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "hqC4cQ=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561393202,
               "name": {
                  "value": "Totals",
                  "sign": "BzuFKg=="
               },
               "results": [
                  {
                     "id": 1630340859,
                     "odds": 1.4,
                     "name": {
                        "value": "Over 212.5",
                        "sign": "TllNcQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 2,
                     "denominator": 5,
                     "americanOdds": -250
                  },
                  {
                     "id": 1630340860,
                     "odds": 3.0,
                     "name": {
                        "value": "Under 212.5",
                        "sign": "eiIfzQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 2,
                     "denominator": 1,
                     "americanOdds": 200
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "212.5",
               "spread": 1.6,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "BzuFKg=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561393205,
               "name": {
                  "value": "Totals",
                  "sign": "RwVcTw=="
               },
               "results": [
                  {
                     "id": 1630340865,
                     "odds": 1.44,
                     "name": {
                        "value": "Over 213.5",
                        "sign": "SYH/Jg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 4,
                     "denominator": 9,
                     "americanOdds": -225
                  },
                  {
                     "id": 1630340866,
                     "odds": 2.85,
                     "name": {
                        "value": "Under 213.5",
                        "sign": "FmMTiw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 15,
                     "denominator": 8,
                     "americanOdds": 185
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "213.5",
               "spread": 1.41,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "RwVcTw=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561393209,
               "name": {
                  "value": "Totals",
                  "sign": "R7C+qg=="
               },
               "results": [
                  {
                     "id": 1630340873,
                     "odds": 1.48,
                     "name": {
                        "value": "Over 214.5",
                        "sign": "cLqZdg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 10,
                     "denominator": 21,
                     "americanOdds": -200
                  },
                  {
                     "id": 1630340874,
                     "odds": 2.7,
                     "name": {
                        "value": "Under 214.5",
                        "sign": "GaPeTA==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 17,
                     "denominator": 10,
                     "americanOdds": 170
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "214.5",
               "spread": 1.22,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "R7C+qg=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561393220,
               "name": {
                  "value": "Totals",
                  "sign": "wppSsw=="
               },
               "results": [
                  {
                     "id": 1630340895,
                     "odds": 1.53,
                     "name": {
                        "value": "Over 215.5",
                        "sign": "jlbX4g==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 8,
                     "denominator": 15,
                     "americanOdds": -190
                  },
                  {
                     "id": 1630340896,
                     "odds": 2.55,
                     "name": {
                        "value": "Under 215.5",
                        "sign": "jH9v7g==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 31,
                     "denominator": 20,
                     "americanOdds": 155
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "215.5",
               "spread": 1.02,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "wppSsw=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561393223,
               "name": {
                  "value": "Totals",
                  "sign": "gjcqig=="
               },
               "results": [
                  {
                     "id": 1630340901,
                     "odds": 1.57,
                     "name": {
                        "value": "Over 216.5",
                        "sign": "zWV1hQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 4,
                     "denominator": 7,
                     "americanOdds": -175
                  },
                  {
                     "id": 1630340902,
                     "odds": 2.45,
                     "name": {
                        "value": "Under 216.5",
                        "sign": "EpxwUA==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 29,
                     "denominator": 20,
                     "americanOdds": 145
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "216.5",
               "spread": 0.88,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "gjcqig=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561393226,
               "name": {
                  "value": "Totals",
                  "sign": "QubgeA=="
               },
               "results": [
                  {
                     "id": 1630340907,
                     "odds": 1.62,
                     "name": {
                        "value": "Over 217.5",
                        "sign": "9/9A7g==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 8,
                     "denominator": 13,
                     "americanOdds": -160
                  },
                  {
                     "id": 1630340908,
                     "odds": 2.35,
                     "name": {
                        "value": "Under 217.5",
                        "sign": "LRYoTg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 27,
                     "denominator": 20,
                     "americanOdds": 135
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "217.5",
               "spread": 0.73,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "QubgeA=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561393229,
               "name": {
                  "value": "Totals",
                  "sign": "Atg5HQ=="
               },
               "results": [
                  {
                     "id": 1630340913,
                     "odds": 1.67,
                     "name": {
                        "value": "Over 218.5",
                        "sign": "YYUtDw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 4,
                     "denominator": 6,
                     "americanOdds": -150
                  },
                  {
                     "id": 1630340914,
                     "odds": 2.25,
                     "name": {
                        "value": "Under 218.5",
                        "sign": "CHHbZQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 5,
                     "denominator": 4,
                     "americanOdds": 125
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "218.5",
               "spread": 0.58,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "Atg5HQ=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561393232,
               "name": {
                  "value": "Totals",
                  "sign": "g0MERg=="
               },
               "results": [
                  {
                     "id": 1630340919,
                     "odds": 1.72,
                     "name": {
                        "value": "Over 219.5",
                        "sign": "5zIxMQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 8,
                     "denominator": 11,
                     "americanOdds": -140
                  },
                  {
                     "id": 1630340920,
                     "odds": 2.15,
                     "name": {
                        "value": "Under 219.5",
                        "sign": "N/uDew==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 23,
                     "denominator": 20,
                     "americanOdds": 115
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "219.5",
               "spread": 0.43,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "g0MERg=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561393235,
               "name": {
                  "value": "Totals",
                  "sign": "w+58fw=="
               },
               "results": [
                  {
                     "id": 1630340925,
                     "odds": 1.78,
                     "name": {
                        "value": "Over 220.5",
                        "sign": "VcA2bA==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 4,
                     "denominator": 5,
                     "americanOdds": -130
                  },
                  {
                     "id": 1630340926,
                     "odds": 2.05,
                     "name": {
                        "value": "Under 220.5",
                        "sign": "kTK+2g==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 21,
                     "denominator": 20,
                     "americanOdds": 105
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "220.5",
               "spread": 0.27,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "w+58fw=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561393238,
               "name": {
                  "value": "Totals",
                  "sign": "Axn1NA=="
               },
               "results": [
                  {
                     "id": 1630340931,
                     "odds": 1.87,
                     "name": {
                        "value": "Over 221.5",
                        "sign": "hdZe5g==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 20,
                     "denominator": 23,
                     "americanOdds": -115
                  },
                  {
                     "id": 1630340932,
                     "odds": 1.95,
                     "name": {
                        "value": "Under 221.5",
                        "sign": "RO7YhA==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 19,
                     "denominator": 20,
                     "americanOdds": -105
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "221.5",
               "spread": 0.08,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "Axn1NA=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561393241,
               "name": {
                  "value": "Totals",
                  "sign": "QwFv6A=="
               },
               "results": [
                  {
                     "id": 1630340937,
                     "odds": 1.91,
                     "name": {
                        "value": "Over 222.5",
                        "sign": "Dw57pA==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 10,
                     "denominator": 11,
                     "americanOdds": -110
                  },
                  {
                     "id": 1630340938,
                     "odds": 1.91,
                     "name": {
                        "value": "Under 222.5",
                        "sign": "UNtF+A==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 10,
                     "denominator": 11,
                     "americanOdds": -110
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "222.5",
               "spread": 0.0,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "QwFv6A=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561393244,
               "name": {
                  "value": "Totals",
                  "sign": "g/bmow=="
               },
               "results": [
                  {
                     "id": 1630340943,
                     "odds": 2.0,
                     "name": {
                        "value": "Over 223.5",
                        "sign": "iblnmg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 1,
                     "denominator": 1,
                     "americanOdds": 100
                  },
                  {
                     "id": 1630340944,
                     "odds": 1.83,
                     "name": {
                        "value": "Under 223.5",
                        "sign": "OvppuA==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 5,
                     "denominator": 6,
                     "americanOdds": -120
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "223.5",
               "spread": 0.17,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "g/bmow=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561393247,
               "name": {
                  "value": "Totals",
                  "sign": "w1uemg=="
               },
               "results": [
                  {
                     "id": 1630340949,
                     "odds": 2.1,
                     "name": {
                        "value": "Over 224.5",
                        "sign": "p5IZdQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 11,
                     "denominator": 10,
                     "americanOdds": 110
                  },
                  {
                     "id": 1630340950,
                     "odds": 1.75,
                     "name": {
                        "value": "Under 224.5",
                        "sign": "eLF/AQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 3,
                     "denominator": 4,
                     "americanOdds": -135
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "224.5",
               "spread": 0.35,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "w1uemg=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561393250,
               "name": {
                  "value": "Totals",
                  "sign": "gVI8SQ=="
               },
               "results": [
                  {
                     "id": 1630340955,
                     "odds": 2.15,
                     "name": {
                        "value": "Over 225.5",
                        "sign": "nQgsHg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 23,
                     "denominator": 20,
                     "americanOdds": 115
                  },
                  {
                     "id": 1630340956,
                     "odds": 1.72,
                     "name": {
                        "value": "Under 225.5",
                        "sign": "RzsnHw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 8,
                     "denominator": 11,
                     "americanOdds": -140
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "225.5",
               "spread": 0.43,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "gVI8SQ=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561393253,
               "name": {
                  "value": "Totals",
                  "sign": "wWzlLA=="
               },
               "results": [
                  {
                     "id": 1630340961,
                     "odds": 2.25,
                     "name": {
                        "value": "Over 226.5",
                        "sign": "3juOeQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 5,
                     "denominator": 4,
                     "americanOdds": 125
                  },
                  {
                     "id": 1630340962,
                     "odds": 1.65,
                     "name": {
                        "value": "Under 226.5",
                        "sign": "rPMngQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 13,
                     "denominator": 20,
                     "americanOdds": -155
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "226.5",
               "spread": 0.6,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "wWzlLA=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561393256,
               "name": {
                  "value": "Totals",
                  "sign": "Ab0v3g=="
               },
               "results": [
                  {
                     "id": 1630340967,
                     "odds": 2.35,
                     "name": {
                        "value": "Over 227.5",
                        "sign": "WIySRw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 27,
                     "denominator": 20,
                     "americanOdds": 135
                  },
                  {
                     "id": 1630340968,
                     "odds": 1.6,
                     "name": {
                        "value": "Under 227.5",
                        "sign": "k3l/nw==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 3,
                     "denominator": 5,
                     "americanOdds": -165
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "227.5",
               "spread": 0.75,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "Ab0v3g=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561393259,
               "name": {
                  "value": "Totals",
                  "sign": "QRBX5w=="
               },
               "results": [
                  {
                     "id": 1630340973,
                     "odds": 2.45,
                     "name": {
                        "value": "Over 228.5",
                        "sign": "tq2tDA==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 29,
                     "denominator": 20,
                     "americanOdds": 145
                  },
                  {
                     "id": 1630340974,
                     "odds": 1.57,
                     "name": {
                        "value": "Under 228.5",
                        "sign": "aWN6KA==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 4,
                     "denominator": 7,
                     "americanOdds": -175
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "228.5",
               "spread": 0.88,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "QRBX5w=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561393262,
               "name": {
                  "value": "Totals",
                  "sign": "geferA=="
               },
               "results": [
                  {
                     "id": 1630340979,
                     "odds": 2.55,
                     "name": {
                        "value": "Over 229.5",
                        "sign": "9GzKzQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 31,
                     "denominator": 20,
                     "americanOdds": 155
                  },
                  {
                     "id": 1630340980,
                     "odds": 1.53,
                     "name": {
                        "value": "Under 229.5",
                        "sign": "A0JWaA==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 8,
                     "denominator": 15,
                     "americanOdds": -190
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "229.5",
               "spread": 1.02,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "geferA=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            },
            {
               "id": 561393265,
               "name": {
                  "value": "Totals",
                  "sign": "gLWz2Q=="
               },
               "results": [
                  {
                     "id": 1630340985,
                     "odds": 2.7,
                     "name": {
                        "value": "Over 230.5",
                        "sign": "zVbEOg==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Over",
                     "numerator": 17,
                     "denominator": 10,
                     "americanOdds": 170
                  },
                  {
                     "id": 1630340986,
                     "odds": 1.48,
                     "name": {
                        "value": "Under 230.5",
                        "sign": "pJXgoQ==",
                        "shortSign": ""
                     },
                     "visibility": "Visible",
                     "totalsPrefix": "Under",
                     "numerator": 10,
                     "denominator": 21,
                     "americanOdds": -200
                  }
               ],
               "templateId": 11577,
               "categoryId": 48,
               "resultOrder": "Default",
               "combo1": "NoEventCombo",
               "combo2": "Single",
               "visibility": "Visible",
               "balanced": 1,
               "attr": "230.5",
               "spread": 1.22,
               "category": "Gridable",
               "templateCategory": {
                  "id": 48,
                  "name": {
                     "value": "Totals",
                     "sign": "gLWz2Q=="
                  },
                  "category": "Gridable"
               },
               "isMain": false,
               "grouping": {
                  "gridGroups": [
                     "x69gzhrb4"
                  ],
                  "detailed": [
                     {
                        "group": 0,
                        "index": 8,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     },
                     {
                        "group": 1,
                        "index": 0,
                        "subIndex": 1,
                        "marketTabId": 8,
                        "name": "Totals",
                        "displayType": "OverUnder"
                     }
                  ],
                  "parameters": {
                     "marketType": "OverUnder"
                  }
               }
            }
         ],
         "participants": [
            {
               "participantId": 2554185,
               "name": {
                  "value": "New York Knicks",
                  "sign": "n0pjkA==",
                  "short": "Knicks",
                  "shortSign": "jqbHoA=="
               },
               "image": {
                  "logo": "2554185_logo",
                  "jersey": "2554185_1",
                  "rotateJersey": true
               },
               "options": []
            },
            {
               "participantId": 2554151,
               "name": {
                  "value": "Atlanta Hawks",
                  "sign": "b9j9NQ==",
                  "short": "Hawks",
                  "shortSign": "jhQGoA=="
               },
               "image": {
                  "logo": "2554151_logo",
                  "jersey": "2554151_1",
                  "rotateJersey": true
               },
               "options": []
            }
         ],
         "id": "10974870",
         "name": {
            "value": "New York Knicks at Atlanta Hawks",
            "sign": "mjoC7A=="
         },
         "sourceId": 10974870,
         "source": "V1",
         "fixtureType": "Standard",
         "context": "v1|en-us|10974870",
         "addons": {
            "participantDividend": {}
         },
         "stage": "PreMatch",
         "groupId": 2346894,
         "liveType": "NotSet",
         "liveAlert": true,
         "startDate": "2021-01-05T00:35:00Z",
         "cutOffDate": "2021-01-05T00:35:00Z",
         "sport": {
            "type": "Sport",
            "id": 7,
            "name": {
               "value": "Basketball",
               "sign": "WotGFA=="
            }
         },
         "competition": {
            "parentLeagueId": 6004,
            "statistics": true,
            "sportId": 7,
            "type": "Competition",
            "id": 6004,
            "parentId": 9,
            "name": {
               "value": "NBA",
               "sign": "pL+WdA=="
            }
         },
         "region": {
            "code": "N-A",
            "sportId": 7,
            "type": "Region",
            "id": 9,
            "parentId": 7,
            "name": {
               "value": "USA",
               "sign": "ULdUtw=="
            }
         },
         "viewType": "American",
         "isOpenForBetting": true,
         "isVirtual": false,
         "taggedLocations": [],
         "totalMarketsCount": 68
      }
   ],
   "totalCount": 7,
   "totalSports": 1,
   "totalRegions": 1,
   "totalCompetitions": 1
}
