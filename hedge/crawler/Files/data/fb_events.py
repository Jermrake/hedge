{
   "event": [
      {
         "attributes": {
            "attrib": [
               {
                  "value": "sr:match:24751000",
                  "key": "betradarId"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               },
               {
                  "value": "NBA TV",
                  "key": "TvDataChannel"
               },
               {
                  "value": "1609715100000",
                  "key": "TvDataStartTime"
               },
               {
                  "value": "1609722300000",
                  "key": "TvDataEndTime"
               }
            ]
         },
         "markets": [
            {
               "selection": [
                  {
                     "odds": {
                        "dec": "3.7",
                        "frac": "27/10",
                        "rootIdx": 134
                     },
                     "pos": {
                        "row": 1,
                        "col": 2
                     },
                     "names": {
                        "longName": "Washington Wizards",
                        "shortName": "Washington Wizards"
                     },
                     "wasPrice": [],
                     "id": 202302661,
                     "name": "Washington Wizards",
                     "type": "BH",
                     "suspended": false
                  },
                  {
                     "odds": {
                        "dec": "1.28",
                        "frac": "2/7",
                        "rootIdx": 34
                     },
                     "pos": {
                        "row": 1,
                        "col": 1
                     },
                     "names": {
                        "longName": "Brooklyn Nets",
                        "shortName": "Brooklyn Nets"
                     },
                     "wasPrice": [],
                     "id": 202302642,
                     "name": "Brooklyn Nets",
                     "type": "AH",
                     "suspended": false
                  }
               ],
               "attributes": {
                  "attrib": [
                     {
                        "value": "false",
                        "key": "isBPG"
                     },
                     {
                        "value": "true",
                        "key": "cashoutAvailable"
                     }
                  ]
               },
               "rule4S": [],
               "names": {
                  "longName": "Money Line (Incl. OT)",
                  "shortName": "Money Line (Incl. OT)"
               },
               "id": 69661141,
               "name": "Money Line (Incl. OT)",
               "type": "BASKETBALL:FTOT:ML",
               "subtype": "M",
               "displayOrder": -500,
               "columnCount": 2,
               "suspended": false,
               "displayed": true,
               "winningLegsBonus": false,
               "periodMarket": false,
               "period": "M"
            },
            {
               "selection": [
                  {
                     "odds": {
                        "dec": "1.91",
                        "frac": "10/11",
                        "rootIdx": 74
                     },
                     "pos": {
                        "row": 1,
                        "col": 2
                     },
                     "names": {
                        "longName": "Washington Wizards {awayLine}",
                        "shortName": "Washington Wizards {awayLine}"
                     },
                     "wasPrice": [],
                     "id": 202302689,
                     "name": "Washington Wizards {awayLine}",
                     "type": "BH",
                     "suspended": false
                  },
                  {
                     "odds": {
                        "dec": "1.91",
                        "frac": "10/11",
                        "rootIdx": 74
                     },
                     "pos": {
                        "row": 1,
                        "col": 1
                     },
                     "names": {
                        "longName": "Brooklyn Nets {homeLine}",
                        "shortName": "Brooklyn Nets {homeLine}"
                     },
                     "wasPrice": [],
                     "id": 202302681,
                     "name": "Brooklyn Nets {homeLine}",
                     "type": "AH",
                     "suspended": false
                  }
               ],
               "attributes": {
                  "attrib": [
                     {
                        "value": "false",
                        "key": "isBPG"
                     }
                  ]
               },
               "rule4S": [],
               "names": {
                  "longName": "Spread",
                  "shortName": "Spread"
               },
               "id": 69661150,
               "name": "Spread",
               "type": "BASKETBALL:FTOT:AHCP_MAIN",
               "subtype": "-7.5",
               "line": -7.5,
               "lineType": "HANDICAP",
               "displayOrder": -495,
               "columnCount": 2,
               "suspended": false,
               "displayed": true,
               "winningLegsBonus": false,
               "periodMarket": false,
               "period": "-7.5"
            },
            {
               "selection": [
                  {
                     "odds": {
                        "dec": "1.91",
                        "frac": "10/11",
                        "rootIdx": 74
                     },
                     "pos": {
                        "row": 1,
                        "col": 2
                     },
                     "names": {
                        "longName": "Under {Line}",
                        "shortName": "Under {Line}"
                     },
                     "wasPrice": [],
                     "id": 202304395,
                     "name": "Under {Line}",
                     "type": "Under",
                     "suspended": false
                  },
                  {
                     "odds": {
                        "dec": "1.91",
                        "frac": "10/11",
                        "rootIdx": 74
                     },
                     "pos": {
                        "row": 1,
                        "col": 1
                     },
                     "names": {
                        "longName": "Over {Line}",
                        "shortName": "Over {Line}"
                     },
                     "wasPrice": [],
                     "id": 202304393,
                     "name": "Over {Line}",
                     "type": "Over",
                     "suspended": false
                  }
               ],
               "attributes": {
                  "attrib": [
                     {
                        "value": "false",
                        "key": "isBPG"
                     }
                  ]
               },
               "rule4S": [],
               "names": {
                  "longName": "Total Points",
                  "shortName": "Total Points"
               },
               "id": 69661961,
               "name": "Total Points",
               "type": "BASKETBALL:FTOT:OU_MAIN",
               "subtype": "241.0",
               "line": 241.0,
               "lineType": "TOTAL",
               "displayOrder": -490,
               "columnCount": 2,
               "suspended": false,
               "displayed": true,
               "winningLegsBonus": false,
               "periodMarket": false,
               "period": "241"
            }
         ],
         "pastForm": [],
         "participants": {
            "participant": [
               {
                  "names": {
                     "longName": "Brooklyn Nets",
                     "shortName": "BKN"
                  },
                  "id": 4062501,
                  "name": "Brooklyn Nets",
                  "type": "HOME"
               },
               {
                  "names": {
                     "longName": "Washington Wizards",
                     "shortName": "WAS"
                  },
                  "id": 4062508,
                  "name": "Washington Wizards",
                  "type": "AWAY"
               }
            ]
         },
         "names": {
            "longName": "Brooklyn Nets vs. Washington Wizards",
            "shortName": "BKN vs. WAS",
            "veryshortName": "Brooklyn Nets vs. Washington Wizards"
         },
         "compNames": {
            "longName": "NBA",
            "shortName": "NBA"
         },
         "categoryNames": {
            "longName": "USA",
            "shortName": "USA"
         },
         "id": 8712228,
         "compId": 8370429,
         "compName": "NBA",
         "compWeighting": 100000.0,
         "categoryId": 8370427,
         "categoryName": "USA",
         "path": "226652:8370427:8370429:8712228",
         "name": "Brooklyn Nets vs. Washington Wizards",
         "sport": "BASKETBALL",
         "state": "ACTIVE",
         "locale": "en-us",
         "displayed": true,
         "offeredInplay": true,
         "isInplay": false,
         "eventTime": 1609714800000,
         "numMarkets": 219,
         "numSpecSelections": 4,
         "numRABSelections": 38,
         "lastUpdatedTime": 1609712330202,
         "outright": false,
         "neutralVenue": false,
         "winningLegsBonus": false,
         "usDisplay": true
      },
      {
         "attributes": {
            "attrib": [
               {
                  "value": "sr:match:24751494",
                  "key": "betradarId"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "markets": [
            {
               "selection": [
                  {
                     "odds": {
                        "dec": "5",
                        "frac": "4/1",
                        "rootIdx": 160
                     },
                     "pos": {
                        "row": 1,
                        "col": 1
                     },
                     "names": {
                        "longName": "Memphis Grizzlies",
                        "shortName": "Memphis Grizzlies"
                     },
                     "wasPrice": [],
                     "id": 202302327,
                     "name": "Memphis Grizzlies",
                     "type": "AH",
                     "suspended": false
                  },
                  {
                     "odds": {
                        "dec": "1.18",
                        "frac": "2/11",
                        "rootIdx": 26
                     },
                     "pos": {
                        "row": 1,
                        "col": 2
                     },
                     "names": {
                        "longName": "Los Angeles Lakers",
                        "shortName": "Los Angeles Lakers"
                     },
                     "wasPrice": [],
                     "id": 202302325,
                     "name": "Los Angeles Lakers",
                     "type": "BH",
                     "suspended": false
                  }
               ],
               "attributes": {
                  "attrib": [
                     {
                        "value": "false",
                        "key": "isBPG"
                     },
                     {
                        "value": "true",
                        "key": "cashoutAvailable"
                     }
                  ]
               },
               "rule4S": [],
               "names": {
                  "longName": "Money Line (Incl. OT)",
                  "shortName": "Money Line (Incl. OT)"
               },
               "id": 69660969,
               "name": "Money Line (Incl. OT)",
               "type": "BASKETBALL:FTOT:ML",
               "subtype": "M",
               "displayOrder": -500,
               "columnCount": 2,
               "suspended": false,
               "displayed": true,
               "winningLegsBonus": false,
               "periodMarket": false,
               "period": "M"
            },
            {
               "selection": [
                  {
                     "odds": {
                        "dec": "1.91",
                        "frac": "10/11",
                        "rootIdx": 74
                     },
                     "pos": {
                        "row": 1,
                        "col": 2
                     },
                     "names": {
                        "longName": "Los Angeles Lakers {awayLine}",
                        "shortName": "Los Angeles Lakers {awayLine}"
                     },
                     "wasPrice": [],
                     "id": 202302293,
                     "name": "Los Angeles Lakers {awayLine}",
                     "type": "BH",
                     "suspended": false
                  },
                  {
                     "odds": {
                        "dec": "1.91",
                        "frac": "10/11",
                        "rootIdx": 74
                     },
                     "pos": {
                        "row": 1,
                        "col": 1
                     },
                     "names": {
                        "longName": "Memphis Grizzlies {homeLine}",
                        "shortName": "Memphis Grizzlies {homeLine}"
                     },
                     "wasPrice": [],
                     "id": 202302323,
                     "name": "Memphis Grizzlies {homeLine}",
                     "type": "AH",
                     "suspended": false
                  }
               ],
               "attributes": {
                  "attrib": [
                     {
                        "value": "false",
                        "key": "isBPG"
                     }
                  ]
               },
               "rule4S": [],
               "names": {
                  "longName": "Spread",
                  "shortName": "Spread"
               },
               "id": 69660957,
               "name": "Spread",
               "type": "BASKETBALL:FTOT:AHCP_MAIN",
               "subtype": "9.0",
               "line": 9.0,
               "lineType": "HANDICAP",
               "displayOrder": -495,
               "columnCount": 2,
               "suspended": false,
               "displayed": true,
               "winningLegsBonus": false,
               "periodMarket": false,
               "period": "9"
            },
            {
               "selection": [
                  {
                     "odds": {
                        "dec": "1.91",
                        "frac": "10/11",
                        "rootIdx": 74
                     },
                     "pos": {
                        "row": 1,
                        "col": 2
                     },
                     "names": {
                        "longName": "Under {Line}",
                        "shortName": "Under {Line}"
                     },
                     "wasPrice": [],
                     "id": 202305723,
                     "name": "Under {Line}",
                     "type": "Under",
                     "suspended": false
                  },
                  {
                     "odds": {
                        "dec": "1.91",
                        "frac": "10/11",
                        "rootIdx": 74
                     },
                     "pos": {
                        "row": 1,
                        "col": 1
                     },
                     "names": {
                        "longName": "Over {Line}",
                        "shortName": "Over {Line}"
                     },
                     "wasPrice": [],
                     "id": 202305721,
                     "name": "Over {Line}",
                     "type": "Over",
                     "suspended": false
                  }
               ],
               "attributes": {
                  "attrib": [
                     {
                        "value": "false",
                        "key": "isBPG"
                     }
                  ]
               },
               "rule4S": [],
               "names": {
                  "longName": "Total Points",
                  "shortName": "Total Points"
               },
               "id": 69662495,
               "name": "Total Points",
               "type": "BASKETBALL:FTOT:OU_MAIN",
               "subtype": "219.5",
               "line": 219.5,
               "lineType": "TOTAL",
               "displayOrder": -490,
               "columnCount": 2,
               "suspended": false,
               "displayed": true,
               "winningLegsBonus": false,
               "periodMarket": false,
               "period": "219.5"
            }
         ],
         "pastForm": [],
         "participants": {
            "participant": [
               {
                  "names": {
                     "longName": "Memphis Grizzlies",
                     "shortName": "MEM"
                  },
                  "id": 4062511,
                  "name": "Memphis Grizzlies",
                  "type": "HOME"
               },
               {
                  "names": {
                     "longName": "Los Angeles Lakers",
                     "shortName": "LAL"
                  },
                  "id": 4062498,
                  "name": "Los Angeles Lakers",
                  "type": "AWAY"
               }
            ]
         },
         "names": {
            "longName": "Memphis Grizzlies vs. Los Angeles Lakers",
            "shortName": "MEM vs. LAL",
            "veryshortName": "Memphis Grizzlies vs. Los Angeles Lakers"
         },
         "compNames": {
            "longName": "NBA",
            "shortName": "NBA"
         },
         "categoryNames": {
            "longName": "USA",
            "shortName": "USA"
         },
         "id": 8712226,
         "compId": 8370429,
         "compName": "NBA",
         "compWeighting": 100000.0,
         "categoryId": 8370427,
         "categoryName": "USA",
         "path": "226652:8370427:8370429:8712226",
         "name": "Memphis Grizzlies vs. Los Angeles Lakers",
         "sport": "BASKETBALL",
         "state": "ACTIVE",
         "locale": "en-us",
         "displayed": true,
         "offeredInplay": true,
         "isInplay": false,
         "eventTime": 1609714800000,
         "numMarkets": 158,
         "numSpecSelections": 4,
         "numRABSelections": 41,
         "lastUpdatedTime": 1609711802965,
         "outright": false,
         "neutralVenue": false,
         "winningLegsBonus": false,
         "usDisplay": true
      },
      {
         "attributes": {
            "attrib": [
               {
                  "value": "sr:match:24750746",
                  "key": "betradarId"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               },
               {
                  "value": "ESPN 2",
                  "key": "TvDataChannel"
               },
               {
                  "value": "1609718400000",
                  "key": "TvDataStartTime"
               },
               {
                  "value": "1609725600000",
                  "key": "TvDataEndTime"
               }
            ]
         },
         "markets": [
            {
               "selection": [
                  {
                     "odds": {
                        "dec": "1.22",
                        "frac": "2/9",
                        "rootIdx": 30
                     },
                     "pos": {
                        "row": 1,
                        "col": 2
                     },
                     "names": {
                        "longName": "Denver Nuggets",
                        "shortName": "Denver Nuggets"
                     },
                     "wasPrice": [],
                     "id": 202331938,
                     "name": "Denver Nuggets",
                     "type": "BH",
                     "suspended": false
                  },
                  {
                     "odds": {
                        "dec": "4.33",
                        "frac": "10/3",
                        "rootIdx": 147
                     },
                     "pos": {
                        "row": 1,
                        "col": 1
                     },
                     "names": {
                        "longName": "Minnesota Timberwolves",
                        "shortName": "Minnesota Timberwolves"
                     },
                     "wasPrice": [],
                     "id": 202331964,
                     "name": "Minnesota Timberwolves",
                     "type": "AH",
                     "suspended": false
                  }
               ],
               "attributes": {
                  "attrib": [
                     {
                        "value": "false",
                        "key": "isBPG"
                     },
                     {
                        "value": "true",
                        "key": "cashoutAvailable"
                     }
                  ]
               },
               "rule4S": [],
               "names": {
                  "longName": "Money Line (Incl. OT)",
                  "shortName": "Money Line (Incl. OT)"
               },
               "id": 69667248,
               "name": "Money Line (Incl. OT)",
               "type": "BASKETBALL:FTOT:ML",
               "subtype": "M",
               "displayOrder": -500,
               "columnCount": 2,
               "suspended": false,
               "displayed": true,
               "winningLegsBonus": false,
               "periodMarket": false,
               "period": "M"
            },
            {
               "selection": [
                  {
                     "odds": {
                        "dec": "1.91",
                        "frac": "10/11",
                        "rootIdx": 74
                     },
                     "pos": {
                        "row": 1,
                        "col": 1
                     },
                     "names": {
                        "longName": "Minnesota Timberwolves {homeLine}",
                        "shortName": "Minnesota Timberwolves {homeLine}"
                     },
                     "wasPrice": [],
                     "id": 202331977,
                     "name": "Minnesota Timberwolves {homeLine}",
                     "type": "AH",
                     "suspended": false
                  },
                  {
                     "odds": {
                        "dec": "1.95",
                        "frac": "19/20",
                        "rootIdx": 78
                     },
                     "pos": {
                        "row": 1,
                        "col": 2
                     },
                     "names": {
                        "longName": "Denver Nuggets {awayLine}",
                        "shortName": "Denver Nuggets {awayLine}"
                     },
                     "wasPrice": [],
                     "id": 202331961,
                     "name": "Denver Nuggets {awayLine}",
                     "type": "BH",
                     "suspended": false
                  }
               ],
               "attributes": {
                  "attrib": [
                     {
                        "value": "false",
                        "key": "isBPG"
                     }
                  ]
               },
               "rule4S": [],
               "names": {
                  "longName": "Spread",
                  "shortName": "Spread"
               },
               "id": 69667251,
               "name": "Spread",
               "type": "BASKETBALL:FTOT:AHCP_MAIN",
               "subtype": "8.5",
               "line": 8.5,
               "lineType": "HANDICAP",
               "displayOrder": -495,
               "columnCount": 2,
               "suspended": false,
               "displayed": true,
               "winningLegsBonus": false,
               "periodMarket": false,
               "period": "8.5"
            },
            {
               "selection": [
                  {
                     "odds": {
                        "dec": "1.91",
                        "frac": "10/11",
                        "rootIdx": 74
                     },
                     "pos": {
                        "row": 1,
                        "col": 1
                     },
                     "names": {
                        "longName": "Over {Line}",
                        "shortName": "Over {Line}"
                     },
                     "wasPrice": [],
                     "id": 202337389,
                     "name": "Over {Line}",
                     "type": "Over",
                     "suspended": false
                  },
                  {
                     "odds": {
                        "dec": "1.91",
                        "frac": "10/11",
                        "rootIdx": 74
                     },
                     "pos": {
                        "row": 1,
                        "col": 2
                     },
                     "names": {
                        "longName": "Under {Line}",
                        "shortName": "Under {Line}"
                     },
                     "wasPrice": [],
                     "id": 202337391,
                     "name": "Under {Line}",
                     "type": "Under",
                     "suspended": false
                  }
               ],
               "attributes": {
                  "attrib": [
                     {
                        "value": "false",
                        "key": "isBPG"
                     }
                  ]
               },
               "rule4S": [],
               "names": {
                  "longName": "Total Points",
                  "shortName": "Total Points"
               },
               "id": 69669665,
               "name": "Total Points",
               "type": "BASKETBALL:FTOT:OU_MAIN",
               "subtype": "226.5",
               "line": 226.5,
               "lineType": "TOTAL",
               "displayOrder": -490,
               "columnCount": 2,
               "suspended": false,
               "displayed": true,
               "winningLegsBonus": false,
               "periodMarket": false,
               "period": "226.5"
            }
         ],
         "pastForm": [],
         "participants": {
            "participant": [
               {
                  "names": {
                     "longName": "Minnesota Timberwolves",
                     "shortName": "MIN"
                  },
                  "id": 4062469,
                  "name": "Minnesota Timberwolves",
                  "type": "HOME"
               },
               {
                  "names": {
                     "longName": "Denver Nuggets",
                     "shortName": "DEN"
                  },
                  "id": 4062505,
                  "name": "Denver Nuggets",
                  "type": "AWAY"
               }
            ]
         },
         "names": {
            "longName": "Minnesota Timberwolves vs. Denver Nuggets",
            "shortName": "MIN vs. DEN",
            "veryshortName": "Minnesota Timberwolves vs. Denver Nuggets"
         },
         "compNames": {
            "longName": "NBA",
            "shortName": "NBA"
         },
         "categoryNames": {
            "longName": "USA",
            "shortName": "USA"
         },
         "id": 8712260,
         "compId": 8370429,
         "compName": "NBA",
         "compWeighting": 100000.0,
         "categoryId": 8370427,
         "categoryName": "USA",
         "path": "226652:8370427:8370429:8712260",
         "name": "Minnesota Timberwolves vs. Denver Nuggets",
         "sport": "BASKETBALL",
         "state": "ACTIVE",
         "locale": "en-us",
         "displayed": true,
         "offeredInplay": true,
         "isInplay": false,
         "eventTime": 1609718400000,
         "numMarkets": 211,
         "numSpecSelections": 4,
         "numRABSelections": 40,
         "lastUpdatedTime": 1609705799204,
         "outright": false,
         "neutralVenue": false,
         "winningLegsBonus": false,
         "usDisplay": true
      },
      {
         "attributes": {
            "attrib": [
               {
                  "value": "sr:match:24751640",
                  "key": "betradarId"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               },
               {
                  "value": "ESPN 2",
                  "key": "TvDataChannel"
               },
               {
                  "value": "1609718400000",
                  "key": "TvDataStartTime"
               },
               {
                  "value": "1609725600000",
                  "key": "TvDataEndTime"
               }
            ]
         },
         "markets": [
            {
               "selection": [
                  {
                     "odds": {
                        "dec": "1.47",
                        "frac": "40/85",
                        "rootIdx": 45
                     },
                     "pos": {
                        "row": 1,
                        "col": 2
                     },
                     "names": {
                        "longName": "Utah Jazz",
                        "shortName": "Utah Jazz"
                     },
                     "wasPrice": [],
                     "id": 202330767,
                     "name": "Utah Jazz",
                     "type": "BH",
                     "suspended": false
                  },
                  {
                     "odds": {
                        "dec": "2.7",
                        "frac": "17/10",
                        "rootIdx": 113
                     },
                     "pos": {
                        "row": 1,
                        "col": 1
                     },
                     "names": {
                        "longName": "San Antonio Spurs",
                        "shortName": "San Antonio Spurs"
                     },
                     "wasPrice": [],
                     "id": 202330764,
                     "name": "San Antonio Spurs",
                     "type": "AH",
                     "suspended": false
                  }
               ],
               "attributes": {
                  "attrib": [
                     {
                        "value": "false",
                        "key": "isBPG"
                     },
                     {
                        "value": "true",
                        "key": "cashoutAvailable"
                     }
                  ]
               },
               "rule4S": [],
               "names": {
                  "longName": "Money Line (Incl. OT)",
                  "shortName": "Money Line (Incl. OT)"
               },
               "id": 69666705,
               "name": "Money Line (Incl. OT)",
               "type": "BASKETBALL:FTOT:ML",
               "subtype": "M",
               "displayOrder": -500,
               "columnCount": 2,
               "suspended": false,
               "displayed": true,
               "winningLegsBonus": false,
               "periodMarket": false,
               "period": "M"
            },
            {
               "selection": [
                  {
                     "odds": {
                        "dec": "1.91",
                        "frac": "10/11",
                        "rootIdx": 74
                     },
                     "pos": {
                        "row": 1,
                        "col": 2
                     },
                     "names": {
                        "longName": "Utah Jazz {awayLine}",
                        "shortName": "Utah Jazz {awayLine}"
                     },
                     "wasPrice": [],
                     "id": 202330790,
                     "name": "Utah Jazz {awayLine}",
                     "type": "BH",
                     "suspended": false
                  },
                  {
                     "odds": {
                        "dec": "1.91",
                        "frac": "10/11",
                        "rootIdx": 74
                     },
                     "pos": {
                        "row": 1,
                        "col": 1
                     },
                     "names": {
                        "longName": "San Antonio Spurs {homeLine}",
                        "shortName": "San Antonio Spurs {homeLine}"
                     },
                     "wasPrice": [],
                     "id": 202330788,
                     "name": "San Antonio Spurs {homeLine}",
                     "type": "AH",
                     "suspended": false
                  }
               ],
               "attributes": {
                  "attrib": [
                     {
                        "value": "false",
                        "key": "isBPG"
                     }
                  ]
               },
               "rule4S": [],
               "names": {
                  "longName": "Spread",
                  "shortName": "Spread"
               },
               "id": 69666713,
               "name": "Spread",
               "type": "BASKETBALL:FTOT:AHCP_MAIN",
               "subtype": "5.0",
               "line": 5.0,
               "lineType": "HANDICAP",
               "displayOrder": -495,
               "columnCount": 2,
               "suspended": false,
               "displayed": true,
               "winningLegsBonus": false,
               "periodMarket": false,
               "period": "5"
            },
            {
               "selection": [
                  {
                     "odds": {
                        "dec": "1.95",
                        "frac": "19/20",
                        "rootIdx": 78
                     },
                     "pos": {
                        "row": 1,
                        "col": 2
                     },
                     "names": {
                        "longName": "Under {Line}",
                        "shortName": "Under {Line}"
                     },
                     "wasPrice": [],
                     "id": 202471723,
                     "name": "Under {Line}",
                     "type": "Under",
                     "suspended": false
                  },
                  {
                     "odds": {
                        "dec": "1.91",
                        "frac": "10/11",
                        "rootIdx": 74
                     },
                     "pos": {
                        "row": 1,
                        "col": 1
                     },
                     "names": {
                        "longName": "Over {Line}",
                        "shortName": "Over {Line}"
                     },
                     "wasPrice": [],
                     "id": 202471721,
                     "name": "Over {Line}",
                     "type": "Over",
                     "suspended": false
                  }
               ],
               "attributes": {
                  "attrib": [
                     {
                        "value": "false",
                        "key": "isBPG"
                     }
                  ]
               },
               "rule4S": [],
               "names": {
                  "longName": "Total Points",
                  "shortName": "Total Points"
               },
               "id": 69715453,
               "name": "Total Points",
               "type": "BASKETBALL:FTOT:OU_MAIN",
               "subtype": "221.5",
               "line": 221.5,
               "lineType": "TOTAL",
               "displayOrder": -490,
               "columnCount": 2,
               "suspended": false,
               "displayed": true,
               "winningLegsBonus": false,
               "periodMarket": false,
               "period": "221.5"
            }
         ],
         "pastForm": [],
         "participants": {
            "participant": [
               {
                  "names": {
                     "longName": "San Antonio Spurs",
                     "shortName": "SAS"
                  },
                  "id": 4062318,
                  "name": "San Antonio Spurs",
                  "type": "HOME"
               },
               {
                  "names": {
                     "longName": "Utah Jazz",
                     "shortName": "UTA"
                  },
                  "id": 4062506,
                  "name": "Utah Jazz",
                  "type": "AWAY"
               }
            ]
         },
         "names": {
            "longName": "San Antonio Spurs vs. Utah Jazz",
            "shortName": "SAS vs. UTA",
            "veryshortName": "San Antonio Spurs vs. Utah Jazz"
         },
         "compNames": {
            "longName": "NBA",
            "shortName": "NBA"
         },
         "categoryNames": {
            "longName": "USA",
            "shortName": "USA"
         },
         "id": 8712254,
         "compId": 8370429,
         "compName": "NBA",
         "compWeighting": 100000.0,
         "categoryId": 8370427,
         "categoryName": "USA",
         "path": "226652:8370427:8370429:8712254",
         "name": "San Antonio Spurs vs. Utah Jazz",
         "sport": "BASKETBALL",
         "state": "ACTIVE",
         "locale": "en-us",
         "displayed": true,
         "offeredInplay": true,
         "isInplay": false,
         "eventTime": 1609718400000,
         "numMarkets": 156,
         "numSpecSelections": 4,
         "numRABSelections": 37,
         "lastUpdatedTime": 1609712374871,
         "outright": false,
         "neutralVenue": false,
         "winningLegsBonus": false,
         "usDisplay": true
      },
      {
         "attributes": {
            "attrib": [
               {
                  "value": "sr:match:24750896",
                  "key": "betradarId"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "markets": [
            {
               "selection": [
                  {
                     "odds": {
                        "dec": "2.45",
                        "frac": "29/20",
                        "rootIdx": 107
                     },
                     "pos": {
                        "row": 1,
                        "col": 1
                     },
                     "names": {
                        "longName": "Chicago Bulls",
                        "shortName": "Chicago Bulls"
                     },
                     "wasPrice": [],
                     "id": 202330960,
                     "name": "Chicago Bulls",
                     "type": "AH",
                     "suspended": false
                  },
                  {
                     "odds": {
                        "dec": "1.55",
                        "frac": "11/20",
                        "rootIdx": 51
                     },
                     "pos": {
                        "row": 1,
                        "col": 2
                     },
                     "names": {
                        "longName": "Dallas Mavericks",
                        "shortName": "Dallas Mavericks"
                     },
                     "wasPrice": [],
                     "id": 202330970,
                     "name": "Dallas Mavericks",
                     "type": "BH",
                     "suspended": false
                  }
               ],
               "attributes": {
                  "attrib": [
                     {
                        "value": "false",
                        "key": "isBPG"
                     },
                     {
                        "value": "true",
                        "key": "cashoutAvailable"
                     }
                  ]
               },
               "rule4S": [],
               "names": {
                  "longName": "Money Line (Incl. OT)",
                  "shortName": "Money Line (Incl. OT)"
               },
               "id": 69666802,
               "name": "Money Line (Incl. OT)",
               "type": "BASKETBALL:FTOT:ML",
               "subtype": "M",
               "displayOrder": -500,
               "columnCount": 2,
               "suspended": false,
               "displayed": true,
               "winningLegsBonus": false,
               "periodMarket": false,
               "period": "M"
            },
            {
               "selection": [
                  {
                     "odds": {
                        "dec": "2",
                        "frac": "1/1",
                        "rootIdx": 83
                     },
                     "pos": {
                        "row": 1,
                        "col": 2
                     },
                     "names": {
                        "longName": "Dallas Mavericks {awayLine}",
                        "shortName": "Dallas Mavericks {awayLine}"
                     },
                     "wasPrice": [],
                     "id": 202330977,
                     "name": "Dallas Mavericks {awayLine}",
                     "type": "BH",
                     "suspended": false
                  },
                  {
                     "odds": {
                        "dec": "1.85",
                        "frac": "17/20",
                        "rootIdx": 70
                     },
                     "pos": {
                        "row": 1,
                        "col": 1
                     },
                     "names": {
                        "longName": "Chicago Bulls {homeLine}",
                        "shortName": "Chicago Bulls {homeLine}"
                     },
                     "wasPrice": [],
                     "id": 202330971,
                     "name": "Chicago Bulls {homeLine}",
                     "type": "AH",
                     "suspended": false
                  }
               ],
               "attributes": {
                  "attrib": [
                     {
                        "value": "false",
                        "key": "isBPG"
                     }
                  ]
               },
               "rule4S": [],
               "names": {
                  "longName": "Spread",
                  "shortName": "Spread"
               },
               "id": 69666807,
               "name": "Spread",
               "type": "BASKETBALL:FTOT:AHCP_MAIN",
               "subtype": "4.5",
               "line": 4.5,
               "lineType": "HANDICAP",
               "displayOrder": -495,
               "columnCount": 2,
               "suspended": false,
               "displayed": true,
               "winningLegsBonus": false,
               "periodMarket": false,
               "period": "4.5"
            },
            {
               "selection": [
                  {
                     "odds": {
                        "dec": "1.91",
                        "frac": "10/11",
                        "rootIdx": 74
                     },
                     "pos": {
                        "row": 1,
                        "col": 2
                     },
                     "names": {
                        "longName": "Under {Line}",
                        "shortName": "Under {Line}"
                     },
                     "wasPrice": [],
                     "id": 202470869,
                     "name": "Under {Line}",
                     "type": "Under",
                     "suspended": false
                  },
                  {
                     "odds": {
                        "dec": "1.91",
                        "frac": "10/11",
                        "rootIdx": 74
                     },
                     "pos": {
                        "row": 1,
                        "col": 1
                     },
                     "names": {
                        "longName": "Over {Line}",
                        "shortName": "Over {Line}"
                     },
                     "wasPrice": [],
                     "id": 202470867,
                     "name": "Over {Line}",
                     "type": "Over",
                     "suspended": false
                  }
               ],
               "attributes": {
                  "attrib": [
                     {
                        "value": "false",
                        "key": "isBPG"
                     }
                  ]
               },
               "rule4S": [],
               "names": {
                  "longName": "Total Points",
                  "shortName": "Total Points"
               },
               "id": 69715097,
               "name": "Total Points",
               "type": "BASKETBALL:FTOT:OU_MAIN",
               "subtype": "223.5",
               "line": 223.5,
               "lineType": "TOTAL",
               "displayOrder": -490,
               "columnCount": 2,
               "suspended": false,
               "displayed": true,
               "winningLegsBonus": false,
               "periodMarket": false,
               "period": "223.5"
            }
         ],
         "pastForm": [],
         "participants": {
            "participant": [
               {
                  "names": {
                     "longName": "Chicago Bulls",
                     "shortName": "CHI"
                  },
                  "id": 4062475,
                  "name": "Chicago Bulls",
                  "type": "HOME"
               },
               {
                  "names": {
                     "longName": "Dallas Mavericks",
                     "shortName": "DAL"
                  },
                  "id": 4062513,
                  "name": "Dallas Mavericks",
                  "type": "AWAY"
               }
            ]
         },
         "names": {
            "longName": "Chicago Bulls vs. Dallas Mavericks",
            "shortName": "CHI vs. DAL",
            "veryshortName": "Chicago Bulls vs. Dallas Mavericks"
         },
         "compNames": {
            "longName": "NBA",
            "shortName": "NBA"
         },
         "categoryNames": {
            "longName": "USA",
            "shortName": "USA"
         },
         "id": 8712256,
         "compId": 8370429,
         "compName": "NBA",
         "compWeighting": 100000.0,
         "categoryId": 8370427,
         "categoryName": "USA",
         "path": "226652:8370427:8370429:8712256",
         "name": "Chicago Bulls vs. Dallas Mavericks",
         "sport": "BASKETBALL",
         "state": "ACTIVE",
         "locale": "en-us",
         "displayed": true,
         "offeredInplay": true,
         "isInplay": false,
         "eventTime": 1609722000000,
         "numMarkets": 193,
         "numSpecSelections": 3,
         "numRABSelections": 30,
         "lastUpdatedTime": 1609712166910,
         "outright": false,
         "neutralVenue": false,
         "winningLegsBonus": false,
         "usDisplay": true
      },
      {
         "attributes": {
            "attrib": [
               {
                  "value": "sr:match:24751684",
                  "key": "betradarId"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "markets": [
            {
               "selection": [
                  {
                     "odds": {
                        "dec": "1.62",
                        "frac": "8/13",
                        "rootIdx": 54
                     },
                     "pos": {
                        "row": 1,
                        "col": 2
                     },
                     "names": {
                        "longName": "Los Angeles Clippers",
                        "shortName": "Los Angeles Clippers"
                     },
                     "wasPrice": [],
                     "id": 202302949,
                     "name": "Los Angeles Clippers",
                     "type": "BH",
                     "suspended": false
                  },
                  {
                     "odds": {
                        "dec": "2.3",
                        "frac": "13/10",
                        "rootIdx": 103
                     },
                     "pos": {
                        "row": 1,
                        "col": 1
                     },
                     "names": {
                        "longName": "Phoenix Suns",
                        "shortName": "Phoenix Suns"
                     },
                     "wasPrice": [],
                     "id": 202302958,
                     "name": "Phoenix Suns",
                     "type": "AH",
                     "suspended": false
                  }
               ],
               "attributes": {
                  "attrib": [
                     {
                        "value": "false",
                        "key": "isBPG"
                     },
                     {
                        "value": "true",
                        "key": "cashoutAvailable"
                     }
                  ]
               },
               "rule4S": [],
               "names": {
                  "longName": "Money Line (Incl. OT)",
                  "shortName": "Money Line (Incl. OT)"
               },
               "id": 69661279,
               "name": "Money Line (Incl. OT)",
               "type": "BASKETBALL:FTOT:ML",
               "subtype": "M",
               "displayOrder": -500,
               "columnCount": 2,
               "suspended": false,
               "displayed": true,
               "winningLegsBonus": false,
               "periodMarket": false,
               "period": "M"
            },
            {
               "selection": [
                  {
                     "odds": {
                        "dec": "1.91",
                        "frac": "10/11",
                        "rootIdx": 74
                     },
                     "pos": {
                        "row": 1,
                        "col": 2
                     },
                     "names": {
                        "longName": "Los Angeles Clippers {awayLine}",
                        "shortName": "Los Angeles Clippers {awayLine}"
                     },
                     "wasPrice": [],
                     "id": 202302894,
                     "name": "Los Angeles Clippers {awayLine}",
                     "type": "BH",
                     "suspended": false
                  },
                  {
                     "odds": {
                        "dec": "1.91",
                        "frac": "10/11",
                        "rootIdx": 74
                     },
                     "pos": {
                        "row": 1,
                        "col": 1
                     },
                     "names": {
                        "longName": "Phoenix Suns {homeLine}",
                        "shortName": "Phoenix Suns {homeLine}"
                     },
                     "wasPrice": [],
                     "id": 202302910,
                     "name": "Phoenix Suns {homeLine}",
                     "type": "AH",
                     "suspended": false
                  }
               ],
               "attributes": {
                  "attrib": [
                     {
                        "value": "false",
                        "key": "isBPG"
                     }
                  ]
               },
               "rule4S": [],
               "names": {
                  "longName": "Spread",
                  "shortName": "Spread"
               },
               "id": 69661263,
               "name": "Spread",
               "type": "BASKETBALL:FTOT:AHCP_MAIN",
               "subtype": "3.5",
               "line": 3.5,
               "lineType": "HANDICAP",
               "displayOrder": -495,
               "columnCount": 2,
               "suspended": false,
               "displayed": true,
               "winningLegsBonus": false,
               "periodMarket": false,
               "period": "3.5"
            },
            {
               "selection": [
                  {
                     "odds": {
                        "dec": "1.91",
                        "frac": "10/11",
                        "rootIdx": 74
                     },
                     "pos": {
                        "row": 1,
                        "col": 2
                     },
                     "names": {
                        "longName": "Under {Line}",
                        "shortName": "Under {Line}"
                     },
                     "wasPrice": [],
                     "id": 202304251,
                     "name": "Under {Line}",
                     "type": "Under",
                     "suspended": false
                  },
                  {
                     "odds": {
                        "dec": "1.91",
                        "frac": "10/11",
                        "rootIdx": 74
                     },
                     "pos": {
                        "row": 1,
                        "col": 1
                     },
                     "names": {
                        "longName": "Over {Line}",
                        "shortName": "Over {Line}"
                     },
                     "wasPrice": [],
                     "id": 202304249,
                     "name": "Over {Line}",
                     "type": "Over",
                     "suspended": false
                  }
               ],
               "attributes": {
                  "attrib": [
                     {
                        "value": "false",
                        "key": "isBPG"
                     }
                  ]
               },
               "rule4S": [],
               "names": {
                  "longName": "Total Points",
                  "shortName": "Total Points"
               },
               "id": 69661893,
               "name": "Total Points",
               "type": "BASKETBALL:FTOT:OU_MAIN",
               "subtype": "215.5",
               "line": 215.5,
               "lineType": "TOTAL",
               "displayOrder": -490,
               "columnCount": 2,
               "suspended": false,
               "displayed": true,
               "winningLegsBonus": false,
               "periodMarket": false,
               "period": "215.5"
            }
         ],
         "pastForm": [],
         "participants": {
            "participant": [
               {
                  "names": {
                     "longName": "Phoenix Suns",
                     "shortName": "PHX"
                  },
                  "id": 4062467,
                  "name": "Phoenix Suns",
                  "type": "HOME"
               },
               {
                  "names": {
                     "longName": "Los Angeles Clippers",
                     "shortName": "LAC"
                  },
                  "id": 4062552,
                  "name": "Los Angeles Clippers",
                  "type": "AWAY"
               }
            ]
         },
         "names": {
            "longName": "Phoenix Suns vs. Los Angeles Clippers",
            "shortName": "PHX vs. LAC",
            "veryshortName": "Phoenix Suns vs. Los Angeles Clippers"
         },
         "compNames": {
            "longName": "NBA",
            "shortName": "NBA"
         },
         "categoryNames": {
            "longName": "USA",
            "shortName": "USA"
         },
         "id": 8712230,
         "compId": 8370429,
         "compName": "NBA",
         "compWeighting": 100000.0,
         "categoryId": 8370427,
         "categoryName": "USA",
         "path": "226652:8370427:8370429:8712230",
         "name": "Phoenix Suns vs. Los Angeles Clippers",
         "sport": "BASKETBALL",
         "state": "ACTIVE",
         "locale": "en-us",
         "displayed": true,
         "offeredInplay": true,
         "isInplay": false,
         "eventTime": 1609722000000,
         "numMarkets": 210,
         "numSpecSelections": 3,
         "numRABSelections": 42,
         "lastUpdatedTime": 1609711791055,
         "outright": false,
         "neutralVenue": false,
         "winningLegsBonus": false,
         "usDisplay": true
      },
      {
         "attributes": {
            "attrib": [
               {
                  "value": "sr:match:24751188",
                  "key": "betradarId"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               },
               {
                  "value": "NBA TV",
                  "key": "TvDataChannel"
               },
               {
                  "value": "1609724100000",
                  "key": "TvDataStartTime"
               },
               {
                  "value": "1609731300000",
                  "key": "TvDataEndTime"
               }
            ]
         },
         "markets": [
            {
               "selection": [
                  {
                     "odds": {
                        "dec": "1.55",
                        "frac": "11/20",
                        "rootIdx": 51
                     },
                     "pos": {
                        "row": 1,
                        "col": 2
                     },
                     "names": {
                        "longName": "Portland Trail Blazers",
                        "shortName": "Portland Trail Blazers"
                     },
                     "wasPrice": [],
                     "id": 202303368,
                     "name": "Portland Trail Blazers",
                     "type": "BH",
                     "suspended": false
                  },
                  {
                     "odds": {
                        "dec": "2.45",
                        "frac": "29/20",
                        "rootIdx": 107
                     },
                     "pos": {
                        "row": 1,
                        "col": 1
                     },
                     "names": {
                        "longName": "Golden State Warriors",
                        "shortName": "Golden State Warriors"
                     },
                     "wasPrice": [],
                     "id": 202303361,
                     "name": "Golden State Warriors",
                     "type": "AH",
                     "suspended": false
                  }
               ],
               "attributes": {
                  "attrib": [
                     {
                        "value": "false",
                        "key": "isBPG"
                     },
                     {
                        "value": "true",
                        "key": "cashoutAvailable"
                     }
                  ]
               },
               "rule4S": [],
               "names": {
                  "longName": "Money Line (Incl. OT)",
                  "shortName": "Money Line (Incl. OT)"
               },
               "id": 69661482,
               "name": "Money Line (Incl. OT)",
               "type": "BASKETBALL:FTOT:ML",
               "subtype": "M",
               "displayOrder": -500,
               "columnCount": 2,
               "suspended": false,
               "displayed": true,
               "winningLegsBonus": false,
               "periodMarket": false,
               "period": "M"
            },
            {
               "selection": [
                  {
                     "odds": {
                        "dec": "1.91",
                        "frac": "10/11",
                        "rootIdx": 74
                     },
                     "pos": {
                        "row": 1,
                        "col": 2
                     },
                     "names": {
                        "longName": "Portland Trail Blazers {awayLine}",
                        "shortName": "Portland Trail Blazers {awayLine}"
                     },
                     "wasPrice": [],
                     "id": 202303369,
                     "name": "Portland Trail Blazers {awayLine}",
                     "type": "BH",
                     "suspended": false
                  },
                  {
                     "odds": {
                        "dec": "1.91",
                        "frac": "10/11",
                        "rootIdx": 74
                     },
                     "pos": {
                        "row": 1,
                        "col": 1
                     },
                     "names": {
                        "longName": "Golden State Warriors {homeLine}",
                        "shortName": "Golden State Warriors {homeLine}"
                     },
                     "wasPrice": [],
                     "id": 202303363,
                     "name": "Golden State Warriors {homeLine}",
                     "type": "AH",
                     "suspended": false
                  }
               ],
               "attributes": {
                  "attrib": [
                     {
                        "value": "false",
                        "key": "isBPG"
                     }
                  ]
               },
               "rule4S": [],
               "names": {
                  "longName": "Spread",
                  "shortName": "Spread"
               },
               "id": 69661484,
               "name": "Spread",
               "type": "BASKETBALL:FTOT:AHCP_MAIN",
               "subtype": "4.5",
               "line": 4.5,
               "lineType": "HANDICAP",
               "displayOrder": -495,
               "columnCount": 2,
               "suspended": false,
               "displayed": true,
               "winningLegsBonus": false,
               "periodMarket": false,
               "period": "4.5"
            },
            {
               "selection": [
                  {
                     "odds": {
                        "dec": "1.91",
                        "frac": "10/11",
                        "rootIdx": 74
                     },
                     "pos": {
                        "row": 1,
                        "col": 2
                     },
                     "names": {
                        "longName": "Under {Line}",
                        "shortName": "Under {Line}"
                     },
                     "wasPrice": [],
                     "id": 202305127,
                     "name": "Under {Line}",
                     "type": "Under",
                     "suspended": false
                  },
                  {
                     "odds": {
                        "dec": "1.91",
                        "frac": "10/11",
                        "rootIdx": 74
                     },
                     "pos": {
                        "row": 1,
                        "col": 1
                     },
                     "names": {
                        "longName": "Over {Line}",
                        "shortName": "Over {Line}"
                     },
                     "wasPrice": [],
                     "id": 202305125,
                     "name": "Over {Line}",
                     "type": "Over",
                     "suspended": false
                  }
               ],
               "attributes": {
                  "attrib": [
                     {
                        "value": "false",
                        "key": "isBPG"
                     }
                  ]
               },
               "rule4S": [],
               "names": {
                  "longName": "Total Points",
                  "shortName": "Total Points"
               },
               "id": 69662247,
               "name": "Total Points",
               "type": "BASKETBALL:FTOT:OU_MAIN",
               "subtype": "235.0",
               "line": 235.0,
               "lineType": "TOTAL",
               "displayOrder": -490,
               "columnCount": 2,
               "suspended": false,
               "displayed": true,
               "winningLegsBonus": false,
               "periodMarket": false,
               "period": "235"
            }
         ],
         "pastForm": [],
         "participants": {
            "participant": [
               {
                  "names": {
                     "longName": "Golden State Warriors",
                     "shortName": "GSW"
                  },
                  "id": 4062499,
                  "name": "Golden State Warriors",
                  "type": "HOME"
               },
               {
                  "names": {
                     "longName": "Portland Trail Blazers",
                     "shortName": "POR"
                  },
                  "id": 4062503,
                  "name": "Portland Trail Blazers",
                  "type": "AWAY"
               }
            ]
         },
         "names": {
            "longName": "Golden State Warriors vs. Portland Trail Blazers",
            "shortName": "GSW vs. POR",
            "veryshortName": "Golden State Warriors vs. Portland Trail Blazers"
         },
         "compNames": {
            "longName": "NBA",
            "shortName": "NBA"
         },
         "categoryNames": {
            "longName": "USA",
            "shortName": "USA"
         },
         "id": 8712232,
         "compId": 8370429,
         "compName": "NBA",
         "compWeighting": 100000.0,
         "categoryId": 8370427,
         "categoryName": "USA",
         "path": "226652:8370427:8370429:8712232",
         "name": "Golden State Warriors vs. Portland Trail Blazers",
         "sport": "BASKETBALL",
         "state": "ACTIVE",
         "locale": "en-us",
         "displayed": true,
         "offeredInplay": true,
         "isInplay": false,
         "eventTime": 1609723800000,
         "numMarkets": 208,
         "numSpecSelections": 4,
         "numRABSelections": 44,
         "lastUpdatedTime": 1609711210231,
         "outright": false,
         "neutralVenue": false,
         "eventCode": "16683568",
         "winningLegsBonus": false,
         "usDisplay": true
      },
      {
         "attributes": {
            "attrib": [
               {
                  "value": "sr:match:24751632",
                  "key": "betradarId"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               },
               {
                  "value": "ESPN 2",
                  "key": "TvDataChannel"
               },
               {
                  "value": "1609804800000",
                  "key": "TvDataStartTime"
               },
               {
                  "value": "1609812000000",
                  "key": "TvDataEndTime"
               }
            ]
         },
         "markets": [
            {
               "selection": [
                  {
                     "odds": {
                        "dec": "5.25",
                        "frac": "17/4",
                        "rootIdx": 163
                     },
                     "pos": {
                        "row": 1,
                        "col": 2
                     },
                     "names": {
                        "longName": "Charlotte Hornets",
                        "shortName": "Charlotte Hornets"
                     },
                     "wasPrice": [],
                     "id": 202674749,
                     "name": "Charlotte Hornets",
                     "type": "BH",
                     "suspended": false
                  },
                  {
                     "odds": {
                        "dec": "1.15",
                        "frac": "2/13",
                        "rootIdx": 23
                     },
                     "pos": {
                        "row": 1,
                        "col": 1
                     },
                     "names": {
                        "longName": "Philadelphia 76ers",
                        "shortName": "Philadelphia 76ers"
                     },
                     "wasPrice": [],
                     "id": 202674768,
                     "name": "Philadelphia 76ers",
                     "type": "AH",
                     "suspended": false
                  }
               ],
               "attributes": {
                  "attrib": [
                     {
                        "value": "false",
                        "key": "isBPG"
                     },
                     {
                        "value": "true",
                        "key": "cashoutAvailable"
                     }
                  ]
               },
               "rule4S": [],
               "names": {
                  "longName": "Money Line (Incl. OT)",
                  "shortName": "Money Line (Incl. OT)"
               },
               "id": 69778435,
               "name": "Money Line (Incl. OT)",
               "type": "BASKETBALL:FTOT:ML",
               "subtype": "M",
               "displayOrder": -500,
               "columnCount": 2,
               "suspended": false,
               "displayed": true,
               "winningLegsBonus": false,
               "periodMarket": false,
               "period": "M"
            },
            {
               "selection": [
                  {
                     "odds": {
                        "dec": "1.91",
                        "frac": "10/11",
                        "rootIdx": 74
                     },
                     "pos": {
                        "row": 1,
                        "col": 1
                     },
                     "names": {
                        "longName": "Philadelphia 76ers {homeLine}",
                        "shortName": "Philadelphia 76ers {homeLine}"
                     },
                     "wasPrice": [],
                     "id": 202674716,
                     "name": "Philadelphia 76ers {homeLine}",
                     "type": "AH",
                     "suspended": false
                  },
                  {
                     "odds": {
                        "dec": "1.91",
                        "frac": "10/11",
                        "rootIdx": 74
                     },
                     "pos": {
                        "row": 1,
                        "col": 2
                     },
                     "names": {
                        "longName": "Charlotte Hornets {awayLine}",
                        "shortName": "Charlotte Hornets {awayLine}"
                     },
                     "wasPrice": [],
                     "id": 202674714,
                     "name": "Charlotte Hornets {awayLine}",
                     "type": "BH",
                     "suspended": false
                  }
               ],
               "attributes": {
                  "attrib": [
                     {
                        "value": "false",
                        "key": "isBPG"
                     }
                  ]
               },
               "rule4S": [],
               "names": {
                  "longName": "Spread",
                  "shortName": "Spread"
               },
               "id": 69778417,
               "name": "Spread",
               "type": "BASKETBALL:FTOT:AHCP_MAIN",
               "subtype": "-9.5",
               "line": -9.5,
               "lineType": "HANDICAP",
               "displayOrder": -495,
               "columnCount": 2,
               "suspended": false,
               "displayed": true,
               "winningLegsBonus": false,
               "periodMarket": false,
               "period": "-9.5"
            },
            {
               "selection": [
                  {
                     "odds": {
                        "dec": "1.91",
                        "frac": "10/11",
                        "rootIdx": 74
                     },
                     "pos": {
                        "row": 1,
                        "col": 2
                     },
                     "names": {
                        "longName": "Under {Line}",
                        "shortName": "Under {Line}"
                     },
                     "wasPrice": [],
                     "id": 202676503,
                     "name": "Under {Line}",
                     "type": "Under",
                     "suspended": false
                  },
                  {
                     "odds": {
                        "dec": "1.91",
                        "frac": "10/11",
                        "rootIdx": 74
                     },
                     "pos": {
                        "row": 1,
                        "col": 1
                     },
                     "names": {
                        "longName": "Over {Line}",
                        "shortName": "Over {Line}"
                     },
                     "wasPrice": [],
                     "id": 202676501,
                     "name": "Over {Line}",
                     "type": "Over",
                     "suspended": false
                  }
               ],
               "attributes": {
                  "attrib": [
                     {
                        "value": "false",
                        "key": "isBPG"
                     }
                  ]
               },
               "rule4S": [],
               "names": {
                  "longName": "Total Points",
                  "shortName": "Total Points"
               },
               "id": 69779174,
               "name": "Total Points",
               "type": "BASKETBALL:FTOT:OU_MAIN",
               "subtype": "219.0",
               "line": 219.0,
               "lineType": "TOTAL",
               "displayOrder": -490,
               "columnCount": 2,
               "suspended": false,
               "displayed": true,
               "winningLegsBonus": false,
               "periodMarket": false,
               "period": "219"
            }
         ],
         "pastForm": [],
         "participants": {
            "participant": [
               {
                  "names": {
                     "longName": "Philadelphia 76ers",
                     "shortName": "PHI"
                  },
                  "id": 4062516,
                  "name": "Philadelphia 76ers",
                  "type": "HOME"
               },
               {
                  "names": {
                     "longName": "Charlotte Hornets",
                     "shortName": "CHA"
                  },
                  "id": 4062478,
                  "name": "Charlotte Hornets",
                  "type": "AWAY"
               }
            ]
         },
         "names": {
            "longName": "Philadelphia 76ers vs. Charlotte Hornets",
            "shortName": "PHI vs. CHA",
            "veryshortName": "Philadelphia 76ers vs. Charlotte Hornets"
         },
         "compNames": {
            "longName": "NBA",
            "shortName": "NBA"
         },
         "categoryNames": {
            "longName": "USA",
            "shortName": "USA"
         },
         "id": 8713128,
         "compId": 8370429,
         "compName": "NBA",
         "compWeighting": 100000.0,
         "categoryId": 8370427,
         "categoryName": "USA",
         "path": "226652:8370427:8370429:8713128",
         "name": "Philadelphia 76ers vs. Charlotte Hornets",
         "sport": "BASKETBALL",
         "state": "ACTIVE",
         "locale": "en-us",
         "displayed": true,
         "offeredInplay": true,
         "isInplay": false,
         "eventTime": 1609804800000,
         "numMarkets": 143,
         "lastUpdatedTime": 1609709799250,
         "outright": false,
         "neutralVenue": false,
         "winningLegsBonus": false,
         "usDisplay": true
      },
      {
         "attributes": {
            "attrib": [
               {
                  "value": "sr:match:24750726",
                  "key": "betradarId"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               },
               {
                  "value": "MSG Plus",
                  "key": "TvDataChannel"
               },
               {
                  "value": "1609806900000",
                  "key": "TvDataStartTime"
               },
               {
                  "value": "1609814100000",
                  "key": "TvDataEndTime"
               }
            ]
         },
         "markets": [
            {
               "selection": [
                  {
                     "odds": {
                        "dec": "3",
                        "frac": "2/1",
                        "rootIdx": 120
                     },
                     "pos": {
                        "row": 1,
                        "col": 2
                     },
                     "names": {
                        "longName": "New York Knicks",
                        "shortName": "New York Knicks"
                     },
                     "wasPrice": [],
                     "id": 202673613,
                     "name": "New York Knicks",
                     "type": "BH",
                     "suspended": false
                  },
                  {
                     "odds": {
                        "dec": "1.4",
                        "frac": "2/5",
                        "rootIdx": 41
                     },
                     "pos": {
                        "row": 1,
                        "col": 1
                     },
                     "names": {
                        "longName": "Atlanta Hawks",
                        "shortName": "Atlanta Hawks"
                     },
                     "wasPrice": [],
                     "id": 202673611,
                     "name": "Atlanta Hawks",
                     "type": "AH",
                     "suspended": false
                  }
               ],
               "attributes": {
                  "attrib": [
                     {
                        "value": "false",
                        "key": "isBPG"
                     },
                     {
                        "value": "true",
                        "key": "cashoutAvailable"
                     }
                  ]
               },
               "rule4S": [],
               "names": {
                  "longName": "Money Line (Incl. OT)",
                  "shortName": "Money Line (Incl. OT)"
               },
               "id": 69778047,
               "name": "Money Line (Incl. OT)",
               "type": "BASKETBALL:FTOT:ML",
               "subtype": "M",
               "displayOrder": -500,
               "columnCount": 2,
               "suspended": false,
               "displayed": true,
               "winningLegsBonus": false,
               "periodMarket": false,
               "period": "M"
            },
            {
               "selection": [
                  {
                     "odds": {
                        "dec": "1.85",
                        "frac": "17/20",
                        "rootIdx": 70
                     },
                     "pos": {
                        "row": 1,
                        "col": 1
                     },
                     "names": {
                        "longName": "Atlanta Hawks {homeLine}",
                        "shortName": "Atlanta Hawks {homeLine}"
                     },
                     "wasPrice": [],
                     "id": 202673625,
                     "name": "Atlanta Hawks {homeLine}",
                     "type": "AH",
                     "suspended": false
                  },
                  {
                     "odds": {
                        "dec": "2",
                        "frac": "1/1",
                        "rootIdx": 83
                     },
                     "pos": {
                        "row": 1,
                        "col": 2
                     },
                     "names": {
                        "longName": "New York Knicks {awayLine}",
                        "shortName": "New York Knicks {awayLine}"
                     },
                     "wasPrice": [],
                     "id": 202673637,
                     "name": "New York Knicks {awayLine}",
                     "type": "BH",
                     "suspended": false
                  }
               ],
               "attributes": {
                  "attrib": [
                     {
                        "value": "false",
                        "key": "isBPG"
                     }
                  ]
               },
               "rule4S": [],
               "names": {
                  "longName": "Spread",
                  "shortName": "Spread"
               },
               "id": 69778049,
               "name": "Spread",
               "type": "BASKETBALL:FTOT:AHCP_MAIN",
               "subtype": "-5.5",
               "line": -5.5,
               "lineType": "HANDICAP",
               "displayOrder": -495,
               "columnCount": 2,
               "suspended": false,
               "displayed": true,
               "winningLegsBonus": false,
               "periodMarket": false,
               "period": "-5.5"
            },
            {
               "selection": [
                  {
                     "odds": {
                        "dec": "1.95",
                        "frac": "19/20",
                        "rootIdx": 78
                     },
                     "pos": {
                        "row": 1,
                        "col": 2
                     },
                     "names": {
                        "longName": "Under {Line}",
                        "shortName": "Under {Line}"
                     },
                     "wasPrice": [],
                     "id": 202678123,
                     "name": "Under {Line}",
                     "type": "Under",
                     "suspended": false
                  },
                  {
                     "odds": {
                        "dec": "1.91",
                        "frac": "10/11",
                        "rootIdx": 74
                     },
                     "pos": {
                        "row": 1,
                        "col": 1
                     },
                     "names": {
                        "longName": "Over {Line}",
                        "shortName": "Over {Line}"
                     },
                     "wasPrice": [],
                     "id": 202678121,
                     "name": "Over {Line}",
                     "type": "Over",
                     "suspended": false
                  }
               ],
               "attributes": {
                  "attrib": [
                     {
                        "value": "false",
                        "key": "isBPG"
                     }
                  ]
               },
               "rule4S": [],
               "names": {
                  "longName": "Total Points",
                  "shortName": "Total Points"
               },
               "id": 69779878,
               "name": "Total Points",
               "type": "BASKETBALL:FTOT:OU_MAIN",
               "subtype": "221.5",
               "line": 221.5,
               "lineType": "TOTAL",
               "displayOrder": -490,
               "columnCount": 2,
               "suspended": false,
               "displayed": true,
               "winningLegsBonus": false,
               "periodMarket": false,
               "period": "221.5"
            }
         ],
         "pastForm": [],
         "participants": {
            "participant": [
               {
                  "names": {
                     "longName": "Atlanta Hawks",
                     "shortName": "ATL"
                  },
                  "id": 4062480,
                  "name": "Atlanta Hawks",
                  "type": "HOME"
               },
               {
                  "names": {
                     "longName": "New York Knicks",
                     "shortName": "NYK"
                  },
                  "id": 4062507,
                  "name": "New York Knicks",
                  "type": "AWAY"
               }
            ]
         },
         "names": {
            "longName": "Atlanta Hawks vs. New York Knicks",
            "shortName": "ATL vs. NYK",
            "veryshortName": "Atlanta Hawks vs. New York Knicks"
         },
         "compNames": {
            "longName": "NBA",
            "shortName": "NBA"
         },
         "categoryNames": {
            "longName": "USA",
            "shortName": "USA"
         },
         "id": 8713122,
         "compId": 8370429,
         "compName": "NBA",
         "compWeighting": 100000.0,
         "categoryId": 8370427,
         "categoryName": "USA",
         "path": "226652:8370427:8370429:8713122",
         "name": "Atlanta Hawks vs. New York Knicks",
         "sport": "BASKETBALL",
         "state": "ACTIVE",
         "locale": "en-us",
         "displayed": true,
         "offeredInplay": true,
         "isInplay": false,
         "eventTime": 1609806600000,
         "numMarkets": 143,
         "lastUpdatedTime": 1609707750201,
         "outright": false,
         "neutralVenue": false,
         "winningLegsBonus": false,
         "usDisplay": true
      },
      {
         "attributes": {
            "attrib": [
               {
                  "value": "sr:match:24750930",
                  "key": "betradarId"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               },
               {
                  "value": "MSG Plus",
                  "key": "TvDataChannel"
               },
               {
                  "value": "1609806900000",
                  "key": "TvDataStartTime"
               },
               {
                  "value": "1609814100000",
                  "key": "TvDataEndTime"
               }
            ]
         },
         "markets": [
            {
               "selection": [
                  {
                     "odds": {
                        "dec": "4",
                        "frac": "3/1",
                        "rootIdx": 140
                     },
                     "pos": {
                        "row": 1,
                        "col": 2
                     },
                     "names": {
                        "longName": "Oklahoma City Thunder",
                        "shortName": "Oklahoma City Thunder"
                     },
                     "wasPrice": [],
                     "id": 202673410,
                     "name": "Oklahoma City Thunder",
                     "type": "BH",
                     "suspended": false
                  },
                  {
                     "odds": {
                        "dec": "1.25",
                        "frac": "1/4",
                        "rootIdx": 32
                     },
                     "pos": {
                        "row": 1,
                        "col": 1
                     },
                     "names": {
                        "longName": "Miami Heat",
                        "shortName": "Miami Heat"
                     },
                     "wasPrice": [],
                     "id": 202673407,
                     "name": "Miami Heat",
                     "type": "AH",
                     "suspended": false
                  }
               ],
               "attributes": {
                  "attrib": [
                     {
                        "value": "false",
                        "key": "isBPG"
                     },
                     {
                        "value": "true",
                        "key": "cashoutAvailable"
                     }
                  ]
               },
               "rule4S": [],
               "names": {
                  "longName": "Money Line (Incl. OT)",
                  "shortName": "Money Line (Incl. OT)"
               },
               "id": 69777937,
               "name": "Money Line (Incl. OT)",
               "type": "BASKETBALL:FTOT:ML",
               "subtype": "M",
               "displayOrder": -500,
               "columnCount": 2,
               "suspended": false,
               "displayed": true,
               "winningLegsBonus": false,
               "periodMarket": false,
               "period": "M"
            },
            {
               "selection": [
                  {
                     "odds": {
                        "dec": "1.85",
                        "frac": "17/20",
                        "rootIdx": 70
                     },
                     "pos": {
                        "row": 1,
                        "col": 1
                     },
                     "names": {
                        "longName": "Miami Heat {homeLine}",
                        "shortName": "Miami Heat {homeLine}"
                     },
                     "wasPrice": [],
                     "id": 202673411,
                     "name": "Miami Heat {homeLine}",
                     "type": "AH",
                     "suspended": false
                  },
                  {
                     "odds": {
                        "dec": "2",
                        "frac": "1/1",
                        "rootIdx": 83
                     },
                     "pos": {
                        "row": 1,
                        "col": 2
                     },
                     "names": {
                        "longName": "Oklahoma City Thunder {awayLine}",
                        "shortName": "Oklahoma City Thunder {awayLine}"
                     },
                     "wasPrice": [],
                     "id": 202673413,
                     "name": "Oklahoma City Thunder {awayLine}",
                     "type": "BH",
                     "suspended": false
                  }
               ],
               "attributes": {
                  "attrib": [
                     {
                        "value": "false",
                        "key": "isBPG"
                     }
                  ]
               },
               "rule4S": [],
               "names": {
                  "longName": "Spread",
                  "shortName": "Spread"
               },
               "id": 69777939,
               "name": "Spread",
               "type": "BASKETBALL:FTOT:AHCP_MAIN",
               "subtype": "-7.5",
               "line": -7.5,
               "lineType": "HANDICAP",
               "displayOrder": -495,
               "columnCount": 2,
               "suspended": false,
               "displayed": true,
               "winningLegsBonus": false,
               "periodMarket": false,
               "period": "-7.5"
            },
            {
               "selection": [
                  {
                     "odds": {
                        "dec": "1.91",
                        "frac": "10/11",
                        "rootIdx": 74
                     },
                     "pos": {
                        "row": 1,
                        "col": 2
                     },
                     "names": {
                        "longName": "Under {Line}",
                        "shortName": "Under {Line}"
                     },
                     "wasPrice": [],
                     "id": 202677351,
                     "name": "Under {Line}",
                     "type": "Under",
                     "suspended": false
                  },
                  {
                     "odds": {
                        "dec": "1.95",
                        "frac": "19/20",
                        "rootIdx": 78
                     },
                     "pos": {
                        "row": 1,
                        "col": 1
                     },
                     "names": {
                        "longName": "Over {Line}",
                        "shortName": "Over {Line}"
                     },
                     "wasPrice": [],
                     "id": 202677349,
                     "name": "Over {Line}",
                     "type": "Over",
                     "suspended": false
                  }
               ],
               "attributes": {
                  "attrib": [
                     {
                        "value": "false",
                        "key": "isBPG"
                     }
                  ]
               },
               "rule4S": [],
               "names": {
                  "longName": "Total Points",
                  "shortName": "Total Points"
               },
               "id": 69779548,
               "name": "Total Points",
               "type": "BASKETBALL:FTOT:OU_MAIN",
               "subtype": "213.0",
               "line": 213.0,
               "lineType": "TOTAL",
               "displayOrder": -490,
               "columnCount": 2,
               "suspended": false,
               "displayed": true,
               "winningLegsBonus": false,
               "periodMarket": false,
               "period": "213"
            }
         ],
         "pastForm": [],
         "participants": {
            "participant": [
               {
                  "names": {
                     "longName": "Miami Heat",
                     "shortName": "MIA"
                  },
                  "id": 4062479,
                  "name": "Miami Heat",
                  "type": "HOME"
               },
               {
                  "names": {
                     "longName": "Oklahoma City Thunder",
                     "shortName": "OKC"
                  },
                  "id": 4062514,
                  "name": "Oklahoma City Thunder",
                  "type": "AWAY"
               }
            ]
         },
         "names": {
            "longName": "Miami Heat vs. Oklahoma City Thunder",
            "shortName": "MIA vs. OKC",
            "veryshortName": "Miami Heat vs. Oklahoma City Thunder"
         },
         "compNames": {
            "longName": "NBA",
            "shortName": "NBA"
         },
         "categoryNames": {
            "longName": "USA",
            "shortName": "USA"
         },
         "id": 8713120,
         "compId": 8370429,
         "compName": "NBA",
         "compWeighting": 100000.0,
         "categoryId": 8370427,
         "categoryName": "USA",
         "path": "226652:8370427:8370429:8713120",
         "name": "Miami Heat vs. Oklahoma City Thunder",
         "sport": "BASKETBALL",
         "state": "ACTIVE",
         "locale": "en-us",
         "displayed": true,
         "offeredInplay": true,
         "isInplay": false,
         "eventTime": 1609806600000,
         "numMarkets": 143,
         "lastUpdatedTime": 1609708902699,
         "outright": false,
         "neutralVenue": false,
         "winningLegsBonus": false,
         "usDisplay": true
      },
      {
         "attributes": {
            "attrib": [
               {
                  "value": "sr:match:24751390",
                  "key": "betradarId"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "markets": [
            {
               "selection": [
                  {
                     "odds": {
                        "dec": "2.2",
                        "frac": "6/5",
                        "rootIdx": 99
                     },
                     "pos": {
                        "row": 1,
                        "col": 2
                     },
                     "names": {
                        "longName": "Boston Celtics",
                        "shortName": "Boston Celtics"
                     },
                     "wasPrice": [],
                     "id": 202674472,
                     "name": "Boston Celtics",
                     "type": "BH",
                     "suspended": false
                  },
                  {
                     "odds": {
                        "dec": "1.67",
                        "frac": "4/6",
                        "rootIdx": 57
                     },
                     "pos": {
                        "row": 1,
                        "col": 1
                     },
                     "names": {
                        "longName": "Toronto Raptors",
                        "shortName": "Toronto Raptors"
                     },
                     "wasPrice": [],
                     "id": 202674477,
                     "name": "Toronto Raptors",
                     "type": "AH",
                     "suspended": false
                  }
               ],
               "attributes": {
                  "attrib": [
                     {
                        "value": "false",
                        "key": "isBPG"
                     },
                     {
                        "value": "true",
                        "key": "cashoutAvailable"
                     }
                  ]
               },
               "rule4S": [],
               "names": {
                  "longName": "Money Line (Incl. OT)",
                  "shortName": "Money Line (Incl. OT)"
               },
               "id": 69778293,
               "name": "Money Line (Incl. OT)",
               "type": "BASKETBALL:FTOT:ML",
               "subtype": "M",
               "displayOrder": -500,
               "columnCount": 2,
               "suspended": false,
               "displayed": true,
               "winningLegsBonus": false,
               "periodMarket": false,
               "period": "M"
            },
            {
               "selection": [
                  {
                     "odds": {
                        "dec": "1.91",
                        "frac": "10/11",
                        "rootIdx": 74
                     },
                     "pos": {
                        "row": 1,
                        "col": 1
                     },
                     "names": {
                        "longName": "Toronto Raptors {homeLine}",
                        "shortName": "Toronto Raptors {homeLine}"
                     },
                     "wasPrice": [],
                     "id": 202674432,
                     "name": "Toronto Raptors {homeLine}",
                     "type": "AH",
                     "suspended": false
                  },
                  {
                     "odds": {
                        "dec": "1.91",
                        "frac": "10/11",
                        "rootIdx": 74
                     },
                     "pos": {
                        "row": 1,
                        "col": 2
                     },
                     "names": {
                        "longName": "Boston Celtics {awayLine}",
                        "shortName": "Boston Celtics {awayLine}"
                     },
                     "wasPrice": [],
                     "id": 202674421,
                     "name": "Boston Celtics {awayLine}",
                     "type": "BH",
                     "suspended": false
                  }
               ],
               "attributes": {
                  "attrib": [
                     {
                        "value": "false",
                        "key": "isBPG"
                     }
                  ]
               },
               "rule4S": [],
               "names": {
                  "longName": "Spread",
                  "shortName": "Spread"
               },
               "id": 69778277,
               "name": "Spread",
               "type": "BASKETBALL:FTOT:AHCP_MAIN",
               "subtype": "-3.0",
               "line": -3.0,
               "lineType": "HANDICAP",
               "displayOrder": -495,
               "columnCount": 2,
               "suspended": false,
               "displayed": true,
               "winningLegsBonus": false,
               "periodMarket": false,
               "period": "-3"
            },
            {
               "selection": [
                  {
                     "odds": {
                        "dec": "1.91",
                        "frac": "10/11",
                        "rootIdx": 74
                     },
                     "pos": {
                        "row": 1,
                        "col": 2
                     },
                     "names": {
                        "longName": "Under {Line}",
                        "shortName": "Under {Line}"
                     },
                     "wasPrice": [],
                     "id": 202676009,
                     "name": "Under {Line}",
                     "type": "Under",
                     "suspended": false
                  },
                  {
                     "odds": {
                        "dec": "1.91",
                        "frac": "10/11",
                        "rootIdx": 74
                     },
                     "pos": {
                        "row": 1,
                        "col": 1
                     },
                     "names": {
                        "longName": "Over {Line}",
                        "shortName": "Over {Line}"
                     },
                     "wasPrice": [],
                     "id": 202676007,
                     "name": "Over {Line}",
                     "type": "Over",
                     "suspended": false
                  }
               ],
               "attributes": {
                  "attrib": [
                     {
                        "value": "false",
                        "key": "isBPG"
                     }
                  ]
               },
               "rule4S": [],
               "names": {
                  "longName": "Total Points",
                  "shortName": "Total Points"
               },
               "id": 69778966,
               "name": "Total Points",
               "type": "BASKETBALL:FTOT:OU_MAIN",
               "subtype": "214.5",
               "line": 214.5,
               "lineType": "TOTAL",
               "displayOrder": -490,
               "columnCount": 2,
               "suspended": false,
               "displayed": true,
               "winningLegsBonus": false,
               "periodMarket": false,
               "period": "214.5"
            }
         ],
         "pastForm": [],
         "participants": {
            "participant": [
               {
                  "names": {
                     "longName": "Toronto Raptors",
                     "shortName": "TOR"
                  },
                  "id": 4062578,
                  "name": "Toronto Raptors",
                  "type": "HOME"
               },
               {
                  "names": {
                     "longName": "Boston Celtics",
                     "shortName": "BOS"
                  },
                  "id": 4062500,
                  "name": "Boston Celtics",
                  "type": "AWAY"
               }
            ]
         },
         "names": {
            "longName": "Toronto Raptors vs. Boston Celtics",
            "shortName": "TOR vs. BOS",
            "veryshortName": "Toronto Raptors vs. Boston Celtics"
         },
         "compNames": {
            "longName": "NBA",
            "shortName": "NBA"
         },
         "categoryNames": {
            "longName": "USA",
            "shortName": "USA"
         },
         "id": 8713126,
         "compId": 8370429,
         "compName": "NBA",
         "compWeighting": 100000.0,
         "categoryId": 8370427,
         "categoryName": "USA",
         "path": "226652:8370427:8370429:8713126",
         "name": "Toronto Raptors vs. Boston Celtics",
         "sport": "BASKETBALL",
         "state": "ACTIVE",
         "locale": "en-us",
         "displayed": true,
         "offeredInplay": true,
         "isInplay": false,
         "eventTime": 1609806600000,
         "numMarkets": 143,
         "lastUpdatedTime": 1609712173390,
         "outright": false,
         "neutralVenue": false,
         "winningLegsBonus": false,
         "usDisplay": true
      },
      {
         "attributes": {
            "attrib": [
               {
                  "value": "sr:match:24750970",
                  "key": "betradarId"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               },
               {
                  "value": "NBA TV",
                  "key": "TvDataChannel"
               },
               {
                  "value": "1609808700000",
                  "key": "TvDataStartTime"
               },
               {
                  "value": "1609815900000",
                  "key": "TvDataEndTime"
               }
            ]
         },
         "markets": [
            {
               "selection": [
                  {
                     "odds": {
                        "dec": "8.5",
                        "frac": "15/2",
                        "rootIdx": 190
                     },
                     "pos": {
                        "row": 1,
                        "col": 2
                     },
                     "names": {
                        "longName": "Detroit Pistons",
                        "shortName": "Detroit Pistons"
                     },
                     "wasPrice": [],
                     "id": 202702303,
                     "name": "Detroit Pistons",
                     "type": "BH",
                     "suspended": false
                  },
                  {
                     "odds": {
                        "dec": "1.07",
                        "frac": "1/14",
                        "rootIdx": 14
                     },
                     "pos": {
                        "row": 1,
                        "col": 1
                     },
                     "names": {
                        "longName": "Milwaukee Bucks",
                        "shortName": "Milwaukee Bucks"
                     },
                     "wasPrice": [],
                     "id": 202702305,
                     "name": "Milwaukee Bucks",
                     "type": "AH",
                     "suspended": false
                  }
               ],
               "attributes": {
                  "attrib": [
                     {
                        "value": "false",
                        "key": "isBPG"
                     },
                     {
                        "value": "true",
                        "key": "cashoutAvailable"
                     }
                  ]
               },
               "rule4S": [],
               "names": {
                  "longName": "Money Line (Incl. OT)",
                  "shortName": "Money Line (Incl. OT)"
               },
               "id": 69788459,
               "name": "Money Line (Incl. OT)",
               "type": "BASKETBALL:FTOT:ML",
               "subtype": "M",
               "displayOrder": -500,
               "columnCount": 2,
               "suspended": false,
               "displayed": true,
               "winningLegsBonus": false,
               "periodMarket": false,
               "period": "M"
            },
            {
               "selection": [
                  {
                     "odds": {
                        "dec": "1.91",
                        "frac": "10/11",
                        "rootIdx": 74
                     },
                     "pos": {
                        "row": 1,
                        "col": 2
                     },
                     "names": {
                        "longName": "Detroit Pistons {awayLine}",
                        "shortName": "Detroit Pistons {awayLine}"
                     },
                     "wasPrice": [],
                     "id": 202702260,
                     "name": "Detroit Pistons {awayLine}",
                     "type": "BH",
                     "suspended": false
                  },
                  {
                     "odds": {
                        "dec": "1.91",
                        "frac": "10/11",
                        "rootIdx": 74
                     },
                     "pos": {
                        "row": 1,
                        "col": 1
                     },
                     "names": {
                        "longName": "Milwaukee Bucks {homeLine}",
                        "shortName": "Milwaukee Bucks {homeLine}"
                     },
                     "wasPrice": [],
                     "id": 202702271,
                     "name": "Milwaukee Bucks {homeLine}",
                     "type": "AH",
                     "suspended": false
                  }
               ],
               "attributes": {
                  "attrib": [
                     {
                        "value": "false",
                        "key": "isBPG"
                     }
                  ]
               },
               "rule4S": [],
               "names": {
                  "longName": "Spread",
                  "shortName": "Spread"
               },
               "id": 69788444,
               "name": "Spread",
               "type": "BASKETBALL:FTOT:AHCP_MAIN",
               "subtype": "-14",
               "line": -14.0,
               "lineType": "HANDICAP",
               "displayOrder": -495,
               "columnCount": 2,
               "suspended": false,
               "displayed": true,
               "winningLegsBonus": false,
               "periodMarket": false,
               "period": "-14"
            }
         ],
         "pastForm": [],
         "participants": {
            "participant": [
               {
                  "names": {
                     "longName": "Milwaukee Bucks",
                     "shortName": "MIL"
                  },
                  "id": 4062504,
                  "name": "Milwaukee Bucks",
                  "type": "HOME"
               },
               {
                  "names": {
                     "longName": "Detroit Pistons",
                     "shortName": "DET"
                  },
                  "id": 4062491,
                  "name": "Detroit Pistons",
                  "type": "AWAY"
               }
            ]
         },
         "names": {
            "longName": "Milwaukee Bucks vs. Detroit Pistons",
            "shortName": "MIL vs. DET",
            "veryshortName": "Milwaukee Bucks vs. Detroit Pistons"
         },
         "compNames": {
            "longName": "NBA",
            "shortName": "NBA"
         },
         "categoryNames": {
            "longName": "USA",
            "shortName": "USA"
         },
         "id": 8713180,
         "compId": 8370429,
         "compName": "NBA",
         "compWeighting": 100000.0,
         "categoryId": 8370427,
         "categoryName": "USA",
         "path": "226652:8370427:8370429:8713180",
         "name": "Milwaukee Bucks vs. Detroit Pistons",
         "sport": "BASKETBALL",
         "state": "ACTIVE",
         "locale": "en-us",
         "displayed": true,
         "offeredInplay": true,
         "isInplay": false,
         "missingKeyMarket": true,
         "eventTime": 1609808400000,
         "numMarkets": 57,
         "lastUpdatedTime": 1609707177223,
         "outright": false,
         "neutralVenue": false,
         "winningLegsBonus": false,
         "usDisplay": true
      },
      {
         "attributes": {
            "attrib": [
               {
                  "value": "sr:match:24751076",
                  "key": "betradarId"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               },
               {
                  "value": "NBA TV",
                  "key": "TvDataChannel"
               },
               {
                  "value": "1609808700000",
                  "key": "TvDataStartTime"
               },
               {
                  "value": "1609815900000",
                  "key": "TvDataEndTime"
               }
            ]
         },
         "markets": [
            {
               "selection": [
                  {
                     "odds": {
                        "dec": "1.83",
                        "frac": "5/6",
                        "rootIdx": 69
                     },
                     "pos": {
                        "row": 1,
                        "col": 1
                     },
                     "names": {
                        "longName": "New Orleans Pelicans",
                        "shortName": "New Orleans Pelicans"
                     },
                     "wasPrice": [],
                     "id": 202675302,
                     "name": "New Orleans Pelicans",
                     "type": "AH",
                     "suspended": false
                  },
                  {
                     "odds": {
                        "dec": "2",
                        "frac": "1/1",
                        "rootIdx": 83
                     },
                     "pos": {
                        "row": 1,
                        "col": 2
                     },
                     "names": {
                        "longName": "Indiana Pacers",
                        "shortName": "Indiana Pacers"
                     },
                     "wasPrice": [],
                     "id": 202675297,
                     "name": "Indiana Pacers",
                     "type": "BH",
                     "suspended": false
                  }
               ],
               "attributes": {
                  "attrib": [
                     {
                        "value": "false",
                        "key": "isBPG"
                     },
                     {
                        "value": "true",
                        "key": "cashoutAvailable"
                     }
                  ]
               },
               "rule4S": [],
               "names": {
                  "longName": "Money Line (Incl. OT)",
                  "shortName": "Money Line (Incl. OT)"
               },
               "id": 69778698,
               "name": "Money Line (Incl. OT)",
               "type": "BASKETBALL:FTOT:ML",
               "subtype": "M",
               "displayOrder": -500,
               "columnCount": 2,
               "suspended": false,
               "displayed": true,
               "winningLegsBonus": false,
               "periodMarket": false,
               "period": "M"
            },
            {
               "selection": [
                  {
                     "odds": {
                        "dec": "1.91",
                        "frac": "10/11",
                        "rootIdx": 74
                     },
                     "pos": {
                        "row": 1,
                        "col": 2
                     },
                     "names": {
                        "longName": "Indiana Pacers {awayLine}",
                        "shortName": "Indiana Pacers {awayLine}"
                     },
                     "wasPrice": [],
                     "id": 202675113,
                     "name": "Indiana Pacers {awayLine}",
                     "type": "BH",
                     "suspended": false
                  },
                  {
                     "odds": {
                        "dec": "1.91",
                        "frac": "10/11",
                        "rootIdx": 74
                     },
                     "pos": {
                        "row": 1,
                        "col": 1
                     },
                     "names": {
                        "longName": "New Orleans Pelicans {homeLine}",
                        "shortName": "New Orleans Pelicans {homeLine}"
                     },
                     "wasPrice": [],
                     "id": 202675134,
                     "name": "New Orleans Pelicans {homeLine}",
                     "type": "AH",
                     "suspended": false
                  }
               ],
               "attributes": {
                  "attrib": [
                     {
                        "value": "false",
                        "key": "isBPG"
                     }
                  ]
               },
               "rule4S": [],
               "names": {
                  "longName": "Spread",
                  "shortName": "Spread"
               },
               "id": 69778612,
               "name": "Spread",
               "type": "BASKETBALL:FTOT:AHCP_MAIN",
               "subtype": "-1.5",
               "line": -1.5,
               "lineType": "HANDICAP",
               "displayOrder": -495,
               "columnCount": 2,
               "suspended": false,
               "displayed": true,
               "winningLegsBonus": false,
               "periodMarket": false,
               "period": "-1.5"
            },
            {
               "selection": [
                  {
                     "odds": {
                        "dec": "1.91",
                        "frac": "10/11",
                        "rootIdx": 74
                     },
                     "pos": {
                        "row": 1,
                        "col": 1
                     },
                     "names": {
                        "longName": "Over {Line}",
                        "shortName": "Over {Line}"
                     },
                     "wasPrice": [],
                     "id": 202675120,
                     "name": "Over {Line}",
                     "type": "Over",
                     "suspended": false
                  },
                  {
                     "odds": {
                        "dec": "1.91",
                        "frac": "10/11",
                        "rootIdx": 74
                     },
                     "pos": {
                        "row": 1,
                        "col": 2
                     },
                     "names": {
                        "longName": "Under {Line}",
                        "shortName": "Under {Line}"
                     },
                     "wasPrice": [],
                     "id": 202675146,
                     "name": "Under {Line}",
                     "type": "Under",
                     "suspended": false
                  }
               ],
               "attributes": {
                  "attrib": [
                     {
                        "value": "false",
                        "key": "isBPG"
                     }
                  ]
               },
               "rule4S": [],
               "names": {
                  "longName": "Total Points",
                  "shortName": "Total Points"
               },
               "id": 69778615,
               "name": "Total Points",
               "type": "BASKETBALL:FTOT:OU_MAIN",
               "subtype": "218",
               "line": 218.0,
               "lineType": "TOTAL",
               "displayOrder": -490,
               "columnCount": 2,
               "suspended": false,
               "displayed": true,
               "winningLegsBonus": false,
               "periodMarket": false,
               "period": "218"
            }
         ],
         "pastForm": [],
         "participants": {
            "participant": [
               {
                  "names": {
                     "longName": "New Orleans Pelicans",
                     "shortName": "NOP"
                  },
                  "id": 4062471,
                  "name": "New Orleans Pelicans",
                  "type": "HOME"
               },
               {
                  "names": {
                     "longName": "Indiana Pacers",
                     "shortName": "IND"
                  },
                  "id": 4062497,
                  "name": "Indiana Pacers",
                  "type": "AWAY"
               }
            ]
         },
         "names": {
            "longName": "New Orleans Pelicans vs. Indiana Pacers",
            "shortName": "NOP vs. IND",
            "veryshortName": "New Orleans Pelicans vs. Indiana Pacers"
         },
         "compNames": {
            "longName": "NBA",
            "shortName": "NBA"
         },
         "categoryNames": {
            "longName": "USA",
            "shortName": "USA"
         },
         "id": 8713130,
         "compId": 8370429,
         "compName": "NBA",
         "compWeighting": 100000.0,
         "categoryId": 8370427,
         "categoryName": "USA",
         "path": "226652:8370427:8370429:8713130",
         "name": "New Orleans Pelicans vs. Indiana Pacers",
         "sport": "BASKETBALL",
         "state": "ACTIVE",
         "locale": "en-us",
         "displayed": true,
         "offeredInplay": true,
         "isInplay": false,
         "eventTime": 1609808400000,
         "numMarkets": 143,
         "lastUpdatedTime": 1609709342801,
         "outright": false,
         "neutralVenue": false,
         "eventCode": "16683576",
         "winningLegsBonus": false,
         "usDisplay": true
      },
      {
         "attributes": {
            "attrib": [
               {
                  "value": "sr:match:24751486",
                  "key": "betradarId"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               },
               {
                  "value": "NBA TV",
                  "key": "TvDataChannel"
               },
               {
                  "value": "1609808700000",
                  "key": "TvDataStartTime"
               },
               {
                  "value": "1609815900000",
                  "key": "TvDataEndTime"
               }
            ]
         },
         "markets": [
            {
               "selection": [
                  {
                     "odds": {
                        "dec": "2.15",
                        "frac": "23/20",
                        "rootIdx": 96
                     },
                     "pos": {
                        "row": 1,
                        "col": 1
                     },
                     "names": {
                        "longName": "Golden State Warriors",
                        "shortName": "Golden State Warriors"
                     },
                     "wasPrice": [],
                     "id": 202677152,
                     "name": "Golden State Warriors",
                     "type": "AH",
                     "suspended": false
                  },
                  {
                     "odds": {
                        "dec": "1.72",
                        "frac": "8/11",
                        "rootIdx": 60
                     },
                     "pos": {
                        "row": 1,
                        "col": 2
                     },
                     "names": {
                        "longName": "Sacramento Kings",
                        "shortName": "Sacramento Kings"
                     },
                     "wasPrice": [],
                     "id": 202677165,
                     "name": "Sacramento Kings",
                     "type": "BH",
                     "suspended": false
                  }
               ],
               "attributes": {
                  "attrib": [
                     {
                        "value": "false",
                        "key": "isBPG"
                     },
                     {
                        "value": "true",
                        "key": "cashoutAvailable"
                     }
                  ]
               },
               "rule4S": [],
               "names": {
                  "longName": "Money Line (Incl. OT)",
                  "shortName": "Money Line (Incl. OT)"
               },
               "id": 69779477,
               "name": "Money Line (Incl. OT)",
               "type": "BASKETBALL:FTOT:ML",
               "subtype": "M",
               "displayOrder": -500,
               "columnCount": 2,
               "suspended": false,
               "displayed": true,
               "winningLegsBonus": false,
               "periodMarket": false,
               "period": "M"
            },
            {
               "selection": [
                  {
                     "odds": {
                        "dec": "1.85",
                        "frac": "17/20",
                        "rootIdx": 70
                     },
                     "pos": {
                        "row": 1,
                        "col": 2
                     },
                     "names": {
                        "longName": "Sacramento Kings {awayLine}",
                        "shortName": "Sacramento Kings {awayLine}"
                     },
                     "wasPrice": [],
                     "id": 202676968,
                     "name": "Sacramento Kings {awayLine}",
                     "type": "BH",
                     "suspended": false
                  },
                  {
                     "odds": {
                        "dec": "2",
                        "frac": "1/1",
                        "rootIdx": 83
                     },
                     "pos": {
                        "row": 1,
                        "col": 1
                     },
                     "names": {
                        "longName": "Golden State Warriors {homeLine}",
                        "shortName": "Golden State Warriors {homeLine}"
                     },
                     "wasPrice": [],
                     "id": 202676953,
                     "name": "Golden State Warriors {homeLine}",
                     "type": "AH",
                     "suspended": false
                  }
               ],
               "attributes": {
                  "attrib": [
                     {
                        "value": "false",
                        "key": "isBPG"
                     }
                  ]
               },
               "rule4S": [],
               "names": {
                  "longName": "Spread",
                  "shortName": "Spread"
               },
               "id": 69779392,
               "name": "Spread",
               "type": "BASKETBALL:FTOT:AHCP_MAIN",
               "subtype": "2.5",
               "line": 2.5,
               "lineType": "HANDICAP",
               "displayOrder": -495,
               "columnCount": 2,
               "suspended": false,
               "displayed": true,
               "winningLegsBonus": false,
               "periodMarket": false,
               "period": "2.5"
            },
            {
               "selection": [
                  {
                     "odds": {
                        "dec": "1.91",
                        "frac": "10/11",
                        "rootIdx": 74
                     },
                     "pos": {
                        "row": 1,
                        "col": 2
                     },
                     "names": {
                        "longName": "Under {Line}",
                        "shortName": "Under {Line}"
                     },
                     "wasPrice": [],
                     "id": 202677297,
                     "name": "Under {Line}",
                     "type": "Under",
                     "suspended": false
                  },
                  {
                     "odds": {
                        "dec": "1.91",
                        "frac": "10/11",
                        "rootIdx": 74
                     },
                     "pos": {
                        "row": 1,
                        "col": 1
                     },
                     "names": {
                        "longName": "Over {Line}",
                        "shortName": "Over {Line}"
                     },
                     "wasPrice": [],
                     "id": 202677295,
                     "name": "Over {Line}",
                     "type": "Over",
                     "suspended": false
                  }
               ],
               "attributes": {
                  "attrib": [
                     {
                        "value": "false",
                        "key": "isBPG"
                     }
                  ]
               },
               "rule4S": [],
               "names": {
                  "longName": "Total Points",
                  "shortName": "Total Points"
               },
               "id": 69779526,
               "name": "Total Points",
               "type": "BASKETBALL:FTOT:OU_MAIN",
               "subtype": "232.5",
               "line": 232.5,
               "lineType": "TOTAL",
               "displayOrder": -490,
               "columnCount": 2,
               "suspended": false,
               "displayed": true,
               "winningLegsBonus": false,
               "periodMarket": false,
               "period": "232.5"
            }
         ],
         "pastForm": [],
         "participants": {
            "participant": [
               {
                  "names": {
                     "longName": "Golden State Warriors",
                     "shortName": "GSW"
                  },
                  "id": 4062499,
                  "name": "Golden State Warriors",
                  "type": "HOME"
               },
               {
                  "names": {
                     "longName": "Sacramento Kings",
                     "shortName": "SAC"
                  },
                  "id": 4062496,
                  "name": "Sacramento Kings",
                  "type": "AWAY"
               }
            ]
         },
         "names": {
            "longName": "Golden State Warriors vs. Sacramento Kings",
            "shortName": "GSW vs. SAC",
            "veryshortName": "Golden State Warriors vs. Sacramento Kings"
         },
         "compNames": {
            "longName": "NBA",
            "shortName": "NBA"
         },
         "categoryNames": {
            "longName": "USA",
            "shortName": "USA"
         },
         "id": 8713132,
         "compId": 8370429,
         "compName": "NBA",
         "compWeighting": 100000.0,
         "categoryId": 8370427,
         "categoryName": "USA",
         "path": "226652:8370427:8370429:8713132",
         "name": "Golden State Warriors vs. Sacramento Kings",
         "sport": "BASKETBALL",
         "state": "ACTIVE",
         "locale": "en-us",
         "displayed": true,
         "offeredInplay": true,
         "isInplay": false,
         "eventTime": 1609815600000,
         "numMarkets": 143,
         "lastUpdatedTime": 1609711925107,
         "outright": false,
         "neutralVenue": false,
         "eventCode": "16683577",
         "winningLegsBonus": false,
         "usDisplay": true
      },
      {
         "attributes": {
            "attrib": [
               {
                  "value": "sr:match:24750924",
                  "key": "betradarId"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               },
               {
                  "value": "ESPN 2",
                  "key": "TvDataChannel"
               },
               {
                  "value": "1609898400000",
                  "key": "TvDataStartTime"
               },
               {
                  "value": "1609905600000",
                  "key": "TvDataEndTime"
               }
            ]
         },
         "markets": [
            {
               "selection": [
                  {
                     "odds": {
                        "dec": "4.33",
                        "frac": "10/3",
                        "rootIdx": 147
                     },
                     "pos": {
                        "row": 1,
                        "col": 2
                     },
                     "names": {
                        "longName": "Minnesota Timberwolves",
                        "shortName": "Minnesota Timberwolves"
                     },
                     "wasPrice": [],
                     "id": 202424315,
                     "name": "Minnesota Timberwolves",
                     "type": "BH",
                     "suspended": false
                  },
                  {
                     "odds": {
                        "dec": "1.22",
                        "frac": "2/9",
                        "rootIdx": 30
                     },
                     "pos": {
                        "row": 1,
                        "col": 1
                     },
                     "names": {
                        "longName": "Denver Nuggets",
                        "shortName": "Denver Nuggets"
                     },
                     "wasPrice": [],
                     "id": 202424311,
                     "name": "Denver Nuggets",
                     "type": "AH",
                     "suspended": false
                  }
               ],
               "attributes": {
                  "attrib": [
                     {
                        "value": "false",
                        "key": "isBPG"
                     },
                     {
                        "value": "true",
                        "key": "cashoutAvailable"
                     }
                  ]
               },
               "rule4S": [],
               "names": {
                  "longName": "Money Line (Incl. OT)",
                  "shortName": "Money Line (Incl. OT)"
               },
               "id": 69695755,
               "name": "Money Line (Incl. OT)",
               "type": "BASKETBALL:FTOT:ML",
               "subtype": "M",
               "displayOrder": -500,
               "columnCount": 2,
               "suspended": false,
               "displayed": true,
               "winningLegsBonus": false,
               "periodMarket": false,
               "period": "M"
            },
            {
               "selection": [
                  {
                     "odds": {
                        "dec": "1.91",
                        "frac": "10/11",
                        "rootIdx": 74
                     },
                     "pos": {
                        "row": 1,
                        "col": 1
                     },
                     "names": {
                        "longName": "Denver Nuggets {homeLine}",
                        "shortName": "Denver Nuggets {homeLine}"
                     },
                     "wasPrice": [],
                     "id": 202424302,
                     "name": "Denver Nuggets {homeLine}",
                     "type": "AH",
                     "suspended": false
                  },
                  {
                     "odds": {
                        "dec": "1.91",
                        "frac": "10/11",
                        "rootIdx": 74
                     },
                     "pos": {
                        "row": 1,
                        "col": 2
                     },
                     "names": {
                        "longName": "Minnesota Timberwolves {awayLine}",
                        "shortName": "Minnesota Timberwolves {awayLine}"
                     },
                     "wasPrice": [],
                     "id": 202424308,
                     "name": "Minnesota Timberwolves {awayLine}",
                     "type": "BH",
                     "suspended": false
                  }
               ],
               "attributes": {
                  "attrib": [
                     {
                        "value": "false",
                        "key": "isBPG"
                     }
                  ]
               },
               "rule4S": [],
               "names": {
                  "longName": "Spread",
                  "shortName": "Spread"
               },
               "id": 69695748,
               "name": "Spread",
               "type": "BASKETBALL:FTOT:AHCP_MAIN",
               "subtype": "-8.5",
               "line": -8.5,
               "lineType": "HANDICAP",
               "displayOrder": -495,
               "columnCount": 2,
               "suspended": false,
               "displayed": true,
               "winningLegsBonus": false,
               "periodMarket": false,
               "period": "-8.5"
            }
         ],
         "pastForm": [],
         "participants": {
            "participant": [
               {
                  "names": {
                     "longName": "Denver Nuggets",
                     "shortName": "DEN"
                  },
                  "id": 4062505,
                  "name": "Denver Nuggets",
                  "type": "HOME"
               },
               {
                  "names": {
                     "longName": "Minnesota Timberwolves",
                     "shortName": "MIN"
                  },
                  "id": 4062469,
                  "name": "Minnesota Timberwolves",
                  "type": "AWAY"
               }
            ]
         },
         "names": {
            "longName": "Denver Nuggets vs. Minnesota Timberwolves",
            "shortName": "DEN vs. MIN",
            "veryshortName": "Denver Nuggets vs. Minnesota Timberwolves"
         },
         "compNames": {
            "longName": "NBA",
            "shortName": "NBA"
         },
         "categoryNames": {
            "longName": "USA",
            "shortName": "USA"
         },
         "id": 8712422,
         "compId": 8370429,
         "compName": "NBA",
         "compWeighting": 100000.0,
         "categoryId": 8370427,
         "categoryName": "USA",
         "path": "226652:8370427:8370429:8712422",
         "name": "Denver Nuggets vs. Minnesota Timberwolves",
         "sport": "BASKETBALL",
         "state": "ACTIVE",
         "locale": "en-us",
         "displayed": true,
         "offeredInplay": true,
         "isInplay": false,
         "missingKeyMarket": true,
         "eventTime": 1609898400000,
         "numMarkets": 57,
         "lastUpdatedTime": 1609663417385,
         "outright": false,
         "neutralVenue": false,
         "eventCode": "16683597",
         "winningLegsBonus": false,
         "usDisplay": true
      }
   ],
   "id": 8370429,
   "name": "-",
   "numEvents": 15,
   "numOutrightEvents": 0
}
