{
   "event": {
      "eventId": 179647431,
      "displayGroupId": 4,
      "eventGroupId": 103,
      "eventGroupName": "NBA",
      "providerEventId": 1007123606,
      "providerId": 1,
      "name": "Cleveland Cavaliers @ Indiana Pacers",
      "shortName": "Cleveland Cavaliers @ Indiana Pacers",
      "startDate": "2020-12-31T20:10:00.0000000Z",
      "teamName1": "CLE Cavaliers",
      "teamName2": "IND Pacers",
      "eventStatus": {
         "state": "NOT_STARTED",
         "isClockDisabled": false,
         "minute": 0,
         "second": 0,
         "isClockRunning": false
      },
      "eventScorecard": {
         "scorecardComponentId": 1
      },
      "liveBettingOffered": true,
      "liveBettingEnabled": false
   },
   "eventCategories": [
      {
         "categoryId": -2,
         "name": "Popular",
         "componentizedOffers": [
            {
               "componentId": 10,
               "subcategoryName": "Game",
               "offers": [
                  [
                     {
                        "providerOfferId": 2242630885,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -262,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775584,
                              "providerId": 1,
                              "providerOfferId": 2242630885,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "-114",
                              "oddsDecimal": 1.88,
                              "oddsFractional": "22/25",
                              "line": 7.5,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846775582,
                              "providerId": 1,
                              "providerOfferId": 2242630885,
                              "label": "IND Pacers",
                              "oddsAmerican": "-107",
                              "oddsDecimal": 1.94,
                              "oddsFractional": "23/25",
                              "line": -7.5,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 0,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630912,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -262,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775635,
                              "providerId": 1,
                              "providerOfferId": 2242630912,
                              "label": "Over",
                              "oddsAmerican": "-113",
                              "oddsDecimal": 1.89,
                              "oddsFractional": "22/25",
                              "line": 220.5
                           },
                           {
                              "providerOutcomeId": 2846775640,
                              "providerId": 1,
                              "providerOfferId": 2242630912,
                              "label": "Under",
                              "oddsAmerican": "-109",
                              "oddsDecimal": 1.92,
                              "oddsFractional": "10/11",
                              "line": 220.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 0,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630837,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Moneyline",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -262,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 2,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159732,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775488,
                              "providerId": 1,
                              "providerOfferId": 2242630837,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "+235",
                              "oddsDecimal": 3.35,
                              "oddsFractional": "47/20",
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846775485,
                              "providerId": 1,
                              "providerOfferId": 2242630837,
                              "label": "IND Pacers",
                              "oddsAmerican": "-286",
                              "oddsDecimal": 1.35,
                              "oddsFractional": "7/20",
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 0,
                        "source": "1"
                     }
                  ]
               ]
            },
            {
               "componentId": 29,
               "subcategoryName": "First Field Goal",
               "offers": [
                  [
                     {
                        "providerOfferId": 2242674574,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Player to Score the First Field Goal of the Game",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2198,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 4,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1005432697,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846899390,
                              "providerId": 1,
                              "providerOfferId": 2242674574,
                              "label": "Domantas Sabonis",
                              "oddsAmerican": "+550",
                              "oddsDecimal": 6.5,
                              "oddsFractional": "11/2",
                              "participant": "Domantas Sabonis"
                           },
                           {
                              "providerOutcomeId": 2846899409,
                              "providerId": 1,
                              "providerOfferId": 2242674574,
                              "label": "Andre Drummond",
                              "oddsAmerican": "+550",
                              "oddsDecimal": 6.5,
                              "oddsFractional": "11/2",
                              "participant": "Andre Drummond"
                           },
                           {
                              "providerOutcomeId": 2846899419,
                              "providerId": 1,
                              "providerOfferId": 2242674574,
                              "label": "Collin Sexton",
                              "oddsAmerican": "+550",
                              "oddsDecimal": 6.5,
                              "oddsFractional": "11/2",
                              "participant": "Collin Sexton"
                           },
                           {
                              "providerOutcomeId": 2846899389,
                              "providerId": 1,
                              "providerOfferId": 2242674574,
                              "label": "Victor Oladipo",
                              "oddsAmerican": "+700",
                              "oddsDecimal": 8.0,
                              "oddsFractional": "7/1",
                              "participant": "Victor Oladipo"
                           },
                           {
                              "providerOutcomeId": 2846899366,
                              "providerId": 1,
                              "providerOfferId": 2242674574,
                              "label": "Malcolm Brogdon",
                              "oddsAmerican": "+800",
                              "oddsDecimal": 9.0,
                              "oddsFractional": "8/1",
                              "participant": "Malcolm Brogdon"
                           },
                           {
                              "providerOutcomeId": 2846899368,
                              "providerId": 1,
                              "providerOfferId": 2242674574,
                              "label": "Justin Holiday",
                              "oddsAmerican": "+1000",
                              "oddsDecimal": 11.0,
                              "oddsFractional": "10/1",
                              "participant": "Justin Holiday"
                           },
                           {
                              "providerOutcomeId": 2846899394,
                              "providerId": 1,
                              "providerOfferId": 2242674574,
                              "label": "Myles Turner",
                              "oddsAmerican": "+1000",
                              "oddsDecimal": 11.0,
                              "oddsFractional": "10/1",
                              "participant": "Myles Turner"
                           },
                           {
                              "providerOutcomeId": 2846899411,
                              "providerId": 1,
                              "providerOfferId": 2242674574,
                              "label": "Darius Garland",
                              "oddsAmerican": "+1000",
                              "oddsDecimal": 11.0,
                              "oddsFractional": "10/1",
                              "participant": "Darius Garland"
                           },
                           {
                              "providerOutcomeId": 2846899415,
                              "providerId": 1,
                              "providerOfferId": 2242674574,
                              "label": "Larry Nance Jr.",
                              "oddsAmerican": "+1000",
                              "oddsDecimal": 11.0,
                              "oddsFractional": "10/1",
                              "participant": "Larry Nance Jr."
                           },
                           {
                              "providerOutcomeId": 2846899417,
                              "providerId": 1,
                              "providerOfferId": 2242674574,
                              "label": "Cedi Osman",
                              "oddsAmerican": "+1200",
                              "oddsDecimal": 13.0,
                              "oddsFractional": "12/1",
                              "participant": "Cedi Osman"
                           }
                        ],
                        "extra": "Wagers on players not in starting 5 will be refunded.",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 0,
                        "source": "1"
                     }
                  ]
               ]
            },
            {
               "componentId": 8,
               "subcategoryName": "Points",
               "offers": [
                  [
                     {
                        "providerOfferId": 2242674179,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Points scored by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -820,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001223303,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846897010,
                              "providerId": 1,
                              "providerOfferId": 2242674179,
                              "label": "Over 12.5",
                              "oddsAmerican": "-121",
                              "oddsDecimal": 1.83,
                              "oddsFractional": "41/50",
                              "participant": "Cedi Osman"
                           },
                           {
                              "providerOutcomeId": 2846897011,
                              "providerId": 1,
                              "providerOfferId": 2242674179,
                              "label": "Under 12.5",
                              "oddsAmerican": "+100",
                              "oddsDecimal": 2.0,
                              "oddsFractional": "Evens",
                              "participant": "Cedi Osman"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 0,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242674177,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Points scored by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -820,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001223303,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846897006,
                              "providerId": 1,
                              "providerOfferId": 2242674177,
                              "label": "Over 14.5",
                              "oddsAmerican": "-121",
                              "oddsDecimal": 1.83,
                              "oddsFractional": "41/50",
                              "participant": "Darius Garland"
                           },
                           {
                              "providerOutcomeId": 2846897007,
                              "providerId": 1,
                              "providerOfferId": 2242674177,
                              "label": "Under 14.5",
                              "oddsAmerican": "+100",
                              "oddsDecimal": 2.0,
                              "oddsFractional": "Evens",
                              "participant": "Darius Garland"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 0,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242674170,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Points scored by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -820,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001223303,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846896991,
                              "providerId": 1,
                              "providerOfferId": 2242674170,
                              "label": "Over 19.5",
                              "oddsAmerican": "+100",
                              "oddsDecimal": 2.0,
                              "oddsFractional": "Evens",
                              "participant": "Victor Oladipo"
                           },
                           {
                              "providerOutcomeId": 2846896992,
                              "providerId": 1,
                              "providerOfferId": 2242674170,
                              "label": "Under 19.5",
                              "oddsAmerican": "-121",
                              "oddsDecimal": 1.83,
                              "oddsFractional": "41/50",
                              "participant": "Victor Oladipo"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 0,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242674173,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Points scored by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -820,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001223303,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846896998,
                              "providerId": 1,
                              "providerOfferId": 2242674173,
                              "label": "Over 13.5",
                              "oddsAmerican": "+100",
                              "oddsDecimal": 2.0,
                              "oddsFractional": "Evens",
                              "participant": "Myles Turner"
                           },
                           {
                              "providerOutcomeId": 2846896999,
                              "providerId": 1,
                              "providerOfferId": 2242674173,
                              "label": "Under 13.5",
                              "oddsAmerican": "-121",
                              "oddsDecimal": 1.83,
                              "oddsFractional": "41/50",
                              "participant": "Myles Turner"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 0,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242674171,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Points scored by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -820,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001223303,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846896993,
                              "providerId": 1,
                              "providerOfferId": 2242674171,
                              "label": "Over 19.5",
                              "oddsAmerican": "-110",
                              "oddsDecimal": 1.91,
                              "oddsFractional": "10/11",
                              "participant": "Malcolm Brogdon"
                           },
                           {
                              "providerOutcomeId": 2846896994,
                              "providerId": 1,
                              "providerOfferId": 2242674171,
                              "label": "Under 19.5",
                              "oddsAmerican": "-110",
                              "oddsDecimal": 1.91,
                              "oddsFractional": "10/11",
                              "participant": "Malcolm Brogdon"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 0,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242674174,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Points scored by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -820,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001223303,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846897000,
                              "providerId": 1,
                              "providerOfferId": 2242674174,
                              "label": "Over 9.5",
                              "oddsAmerican": "+100",
                              "oddsDecimal": 2.0,
                              "oddsFractional": "Evens",
                              "participant": "Justin Holiday"
                           },
                           {
                              "providerOutcomeId": 2846897001,
                              "providerId": 1,
                              "providerOfferId": 2242674174,
                              "label": "Under 9.5",
                              "oddsAmerican": "-121",
                              "oddsDecimal": 1.83,
                              "oddsFractional": "41/50",
                              "participant": "Justin Holiday"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 0,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242674176,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Points scored by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -820,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001223303,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846897004,
                              "providerId": 1,
                              "providerOfferId": 2242674176,
                              "label": "Over 18.5",
                              "oddsAmerican": "-121",
                              "oddsDecimal": 1.83,
                              "oddsFractional": "41/50",
                              "participant": "Andre Drummond"
                           },
                           {
                              "providerOutcomeId": 2846897005,
                              "providerId": 1,
                              "providerOfferId": 2242674176,
                              "label": "Under 18.5",
                              "oddsAmerican": "+100",
                              "oddsDecimal": 2.0,
                              "oddsFractional": "Evens",
                              "participant": "Andre Drummond"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 0,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242674175,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Points scored by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -820,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001223303,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846897002,
                              "providerId": 1,
                              "providerOfferId": 2242674175,
                              "label": "Over 23.5",
                              "oddsAmerican": "+110",
                              "oddsDecimal": 2.1,
                              "oddsFractional": "11/10",
                              "participant": "Collin Sexton"
                           },
                           {
                              "providerOutcomeId": 2846897003,
                              "providerId": 1,
                              "providerOfferId": 2242674175,
                              "label": "Under 23.5",
                              "oddsAmerican": "-134",
                              "oddsDecimal": 1.75,
                              "oddsFractional": "3/4",
                              "participant": "Collin Sexton"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 0,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242674178,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Points scored by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -820,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001223303,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846897008,
                              "providerId": 1,
                              "providerOfferId": 2242674178,
                              "label": "Over 12.5",
                              "oddsAmerican": "+100",
                              "oddsDecimal": 2.0,
                              "oddsFractional": "Evens",
                              "participant": "Larry Nance Jr."
                           },
                           {
                              "providerOutcomeId": 2846897009,
                              "providerId": 1,
                              "providerOfferId": 2242674178,
                              "label": "Under 12.5",
                              "oddsAmerican": "-121",
                              "oddsDecimal": 1.83,
                              "oddsFractional": "41/50",
                              "participant": "Larry Nance Jr."
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 0,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242674169,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Points scored by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -820,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001223303,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846896989,
                              "providerId": 1,
                              "providerOfferId": 2242674169,
                              "label": "Over 20.5",
                              "oddsAmerican": "-110",
                              "oddsDecimal": 1.91,
                              "oddsFractional": "10/11",
                              "participant": "Domantas Sabonis"
                           },
                           {
                              "providerOutcomeId": 2846896990,
                              "providerId": 1,
                              "providerOfferId": 2242674169,
                              "label": "Under 20.5",
                              "oddsAmerican": "-110",
                              "oddsDecimal": 1.91,
                              "oddsFractional": "10/11",
                              "participant": "Domantas Sabonis"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 0,
                        "source": "1"
                     }
                  ]
               ]
            },
            {
               "componentId": 8,
               "subcategoryName": "3-Pointers",
               "offers": [
                  [
                     {
                        "providerOfferId": 2242674485,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "3-point field goals made by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -817,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1002884079,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846899140,
                              "providerId": 1,
                              "providerOfferId": 2242674485,
                              "label": "Over 2.5",
                              "oddsAmerican": "+125",
                              "oddsDecimal": 2.25,
                              "oddsFractional": "5/4",
                              "participant": "Victor Oladipo"
                           },
                           {
                              "providerOutcomeId": 2846899143,
                              "providerId": 1,
                              "providerOfferId": 2242674485,
                              "label": "Under 2.5",
                              "oddsAmerican": "-152",
                              "oddsDecimal": 1.66,
                              "oddsFractional": "13/20",
                              "participant": "Victor Oladipo"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 0,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242674488,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "3-point field goals made by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -817,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1002884079,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846899146,
                              "providerId": 1,
                              "providerOfferId": 2242674488,
                              "label": "Over 1.5",
                              "oddsAmerican": "-177",
                              "oddsDecimal": 1.57,
                              "oddsFractional": "11/20",
                              "participant": "Malcolm Brogdon"
                           },
                           {
                              "providerOutcomeId": 2846899147,
                              "providerId": 1,
                              "providerOfferId": 2242674488,
                              "label": "Under 1.5",
                              "oddsAmerican": "+144",
                              "oddsDecimal": 2.44,
                              "oddsFractional": "7/5",
                              "participant": "Malcolm Brogdon"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 0,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242674491,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "3-point field goals made by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -817,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1002884079,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846899152,
                              "providerId": 1,
                              "providerOfferId": 2242674491,
                              "label": "Over 1.5",
                              "oddsAmerican": "+125",
                              "oddsDecimal": 2.25,
                              "oddsFractional": "5/4",
                              "participant": "Myles Turner"
                           },
                           {
                              "providerOutcomeId": 2846899153,
                              "providerId": 1,
                              "providerOfferId": 2242674491,
                              "label": "Under 1.5",
                              "oddsAmerican": "-152",
                              "oddsDecimal": 1.66,
                              "oddsFractional": "13/20",
                              "participant": "Myles Turner"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 0,
                        "source": "1"
                     }
                  ]
               ]
            },
            {
               "componentId": 10,
               "subcategoryName": "1st Half",
               "offers": [
                  [
                     {
                        "providerOfferId": 2242649627,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Spread - 1st Half",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -233,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159940,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875605,
                              "providerId": 1,
                              "providerOfferId": 2242649627,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "-112",
                              "oddsDecimal": 1.9,
                              "oddsFractional": "9/10",
                              "line": 4.0,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846875604,
                              "providerId": 1,
                              "providerOfferId": 2242649627,
                              "label": "IND Pacers",
                              "oddsAmerican": "-115",
                              "oddsDecimal": 1.87,
                              "oddsFractional": "17/20",
                              "line": -4.0,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 0,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242650445,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points - 1st Half",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -233,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159947,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846917008,
                              "providerId": 1,
                              "providerOfferId": 2242650445,
                              "label": "Over",
                              "oddsAmerican": "-110",
                              "oddsDecimal": 1.91,
                              "oddsFractional": "10/11",
                              "line": 112.0
                           },
                           {
                              "providerOutcomeId": 2846917009,
                              "providerId": 1,
                              "providerOfferId": 2242650445,
                              "label": "Under",
                              "oddsAmerican": "-118",
                              "oddsDecimal": 1.85,
                              "oddsFractional": "17/20",
                              "line": 112.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 0,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242649604,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Moneyline - 1st Half",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -233,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 2,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159815,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875558,
                              "providerId": 1,
                              "providerOfferId": 2242649604,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "+175",
                              "oddsDecimal": 2.75,
                              "oddsFractional": "7/4",
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846875557,
                              "providerId": 1,
                              "providerOfferId": 2242649604,
                              "label": "IND Pacers",
                              "oddsAmerican": "-230",
                              "oddsDecimal": 1.44,
                              "oddsFractional": "11/25",
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 0,
                        "source": "1"
                     }
                  ]
               ]
            },
            {
               "componentId": 10,
               "subcategoryName": "1st Quarter",
               "offers": [
                  [
                     {
                        "providerOfferId": 2242649641,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Spread - 1st Quarter",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -239,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159576,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875635,
                              "providerId": 1,
                              "providerOfferId": 2242649641,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "-115",
                              "oddsDecimal": 1.87,
                              "oddsFractional": "17/20",
                              "line": 2.5,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846875633,
                              "providerId": 1,
                              "providerOfferId": 2242649641,
                              "label": "IND Pacers",
                              "oddsAmerican": "-110",
                              "oddsDecimal": 1.91,
                              "oddsFractional": "10/11",
                              "line": -2.5,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 0,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242650404,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points - 1st Quarter",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -239,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159802,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846916909,
                              "providerId": 1,
                              "providerOfferId": 2242650404,
                              "label": "Over",
                              "oddsAmerican": "-114",
                              "oddsDecimal": 1.88,
                              "oddsFractional": "22/25",
                              "line": 56.0
                           },
                           {
                              "providerOutcomeId": 2846916910,
                              "providerId": 1,
                              "providerOfferId": 2242650404,
                              "label": "Under",
                              "oddsAmerican": "-114",
                              "oddsDecimal": 1.88,
                              "oddsFractional": "22/25",
                              "line": 56.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 0,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242649642,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Moneyline - Quarter 1",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -239,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 2,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159866,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875636,
                              "providerId": 1,
                              "providerOfferId": 2242649642,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "+150",
                              "oddsDecimal": 2.5,
                              "oddsFractional": "6/4",
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846875634,
                              "providerId": 1,
                              "providerOfferId": 2242649642,
                              "label": "IND Pacers",
                              "oddsAmerican": "-195",
                              "oddsDecimal": 1.52,
                              "oddsFractional": "13/25",
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 0,
                        "source": "1"
                     }
                  ]
               ]
            },
            {
               "componentId": 29,
               "subcategoryName": "Team Totals",
               "offers": [
                  [
                     {
                        "providerOfferId": 2242649630,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points by CLE Cavaliers - Quarter 1",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -3781,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1002167438,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875611,
                              "providerId": 1,
                              "providerOfferId": 2242649630,
                              "label": "Over",
                              "oddsAmerican": "-125",
                              "oddsDecimal": 1.8,
                              "oddsFractional": "4/5",
                              "line": 26.5
                           },
                           {
                              "providerOutcomeId": 2846875612,
                              "providerId": 1,
                              "providerOfferId": 2242649630,
                              "label": "Under",
                              "oddsAmerican": "-106",
                              "oddsDecimal": 1.95,
                              "oddsFractional": "19/20",
                              "line": 26.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 0,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242650024,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points by IND Pacers - Quarter 1",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -3781,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1002167437,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846915682,
                              "providerId": 1,
                              "providerOfferId": 2242650024,
                              "label": "Over",
                              "oddsAmerican": "-107",
                              "oddsDecimal": 1.94,
                              "oddsFractional": "23/25",
                              "line": 29.5
                           },
                           {
                              "providerOutcomeId": 2846915683,
                              "providerId": 1,
                              "providerOfferId": 2242650024,
                              "label": "Under",
                              "oddsAmerican": "-125",
                              "oddsDecimal": 1.8,
                              "oddsFractional": "4/5",
                              "line": 29.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 0,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242650017,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points by CLE Cavaliers - 1st Half",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -3781,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159579,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846915668,
                              "providerId": 1,
                              "providerOfferId": 2242650017,
                              "label": "Over",
                              "oddsAmerican": "-124",
                              "oddsDecimal": 1.81,
                              "oddsFractional": "4/5",
                              "line": 53.5
                           },
                           {
                              "providerOutcomeId": 2846915669,
                              "providerId": 1,
                              "providerOfferId": 2242650017,
                              "label": "Under",
                              "oddsAmerican": "-107",
                              "oddsDecimal": 1.94,
                              "oddsFractional": "23/25",
                              "line": 53.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 0,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242649719,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points by IND Pacers - 1st Half",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -3781,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159567,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846876008,
                              "providerId": 1,
                              "providerOfferId": 2242649719,
                              "label": "Over",
                              "oddsAmerican": "-127",
                              "oddsDecimal": 1.79,
                              "oddsFractional": "10/13",
                              "line": 57.5
                           },
                           {
                              "providerOutcomeId": 2846876009,
                              "providerId": 1,
                              "providerOfferId": 2242649719,
                              "label": "Under",
                              "oddsAmerican": "-105",
                              "oddsDecimal": 1.96,
                              "oddsFractional": "19/20",
                              "line": 57.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 0,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242650434,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points by CLE Cavaliers",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -3781,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159837,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846916986,
                              "providerId": 1,
                              "providerOfferId": 2242650434,
                              "label": "Over",
                              "oddsAmerican": "-121",
                              "oddsDecimal": 1.83,
                              "oddsFractional": "41/50",
                              "line": 106.5
                           },
                           {
                              "providerOutcomeId": 2846916987,
                              "providerId": 1,
                              "providerOfferId": 2242650434,
                              "label": "Under",
                              "oddsAmerican": "-108",
                              "oddsDecimal": 1.93,
                              "oddsFractional": "10/11",
                              "line": 106.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 0,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242650014,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points by IND Pacers",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -3781,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159855,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846915662,
                              "providerId": 1,
                              "providerOfferId": 2242650014,
                              "label": "Over",
                              "oddsAmerican": "-122",
                              "oddsDecimal": 1.82,
                              "oddsFractional": "4/5",
                              "line": 113.5
                           },
                           {
                              "providerOutcomeId": 2846915663,
                              "providerId": 1,
                              "providerOfferId": 2242650014,
                              "label": "Under",
                              "oddsAmerican": "-107",
                              "oddsDecimal": 1.94,
                              "oddsFractional": "23/25",
                              "line": 113.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 0,
                        "source": "1"
                     }
                  ]
               ]
            }
         ]
      },
      {
         "categoryId": 62,
         "name": "Game Lines",
         "componentizedOffers": [
            {
               "componentId": 10,
               "subcategoryName": "Game",
               "offers": [
                  [
                     {
                        "providerOfferId": 2242630885,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 262,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775584,
                              "providerId": 1,
                              "providerOfferId": 2242630885,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "-114",
                              "oddsDecimal": 1.88,
                              "oddsFractional": "22/25",
                              "line": 7.5,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846775582,
                              "providerId": 1,
                              "providerOfferId": 2242630885,
                              "label": "IND Pacers",
                              "oddsAmerican": "-107",
                              "oddsDecimal": 1.94,
                              "oddsFractional": "23/25",
                              "line": -7.5,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630912,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 262,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775635,
                              "providerId": 1,
                              "providerOfferId": 2242630912,
                              "label": "Over",
                              "oddsAmerican": "-113",
                              "oddsDecimal": 1.89,
                              "oddsFractional": "22/25",
                              "line": 220.5
                           },
                           {
                              "providerOutcomeId": 2846775640,
                              "providerId": 1,
                              "providerOfferId": 2242630912,
                              "label": "Under",
                              "oddsAmerican": "-109",
                              "oddsDecimal": 1.92,
                              "oddsFractional": "10/11",
                              "line": 220.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630837,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Moneyline",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 262,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 2,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159732,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775488,
                              "providerId": 1,
                              "providerOfferId": 2242630837,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "+235",
                              "oddsDecimal": 3.35,
                              "oddsFractional": "47/20",
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846775485,
                              "providerId": 1,
                              "providerOfferId": 2242630837,
                              "label": "IND Pacers",
                              "oddsAmerican": "-286",
                              "oddsDecimal": 1.35,
                              "oddsFractional": "7/20",
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     }
                  ]
               ]
            },
            {
               "componentId": 12,
               "subcategoryName": "Alternate Point Spread",
               "offers": [
                  [
                     {
                        "providerOfferId": 2242649636,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875624,
                              "providerId": 1,
                              "providerOfferId": 2242649636,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "+450",
                              "oddsDecimal": 5.5,
                              "oddsFractional": "9/2",
                              "line": -5.5,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846875623,
                              "providerId": 1,
                              "providerOfferId": 2242649636,
                              "label": "IND Pacers",
                              "oddsAmerican": "-625",
                              "oddsDecimal": 1.16,
                              "oddsFractional": "2/13",
                              "line": 5.5,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242649600,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875550,
                              "providerId": 1,
                              "providerOfferId": 2242649600,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "+440",
                              "oddsDecimal": 5.4,
                              "oddsFractional": "22/5",
                              "line": -5.0,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846875549,
                              "providerId": 1,
                              "providerOfferId": 2242649600,
                              "label": "IND Pacers",
                              "oddsAmerican": "-625",
                              "oddsDecimal": 1.16,
                              "oddsFractional": "2/13",
                              "line": 5.0,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242649634,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875620,
                              "providerId": 1,
                              "providerOfferId": 2242649634,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "+390",
                              "oddsDecimal": 4.9,
                              "oddsFractional": "19/5",
                              "line": -4.5,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846875618,
                              "providerId": 1,
                              "providerOfferId": 2242649634,
                              "label": "IND Pacers",
                              "oddsAmerican": "-530",
                              "oddsDecimal": 1.19,
                              "oddsFractional": "2/11",
                              "line": 4.5,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242649592,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875532,
                              "providerId": 1,
                              "providerOfferId": 2242649592,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "+375",
                              "oddsDecimal": 4.75,
                              "oddsFractional": "15/4",
                              "line": -4.0,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846875531,
                              "providerId": 1,
                              "providerOfferId": 2242649592,
                              "label": "IND Pacers",
                              "oddsAmerican": "-500",
                              "oddsDecimal": 1.2,
                              "oddsFractional": "1/5",
                              "line": 4.0,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630907,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775627,
                              "providerId": 1,
                              "providerOfferId": 2242630907,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "+335",
                              "oddsDecimal": 4.35,
                              "oddsFractional": "10/3",
                              "line": -3.5,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846775626,
                              "providerId": 1,
                              "providerOfferId": 2242630907,
                              "label": "IND Pacers",
                              "oddsAmerican": "-455",
                              "oddsDecimal": 1.22,
                              "oddsFractional": "11/50",
                              "line": 3.5,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630902,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775618,
                              "providerId": 1,
                              "providerOfferId": 2242630902,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "+325",
                              "oddsDecimal": 4.25,
                              "oddsFractional": "13/4",
                              "line": -3.0,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846775616,
                              "providerId": 1,
                              "providerOfferId": 2242630902,
                              "label": "IND Pacers",
                              "oddsAmerican": "-435",
                              "oddsDecimal": 1.23,
                              "oddsFractional": "2/9",
                              "line": 3.0,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630895,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775603,
                              "providerId": 1,
                              "providerOfferId": 2242630895,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "+290",
                              "oddsDecimal": 3.9,
                              "oddsFractional": "29/10",
                              "line": -2.5,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846775602,
                              "providerId": 1,
                              "providerOfferId": 2242630895,
                              "label": "IND Pacers",
                              "oddsAmerican": "-385",
                              "oddsDecimal": 1.26,
                              "oddsFractional": "1/4",
                              "line": 2.5,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630888,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775589,
                              "providerId": 1,
                              "providerOfferId": 2242630888,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "+285",
                              "oddsDecimal": 3.85,
                              "oddsFractional": "14/5",
                              "line": -2.0,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846775588,
                              "providerId": 1,
                              "providerOfferId": 2242630888,
                              "label": "IND Pacers",
                              "oddsAmerican": "-375",
                              "oddsDecimal": 1.27,
                              "oddsFractional": "27/100",
                              "line": 2.0,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630884,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775581,
                              "providerId": 1,
                              "providerOfferId": 2242630884,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "+260",
                              "oddsDecimal": 3.6,
                              "oddsFractional": "13/5",
                              "line": -1.5,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846775580,
                              "providerId": 1,
                              "providerOfferId": 2242630884,
                              "label": "IND Pacers",
                              "oddsAmerican": "-335",
                              "oddsDecimal": 1.3,
                              "oddsFractional": "3/10",
                              "line": 1.5,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630889,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775591,
                              "providerId": 1,
                              "providerOfferId": 2242630889,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "+245",
                              "oddsDecimal": 3.45,
                              "oddsFractional": "49/20",
                              "line": -1.0,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846775590,
                              "providerId": 1,
                              "providerOfferId": 2242630889,
                              "label": "IND Pacers",
                              "oddsAmerican": "-315",
                              "oddsDecimal": 1.32,
                              "oddsFractional": "8/25",
                              "line": 1.0,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630881,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775576,
                              "providerId": 1,
                              "providerOfferId": 2242630881,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "+235",
                              "oddsDecimal": 3.35,
                              "oddsFractional": "47/20",
                              "line": -0.5,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846775574,
                              "providerId": 1,
                              "providerOfferId": 2242630881,
                              "label": "IND Pacers",
                              "oddsAmerican": "-295",
                              "oddsDecimal": 1.34,
                              "oddsFractional": "1/3",
                              "line": 0.5,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630923,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775659,
                              "providerId": 1,
                              "providerOfferId": 2242630923,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "+235",
                              "oddsDecimal": 3.35,
                              "oddsFractional": "47/20",
                              "line": 0.0,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846775658,
                              "providerId": 1,
                              "providerOfferId": 2242630923,
                              "label": "IND Pacers",
                              "oddsAmerican": "-295",
                              "oddsDecimal": 1.34,
                              "oddsFractional": "1/3",
                              "line": 0.0,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630868,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775549,
                              "providerId": 1,
                              "providerOfferId": 2242630868,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "+235",
                              "oddsDecimal": 3.35,
                              "oddsFractional": "47/20",
                              "line": 0.5,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846775548,
                              "providerId": 1,
                              "providerOfferId": 2242630868,
                              "label": "IND Pacers",
                              "oddsAmerican": "-295",
                              "oddsDecimal": 1.34,
                              "oddsFractional": "1/3",
                              "line": -0.5,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630914,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775641,
                              "providerId": 1,
                              "providerOfferId": 2242630914,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "+225",
                              "oddsDecimal": 3.25,
                              "oddsFractional": "9/4",
                              "line": 1.0,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846775639,
                              "providerId": 1,
                              "providerOfferId": 2242630914,
                              "label": "IND Pacers",
                              "oddsAmerican": "-286",
                              "oddsDecimal": 1.35,
                              "oddsFractional": "7/20",
                              "line": -1.0,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630947,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775708,
                              "providerId": 1,
                              "providerOfferId": 2242630947,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "+210",
                              "oddsDecimal": 3.1,
                              "oddsFractional": "21/10",
                              "line": 1.5,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846775706,
                              "providerId": 1,
                              "providerOfferId": 2242630947,
                              "label": "IND Pacers",
                              "oddsAmerican": "-265",
                              "oddsDecimal": 1.38,
                              "oddsFractional": "4/11",
                              "line": -1.5,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630953,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775719,
                              "providerId": 1,
                              "providerOfferId": 2242630953,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "+200",
                              "oddsDecimal": 3.0,
                              "oddsFractional": "2/1",
                              "line": 2.0,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846775718,
                              "providerId": 1,
                              "providerOfferId": 2242630953,
                              "label": "IND Pacers",
                              "oddsAmerican": "-250",
                              "oddsDecimal": 1.4,
                              "oddsFractional": "2/5",
                              "line": -2.0,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630937,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775687,
                              "providerId": 1,
                              "providerOfferId": 2242630937,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "+185",
                              "oddsDecimal": 2.85,
                              "oddsFractional": "37/20",
                              "line": 2.5,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846775686,
                              "providerId": 1,
                              "providerOfferId": 2242630937,
                              "label": "IND Pacers",
                              "oddsAmerican": "-230",
                              "oddsDecimal": 1.44,
                              "oddsFractional": "11/25",
                              "line": -2.5,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630934,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775681,
                              "providerId": 1,
                              "providerOfferId": 2242630934,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "+175",
                              "oddsDecimal": 2.75,
                              "oddsFractional": "7/4",
                              "line": 3.0,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846775678,
                              "providerId": 1,
                              "providerOfferId": 2242630934,
                              "label": "IND Pacers",
                              "oddsAmerican": "-220",
                              "oddsDecimal": 1.46,
                              "oddsFractional": "4/9",
                              "line": -3.0,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630845,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775502,
                              "providerId": 1,
                              "providerOfferId": 2242630845,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "+155",
                              "oddsDecimal": 2.55,
                              "oddsFractional": "31/20",
                              "line": 3.5,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846775495,
                              "providerId": 1,
                              "providerOfferId": 2242630845,
                              "label": "IND Pacers",
                              "oddsAmerican": "-195",
                              "oddsDecimal": 1.52,
                              "oddsFractional": "13/25",
                              "line": -3.5,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630841,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775500,
                              "providerId": 1,
                              "providerOfferId": 2242630841,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "+150",
                              "oddsDecimal": 2.5,
                              "oddsFractional": "6/4",
                              "line": 4.0,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846775492,
                              "providerId": 1,
                              "providerOfferId": 2242630841,
                              "label": "IND Pacers",
                              "oddsAmerican": "-186",
                              "oddsDecimal": 1.54,
                              "oddsFractional": "8/15",
                              "line": -4.0,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630925,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775663,
                              "providerId": 1,
                              "providerOfferId": 2242630925,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "+138",
                              "oddsDecimal": 2.38,
                              "oddsFractional": "11/8",
                              "line": 4.5,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846775661,
                              "providerId": 1,
                              "providerOfferId": 2242630925,
                              "label": "IND Pacers",
                              "oddsAmerican": "-167",
                              "oddsDecimal": 1.6,
                              "oddsFractional": "3/5",
                              "line": -4.5,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630870,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775560,
                              "providerId": 1,
                              "providerOfferId": 2242630870,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "+130",
                              "oddsDecimal": 2.3,
                              "oddsFractional": "13/10",
                              "line": 5.0,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846775555,
                              "providerId": 1,
                              "providerOfferId": 2242630870,
                              "label": "IND Pacers",
                              "oddsAmerican": "-157",
                              "oddsDecimal": 1.64,
                              "oddsFractional": "16/25",
                              "line": -5.0,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630918,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775649,
                              "providerId": 1,
                              "providerOfferId": 2242630918,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "+118",
                              "oddsDecimal": 2.18,
                              "oddsFractional": "59/50",
                              "line": 5.5,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846775647,
                              "providerId": 1,
                              "providerOfferId": 2242630918,
                              "label": "IND Pacers",
                              "oddsAmerican": "-143",
                              "oddsDecimal": 1.7,
                              "oddsFractional": "7/10",
                              "line": -5.5,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630896,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775606,
                              "providerId": 1,
                              "providerOfferId": 2242630896,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "+110",
                              "oddsDecimal": 2.1,
                              "oddsFractional": "11/10",
                              "line": 6.0,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846775604,
                              "providerId": 1,
                              "providerOfferId": 2242630896,
                              "label": "IND Pacers",
                              "oddsAmerican": "-134",
                              "oddsDecimal": 1.75,
                              "oddsFractional": "3/4",
                              "line": -6.0,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630838,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775487,
                              "providerId": 1,
                              "providerOfferId": 2242630838,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "+102",
                              "oddsDecimal": 2.02,
                              "oddsFractional": "Evens",
                              "line": 6.5,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846775484,
                              "providerId": 1,
                              "providerOfferId": 2242630838,
                              "label": "IND Pacers",
                              "oddsAmerican": "-124",
                              "oddsDecimal": 1.81,
                              "oddsFractional": "4/5",
                              "line": -6.5,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630839,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775489,
                              "providerId": 1,
                              "providerOfferId": 2242630839,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "-106",
                              "oddsDecimal": 1.95,
                              "oddsFractional": "19/20",
                              "line": 7.0,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846775486,
                              "providerId": 1,
                              "providerOfferId": 2242630839,
                              "label": "IND Pacers",
                              "oddsAmerican": "-115",
                              "oddsDecimal": 1.87,
                              "oddsFractional": "17/20",
                              "line": -7.0,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630894,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775601,
                              "providerId": 1,
                              "providerOfferId": 2242630894,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "-122",
                              "oddsDecimal": 1.82,
                              "oddsFractional": "4/5",
                              "line": 8.0,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846775600,
                              "providerId": 1,
                              "providerOfferId": 2242630894,
                              "label": "IND Pacers",
                              "oddsAmerican": "+102",
                              "oddsDecimal": 2.02,
                              "oddsFractional": "Evens",
                              "line": -8.0,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630878,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775571,
                              "providerId": 1,
                              "providerOfferId": 2242630878,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "-132",
                              "oddsDecimal": 1.76,
                              "oddsFractional": "3/4",
                              "line": 8.5,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846775567,
                              "providerId": 1,
                              "providerOfferId": 2242630878,
                              "label": "IND Pacers",
                              "oddsAmerican": "+108",
                              "oddsDecimal": 2.08,
                              "oddsFractional": "27/25",
                              "line": -8.5,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630924,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775662,
                              "providerId": 1,
                              "providerOfferId": 2242630924,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "-143",
                              "oddsDecimal": 1.7,
                              "oddsFractional": "7/10",
                              "line": 9.0,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846775660,
                              "providerId": 1,
                              "providerOfferId": 2242630924,
                              "label": "IND Pacers",
                              "oddsAmerican": "+117",
                              "oddsDecimal": 2.17,
                              "oddsFractional": "23/20",
                              "line": -9.0,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630863,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775543,
                              "providerId": 1,
                              "providerOfferId": 2242630863,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "-152",
                              "oddsDecimal": 1.66,
                              "oddsFractional": "13/20",
                              "line": 9.5,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846775536,
                              "providerId": 1,
                              "providerOfferId": 2242630863,
                              "label": "IND Pacers",
                              "oddsAmerican": "+125",
                              "oddsDecimal": 2.25,
                              "oddsFractional": "5/4",
                              "line": -9.5,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630911,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775637,
                              "providerId": 1,
                              "providerOfferId": 2242630911,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "-165",
                              "oddsDecimal": 1.61,
                              "oddsFractional": "3/5",
                              "line": 10.0,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846775633,
                              "providerId": 1,
                              "providerOfferId": 2242630911,
                              "label": "IND Pacers",
                              "oddsAmerican": "+135",
                              "oddsDecimal": 2.35,
                              "oddsFractional": "27/20",
                              "line": -10.0,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630940,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775693,
                              "providerId": 1,
                              "providerOfferId": 2242630940,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "-177",
                              "oddsDecimal": 1.57,
                              "oddsFractional": "11/20",
                              "line": 10.5,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846775692,
                              "providerId": 1,
                              "providerOfferId": 2242630940,
                              "label": "IND Pacers",
                              "oddsAmerican": "+143",
                              "oddsDecimal": 2.43,
                              "oddsFractional": "7/5",
                              "line": -10.5,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630844,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775503,
                              "providerId": 1,
                              "providerOfferId": 2242630844,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "-190",
                              "oddsDecimal": 1.53,
                              "oddsFractional": "13/25",
                              "line": 11.0,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846775496,
                              "providerId": 1,
                              "providerOfferId": 2242630844,
                              "label": "IND Pacers",
                              "oddsAmerican": "+155",
                              "oddsDecimal": 2.55,
                              "oddsFractional": "31/20",
                              "line": -11.0,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630852,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775517,
                              "providerId": 1,
                              "providerOfferId": 2242630852,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "-200",
                              "oddsDecimal": 1.5,
                              "oddsFractional": "1/2",
                              "line": 11.5,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846775516,
                              "providerId": 1,
                              "providerOfferId": 2242630852,
                              "label": "IND Pacers",
                              "oddsAmerican": "+163",
                              "oddsDecimal": 2.63,
                              "oddsFractional": "13/8",
                              "line": -11.5,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630935,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775685,
                              "providerId": 1,
                              "providerOfferId": 2242630935,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "-225",
                              "oddsDecimal": 1.45,
                              "oddsFractional": "4/9",
                              "line": 12.0,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846775683,
                              "providerId": 1,
                              "providerOfferId": 2242630935,
                              "label": "IND Pacers",
                              "oddsAmerican": "+180",
                              "oddsDecimal": 2.8,
                              "oddsFractional": "9/5",
                              "line": -12.0,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630938,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775689,
                              "providerId": 1,
                              "providerOfferId": 2242630938,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "-235",
                              "oddsDecimal": 1.43,
                              "oddsFractional": "21/50",
                              "line": 12.5,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846775688,
                              "providerId": 1,
                              "providerOfferId": 2242630938,
                              "label": "IND Pacers",
                              "oddsAmerican": "+185",
                              "oddsDecimal": 2.85,
                              "oddsFractional": "37/20",
                              "line": -12.5,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630875,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775563,
                              "providerId": 1,
                              "providerOfferId": 2242630875,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "-250",
                              "oddsDecimal": 1.4,
                              "oddsFractional": "2/5",
                              "line": 13.0,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846775562,
                              "providerId": 1,
                              "providerOfferId": 2242630875,
                              "label": "IND Pacers",
                              "oddsAmerican": "+200",
                              "oddsDecimal": 3.0,
                              "oddsFractional": "2/1",
                              "line": -13.0,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630927,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775669,
                              "providerId": 1,
                              "providerOfferId": 2242630927,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "-275",
                              "oddsDecimal": 1.37,
                              "oddsFractional": "4/11",
                              "line": 13.5,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846775665,
                              "providerId": 1,
                              "providerOfferId": 2242630927,
                              "label": "IND Pacers",
                              "oddsAmerican": "+215",
                              "oddsDecimal": 3.15,
                              "oddsFractional": "43/20",
                              "line": -13.5,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630862,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775542,
                              "providerId": 1,
                              "providerOfferId": 2242630862,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "-295",
                              "oddsDecimal": 1.34,
                              "oddsFractional": "1/3",
                              "line": 14.0,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846775535,
                              "providerId": 1,
                              "providerOfferId": 2242630862,
                              "label": "IND Pacers",
                              "oddsAmerican": "+235",
                              "oddsDecimal": 3.35,
                              "oddsFractional": "47/20",
                              "line": -14.0,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630909,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775630,
                              "providerId": 1,
                              "providerOfferId": 2242630909,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "-315",
                              "oddsDecimal": 1.32,
                              "oddsFractional": "8/25",
                              "line": 14.5,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846775629,
                              "providerId": 1,
                              "providerOfferId": 2242630909,
                              "label": "IND Pacers",
                              "oddsAmerican": "+245",
                              "oddsDecimal": 3.45,
                              "oddsFractional": "49/20",
                              "line": -14.5,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630873,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775556,
                              "providerId": 1,
                              "providerOfferId": 2242630873,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "-345",
                              "oddsDecimal": 1.29,
                              "oddsFractional": "2/7",
                              "line": 15.0,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846775552,
                              "providerId": 1,
                              "providerOfferId": 2242630873,
                              "label": "IND Pacers",
                              "oddsAmerican": "+270",
                              "oddsDecimal": 3.7,
                              "oddsFractional": "27/10",
                              "line": -15.0,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630890,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775595,
                              "providerId": 1,
                              "providerOfferId": 2242630890,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "-360",
                              "oddsDecimal": 1.28,
                              "oddsFractional": "7/25",
                              "line": 15.5,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846775592,
                              "providerId": 1,
                              "providerOfferId": 2242630890,
                              "label": "IND Pacers",
                              "oddsAmerican": "+275",
                              "oddsDecimal": 3.75,
                              "oddsFractional": "11/4",
                              "line": -15.5,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630886,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775585,
                              "providerId": 1,
                              "providerOfferId": 2242630886,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "-400",
                              "oddsDecimal": 1.25,
                              "oddsFractional": "1/4",
                              "line": 16.0,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846775583,
                              "providerId": 1,
                              "providerOfferId": 2242630886,
                              "label": "IND Pacers",
                              "oddsAmerican": "+310",
                              "oddsDecimal": 4.1,
                              "oddsFractional": "3/1",
                              "line": -16.0,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630903,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775619,
                              "providerId": 1,
                              "providerOfferId": 2242630903,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "-420",
                              "oddsDecimal": 1.24,
                              "oddsFractional": "6/25",
                              "line": 16.5,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846775617,
                              "providerId": 1,
                              "providerOfferId": 2242630903,
                              "label": "IND Pacers",
                              "oddsAmerican": "+320",
                              "oddsDecimal": 4.2,
                              "oddsFractional": "16/5",
                              "line": -16.5,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242649614,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875579,
                              "providerId": 1,
                              "providerOfferId": 2242649614,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "-480",
                              "oddsDecimal": 1.21,
                              "oddsFractional": "1/5",
                              "line": 17.0,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846875576,
                              "providerId": 1,
                              "providerOfferId": 2242649614,
                              "label": "IND Pacers",
                              "oddsAmerican": "+350",
                              "oddsDecimal": 4.5,
                              "oddsFractional": "7/2",
                              "line": -17.0,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242649590,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875529,
                              "providerId": 1,
                              "providerOfferId": 2242649590,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "-500",
                              "oddsDecimal": 1.2,
                              "oddsFractional": "1/5",
                              "line": 17.5,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846875528,
                              "providerId": 1,
                              "providerOfferId": 2242649590,
                              "label": "IND Pacers",
                              "oddsAmerican": "+360",
                              "oddsDecimal": 4.6,
                              "oddsFractional": "18/5",
                              "line": -17.5,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242649623,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875597,
                              "providerId": 1,
                              "providerOfferId": 2242649623,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "-560",
                              "oddsDecimal": 1.18,
                              "oddsFractional": "1/6",
                              "line": 18.0,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846875596,
                              "providerId": 1,
                              "providerOfferId": 2242649623,
                              "label": "IND Pacers",
                              "oddsAmerican": "+390",
                              "oddsDecimal": 4.9,
                              "oddsFractional": "19/5",
                              "line": -18.0,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242649714,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Point Spread",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -2019692735,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159512,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875998,
                              "providerId": 1,
                              "providerOfferId": 2242649714,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "-590",
                              "oddsDecimal": 1.17,
                              "oddsFractional": "1/6",
                              "line": 18.5,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846875996,
                              "providerId": 1,
                              "providerOfferId": 2242649714,
                              "label": "IND Pacers",
                              "oddsAmerican": "+410",
                              "oddsDecimal": 5.1,
                              "oddsFractional": "4/1",
                              "line": -18.5,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     }
                  ]
               ]
            },
            {
               "componentId": 12,
               "subcategoryName": "Alternate Total Points",
               "offers": [
                  [
                     {
                        "providerOfferId": 2242630945,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775702,
                              "providerId": 1,
                              "providerOfferId": 2242630945,
                              "label": "Over",
                              "oddsAmerican": "-770",
                              "oddsDecimal": 1.13,
                              "oddsFractional": "1/8",
                              "line": 201.5
                           },
                           {
                              "providerOutcomeId": 2846775704,
                              "providerId": 1,
                              "providerOfferId": 2242630945,
                              "label": "Under",
                              "oddsAmerican": "+510",
                              "oddsDecimal": 6.1,
                              "oddsFractional": "5/1",
                              "line": 201.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630949,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775710,
                              "providerId": 1,
                              "providerOfferId": 2242630949,
                              "label": "Over",
                              "oddsAmerican": "-770",
                              "oddsDecimal": 1.13,
                              "oddsFractional": "1/8",
                              "line": 202.0
                           },
                           {
                              "providerOutcomeId": 2846775712,
                              "providerId": 1,
                              "providerOfferId": 2242630949,
                              "label": "Under",
                              "oddsAmerican": "+500",
                              "oddsDecimal": 6.0,
                              "oddsFractional": "5/1",
                              "line": 202.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630906,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775624,
                              "providerId": 1,
                              "providerOfferId": 2242630906,
                              "label": "Over",
                              "oddsAmerican": "-670",
                              "oddsDecimal": 1.15,
                              "oddsFractional": "1/7",
                              "line": 202.5
                           },
                           {
                              "providerOutcomeId": 2846775625,
                              "providerId": 1,
                              "providerOfferId": 2242630906,
                              "label": "Under",
                              "oddsAmerican": "+460",
                              "oddsDecimal": 5.6,
                              "oddsFractional": "23/5",
                              "line": 202.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630869,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775550,
                              "providerId": 1,
                              "providerOfferId": 2242630869,
                              "label": "Over",
                              "oddsAmerican": "-670",
                              "oddsDecimal": 1.15,
                              "oddsFractional": "1/7",
                              "line": 203.0
                           },
                           {
                              "providerOutcomeId": 2846775551,
                              "providerId": 1,
                              "providerOfferId": 2242630869,
                              "label": "Under",
                              "oddsAmerican": "+460",
                              "oddsDecimal": 5.6,
                              "oddsFractional": "23/5",
                              "line": 203.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630919,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775648,
                              "providerId": 1,
                              "providerOfferId": 2242630919,
                              "label": "Over",
                              "oddsAmerican": "-590",
                              "oddsDecimal": 1.17,
                              "oddsFractional": "1/6",
                              "line": 203.5
                           },
                           {
                              "providerOutcomeId": 2846775651,
                              "providerId": 1,
                              "providerOfferId": 2242630919,
                              "label": "Under",
                              "oddsAmerican": "+420",
                              "oddsDecimal": 5.2,
                              "oddsFractional": "21/5",
                              "line": 203.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630877,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775566,
                              "providerId": 1,
                              "providerOfferId": 2242630877,
                              "label": "Over",
                              "oddsAmerican": "-590",
                              "oddsDecimal": 1.17,
                              "oddsFractional": "1/6",
                              "line": 204.0
                           },
                           {
                              "providerOutcomeId": 2846775573,
                              "providerId": 1,
                              "providerOfferId": 2242630877,
                              "label": "Under",
                              "oddsAmerican": "+410",
                              "oddsDecimal": 5.1,
                              "oddsFractional": "4/1",
                              "line": 204.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630931,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775674,
                              "providerId": 1,
                              "providerOfferId": 2242630931,
                              "label": "Over",
                              "oddsAmerican": "-530",
                              "oddsDecimal": 1.19,
                              "oddsFractional": "2/11",
                              "line": 204.5
                           },
                           {
                              "providerOutcomeId": 2846775677,
                              "providerId": 1,
                              "providerOfferId": 2242630931,
                              "label": "Under",
                              "oddsAmerican": "+380",
                              "oddsDecimal": 4.8,
                              "oddsFractional": "19/5",
                              "line": 204.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630926,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775664,
                              "providerId": 1,
                              "providerOfferId": 2242630926,
                              "label": "Over",
                              "oddsAmerican": "-530",
                              "oddsDecimal": 1.19,
                              "oddsFractional": "2/11",
                              "line": 205.0
                           },
                           {
                              "providerOutcomeId": 2846775668,
                              "providerId": 1,
                              "providerOfferId": 2242630926,
                              "label": "Under",
                              "oddsAmerican": "+375",
                              "oddsDecimal": 4.75,
                              "oddsFractional": "15/4",
                              "line": 205.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630874,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775559,
                              "providerId": 1,
                              "providerOfferId": 2242630874,
                              "label": "Over",
                              "oddsAmerican": "-480",
                              "oddsDecimal": 1.21,
                              "oddsFractional": "1/5",
                              "line": 205.5
                           },
                           {
                              "providerOutcomeId": 2846775561,
                              "providerId": 1,
                              "providerOfferId": 2242630874,
                              "label": "Under",
                              "oddsAmerican": "+350",
                              "oddsDecimal": 4.5,
                              "oddsFractional": "7/2",
                              "line": 205.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630913,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775634,
                              "providerId": 1,
                              "providerOfferId": 2242630913,
                              "label": "Over",
                              "oddsAmerican": "-455",
                              "oddsDecimal": 1.22,
                              "oddsFractional": "11/50",
                              "line": 206.0
                           },
                           {
                              "providerOutcomeId": 2846775638,
                              "providerId": 1,
                              "providerOfferId": 2242630913,
                              "label": "Under",
                              "oddsAmerican": "+340",
                              "oddsDecimal": 4.4,
                              "oddsFractional": "17/5",
                              "line": 206.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630864,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775537,
                              "providerId": 1,
                              "providerOfferId": 2242630864,
                              "label": "Over",
                              "oddsAmerican": "-420",
                              "oddsDecimal": 1.24,
                              "oddsFractional": "6/25",
                              "line": 206.5
                           },
                           {
                              "providerOutcomeId": 2846775544,
                              "providerId": 1,
                              "providerOfferId": 2242630864,
                              "label": "Under",
                              "oddsAmerican": "+320",
                              "oddsDecimal": 4.2,
                              "oddsFractional": "16/5",
                              "line": 206.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630905,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775621,
                              "providerId": 1,
                              "providerOfferId": 2242630905,
                              "label": "Over",
                              "oddsAmerican": "-420",
                              "oddsDecimal": 1.24,
                              "oddsFractional": "6/25",
                              "line": 207.0
                           },
                           {
                              "providerOutcomeId": 2846775623,
                              "providerId": 1,
                              "providerOfferId": 2242630905,
                              "label": "Under",
                              "oddsAmerican": "+310",
                              "oddsDecimal": 4.1,
                              "oddsFractional": "3/1",
                              "line": 207.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630899,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775610,
                              "providerId": 1,
                              "providerOfferId": 2242630899,
                              "label": "Over",
                              "oddsAmerican": "-385",
                              "oddsDecimal": 1.26,
                              "oddsFractional": "1/4",
                              "line": 207.5
                           },
                           {
                              "providerOutcomeId": 2846775612,
                              "providerId": 1,
                              "providerOfferId": 2242630899,
                              "label": "Under",
                              "oddsAmerican": "+290",
                              "oddsDecimal": 3.9,
                              "oddsFractional": "29/10",
                              "line": 207.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630897,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775605,
                              "providerId": 1,
                              "providerOfferId": 2242630897,
                              "label": "Over",
                              "oddsAmerican": "-375",
                              "oddsDecimal": 1.27,
                              "oddsFractional": "27/100",
                              "line": 208.0
                           },
                           {
                              "providerOutcomeId": 2846775608,
                              "providerId": 1,
                              "providerOfferId": 2242630897,
                              "label": "Under",
                              "oddsAmerican": "+285",
                              "oddsDecimal": 3.85,
                              "oddsFractional": "14/5",
                              "line": 208.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630892,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775594,
                              "providerId": 1,
                              "providerOfferId": 2242630892,
                              "label": "Over",
                              "oddsAmerican": "-345",
                              "oddsDecimal": 1.29,
                              "oddsFractional": "2/7",
                              "line": 208.5
                           },
                           {
                              "providerOutcomeId": 2846775598,
                              "providerId": 1,
                              "providerOfferId": 2242630892,
                              "label": "Under",
                              "oddsAmerican": "+265",
                              "oddsDecimal": 3.65,
                              "oddsFractional": "13/5",
                              "line": 208.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630882,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775575,
                              "providerId": 1,
                              "providerOfferId": 2242630882,
                              "label": "Over",
                              "oddsAmerican": "-335",
                              "oddsDecimal": 1.3,
                              "oddsFractional": "3/10",
                              "line": 209.0
                           },
                           {
                              "providerOutcomeId": 2846775577,
                              "providerId": 1,
                              "providerOfferId": 2242630882,
                              "label": "Under",
                              "oddsAmerican": "+260",
                              "oddsDecimal": 3.6,
                              "oddsFractional": "13/5",
                              "line": 209.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630848,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775507,
                              "providerId": 1,
                              "providerOfferId": 2242630848,
                              "label": "Over",
                              "oddsAmerican": "-315",
                              "oddsDecimal": 1.32,
                              "oddsFractional": "8/25",
                              "line": 209.5
                           },
                           {
                              "providerOutcomeId": 2846775513,
                              "providerId": 1,
                              "providerOfferId": 2242630848,
                              "label": "Under",
                              "oddsAmerican": "+245",
                              "oddsDecimal": 3.45,
                              "oddsFractional": "49/20",
                              "line": 209.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630846,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775499,
                              "providerId": 1,
                              "providerOfferId": 2242630846,
                              "label": "Over",
                              "oddsAmerican": "-305",
                              "oddsDecimal": 1.33,
                              "oddsFractional": "33/100",
                              "line": 210.0
                           },
                           {
                              "providerOutcomeId": 2846775504,
                              "providerId": 1,
                              "providerOfferId": 2242630846,
                              "label": "Under",
                              "oddsAmerican": "+240",
                              "oddsDecimal": 3.4,
                              "oddsFractional": "12/5",
                              "line": 210.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630916,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775644,
                              "providerId": 1,
                              "providerOfferId": 2242630916,
                              "label": "Over",
                              "oddsAmerican": "-278",
                              "oddsDecimal": 1.36,
                              "oddsFractional": "7/20",
                              "line": 210.5
                           },
                           {
                              "providerOutcomeId": 2846775645,
                              "providerId": 1,
                              "providerOfferId": 2242630916,
                              "label": "Under",
                              "oddsAmerican": "+220",
                              "oddsDecimal": 3.2,
                              "oddsFractional": "11/5",
                              "line": 210.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630871,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775554,
                              "providerId": 1,
                              "providerOfferId": 2242630871,
                              "label": "Over",
                              "oddsAmerican": "-275",
                              "oddsDecimal": 1.37,
                              "oddsFractional": "4/11",
                              "line": 211.0
                           },
                           {
                              "providerOutcomeId": 2846775558,
                              "providerId": 1,
                              "providerOfferId": 2242630871,
                              "label": "Under",
                              "oddsAmerican": "+215",
                              "oddsDecimal": 3.15,
                              "oddsFractional": "43/20",
                              "line": 211.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630951,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775714,
                              "providerId": 1,
                              "providerOfferId": 2242630951,
                              "label": "Over",
                              "oddsAmerican": "-250",
                              "oddsDecimal": 1.4,
                              "oddsFractional": "2/5",
                              "line": 211.5
                           },
                           {
                              "providerOutcomeId": 2846775715,
                              "providerId": 1,
                              "providerOfferId": 2242630951,
                              "label": "Under",
                              "oddsAmerican": "+200",
                              "oddsDecimal": 3.0,
                              "oddsFractional": "2/1",
                              "line": 211.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630952,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775716,
                              "providerId": 1,
                              "providerOfferId": 2242630952,
                              "label": "Over",
                              "oddsAmerican": "-250",
                              "oddsDecimal": 1.4,
                              "oddsFractional": "2/5",
                              "line": 212.0
                           },
                           {
                              "providerOutcomeId": 2846775717,
                              "providerId": 1,
                              "providerOfferId": 2242630952,
                              "label": "Under",
                              "oddsAmerican": "+200",
                              "oddsDecimal": 3.0,
                              "oddsFractional": "2/1",
                              "line": 212.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630944,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775700,
                              "providerId": 1,
                              "providerOfferId": 2242630944,
                              "label": "Over",
                              "oddsAmerican": "-235",
                              "oddsDecimal": 1.43,
                              "oddsFractional": "21/50",
                              "line": 212.5
                           },
                           {
                              "providerOutcomeId": 2846775701,
                              "providerId": 1,
                              "providerOfferId": 2242630944,
                              "label": "Under",
                              "oddsAmerican": "+188",
                              "oddsDecimal": 2.88,
                              "oddsFractional": "15/8",
                              "line": 212.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630939,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775690,
                              "providerId": 1,
                              "providerOfferId": 2242630939,
                              "label": "Over",
                              "oddsAmerican": "-230",
                              "oddsDecimal": 1.44,
                              "oddsFractional": "11/25",
                              "line": 213.0
                           },
                           {
                              "providerOutcomeId": 2846775691,
                              "providerId": 1,
                              "providerOfferId": 2242630939,
                              "label": "Under",
                              "oddsAmerican": "+180",
                              "oddsDecimal": 2.8,
                              "oddsFractional": "9/5",
                              "line": 213.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630936,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775682,
                              "providerId": 1,
                              "providerOfferId": 2242630936,
                              "label": "Over",
                              "oddsAmerican": "-215",
                              "oddsDecimal": 1.47,
                              "oddsFractional": "4/9",
                              "line": 213.5
                           },
                           {
                              "providerOutcomeId": 2846775684,
                              "providerId": 1,
                              "providerOfferId": 2242630936,
                              "label": "Under",
                              "oddsAmerican": "+170",
                              "oddsDecimal": 2.7,
                              "oddsFractional": "17/10",
                              "line": 213.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630928,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775667,
                              "providerId": 1,
                              "providerOfferId": 2242630928,
                              "label": "Over",
                              "oddsAmerican": "-205",
                              "oddsDecimal": 1.49,
                              "oddsFractional": "12/25",
                              "line": 214.0
                           },
                           {
                              "providerOutcomeId": 2846775670,
                              "providerId": 1,
                              "providerOfferId": 2242630928,
                              "label": "Under",
                              "oddsAmerican": "+165",
                              "oddsDecimal": 2.65,
                              "oddsFractional": "33/20",
                              "line": 214.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630872,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775553,
                              "providerId": 1,
                              "providerOfferId": 2242630872,
                              "label": "Over",
                              "oddsAmerican": "-195",
                              "oddsDecimal": 1.52,
                              "oddsFractional": "13/25",
                              "line": 214.5
                           },
                           {
                              "providerOutcomeId": 2846775557,
                              "providerId": 1,
                              "providerOfferId": 2242630872,
                              "label": "Under",
                              "oddsAmerican": "+155",
                              "oddsDecimal": 2.55,
                              "oddsFractional": "31/20",
                              "line": 214.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630910,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775632,
                              "providerId": 1,
                              "providerOfferId": 2242630910,
                              "label": "Over",
                              "oddsAmerican": "-186",
                              "oddsDecimal": 1.54,
                              "oddsFractional": "8/15",
                              "line": 215.0
                           },
                           {
                              "providerOutcomeId": 2846775636,
                              "providerId": 1,
                              "providerOfferId": 2242630910,
                              "label": "Under",
                              "oddsAmerican": "+150",
                              "oddsDecimal": 2.5,
                              "oddsFractional": "6/4",
                              "line": 215.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630866,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775540,
                              "providerId": 1,
                              "providerOfferId": 2242630866,
                              "label": "Over",
                              "oddsAmerican": "-177",
                              "oddsDecimal": 1.57,
                              "oddsFractional": "11/20",
                              "line": 215.5
                           },
                           {
                              "providerOutcomeId": 2846775547,
                              "providerId": 1,
                              "providerOfferId": 2242630866,
                              "label": "Under",
                              "oddsAmerican": "+143",
                              "oddsDecimal": 2.43,
                              "oddsFractional": "7/5",
                              "line": 215.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630842,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775493,
                              "providerId": 1,
                              "providerOfferId": 2242630842,
                              "label": "Over",
                              "oddsAmerican": "-167",
                              "oddsDecimal": 1.6,
                              "oddsFractional": "3/5",
                              "line": 216.0
                           },
                           {
                              "providerOutcomeId": 2846775497,
                              "providerId": 1,
                              "providerOfferId": 2242630842,
                              "label": "Under",
                              "oddsAmerican": "+138",
                              "oddsDecimal": 2.38,
                              "oddsFractional": "11/8",
                              "line": 216.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630850,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775510,
                              "providerId": 1,
                              "providerOfferId": 2242630850,
                              "label": "Over",
                              "oddsAmerican": "-159",
                              "oddsDecimal": 1.63,
                              "oddsFractional": "8/13",
                              "line": 216.5
                           },
                           {
                              "providerOutcomeId": 2846775514,
                              "providerId": 1,
                              "providerOfferId": 2242630850,
                              "label": "Under",
                              "oddsAmerican": "+132",
                              "oddsDecimal": 2.32,
                              "oddsFractional": "13/10",
                              "line": 216.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630898,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775607,
                              "providerId": 1,
                              "providerOfferId": 2242630898,
                              "label": "Over",
                              "oddsAmerican": "-155",
                              "oddsDecimal": 1.65,
                              "oddsFractional": "13/20",
                              "line": 217.0
                           },
                           {
                              "providerOutcomeId": 2846775609,
                              "providerId": 1,
                              "providerOfferId": 2242630898,
                              "label": "Under",
                              "oddsAmerican": "+125",
                              "oddsDecimal": 2.25,
                              "oddsFractional": "5/4",
                              "line": 217.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630893,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775597,
                              "providerId": 1,
                              "providerOfferId": 2242630893,
                              "label": "Over",
                              "oddsAmerican": "-148",
                              "oddsDecimal": 1.68,
                              "oddsFractional": "4/6",
                              "line": 217.5
                           },
                           {
                              "providerOutcomeId": 2846775599,
                              "providerId": 1,
                              "providerOfferId": 2242630893,
                              "label": "Under",
                              "oddsAmerican": "+120",
                              "oddsDecimal": 2.2,
                              "oddsFractional": "6/5",
                              "line": 217.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630887,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775586,
                              "providerId": 1,
                              "providerOfferId": 2242630887,
                              "label": "Over",
                              "oddsAmerican": "-141",
                              "oddsDecimal": 1.71,
                              "oddsFractional": "7/10",
                              "line": 218.0
                           },
                           {
                              "providerOutcomeId": 2846775587,
                              "providerId": 1,
                              "providerOfferId": 2242630887,
                              "label": "Under",
                              "oddsAmerican": "+116",
                              "oddsDecimal": 2.16,
                              "oddsFractional": "23/20",
                              "line": 218.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630922,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775654,
                              "providerId": 1,
                              "providerOfferId": 2242630922,
                              "label": "Over",
                              "oddsAmerican": "-134",
                              "oddsDecimal": 1.75,
                              "oddsFractional": "3/4",
                              "line": 218.5
                           },
                           {
                              "providerOutcomeId": 2846775656,
                              "providerId": 1,
                              "providerOfferId": 2242630922,
                              "label": "Under",
                              "oddsAmerican": "+110",
                              "oddsDecimal": 2.1,
                              "oddsFractional": "11/10",
                              "line": 218.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630879,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775568,
                              "providerId": 1,
                              "providerOfferId": 2242630879,
                              "label": "Over",
                              "oddsAmerican": "-129",
                              "oddsDecimal": 1.78,
                              "oddsFractional": "10/13",
                              "line": 219.0
                           },
                           {
                              "providerOutcomeId": 2846775570,
                              "providerId": 1,
                              "providerOfferId": 2242630879,
                              "label": "Under",
                              "oddsAmerican": "+106",
                              "oddsDecimal": 2.06,
                              "oddsFractional": "21/20",
                              "line": 219.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630908,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775628,
                              "providerId": 1,
                              "providerOfferId": 2242630908,
                              "label": "Over",
                              "oddsAmerican": "-122",
                              "oddsDecimal": 1.82,
                              "oddsFractional": "4/5",
                              "line": 219.5
                           },
                           {
                              "providerOutcomeId": 2846775631,
                              "providerId": 1,
                              "providerOfferId": 2242630908,
                              "label": "Under",
                              "oddsAmerican": "+100",
                              "oddsDecimal": 2.0,
                              "oddsFractional": "Evens",
                              "line": 219.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630861,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775534,
                              "providerId": 1,
                              "providerOfferId": 2242630861,
                              "label": "Over",
                              "oddsAmerican": "-118",
                              "oddsDecimal": 1.85,
                              "oddsFractional": "17/20",
                              "line": 220.0
                           },
                           {
                              "providerOutcomeId": 2846775538,
                              "providerId": 1,
                              "providerOfferId": 2242630861,
                              "label": "Under",
                              "oddsAmerican": "-104",
                              "oddsDecimal": 1.97,
                              "oddsFractional": "19/20",
                              "line": 220.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630941,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775694,
                              "providerId": 1,
                              "providerOfferId": 2242630941,
                              "label": "Over",
                              "oddsAmerican": "-108",
                              "oddsDecimal": 1.93,
                              "oddsFractional": "10/11",
                              "line": 221.0
                           },
                           {
                              "providerOutcomeId": 2846775695,
                              "providerId": 1,
                              "providerOfferId": 2242630941,
                              "label": "Under",
                              "oddsAmerican": "-114",
                              "oddsDecimal": 1.88,
                              "oddsFractional": "22/25",
                              "line": 221.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630948,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775707,
                              "providerId": 1,
                              "providerOfferId": 2242630948,
                              "label": "Over",
                              "oddsAmerican": "-103",
                              "oddsDecimal": 1.98,
                              "oddsFractional": "49/50",
                              "line": 221.5
                           },
                           {
                              "providerOutcomeId": 2846775709,
                              "providerId": 1,
                              "providerOfferId": 2242630948,
                              "label": "Under",
                              "oddsAmerican": "-118",
                              "oddsDecimal": 1.85,
                              "oddsFractional": "17/20",
                              "line": 221.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630843,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775494,
                              "providerId": 1,
                              "providerOfferId": 2242630843,
                              "label": "Over",
                              "oddsAmerican": "+102",
                              "oddsDecimal": 2.02,
                              "oddsFractional": "Evens",
                              "line": 222.0
                           },
                           {
                              "providerOutcomeId": 2846775501,
                              "providerId": 1,
                              "providerOfferId": 2242630843,
                              "label": "Under",
                              "oddsAmerican": "-124",
                              "oddsDecimal": 1.81,
                              "oddsFractional": "4/5",
                              "line": 222.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630851,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775509,
                              "providerId": 1,
                              "providerOfferId": 2242630851,
                              "label": "Over",
                              "oddsAmerican": "+106",
                              "oddsDecimal": 2.06,
                              "oddsFractional": "21/20",
                              "line": 222.5
                           },
                           {
                              "providerOutcomeId": 2846775512,
                              "providerId": 1,
                              "providerOfferId": 2242630851,
                              "label": "Under",
                              "oddsAmerican": "-130",
                              "oddsDecimal": 1.77,
                              "oddsFractional": "3/4",
                              "line": 222.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630920,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775652,
                              "providerId": 1,
                              "providerOfferId": 2242630920,
                              "label": "Over",
                              "oddsAmerican": "+112",
                              "oddsDecimal": 2.12,
                              "oddsFractional": "11/10",
                              "line": 223.0
                           },
                           {
                              "providerOutcomeId": 2846775653,
                              "providerId": 1,
                              "providerOfferId": 2242630920,
                              "label": "Under",
                              "oddsAmerican": "-136",
                              "oddsDecimal": 1.74,
                              "oddsFractional": "8/11",
                              "line": 223.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630880,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775569,
                              "providerId": 1,
                              "providerOfferId": 2242630880,
                              "label": "Over",
                              "oddsAmerican": "+116",
                              "oddsDecimal": 2.16,
                              "oddsFractional": "23/20",
                              "line": 223.5
                           },
                           {
                              "providerOutcomeId": 2846775572,
                              "providerId": 1,
                              "providerOfferId": 2242630880,
                              "label": "Under",
                              "oddsAmerican": "-141",
                              "oddsDecimal": 1.71,
                              "oddsFractional": "7/10",
                              "line": 223.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630929,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775666,
                              "providerId": 1,
                              "providerOfferId": 2242630929,
                              "label": "Over",
                              "oddsAmerican": "+123",
                              "oddsDecimal": 2.23,
                              "oddsFractional": "6/5",
                              "line": 224.0
                           },
                           {
                              "providerOutcomeId": 2846775671,
                              "providerId": 1,
                              "providerOfferId": 2242630929,
                              "label": "Under",
                              "oddsAmerican": "-150",
                              "oddsDecimal": 1.67,
                              "oddsFractional": "4/6",
                              "line": 224.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630867,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775541,
                              "providerId": 1,
                              "providerOfferId": 2242630867,
                              "label": "Over",
                              "oddsAmerican": "+128",
                              "oddsDecimal": 2.28,
                              "oddsFractional": "5/4",
                              "line": 224.5
                           },
                           {
                              "providerOutcomeId": 2846775546,
                              "providerId": 1,
                              "providerOfferId": 2242630867,
                              "label": "Under",
                              "oddsAmerican": "-155",
                              "oddsDecimal": 1.65,
                              "oddsFractional": "13/20",
                              "line": 224.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630917,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775646,
                              "providerId": 1,
                              "providerOfferId": 2242630917,
                              "label": "Over",
                              "oddsAmerican": "+133",
                              "oddsDecimal": 2.33,
                              "oddsFractional": "13/10",
                              "line": 225.0
                           },
                           {
                              "providerOutcomeId": 2846775650,
                              "providerId": 1,
                              "providerOfferId": 2242630917,
                              "label": "Under",
                              "oddsAmerican": "-162",
                              "oddsDecimal": 1.62,
                              "oddsFractional": "8/13",
                              "line": 225.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630900,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775611,
                              "providerId": 1,
                              "providerOfferId": 2242630900,
                              "label": "Over",
                              "oddsAmerican": "+138",
                              "oddsDecimal": 2.38,
                              "oddsFractional": "11/8",
                              "line": 225.5
                           },
                           {
                              "providerOutcomeId": 2846775613,
                              "providerId": 1,
                              "providerOfferId": 2242630900,
                              "label": "Under",
                              "oddsAmerican": "-167",
                              "oddsDecimal": 1.6,
                              "oddsFractional": "3/5",
                              "line": 225.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630901,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775614,
                              "providerId": 1,
                              "providerOfferId": 2242630901,
                              "label": "Over",
                              "oddsAmerican": "+145",
                              "oddsDecimal": 2.45,
                              "oddsFractional": "29/20",
                              "line": 226.0
                           },
                           {
                              "providerOutcomeId": 2846775615,
                              "providerId": 1,
                              "providerOfferId": 2242630901,
                              "label": "Under",
                              "oddsAmerican": "-180",
                              "oddsDecimal": 1.56,
                              "oddsFractional": "11/20",
                              "line": 226.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630883,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775578,
                              "providerId": 1,
                              "providerOfferId": 2242630883,
                              "label": "Over",
                              "oddsAmerican": "+150",
                              "oddsDecimal": 2.5,
                              "oddsFractional": "6/4",
                              "line": 226.5
                           },
                           {
                              "providerOutcomeId": 2846775579,
                              "providerId": 1,
                              "providerOfferId": 2242630883,
                              "label": "Under",
                              "oddsAmerican": "-186",
                              "oddsDecimal": 1.54,
                              "oddsFractional": "8/15",
                              "line": 226.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630891,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775593,
                              "providerId": 1,
                              "providerOfferId": 2242630891,
                              "label": "Over",
                              "oddsAmerican": "+155",
                              "oddsDecimal": 2.55,
                              "oddsFractional": "31/20",
                              "line": 227.0
                           },
                           {
                              "providerOutcomeId": 2846775596,
                              "providerId": 1,
                              "providerOfferId": 2242630891,
                              "label": "Under",
                              "oddsAmerican": "-195",
                              "oddsDecimal": 1.52,
                              "oddsFractional": "13/25",
                              "line": 227.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630921,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775655,
                              "providerId": 1,
                              "providerOfferId": 2242630921,
                              "label": "Over",
                              "oddsAmerican": "+163",
                              "oddsDecimal": 2.63,
                              "oddsFractional": "13/8",
                              "line": 227.5
                           },
                           {
                              "providerOutcomeId": 2846775657,
                              "providerId": 1,
                              "providerOfferId": 2242630921,
                              "label": "Under",
                              "oddsAmerican": "-200",
                              "oddsDecimal": 1.5,
                              "oddsFractional": "1/2",
                              "line": 227.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630876,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775564,
                              "providerId": 1,
                              "providerOfferId": 2242630876,
                              "label": "Over",
                              "oddsAmerican": "+175",
                              "oddsDecimal": 2.75,
                              "oddsFractional": "7/4",
                              "line": 228.0
                           },
                           {
                              "providerOutcomeId": 2846775565,
                              "providerId": 1,
                              "providerOfferId": 2242630876,
                              "label": "Under",
                              "oddsAmerican": "-215",
                              "oddsDecimal": 1.47,
                              "oddsFractional": "4/9",
                              "line": 228.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630932,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775675,
                              "providerId": 1,
                              "providerOfferId": 2242630932,
                              "label": "Over",
                              "oddsAmerican": "+180",
                              "oddsDecimal": 2.8,
                              "oddsFractional": "9/5",
                              "line": 228.5
                           },
                           {
                              "providerOutcomeId": 2846775679,
                              "providerId": 1,
                              "providerOfferId": 2242630932,
                              "label": "Under",
                              "oddsAmerican": "-225",
                              "oddsDecimal": 1.45,
                              "oddsFractional": "4/9",
                              "line": 228.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630865,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775539,
                              "providerId": 1,
                              "providerOfferId": 2242630865,
                              "label": "Over",
                              "oddsAmerican": "+188",
                              "oddsDecimal": 2.88,
                              "oddsFractional": "15/8",
                              "line": 229.0
                           },
                           {
                              "providerOutcomeId": 2846775545,
                              "providerId": 1,
                              "providerOfferId": 2242630865,
                              "label": "Under",
                              "oddsAmerican": "-235",
                              "oddsDecimal": 1.43,
                              "oddsFractional": "21/50",
                              "line": 229.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630915,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775642,
                              "providerId": 1,
                              "providerOfferId": 2242630915,
                              "label": "Over",
                              "oddsAmerican": "+195",
                              "oddsDecimal": 2.95,
                              "oddsFractional": "39/20",
                              "line": 229.5
                           },
                           {
                              "providerOutcomeId": 2846775643,
                              "providerId": 1,
                              "providerOfferId": 2242630915,
                              "label": "Under",
                              "oddsAmerican": "-245",
                              "oddsDecimal": 1.41,
                              "oddsFractional": "2/5",
                              "line": 229.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630942,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775696,
                              "providerId": 1,
                              "providerOfferId": 2242630942,
                              "label": "Over",
                              "oddsAmerican": "+205",
                              "oddsDecimal": 3.05,
                              "oddsFractional": "41/20",
                              "line": 230.0
                           },
                           {
                              "providerOutcomeId": 2846775697,
                              "providerId": 1,
                              "providerOfferId": 2242630942,
                              "label": "Under",
                              "oddsAmerican": "-265",
                              "oddsDecimal": 1.38,
                              "oddsFractional": "4/11",
                              "line": 230.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630950,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775711,
                              "providerId": 1,
                              "providerOfferId": 2242630950,
                              "label": "Over",
                              "oddsAmerican": "+210",
                              "oddsDecimal": 3.1,
                              "oddsFractional": "21/10",
                              "line": 230.5
                           },
                           {
                              "providerOutcomeId": 2846775713,
                              "providerId": 1,
                              "providerOfferId": 2242630950,
                              "label": "Under",
                              "oddsAmerican": "-265",
                              "oddsDecimal": 1.38,
                              "oddsFractional": "4/11",
                              "line": 230.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242630933,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775676,
                              "providerId": 1,
                              "providerOfferId": 2242630933,
                              "label": "Over",
                              "oddsAmerican": "+225",
                              "oddsDecimal": 3.25,
                              "oddsFractional": "9/4",
                              "line": 231.0
                           },
                           {
                              "providerOutcomeId": 2846775680,
                              "providerId": 1,
                              "providerOfferId": 2242630933,
                              "label": "Under",
                              "oddsAmerican": "-286",
                              "oddsDecimal": 1.35,
                              "oddsFractional": "7/20",
                              "line": 231.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242648098,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846804711,
                              "providerId": 1,
                              "providerOfferId": 2242648098,
                              "label": "Over",
                              "oddsAmerican": "+230",
                              "oddsDecimal": 3.3,
                              "oddsFractional": "23/10",
                              "line": 231.5
                           },
                           {
                              "providerOutcomeId": 2846804712,
                              "providerId": 1,
                              "providerOfferId": 2242648098,
                              "label": "Under",
                              "oddsAmerican": "-295",
                              "oddsDecimal": 1.34,
                              "oddsFractional": "1/3",
                              "line": 231.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242648131,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846804779,
                              "providerId": 1,
                              "providerOfferId": 2242648131,
                              "label": "Over",
                              "oddsAmerican": "+245",
                              "oddsDecimal": 3.45,
                              "oddsFractional": "49/20",
                              "line": 232.0
                           },
                           {
                              "providerOutcomeId": 2846804780,
                              "providerId": 1,
                              "providerOfferId": 2242648131,
                              "label": "Under",
                              "oddsAmerican": "-315",
                              "oddsDecimal": 1.32,
                              "oddsFractional": "8/25",
                              "line": 232.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242648129,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846804775,
                              "providerId": 1,
                              "providerOfferId": 2242648129,
                              "label": "Over",
                              "oddsAmerican": "+245",
                              "oddsDecimal": 3.45,
                              "oddsFractional": "49/20",
                              "line": 232.5
                           },
                           {
                              "providerOutcomeId": 2846804776,
                              "providerId": 1,
                              "providerOfferId": 2242648129,
                              "label": "Under",
                              "oddsAmerican": "-315",
                              "oddsDecimal": 1.32,
                              "oddsFractional": "8/25",
                              "line": 232.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242649620,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875590,
                              "providerId": 1,
                              "providerOfferId": 2242649620,
                              "label": "Over",
                              "oddsAmerican": "+265",
                              "oddsDecimal": 3.65,
                              "oddsFractional": "13/5",
                              "line": 233.0
                           },
                           {
                              "providerOutcomeId": 2846875592,
                              "providerId": 1,
                              "providerOfferId": 2242649620,
                              "label": "Under",
                              "oddsAmerican": "-345",
                              "oddsDecimal": 1.29,
                              "oddsFractional": "2/7",
                              "line": 233.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242649596,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875539,
                              "providerId": 1,
                              "providerOfferId": 2242649596,
                              "label": "Over",
                              "oddsAmerican": "+275",
                              "oddsDecimal": 3.75,
                              "oddsFractional": "11/4",
                              "line": 233.5
                           },
                           {
                              "providerOutcomeId": 2846875541,
                              "providerId": 1,
                              "providerOfferId": 2242649596,
                              "label": "Under",
                              "oddsAmerican": "-360",
                              "oddsDecimal": 1.28,
                              "oddsFractional": "7/25",
                              "line": 233.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242649635,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875621,
                              "providerId": 1,
                              "providerOfferId": 2242649635,
                              "label": "Over",
                              "oddsAmerican": "+290",
                              "oddsDecimal": 3.9,
                              "oddsFractional": "29/10",
                              "line": 234.0
                           },
                           {
                              "providerOutcomeId": 2846875622,
                              "providerId": 1,
                              "providerOfferId": 2242649635,
                              "label": "Under",
                              "oddsAmerican": "-385",
                              "oddsDecimal": 1.26,
                              "oddsFractional": "1/4",
                              "line": 234.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242649609,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875568,
                              "providerId": 1,
                              "providerOfferId": 2242649609,
                              "label": "Over",
                              "oddsAmerican": "+295",
                              "oddsDecimal": 3.95,
                              "oddsFractional": "29/10",
                              "line": 234.5
                           },
                           {
                              "providerOutcomeId": 2846875570,
                              "providerId": 1,
                              "providerOfferId": 2242649609,
                              "label": "Under",
                              "oddsAmerican": "-385",
                              "oddsDecimal": 1.26,
                              "oddsFractional": "1/4",
                              "line": 234.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242650021,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846915676,
                              "providerId": 1,
                              "providerOfferId": 2242650021,
                              "label": "Over",
                              "oddsAmerican": "+320",
                              "oddsDecimal": 4.2,
                              "oddsFractional": "16/5",
                              "line": 235.0
                           },
                           {
                              "providerOutcomeId": 2846915677,
                              "providerId": 1,
                              "providerOfferId": 2242650021,
                              "label": "Under",
                              "oddsAmerican": "-420",
                              "oddsDecimal": 1.24,
                              "oddsFractional": "6/25",
                              "line": 235.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242650016,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846915666,
                              "providerId": 1,
                              "providerOfferId": 2242650016,
                              "label": "Over",
                              "oddsAmerican": "+325",
                              "oddsDecimal": 4.25,
                              "oddsFractional": "13/4",
                              "line": 235.5
                           },
                           {
                              "providerOutcomeId": 2846915667,
                              "providerId": 1,
                              "providerOfferId": 2242650016,
                              "label": "Under",
                              "oddsAmerican": "-435",
                              "oddsDecimal": 1.23,
                              "oddsFractional": "2/9",
                              "line": 235.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242650015,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846915664,
                              "providerId": 1,
                              "providerOfferId": 2242650015,
                              "label": "Over",
                              "oddsAmerican": "+340",
                              "oddsDecimal": 4.4,
                              "oddsFractional": "17/5",
                              "line": 236.0
                           },
                           {
                              "providerOutcomeId": 2846915665,
                              "providerId": 1,
                              "providerOfferId": 2242650015,
                              "label": "Under",
                              "oddsAmerican": "-455",
                              "oddsDecimal": 1.22,
                              "oddsFractional": "11/50",
                              "line": 236.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242650022,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846915678,
                              "providerId": 1,
                              "providerOfferId": 2242650022,
                              "label": "Over",
                              "oddsAmerican": "+350",
                              "oddsDecimal": 4.5,
                              "oddsFractional": "7/2",
                              "line": 236.5
                           },
                           {
                              "providerOutcomeId": 2846915679,
                              "providerId": 1,
                              "providerOfferId": 2242650022,
                              "label": "Under",
                              "oddsAmerican": "-480",
                              "oddsDecimal": 1.21,
                              "oddsFractional": "1/5",
                              "line": 236.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242650436,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846916990,
                              "providerId": 1,
                              "providerOfferId": 2242650436,
                              "label": "Over",
                              "oddsAmerican": "+375",
                              "oddsDecimal": 4.75,
                              "oddsFractional": "15/4",
                              "line": 237.0
                           },
                           {
                              "providerOutcomeId": 2846916991,
                              "providerId": 1,
                              "providerOfferId": 2242650436,
                              "label": "Under",
                              "oddsAmerican": "-530",
                              "oddsDecimal": 1.19,
                              "oddsFractional": "2/11",
                              "line": 237.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242650435,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846916988,
                              "providerId": 1,
                              "providerOfferId": 2242650435,
                              "label": "Over",
                              "oddsAmerican": "+380",
                              "oddsDecimal": 4.8,
                              "oddsFractional": "19/5",
                              "line": 237.5
                           },
                           {
                              "providerOutcomeId": 2846916989,
                              "providerId": 1,
                              "providerOfferId": 2242650435,
                              "label": "Under",
                              "oddsAmerican": "-530",
                              "oddsDecimal": 1.19,
                              "oddsFractional": "2/11",
                              "line": 237.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242650441,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846917000,
                              "providerId": 1,
                              "providerOfferId": 2242650441,
                              "label": "Over",
                              "oddsAmerican": "+400",
                              "oddsDecimal": 5.0,
                              "oddsFractional": "4/1",
                              "line": 238.0
                           },
                           {
                              "providerOutcomeId": 2846917001,
                              "providerId": 1,
                              "providerOfferId": 2242650441,
                              "label": "Under",
                              "oddsAmerican": "-560",
                              "oddsDecimal": 1.18,
                              "oddsFractional": "1/6",
                              "line": 238.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242650443,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -373516789,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159509,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846917004,
                              "providerId": 1,
                              "providerOfferId": 2242650443,
                              "label": "Over",
                              "oddsAmerican": "+420",
                              "oddsDecimal": 5.2,
                              "oddsFractional": "21/5",
                              "line": 238.5
                           },
                           {
                              "providerOutcomeId": 2846917005,
                              "providerId": 1,
                              "providerOfferId": 2242650443,
                              "label": "Under",
                              "oddsAmerican": "-590",
                              "oddsDecimal": 1.17,
                              "oddsFractional": "1/6",
                              "line": 238.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     }
                  ]
               ]
            }
         ]
      },
      {
         "categoryId": 65,
         "name": "Player Props",
         "componentizedOffers": [
            {
               "componentId": 29,
               "subcategoryName": "First Field Goal",
               "offers": [
                  [
                     {
                        "providerOfferId": 2242674574,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Player to Score the First Field Goal of the Game",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 2198,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 4,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1005432697,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846899390,
                              "providerId": 1,
                              "providerOfferId": 2242674574,
                              "label": "Domantas Sabonis",
                              "oddsAmerican": "+550",
                              "oddsDecimal": 6.5,
                              "oddsFractional": "11/2",
                              "participant": "Domantas Sabonis"
                           },
                           {
                              "providerOutcomeId": 2846899409,
                              "providerId": 1,
                              "providerOfferId": 2242674574,
                              "label": "Andre Drummond",
                              "oddsAmerican": "+550",
                              "oddsDecimal": 6.5,
                              "oddsFractional": "11/2",
                              "participant": "Andre Drummond"
                           },
                           {
                              "providerOutcomeId": 2846899419,
                              "providerId": 1,
                              "providerOfferId": 2242674574,
                              "label": "Collin Sexton",
                              "oddsAmerican": "+550",
                              "oddsDecimal": 6.5,
                              "oddsFractional": "11/2",
                              "participant": "Collin Sexton"
                           },
                           {
                              "providerOutcomeId": 2846899389,
                              "providerId": 1,
                              "providerOfferId": 2242674574,
                              "label": "Victor Oladipo",
                              "oddsAmerican": "+700",
                              "oddsDecimal": 8.0,
                              "oddsFractional": "7/1",
                              "participant": "Victor Oladipo"
                           },
                           {
                              "providerOutcomeId": 2846899366,
                              "providerId": 1,
                              "providerOfferId": 2242674574,
                              "label": "Malcolm Brogdon",
                              "oddsAmerican": "+800",
                              "oddsDecimal": 9.0,
                              "oddsFractional": "8/1",
                              "participant": "Malcolm Brogdon"
                           },
                           {
                              "providerOutcomeId": 2846899368,
                              "providerId": 1,
                              "providerOfferId": 2242674574,
                              "label": "Justin Holiday",
                              "oddsAmerican": "+1000",
                              "oddsDecimal": 11.0,
                              "oddsFractional": "10/1",
                              "participant": "Justin Holiday"
                           },
                           {
                              "providerOutcomeId": 2846899394,
                              "providerId": 1,
                              "providerOfferId": 2242674574,
                              "label": "Myles Turner",
                              "oddsAmerican": "+1000",
                              "oddsDecimal": 11.0,
                              "oddsFractional": "10/1",
                              "participant": "Myles Turner"
                           },
                           {
                              "providerOutcomeId": 2846899411,
                              "providerId": 1,
                              "providerOfferId": 2242674574,
                              "label": "Darius Garland",
                              "oddsAmerican": "+1000",
                              "oddsDecimal": 11.0,
                              "oddsFractional": "10/1",
                              "participant": "Darius Garland"
                           },
                           {
                              "providerOutcomeId": 2846899415,
                              "providerId": 1,
                              "providerOfferId": 2242674574,
                              "label": "Larry Nance Jr.",
                              "oddsAmerican": "+1000",
                              "oddsDecimal": 11.0,
                              "oddsFractional": "10/1",
                              "participant": "Larry Nance Jr."
                           },
                           {
                              "providerOutcomeId": 2846899417,
                              "providerId": 1,
                              "providerOfferId": 2242674574,
                              "label": "Cedi Osman",
                              "oddsAmerican": "+1200",
                              "oddsDecimal": 13.0,
                              "oddsFractional": "12/1",
                              "participant": "Cedi Osman"
                           }
                        ],
                        "extra": "Wagers on players not in starting 5 will be refunded.",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185536183,
                        "source": "1"
                     }
                  ]
               ]
            },
            {
               "componentId": 8,
               "subcategoryName": "Points",
               "offers": [
                  [
                     {
                        "providerOfferId": 2242674179,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Points scored by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 820,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001223303,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846897010,
                              "providerId": 1,
                              "providerOfferId": 2242674179,
                              "label": "Over 12.5",
                              "oddsAmerican": "-121",
                              "oddsDecimal": 1.83,
                              "oddsFractional": "41/50",
                              "participant": "Cedi Osman"
                           },
                           {
                              "providerOutcomeId": 2846897011,
                              "providerId": 1,
                              "providerOfferId": 2242674179,
                              "label": "Under 12.5",
                              "oddsAmerican": "+100",
                              "oddsDecimal": 2.0,
                              "oddsFractional": "Evens",
                              "participant": "Cedi Osman"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185536022,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242674177,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Points scored by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 820,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001223303,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846897006,
                              "providerId": 1,
                              "providerOfferId": 2242674177,
                              "label": "Over 14.5",
                              "oddsAmerican": "-121",
                              "oddsDecimal": 1.83,
                              "oddsFractional": "41/50",
                              "participant": "Darius Garland"
                           },
                           {
                              "providerOutcomeId": 2846897007,
                              "providerId": 1,
                              "providerOfferId": 2242674177,
                              "label": "Under 14.5",
                              "oddsAmerican": "+100",
                              "oddsDecimal": 2.0,
                              "oddsFractional": "Evens",
                              "participant": "Darius Garland"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185535963,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242674170,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Points scored by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 820,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001223303,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846896991,
                              "providerId": 1,
                              "providerOfferId": 2242674170,
                              "label": "Over 19.5",
                              "oddsAmerican": "+100",
                              "oddsDecimal": 2.0,
                              "oddsFractional": "Evens",
                              "participant": "Victor Oladipo"
                           },
                           {
                              "providerOutcomeId": 2846896992,
                              "providerId": 1,
                              "providerOfferId": 2242674170,
                              "label": "Under 19.5",
                              "oddsAmerican": "-121",
                              "oddsDecimal": 1.83,
                              "oddsFractional": "41/50",
                              "participant": "Victor Oladipo"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185620870,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242674173,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Points scored by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 820,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001223303,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846896998,
                              "providerId": 1,
                              "providerOfferId": 2242674173,
                              "label": "Over 13.5",
                              "oddsAmerican": "+100",
                              "oddsDecimal": 2.0,
                              "oddsFractional": "Evens",
                              "participant": "Myles Turner"
                           },
                           {
                              "providerOutcomeId": 2846896999,
                              "providerId": 1,
                              "providerOfferId": 2242674173,
                              "label": "Under 13.5",
                              "oddsAmerican": "-121",
                              "oddsDecimal": 1.83,
                              "oddsFractional": "41/50",
                              "participant": "Myles Turner"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185536022,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242674171,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Points scored by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 820,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001223303,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846896993,
                              "providerId": 1,
                              "providerOfferId": 2242674171,
                              "label": "Over 19.5",
                              "oddsAmerican": "-110",
                              "oddsDecimal": 1.91,
                              "oddsFractional": "10/11",
                              "participant": "Malcolm Brogdon"
                           },
                           {
                              "providerOutcomeId": 2846896994,
                              "providerId": 1,
                              "providerOfferId": 2242674171,
                              "label": "Under 19.5",
                              "oddsAmerican": "-110",
                              "oddsDecimal": 1.91,
                              "oddsFractional": "10/11",
                              "participant": "Malcolm Brogdon"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185535963,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242674174,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Points scored by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 820,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001223303,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846897000,
                              "providerId": 1,
                              "providerOfferId": 2242674174,
                              "label": "Over 9.5",
                              "oddsAmerican": "+100",
                              "oddsDecimal": 2.0,
                              "oddsFractional": "Evens",
                              "participant": "Justin Holiday"
                           },
                           {
                              "providerOutcomeId": 2846897001,
                              "providerId": 1,
                              "providerOfferId": 2242674174,
                              "label": "Under 9.5",
                              "oddsAmerican": "-121",
                              "oddsDecimal": 1.83,
                              "oddsFractional": "41/50",
                              "participant": "Justin Holiday"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185535963,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242674176,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Points scored by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 820,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001223303,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846897004,
                              "providerId": 1,
                              "providerOfferId": 2242674176,
                              "label": "Over 18.5",
                              "oddsAmerican": "-121",
                              "oddsDecimal": 1.83,
                              "oddsFractional": "41/50",
                              "participant": "Andre Drummond"
                           },
                           {
                              "providerOutcomeId": 2846897005,
                              "providerId": 1,
                              "providerOfferId": 2242674176,
                              "label": "Under 18.5",
                              "oddsAmerican": "+100",
                              "oddsDecimal": 2.0,
                              "oddsFractional": "Evens",
                              "participant": "Andre Drummond"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185536022,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242674175,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Points scored by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 820,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001223303,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846897002,
                              "providerId": 1,
                              "providerOfferId": 2242674175,
                              "label": "Over 23.5",
                              "oddsAmerican": "+110",
                              "oddsDecimal": 2.1,
                              "oddsFractional": "11/10",
                              "participant": "Collin Sexton"
                           },
                           {
                              "providerOutcomeId": 2846897003,
                              "providerId": 1,
                              "providerOfferId": 2242674175,
                              "label": "Under 23.5",
                              "oddsAmerican": "-134",
                              "oddsDecimal": 1.75,
                              "oddsFractional": "3/4",
                              "participant": "Collin Sexton"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185536022,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242674178,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Points scored by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 820,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001223303,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846897008,
                              "providerId": 1,
                              "providerOfferId": 2242674178,
                              "label": "Over 12.5",
                              "oddsAmerican": "+100",
                              "oddsDecimal": 2.0,
                              "oddsFractional": "Evens",
                              "participant": "Larry Nance Jr."
                           },
                           {
                              "providerOutcomeId": 2846897009,
                              "providerId": 1,
                              "providerOfferId": 2242674178,
                              "label": "Under 12.5",
                              "oddsAmerican": "-121",
                              "oddsDecimal": 1.83,
                              "oddsFractional": "41/50",
                              "participant": "Larry Nance Jr."
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185536022,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242674169,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Points scored by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 820,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001223303,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846896989,
                              "providerId": 1,
                              "providerOfferId": 2242674169,
                              "label": "Over 20.5",
                              "oddsAmerican": "-110",
                              "oddsDecimal": 1.91,
                              "oddsFractional": "10/11",
                              "participant": "Domantas Sabonis"
                           },
                           {
                              "providerOutcomeId": 2846896990,
                              "providerId": 1,
                              "providerOfferId": 2242674169,
                              "label": "Under 20.5",
                              "oddsAmerican": "-110",
                              "oddsDecimal": 1.91,
                              "oddsFractional": "10/11",
                              "participant": "Domantas Sabonis"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185535963,
                        "source": "1"
                     }
                  ]
               ]
            },
            {
               "componentId": 8,
               "subcategoryName": "3-Pointers",
               "offers": [
                  [
                     {
                        "providerOfferId": 2242674485,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "3-point field goals made by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 817,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1002884079,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846899140,
                              "providerId": 1,
                              "providerOfferId": 2242674485,
                              "label": "Over 2.5",
                              "oddsAmerican": "+125",
                              "oddsDecimal": 2.25,
                              "oddsFractional": "5/4",
                              "participant": "Victor Oladipo"
                           },
                           {
                              "providerOutcomeId": 2846899143,
                              "providerId": 1,
                              "providerOfferId": 2242674485,
                              "label": "Under 2.5",
                              "oddsAmerican": "-152",
                              "oddsDecimal": 1.66,
                              "oddsFractional": "13/20",
                              "participant": "Victor Oladipo"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185551739,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242674488,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "3-point field goals made by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 817,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1002884079,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846899146,
                              "providerId": 1,
                              "providerOfferId": 2242674488,
                              "label": "Over 1.5",
                              "oddsAmerican": "-177",
                              "oddsDecimal": 1.57,
                              "oddsFractional": "11/20",
                              "participant": "Malcolm Brogdon"
                           },
                           {
                              "providerOutcomeId": 2846899147,
                              "providerId": 1,
                              "providerOfferId": 2242674488,
                              "label": "Under 1.5",
                              "oddsAmerican": "+144",
                              "oddsDecimal": 2.44,
                              "oddsFractional": "7/5",
                              "participant": "Malcolm Brogdon"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185535963,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242674491,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "3-point field goals made by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 817,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1002884079,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846899152,
                              "providerId": 1,
                              "providerOfferId": 2242674491,
                              "label": "Over 1.5",
                              "oddsAmerican": "+125",
                              "oddsDecimal": 2.25,
                              "oddsFractional": "5/4",
                              "participant": "Myles Turner"
                           },
                           {
                              "providerOutcomeId": 2846899153,
                              "providerId": 1,
                              "providerOfferId": 2242674491,
                              "label": "Under 1.5",
                              "oddsAmerican": "-152",
                              "oddsDecimal": 1.66,
                              "oddsFractional": "13/20",
                              "participant": "Myles Turner"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185536022,
                        "source": "1"
                     }
                  ]
               ]
            },
            {
               "componentId": 8,
               "subcategoryName": "Points, Rebounds & Assists",
               "offers": [
                  [
                     {
                        "providerOfferId": 2242674453,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Points, rebounds & assists by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 824,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1002098847,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846899033,
                              "providerId": 1,
                              "providerOfferId": 2242674453,
                              "label": "Over 27.5",
                              "oddsAmerican": "-125",
                              "oddsDecimal": 1.8,
                              "oddsFractional": "4/5",
                              "participant": "Victor Oladipo"
                           },
                           {
                              "providerOutcomeId": 2846899034,
                              "providerId": 1,
                              "providerOfferId": 2242674453,
                              "label": "Under 27.5",
                              "oddsAmerican": "+103",
                              "oddsDecimal": 2.03,
                              "oddsFractional": "Evens",
                              "participant": "Victor Oladipo"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185587486,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242674455,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Points, rebounds & assists by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 824,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1002098847,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846899038,
                              "providerId": 1,
                              "providerOfferId": 2242674455,
                              "label": "Over 22.5",
                              "oddsAmerican": "-110",
                              "oddsDecimal": 1.91,
                              "oddsFractional": "10/11",
                              "participant": "Myles Turner"
                           },
                           {
                              "providerOutcomeId": 2846899039,
                              "providerId": 1,
                              "providerOfferId": 2242674455,
                              "label": "Under 22.5",
                              "oddsAmerican": "-110",
                              "oddsDecimal": 1.91,
                              "oddsFractional": "10/11",
                              "participant": "Myles Turner"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185536022,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242674449,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Points, rebounds & assists by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 824,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1002098847,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846899023,
                              "providerId": 1,
                              "providerOfferId": 2242674449,
                              "label": "Over 31.5",
                              "oddsAmerican": "-110",
                              "oddsDecimal": 1.91,
                              "oddsFractional": "10/11",
                              "participant": "Malcolm Brogdon"
                           },
                           {
                              "providerOutcomeId": 2846899024,
                              "providerId": 1,
                              "providerOfferId": 2242674449,
                              "label": "Under 31.5",
                              "oddsAmerican": "-110",
                              "oddsDecimal": 1.91,
                              "oddsFractional": "10/11",
                              "participant": "Malcolm Brogdon"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185535963,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242674462,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Points, rebounds & assists by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 824,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1002098847,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846899084,
                              "providerId": 1,
                              "providerOfferId": 2242674462,
                              "label": "Over 22.5",
                              "oddsAmerican": "-134",
                              "oddsDecimal": 1.75,
                              "oddsFractional": "3/4",
                              "participant": "Darius Garland"
                           },
                           {
                              "providerOutcomeId": 2846899085,
                              "providerId": 1,
                              "providerOfferId": 2242674462,
                              "label": "Under 22.5",
                              "oddsAmerican": "+110",
                              "oddsDecimal": 2.1,
                              "oddsFractional": "11/10",
                              "participant": "Darius Garland"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185632650,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242674458,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Points, rebounds & assists by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 824,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1002098847,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846899044,
                              "providerId": 1,
                              "providerOfferId": 2242674458,
                              "label": "Over 30.5",
                              "oddsAmerican": "+100",
                              "oddsDecimal": 2.0,
                              "oddsFractional": "Evens",
                              "participant": "Collin Sexton"
                           },
                           {
                              "providerOutcomeId": 2846899045,
                              "providerId": 1,
                              "providerOfferId": 2242674458,
                              "label": "Under 30.5",
                              "oddsAmerican": "-121",
                              "oddsDecimal": 1.83,
                              "oddsFractional": "41/50",
                              "participant": "Collin Sexton"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185637757,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242674467,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Points, rebounds & assists by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 824,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1002098847,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846899094,
                              "providerId": 1,
                              "providerOfferId": 2242674467,
                              "label": "Over 19.5",
                              "oddsAmerican": "-110",
                              "oddsDecimal": 1.91,
                              "oddsFractional": "10/11",
                              "participant": "Cedi Osman"
                           },
                           {
                              "providerOutcomeId": 2846899095,
                              "providerId": 1,
                              "providerOfferId": 2242674467,
                              "label": "Under 19.5",
                              "oddsAmerican": "-110",
                              "oddsDecimal": 1.91,
                              "oddsFractional": "10/11",
                              "participant": "Cedi Osman"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185536022,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242674456,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Points, rebounds & assists by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 824,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1002098847,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846899040,
                              "providerId": 1,
                              "providerOfferId": 2242674456,
                              "label": "Over 14.5",
                              "oddsAmerican": "-110",
                              "oddsDecimal": 1.91,
                              "oddsFractional": "10/11",
                              "participant": "Justin Holiday"
                           },
                           {
                              "providerOutcomeId": 2846899041,
                              "providerId": 1,
                              "providerOfferId": 2242674456,
                              "label": "Under 14.5",
                              "oddsAmerican": "-110",
                              "oddsDecimal": 1.91,
                              "oddsFractional": "10/11",
                              "participant": "Justin Holiday"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185535963,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242674451,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Points, rebounds & assists by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 824,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1002098847,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846899028,
                              "providerId": 1,
                              "providerOfferId": 2242674451,
                              "label": "Over 37.5",
                              "oddsAmerican": "-110",
                              "oddsDecimal": 1.91,
                              "oddsFractional": "10/11",
                              "participant": "Domantas Sabonis"
                           },
                           {
                              "providerOutcomeId": 2846899029,
                              "providerId": 1,
                              "providerOfferId": 2242674451,
                              "label": "Under 37.5",
                              "oddsAmerican": "-110",
                              "oddsDecimal": 1.91,
                              "oddsFractional": "10/11",
                              "participant": "Domantas Sabonis"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185535963,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242674465,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Points, rebounds & assists by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 824,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1002098847,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846899090,
                              "providerId": 1,
                              "providerOfferId": 2242674465,
                              "label": "Over 24.5",
                              "oddsAmerican": "-110",
                              "oddsDecimal": 1.91,
                              "oddsFractional": "10/11",
                              "participant": "Larry Nance Jr."
                           },
                           {
                              "providerOutcomeId": 2846899091,
                              "providerId": 1,
                              "providerOfferId": 2242674465,
                              "label": "Under 24.5",
                              "oddsAmerican": "-110",
                              "oddsDecimal": 1.91,
                              "oddsFractional": "10/11",
                              "participant": "Larry Nance Jr."
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185536022,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242674461,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Points, rebounds & assists by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 824,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1002098847,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846899079,
                              "providerId": 1,
                              "providerOfferId": 2242674461,
                              "label": "Over 37.5",
                              "oddsAmerican": "-110",
                              "oddsDecimal": 1.91,
                              "oddsFractional": "10/11",
                              "participant": "Andre Drummond"
                           },
                           {
                              "providerOutcomeId": 2846899081,
                              "providerId": 1,
                              "providerOfferId": 2242674461,
                              "label": "Under 37.5",
                              "oddsAmerican": "-110",
                              "oddsDecimal": 1.91,
                              "oddsFractional": "10/11",
                              "participant": "Andre Drummond"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185536022,
                        "source": "1"
                     }
                  ]
               ]
            },
            {
               "componentId": 8,
               "subcategoryName": "Rebounds",
               "offers": [
                  [
                     {
                        "providerOfferId": 2242674233,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Rebounds by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 825,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001225286,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846897188,
                              "providerId": 1,
                              "providerOfferId": 2242674233,
                              "label": "Over 4.5",
                              "oddsAmerican": "-134",
                              "oddsDecimal": 1.75,
                              "oddsFractional": "3/4",
                              "participant": "Malcolm Brogdon"
                           },
                           {
                              "providerOutcomeId": 2846897189,
                              "providerId": 1,
                              "providerOfferId": 2242674233,
                              "label": "Under 4.5",
                              "oddsAmerican": "+110",
                              "oddsDecimal": 2.1,
                              "oddsFractional": "11/10",
                              "participant": "Malcolm Brogdon"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185535963,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242674234,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Rebounds by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 825,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001225286,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846897190,
                              "providerId": 1,
                              "providerOfferId": 2242674234,
                              "label": "Over 6.5",
                              "oddsAmerican": "-134",
                              "oddsDecimal": 1.75,
                              "oddsFractional": "3/4",
                              "participant": "Myles Turner"
                           },
                           {
                              "providerOutcomeId": 2846897191,
                              "providerId": 1,
                              "providerOfferId": 2242674234,
                              "label": "Under 6.5",
                              "oddsAmerican": "+110",
                              "oddsDecimal": 2.1,
                              "oddsFractional": "11/10",
                              "participant": "Myles Turner"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185536022,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242674236,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Rebounds by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 825,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001225286,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846897194,
                              "providerId": 1,
                              "providerOfferId": 2242674236,
                              "label": "Over 8.5",
                              "oddsAmerican": "-121",
                              "oddsDecimal": 1.83,
                              "oddsFractional": "41/50",
                              "participant": "Larry Nance Jr."
                           },
                           {
                              "providerOutcomeId": 2846897195,
                              "providerId": 1,
                              "providerOfferId": 2242674236,
                              "label": "Under 8.5",
                              "oddsAmerican": "+100",
                              "oddsDecimal": 2.0,
                              "oddsFractional": "Evens",
                              "participant": "Larry Nance Jr."
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185584842,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242674235,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Rebounds by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 825,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001225286,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846897192,
                              "providerId": 1,
                              "providerOfferId": 2242674235,
                              "label": "Over 15.5",
                              "oddsAmerican": "+110",
                              "oddsDecimal": 2.1,
                              "oddsFractional": "11/10",
                              "participant": "Andre Drummond"
                           },
                           {
                              "providerOutcomeId": 2846897193,
                              "providerId": 1,
                              "providerOfferId": 2242674235,
                              "label": "Under 15.5",
                              "oddsAmerican": "-134",
                              "oddsDecimal": 1.75,
                              "oddsFractional": "3/4",
                              "participant": "Andre Drummond"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185536022,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242674231,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Rebounds by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 825,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001225286,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846897184,
                              "providerId": 1,
                              "providerOfferId": 2242674231,
                              "label": "Over 11.5",
                              "oddsAmerican": "+110",
                              "oddsDecimal": 2.1,
                              "oddsFractional": "11/10",
                              "participant": "Domantas Sabonis"
                           },
                           {
                              "providerOutcomeId": 2846897185,
                              "providerId": 1,
                              "providerOfferId": 2242674231,
                              "label": "Under 11.5",
                              "oddsAmerican": "-134",
                              "oddsDecimal": 1.75,
                              "oddsFractional": "3/4",
                              "participant": "Domantas Sabonis"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185535963,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242674232,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Rebounds by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 825,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001225286,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846897186,
                              "providerId": 1,
                              "providerOfferId": 2242674232,
                              "label": "Over 4.5",
                              "oddsAmerican": "-110",
                              "oddsDecimal": 1.91,
                              "oddsFractional": "10/11",
                              "participant": "Victor Oladipo"
                           },
                           {
                              "providerOutcomeId": 2846897187,
                              "providerId": 1,
                              "providerOfferId": 2242674232,
                              "label": "Under 4.5",
                              "oddsAmerican": "-110",
                              "oddsDecimal": 1.91,
                              "oddsFractional": "10/11",
                              "participant": "Victor Oladipo"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185535963,
                        "source": "1"
                     }
                  ]
               ]
            },
            {
               "componentId": 8,
               "subcategoryName": "Assists",
               "offers": [
                  [
                     {
                        "providerOfferId": 2242674334,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Assists by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 818,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001225285,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846897500,
                              "providerId": 1,
                              "providerOfferId": 2242674334,
                              "label": "Over 6.5",
                              "oddsAmerican": "-121",
                              "oddsDecimal": 1.83,
                              "oddsFractional": "41/50",
                              "participant": "Malcolm Brogdon"
                           },
                           {
                              "providerOutcomeId": 2846897501,
                              "providerId": 1,
                              "providerOfferId": 2242674334,
                              "label": "Under 6.5",
                              "oddsAmerican": "+100",
                              "oddsDecimal": 2.0,
                              "oddsFractional": "Evens",
                              "participant": "Malcolm Brogdon"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185535963,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242674335,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Assists by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 818,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001225285,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846897502,
                              "providerId": 1,
                              "providerOfferId": 2242674335,
                              "label": "Over 5.5",
                              "oddsAmerican": "-134",
                              "oddsDecimal": 1.75,
                              "oddsFractional": "3/4",
                              "participant": "Darius Garland"
                           },
                           {
                              "providerOutcomeId": 2846897503,
                              "providerId": 1,
                              "providerOfferId": 2242674335,
                              "label": "Under 5.5",
                              "oddsAmerican": "+110",
                              "oddsDecimal": 2.1,
                              "oddsFractional": "11/10",
                              "participant": "Darius Garland"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185535963,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242674332,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Assists by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 818,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001225285,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846897496,
                              "providerId": 1,
                              "providerOfferId": 2242674332,
                              "label": "Over 3.5",
                              "oddsAmerican": "-134",
                              "oddsDecimal": 1.75,
                              "oddsFractional": "3/4",
                              "participant": "Collin Sexton"
                           },
                           {
                              "providerOutcomeId": 2846897497,
                              "providerId": 1,
                              "providerOfferId": 2242674332,
                              "label": "Under 3.5",
                              "oddsAmerican": "+110",
                              "oddsDecimal": 2.1,
                              "oddsFractional": "11/10",
                              "participant": "Collin Sexton"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185536022,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242674331,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Assists by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 818,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001225285,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846897494,
                              "providerId": 1,
                              "providerOfferId": 2242674331,
                              "label": "Over 3.5",
                              "oddsAmerican": "-110",
                              "oddsDecimal": 1.91,
                              "oddsFractional": "10/11",
                              "participant": "Victor Oladipo"
                           },
                           {
                              "providerOutcomeId": 2846897495,
                              "providerId": 1,
                              "providerOfferId": 2242674331,
                              "label": "Under 3.5",
                              "oddsAmerican": "-110",
                              "oddsDecimal": 1.91,
                              "oddsFractional": "10/11",
                              "participant": "Victor Oladipo"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185535963,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242674333,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Assists by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 818,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001225285,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846897498,
                              "providerId": 1,
                              "providerOfferId": 2242674333,
                              "label": "Over 3.5",
                              "oddsAmerican": "-110",
                              "oddsDecimal": 1.91,
                              "oddsFractional": "10/11",
                              "participant": "Larry Nance Jr."
                           },
                           {
                              "providerOutcomeId": 2846897499,
                              "providerId": 1,
                              "providerOfferId": 2242674333,
                              "label": "Under 3.5",
                              "oddsAmerican": "-110",
                              "oddsDecimal": 1.91,
                              "oddsFractional": "10/11",
                              "participant": "Larry Nance Jr."
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185536022,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242674253,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Assists by the player",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 818,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001225285,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846897237,
                              "providerId": 1,
                              "providerOfferId": 2242674253,
                              "label": "Over 5.5",
                              "oddsAmerican": "-110",
                              "oddsDecimal": 1.91,
                              "oddsFractional": "10/11",
                              "participant": "Domantas Sabonis"
                           },
                           {
                              "providerOutcomeId": 2846897238,
                              "providerId": 1,
                              "providerOfferId": 2242674253,
                              "label": "Under 5.5",
                              "oddsAmerican": "-110",
                              "oddsDecimal": 1.91,
                              "oddsFractional": "10/11",
                              "participant": "Domantas Sabonis"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185535963,
                        "source": "1"
                     }
                  ]
               ]
            },
            {
               "componentId": 8,
               "subcategoryName": "Double-Double",
               "offers": [
                  [
                     {
                        "providerOfferId": 2242674535,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "To record a double-double",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 3827,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001225291,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846899272,
                              "providerId": 1,
                              "providerOfferId": 2242674535,
                              "label": "Yes",
                              "oddsAmerican": "+420",
                              "oddsDecimal": 5.2,
                              "oddsFractional": "21/5",
                              "participant": "Malcolm Brogdon"
                           },
                           {
                              "providerOutcomeId": 2846899273,
                              "providerId": 1,
                              "providerOfferId": 2242674535,
                              "label": "No",
                              "oddsAmerican": "-590",
                              "oddsDecimal": 1.17,
                              "oddsFractional": "1/6",
                              "participant": "Malcolm Brogdon"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185535963,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242674536,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "To record a double-double",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 3827,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001225291,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846899274,
                              "providerId": 1,
                              "providerOfferId": 2242674536,
                              "label": "Yes",
                              "oddsAmerican": "-910",
                              "oddsDecimal": 1.11,
                              "oddsFractional": "1/10",
                              "participant": "Andre Drummond"
                           },
                           {
                              "providerOutcomeId": 2846899275,
                              "providerId": 1,
                              "providerOfferId": 2242674536,
                              "label": "No",
                              "oddsAmerican": "+580",
                              "oddsDecimal": 6.8,
                              "oddsFractional": "23/4",
                              "participant": "Andre Drummond"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185535963,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242674534,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "To record a double-double",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 3827,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001225291,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846899270,
                              "providerId": 1,
                              "providerOfferId": 2242674534,
                              "label": "Yes",
                              "oddsAmerican": "-250",
                              "oddsDecimal": 1.4,
                              "oddsFractional": "2/5",
                              "participant": "Domantas Sabonis"
                           },
                           {
                              "providerOutcomeId": 2846899271,
                              "providerId": 1,
                              "providerOfferId": 2242674534,
                              "label": "No",
                              "oddsAmerican": "+200",
                              "oddsDecimal": 3.0,
                              "oddsFractional": "2/1",
                              "participant": "Domantas Sabonis"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185536022,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242674537,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "To record a double-double",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 3827,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 13,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001225291,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846899276,
                              "providerId": 1,
                              "providerOfferId": 2242674537,
                              "label": "Yes",
                              "oddsAmerican": "+175",
                              "oddsDecimal": 2.75,
                              "oddsFractional": "7/4",
                              "participant": "Larry Nance Jr."
                           },
                           {
                              "providerOutcomeId": 2846899277,
                              "providerId": 1,
                              "providerOfferId": 2242674537,
                              "label": "No",
                              "oddsAmerican": "-220",
                              "oddsDecimal": 1.46,
                              "oddsFractional": "4/9",
                              "participant": "Larry Nance Jr."
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185536022,
                        "source": "1"
                     }
                  ]
               ]
            },
            {
               "componentId": 29,
               "subcategoryName": "Top Point Scorer",
               "offers": [
                  [
                     {
                        "providerOfferId": 2242674612,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Top point scorer of the game",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 2896,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 4,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001493090,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846899611,
                              "providerId": 1,
                              "providerOfferId": 2242674612,
                              "label": "Collin Sexton",
                              "oddsAmerican": "+162",
                              "oddsDecimal": 2.62,
                              "oddsFractional": "8/5",
                              "participant": "Collin Sexton"
                           },
                           {
                              "providerOutcomeId": 2846899582,
                              "providerId": 1,
                              "providerOfferId": 2242674612,
                              "label": "Domantas Sabonis",
                              "oddsAmerican": "+300",
                              "oddsDecimal": 4.0,
                              "oddsFractional": "3/1",
                              "participant": "Domantas Sabonis"
                           },
                           {
                              "providerOutcomeId": 2846899581,
                              "providerId": 1,
                              "providerOfferId": 2242674612,
                              "label": "Victor Oladipo",
                              "oddsAmerican": "+500",
                              "oddsDecimal": 6.0,
                              "oddsFractional": "5/1",
                              "participant": "Victor Oladipo"
                           },
                           {
                              "providerOutcomeId": 2846899558,
                              "providerId": 1,
                              "providerOfferId": 2242674612,
                              "label": "Malcolm Brogdon",
                              "oddsAmerican": "+700",
                              "oddsDecimal": 8.0,
                              "oddsFractional": "7/1",
                              "participant": "Malcolm Brogdon"
                           },
                           {
                              "providerOutcomeId": 2846899601,
                              "providerId": 1,
                              "providerOfferId": 2242674612,
                              "label": "Andre Drummond",
                              "oddsAmerican": "+700",
                              "oddsDecimal": 8.0,
                              "oddsFractional": "7/1",
                              "participant": "Andre Drummond"
                           },
                           {
                              "providerOutcomeId": 2846899603,
                              "providerId": 1,
                              "providerOfferId": 2242674612,
                              "label": "Darius Garland",
                              "oddsAmerican": "+2500",
                              "oddsDecimal": 26.0,
                              "oddsFractional": "25/1",
                              "participant": "Darius Garland"
                           },
                           {
                              "providerOutcomeId": 2846899586,
                              "providerId": 1,
                              "providerOfferId": 2242674612,
                              "label": "Myles Turner",
                              "oddsAmerican": "+5000",
                              "oddsDecimal": 51.0,
                              "oddsFractional": "50/1",
                              "participant": "Myles Turner"
                           },
                           {
                              "providerOutcomeId": 2846899607,
                              "providerId": 1,
                              "providerOfferId": 2242674612,
                              "label": "Larry Nance Jr.",
                              "oddsAmerican": "+5000",
                              "oddsDecimal": 51.0,
                              "oddsFractional": "50/1",
                              "participant": "Larry Nance Jr."
                           },
                           {
                              "providerOutcomeId": 2846899609,
                              "providerId": 1,
                              "providerOfferId": 2242674612,
                              "label": "Cedi Osman",
                              "oddsAmerican": "+5000",
                              "oddsDecimal": 51.0,
                              "oddsFractional": "50/1",
                              "participant": "Cedi Osman"
                           },
                           {
                              "providerOutcomeId": 2846899560,
                              "providerId": 1,
                              "providerOfferId": 2242674612,
                              "label": "Justin Holiday",
                              "oddsAmerican": "+10000",
                              "oddsDecimal": 101.0,
                              "oddsFractional": "100/1",
                              "participant": "Justin Holiday"
                           }
                        ],
                        "extra": "Only the players listed will be taken into consideration",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185536183,
                        "source": "1"
                     }
                  ]
               ]
            }
         ]
      },
      {
         "categoryId": 60,
         "name": "Halves",
         "componentizedOffers": [
            {
               "componentId": 10,
               "subcategoryName": "1st Half",
               "offers": [
                  [
                     {
                        "providerOfferId": 2242649627,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Spread - 1st Half",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 233,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159940,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875605,
                              "providerId": 1,
                              "providerOfferId": 2242649627,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "-112",
                              "oddsDecimal": 1.9,
                              "oddsFractional": "9/10",
                              "line": 4.0,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846875604,
                              "providerId": 1,
                              "providerOfferId": 2242649627,
                              "label": "IND Pacers",
                              "oddsAmerican": "-115",
                              "oddsDecimal": 1.87,
                              "oddsFractional": "17/20",
                              "line": -4.0,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242650445,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points - 1st Half",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 233,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159947,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846917008,
                              "providerId": 1,
                              "providerOfferId": 2242650445,
                              "label": "Over",
                              "oddsAmerican": "-110",
                              "oddsDecimal": 1.91,
                              "oddsFractional": "10/11",
                              "line": 112.0
                           },
                           {
                              "providerOutcomeId": 2846917009,
                              "providerId": 1,
                              "providerOfferId": 2242650445,
                              "label": "Under",
                              "oddsAmerican": "-118",
                              "oddsDecimal": 1.85,
                              "oddsFractional": "17/20",
                              "line": 112.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242649604,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Moneyline - 1st Half",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 233,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 2,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159815,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875558,
                              "providerId": 1,
                              "providerOfferId": 2242649604,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "+175",
                              "oddsDecimal": 2.75,
                              "oddsFractional": "7/4",
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846875557,
                              "providerId": 1,
                              "providerOfferId": 2242649604,
                              "label": "IND Pacers",
                              "oddsAmerican": "-230",
                              "oddsDecimal": 1.44,
                              "oddsFractional": "11/25",
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     }
                  ]
               ]
            },
            {
               "componentId": 12,
               "subcategoryName": "Alternate Spread - 1st Half",
               "offers": [
                  [
                     {
                        "providerOfferId": 2242649611,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Spread - 1st Half",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -1217536150,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159940,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875573,
                              "providerId": 1,
                              "providerOfferId": 2242649611,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "+135",
                              "oddsDecimal": 2.35,
                              "oddsFractional": "27/20",
                              "line": 1.5,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846875572,
                              "providerId": 1,
                              "providerOfferId": 2242649611,
                              "label": "IND Pacers",
                              "oddsAmerican": "-175",
                              "oddsDecimal": 1.58,
                              "oddsFractional": "4/7",
                              "line": -1.5,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242649717,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Spread - 1st Half",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -1217536150,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159940,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846876004,
                              "providerId": 1,
                              "providerOfferId": 2242649717,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "-121",
                              "oddsDecimal": 1.83,
                              "oddsFractional": "41/50",
                              "line": 4.5,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846876002,
                              "providerId": 1,
                              "providerOfferId": 2242649717,
                              "label": "IND Pacers",
                              "oddsAmerican": "-106",
                              "oddsDecimal": 1.95,
                              "oddsFractional": "19/20",
                              "line": -4.5,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242649654,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Spread - 1st Half",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -1217536150,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159940,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875664,
                              "providerId": 1,
                              "providerOfferId": 2242649654,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "-200",
                              "oddsDecimal": 1.5,
                              "oddsFractional": "1/2",
                              "line": 7.5,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846875662,
                              "providerId": 1,
                              "providerOfferId": 2242649654,
                              "label": "IND Pacers",
                              "oddsAmerican": "+155",
                              "oddsDecimal": 2.55,
                              "oddsFractional": "31/20",
                              "line": -7.5,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     }
                  ]
               ]
            },
            {
               "componentId": 12,
               "subcategoryName": "Alternate Total Points - 1st Half",
               "offers": [
                  [
                     {
                        "providerOfferId": 2242650405,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points - 1st Half",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -1913263239,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159947,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846916911,
                              "providerId": 1,
                              "providerOfferId": 2242650405,
                              "label": "Over",
                              "oddsAmerican": "-120",
                              "oddsDecimal": 1.84,
                              "oddsFractional": "5/6",
                              "line": 111.5
                           },
                           {
                              "providerOutcomeId": 2846916912,
                              "providerId": 1,
                              "providerOfferId": 2242650405,
                              "label": "Under",
                              "oddsAmerican": "-109",
                              "oddsDecimal": 1.92,
                              "oddsFractional": "10/11",
                              "line": 111.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     }
                  ]
               ]
            },
            {
               "componentId": 10,
               "subcategoryName": "2nd Half",
               "offers": [
                  [
                     {
                        "providerOfferId": 2242649632,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Spread - 2nd Half - Excluding Overtime",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 241,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001906831,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875616,
                              "providerId": 1,
                              "providerOfferId": 2242649632,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "-115",
                              "oddsDecimal": 1.87,
                              "oddsFractional": "17/20",
                              "line": 3.0,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846875615,
                              "providerId": 1,
                              "providerOfferId": 2242649632,
                              "label": "IND Pacers",
                              "oddsAmerican": "-113",
                              "oddsDecimal": 1.89,
                              "oddsFractional": "22/25",
                              "line": -3.0,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242650437,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points - 2nd Half - Excluding Overtime",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 241,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001906832,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846916992,
                              "providerId": 1,
                              "providerOfferId": 2242650437,
                              "label": "Over",
                              "oddsAmerican": "-118",
                              "oddsDecimal": 1.85,
                              "oddsFractional": "17/20",
                              "line": 107.5
                           },
                           {
                              "providerOutcomeId": 2846916993,
                              "providerId": 1,
                              "providerOfferId": 2242650437,
                              "label": "Under",
                              "oddsAmerican": "-113",
                              "oddsDecimal": 1.89,
                              "oddsFractional": "22/25",
                              "line": 107.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242649643,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Moneyline - 2nd Half - Excluding Overtime",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 241,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 2,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001906830,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875638,
                              "providerId": 1,
                              "providerOfferId": 2242649643,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "+145",
                              "oddsDecimal": 2.45,
                              "oddsFractional": "29/20",
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846875637,
                              "providerId": 1,
                              "providerOfferId": 2242649643,
                              "label": "IND Pacers",
                              "oddsAmerican": "-186",
                              "oddsDecimal": 1.54,
                              "oddsFractional": "8/15",
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     }
                  ]
               ]
            },
            {
               "componentId": 12,
               "subcategoryName": "Alternate Spread - 2nd Half - Excluding Overtime",
               "offers": [
                  [
                     {
                        "providerOfferId": 2242649601,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Spread - 2nd Half - Excluding Overtime",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -1530005260,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001906831,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875552,
                              "providerId": 1,
                              "providerOfferId": 2242649601,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "-107",
                              "oddsDecimal": 1.94,
                              "oddsFractional": "23/25",
                              "line": 2.5,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846875551,
                              "providerId": 1,
                              "providerOfferId": 2242649601,
                              "label": "IND Pacers",
                              "oddsAmerican": "-122",
                              "oddsDecimal": 1.82,
                              "oddsFractional": "4/5",
                              "line": -2.5,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     }
                  ]
               ]
            },
            {
               "componentId": 12,
               "subcategoryName": "Alternate Total Points - 2nd Half - Excluding Overtime",
               "offers": [
                  [
                     {
                        "providerOfferId": 2242650446,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points - 2nd Half - Excluding Overtime",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -768844864,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001906832,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846917010,
                              "providerId": 1,
                              "providerOfferId": 2242650446,
                              "label": "Over",
                              "oddsAmerican": "-110",
                              "oddsDecimal": 1.91,
                              "oddsFractional": "10/11",
                              "line": 108.0
                           },
                           {
                              "providerOutcomeId": 2846917011,
                              "providerId": 1,
                              "providerOfferId": 2242650446,
                              "label": "Under",
                              "oddsAmerican": "-121",
                              "oddsDecimal": 1.83,
                              "oddsFractional": "41/50",
                              "line": 108.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     }
                  ]
               ]
            },
            {
               "componentId": 11,
               "subcategoryName": "1st Half - 3 Way",
               "offers": [
                  [
                     {
                        "providerOfferId": 2242649648,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "1st Half (3-way)",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 1390,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 2,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159488,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875649,
                              "providerId": 1,
                              "providerOfferId": 2242649648,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "+188",
                              "oddsDecimal": 2.88,
                              "oddsFractional": "15/8",
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846875648,
                              "providerId": 1,
                              "providerOfferId": 2242649648,
                              "label": "Tie",
                              "oddsAmerican": "+2000",
                              "oddsDecimal": 21.0,
                              "oddsFractional": "20/1",
                              "participant": "Tie"
                           },
                           {
                              "providerOutcomeId": 2846875647,
                              "providerId": 1,
                              "providerOfferId": 2242649648,
                              "label": "IND Pacers",
                              "oddsAmerican": "-200",
                              "oddsDecimal": 1.5,
                              "oddsFractional": "1/2",
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     }
                  ]
               ]
            },
            {
               "componentId": 11,
               "subcategoryName": "2nd Half - 3 Way",
               "offers": [
                  [
                     {
                        "providerOfferId": 2242649650,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "3-way 2nd Half - Excluding Overtime",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 1391,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 2,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001906835,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875656,
                              "providerId": 1,
                              "providerOfferId": 2242649650,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "+155",
                              "oddsDecimal": 2.55,
                              "oddsFractional": "31/20",
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846875655,
                              "providerId": 1,
                              "providerOfferId": 2242649650,
                              "label": "Tie",
                              "oddsAmerican": "+2000",
                              "oddsDecimal": 21.0,
                              "oddsFractional": "20/1",
                              "participant": "Tie"
                           },
                           {
                              "providerOutcomeId": 2846875652,
                              "providerId": 1,
                              "providerOfferId": 2242649650,
                              "label": "IND Pacers",
                              "oddsAmerican": "-162",
                              "oddsDecimal": 1.62,
                              "oddsFractional": "8/13",
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     }
                  ]
               ]
            }
         ]
      },
      {
         "categoryId": 61,
         "name": "Quarters",
         "componentizedOffers": [
            {
               "componentId": 10,
               "subcategoryName": "1st Quarter",
               "offers": [
                  [
                     {
                        "providerOfferId": 2242649641,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Spread - 1st Quarter",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 239,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159576,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875635,
                              "providerId": 1,
                              "providerOfferId": 2242649641,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "-115",
                              "oddsDecimal": 1.87,
                              "oddsFractional": "17/20",
                              "line": 2.5,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846875633,
                              "providerId": 1,
                              "providerOfferId": 2242649641,
                              "label": "IND Pacers",
                              "oddsAmerican": "-110",
                              "oddsDecimal": 1.91,
                              "oddsFractional": "10/11",
                              "line": -2.5,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242650404,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points - 1st Quarter",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 239,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159802,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846916909,
                              "providerId": 1,
                              "providerOfferId": 2242650404,
                              "label": "Over",
                              "oddsAmerican": "-114",
                              "oddsDecimal": 1.88,
                              "oddsFractional": "22/25",
                              "line": 56.0
                           },
                           {
                              "providerOutcomeId": 2846916910,
                              "providerId": 1,
                              "providerOfferId": 2242650404,
                              "label": "Under",
                              "oddsAmerican": "-114",
                              "oddsDecimal": 1.88,
                              "oddsFractional": "22/25",
                              "line": 56.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242649642,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Moneyline - Quarter 1",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 239,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 2,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159866,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875636,
                              "providerId": 1,
                              "providerOfferId": 2242649642,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "+150",
                              "oddsDecimal": 2.5,
                              "oddsFractional": "6/4",
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846875634,
                              "providerId": 1,
                              "providerOfferId": 2242649642,
                              "label": "IND Pacers",
                              "oddsAmerican": "-195",
                              "oddsDecimal": 1.52,
                              "oddsFractional": "13/25",
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     }
                  ]
               ]
            },
            {
               "componentId": 12,
               "subcategoryName": "Alternate Spread - 1st Quarter",
               "offers": [
                  [
                     {
                        "providerOfferId": 2242649607,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Spread - 1st Quarter",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -658990858,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159576,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875564,
                              "providerId": 1,
                              "providerOfferId": 2242649607,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "+200",
                              "oddsDecimal": 3.0,
                              "oddsFractional": "2/1",
                              "line": -1.5,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846875561,
                              "providerId": 1,
                              "providerOfferId": 2242649607,
                              "label": "IND Pacers",
                              "oddsAmerican": "-265",
                              "oddsDecimal": 1.38,
                              "oddsFractional": "4/11",
                              "line": 1.5,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242649593,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Spread - 1st Quarter",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -658990858,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159576,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875535,
                              "providerId": 1,
                              "providerOfferId": 2242649593,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "+185",
                              "oddsDecimal": 2.85,
                              "oddsFractional": "37/20",
                              "line": -1.0,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846875534,
                              "providerId": 1,
                              "providerOfferId": 2242649593,
                              "label": "IND Pacers",
                              "oddsAmerican": "-245",
                              "oddsDecimal": 1.41,
                              "oddsFractional": "2/5",
                              "line": 1.0,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242649655,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Spread - 1st Quarter",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -658990858,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159576,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875665,
                              "providerId": 1,
                              "providerOfferId": 2242649655,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "-104",
                              "oddsDecimal": 1.97,
                              "oddsFractional": "19/20",
                              "line": 2.0,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846875663,
                              "providerId": 1,
                              "providerOfferId": 2242649655,
                              "label": "IND Pacers",
                              "oddsAmerican": "-124",
                              "oddsDecimal": 1.81,
                              "oddsFractional": "4/5",
                              "line": -2.0,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242649715,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Spread - 1st Quarter",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -658990858,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159576,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846876001,
                              "providerId": 1,
                              "providerOfferId": 2242649715,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "-200",
                              "oddsDecimal": 1.5,
                              "oddsFractional": "1/2",
                              "line": 5.0,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846876000,
                              "providerId": 1,
                              "providerOfferId": 2242649715,
                              "label": "IND Pacers",
                              "oddsAmerican": "+155",
                              "oddsDecimal": 2.55,
                              "oddsFractional": "31/20",
                              "line": -5.0,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242649718,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Spread - 1st Quarter",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -658990858,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159576,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846876007,
                              "providerId": 1,
                              "providerOfferId": 2242649718,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "-220",
                              "oddsDecimal": 1.46,
                              "oddsFractional": "4/9",
                              "line": 5.5,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846876006,
                              "providerId": 1,
                              "providerOfferId": 2242649718,
                              "label": "IND Pacers",
                              "oddsAmerican": "+170",
                              "oddsDecimal": 2.7,
                              "oddsFractional": "17/10",
                              "line": -5.5,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     }
                  ]
               ]
            },
            {
               "componentId": 12,
               "subcategoryName": "Alternate Total Points - 1st Quarter",
               "offers": [
                  [
                     {
                        "providerOfferId": 2242649963,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points - 1st Quarter",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -710201223,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159802,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846915556,
                              "providerId": 1,
                              "providerOfferId": 2242649963,
                              "label": "Over",
                              "oddsAmerican": "-125",
                              "oddsDecimal": 1.8,
                              "oddsFractional": "4/5",
                              "line": 55.5
                           },
                           {
                              "providerOutcomeId": 2846915557,
                              "providerId": 1,
                              "providerOfferId": 2242649963,
                              "label": "Under",
                              "oddsAmerican": "-104",
                              "oddsDecimal": 1.97,
                              "oddsFractional": "19/20",
                              "line": 55.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     }
                  ]
               ]
            },
            {
               "componentId": 10,
               "subcategoryName": "2nd Quarter",
               "offers": [
                  [
                     {
                        "providerOfferId": 2242649608,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Spread - 2nd Quarter",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 245,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001906818,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875567,
                              "providerId": 1,
                              "providerOfferId": 2242649608,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "-109",
                              "oddsDecimal": 1.92,
                              "oddsFractional": "10/11",
                              "line": 1.5,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846875566,
                              "providerId": 1,
                              "providerOfferId": 2242649608,
                              "label": "IND Pacers",
                              "oddsAmerican": "-120",
                              "oddsDecimal": 1.84,
                              "oddsFractional": "5/6",
                              "line": -1.5,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242650023,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points - 2nd Quarter",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 245,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001906821,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846915680,
                              "providerId": 1,
                              "providerOfferId": 2242650023,
                              "label": "Over",
                              "oddsAmerican": "-121",
                              "oddsDecimal": 1.83,
                              "oddsFractional": "41/50",
                              "line": 55.5
                           },
                           {
                              "providerOutcomeId": 2846915681,
                              "providerId": 1,
                              "providerOfferId": 2242650023,
                              "label": "Under",
                              "oddsAmerican": "-110",
                              "oddsDecimal": 1.91,
                              "oddsFractional": "10/11",
                              "line": 55.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242649603,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Moneyline - Quarter 2",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 245,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 2,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001906815,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875556,
                              "providerId": 1,
                              "providerOfferId": 2242649603,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "+130",
                              "oddsDecimal": 2.3,
                              "oddsFractional": "13/10",
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846875555,
                              "providerId": 1,
                              "providerOfferId": 2242649603,
                              "label": "IND Pacers",
                              "oddsAmerican": "-167",
                              "oddsDecimal": 1.6,
                              "oddsFractional": "3/5",
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     }
                  ]
               ]
            },
            {
               "componentId": 12,
               "subcategoryName": "Alternate Spread - 2nd Quarter",
               "offers": [
                  [
                     {
                        "providerOfferId": 2242649605,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Spread - 2nd Quarter",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -678604060,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001906818,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875563,
                              "providerId": 1,
                              "providerOfferId": 2242649605,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "-122",
                              "oddsDecimal": 1.82,
                              "oddsFractional": "4/5",
                              "line": 2.0,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846875559,
                              "providerId": 1,
                              "providerOfferId": 2242649605,
                              "label": "IND Pacers",
                              "oddsAmerican": "-107",
                              "oddsDecimal": 1.94,
                              "oddsFractional": "23/25",
                              "line": -2.0,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     }
                  ]
               ]
            },
            {
               "componentId": 12,
               "subcategoryName": "Alternate Total Points - 2nd Quarter",
               "offers": [
                  [
                     {
                        "providerOfferId": 2242650439,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points - 2nd Quarter",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -1875568043,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001906821,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846916996,
                              "providerId": 1,
                              "providerOfferId": 2242650439,
                              "label": "Over",
                              "oddsAmerican": "-110",
                              "oddsDecimal": 1.91,
                              "oddsFractional": "10/11",
                              "line": 56.0
                           },
                           {
                              "providerOutcomeId": 2846916997,
                              "providerId": 1,
                              "providerOfferId": 2242650439,
                              "label": "Under",
                              "oddsAmerican": "-121",
                              "oddsDecimal": 1.83,
                              "oddsFractional": "41/50",
                              "line": 56.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     }
                  ]
               ]
            },
            {
               "componentId": 10,
               "subcategoryName": "3rd Quarter",
               "offers": [
                  [
                     {
                        "providerOfferId": 2242649613,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Spread - 3rd Quarter",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 247,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001906819,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875578,
                              "providerId": 1,
                              "providerOfferId": 2242649613,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "-117",
                              "oddsDecimal": 1.86,
                              "oddsFractional": "17/20",
                              "line": 2.0,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846875575,
                              "providerId": 1,
                              "providerOfferId": 2242649613,
                              "label": "IND Pacers",
                              "oddsAmerican": "-112",
                              "oddsDecimal": 1.9,
                              "oddsFractional": "9/10",
                              "line": -2.0,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242650402,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points - 3rd Quarter",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 247,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001906822,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846916906,
                              "providerId": 1,
                              "providerOfferId": 2242650402,
                              "label": "Over",
                              "oddsAmerican": "-112",
                              "oddsDecimal": 1.9,
                              "oddsFractional": "9/10",
                              "line": 54.0
                           },
                           {
                              "providerOutcomeId": 2846916908,
                              "providerId": 1,
                              "providerOfferId": 2242650402,
                              "label": "Under",
                              "oddsAmerican": "-118",
                              "oddsDecimal": 1.85,
                              "oddsFractional": "17/20",
                              "line": 54.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242649624,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Moneyline - Quarter 3",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 247,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 2,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001906816,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875599,
                              "providerId": 1,
                              "providerOfferId": 2242649624,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "+130",
                              "oddsDecimal": 2.3,
                              "oddsFractional": "13/10",
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846875598,
                              "providerId": 1,
                              "providerOfferId": 2242649624,
                              "label": "IND Pacers",
                              "oddsAmerican": "-167",
                              "oddsDecimal": 1.6,
                              "oddsFractional": "3/5",
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     }
                  ]
               ]
            },
            {
               "componentId": 12,
               "subcategoryName": "Alternate Spread - 3rd Quarter",
               "offers": [
                  [
                     {
                        "providerOfferId": 2242649612,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Spread - 3rd Quarter",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -883232466,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001906819,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875577,
                              "providerId": 1,
                              "providerOfferId": 2242649612,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "-106",
                              "oddsDecimal": 1.95,
                              "oddsFractional": "19/20",
                              "line": 1.5,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846875574,
                              "providerId": 1,
                              "providerOfferId": 2242649612,
                              "label": "IND Pacers",
                              "oddsAmerican": "-122",
                              "oddsDecimal": 1.82,
                              "oddsFractional": "4/5",
                              "line": -1.5,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     }
                  ]
               ]
            },
            {
               "componentId": 12,
               "subcategoryName": "Alternate Total Points - 3rd Quarter",
               "offers": [
                  [
                     {
                        "providerOfferId": 2242649639,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points - 3rd Quarter",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -1449857277,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001906822,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875629,
                              "providerId": 1,
                              "providerOfferId": 2242649639,
                              "label": "Over",
                              "oddsAmerican": "-124",
                              "oddsDecimal": 1.81,
                              "oddsFractional": "4/5",
                              "line": 53.5
                           },
                           {
                              "providerOutcomeId": 2846875631,
                              "providerId": 1,
                              "providerOfferId": 2242649639,
                              "label": "Under",
                              "oddsAmerican": "-108",
                              "oddsDecimal": 1.93,
                              "oddsFractional": "10/11",
                              "line": 53.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     }
                  ]
               ]
            },
            {
               "componentId": 10,
               "subcategoryName": "4th Quarter",
               "offers": [
                  [
                     {
                        "providerOfferId": 2242649652,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Spread - 4th Quarter",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 249,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001906820,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875658,
                              "providerId": 1,
                              "providerOfferId": 2242649652,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "-113",
                              "oddsDecimal": 1.89,
                              "oddsFractional": "22/25",
                              "line": 1.0,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846875657,
                              "providerId": 1,
                              "providerOfferId": 2242649652,
                              "label": "IND Pacers",
                              "oddsAmerican": "-115",
                              "oddsDecimal": 1.87,
                              "oddsFractional": "17/20",
                              "line": -1.0,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242649646,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points - 4th Quarter",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 249,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001906823,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875643,
                              "providerId": 1,
                              "providerOfferId": 2242649646,
                              "label": "Over",
                              "oddsAmerican": "-120",
                              "oddsDecimal": 1.84,
                              "oddsFractional": "5/6",
                              "line": 53.5
                           },
                           {
                              "providerOutcomeId": 2846875644,
                              "providerId": 1,
                              "providerOfferId": 2242649646,
                              "label": "Under",
                              "oddsAmerican": "-112",
                              "oddsDecimal": 1.9,
                              "oddsFractional": "9/10",
                              "line": 53.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242649633,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Moneyline - Quarter 4",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 249,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 2,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001906817,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875619,
                              "providerId": 1,
                              "providerOfferId": 2242649633,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "+112",
                              "oddsDecimal": 2.12,
                              "oddsFractional": "11/10",
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846875617,
                              "providerId": 1,
                              "providerOfferId": 2242649633,
                              "label": "IND Pacers",
                              "oddsAmerican": "-143",
                              "oddsDecimal": 1.7,
                              "oddsFractional": "7/10",
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     }
                  ]
               ]
            },
            {
               "componentId": 12,
               "subcategoryName": "Alternate Spread - 4th Quarter",
               "offers": [
                  [
                     {
                        "providerOfferId": 2242649647,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Spread - 4th Quarter",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -892204161,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001906820,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875646,
                              "providerId": 1,
                              "providerOfferId": 2242649647,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "-124",
                              "oddsDecimal": 1.81,
                              "oddsFractional": "4/5",
                              "line": 1.5,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846875645,
                              "providerId": 1,
                              "providerOfferId": 2242649647,
                              "label": "IND Pacers",
                              "oddsAmerican": "-105",
                              "oddsDecimal": 1.96,
                              "oddsFractional": "19/20",
                              "line": -1.5,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     }
                  ]
               ]
            },
            {
               "componentId": 12,
               "subcategoryName": "Alternate Total Points - 4th Quarter",
               "offers": [
                  [
                     {
                        "providerOfferId": 2242650444,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points - 4th Quarter",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": -1116188655,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001906823,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846917006,
                              "providerId": 1,
                              "providerOfferId": 2242650444,
                              "label": "Over",
                              "oddsAmerican": "-109",
                              "oddsDecimal": 1.92,
                              "oddsFractional": "10/11",
                              "line": 54.0
                           },
                           {
                              "providerOutcomeId": 2846917007,
                              "providerId": 1,
                              "providerOfferId": 2242650444,
                              "label": "Under",
                              "oddsAmerican": "-122",
                              "oddsDecimal": 1.82,
                              "oddsFractional": "4/5",
                              "line": 54.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     }
                  ]
               ]
            },
            {
               "componentId": 30,
               "subcategoryName": "Result at End of 4th Quarter",
               "offers": [
                  [
                     {
                        "providerOfferId": 2242630849,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Result at end of 4th Quarter",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 1096,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 2,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001211820,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775508,
                              "providerId": 1,
                              "providerOfferId": 2242630849,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "+265",
                              "oddsDecimal": 3.65,
                              "oddsFractional": "13/5",
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846775515,
                              "providerId": 1,
                              "providerOfferId": 2242630849,
                              "label": "Tie",
                              "oddsAmerican": "+1500",
                              "oddsDecimal": 16.0,
                              "oddsFractional": "15/1",
                              "participant": "Tie"
                           },
                           {
                              "providerOutcomeId": 2846775511,
                              "providerId": 1,
                              "providerOfferId": 2242630849,
                              "label": "IND Pacers",
                              "oddsAmerican": "-245",
                              "oddsDecimal": 1.41,
                              "oddsFractional": "2/5",
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     }
                  ]
               ]
            },
            {
               "componentId": 12,
               "subcategoryName": "Spread - End of 3rd Quarter",
               "offers": [
                  [
                     {
                        "providerOfferId": 2242649594,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Spread - Result at end of 3rd Quarter",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 938,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159653,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875537,
                              "providerId": 1,
                              "providerOfferId": 2242649594,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "-107",
                              "oddsDecimal": 1.94,
                              "oddsFractional": "23/25",
                              "line": 5.5,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846875536,
                              "providerId": 1,
                              "providerOfferId": 2242649594,
                              "label": "IND Pacers",
                              "oddsAmerican": "-120",
                              "oddsDecimal": 1.84,
                              "oddsFractional": "5/6",
                              "line": -5.5,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242649638,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Spread - Result at end of 3rd Quarter",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 938,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 1,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159653,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875627,
                              "providerId": 1,
                              "providerOfferId": 2242649638,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "-113",
                              "oddsDecimal": 1.89,
                              "oddsFractional": "22/25",
                              "line": 6.0,
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846875626,
                              "providerId": 1,
                              "providerOfferId": 2242649638,
                              "label": "IND Pacers",
                              "oddsAmerican": "-113",
                              "oddsDecimal": 1.89,
                              "oddsFractional": "22/25",
                              "line": -6.0,
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     }
                  ]
               ]
            },
            {
               "componentId": 12,
               "subcategoryName": "Total Points - End of 3rd Quarter",
               "offers": [
                  [
                     {
                        "providerOfferId": 2242650440,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points - Result at end of 3rd Quarter",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 1373,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159726,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846916998,
                              "providerId": 1,
                              "providerOfferId": 2242650440,
                              "label": "Over",
                              "oddsAmerican": "-117",
                              "oddsDecimal": 1.86,
                              "oddsFractional": "17/20",
                              "line": 165.5
                           },
                           {
                              "providerOutcomeId": 2846916999,
                              "providerId": 1,
                              "providerOfferId": 2242650440,
                              "label": "Under",
                              "oddsAmerican": "-112",
                              "oddsDecimal": 1.9,
                              "oddsFractional": "9/10",
                              "line": 165.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242650442,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points - Result at end of 3rd Quarter",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 1373,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159726,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846917002,
                              "providerId": 1,
                              "providerOfferId": 2242650442,
                              "label": "Over",
                              "oddsAmerican": "-110",
                              "oddsDecimal": 1.91,
                              "oddsFractional": "10/11",
                              "line": 166.0
                           },
                           {
                              "providerOutcomeId": 2846917003,
                              "providerId": 1,
                              "providerOfferId": 2242650442,
                              "label": "Under",
                              "oddsAmerican": "-118",
                              "oddsDecimal": 1.85,
                              "oddsFractional": "17/20",
                              "line": 166.0
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     }
                  ]
               ]
            },
            {
               "componentId": 30,
               "subcategoryName": "Quarters (3-Way)",
               "offers": [
                  [
                     {
                        "providerOfferId": 2242649597,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "1st Quarter (3-way)",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 4931,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 2,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159585,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875546,
                              "providerId": 1,
                              "providerOfferId": 2242649597,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "+163",
                              "oddsDecimal": 2.63,
                              "oddsFractional": "13/8",
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846875545,
                              "providerId": 1,
                              "providerOfferId": 2242649597,
                              "label": "Tie",
                              "oddsAmerican": "+1600",
                              "oddsDecimal": 17.0,
                              "oddsFractional": "16/1",
                              "participant": "Tie"
                           },
                           {
                              "providerOutcomeId": 2846875543,
                              "providerId": 1,
                              "providerOfferId": 2242649597,
                              "label": "IND Pacers",
                              "oddsAmerican": "-165",
                              "oddsDecimal": 1.61,
                              "oddsFractional": "3/5",
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242649606,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "2nd Quarter (3-way)",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 4931,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 2,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001922699,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875562,
                              "providerId": 1,
                              "providerOfferId": 2242649606,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "+143",
                              "oddsDecimal": 2.43,
                              "oddsFractional": "7/5",
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846875560,
                              "providerId": 1,
                              "providerOfferId": 2242649606,
                              "label": "Tie",
                              "oddsAmerican": "+1600",
                              "oddsDecimal": 17.0,
                              "oddsFractional": "16/1",
                              "participant": "Tie"
                           },
                           {
                              "providerOutcomeId": 2846875565,
                              "providerId": 1,
                              "providerOfferId": 2242649606,
                              "label": "IND Pacers",
                              "oddsAmerican": "-143",
                              "oddsDecimal": 1.7,
                              "oddsFractional": "7/10",
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242649629,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "3rd Quarter (3-way)",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 4931,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 2,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001922700,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875610,
                              "providerId": 1,
                              "providerOfferId": 2242649629,
                              "label": "CLE Cavaliers",
                              "oddsAmerican": "+145",
                              "oddsDecimal": 2.45,
                              "oddsFractional": "29/20",
                              "participant": "CLE Cavaliers"
                           },
                           {
                              "providerOutcomeId": 2846875609,
                              "providerId": 1,
                              "providerOfferId": 2242649629,
                              "label": "Tie",
                              "oddsAmerican": "+1700",
                              "oddsDecimal": 18.0,
                              "oddsFractional": "17/1",
                              "participant": "Tie"
                           },
                           {
                              "providerOutcomeId": 2846875608,
                              "providerId": 1,
                              "providerOfferId": 2242649629,
                              "label": "IND Pacers",
                              "oddsAmerican": "-143",
                              "oddsDecimal": 1.7,
                              "oddsFractional": "7/10",
                              "participant": "IND Pacers"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     }
                  ]
               ]
            }
         ]
      },
      {
         "categoryId": 59,
         "name": "Game Props",
         "componentizedOffers": [
            {
               "componentId": 29,
               "subcategoryName": "Team Totals",
               "offers": [
                  [
                     {
                        "providerOfferId": 2242649630,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points by CLE Cavaliers - Quarter 1",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 3781,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1002167438,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846875611,
                              "providerId": 1,
                              "providerOfferId": 2242649630,
                              "label": "Over",
                              "oddsAmerican": "-125",
                              "oddsDecimal": 1.8,
                              "oddsFractional": "4/5",
                              "line": 26.5
                           },
                           {
                              "providerOutcomeId": 2846875612,
                              "providerId": 1,
                              "providerOfferId": 2242649630,
                              "label": "Under",
                              "oddsAmerican": "-106",
                              "oddsDecimal": 1.95,
                              "oddsFractional": "19/20",
                              "line": 26.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242650024,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points by IND Pacers - Quarter 1",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 3781,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1002167437,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846915682,
                              "providerId": 1,
                              "providerOfferId": 2242650024,
                              "label": "Over",
                              "oddsAmerican": "-107",
                              "oddsDecimal": 1.94,
                              "oddsFractional": "23/25",
                              "line": 29.5
                           },
                           {
                              "providerOutcomeId": 2846915683,
                              "providerId": 1,
                              "providerOfferId": 2242650024,
                              "label": "Under",
                              "oddsAmerican": "-125",
                              "oddsDecimal": 1.8,
                              "oddsFractional": "4/5",
                              "line": 29.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242650017,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points by CLE Cavaliers - 1st Half",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 3781,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159579,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846915668,
                              "providerId": 1,
                              "providerOfferId": 2242650017,
                              "label": "Over",
                              "oddsAmerican": "-124",
                              "oddsDecimal": 1.81,
                              "oddsFractional": "4/5",
                              "line": 53.5
                           },
                           {
                              "providerOutcomeId": 2846915669,
                              "providerId": 1,
                              "providerOfferId": 2242650017,
                              "label": "Under",
                              "oddsAmerican": "-107",
                              "oddsDecimal": 1.94,
                              "oddsFractional": "23/25",
                              "line": 53.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242649719,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points by IND Pacers - 1st Half",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 3781,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159567,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846876008,
                              "providerId": 1,
                              "providerOfferId": 2242649719,
                              "label": "Over",
                              "oddsAmerican": "-127",
                              "oddsDecimal": 1.79,
                              "oddsFractional": "10/13",
                              "line": 57.5
                           },
                           {
                              "providerOutcomeId": 2846876009,
                              "providerId": 1,
                              "providerOfferId": 2242649719,
                              "label": "Under",
                              "oddsAmerican": "-105",
                              "oddsDecimal": 1.96,
                              "oddsFractional": "19/20",
                              "line": 57.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242650434,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points by CLE Cavaliers",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 3781,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159837,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846916986,
                              "providerId": 1,
                              "providerOfferId": 2242650434,
                              "label": "Over",
                              "oddsAmerican": "-121",
                              "oddsDecimal": 1.83,
                              "oddsFractional": "41/50",
                              "line": 106.5
                           },
                           {
                              "providerOutcomeId": 2846916987,
                              "providerId": 1,
                              "providerOfferId": 2242650434,
                              "label": "Under",
                              "oddsAmerican": "-108",
                              "oddsDecimal": 1.93,
                              "oddsFractional": "10/11",
                              "line": 106.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     },
                     {
                        "providerOfferId": 2242650014,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points by IND Pacers",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 3781,
                        "isSubcategoryFeatured": true,
                        "betOfferTypeId": 6,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001159855,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846915662,
                              "providerId": 1,
                              "providerOfferId": 2242650014,
                              "label": "Over",
                              "oddsAmerican": "-122",
                              "oddsDecimal": 1.82,
                              "oddsFractional": "4/5",
                              "line": 113.5
                           },
                           {
                              "providerOutcomeId": 2846915663,
                              "providerId": 1,
                              "providerOfferId": 2242650014,
                              "label": "Under",
                              "oddsAmerican": "-107",
                              "oddsDecimal": 1.94,
                              "oddsFractional": "23/25",
                              "line": 113.5
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     }
                  ]
               ]
            },
            {
               "componentId": 12,
               "subcategoryName": "Odd/Even",
               "offers": [
                  [
                     {
                        "providerOfferId": 2242630847,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Total Points Odd/Even",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 786,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 10,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001223479,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775505,
                              "providerId": 1,
                              "providerOfferId": 2242630847,
                              "label": "Odd",
                              "oddsAmerican": "-110",
                              "oddsDecimal": 1.91,
                              "oddsFractional": "10/11"
                           },
                           {
                              "providerOutcomeId": 2846775506,
                              "providerId": 1,
                              "providerOfferId": 2242630847,
                              "label": "Even",
                              "oddsAmerican": "-110",
                              "oddsDecimal": 1.91,
                              "oddsFractional": "10/11"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185302514,
                        "source": "1"
                     }
                  ]
               ]
            },
            {
               "componentId": 30,
               "subcategoryName": "Double Chance",
               "offers": [
                  [
                     {
                        "providerOfferId": 2242630840,
                        "providerId": 1,
                        "providerEventId": 1007123606,
                        "providerEventGroupId": 1000093652,
                        "label": "Double Chance - Result at end of 4th Quarter",
                        "isSuspended": false,
                        "isOpen": true,
                        "offerSubcategoryId": 4936,
                        "isSubcategoryFeatured": false,
                        "betOfferTypeId": 12,
                        "displayGroupId": 4,
                        "eventGroupId": 103,
                        "providerCriterionId": 1001657521,
                        "outcomes": [
                           {
                              "providerOutcomeId": 2846775498,
                              "providerId": 1,
                              "providerOfferId": 2242630840,
                              "label": "CLE Cavaliers or Tie",
                              "oddsAmerican": "+195",
                              "oddsDecimal": 2.95,
                              "oddsFractional": "39/20"
                           },
                           {
                              "providerOutcomeId": 2846775491,
                              "providerId": 1,
                              "providerOfferId": 2242630840,
                              "label": "CLE Cavaliers or IND Pacers",
                              "oddsAmerican": "-5000",
                              "oddsDecimal": 1.02,
                              "oddsFractional": "1/50"
                           },
                           {
                              "providerOutcomeId": 2846775490,
                              "providerId": 1,
                              "providerOfferId": 2242630840,
                              "label": "IND Pacers or Tie",
                              "oddsAmerican": "-375",
                              "oddsDecimal": 1.27,
                              "oddsFractional": "27/100"
                           }
                        ],
                        "extra": "",
                        "additionalInformation": "",
                        "offerUpdateState": "0",
                        "offerSequence": 185616660,
                        "source": "1"
                     }
                  ]
               ]
            }
         ]
      }
   ]
}
