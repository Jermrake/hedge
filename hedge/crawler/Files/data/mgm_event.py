{
   "fixture": {
      "optionMarkets": [],
      "games": [
         {
            "id": 561066238,
            "name": {
               "value": "Money Line",
               "sign": "d/LacA=="
            },
            "results": [
               {
                  "id": 1629478867,
                  "odds": 4.4,
                  "name": {
                     "value": "Hornets",
                     "sign": "FjBfeg==",
                     "shortSign": ""
                  },
                  "sourceName": {
                     "value": "1",
                     "sign": "ZFUzfg=="
                  },
                  "visibility": "Visible",
                  "numerator": 17,
                  "denominator": 5,
                  "americanOdds": 340
               },
               {
                  "id": 1629478868,
                  "odds": 1.22,
                  "name": {
                     "value": "76ers",
                     "sign": "pIxfFQ==",
                     "shortSign": ""
                  },
                  "sourceName": {
                     "value": "2",
                     "sign": "lXioxw=="
                  },
                  "visibility": "Visible",
                  "numerator": 2,
                  "denominator": 9,
                  "americanOdds": -450
               }
            ],
            "templateId": 3450,
            "categoryId": 43,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "balanced": 1,
            "spread": 3.18,
            "category": "Gridable",
            "templateCategory": {
               "id": 43,
               "name": {
                  "value": "Money Line",
                  "sign": "d/LacA=="
               },
               "category": "Gridable"
            },
            "isMain": true,
            "grouping": {
               "gridGroups": [
                  "xt229vkj8"
               ],
               "detailed": [
                  {
                     "group": 0,
                     "index": 2,
                     "subIndex": 0,
                     "marketTabId": 8,
                     "displayType": "Regular"
                  }
               ],
               "parameters": {}
            }
         },
         {
            "id": 561387860,
            "name": {
               "value": "Spread",
               "sign": "jl3ERA=="
            },
            "results": [
               {
                  "id": 1630328970,
                  "odds": 1.26,
                  "name": {
                     "value": "Charlotte Hornets +18.5",
                     "sign": "PULdFw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "+18.5",
                  "numerator": 4,
                  "denominator": 15,
                  "americanOdds": -375
               },
               {
                  "id": 1630328971,
                  "odds": 3.9,
                  "name": {
                     "value": "Philadelphia 76ers -18.5",
                     "sign": "acj+jQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "-18.5",
                  "numerator": 29,
                  "denominator": 10,
                  "americanOdds": 290
               }
            ],
            "templateId": 8098,
            "categoryId": 44,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "balanced": 1,
            "spread": 2.64,
            "category": "Gridable",
            "templateCategory": {
               "id": 44,
               "name": {
                  "value": "Spread",
                  "sign": "jl3ERA=="
               },
               "category": "Gridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [
                  "1n91ubk9k"
               ],
               "detailed": [
                  {
                     "group": 0,
                     "index": 7,
                     "subIndex": 0,
                     "marketTabId": 8,
                     "name": "Spread",
                     "displayType": "Spread"
                  },
                  {
                     "group": 2,
                     "index": 1,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Spread",
                     "displayType": "Spread"
                  }
               ],
               "parameters": {}
            }
         },
         {
            "id": 561387863,
            "name": {
               "value": "Spread",
               "sign": "zvC8fQ=="
            },
            "results": [
               {
                  "id": 1630328976,
                  "odds": 1.3,
                  "name": {
                     "value": "Charlotte Hornets +17.5",
                     "sign": "z+r+qw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "+17.5",
                  "numerator": 3,
                  "denominator": 10,
                  "americanOdds": -350
               },
               {
                  "id": 1630328977,
                  "odds": 3.6,
                  "name": {
                     "value": "Philadelphia 76ers -17.5",
                     "sign": "mi/OjQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "-17.5",
                  "numerator": 13,
                  "denominator": 5,
                  "americanOdds": 260
               }
            ],
            "templateId": 8098,
            "categoryId": 44,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "balanced": 1,
            "spread": 2.3,
            "category": "Gridable",
            "templateCategory": {
               "id": 44,
               "name": {
                  "value": "Spread",
                  "sign": "zvC8fQ=="
               },
               "category": "Gridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [
                  "1n91ubk9k"
               ],
               "detailed": [
                  {
                     "group": 0,
                     "index": 7,
                     "subIndex": 0,
                     "marketTabId": 8,
                     "name": "Spread",
                     "displayType": "Spread"
                  },
                  {
                     "group": 2,
                     "index": 1,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Spread",
                     "displayType": "Spread"
                  }
               ],
               "parameters": {}
            }
         },
         {
            "id": 561387866,
            "name": {
               "value": "Spread",
               "sign": "DiF2jw=="
            },
            "results": [
               {
                  "id": 1630328982,
                  "odds": 1.35,
                  "name": {
                     "value": "Charlotte Hornets +16.5",
                     "sign": "T7971g==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "+16.5",
                  "numerator": 7,
                  "denominator": 20,
                  "americanOdds": -275
               },
               {
                  "id": 1630328983,
                  "odds": 3.25,
                  "name": {
                     "value": "Philadelphia 76ers -16.5",
                     "sign": "vSR12Q==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "-16.5",
                  "numerator": 9,
                  "denominator": 4,
                  "americanOdds": 225
               }
            ],
            "templateId": 8098,
            "categoryId": 44,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "balanced": 1,
            "spread": 1.9,
            "category": "Gridable",
            "templateCategory": {
               "id": 44,
               "name": {
                  "value": "Spread",
                  "sign": "DiF2jw=="
               },
               "category": "Gridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [
                  "1n91ubk9k"
               ],
               "detailed": [
                  {
                     "group": 0,
                     "index": 7,
                     "subIndex": 0,
                     "marketTabId": 8,
                     "name": "Spread",
                     "displayType": "Spread"
                  },
                  {
                     "group": 2,
                     "index": 1,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Spread",
                     "displayType": "Spread"
                  }
               ],
               "parameters": {}
            }
         },
         {
            "id": 561387869,
            "name": {
               "value": "Spread",
               "sign": "Th+v6g=="
            },
            "results": [
               {
                  "id": 1630328988,
                  "odds": 1.4,
                  "name": {
                     "value": "Charlotte Hornets +15.5",
                     "sign": "z0H0UA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "+15.5",
                  "numerator": 2,
                  "denominator": 5,
                  "americanOdds": -250
               },
               {
                  "id": 1630328989,
                  "odds": 3.0,
                  "name": {
                     "value": "Philadelphia 76ers -15.5",
                     "sign": "1Dm4JA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "-15.5",
                  "numerator": 2,
                  "denominator": 1,
                  "americanOdds": 200
               }
            ],
            "templateId": 8098,
            "categoryId": 44,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "balanced": 1,
            "spread": 1.6,
            "category": "Gridable",
            "templateCategory": {
               "id": 44,
               "name": {
                  "value": "Spread",
                  "sign": "Th+v6g=="
               },
               "category": "Gridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [
                  "1n91ubk9k"
               ],
               "detailed": [
                  {
                     "group": 0,
                     "index": 7,
                     "subIndex": 0,
                     "marketTabId": 8,
                     "name": "Spread",
                     "displayType": "Spread"
                  },
                  {
                     "group": 2,
                     "index": 1,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Spread",
                     "displayType": "Spread"
                  }
               ],
               "parameters": {}
            }
         },
         {
            "id": 561387872,
            "name": {
               "value": "Spread",
               "sign": "DBYNOQ=="
            },
            "results": [
               {
                  "id": 1630328994,
                  "odds": 1.45,
                  "name": {
                     "value": "Charlotte Hornets +14.5",
                     "sign": "/rJxYA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "+14.5",
                  "numerator": 5,
                  "denominator": 11,
                  "americanOdds": -225
               },
               {
                  "id": 1630328995,
                  "odds": 2.8,
                  "name": {
                     "value": "Philadelphia 76ers -14.5",
                     "sign": "T7EozA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "-14.5",
                  "numerator": 9,
                  "denominator": 5,
                  "americanOdds": 180
               }
            ],
            "templateId": 8098,
            "categoryId": 44,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "balanced": 1,
            "spread": 1.35,
            "category": "Gridable",
            "templateCategory": {
               "id": 44,
               "name": {
                  "value": "Spread",
                  "sign": "DBYNOQ=="
               },
               "category": "Gridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [
                  "1n91ubk9k"
               ],
               "detailed": [
                  {
                     "group": 0,
                     "index": 7,
                     "subIndex": 0,
                     "marketTabId": 8,
                     "name": "Spread",
                     "displayType": "Spread"
                  },
                  {
                     "group": 2,
                     "index": 1,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Spread",
                     "displayType": "Spread"
                  }
               ],
               "parameters": {}
            }
         },
         {
            "id": 561387876,
            "name": {
               "value": "Spread",
               "sign": "DIWsZQ=="
            },
            "results": [
               {
                  "id": 1630329002,
                  "odds": 1.53,
                  "name": {
                     "value": "Charlotte Hornets +13.5",
                     "sign": "8PPlfA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "+13.5",
                  "numerator": 8,
                  "denominator": 15,
                  "americanOdds": -190
               },
               {
                  "id": 1630329003,
                  "odds": 2.55,
                  "name": {
                     "value": "Philadelphia 76ers -13.5",
                     "sign": "NSYUsw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "-13.5",
                  "numerator": 31,
                  "denominator": 20,
                  "americanOdds": 155
               }
            ],
            "templateId": 8098,
            "categoryId": 44,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "balanced": 1,
            "spread": 1.02,
            "category": "Gridable",
            "templateCategory": {
               "id": 44,
               "name": {
                  "value": "Spread",
                  "sign": "DIWsZQ=="
               },
               "category": "Gridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [
                  "1n91ubk9k"
               ],
               "detailed": [
                  {
                     "group": 0,
                     "index": 7,
                     "subIndex": 0,
                     "marketTabId": 8,
                     "name": "Spread",
                     "displayType": "Spread"
                  },
                  {
                     "group": 2,
                     "index": 1,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Spread",
                     "displayType": "Spread"
                  }
               ],
               "parameters": {}
            }
         },
         {
            "id": 561387880,
            "name": {
               "value": "Spread",
               "sign": "DDBOgA=="
            },
            "results": [
               {
                  "id": 1630329010,
                  "odds": 1.6,
                  "name": {
                     "value": "Charlotte Hornets +12.5",
                     "sign": "WmFKVw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "+12.5",
                  "numerator": 3,
                  "denominator": 5,
                  "americanOdds": -165
               },
               {
                  "id": 1630329011,
                  "odds": 2.4,
                  "name": {
                     "value": "Philadelphia 76ers -12.5",
                     "sign": "A85CPA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "-12.5",
                  "numerator": 7,
                  "denominator": 5,
                  "americanOdds": 140
               }
            ],
            "templateId": 8098,
            "categoryId": 44,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "balanced": 1,
            "spread": 0.8,
            "category": "Gridable",
            "templateCategory": {
               "id": 44,
               "name": {
                  "value": "Spread",
                  "sign": "DDBOgA=="
               },
               "category": "Gridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [
                  "1n91ubk9k"
               ],
               "detailed": [
                  {
                     "group": 0,
                     "index": 7,
                     "subIndex": 0,
                     "marketTabId": 8,
                     "name": "Spread",
                     "displayType": "Spread"
                  },
                  {
                     "group": 2,
                     "index": 1,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Spread",
                     "displayType": "Spread"
                  }
               ],
               "parameters": {}
            }
         },
         {
            "id": 561387883,
            "name": {
               "value": "Spread",
               "sign": "TJ02uQ=="
            },
            "results": [
               {
                  "id": 1630329016,
                  "odds": 1.67,
                  "name": {
                     "value": "Charlotte Hornets +11.5",
                     "sign": "2p/F0Q==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "+11.5",
                  "numerator": 4,
                  "denominator": 6,
                  "americanOdds": -150
               },
               {
                  "id": 1630329017,
                  "odds": 2.25,
                  "name": {
                     "value": "Philadelphia 76ers -11.5",
                     "sign": "atOPwQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "-11.5",
                  "numerator": 5,
                  "denominator": 4,
                  "americanOdds": 125
               }
            ],
            "templateId": 8098,
            "categoryId": 44,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "balanced": 1,
            "spread": 0.58,
            "category": "Gridable",
            "templateCategory": {
               "id": 44,
               "name": {
                  "value": "Spread",
                  "sign": "TJ02uQ=="
               },
               "category": "Gridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [
                  "1n91ubk9k"
               ],
               "detailed": [
                  {
                     "group": 0,
                     "index": 7,
                     "subIndex": 0,
                     "marketTabId": 8,
                     "name": "Spread",
                     "displayType": "Spread"
                  },
                  {
                     "group": 2,
                     "index": 1,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Spread",
                     "displayType": "Spread"
                  }
               ],
               "parameters": {}
            }
         },
         {
            "id": 561387886,
            "name": {
               "value": "Spread",
               "sign": "jGq/8g=="
            },
            "results": [
               {
                  "id": 1630329022,
                  "odds": 1.78,
                  "name": {
                     "value": "Charlotte Hornets +10.5",
                     "sign": "WspArA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "+10.5",
                  "numerator": 4,
                  "denominator": 5,
                  "americanOdds": -130
               },
               {
                  "id": 1630329023,
                  "odds": 2.05,
                  "name": {
                     "value": "Philadelphia 76ers -10.5",
                     "sign": "Tdg0lQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "-10.5",
                  "numerator": 21,
                  "denominator": 20,
                  "americanOdds": 105
               }
            ],
            "templateId": 8098,
            "categoryId": 44,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "balanced": 1,
            "spread": 0.27,
            "category": "Gridable",
            "templateCategory": {
               "id": 44,
               "name": {
                  "value": "Spread",
                  "sign": "jGq/8g=="
               },
               "category": "Gridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [
                  "1n91ubk9k"
               ],
               "detailed": [
                  {
                     "group": 0,
                     "index": 7,
                     "subIndex": 0,
                     "marketTabId": 8,
                     "name": "Spread",
                     "displayType": "Spread"
                  },
                  {
                     "group": 2,
                     "index": 1,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Spread",
                     "displayType": "Spread"
                  }
               ],
               "parameters": {}
            }
         },
         {
            "id": 561387889,
            "name": {
               "value": "Spread",
               "sign": "jTjShw=="
            },
            "results": [
               {
                  "id": 1630329028,
                  "odds": 1.91,
                  "name": {
                     "value": "Charlotte Hornets +9.5",
                     "sign": "IR6Rag==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "+9.5",
                  "numerator": 10,
                  "denominator": 11,
                  "americanOdds": -110
               },
               {
                  "id": 1630329029,
                  "odds": 1.91,
                  "name": {
                     "value": "Philadelphia 76ers -9.5",
                     "sign": "6xUHyg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "-9.5",
                  "numerator": 10,
                  "denominator": 11,
                  "americanOdds": -110
               }
            ],
            "templateId": 8098,
            "categoryId": 44,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "balanced": 1,
            "spread": 0.0,
            "category": "Gridable",
            "templateCategory": {
               "id": 44,
               "name": {
                  "value": "Spread",
                  "sign": "jTjShw=="
               },
               "category": "Gridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [
                  "1n91ubk9k"
               ],
               "detailed": [
                  {
                     "group": 0,
                     "index": 7,
                     "subIndex": 0,
                     "marketTabId": 8,
                     "name": "Spread",
                     "displayType": "Spread"
                  },
                  {
                     "group": 2,
                     "index": 1,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Spread",
                     "displayType": "Spread"
                  }
               ],
               "parameters": {}
            }
         },
         {
            "id": 561387892,
            "name": {
               "value": "Spread",
               "sign": "Tc9bzA=="
            },
            "results": [
               {
                  "id": 1630329034,
                  "odds": 2.05,
                  "name": {
                     "value": "Charlotte Hornets +8.5",
                     "sign": "To7iRw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "+8.5",
                  "numerator": 21,
                  "denominator": 20,
                  "americanOdds": 105
               },
               {
                  "id": 1630329035,
                  "odds": 1.78,
                  "name": {
                     "value": "Philadelphia 76ers -8.5",
                     "sign": "4BdZrg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "-8.5",
                  "numerator": 4,
                  "denominator": 5,
                  "americanOdds": -130
               }
            ],
            "templateId": 8098,
            "categoryId": 44,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "balanced": 1,
            "spread": 0.27,
            "category": "Gridable",
            "templateCategory": {
               "id": 44,
               "name": {
                  "value": "Spread",
                  "sign": "Tc9bzA=="
               },
               "category": "Gridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [
                  "1n91ubk9k"
               ],
               "detailed": [
                  {
                     "group": 0,
                     "index": 7,
                     "subIndex": 0,
                     "marketTabId": 8,
                     "name": "Spread",
                     "displayType": "Spread"
                  },
                  {
                     "group": 2,
                     "index": 1,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Spread",
                     "displayType": "Spread"
                  }
               ],
               "parameters": {}
            }
         },
         {
            "id": 561387895,
            "name": {
               "value": "Spread",
               "sign": "DWIj9Q=="
            },
            "results": [
               {
                  "id": 1630329040,
                  "odds": 2.25,
                  "name": {
                     "value": "Charlotte Hornets +7.5",
                     "sign": "DT0Njg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "+7.5",
                  "numerator": 5,
                  "denominator": 4,
                  "americanOdds": 125
               },
               {
                  "id": 1630329041,
                  "odds": 1.67,
                  "name": {
                     "value": "Philadelphia 76ers -7.5",
                     "sign": "Er96Eg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "-7.5",
                  "numerator": 4,
                  "denominator": 6,
                  "americanOdds": -150
               }
            ],
            "templateId": 8098,
            "categoryId": 44,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "balanced": 1,
            "spread": 0.58,
            "category": "Gridable",
            "templateCategory": {
               "id": 44,
               "name": {
                  "value": "Spread",
                  "sign": "DWIj9Q=="
               },
               "category": "Gridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [
                  "1n91ubk9k"
               ],
               "detailed": [
                  {
                     "group": 0,
                     "index": 7,
                     "subIndex": 0,
                     "marketTabId": 8,
                     "name": "Spread",
                     "displayType": "Spread"
                  },
                  {
                     "group": 2,
                     "index": 1,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Spread",
                     "displayType": "Spread"
                  }
               ],
               "parameters": {}
            }
         },
         {
            "id": 561387898,
            "name": {
               "value": "Spread",
               "sign": "zbPpBw=="
            },
            "results": [
               {
                  "id": 1630329046,
                  "odds": 2.5,
                  "name": {
                     "value": "Charlotte Hornets +6.5",
                     "sign": "9FcfFA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "+6.5",
                  "numerator": 6,
                  "denominator": 4,
                  "americanOdds": 150
               },
               {
                  "id": 1630329047,
                  "odds": 1.55,
                  "name": {
                     "value": "Philadelphia 76ers -6.5",
                     "sign": "kur/bw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "-6.5",
                  "numerator": 11,
                  "denominator": 20,
                  "americanOdds": -185
               }
            ],
            "templateId": 8098,
            "categoryId": 44,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "balanced": 1,
            "spread": 0.95,
            "category": "Gridable",
            "templateCategory": {
               "id": 44,
               "name": {
                  "value": "Spread",
                  "sign": "zbPpBw=="
               },
               "category": "Gridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [
                  "1n91ubk9k"
               ],
               "detailed": [
                  {
                     "group": 0,
                     "index": 7,
                     "subIndex": 0,
                     "marketTabId": 8,
                     "name": "Spread",
                     "displayType": "Spread"
                  },
                  {
                     "group": 2,
                     "index": 1,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Spread",
                     "displayType": "Spread"
                  }
               ],
               "parameters": {}
            }
         },
         {
            "id": 561387901,
            "name": {
               "value": "Spread",
               "sign": "jY0wYg=="
            },
            "results": [
               {
                  "id": 1630329052,
                  "odds": 2.7,
                  "name": {
                     "value": "Charlotte Hornets +5.5",
                     "sign": "vu5YYQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "+5.5",
                  "numerator": 17,
                  "denominator": 10,
                  "americanOdds": 170
               },
               {
                  "id": 1630329053,
                  "odds": 1.48,
                  "name": {
                     "value": "Philadelphia 76ers -5.5",
                     "sign": "EhRw6Q==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "-5.5",
                  "numerator": 10,
                  "denominator": 21,
                  "americanOdds": -200
               }
            ],
            "templateId": 8098,
            "categoryId": 44,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "balanced": 1,
            "spread": 1.22,
            "category": "Gridable",
            "templateCategory": {
               "id": 44,
               "name": {
                  "value": "Spread",
                  "sign": "jY0wYg=="
               },
               "category": "Gridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [
                  "1n91ubk9k"
               ],
               "detailed": [
                  {
                     "group": 0,
                     "index": 7,
                     "subIndex": 0,
                     "marketTabId": 8,
                     "name": "Spread",
                     "displayType": "Spread"
                  },
                  {
                     "group": 2,
                     "index": 1,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Spread",
                     "displayType": "Spread"
                  }
               ],
               "parameters": {}
            }
         },
         {
            "id": 561387904,
            "name": {
               "value": "Spread",
               "sign": "x+YxNA=="
            },
            "results": [
               {
                  "id": 1630329058,
                  "odds": 3.0,
                  "name": {
                     "value": "Charlotte Hornets +4.5",
                     "sign": "J22NSQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "+4.5",
                  "numerator": 2,
                  "denominator": 1,
                  "americanOdds": 200
               },
               {
                  "id": 1630329059,
                  "odds": 1.4,
                  "name": {
                     "value": "Philadelphia 76ers -4.5",
                     "sign": "I+f12Q==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "-4.5",
                  "numerator": 2,
                  "denominator": 5,
                  "americanOdds": -250
               }
            ],
            "templateId": 8098,
            "categoryId": 44,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "balanced": 1,
            "spread": 1.6,
            "category": "Gridable",
            "templateCategory": {
               "id": 44,
               "name": {
                  "value": "Spread",
                  "sign": "x+YxNA=="
               },
               "category": "Gridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [
                  "1n91ubk9k"
               ],
               "detailed": [
                  {
                     "group": 0,
                     "index": 7,
                     "subIndex": 0,
                     "marketTabId": 8,
                     "name": "Spread",
                     "displayType": "Spread"
                  },
                  {
                     "group": 2,
                     "index": 1,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Spread",
                     "displayType": "Spread"
                  }
               ],
               "parameters": {}
            }
         },
         {
            "id": 561387907,
            "name": {
               "value": "Spread",
               "sign": "h0tJDQ=="
            },
            "results": [
               {
                  "id": 1630329064,
                  "odds": 3.3,
                  "name": {
                     "value": "Charlotte Hornets +3.5",
                     "sign": "sXzDOw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "+3.5",
                  "numerator": 23,
                  "denominator": 10,
                  "americanOdds": 230
               },
               {
                  "id": 1630329065,
                  "odds": 1.34,
                  "name": {
                     "value": "Philadelphia 76ers -3.5",
                     "sign": "f7FzWA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "-3.5",
                  "numerator": 1,
                  "denominator": 3,
                  "americanOdds": -300
               }
            ],
            "templateId": 8098,
            "categoryId": 44,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "balanced": 1,
            "spread": 1.96,
            "category": "Gridable",
            "templateCategory": {
               "id": 44,
               "name": {
                  "value": "Spread",
                  "sign": "h0tJDQ=="
               },
               "category": "Gridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [
                  "1n91ubk9k"
               ],
               "detailed": [
                  {
                     "group": 0,
                     "index": 7,
                     "subIndex": 0,
                     "marketTabId": 8,
                     "name": "Spread",
                     "displayType": "Spread"
                  },
                  {
                     "group": 2,
                     "index": 1,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Spread",
                     "displayType": "Spread"
                  }
               ],
               "parameters": {}
            }
         },
         {
            "id": 561387911,
            "name": {
               "value": "Spread",
               "sign": "h9joUQ=="
            },
            "results": [
               {
                  "id": 1630329072,
                  "odds": 3.6,
                  "name": {
                     "value": "Charlotte Hornets +2.5",
                     "sign": "fR/SOA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "+2.5",
                  "numerator": 13,
                  "denominator": 5,
                  "americanOdds": 260
               },
               {
                  "id": 1630329073,
                  "odds": 1.3,
                  "name": {
                     "value": "Philadelphia 76ers -2.5",
                     "sign": "1SPccw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "-2.5",
                  "numerator": 3,
                  "denominator": 10,
                  "americanOdds": -350
               }
            ],
            "templateId": 8098,
            "categoryId": 44,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "balanced": 1,
            "spread": 2.3,
            "category": "Gridable",
            "templateCategory": {
               "id": 44,
               "name": {
                  "value": "Spread",
                  "sign": "h9joUQ=="
               },
               "category": "Gridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [
                  "1n91ubk9k"
               ],
               "detailed": [
                  {
                     "group": 0,
                     "index": 7,
                     "subIndex": 0,
                     "marketTabId": 8,
                     "name": "Spread",
                     "displayType": "Spread"
                  },
                  {
                     "group": 2,
                     "index": 1,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Spread",
                     "displayType": "Spread"
                  }
               ],
               "parameters": {}
            }
         },
         {
            "id": 561387914,
            "name": {
               "value": "Spread",
               "sign": "Rwkiow=="
            },
            "results": [
               {
                  "id": 1630329078,
                  "odds": 4.1,
                  "name": {
                     "value": "Charlotte Hornets +1.5",
                     "sign": "6qFEoQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "+1.5",
                  "numerator": 31,
                  "denominator": 10,
                  "americanOdds": 310
               },
               {
                  "id": 1630329079,
                  "odds": 1.25,
                  "name": {
                     "value": "Philadelphia 76ers -1.5",
                     "sign": "O6LdDQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "-1.5",
                  "numerator": 1,
                  "denominator": 4,
                  "americanOdds": -400
               }
            ],
            "templateId": 8098,
            "categoryId": 44,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "balanced": 1,
            "spread": 2.85,
            "category": "Gridable",
            "templateCategory": {
               "id": 44,
               "name": {
                  "value": "Spread",
                  "sign": "Rwkiow=="
               },
               "category": "Gridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [
                  "1n91ubk9k"
               ],
               "detailed": [
                  {
                     "group": 0,
                     "index": 7,
                     "subIndex": 0,
                     "marketTabId": 8,
                     "name": "Spread",
                     "displayType": "Spread"
                  },
                  {
                     "group": 2,
                     "index": 1,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Spread",
                     "displayType": "Spread"
                  }
               ],
               "parameters": {}
            }
         },
         {
            "id": 561387915,
            "name": {
               "value": "Spread",
               "sign": "h20KtA=="
            },
            "results": [
               {
                  "id": 1630329080,
                  "odds": 4.75,
                  "name": {
                     "value": "Charlotte Hornets -1.5",
                     "sign": "bgSeqA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "-1.5",
                  "numerator": 15,
                  "denominator": 4,
                  "americanOdds": 375
               },
               {
                  "id": 1630329081,
                  "odds": 1.2,
                  "name": {
                     "value": "Philadelphia 76ers +1.5",
                     "sign": "25UqTQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "+1.5",
                  "numerator": 1,
                  "denominator": 5,
                  "americanOdds": -500
               }
            ],
            "templateId": 8098,
            "categoryId": 44,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "balanced": 1,
            "spread": 3.55,
            "category": "Gridable",
            "templateCategory": {
               "id": 44,
               "name": {
                  "value": "Spread",
                  "sign": "h20KtA=="
               },
               "category": "Gridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [
                  "1n91ubk9k"
               ],
               "detailed": [
                  {
                     "group": 0,
                     "index": 7,
                     "subIndex": 0,
                     "marketTabId": 8,
                     "name": "Spread",
                     "displayType": "Spread"
                  },
                  {
                     "group": 2,
                     "index": 1,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Spread",
                     "displayType": "Spread"
                  }
               ],
               "parameters": {}
            }
         },
         {
            "id": 561387918,
            "name": {
               "value": "Spread",
               "sign": "R5qD/w=="
            },
            "results": [
               {
                  "id": 1630329086,
                  "odds": 5.25,
                  "name": {
                     "value": "Charlotte Hornets -2.5",
                     "sign": "+boIMQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "-2.5",
                  "numerator": 17,
                  "denominator": 4,
                  "americanOdds": 425
               },
               {
                  "id": 1630329087,
                  "odds": 1.17,
                  "name": {
                     "value": "Philadelphia 76ers +2.5",
                     "sign": "NRQrMw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "+2.5",
                  "numerator": 1,
                  "denominator": 6,
                  "americanOdds": -600
               }
            ],
            "templateId": 8098,
            "categoryId": 44,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "balanced": 1,
            "spread": 4.08,
            "category": "Gridable",
            "templateCategory": {
               "id": 44,
               "name": {
                  "value": "Spread",
                  "sign": "R5qD/w=="
               },
               "category": "Gridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [
                  "1n91ubk9k"
               ],
               "detailed": [
                  {
                     "group": 0,
                     "index": 7,
                     "subIndex": 0,
                     "marketTabId": 8,
                     "name": "Spread",
                     "displayType": "Spread"
                  },
                  {
                     "group": 2,
                     "index": 1,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Spread",
                     "displayType": "Spread"
                  }
               ],
               "parameters": {}
            }
         },
         {
            "id": 561387921,
            "name": {
               "value": "Totals",
               "sign": "J/REYQ=="
            },
            "results": [
               {
                  "id": 1630329092,
                  "odds": 1.4,
                  "name": {
                     "value": "Over 209.5",
                     "sign": "oUS41g==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Over",
                  "numerator": 2,
                  "denominator": 5,
                  "americanOdds": -250
               },
               {
                  "id": 1630329093,
                  "odds": 3.0,
                  "name": {
                     "value": "Under 209.5",
                     "sign": "fFPcHw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Under",
                  "numerator": 2,
                  "denominator": 1,
                  "americanOdds": 200
               }
            ],
            "templateId": 11577,
            "categoryId": 48,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "balanced": 1,
            "attr": "209.5",
            "spread": 1.6,
            "category": "Gridable",
            "templateCategory": {
               "id": 48,
               "name": {
                  "value": "Totals",
                  "sign": "J/REYQ=="
               },
               "category": "Gridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [
                  "x69gzhrb4"
               ],
               "detailed": [
                  {
                     "group": 0,
                     "index": 8,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Totals",
                     "displayType": "OverUnder"
                  },
                  {
                     "group": 1,
                     "index": 0,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Totals",
                     "displayType": "OverUnder"
                  }
               ],
               "parameters": {
                  "marketType": "OverUnder"
               }
            }
         },
         {
            "id": 561387924,
            "name": {
               "value": "Totals",
               "sign": "5wPNKg=="
            },
            "results": [
               {
                  "id": 1630329098,
                  "odds": 1.44,
                  "name": {
                     "value": "Over 210.5",
                     "sign": "RugiCw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Over",
                  "numerator": 4,
                  "denominator": 9,
                  "americanOdds": -225
               },
               {
                  "id": 1630329099,
                  "odds": 2.85,
                  "name": {
                     "value": "Under 210.5",
                     "sign": "24Rq1g==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Under",
                  "numerator": 15,
                  "denominator": 8,
                  "americanOdds": 185
               }
            ],
            "templateId": 11577,
            "categoryId": 48,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "balanced": 1,
            "attr": "210.5",
            "spread": 1.41,
            "category": "Gridable",
            "templateCategory": {
               "id": 48,
               "name": {
                  "value": "Totals",
                  "sign": "5wPNKg=="
               },
               "category": "Gridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [
                  "x69gzhrb4"
               ],
               "detailed": [
                  {
                     "group": 0,
                     "index": 8,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Totals",
                     "displayType": "OverUnder"
                  },
                  {
                     "group": 1,
                     "index": 0,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Totals",
                     "displayType": "OverUnder"
                  }
               ],
               "parameters": {
                  "marketType": "OverUnder"
               }
            }
         },
         {
            "id": 561387927,
            "name": {
               "value": "Totals",
               "sign": "p661Ew=="
            },
            "results": [
               {
                  "id": 1630329104,
                  "odds": 1.48,
                  "name": {
                     "value": "Over 211.5",
                     "sign": "2r/R4A==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Over",
                  "numerator": 10,
                  "denominator": 21,
                  "americanOdds": -200
               },
               {
                  "id": 1630329105,
                  "odds": 2.7,
                  "name": {
                     "value": "Under 211.5",
                     "sign": "saVGlg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Under",
                  "numerator": 17,
                  "denominator": 10,
                  "americanOdds": 170
               }
            ],
            "templateId": 11577,
            "categoryId": 48,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "balanced": 1,
            "attr": "211.5",
            "spread": 1.22,
            "category": "Gridable",
            "templateCategory": {
               "id": 48,
               "name": {
                  "value": "Totals",
                  "sign": "p661Ew=="
               },
               "category": "Gridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [
                  "x69gzhrb4"
               ],
               "detailed": [
                  {
                     "group": 0,
                     "index": 8,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Totals",
                     "displayType": "OverUnder"
                  },
                  {
                     "group": 1,
                     "index": 0,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Totals",
                     "displayType": "OverUnder"
                  }
               ],
               "parameters": {
                  "marketType": "OverUnder"
               }
            }
         },
         {
            "id": 561387930,
            "name": {
               "value": "Totals",
               "sign": "Z39/4Q=="
            },
            "results": [
               {
                  "id": 1630329110,
                  "odds": 1.53,
                  "name": {
                     "value": "Over 212.5",
                     "sign": "MtxJ3Q==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Over",
                  "numerator": 8,
                  "denominator": 15,
                  "americanOdds": -190
               },
               {
                  "id": 1630329111,
                  "odds": 2.55,
                  "name": {
                     "value": "Under 212.5",
                     "sign": "L0ZZKA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Under",
                  "numerator": 31,
                  "denominator": 20,
                  "americanOdds": 155
               }
            ],
            "templateId": 11577,
            "categoryId": 48,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "balanced": 1,
            "attr": "212.5",
            "spread": 1.02,
            "category": "Gridable",
            "templateCategory": {
               "id": 48,
               "name": {
                  "value": "Totals",
                  "sign": "Z39/4Q=="
               },
               "category": "Gridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [
                  "x69gzhrb4"
               ],
               "detailed": [
                  {
                     "group": 0,
                     "index": 8,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Totals",
                     "displayType": "OverUnder"
                  },
                  {
                     "group": 1,
                     "index": 0,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Totals",
                     "displayType": "OverUnder"
                  }
               ],
               "parameters": {
                  "marketType": "OverUnder"
               }
            }
         },
         {
            "id": 561387933,
            "name": {
               "value": "Totals",
               "sign": "J0GmhA=="
            },
            "results": [
               {
                  "id": 1630329116,
                  "odds": 1.57,
                  "name": {
                     "value": "Over 213.5",
                     "sign": "1tDonA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Over",
                  "numerator": 4,
                  "denominator": 7,
                  "americanOdds": -175
               },
               {
                  "id": 1630329117,
                  "odds": 2.45,
                  "name": {
                     "value": "Under 213.5",
                     "sign": "EMwBNg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Under",
                  "numerator": 29,
                  "denominator": 20,
                  "americanOdds": 145
               }
            ],
            "templateId": 11577,
            "categoryId": 48,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "balanced": 1,
            "attr": "213.5",
            "spread": 0.88,
            "category": "Gridable",
            "templateCategory": {
               "id": 48,
               "name": {
                  "value": "Totals",
                  "sign": "J0GmhA=="
               },
               "category": "Gridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [
                  "x69gzhrb4"
               ],
               "detailed": [
                  {
                     "group": 0,
                     "index": 8,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Totals",
                     "displayType": "OverUnder"
                  },
                  {
                     "group": 1,
                     "index": 0,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Totals",
                     "displayType": "OverUnder"
                  }
               ],
               "parameters": {
                  "marketType": "OverUnder"
               }
            }
         },
         {
            "id": 561387936,
            "name": {
               "value": "Totals",
               "sign": "ZUgEVw=="
            },
            "results": [
               {
                  "id": 1630329122,
                  "odds": 1.62,
                  "name": {
                     "value": "Over 214.5",
                     "sign": "l93X1g==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Over",
                  "numerator": 8,
                  "denominator": 13,
                  "americanOdds": -160
               },
               {
                  "id": 1630329123,
                  "odds": 2.35,
                  "name": {
                     "value": "Under 214.5",
                     "sign": "J6wIrw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Under",
                  "numerator": 27,
                  "denominator": 20,
                  "americanOdds": 135
               }
            ],
            "templateId": 11577,
            "categoryId": 48,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "balanced": 1,
            "attr": "214.5",
            "spread": 0.73,
            "category": "Gridable",
            "templateCategory": {
               "id": 48,
               "name": {
                  "value": "Totals",
                  "sign": "ZUgEVw=="
               },
               "category": "Gridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [
                  "x69gzhrb4"
               ],
               "detailed": [
                  {
                     "group": 0,
                     "index": 8,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Totals",
                     "displayType": "OverUnder"
                  },
                  {
                     "group": 1,
                     "index": 0,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Totals",
                     "displayType": "OverUnder"
                  }
               ],
               "parameters": {
                  "marketType": "OverUnder"
               }
            }
         },
         {
            "id": 561387939,
            "name": {
               "value": "Totals",
               "sign": "JeV8bg=="
            },
            "results": [
               {
                  "id": 1630329128,
                  "odds": 1.67,
                  "name": {
                     "value": "Over 215.5",
                     "sign": "c9F2lw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Over",
                  "numerator": 4,
                  "denominator": 6,
                  "americanOdds": -150
               },
               {
                  "id": 1630329129,
                  "odds": 2.25,
                  "name": {
                     "value": "Under 215.5",
                     "sign": "GCZQsQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Under",
                  "numerator": 5,
                  "denominator": 4,
                  "americanOdds": 125
               }
            ],
            "templateId": 11577,
            "categoryId": 48,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "balanced": 1,
            "attr": "215.5",
            "spread": 0.58,
            "category": "Gridable",
            "templateCategory": {
               "id": 48,
               "name": {
                  "value": "Totals",
                  "sign": "JeV8bg=="
               },
               "category": "Gridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [
                  "x69gzhrb4"
               ],
               "detailed": [
                  {
                     "group": 0,
                     "index": 8,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Totals",
                     "displayType": "OverUnder"
                  },
                  {
                     "group": 1,
                     "index": 0,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Totals",
                     "displayType": "OverUnder"
                  }
               ],
               "parameters": {
                  "marketType": "OverUnder"
               }
            }
         },
         {
            "id": 561387942,
            "name": {
               "value": "Totals",
               "sign": "5RL1JQ=="
            },
            "results": [
               {
                  "id": 1630329134,
                  "odds": 1.72,
                  "name": {
                     "value": "Over 216.5",
                     "sign": "m7Luqg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Over",
                  "numerator": 8,
                  "denominator": 11,
                  "americanOdds": -140
               },
               {
                  "id": 1630329135,
                  "odds": 2.15,
                  "name": {
                     "value": "Under 216.5",
                     "sign": "hsVPDw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Under",
                  "numerator": 23,
                  "denominator": 20,
                  "americanOdds": 115
               }
            ],
            "templateId": 11577,
            "categoryId": 48,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "balanced": 1,
            "attr": "216.5",
            "spread": 0.43,
            "category": "Gridable",
            "templateCategory": {
               "id": 48,
               "name": {
                  "value": "Totals",
                  "sign": "5RL1JQ=="
               },
               "category": "Gridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [
                  "x69gzhrb4"
               ],
               "detailed": [
                  {
                     "group": 0,
                     "index": 8,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Totals",
                     "displayType": "OverUnder"
                  },
                  {
                     "group": 1,
                     "index": 0,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Totals",
                     "displayType": "OverUnder"
                  }
               ],
               "parameters": {
                  "marketType": "OverUnder"
               }
            }
         },
         {
            "id": 561387945,
            "name": {
               "value": "Totals",
               "sign": "pQpv+Q=="
            },
            "results": [
               {
                  "id": 1630329140,
                  "odds": 1.8,
                  "name": {
                     "value": "Over 217.5",
                     "sign": "B+UdQQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Over",
                  "numerator": 4,
                  "denominator": 5,
                  "americanOdds": -125
               },
               {
                  "id": 1630329141,
                  "odds": 2.05,
                  "name": {
                     "value": "Under 217.5",
                     "sign": "7ORjTw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Under",
                  "numerator": 21,
                  "denominator": 20,
                  "americanOdds": 105
               }
            ],
            "templateId": 11577,
            "categoryId": 48,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "balanced": 1,
            "attr": "217.5",
            "spread": 0.25,
            "category": "Gridable",
            "templateCategory": {
               "id": 48,
               "name": {
                  "value": "Totals",
                  "sign": "pQpv+Q=="
               },
               "category": "Gridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [
                  "x69gzhrb4"
               ],
               "detailed": [
                  {
                     "group": 0,
                     "index": 8,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Totals",
                     "displayType": "OverUnder"
                  },
                  {
                     "group": 1,
                     "index": 0,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Totals",
                     "displayType": "OverUnder"
                  }
               ],
               "parameters": {
                  "marketType": "OverUnder"
               }
            }
         },
         {
            "id": 561387948,
            "name": {
               "value": "Totals",
               "sign": "Zf3msg=="
            },
            "results": [
               {
                  "id": 1630329146,
                  "odds": 1.87,
                  "name": {
                     "value": "Over 218.5",
                     "sign": "N1K2IA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Over",
                  "numerator": 20,
                  "denominator": 23,
                  "americanOdds": -115
               },
               {
                  "id": 1630329147,
                  "odds": 1.95,
                  "name": {
                     "value": "Under 218.5",
                     "sign": "nCjkOg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Under",
                  "numerator": 19,
                  "denominator": 20,
                  "americanOdds": -105
               }
            ],
            "templateId": 11577,
            "categoryId": 48,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "balanced": 1,
            "attr": "218.5",
            "spread": 0.08,
            "category": "Gridable",
            "templateCategory": {
               "id": 48,
               "name": {
                  "value": "Totals",
                  "sign": "Zf3msg=="
               },
               "category": "Gridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [
                  "x69gzhrb4"
               ],
               "detailed": [
                  {
                     "group": 0,
                     "index": 8,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Totals",
                     "displayType": "OverUnder"
                  },
                  {
                     "group": 1,
                     "index": 0,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Totals",
                     "displayType": "OverUnder"
                  }
               ],
               "parameters": {
                  "marketType": "OverUnder"
               }
            }
         },
         {
            "id": 561387951,
            "name": {
               "value": "Totals",
               "sign": "JVCeiw=="
            },
            "results": [
               {
                  "id": 1630329152,
                  "odds": 1.95,
                  "name": {
                     "value": "Over 219.5",
                     "sign": "OdJKgA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Over",
                  "numerator": 19,
                  "denominator": 20,
                  "americanOdds": -105
               },
               {
                  "id": 1630329153,
                  "odds": 1.87,
                  "name": {
                     "value": "Under 219.5",
                     "sign": "SfSCZA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Under",
                  "numerator": 20,
                  "denominator": 23,
                  "americanOdds": -115
               }
            ],
            "templateId": 11577,
            "categoryId": 48,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "balanced": 1,
            "attr": "219.5",
            "spread": 0.08,
            "category": "Gridable",
            "templateCategory": {
               "id": 48,
               "name": {
                  "value": "Totals",
                  "sign": "JVCeiw=="
               },
               "category": "Gridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [
                  "x69gzhrb4"
               ],
               "detailed": [
                  {
                     "group": 0,
                     "index": 8,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Totals",
                     "displayType": "OverUnder"
                  },
                  {
                     "group": 1,
                     "index": 0,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Totals",
                     "displayType": "OverUnder"
                  }
               ],
               "parameters": {
                  "marketType": "OverUnder"
               }
            }
         },
         {
            "id": 561387954,
            "name": {
               "value": "Totals",
               "sign": "pMuj0A=="
            },
            "results": [
               {
                  "id": 1630329158,
                  "odds": 2.0,
                  "name": {
                     "value": "Over 220.5",
                     "sign": "6Zvwog==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Over",
                  "numerator": 1,
                  "denominator": 1,
                  "americanOdds": 100
               },
               {
                  "id": 1630329159,
                  "odds": 1.83,
                  "name": {
                     "value": "Under 220.5",
                     "sign": "7z2/xQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Under",
                  "numerator": 5,
                  "denominator": 6,
                  "americanOdds": -120
               }
            ],
            "templateId": 11577,
            "categoryId": 48,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "balanced": 1,
            "attr": "220.5",
            "spread": 0.17,
            "category": "Gridable",
            "templateCategory": {
               "id": 48,
               "name": {
                  "value": "Totals",
                  "sign": "pMuj0A=="
               },
               "category": "Gridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [
                  "x69gzhrb4"
               ],
               "detailed": [
                  {
                     "group": 0,
                     "index": 8,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Totals",
                     "displayType": "OverUnder"
                  },
                  {
                     "group": 1,
                     "index": 0,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Totals",
                     "displayType": "OverUnder"
                  }
               ],
               "parameters": {
                  "marketType": "OverUnder"
               }
            }
         },
         {
            "id": 561387957,
            "name": {
               "value": "Totals",
               "sign": "5PV6tQ=="
            },
            "results": [
               {
                  "id": 1630329164,
                  "odds": 2.1,
                  "name": {
                     "value": "Over 221.5",
                     "sign": "DZdR4w==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Over",
                  "numerator": 11,
                  "denominator": 10,
                  "americanOdds": 110
               },
               {
                  "id": 1630329165,
                  "odds": 1.75,
                  "name": {
                     "value": "Under 221.5",
                     "sign": "0Lfn2w==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Under",
                  "numerator": 3,
                  "denominator": 4,
                  "americanOdds": -135
               }
            ],
            "templateId": 11577,
            "categoryId": 48,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "balanced": 1,
            "attr": "221.5",
            "spread": 0.35,
            "category": "Gridable",
            "templateCategory": {
               "id": 48,
               "name": {
                  "value": "Totals",
                  "sign": "5PV6tQ=="
               },
               "category": "Gridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [
                  "x69gzhrb4"
               ],
               "detailed": [
                  {
                     "group": 0,
                     "index": 8,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Totals",
                     "displayType": "OverUnder"
                  },
                  {
                     "group": 1,
                     "index": 0,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Totals",
                     "displayType": "OverUnder"
                  }
               ],
               "parameters": {
                  "marketType": "OverUnder"
               }
            }
         },
         {
            "id": 561387960,
            "name": {
               "value": "Totals",
               "sign": "JCSwRw=="
            },
            "results": [
               {
                  "id": 1630329170,
                  "odds": 2.2,
                  "name": {
                     "value": "Over 222.5",
                     "sign": "IYKyIQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Over",
                  "numerator": 6,
                  "denominator": 5,
                  "americanOdds": 120
               },
               {
                  "id": 1630329171,
                  "odds": 1.7,
                  "name": {
                     "value": "Under 222.5",
                     "sign": "kSkO+Q==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Under",
                  "numerator": 7,
                  "denominator": 10,
                  "americanOdds": -145
               }
            ],
            "templateId": 11577,
            "categoryId": 48,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "balanced": 1,
            "attr": "222.5",
            "spread": 0.5,
            "category": "Gridable",
            "templateCategory": {
               "id": 48,
               "name": {
                  "value": "Totals",
                  "sign": "JCSwRw=="
               },
               "category": "Gridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [
                  "x69gzhrb4"
               ],
               "detailed": [
                  {
                     "group": 0,
                     "index": 8,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Totals",
                     "displayType": "OverUnder"
                  },
                  {
                     "group": 1,
                     "index": 0,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Totals",
                     "displayType": "OverUnder"
                  }
               ],
               "parameters": {
                  "marketType": "OverUnder"
               }
            }
         },
         {
            "id": 561387963,
            "name": {
               "value": "Totals",
               "sign": "ZInIfg=="
            },
            "results": [
               {
                  "id": 1630329176,
                  "odds": 2.25,
                  "name": {
                     "value": "Over 223.5",
                     "sign": "xY4TYA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Over",
                  "numerator": 5,
                  "denominator": 4,
                  "americanOdds": 125
               },
               {
                  "id": 1630329177,
                  "odds": 1.65,
                  "name": {
                     "value": "Under 223.5",
                     "sign": "rqNW5w==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Under",
                  "numerator": 13,
                  "denominator": 20,
                  "americanOdds": -155
               }
            ],
            "templateId": 11577,
            "categoryId": 48,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "balanced": 1,
            "attr": "223.5",
            "spread": 0.6,
            "category": "Gridable",
            "templateCategory": {
               "id": 48,
               "name": {
                  "value": "Totals",
                  "sign": "ZInIfg=="
               },
               "category": "Gridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [
                  "x69gzhrb4"
               ],
               "detailed": [
                  {
                     "group": 0,
                     "index": 8,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Totals",
                     "displayType": "OverUnder"
                  },
                  {
                     "group": 1,
                     "index": 0,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Totals",
                     "displayType": "OverUnder"
                  }
               ],
               "parameters": {
                  "marketType": "OverUnder"
               }
            }
         },
         {
            "id": 561387966,
            "name": {
               "value": "Totals",
               "sign": "pH5BNQ=="
            },
            "results": [
               {
                  "id": 1630329182,
                  "odds": 2.4,
                  "name": {
                     "value": "Over 224.5",
                     "sign": "8UWCWg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Over",
                  "numerator": 7,
                  "denominator": 5,
                  "americanOdds": 140
               },
               {
                  "id": 1630329183,
                  "odds": 1.6,
                  "name": {
                     "value": "Under 224.5",
                     "sign": "7OhAXg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Under",
                  "numerator": 3,
                  "denominator": 5,
                  "americanOdds": -165
               }
            ],
            "templateId": 11577,
            "categoryId": 48,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "balanced": 1,
            "attr": "224.5",
            "spread": 0.8,
            "category": "Gridable",
            "templateCategory": {
               "id": 48,
               "name": {
                  "value": "Totals",
                  "sign": "pH5BNQ=="
               },
               "category": "Gridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [
                  "x69gzhrb4"
               ],
               "detailed": [
                  {
                     "group": 0,
                     "index": 8,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Totals",
                     "displayType": "OverUnder"
                  },
                  {
                     "group": 1,
                     "index": 0,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Totals",
                     "displayType": "OverUnder"
                  }
               ],
               "parameters": {
                  "marketType": "OverUnder"
               }
            }
         },
         {
            "id": 561387969,
            "name": {
               "value": "Totals",
               "sign": "oZ39Ag=="
            },
            "results": [
               {
                  "id": 1630329188,
                  "odds": 2.5,
                  "name": {
                     "value": "Over 225.5",
                     "sign": "3KKkPg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Over",
                  "numerator": 6,
                  "denominator": 4,
                  "americanOdds": 150
               },
               {
                  "id": 1630329189,
                  "odds": 1.55,
                  "name": {
                     "value": "Under 225.5",
                     "sign": "LJ+Fog==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Under",
                  "numerator": 11,
                  "denominator": 20,
                  "americanOdds": -185
               }
            ],
            "templateId": 11577,
            "categoryId": 48,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "balanced": 1,
            "attr": "225.5",
            "spread": 0.95,
            "category": "Gridable",
            "templateCategory": {
               "id": 48,
               "name": {
                  "value": "Totals",
                  "sign": "oZ39Ag=="
               },
               "category": "Gridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [
                  "x69gzhrb4"
               ],
               "detailed": [
                  {
                     "group": 0,
                     "index": 8,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Totals",
                     "displayType": "OverUnder"
                  },
                  {
                     "group": 1,
                     "index": 0,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Totals",
                     "displayType": "OverUnder"
                  }
               ],
               "parameters": {
                  "marketType": "OverUnder"
               }
            }
         },
         {
            "id": 561387972,
            "name": {
               "value": "Totals",
               "sign": "YWp0SQ=="
            },
            "results": [
               {
                  "id": 1630329194,
                  "odds": 2.65,
                  "name": {
                     "value": "Over 226.5",
                     "sign": "iOwVVg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Over",
                  "numerator": 33,
                  "denominator": 20,
                  "americanOdds": 165
               },
               {
                  "id": 1630329195,
                  "odds": 1.5,
                  "name": {
                     "value": "Under 226.5",
                     "sign": "OKoY3g==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Under",
                  "numerator": 1,
                  "denominator": 2,
                  "americanOdds": -200
               }
            ],
            "templateId": 11577,
            "categoryId": 48,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "balanced": 1,
            "attr": "226.5",
            "spread": 1.15,
            "category": "Gridable",
            "templateCategory": {
               "id": 48,
               "name": {
                  "value": "Totals",
                  "sign": "YWp0SQ=="
               },
               "category": "Gridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [
                  "x69gzhrb4"
               ],
               "detailed": [
                  {
                     "group": 0,
                     "index": 8,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Totals",
                     "displayType": "OverUnder"
                  },
                  {
                     "group": 1,
                     "index": 0,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Totals",
                     "displayType": "OverUnder"
                  }
               ],
               "parameters": {
                  "marketType": "OverUnder"
               }
            }
         },
         {
            "id": 561387976,
            "name": {
               "value": "Totals",
               "sign": "Yd+WrA=="
            },
            "results": [
               {
                  "id": 1630329202,
                  "odds": 2.8,
                  "name": {
                     "value": "Over 227.5",
                     "sign": "e/CsqA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Over",
                  "numerator": 9,
                  "denominator": 5,
                  "americanOdds": 180
               },
               {
                  "id": 1630329203,
                  "odds": 1.45,
                  "name": {
                     "value": "Under 227.5",
                     "sign": "0L0sQw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Under",
                  "numerator": 5,
                  "denominator": 11,
                  "americanOdds": -225
               }
            ],
            "templateId": 11577,
            "categoryId": 48,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "balanced": 1,
            "attr": "227.5",
            "spread": 1.35,
            "category": "Gridable",
            "templateCategory": {
               "id": 48,
               "name": {
                  "value": "Totals",
                  "sign": "Yd+WrA=="
               },
               "category": "Gridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [
                  "x69gzhrb4"
               ],
               "detailed": [
                  {
                     "group": 0,
                     "index": 8,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Totals",
                     "displayType": "OverUnder"
                  },
                  {
                     "group": 1,
                     "index": 0,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Totals",
                     "displayType": "OverUnder"
                  }
               ],
               "parameters": {
                  "marketType": "OverUnder"
               }
            }
         },
         {
            "id": 561387981,
            "name": {
               "value": "Totals",
               "sign": "oSgf5w=="
            },
            "results": [
               {
                  "id": 1630329212,
                  "odds": 2.9,
                  "name": {
                     "value": "Over 228.5",
                     "sign": "S0cHyQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Over",
                  "numerator": 19,
                  "denominator": 10,
                  "americanOdds": 190
               },
               {
                  "id": 1630329213,
                  "odds": 1.42,
                  "name": {
                     "value": "Under 228.5",
                     "sign": "oHGrNg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Under",
                  "numerator": 21,
                  "denominator": 50,
                  "americanOdds": -250
               }
            ],
            "templateId": 11577,
            "categoryId": 48,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "balanced": 1,
            "attr": "228.5",
            "spread": 1.48,
            "category": "Gridable",
            "templateCategory": {
               "id": 48,
               "name": {
                  "value": "Totals",
                  "sign": "oSgf5w=="
               },
               "category": "Gridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [
                  "x69gzhrb4"
               ],
               "detailed": [
                  {
                     "group": 0,
                     "index": 8,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Totals",
                     "displayType": "OverUnder"
                  },
                  {
                     "group": 1,
                     "index": 0,
                     "subIndex": 1,
                     "marketTabId": 8,
                     "name": "Totals",
                     "displayType": "OverUnder"
                  }
               ],
               "parameters": {
                  "marketType": "OverUnder"
               }
            }
         },
         {
            "id": 561387986,
            "name": {
               "value": "How many points will Charlotte Hornets score?",
               "sign": "Exu0Uw=="
            },
            "results": [
               {
                  "id": 1630329222,
                  "odds": 1.45,
                  "name": {
                     "value": "Over 99.5",
                     "sign": "qd/3Cw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 5,
                  "denominator": 11,
                  "americanOdds": -225
               },
               {
                  "id": 1630329223,
                  "odds": 2.7,
                  "name": {
                     "value": "Under 99.5",
                     "sign": "XBADGQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 17,
                  "denominator": 10,
                  "americanOdds": 170
               }
            ],
            "templateId": 17700,
            "categoryId": 380,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "attr": "99.5",
            "category": "NonGridable",
            "templateCategory": {
               "id": 7,
               "name": {
                  "value": "Points",
                  "sign": "",
                  "shortSign": ""
               },
               "category": "NonGridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 1,
                     "index": 14,
                     "subIndex": 0,
                     "marketTabId": 8,
                     "displayType": "OverUnder"
                  }
               ],
               "parameters": {
                  "marketType": "OverUnder"
               }
            }
         },
         {
            "id": 561387988,
            "name": {
               "value": "How many points will Charlotte Hornets score?",
               "sign": "0LNqhA=="
            },
            "results": [
               {
                  "id": 1630329226,
                  "odds": 1.87,
                  "name": {
                     "value": "Over 104.5",
                     "sign": "KJXyoQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 20,
                  "denominator": 23,
                  "americanOdds": -115
               },
               {
                  "id": 1630329227,
                  "odds": 1.9,
                  "name": {
                     "value": "Under 104,5",
                     "sign": "WwN/gg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 9,
                  "denominator": 10,
                  "americanOdds": -110
               }
            ],
            "templateId": 17700,
            "categoryId": 380,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "attr": "104.5",
            "category": "NonGridable",
            "templateCategory": {
               "id": 7,
               "name": {
                  "value": "Points",
                  "sign": "",
                  "shortSign": ""
               },
               "category": "NonGridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 1,
                     "index": 14,
                     "subIndex": 0,
                     "marketTabId": 8,
                     "displayType": "OverUnder"
                  }
               ],
               "parameters": {
                  "marketType": "OverUnder"
               }
            }
         },
         {
            "id": 561387990,
            "name": {
               "value": "How many points will Charlotte Hornets score?",
               "sign": "rinwfw=="
            },
            "results": [
               {
                  "id": 1630329230,
                  "odds": 2.7,
                  "name": {
                     "value": "Over 109.5",
                     "sign": "pZC+gw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 17,
                  "denominator": 10,
                  "americanOdds": 170
               },
               {
                  "id": 1630329231,
                  "odds": 1.45,
                  "name": {
                     "value": "Under 109.5",
                     "sign": "z5nQ2Q==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 5,
                  "denominator": 11,
                  "americanOdds": -225
               }
            ],
            "templateId": 17700,
            "categoryId": 380,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "attr": "109.5",
            "category": "NonGridable",
            "templateCategory": {
               "id": 7,
               "name": {
                  "value": "Points",
                  "sign": "",
                  "shortSign": ""
               },
               "category": "NonGridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 1,
                     "index": 14,
                     "subIndex": 0,
                     "marketTabId": 8,
                     "displayType": "OverUnder"
                  }
               ],
               "parameters": {
                  "marketType": "OverUnder"
               }
            }
         },
         {
            "id": 561387992,
            "name": {
               "value": "How many points will Charlotte Hornets score?",
               "sign": "F+Sm8A=="
            },
            "results": [
               {
                  "id": 1630329234,
                  "odds": 4.1,
                  "name": {
                     "value": "Over 114.5",
                     "sign": "iYQ15g==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 31,
                  "denominator": 10,
                  "americanOdds": 310
               },
               {
                  "id": 1630329235,
                  "odds": 1.22,
                  "name": {
                     "value": "Under 114.5",
                     "sign": "Y3sDlA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 2,
                  "denominator": 9,
                  "americanOdds": -450
               }
            ],
            "templateId": 17700,
            "categoryId": 380,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "attr": "114.5",
            "category": "NonGridable",
            "templateCategory": {
               "id": 7,
               "name": {
                  "value": "Points",
                  "sign": "",
                  "shortSign": ""
               },
               "category": "NonGridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 1,
                     "index": 14,
                     "subIndex": 0,
                     "marketTabId": 8,
                     "displayType": "OverUnder"
                  }
               ],
               "parameters": {
                  "marketType": "OverUnder"
               }
            }
         },
         {
            "id": 561387994,
            "name": {
               "value": "How many points will Charlotte Hornets score?",
               "sign": "aX48Cw=="
            },
            "results": [
               {
                  "id": 1630329238,
                  "odds": 6.75,
                  "name": {
                     "value": "Over 119.5",
                     "sign": "BIF5xA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 23,
                  "denominator": 4,
                  "americanOdds": 575
               },
               {
                  "id": 1630329239,
                  "odds": 1.1,
                  "name": {
                     "value": "Under 119.5",
                     "sign": "dYOa/Q==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 1,
                  "denominator": 10,
                  "americanOdds": -1000
               }
            ],
            "templateId": 17700,
            "categoryId": 380,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "attr": "119.5",
            "category": "NonGridable",
            "templateCategory": {
               "id": 7,
               "name": {
                  "value": "Points",
                  "sign": "",
                  "shortSign": ""
               },
               "category": "NonGridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 1,
                     "index": 14,
                     "subIndex": 0,
                     "marketTabId": 8,
                     "displayType": "OverUnder"
                  }
               ],
               "parameters": {
                  "marketType": "OverUnder"
               }
            }
         },
         {
            "id": 561387996,
            "name": {
               "value": "How many points will Charlotte Hornets score?",
               "sign": "qtbi3A=="
            },
            "results": [
               {
                  "id": 1630329242,
                  "odds": 10.5,
                  "name": {
                     "value": "Over 124.5",
                     "sign": "2wapoQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 19,
                  "denominator": 2,
                  "americanOdds": 950
               },
               {
                  "id": 1630329243,
                  "odds": 1.04,
                  "name": {
                     "value": "Under 124.5",
                     "sign": "BwI0RA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 1,
                  "denominator": 25,
                  "americanOdds": -2500
               }
            ],
            "templateId": 17700,
            "categoryId": 380,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "attr": "124.5",
            "category": "NonGridable",
            "templateCategory": {
               "id": 7,
               "name": {
                  "value": "Points",
                  "sign": "",
                  "shortSign": ""
               },
               "category": "NonGridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 1,
                     "index": 14,
                     "subIndex": 0,
                     "marketTabId": 8,
                     "displayType": "OverUnder"
                  }
               ],
               "parameters": {
                  "marketType": "OverUnder"
               }
            }
         },
         {
            "id": 561387998,
            "name": {
               "value": "How many points will Philadelphia 76ers score?",
               "sign": "33iOyw=="
            },
            "results": [
               {
                  "id": 1630329246,
                  "odds": 1.06,
                  "name": {
                     "value": "Over 99.5",
                     "sign": "cGvrfw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 1,
                  "denominator": 16,
                  "americanOdds": -1600
               },
               {
                  "id": 1630329247,
                  "odds": 8.5,
                  "name": {
                     "value": "Under 99.5",
                     "sign": "mGZ45g==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 15,
                  "denominator": 2,
                  "americanOdds": 750
               }
            ],
            "templateId": 18192,
            "categoryId": 380,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "attr": "99.5",
            "category": "NonGridable",
            "templateCategory": {
               "id": 7,
               "name": {
                  "value": "Points",
                  "sign": "",
                  "shortSign": ""
               },
               "category": "NonGridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 1,
                     "index": 15,
                     "subIndex": 0,
                     "marketTabId": 8,
                     "displayType": "OverUnder"
                  }
               ],
               "parameters": {
                  "marketType": "OverUnder"
               }
            }
         },
         {
            "id": 561388000,
            "name": {
               "value": "How many points will Philadelphia 76ers score?",
               "sign": "KkhubA=="
            },
            "results": [
               {
                  "id": 1630329250,
                  "odds": 1.18,
                  "name": {
                     "value": "Over 104.5",
                     "sign": "JQgOew==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 2,
                  "denominator": 11,
                  "americanOdds": -550
               },
               {
                  "id": 1630329251,
                  "odds": 4.75,
                  "name": {
                     "value": "Under 104,5",
                     "sign": "e4MU/A==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 15,
                  "denominator": 4,
                  "americanOdds": 375
               }
            ],
            "templateId": 18192,
            "categoryId": 380,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "attr": "104.5",
            "category": "NonGridable",
            "templateCategory": {
               "id": 7,
               "name": {
                  "value": "Points",
                  "sign": "",
                  "shortSign": ""
               },
               "category": "NonGridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 1,
                     "index": 15,
                     "subIndex": 0,
                     "marketTabId": 8,
                     "displayType": "OverUnder"
                  }
               ],
               "parameters": {
                  "marketType": "OverUnder"
               }
            }
         },
         {
            "id": 561388002,
            "name": {
               "value": "How many points will Philadelphia 76ers score?",
               "sign": "i44o2w=="
            },
            "results": [
               {
                  "id": 1630329254,
                  "odds": 1.42,
                  "name": {
                     "value": "Over 109.5",
                     "sign": "qA1CWQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 21,
                  "denominator": 50,
                  "americanOdds": -250
               },
               {
                  "id": 1630329255,
                  "odds": 2.85,
                  "name": {
                     "value": "Under 109.5",
                     "sign": "7xm7pw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 15,
                  "denominator": 8,
                  "americanOdds": 185
               }
            ],
            "templateId": 18192,
            "categoryId": 380,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "attr": "109.5",
            "category": "NonGridable",
            "templateCategory": {
               "id": 7,
               "name": {
                  "value": "Points",
                  "sign": "",
                  "shortSign": ""
               },
               "category": "NonGridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 1,
                     "index": 15,
                     "subIndex": 0,
                     "marketTabId": 8,
                     "displayType": "OverUnder"
                  }
               ],
               "parameters": {
                  "marketType": "OverUnder"
               }
            }
         },
         {
            "id": 561388004,
            "name": {
               "value": "How many points will Philadelphia 76ers score?",
               "sign": "KcOS2Q=="
            },
            "results": [
               {
                  "id": 1630329258,
                  "odds": 1.85,
                  "name": {
                     "value": "Over 114.5",
                     "sign": "/EKblg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 17,
                  "denominator": 20,
                  "americanOdds": -115
               },
               {
                  "id": 1630329259,
                  "odds": 1.91,
                  "name": {
                     "value": "Under 114.5",
                     "sign": "FlActA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 10,
                  "denominator": 11,
                  "americanOdds": -110
               }
            ],
            "templateId": 18192,
            "categoryId": 380,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "attr": "114.5",
            "category": "NonGridable",
            "templateCategory": {
               "id": 7,
               "name": {
                  "value": "Points",
                  "sign": "",
                  "shortSign": ""
               },
               "category": "NonGridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 1,
                     "index": 15,
                     "subIndex": 0,
                     "marketTabId": 8,
                     "displayType": "OverUnder"
                  }
               ],
               "parameters": {
                  "marketType": "OverUnder"
               }
            }
         },
         {
            "id": 561388006,
            "name": {
               "value": "How many points will Philadelphia 76ers score?",
               "sign": "iAXUbg=="
            },
            "results": [
               {
                  "id": 1630329262,
                  "odds": 2.7,
                  "name": {
                     "value": "Over 119.5",
                     "sign": "cUfXtA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 17,
                  "denominator": 10,
                  "americanOdds": 170
               },
               {
                  "id": 1630329263,
                  "odds": 1.45,
                  "name": {
                     "value": "Under 119.5",
                     "sign": "AKiF3Q==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 5,
                  "denominator": 11,
                  "americanOdds": -225
               }
            ],
            "templateId": 18192,
            "categoryId": 380,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "attr": "119.5",
            "category": "NonGridable",
            "templateCategory": {
               "id": 7,
               "name": {
                  "value": "Points",
                  "sign": "",
                  "shortSign": ""
               },
               "category": "NonGridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 1,
                     "index": 15,
                     "subIndex": 0,
                     "marketTabId": 8,
                     "displayType": "OverUnder"
                  }
               ],
               "parameters": {
                  "marketType": "OverUnder"
               }
            }
         },
         {
            "id": 561388009,
            "name": {
               "value": "How many points will Philadelphia 76ers score?",
               "sign": "Hbj9ag=="
            },
            "results": [
               {
                  "id": 1630329268,
                  "odds": 4.4,
                  "name": {
                     "value": "Over 124.5",
                     "sign": "Z0aLRA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 17,
                  "denominator": 5,
                  "americanOdds": 340
               },
               {
                  "id": 1630329269,
                  "odds": 1.2,
                  "name": {
                     "value": "Under 124.5",
                     "sign": "4N8Ghg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 1,
                  "denominator": 5,
                  "americanOdds": -500
               }
            ],
            "templateId": 18192,
            "categoryId": 380,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "attr": "124.5",
            "category": "NonGridable",
            "templateCategory": {
               "id": 7,
               "name": {
                  "value": "Points",
                  "sign": "",
                  "shortSign": ""
               },
               "category": "NonGridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 1,
                     "index": 15,
                     "subIndex": 0,
                     "marketTabId": 8,
                     "displayType": "OverUnder"
                  }
               ],
               "parameters": {
                  "marketType": "OverUnder"
               }
            }
         },
         {
            "id": 561395708,
            "name": {
               "value": "Race to 5 Points",
               "sign": "nP1YLw=="
            },
            "results": [
               {
                  "id": 1630346070,
                  "odds": 2.1,
                  "name": {
                     "value": "Hornets",
                     "sign": "cI4OCw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 11,
                  "denominator": 10,
                  "americanOdds": 110
               },
               {
                  "id": 1630346071,
                  "odds": 1.65,
                  "name": {
                     "value": "76ers",
                     "sign": "aNNkDQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 13,
                  "denominator": 20,
                  "americanOdds": -155
               }
            ],
            "templateId": 33875,
            "categoryId": 1333,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "NonGridable",
            "templateCategory": {
               "id": 9,
               "name": {
                  "value": "Races",
                  "sign": "",
                  "shortSign": ""
               },
               "category": "NonGridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 5,
                     "index": 0,
                     "displayType": "Regular"
                  }
               ],
               "parameters": {}
            }
         },
         {
            "id": 561395709,
            "name": {
               "value": "Race to 10 Points",
               "sign": "ZqAeoA=="
            },
            "results": [
               {
                  "id": 1630346072,
                  "odds": 2.3,
                  "name": {
                     "value": "Hornets",
                     "sign": "LL995g==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 13,
                  "denominator": 10,
                  "americanOdds": 130
               },
               {
                  "id": 1630346073,
                  "odds": 1.55,
                  "name": {
                     "value": "76ers",
                     "sign": "2N+r3g==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 11,
                  "denominator": 20,
                  "americanOdds": -185
               }
            ],
            "templateId": 5613,
            "categoryId": 1333,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "NonGridable",
            "templateCategory": {
               "id": 9,
               "name": {
                  "value": "Races",
                  "sign": "",
                  "shortSign": ""
               },
               "category": "NonGridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 5,
                     "index": 1,
                     "displayType": "Regular"
                  }
               ],
               "parameters": {}
            }
         },
         {
            "id": 561395710,
            "name": {
               "value": "Race to 15 Points",
               "sign": "Fx/psw=="
            },
            "results": [
               {
                  "id": 1630346074,
                  "odds": 2.4,
                  "name": {
                     "value": "Hornets",
                     "sign": "xWzrCw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 7,
                  "denominator": 5,
                  "americanOdds": 140
               },
               {
                  "id": 1630346075,
                  "odds": 1.5,
                  "name": {
                     "value": "76ers",
                     "sign": "pd5vOg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 1,
                  "denominator": 2,
                  "americanOdds": -200
               }
            ],
            "templateId": 33876,
            "categoryId": 1333,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "NonGridable",
            "templateCategory": {
               "id": 9,
               "name": {
                  "value": "Races",
                  "sign": "",
                  "shortSign": ""
               },
               "category": "NonGridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 5,
                     "index": 2,
                     "displayType": "Regular"
                  }
               ],
               "parameters": {}
            }
         },
         {
            "id": 561395711,
            "name": {
               "value": "Race to 20 Points",
               "sign": "Rb129A=="
            },
            "results": [
               {
                  "id": 1630346076,
                  "odds": 2.55,
                  "name": {
                     "value": "Hornets",
                     "sign": "vx4h5g==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 31,
                  "denominator": 20,
                  "americanOdds": 155
               },
               {
                  "id": 1630346077,
                  "odds": 1.45,
                  "name": {
                     "value": "76ers",
                     "sign": "Y9tSzA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 5,
                  "denominator": 11,
                  "americanOdds": -225
               }
            ],
            "templateId": 2833,
            "categoryId": 1333,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "NonGridable",
            "templateCategory": {
               "id": 9,
               "name": {
                  "value": "Races",
                  "sign": "",
                  "shortSign": ""
               },
               "category": "NonGridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 5,
                     "index": 3,
                     "displayType": "Regular"
                  }
               ],
               "parameters": {}
            }
         },
         {
            "id": 561395712,
            "name": {
               "value": "Race to 25 Points",
               "sign": "6OQgEw=="
            },
            "results": [
               {
                  "id": 1630346078,
                  "odds": 2.65,
                  "name": {
                     "value": "Hornets",
                     "sign": "Vs23Cw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 33,
                  "denominator": 20,
                  "americanOdds": 165
               },
               {
                  "id": 1630346079,
                  "odds": 1.42,
                  "name": {
                     "value": "76ers",
                     "sign": "HtqWKA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 21,
                  "denominator": 50,
                  "americanOdds": -250
               }
            ],
            "templateId": 33877,
            "categoryId": 1333,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "NonGridable",
            "templateCategory": {
               "id": 9,
               "name": {
                  "value": "Races",
                  "sign": "",
                  "shortSign": ""
               },
               "category": "NonGridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 5,
                     "index": 4,
                     "displayType": "Regular"
                  }
               ],
               "parameters": {}
            }
         },
         {
            "id": 561395713,
            "name": {
               "value": "Race to 30 Points",
               "sign": "PG5Jeg=="
            },
            "results": [
               {
                  "id": 1630346080,
                  "odds": 2.75,
                  "name": {
                     "value": "Hornets",
                     "sign": "3nZT5Q==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 7,
                  "denominator": 4,
                  "americanOdds": 175
               },
               {
                  "id": 1630346081,
                  "odds": 1.4,
                  "name": {
                     "value": "76ers",
                     "sign": "muF1Jw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 2,
                  "denominator": 5,
                  "americanOdds": -250
               }
            ],
            "templateId": 33901,
            "categoryId": 1333,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "NonGridable",
            "templateCategory": {
               "id": 9,
               "name": {
                  "value": "Races",
                  "sign": "",
                  "shortSign": ""
               },
               "category": "NonGridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 5,
                     "index": 5,
                     "displayType": "Regular"
                  }
               ],
               "parameters": {}
            }
         },
         {
            "id": 561395714,
            "name": {
               "value": "Race to 40 Points",
               "sign": "4LI/RQ=="
            },
            "results": [
               {
                  "id": 1630346082,
                  "odds": 2.95,
                  "name": {
                     "value": "Hornets",
                     "sign": "N6XFCA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 19,
                  "denominator": 10,
                  "americanOdds": 195
               },
               {
                  "id": 1630346083,
                  "odds": 1.35,
                  "name": {
                     "value": "76ers",
                     "sign": "5+Cxww==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 7,
                  "denominator": 20,
                  "americanOdds": -275
               }
            ],
            "templateId": 33902,
            "categoryId": 1333,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "NonGridable",
            "templateCategory": {
               "id": 9,
               "name": {
                  "value": "Races",
                  "sign": "",
                  "shortSign": ""
               },
               "category": "NonGridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 5,
                     "index": 6,
                     "displayType": "Regular"
                  }
               ],
               "parameters": {}
            }
         },
         {
            "id": 561395715,
            "name": {
               "value": "Race to 50 Points",
               "sign": "UDa2ZA=="
            },
            "results": [
               {
                  "id": 1630346084,
                  "odds": 3.2,
                  "name": {
                     "value": "Hornets",
                     "sign": "TdcP5Q==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 11,
                  "denominator": 5,
                  "americanOdds": 220
               },
               {
                  "id": 1630346085,
                  "odds": 1.3,
                  "name": {
                     "value": "76ers",
                     "sign": "IeWMNQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 3,
                  "denominator": 10,
                  "americanOdds": -350
               }
            ],
            "templateId": 33903,
            "categoryId": 1333,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "NonGridable",
            "templateCategory": {
               "id": 9,
               "name": {
                  "value": "Races",
                  "sign": "",
                  "shortSign": ""
               },
               "category": "NonGridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 5,
                     "index": 7,
                     "displayType": "Regular"
                  }
               ],
               "parameters": {}
            }
         },
         {
            "id": 561395716,
            "name": {
               "value": "Winning Margin",
               "sign": "8V05jQ=="
            },
            "results": [
               {
                  "id": 1630346086,
                  "odds": 8.0,
                  "name": {
                     "value": "Hornets by 1-5",
                     "sign": "vaz6IQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 7,
                  "denominator": 1,
                  "americanOdds": 700
               },
               {
                  "id": 1630346087,
                  "odds": 5.75,
                  "name": {
                     "value": "76ers by 1-5",
                     "sign": "e2Pmjw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 19,
                  "denominator": 4,
                  "americanOdds": 475
               },
               {
                  "id": 1630346088,
                  "odds": 10.5,
                  "name": {
                     "value": "Hornets by 6-10",
                     "sign": "byP+6Q==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 19,
                  "denominator": 2,
                  "americanOdds": 950
               },
               {
                  "id": 1630346089,
                  "odds": 4.33,
                  "name": {
                     "value": "76ers by 6-10",
                     "sign": "cHmF9Q==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 10,
                  "denominator": 3,
                  "americanOdds": 333
               },
               {
                  "id": 1630346090,
                  "odds": 26.0,
                  "name": {
                     "value": "Hornets by 11-15",
                     "sign": "M/tYxQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 25,
                  "denominator": 1,
                  "americanOdds": 2500
               },
               {
                  "id": 1630346091,
                  "odds": 5.25,
                  "name": {
                     "value": "76ers by 11-15",
                     "sign": "mUZICA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 17,
                  "denominator": 4,
                  "americanOdds": 425
               },
               {
                  "id": 1630346092,
                  "odds": 34.0,
                  "name": {
                     "value": "Hornets by 16-20",
                     "sign": "+XOHSg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 33,
                  "denominator": 1,
                  "americanOdds": 3300
               },
               {
                  "id": 1630346093,
                  "odds": 6.5,
                  "name": {
                     "value": "76ers by 16-20",
                     "sign": "XJOaRw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 11,
                  "denominator": 2,
                  "americanOdds": 550
               },
               {
                  "id": 1630346094,
                  "odds": 34.0,
                  "name": {
                     "value": "Hornets by 21-25",
                     "sign": "Cshf2A==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 33,
                  "denominator": 1,
                  "americanOdds": 3300
               },
               {
                  "id": 1630346095,
                  "odds": 8.75,
                  "name": {
                     "value": "76ers by 21-25",
                     "sign": "quNGlQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 15,
                  "denominator": 2,
                  "americanOdds": 775
               },
               {
                  "id": 1630346096,
                  "odds": 34.0,
                  "name": {
                     "value": "Hornets by 26 or more",
                     "sign": "E0je3w==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 33,
                  "denominator": 1,
                  "americanOdds": 3300
               },
               {
                  "id": 1630346097,
                  "odds": 7.5,
                  "name": {
                     "value": "76ers by 26 or more",
                     "sign": "M0bazQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 13,
                  "denominator": 2,
                  "americanOdds": 650
               }
            ],
            "templateId": 4860,
            "categoryId": 452,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "NonGridable",
            "templateCategory": {
               "id": 16,
               "name": {
                  "value": "Winning margin",
                  "sign": "",
                  "shortSign": ""
               },
               "category": "NonGridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 0,
                     "index": 46,
                     "displayType": "Regular"
                  },
                  {
                     "group": 7,
                     "index": 1,
                     "displayType": "Regular"
                  }
               ],
               "parameters": {}
            }
         },
         {
            "id": 561395718,
            "name": {
               "value": "Will the match go into overtime?",
               "sign": "wfRBsg=="
            },
            "results": [
               {
                  "id": 1630346110,
                  "odds": 12.5,
                  "name": {
                     "value": "Yes",
                     "sign": "NAY1tQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 11,
                  "denominator": 1,
                  "americanOdds": 1150
               },
               {
                  "id": 1630346111,
                  "odds": 1.03,
                  "name": {
                     "value": "No",
                     "sign": "ga6n4A==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 1,
                  "denominator": 33,
                  "americanOdds": -3000
               }
            ],
            "templateId": 1171,
            "categoryId": 4,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 4,
               "name": {
                  "value": "Miscellaneous",
                  "sign": "zAaIHA=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 7,
                     "index": 2,
                     "displayType": "Regular"
                  }
               ],
               "parameters": {}
            }
         },
         {
            "id": 561395719,
            "name": {
               "value": "Will the final score be odd or even?",
               "sign": "Xmj23g=="
            },
            "results": [
               {
                  "id": 1630346112,
                  "odds": 1.87,
                  "name": {
                     "value": "Odd",
                     "sign": "gAL0QQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Over",
                  "numerator": 20,
                  "denominator": 23,
                  "americanOdds": -115
               },
               {
                  "id": 1630346113,
                  "odds": 1.95,
                  "name": {
                     "value": "Even",
                     "sign": "jUmSmQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Under",
                  "numerator": 19,
                  "denominator": 20,
                  "americanOdds": -105
               }
            ],
            "templateId": 7970,
            "categoryId": 48,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Other",
            "templateCategory": {
               "id": 48,
               "name": {
                  "value": "Totals",
                  "sign": "bwO9/Q=="
               },
               "category": "Other"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 7,
                     "index": 4,
                     "displayType": "Regular"
                  }
               ],
               "parameters": {}
            }
         },
         {
            "id": 561395720,
            "name": {
               "value": "Will the 1st half total be odd or even?",
               "sign": "QW9T6A=="
            },
            "results": [
               {
                  "id": 1630346114,
                  "odds": 1.91,
                  "name": {
                     "value": "Odd",
                     "sign": "cQRANQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Over",
                  "numerator": 10,
                  "denominator": 11,
                  "americanOdds": -110
               },
               {
                  "id": 1630346115,
                  "odds": 1.91,
                  "name": {
                     "value": "Even",
                     "sign": "AT9cUw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Under",
                  "numerator": 10,
                  "denominator": 11,
                  "americanOdds": -110
               }
            ],
            "templateId": 12173,
            "categoryId": 48,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Other",
            "templateCategory": {
               "id": 48,
               "name": {
                  "value": "Totals",
                  "sign": "LxsnIQ=="
               },
               "category": "Other"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 7,
                     "index": 5,
                     "displayType": "Regular"
                  }
               ],
               "parameters": {}
            }
         },
         {
            "id": 561395723,
            "name": {
               "value": "Which team will score the first points?",
               "sign": "ymz2oA=="
            },
            "results": [
               {
                  "id": 1630346120,
                  "odds": 2.0,
                  "name": {
                     "value": "Hornets",
                     "sign": "MBJR6Q==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 1,
                  "denominator": 1,
                  "americanOdds": 100
               },
               {
                  "id": 1630346121,
                  "odds": 1.72,
                  "name": {
                     "value": "76ers",
                     "sign": "Zh0dUw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 8,
                  "denominator": 11,
                  "americanOdds": -140
               }
            ],
            "templateId": 4011,
            "categoryId": 393,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "NonGridable",
            "templateCategory": {
               "id": 13,
               "name": {
                  "value": "Which team will score points?",
                  "sign": "",
                  "shortSign": ""
               },
               "category": "NonGridable"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 10000,
                     "index": 0,
                     "displayType": "Regular"
                  }
               ]
            }
         },
         {
            "id": 561395725,
            "name": {
               "value": "How many points will be scored in the game? (Regular time only)",
               "sign": "qpEymA=="
            },
            "results": [
               {
                  "id": 1630346124,
                  "odds": 151.0,
                  "name": {
                     "value": "150 or less",
                     "sign": "Dc64zQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Over",
                  "numerator": 150,
                  "denominator": 1,
                  "americanOdds": 15000
               },
               {
                  "id": 1630346125,
                  "odds": 151.0,
                  "name": {
                     "value": "151 to 160",
                     "sign": "nEpToA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Under",
                  "numerator": 150,
                  "denominator": 1,
                  "americanOdds": 15000
               },
               {
                  "id": 1630346126,
                  "odds": 126.0,
                  "name": {
                     "value": "161 to 170",
                     "sign": "4KqScg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Under",
                  "numerator": 125,
                  "denominator": 1,
                  "americanOdds": 12500
               },
               {
                  "id": 1630346127,
                  "odds": 34.0,
                  "name": {
                     "value": "171 to 180",
                     "sign": "+wRsBQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Under",
                  "numerator": 33,
                  "denominator": 1,
                  "americanOdds": 3300
               },
               {
                  "id": 1630346128,
                  "odds": 14.0,
                  "name": {
                     "value": "181 to 190",
                     "sign": "ifd25A==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Under",
                  "numerator": 13,
                  "denominator": 1,
                  "americanOdds": 1300
               },
               {
                  "id": 1630346129,
                  "odds": 7.25,
                  "name": {
                     "value": "191 to 200",
                     "sign": "TUCUxw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Under",
                  "numerator": 6,
                  "denominator": 1,
                  "americanOdds": 625
               },
               {
                  "id": 1630346130,
                  "odds": 5.0,
                  "name": {
                     "value": "201 to 210",
                     "sign": "rJFMaw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Under",
                  "numerator": 4,
                  "denominator": 1,
                  "americanOdds": 400
               },
               {
                  "id": 1630346131,
                  "odds": 4.1,
                  "name": {
                     "value": "211 to 220",
                     "sign": "u3AHsA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Under",
                  "numerator": 31,
                  "denominator": 10,
                  "americanOdds": 310
               },
               {
                  "id": 1630346132,
                  "odds": 4.4,
                  "name": {
                     "value": "221 to 230",
                     "sign": "GQZSSA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Under",
                  "numerator": 17,
                  "denominator": 5,
                  "americanOdds": 340
               },
               {
                  "id": 1630346133,
                  "odds": 6.0,
                  "name": {
                     "value": "231 to 240",
                     "sign": "CiJ19w==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Under",
                  "numerator": 5,
                  "denominator": 1,
                  "americanOdds": 500
               },
               {
                  "id": 1630346134,
                  "odds": 6.25,
                  "name": {
                     "value": "241 or more",
                     "sign": "j/rptQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "totalsPrefix": "Under",
                  "numerator": 21,
                  "denominator": 4,
                  "americanOdds": 525
               }
            ],
            "templateId": 19901,
            "categoryId": 48,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "attr": "150",
            "category": "Other",
            "templateCategory": {
               "id": 48,
               "name": {
                  "value": "Totals",
                  "sign": "7+yuag=="
               },
               "category": "Other"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 0,
                     "index": 48,
                     "displayType": "Regular"
                  },
                  {
                     "group": 1,
                     "index": 16,
                     "displayType": "Regular"
                  }
               ],
               "parameters": {}
            }
         },
         {
            "id": 561396355,
            "name": {
               "value": "How many three-pointers will Ben Simmons (Phi) record?",
               "sign": "jPPfAw=="
            },
            "results": [
               {
                  "id": 1630347429,
                  "odds": 6.0,
                  "name": {
                     "value": "Over 0.5",
                     "sign": "zbW+hA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "0.5",
                  "numerator": 5,
                  "denominator": 1,
                  "americanOdds": 500
               },
               {
                  "id": 1630347430,
                  "odds": 1.12,
                  "name": {
                     "value": "Under 0.5",
                     "sign": "d8hd1w==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "0.5",
                  "numerator": 2,
                  "denominator": 17,
                  "americanOdds": -800
               }
            ],
            "templateId": 12857,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "7d7Wow=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 4,
                     "marketTabId": 13,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Ben Simmons (Phi)",
               "sign": "soEr0Q==",
               "short": "B. Simmons",
               "shortSign": "UHJ2AA=="
            }
         },
         {
            "id": 561396356,
            "name": {
               "value": "How many blocks will Ben Simmons (Phi) record?",
               "sign": "HOVkeg=="
            },
            "results": [
               {
                  "id": 1630347431,
                  "odds": 1.83,
                  "name": {
                     "value": "Over 0.5",
                     "sign": "wnmFXQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "0.5",
                  "numerator": 5,
                  "denominator": 6,
                  "americanOdds": -120
               },
               {
                  "id": 1630347432,
                  "odds": 1.95,
                  "name": {
                     "value": "Under 0.5",
                     "sign": "ZjeNkA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "0.5",
                  "numerator": 19,
                  "denominator": 20,
                  "americanOdds": -105
               }
            ],
            "templateId": 29304,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "2kRhiw=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 5,
                     "marketTabId": 14,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Ben Simmons (Phi)",
               "sign": "a3P2UQ==",
               "short": "B. Simmons",
               "shortSign": "dgm12A=="
            }
         },
         {
            "id": 561396357,
            "name": {
               "value": "How many total rebounds and assists will Ben Simmons (Phi) record?",
               "sign": "QjP/mA=="
            },
            "results": [
               {
                  "id": 1630347433,
                  "odds": 1.83,
                  "name": {
                     "value": "Over 14.5",
                     "sign": "A3httg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "14.5",
                  "numerator": 5,
                  "denominator": 6,
                  "americanOdds": -120
               },
               {
                  "id": 1630347434,
                  "odds": 1.95,
                  "name": {
                     "value": "Under 14.5",
                     "sign": "wqOBNQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "14.5",
                  "numerator": 19,
                  "denominator": 20,
                  "americanOdds": -105
               }
            ],
            "templateId": 33718,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "C6yGhQ=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 1,
                     "subIndex": 4,
                     "marketTabId": 20,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Ben Simmons (Phi)",
               "sign": "mOMEZw==",
               "short": "B. Simmons",
               "shortSign": "4a+oPw=="
            }
         },
         {
            "id": 561396358,
            "name": {
               "value": "How many rebounds will Ben Simmons (Phi) record?",
               "sign": "eGXjNQ=="
            },
            "results": [
               {
                  "id": 1630347435,
                  "odds": 1.9,
                  "name": {
                     "value": "Over 7.5",
                     "sign": "ps3B4w==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "7.5",
                  "numerator": 9,
                  "denominator": 10,
                  "americanOdds": -110
               },
               {
                  "id": 1630347436,
                  "odds": 1.87,
                  "name": {
                     "value": "Under 7.5",
                     "sign": "GGt/bg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "7.5",
                  "numerator": 20,
                  "denominator": 23,
                  "americanOdds": -115
               }
            ],
            "templateId": 29301,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "eJWulg=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 2,
                     "marketTabId": 10,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Ben Simmons (Phi)",
               "sign": "jVITPA==",
               "short": "B. Simmons",
               "shortSign": "GUL/zQ=="
            }
         },
         {
            "id": 561396359,
            "name": {
               "value": "How many steals will Ben Simmons (Phi) record?",
               "sign": "fIPFTw=="
            },
            "results": [
               {
                  "id": 1630347437,
                  "odds": 1.87,
                  "name": {
                     "value": "Over 1.5",
                     "sign": "RONxVw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "1.5",
                  "numerator": 20,
                  "denominator": 23,
                  "americanOdds": -115
               },
               {
                  "id": 1630347438,
                  "odds": 1.9,
                  "name": {
                     "value": "Under 1.5",
                     "sign": "9zGU+g==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "1.5",
                  "numerator": 9,
                  "denominator": 10,
                  "americanOdds": -110
               }
            ],
            "templateId": 29303,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "qX1JmA=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 3,
                     "marketTabId": 12,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Ben Simmons (Phi)",
               "sign": "fsLhCg==",
               "short": "B. Simmons",
               "shortSign": "juTiKg=="
            }
         },
         {
            "id": 561396360,
            "name": {
               "value": "How many total steals and blocks will Ben Simmons (Phi) record?",
               "sign": "2ZUpgg=="
            },
            "results": [
               {
                  "id": 1630347439,
                  "odds": 2.15,
                  "name": {
                     "value": "Over 2.5",
                     "sign": "EpEMjA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "2.5",
                  "numerator": 23,
                  "denominator": 20,
                  "americanOdds": 115
               },
               {
                  "id": 1630347440,
                  "odds": 1.67,
                  "name": {
                     "value": "Under 2.5",
                     "sign": "0VcV5w==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "2.5",
                  "numerator": 4,
                  "denominator": 6,
                  "americanOdds": -150
               }
            ],
            "templateId": 33717,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "FqHBxw=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 1,
                     "subIndex": 1,
                     "marketTabId": 19,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Ben Simmons (Phi)",
               "sign": "frHZ5g==",
               "short": "B. Simmons",
               "shortSign": "FLIIpw=="
            }
         },
         {
            "id": 561396361,
            "name": {
               "value": "How many total points, rebounds and assists will Ben Simmons (Phi) record?",
               "sign": "cdU8Fw=="
            },
            "results": [
               {
                  "id": 1630347441,
                  "odds": 1.95,
                  "name": {
                     "value": "Over 30.5",
                     "sign": "jaxxbw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "30.5",
                  "numerator": 19,
                  "denominator": 20,
                  "americanOdds": -105
               },
               {
                  "id": 1630347442,
                  "odds": 1.83,
                  "name": {
                     "value": "Under 30.5",
                     "sign": "UbX6Zw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "30.5",
                  "numerator": 5,
                  "denominator": 6,
                  "americanOdds": -120
               }
            ],
            "templateId": 32108,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "x0kmyQ=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 6,
                     "marketTabId": 16,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Ben Simmons (Phi)",
               "sign": "jSEr0A==",
               "short": "B. Simmons",
               "shortSign": "gxQVQA=="
            }
         },
         {
            "id": 561396362,
            "name": {
               "value": "How many total points and assists will Ben Simmons (Phi) record?",
               "sign": "yFCQ3g=="
            },
            "results": [
               {
                  "id": 1630347443,
                  "odds": 1.87,
                  "name": {
                     "value": "Over 22.5",
                     "sign": "2zkvRA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "22.5",
                  "numerator": 20,
                  "denominator": 23,
                  "americanOdds": -115
               },
               {
                  "id": 1630347444,
                  "odds": 1.9,
                  "name": {
                     "value": "Under 22.5",
                     "sign": "69sc4w==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "22.5",
                  "numerator": 9,
                  "denominator": 10,
                  "americanOdds": -110
               }
            ],
            "templateId": 13495,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "tHAO2g=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 1,
                     "subIndex": 2,
                     "marketTabId": 23,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Ben Simmons (Phi)",
               "sign": "mJA8iw==",
               "short": "B. Simmons",
               "shortSign": "e/lCsg=="
            }
         },
         {
            "id": 561396363,
            "name": {
               "value": "How many total points and rebounds will Ben Simmons (Phi) record?",
               "sign": "lv8vpQ=="
            },
            "results": [
               {
                  "id": 1630347445,
                  "odds": 1.85,
                  "name": {
                     "value": "Over 22.5",
                     "sign": "fVX0Lw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "22.5",
                  "numerator": 17,
                  "denominator": 20,
                  "americanOdds": -115
               },
               {
                  "id": 1630347446,
                  "odds": 1.95,
                  "name": {
                     "value": "Under 22.5",
                     "sign": "hJBW9g==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "22.5",
                  "numerator": 19,
                  "denominator": 20,
                  "americanOdds": -105
               }
            ],
            "templateId": 13494,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "ZZjp1A=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 1,
                     "subIndex": 3,
                     "marketTabId": 24,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Ben Simmons (Phi)",
               "sign": "awDOvQ==",
               "short": "B. Simmons",
               "shortSign": "7F9fVQ=="
            }
         },
         {
            "id": 561396365,
            "name": {
               "value": "How many points will Ben Simmons (Phi) score?",
               "sign": "90aUQA=="
            },
            "results": [
               {
                  "id": 1630347449,
                  "odds": 1.91,
                  "name": {
                     "value": "Over 15.5",
                     "sign": "WjW47w==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "15.5",
                  "numerator": 10,
                  "denominator": 11,
                  "americanOdds": -110
               },
               {
                  "id": 1630347450,
                  "odds": 1.85,
                  "name": {
                     "value": "Under 15.5",
                     "sign": "jZIRng==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "15.5",
                  "numerator": 17,
                  "denominator": 20,
                  "americanOdds": -115
               }
            ],
            "templateId": 29300,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "g+q58g=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 0,
                     "marketTabId": 9,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Ben Simmons (Phi)",
               "sign": "QWLhCw==",
               "short": "B. Simmons",
               "shortSign": "XYKBag=="
            }
         },
         {
            "id": 561396366,
            "name": {
               "value": "How many turnovers will Ben Simmons (Phi) record?",
               "sign": "Ok9wmA=="
            },
            "results": [
               {
                  "id": 1630347451,
                  "odds": 1.57,
                  "name": {
                     "value": "Over 2.5",
                     "sign": "cHBhmg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "2.5",
                  "numerator": 4,
                  "denominator": 7,
                  "americanOdds": -175
               },
               {
                  "id": 1630347452,
                  "odds": 2.35,
                  "name": {
                     "value": "Under 2.5",
                     "sign": "nY6jMA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "2.5",
                  "numerator": 27,
                  "denominator": 20,
                  "americanOdds": 135
               }
            ],
            "templateId": 12869,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "8NOR4Q=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 1,
                     "subIndex": 0,
                     "marketTabId": 15,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Ben Simmons (Phi)",
               "sign": "VNP2UA==",
               "short": "B. Simmons",
               "shortSign": "pW/WmA=="
            }
         },
         {
            "id": 561396367,
            "name": {
               "value": "How many assists will Ben Simmons (Phi) record?",
               "sign": "LYGLCQ=="
            },
            "results": [
               {
                  "id": 1630347453,
                  "odds": 2.0,
                  "name": {
                     "value": "Over 7.5",
                     "sign": "y+CXLA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "7.5",
                  "numerator": 1,
                  "denominator": 1,
                  "americanOdds": 100
               },
               {
                  "id": 1630347454,
                  "odds": 1.8,
                  "name": {
                     "value": "Under 7.5",
                     "sign": "K2oOpg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "7.5",
                  "numerator": 4,
                  "denominator": 5,
                  "americanOdds": -125
               }
            ],
            "templateId": 29302,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "ITt27w=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 1,
                     "marketTabId": 11,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Ben Simmons (Phi)",
               "sign": "p0MEZg==",
               "short": "B. Simmons",
               "shortSign": "MsnLfw=="
            }
         },
         {
            "id": 561396378,
            "name": {
               "value": "How many three-pointers will Danny Green (Phi) make?",
               "sign": "6MGX1w=="
            },
            "results": [
               {
                  "id": 1630347475,
                  "odds": 1.65,
                  "name": {
                     "value": "Over 1.5",
                     "sign": "WPYgkg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "1.5",
                  "numerator": 13,
                  "denominator": 20,
                  "americanOdds": -155
               },
               {
                  "id": 1630347476,
                  "odds": 2.2,
                  "name": {
                     "value": "Under 1.5",
                     "sign": "VnbtFQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "1.5",
                  "numerator": 6,
                  "denominator": 5,
                  "americanOdds": 120
               }
            ],
            "templateId": 12857,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "pP1wNA=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 4,
                     "marketTabId": 13,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Danny Green (Phi)",
               "sign": "lgzg8w==",
               "short": "D. Green",
               "shortSign": "mKRrgg=="
            }
         },
         {
            "id": 561396379,
            "name": {
               "value": "How many rebounds will Danny Green (PHI) record?",
               "sign": "drT91Q=="
            },
            "results": [
               {
                  "id": 1630347477,
                  "odds": 2.1,
                  "name": {
                     "value": "Over 2.5",
                     "sign": "URpbIA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "2.5",
                  "numerator": 11,
                  "denominator": 10,
                  "americanOdds": 110
               },
               {
                  "id": 1630347478,
                  "odds": 1.7,
                  "name": {
                     "value": "Under 2.5",
                     "sign": "Uu7Nhw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "2.5",
                  "numerator": 7,
                  "denominator": 10,
                  "americanOdds": -145
               }
            ],
            "templateId": 29301,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "dRWXOg=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 2,
                     "marketTabId": 10,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Danny Green (Phi)",
               "sign": "ZZwSxQ==",
               "short": "D. Green",
               "shortSign": "v8FOAw=="
            }
         },
         {
            "id": 561396380,
            "name": {
               "value": "How many total points, rebounds and assists will Danny Green (PHI) record?",
               "sign": "t7uyJw=="
            },
            "results": [
               {
                  "id": 1630347479,
                  "odds": 1.91,
                  "name": {
                     "value": "Over 11.5",
                     "sign": "srdipA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "11.5",
                  "numerator": 10,
                  "denominator": 11,
                  "americanOdds": -110
               },
               {
                  "id": 1630347480,
                  "odds": 1.87,
                  "name": {
                     "value": "Under 11.5",
                     "sign": "rKZdxw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "11.5",
                  "numerator": 20,
                  "denominator": 23,
                  "americanOdds": -115
               }
            ],
            "templateId": 32108,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "Qo8gEg=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 6,
                     "marketTabId": 16,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Danny Green (Phi)",
               "sign": "vG7PRQ==",
               "short": "D. Green",
               "shortSign": "yPZWMg=="
            }
         },
         {
            "id": 561396381,
            "name": {
               "value": "How many steals will Danny Green (PHI) record?",
               "sign": "ar7m8A=="
            },
            "results": [
               {
                  "id": 1630347481,
                  "odds": 1.57,
                  "name": {
                     "value": "Over 0.5",
                     "sign": "3mzUmA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "0.5",
                  "numerator": 4,
                  "denominator": 7,
                  "americanOdds": -175
               },
               {
                  "id": 1630347482,
                  "odds": 2.4,
                  "name": {
                     "value": "Under 0.5",
                     "sign": "cOP/Uw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "0.5",
                  "numerator": 7,
                  "denominator": 5,
                  "americanOdds": 140
               }
            ],
            "templateId": 29303,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "k2fHHA=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 3,
                     "marketTabId": 12,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Danny Green (Phi)",
               "sign": "T/49cw==",
               "short": "D. Green",
               "shortSign": "75Nzsw=="
            }
         },
         {
            "id": 561396382,
            "name": {
               "value": "How many total staels and blocks will Danny Green (PHI) record?",
               "sign": "Kc7uQA=="
            },
            "results": [
               {
                  "id": 1630347483,
                  "odds": 2.1,
                  "name": {
                     "value": "Over 1.5",
                     "sign": "5sotQA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "1.5",
                  "numerator": 11,
                  "denominator": 10,
                  "americanOdds": 110
               },
               {
                  "id": 1630347484,
                  "odds": 1.72,
                  "name": {
                     "value": "Under 1.5",
                     "sign": "4eXmOQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "1.5",
                  "numerator": 8,
                  "denominator": 11,
                  "americanOdds": -140
               }
            ],
            "templateId": 33717,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "4F7vDw=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 1,
                     "subIndex": 1,
                     "marketTabId": 19,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Danny Green (Phi)",
               "sign": "Wk8qKA==",
               "short": "D. Green",
               "shortSign": "xzpt6w=="
            }
         },
         {
            "id": 561396383,
            "name": {
               "value": "How many total points and rebounds will Danny Green (PHI) record?",
               "sign": "URF8oQ=="
            },
            "results": [
               {
                  "id": 1630347485,
                  "odds": 1.78,
                  "name": {
                     "value": "Over 9.5",
                     "sign": "DskD/g==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "9.5",
                  "numerator": 4,
                  "denominator": 5,
                  "americanOdds": -130
               },
               {
                  "id": 1630347486,
                  "odds": 2.0,
                  "name": {
                     "value": "Under 9.5",
                     "sign": "BJKTpw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "9.5",
                  "numerator": 1,
                  "denominator": 1,
                  "americanOdds": 100
               }
            ],
            "templateId": 13494,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "MbYIAQ=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 1,
                     "subIndex": 3,
                     "marketTabId": 24,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Danny Green (Phi)",
               "sign": "qd/YHg==",
               "short": "D. Green",
               "shortSign": "4F9Iag=="
            }
         },
         {
            "id": 561396384,
            "name": {
               "value": "How many turnovers will Danny Green (PHI) record?",
               "sign": "P+QnNQ=="
            },
            "results": [
               {
                  "id": 1630347487,
                  "odds": 1.65,
                  "name": {
                     "value": "Over 0.5",
                     "sign": "jj7pKA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "0.5",
                  "numerator": 13,
                  "denominator": 20,
                  "americanOdds": -155
               },
               {
                  "id": 1630347488,
                  "odds": 2.25,
                  "name": {
                     "value": "Under 0.5",
                     "sign": "KD+rBw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "0.5",
                  "numerator": 5,
                  "denominator": 4,
                  "americanOdds": 125
               }
            ],
            "templateId": 12869,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "//tytw=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 1,
                     "subIndex": 0,
                     "marketTabId": 15,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Danny Green (Phi)",
               "sign": "Pq7MQw==",
               "short": "D. Green",
               "shortSign": "btXgCA=="
            }
         },
         {
            "id": 561396385,
            "name": {
               "value": "How many assists will Danny Green (PHI) record?",
               "sign": "thQWzQ=="
            },
            "results": [
               {
                  "id": 1630347489,
                  "odds": 2.15,
                  "name": {
                     "value": "Over 1.5",
                     "sign": "ELumyg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "1.5",
                  "numerator": 23,
                  "denominator": 20,
                  "americanOdds": 115
               },
               {
                  "id": 1630347490,
                  "odds": 1.67,
                  "name": {
                     "value": "Under 1.5",
                     "sign": "QnMPlg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "1.5",
                  "numerator": 4,
                  "denominator": 6,
                  "americanOdds": -150
               }
            ],
            "templateId": 29302,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "LhOVuQ=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 1,
                     "marketTabId": 11,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Danny Green (Phi)",
               "sign": "zT4+dQ==",
               "short": "D. Green",
               "shortSign": "SbDFiQ=="
            }
         },
         {
            "id": 561396386,
            "name": {
               "value": "How many points will Danny Green (PHI) score?",
               "sign": "G2AB6g=="
            },
            "results": [
               {
                  "id": 1630347491,
                  "odds": 1.9,
                  "name": {
                     "value": "Over 7.5",
                     "sign": "rQsQFw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "7.5",
                  "numerator": 9,
                  "denominator": 10,
                  "americanOdds": -110
               },
               {
                  "id": 1630347492,
                  "odds": 1.9,
                  "name": {
                     "value": "Under 7.5",
                     "sign": "VmNZ+Q==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "7.5",
                  "numerator": 9,
                  "denominator": 10,
                  "americanOdds": -110
               }
            ],
            "templateId": 29300,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "XSq9qg=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 0,
                     "marketTabId": 9,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Danny Green (Phi)",
               "sign": "2I8pLg==",
               "short": "D. Green",
               "shortSign": "YRnb0Q=="
            }
         },
         {
            "id": 561396387,
            "name": {
               "value": "How many blocks will Danny Green (Phi) record?",
               "sign": "KUI7qg=="
            },
            "results": [
               {
                  "id": 1630347493,
                  "odds": 2.15,
                  "name": {
                     "value": "Over 0.5",
                     "sign": "eE9iog==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "0.5",
                  "numerator": 23,
                  "denominator": 20,
                  "americanOdds": 115
               },
               {
                  "id": 1630347494,
                  "odds": 1.7,
                  "name": {
                     "value": "Under 0.5",
                     "sign": "jlNwbA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "0.5",
                  "numerator": 7,
                  "denominator": 10,
                  "americanOdds": -145
               }
            ],
            "templateId": 29304,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "jMJapA=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 5,
                     "marketTabId": 14,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Danny Green (Phi)",
               "sign": "Kx/bGA==",
               "short": "D. Green",
               "shortSign": "Rnz+UA=="
            }
         },
         {
            "id": 561396388,
            "name": {
               "value": "How many total points and assists will Danny Green (PHI) record?",
               "sign": "AwPgRw=="
            },
            "results": [
               {
                  "id": 1630347495,
                  "odds": 1.75,
                  "name": {
                     "value": "Over 8.5",
                     "sign": "z9JKdQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "8.5",
                  "numerator": 3,
                  "denominator": 4,
                  "americanOdds": -135
               },
               {
                  "id": 1630347496,
                  "odds": 2.05,
                  "name": {
                     "value": "Under 8.5",
                     "sign": "J/2zJQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "8.5",
                  "numerator": 21,
                  "denominator": 20,
                  "americanOdds": 105
               }
            ],
            "templateId": 13495,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "u1jtjA=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 1,
                     "subIndex": 2,
                     "marketTabId": 23,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Danny Green (Phi)",
               "sign": "8u0GmA==",
               "short": "D. Green",
               "shortSign": "MUvmYQ=="
            }
         },
         {
            "id": 561396389,
            "name": {
               "value": "How many total rebounds and assists will Danny Green (PHI) record?",
               "sign": "S6d52g=="
            },
            "results": [
               {
                  "id": 1630347497,
                  "odds": 1.72,
                  "name": {
                     "value": "Over 3.5",
                     "sign": "wFMvGw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "3.5",
                  "numerator": 8,
                  "denominator": 11,
                  "americanOdds": -140
               },
               {
                  "id": 1630347498,
                  "odds": 2.1,
                  "name": {
                     "value": "Under 3.5",
                     "sign": "mzSAuQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "3.5",
                  "numerator": 11,
                  "denominator": 10,
                  "americanOdds": 110
               }
            ],
            "templateId": 33718,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "arAKgg=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 1,
                     "subIndex": 4,
                     "marketTabId": 20,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Danny Green (Phi)",
               "sign": "AX30rg==",
               "short": "D. Green",
               "shortSign": "Fi7D4A=="
            }
         },
         {
            "id": 561396397,
            "name": {
               "value": "How many blocks will Joel Embiid (Phi) record?",
               "sign": "qD9BRA=="
            },
            "results": [
               {
                  "id": 1630347513,
                  "odds": 2.3,
                  "name": {
                     "value": "Over 1.5",
                     "sign": "k/jAZw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "1.5",
                  "numerator": 13,
                  "denominator": 10,
                  "americanOdds": 130
               },
               {
                  "id": 1630347514,
                  "odds": 1.6,
                  "name": {
                     "value": "Under 1.5",
                     "sign": "m8cT4g==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "1.5",
                  "numerator": 3,
                  "denominator": 5,
                  "americanOdds": -165
               }
            ],
            "templateId": 29304,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "4vY19Q=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 5,
                     "marketTabId": 14,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Joel Embiid (Phi)",
               "sign": "mVm19A==",
               "short": "J. Embiid",
               "shortSign": "1ILU/A=="
            }
         },
         {
            "id": 561396398,
            "name": {
               "value": "How many total steals and blocks will Joel Embiid (Phi) record?",
               "sign": "DgAovw=="
            },
            "results": [
               {
                  "id": 1630347515,
                  "odds": 2.1,
                  "name": {
                     "value": "Over 2.5",
                     "sign": "xYq9vA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "2.5",
                  "numerator": 11,
                  "denominator": 10,
                  "americanOdds": 110
               },
               {
                  "id": 1630347516,
                  "odds": 1.72,
                  "name": {
                     "value": "Under 2.5",
                     "sign": "ZBWOiw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "2.5",
                  "numerator": 8,
                  "denominator": 11,
                  "americanOdds": -140
               }
            ],
            "templateId": 33717,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "kc8d5g=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 1,
                     "subIndex": 1,
                     "marketTabId": 19,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Joel Embiid (Phi)",
               "sign": "jOiirw==",
               "short": "J. Embiid",
               "shortSign": "hzQ5yQ=="
            }
         },
         {
            "id": 561396399,
            "name": {
               "value": "How many total points, rebounds and assists will Joel Embiid (Phi) record?",
               "sign": "XYA/qw=="
            },
            "results": [
               {
                  "id": 1630347517,
                  "odds": 1.85,
                  "name": {
                     "value": "Over 42.5",
                     "sign": "7wK5nQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "42.5",
                  "numerator": 17,
                  "denominator": 20,
                  "americanOdds": -115
               },
               {
                  "id": 1630347518,
                  "odds": 1.95,
                  "name": {
                     "value": "Under 42.5",
                     "sign": "x4XOQg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "42.5",
                  "numerator": 19,
                  "denominator": 20,
                  "americanOdds": -105
               }
            ],
            "templateId": 32108,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "QCf66A=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 6,
                     "marketTabId": 16,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Joel Embiid (Phi)",
               "sign": "f3hQmQ==",
               "short": "J. Embiid",
               "shortSign": "iaSybA=="
            }
         },
         {
            "id": 561396400,
            "name": {
               "value": "How many three-pointers will Joel Embiid (Phi) record?",
               "sign": "tWNcHg=="
            },
            "results": [
               {
                  "id": 1630347519,
                  "odds": 2.45,
                  "name": {
                     "value": "Over 1.5",
                     "sign": "w6r91w==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "1.5",
                  "numerator": 29,
                  "denominator": 20,
                  "americanOdds": 145
               },
               {
                  "id": 1630347520,
                  "odds": 1.53,
                  "name": {
                     "value": "Under 1.5",
                     "sign": "rtRlIQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "1.5",
                  "numerator": 8,
                  "denominator": 15,
                  "americanOdds": -190
               }
            ],
            "templateId": 12857,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "73YMWQ=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 4,
                     "marketTabId": 13,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Joel Embiid (Phi)",
               "sign": "zQijrA==",
               "short": "J. Embiid",
               "shortSign": "+Oz+1g=="
            }
         },
         {
            "id": 561396401,
            "name": {
               "value": "How many total points and rebounds will Joel Embiid (Phi) record?",
               "sign": "qXe+jg=="
            },
            "results": [
               {
                  "id": 1630347521,
                  "odds": 1.87,
                  "name": {
                     "value": "Over 39.5",
                     "sign": "JKqWpA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "39.5",
                  "numerator": 20,
                  "denominator": 23,
                  "americanOdds": -115
               },
               {
                  "id": 1630347522,
                  "odds": 1.9,
                  "name": {
                     "value": "Under 39.5",
                     "sign": "Hj3iug==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "39.5",
                  "numerator": 9,
                  "denominator": 10,
                  "americanOdds": -110
               }
            ],
            "templateId": 13494,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "Pp7rVw=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 1,
                     "subIndex": 3,
                     "marketTabId": 24,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Joel Embiid (Phi)",
               "sign": "PphRmg==",
               "short": "J. Embiid",
               "shortSign": "9nx1cw=="
            }
         },
         {
            "id": 561396402,
            "name": {
               "value": "How many assists will Joel Embiid (Phi) record?",
               "sign": "trkrog=="
            },
            "results": [
               {
                  "id": 1630347523,
                  "odds": 2.3,
                  "name": {
                     "value": "Over 3.5",
                     "sign": "ALjZHg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "3.5",
                  "numerator": 13,
                  "denominator": 10,
                  "americanOdds": 130
               },
               {
                  "id": 1630347524,
                  "odds": 1.6,
                  "name": {
                     "value": "Under 3.5",
                     "sign": "O0pc2Q==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "3.5",
                  "numerator": 3,
                  "denominator": 5,
                  "americanOdds": -165
               }
            ],
            "templateId": 29302,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "TafDRA=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 1,
                     "marketTabId": 11,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Joel Embiid (Phi)",
               "sign": "KylGwQ==",
               "short": "J. Embiid",
               "shortSign": "pcqYRg=="
            }
         },
         {
            "id": 561396403,
            "name": {
               "value": "How many points will Joel Embiid (Phi) score?",
               "sign": "GAY6EA=="
            },
            "results": [
               {
                  "id": 1630347525,
                  "odds": 1.9,
                  "name": {
                     "value": "Over 26.5",
                     "sign": "h8DL7A==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "26.5",
                  "numerator": 9,
                  "denominator": 10,
                  "americanOdds": -110
               },
               {
                  "id": 1630347526,
                  "odds": 1.9,
                  "name": {
                     "value": "Under 26.5",
                     "sign": "mIuWIw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "26.5",
                  "numerator": 9,
                  "denominator": 10,
                  "americanOdds": -110
               }
            ],
            "templateId": 29300,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "nE8kSg=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 0,
                     "marketTabId": 9,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Joel Embiid (Phi)",
               "sign": "2Lm09w==",
               "short": "J. Embiid",
               "shortSign": "q1oT4w=="
            }
         },
         {
            "id": 561396404,
            "name": {
               "value": "How many rebounds will Joel Embiid (Phi) record?",
               "sign": "KUM8Hg=="
            },
            "results": [
               {
                  "id": 1630347527,
                  "odds": 2.0,
                  "name": {
                     "value": "Over 13.5",
                     "sign": "34vTaA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "13.5",
                  "numerator": 1,
                  "denominator": 1,
                  "americanOdds": 100
               },
               {
                  "id": 1630347528,
                  "odds": 1.78,
                  "name": {
                     "value": "Under 13.5",
                     "sign": "kBYfXQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "13.5",
                  "numerator": 4,
                  "denominator": 5,
                  "americanOdds": -130
               }
            ],
            "templateId": 29301,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "q9WTYg=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 2,
                     "marketTabId": 10,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Joel Embiid (Phi)",
               "sign": "AUtpdw==",
               "short": "J. Embiid",
               "shortSign": "A6ZDLQ=="
            }
         },
         {
            "id": 561396405,
            "name": {
               "value": "How many total points and assists will Joel Embiid (Phi) record?",
               "sign": "ZLBG1w=="
            },
            "results": [
               {
                  "id": 1630347529,
                  "odds": 1.85,
                  "name": {
                     "value": "Over 29.5",
                     "sign": "9l4hMA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "29.5",
                  "numerator": 17,
                  "denominator": 20,
                  "americanOdds": -115
               },
               {
                  "id": 1630347530,
                  "odds": 1.91,
                  "name": {
                     "value": "Under 29.5",
                     "sign": "x3d3Vw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "29.5",
                  "numerator": 10,
                  "denominator": 11,
                  "americanOdds": -110
               }
            ],
            "templateId": 13495,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "ej10bA=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 1,
                     "subIndex": 2,
                     "marketTabId": 23,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Joel Embiid (Phi)",
               "sign": "8tubQQ==",
               "short": "J. Embiid",
               "shortSign": "DTbIiA=="
            }
         },
         {
            "id": 561396406,
            "name": {
               "value": "How many steals will Joel Embiid (Phi) record?",
               "sign": "QG8Jew=="
            },
            "results": [
               {
                  "id": 1630347531,
                  "odds": 1.45,
                  "name": {
                     "value": "Over 0.5",
                     "sign": "5zqSzg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "0.5",
                  "numerator": 5,
                  "denominator": 11,
                  "americanOdds": -225
               },
               {
                  "id": 1630347532,
                  "odds": 2.65,
                  "name": {
                     "value": "Under 0.5",
                     "sign": "1WcR9w==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "0.5",
                  "numerator": 33,
                  "denominator": 20,
                  "americanOdds": 165
               }
            ],
            "templateId": 29303,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "CQRcfw=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 3,
                     "marketTabId": 12,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Joel Embiid (Phi)",
               "sign": "52qMGg==",
               "short": "J. Embiid",
               "shortSign": "XoAlvQ=="
            }
         },
         {
            "id": 561396407,
            "name": {
               "value": "How many turnovers will Joel Embiid (Phi) record?",
               "sign": "QftD9w=="
            },
            "results": [
               {
                  "id": 1630347533,
                  "odds": 2.35,
                  "name": {
                     "value": "Over 3.5",
                     "sign": "7tbpfA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "3.5",
                  "numerator": 27,
                  "denominator": 20,
                  "americanOdds": 135
               },
               {
                  "id": 1630347534,
                  "odds": 1.57,
                  "name": {
                     "value": "Under 3.5",
                     "sign": "0f8xZQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "3.5",
                  "numerator": 4,
                  "denominator": 7,
                  "americanOdds": -175
               }
            ],
            "templateId": 12869,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "2Oy7cQ=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 1,
                     "subIndex": 0,
                     "marketTabId": 15,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Joel Embiid (Phi)",
               "sign": "FPp+LA==",
               "short": "J. Embiid",
               "shortSign": "UBCuGA=="
            }
         },
         {
            "id": 561396408,
            "name": {
               "value": "How many total rebounds and assists will Joel Embiid (Phi) record?",
               "sign": "DVpLyA=="
            },
            "results": [
               {
                  "id": 1630347535,
                  "odds": 1.91,
                  "name": {
                     "value": "Over 16.5",
                     "sign": "g9oTQg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "16.5",
                  "numerator": 10,
                  "denominator": 11,
                  "americanOdds": -110
               },
               {
                  "id": 1630347536,
                  "odds": 1.85,
                  "name": {
                     "value": "Under 16.5",
                     "sign": "v6KvpA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "16.5",
                  "numerator": 17,
                  "denominator": 20,
                  "americanOdds": -115
               }
            ],
            "templateId": 33718,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "ZzAzLg=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 1,
                     "subIndex": 4,
                     "marketTabId": 20,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Joel Embiid (Phi)",
               "sign": "FIlGwA==",
               "short": "J. Embiid",
               "shortSign": "T3/1+g=="
            }
         },
         {
            "id": 561396409,
            "name": {
               "value": "How many turnovers will Seth Curry (Phi) record?",
               "sign": "84qIOQ=="
            },
            "results": [
               {
                  "id": 1630347537,
                  "odds": 2.75,
                  "name": {
                     "value": "Over 1.5",
                     "sign": "XN8Nuw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "1.5",
                  "numerator": 7,
                  "denominator": 4,
                  "americanOdds": 175
               },
               {
                  "id": 1630347538,
                  "odds": 1.44,
                  "name": {
                     "value": "Under 1.5",
                     "sign": "ndUU6Q==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "1.5",
                  "numerator": 4,
                  "denominator": 9,
                  "americanOdds": -225
               }
            ],
            "templateId": 12869,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "ttjUIA=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 1,
                     "subIndex": 0,
                     "marketTabId": 15,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Seth Curry (Phi)",
               "sign": "rIGMHA==",
               "short": "Se. Curry",
               "shortSign": "9TWQSg=="
            }
         },
         {
            "id": 561396410,
            "name": {
               "value": "How many rebounds will Seth Curry (Phi) record?",
               "sign": "jWuUww=="
            },
            "results": [
               {
                  "id": 1630347539,
                  "odds": 1.6,
                  "name": {
                     "value": "Over 1.5",
                     "sign": "UxM2Yg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "1.5",
                  "numerator": 3,
                  "denominator": 5,
                  "americanOdds": -165
               },
               {
                  "id": 1630347540,
                  "odds": 2.3,
                  "name": {
                     "value": "Under 1.5",
                     "sign": "O7nPgg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "1.5",
                  "numerator": 13,
                  "denominator": 10,
                  "americanOdds": 130
               }
            ],
            "templateId": 29301,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "xeH8Mw=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 2,
                     "marketTabId": 10,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Seth Curry (Phi)",
               "sign": "E4mT1Q==",
               "short": "Se. Curry",
               "shortSign": "poN9fw=="
            }
         },
         {
            "id": 561396411,
            "name": {
               "value": "How many total rebounds and assists will Seth Curry (Phi) record?",
               "sign": "Esa+BA=="
            },
            "results": [
               {
                  "id": 1630347541,
                  "odds": 2.05,
                  "name": {
                     "value": "Over 5.5",
                     "sign": "3+kC1Q==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "5.5",
                  "numerator": 21,
                  "denominator": 20,
                  "americanOdds": 105
               },
               {
                  "id": 1630347542,
                  "odds": 1.75,
                  "name": {
                     "value": "Under 5.5",
                     "sign": "ujegFQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "5.5",
                  "numerator": 3,
                  "denominator": 4,
                  "americanOdds": -135
               }
            ],
            "templateId": 33718,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "FAkbPQ=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 1,
                     "subIndex": 4,
                     "marketTabId": 20,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Seth Curry (Phi)",
               "sign": "uYxJJA==",
               "short": "Se. Curry",
               "shortSign": "qBP22g=="
            }
         },
         {
            "id": 561396412,
            "name": {
               "value": "How many points will Seth Curry (Phi) score?",
               "sign": "GCC8sQ=="
            },
            "results": [
               {
                  "id": 1630347543,
                  "odds": 1.91,
                  "name": {
                     "value": "Over 13.5",
                     "sign": "sazEMA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "13.5",
                  "numerator": 10,
                  "denominator": 11,
                  "americanOdds": -110
               },
               {
                  "id": 1630347544,
                  "odds": 1.85,
                  "name": {
                     "value": "Under 13.5",
                     "sign": "6E1N9w==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "13.5",
                  "numerator": 17,
                  "denominator": 20,
                  "americanOdds": -115
               }
            ],
            "templateId": 29300,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "I5OsFQ=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 0,
                     "marketTabId": 9,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Seth Curry (Phi)",
               "sign": "LJ7cnA==",
               "short": "Se. Curry",
               "shortSign": "AO+mFA=="
            }
         },
         {
            "id": 561396413,
            "name": {
               "value": "How many assists will Seth Curry (Phi) record?",
               "sign": "A3cWZg=="
            },
            "results": [
               {
                  "id": 1630347545,
                  "odds": 1.57,
                  "name": {
                     "value": "Over 2.5",
                     "sign": "u11Gaw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "2.5",
                  "numerator": 4,
                  "denominator": 7,
                  "americanOdds": -175
               },
               {
                  "id": 1630347546,
                  "odds": 2.35,
                  "name": {
                     "value": "Under 2.5",
                     "sign": "c/hZxw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "2.5",
                  "numerator": 27,
                  "denominator": 20,
                  "americanOdds": 135
               }
            ],
            "templateId": 29302,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "8ntLGw=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 1,
                     "marketTabId": 11,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Seth Curry (Phi)",
               "sign": "hpsGbQ==",
               "short": "Se. Curry",
               "shortSign": "Dn8tsQ=="
            }
         },
         {
            "id": 561396416,
            "name": {
               "value": "How many steals will Seth Curry (Phi) record?",
               "sign": "8jD94A=="
            },
            "results": [
               {
                  "id": 1630347551,
                  "odds": 1.75,
                  "name": {
                     "value": "Over 0.5",
                     "sign": "hdv/2A==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "0.5",
                  "numerator": 3,
                  "denominator": 4,
                  "americanOdds": -135
               },
               {
                  "id": 1630347552,
                  "odds": 2.05,
                  "name": {
                     "value": "Under 0.5",
                     "sign": "RfCJkA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "0.5",
                  "numerator": 21,
                  "denominator": 20,
                  "americanOdds": 105
               }
            ],
            "templateId": 29303,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "XN/mvw=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 3,
                     "marketTabId": 12,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Seth Curry (Phi)",
               "sign": "zbjMXA==",
               "short": "Se. Curry",
               "shortSign": "B8QEkA=="
            }
         },
         {
            "id": 561396417,
            "name": {
               "value": "How many total points, rebounds and assists will Seth Curry (Phi) record?",
               "sign": "dJsIhg=="
            },
            "results": [
               {
                  "id": 1630347553,
                  "odds": 1.9,
                  "name": {
                     "value": "Over 18.5",
                     "sign": "REZzvw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "18.5",
                  "numerator": 9,
                  "denominator": 10,
                  "americanOdds": -110
               },
               {
                  "id": 1630347554,
                  "odds": 1.87,
                  "name": {
                     "value": "Under 18.5",
                     "sign": "Ey/8ng==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "18.5",
                  "numerator": 20,
                  "denominator": 23,
                  "americanOdds": -115
               }
            ],
            "templateId": 32108,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "jTcBsQ=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 6,
                     "marketTabId": 16,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Seth Curry (Phi)",
               "sign": "Z70WrQ==",
               "short": "Se. Curry",
               "shortSign": "CVSPNQ=="
            }
         },
         {
            "id": 561396418,
            "name": {
               "value": "How many blocks will Seth Curry (Phi) record?",
               "sign": "5ef+iQ=="
            },
            "results": [
               {
                  "id": 1630347555,
                  "odds": 4.25,
                  "name": {
                     "value": "Over 0.5",
                     "sign": "I/hJ4g==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "0.5",
                  "numerator": 13,
                  "denominator": 4,
                  "americanOdds": 325
               },
               {
                  "id": 1630347556,
                  "odds": 1.22,
                  "name": {
                     "value": "Under 0.5",
                     "sign": "vro0aw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "0.5",
                  "numerator": 2,
                  "denominator": 9,
                  "americanOdds": -450
               }
            ],
            "templateId": 29304,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "/g4pog=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 5,
                     "marketTabId": 14,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Seth Curry (Phi)",
               "sign": "2LUJZA==",
               "short": "Se. Curry",
               "shortSign": "WuJiAA=="
            }
         },
         {
            "id": 561396419,
            "name": {
               "value": "How many total points and assists will Seth Curry (Phi) record?",
               "sign": "UJSU4g=="
            },
            "results": [
               {
                  "id": 1630347557,
                  "odds": 1.9,
                  "name": {
                     "value": "Over 16.5",
                     "sign": "tSFQTg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "16.5",
                  "numerator": 9,
                  "denominator": 10,
                  "americanOdds": -110
               },
               {
                  "id": 1630347558,
                  "odds": 1.87,
                  "name": {
                     "value": "Under 16.5",
                     "sign": "x5T2vg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "16.5",
                  "numerator": 20,
                  "denominator": 23,
                  "americanOdds": -115
               }
            ],
            "templateId": 13495,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "L+bOrA=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 1,
                     "subIndex": 2,
                     "marketTabId": 23,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Seth Curry (Phi)",
               "sign": "crDTlQ==",
               "short": "Se. Curry",
               "shortSign": "VHLppQ=="
            }
         },
         {
            "id": 561396420,
            "name": {
               "value": "How many three-pointers will Seth Curry (Phi) make?",
               "sign": "pkGsIA=="
            },
            "results": [
               {
                  "id": 1630347559,
                  "odds": 1.87,
                  "name": {
                     "value": "Over 2.5",
                     "sign": "ErLLiA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "2.5",
                  "numerator": 20,
                  "denominator": 23,
                  "americanOdds": -115
               },
               {
                  "id": 1630347560,
                  "odds": 1.9,
                  "name": {
                     "value": "Under 2.5",
                     "sign": "nLcGvw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "2.5",
                  "numerator": 9,
                  "denominator": 10,
                  "americanOdds": -110
               }
            ],
            "templateId": 12857,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "GHx5hA=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 4,
                     "marketTabId": 13,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Seth Curry (Phi)",
               "sign": "56JGLQ==",
               "short": "Se. Curry",
               "shortSign": "/I65aw=="
            }
         },
         {
            "id": 561396421,
            "name": {
               "value": "How many total steals and blocks will Seth Curry (Phi) record?",
               "sign": "EXz8OA=="
            },
            "results": [
               {
                  "id": 1630347561,
                  "odds": 1.5,
                  "name": {
                     "value": "Over 0.5",
                     "sign": "kgh/6Q==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "0.5",
                  "numerator": 1,
                  "denominator": 2,
                  "americanOdds": -200
               },
               {
                  "id": 1630347562,
                  "odds": 2.5,
                  "name": {
                     "value": "Under 0.5",
                     "sign": "r0XkLA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "0.5",
                  "numerator": 6,
                  "denominator": 4,
                  "americanOdds": 150
               }
            ],
            "templateId": 33717,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "yZSeig=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 1,
                     "subIndex": 1,
                     "marketTabId": 19,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Seth Curry (Phi)",
               "sign": "Taec3A==",
               "short": "Se. Curry",
               "shortSign": "8h4yzg=="
            }
         },
         {
            "id": 561396422,
            "name": {
               "value": "How many total points and rebounds will Seth Curry (Phi) record?",
               "sign": "NAOiNQ=="
            },
            "results": [
               {
                  "id": 1630347563,
                  "odds": 1.91,
                  "name": {
                     "value": "Over 15.5",
                     "sign": "/WDGCw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "15.5",
                  "numerator": 10,
                  "denominator": 11,
                  "americanOdds": -110
               },
               {
                  "id": 1630347564,
                  "odds": 1.85,
                  "name": {
                     "value": "Under 15.5",
                     "sign": "TUzT/A==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "15.5",
                  "numerator": 17,
                  "denominator": 20,
                  "americanOdds": -115
               }
            ],
            "templateId": 13494,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "uq22mQ=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 1,
                     "subIndex": 3,
                     "marketTabId": 24,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Seth Curry (Phi)",
               "sign": "8q+DFQ==",
               "short": "Se. Curry",
               "shortSign": "oajf+w=="
            }
         },
         {
            "id": 561396423,
            "name": {
               "value": "How many total rebounds and assists will Tobias Harris (Phi) record?",
               "sign": "aiFXRA=="
            },
            "results": [
               {
                  "id": 1630347565,
                  "odds": 1.9,
                  "name": {
                     "value": "Over 10.5",
                     "sign": "sM7WZg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "10.5",
                  "numerator": 9,
                  "denominator": 10,
                  "americanOdds": -110
               },
               {
                  "id": 1630347566,
                  "odds": 1.9,
                  "name": {
                     "value": "Under 10.5",
                     "sign": "ycVS7w==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "10.5",
                  "numerator": 9,
                  "denominator": 10,
                  "americanOdds": -110
               }
            ],
            "templateId": 33718,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "a0VRlw=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 1,
                     "subIndex": 4,
                     "marketTabId": 20,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Tobias Harris (Phi)",
               "sign": "gEG+Bw==",
               "short": "T. Harris",
               "shortSign": "7htvlQ=="
            }
         },
         {
            "id": 561396424,
            "name": {
               "value": "How many three-pointers will Tobias Harris (Phi) record?",
               "sign": "m7+FvQ=="
            },
            "results": [
               {
                  "id": 1630347567,
                  "odds": 2.25,
                  "name": {
                     "value": "Over 2.5",
                     "sign": "rI7GWg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "2.5",
                  "numerator": 5,
                  "denominator": 4,
                  "americanOdds": 125
               },
               {
                  "id": 1630347568,
                  "odds": 1.62,
                  "name": {
                     "value": "Under 2.5",
                     "sign": "RQMayw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "2.5",
                  "numerator": 8,
                  "denominator": 13,
                  "americanOdds": -160
               }
            ],
            "templateId": 12857,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "1JnZyA=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 4,
                     "marketTabId": 13,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Tobias Harris (Phi)",
               "sign": "Po2yzg==",
               "short": "T. Harris",
               "shortSign": "8XQ0dw=="
            }
         },
         {
            "id": 561396425,
            "name": {
               "value": "How many assists will Tobias Harris (Phi) record?",
               "sign": "dMrzOg=="
            },
            "results": [
               {
                  "id": 1630347569,
                  "odds": 2.1,
                  "name": {
                     "value": "Over 3.5",
                     "sign": "SPVfRg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "3.5",
                  "numerator": 11,
                  "denominator": 10,
                  "americanOdds": 110
               },
               {
                  "id": 1630347570,
                  "odds": 1.72,
                  "name": {
                     "value": "Under 3.5",
                     "sign": "L0++Wg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "3.5",
                  "numerator": 8,
                  "denominator": 11,
                  "americanOdds": -140
               }
            ],
            "templateId": 29302,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "BXE+xg=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 1,
                     "marketTabId": 11,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Tobias Harris (Phi)",
               "sign": "9posAQ==",
               "short": "T. Harris",
               "shortSign": "/+S/0g=="
            }
         },
         {
            "id": 561396426,
            "name": {
               "value": "How many total points and rebounds will Tobias Harris (Phi) record?",
               "sign": "hEgKKQ=="
            },
            "results": [
               {
                  "id": 1630347571,
                  "odds": 1.95,
                  "name": {
                     "value": "Over 26.5",
                     "sign": "k8Upbw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "26.5",
                  "numerator": 19,
                  "denominator": 20,
                  "americanOdds": -105
               },
               {
                  "id": 1630347572,
                  "odds": 1.83,
                  "name": {
                     "value": "Under 26.5",
                     "sign": "PitbEw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "26.5",
                  "numerator": 5,
                  "denominator": 6,
                  "americanOdds": -120
               }
            ],
            "templateId": 13494,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "dkgW1Q=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 1,
                     "subIndex": 3,
                     "marketTabId": 24,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Tobias Harris (Phi)",
               "sign": "76T/ig==",
               "short": "T. Harris",
               "shortSign": "rFJS5w=="
            }
         },
         {
            "id": 561396427,
            "name": {
               "value": "How many turnovers will Tobias Harris (Phi) record?",
               "sign": "gQ7p/g=="
            },
            "results": [
               {
                  "id": 1630347573,
                  "odds": 1.91,
                  "name": {
                     "value": "Over 1.5",
                     "sign": "eb/dLA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "1.5",
                  "numerator": 10,
                  "denominator": 11,
                  "americanOdds": -110
               },
               {
                  "id": 1630347574,
                  "odds": 1.87,
                  "name": {
                     "value": "Under 1.5",
                     "sign": "utGHog==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "1.5",
                  "numerator": 20,
                  "denominator": 23,
                  "americanOdds": -115
               }
            ],
            "templateId": 12869,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "p6Dx2w=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 1,
                     "subIndex": 0,
                     "marketTabId": 15,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Tobias Harris (Phi)",
               "sign": "J7NhRQ==",
               "short": "T. Harris",
               "shortSign": "osLZQg=="
            }
         },
         {
            "id": 561396428,
            "name": {
               "value": "How many points will Tobias Harris (Phi) score?",
               "sign": "7R3E8A=="
            },
            "results": [
               {
                  "id": 1630347575,
                  "odds": 1.8,
                  "name": {
                     "value": "Over 18.5",
                     "sign": "jA2/jA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "18.5",
                  "numerator": 4,
                  "denominator": 5,
                  "americanOdds": -125
               },
               {
                  "id": 1630347576,
                  "odds": 1.95,
                  "name": {
                     "value": "Under 18.5",
                     "sign": "uBLNdA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "18.5",
                  "numerator": 19,
                  "denominator": 20,
                  "americanOdds": -105
               }
            ],
            "templateId": 29300,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "kDpG8w=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 0,
                     "marketTabId": 9,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Tobias Harris (Phi)",
               "sign": "nN4oRg==",
               "short": "T. Harris",
               "shortSign": "Cj6JjA=="
            }
         },
         {
            "id": 561396429,
            "name": {
               "value": "How many blocks will Tobias Harris (Phi) record?",
               "sign": "R31pbg=="
            },
            "results": [
               {
                  "id": 1630347577,
                  "odds": 1.83,
                  "name": {
                     "value": "Over 0.5",
                     "sign": "r3cUlg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "0.5",
                  "numerator": 5,
                  "denominator": 6,
                  "americanOdds": -120
               },
               {
                  "id": 1630347578,
                  "odds": 1.95,
                  "name": {
                     "value": "Under 0.5",
                     "sign": "wWLzdA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "0.5",
                  "numerator": 19,
                  "denominator": 20,
                  "americanOdds": -105
               }
            ],
            "templateId": 29304,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "QdKh/Q=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 5,
                     "marketTabId": 14,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Tobias Harris (Phi)",
               "sign": "VMm2iQ==",
               "short": "T. Harris",
               "shortSign": "BK4CKQ=="
            }
         },
         {
            "id": 561396430,
            "name": {
               "value": "How many total steals and blocks will Tobias Harris (Phi) record?",
               "sign": "z3NXdw=="
            },
            "results": [
               {
                  "id": 1630347579,
                  "odds": 2.0,
                  "name": {
                     "value": "Over 1.5",
                     "sign": "l9HtTg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "1.5",
                  "numerator": 1,
                  "denominator": 1,
                  "americanOdds": 100
               },
               {
                  "id": 1630347580,
                  "odds": 1.8,
                  "name": {
                     "value": "Under 1.5",
                     "sign": "UGTqHg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "1.5",
                  "numerator": 4,
                  "denominator": 5,
                  "americanOdds": -125
               }
            ],
            "templateId": 33717,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "MuuJ7g=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 1,
                     "subIndex": 1,
                     "marketTabId": 19,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Tobias Harris (Phi)",
               "sign": "TfdlAg==",
               "short": "T. Harris",
               "shortSign": "VxjvHA=="
            }
         },
         {
            "id": 561396431,
            "name": {
               "value": "How many total points, rebounds and assists will Tobias Harris (Phi) record?",
               "sign": "NaiHHA=="
            },
            "results": [
               {
                  "id": 1630347581,
                  "odds": 1.87,
                  "name": {
                     "value": "Over 29.5",
                     "sign": "v32lIw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "29.5",
                  "numerator": 20,
                  "denominator": 23,
                  "americanOdds": -115
               },
               {
                  "id": 1630347582,
                  "odds": 1.9,
                  "name": {
                     "value": "Under 29.5",
                     "sign": "0ApkWA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "29.5",
                  "numerator": 9,
                  "denominator": 10,
                  "americanOdds": -110
               }
            ],
            "templateId": 32108,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "4wNu4A=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 6,
                     "marketTabId": 16,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Tobias Harris (Phi)",
               "sign": "heD7zQ==",
               "short": "T. Harris",
               "shortSign": "WYhkuQ=="
            }
         },
         {
            "id": 561396433,
            "name": {
               "value": "How many total points and assists will Tobias Harris (Phi) record?",
               "sign": "s77Y1A=="
            },
            "results": [
               {
                  "id": 1630347585,
                  "odds": 1.91,
                  "name": {
                     "value": "Over 22.5",
                     "sign": "WblSqw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "22.5",
                  "numerator": 10,
                  "denominator": 11,
                  "americanOdds": -110
               },
               {
                  "id": 1630347586,
                  "odds": 1.87,
                  "name": {
                     "value": "Under 22.5",
                     "sign": "udLRyg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "22.5",
                  "numerator": 20,
                  "denominator": 23,
                  "americanOdds": -115
               }
            ],
            "templateId": 13495,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "nbp/Xw=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 1,
                     "subIndex": 2,
                     "marketTabId": 23,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Tobias Harris (Phi)",
               "sign": "uH+ThA==",
               "short": "T. Harris",
               "shortSign": "JlCjpg=="
            }
         },
         {
            "id": 561396434,
            "name": {
               "value": "How many rebounds will Tobias Harris (Phi) record?",
               "sign": "nfA/zw=="
            },
            "results": [
               {
                  "id": 1630347587,
                  "odds": 2.15,
                  "name": {
                     "value": "Over 7.5",
                     "sign": "aeoMPw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "7.5",
                  "numerator": 23,
                  "denominator": 20,
                  "americanOdds": 115
               },
               {
                  "id": 1630347588,
                  "odds": 1.67,
                  "name": {
                     "value": "Under 7.5",
                     "sign": "Hnl4ZQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "7.5",
                  "numerator": 4,
                  "denominator": 6,
                  "americanOdds": -150
               }
            ],
            "templateId": 29301,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "7oNXTA=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 2,
                     "marketTabId": 10,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Tobias Harris (Phi)",
               "sign": "oUFADw==",
               "short": "T. Harris",
               "shortSign": "deZOkw=="
            }
         },
         {
            "id": 561396435,
            "name": {
               "value": "How many steals will Tobias Harris (Phi) record?",
               "sign": "HfvXTQ=="
            },
            "results": [
               {
                  "id": 1630347589,
                  "odds": 1.65,
                  "name": {
                     "value": "Over 0.5",
                     "sign": "vK5+ig==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "0.5",
                  "numerator": 13,
                  "denominator": 20,
                  "americanOdds": -155
               },
               {
                  "id": 1630347590,
                  "odds": 2.25,
                  "name": {
                     "value": "Under 0.5",
                     "sign": "xklR8A==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "0.5",
                  "numerator": 5,
                  "denominator": 4,
                  "americanOdds": 125
               }
            ],
            "templateId": 29303,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "P2uwQg=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 3,
                     "marketTabId": 12,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Tobias Harris (Phi)",
               "sign": "aVbewA==",
               "short": "T. Harris",
               "shortSign": "e3bFNg=="
            }
         },
         {
            "id": 561396436,
            "name": {
               "value": "How many total steals and blocks will Bismack Biyombo (Cha) record?",
               "sign": "Vf/DIA=="
            },
            "results": [
               {
                  "id": 1630347591,
                  "odds": 2.55,
                  "name": {
                     "value": "Over 1.5",
                     "sign": "hAiHUg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "1.5",
                  "numerator": 31,
                  "denominator": 20,
                  "americanOdds": 155
               },
               {
                  "id": 1630347592,
                  "odds": 1.5,
                  "name": {
                     "value": "Under 1.5",
                     "sign": "4NxDtg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "1.5",
                  "numerator": 1,
                  "denominator": 2,
                  "americanOdds": -200
               }
            ],
            "templateId": 33717,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "CPEHag=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 1,
                     "subIndex": 1,
                     "marketTabId": 19,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Bismack Biyombo (Cha)",
               "sign": "mbrH+A==",
               "short": "B. Biyombo",
               "shortSign": "s5zN8w=="
            }
         },
         {
            "id": 561396437,
            "name": {
               "value": "How many blocks will Bismack Biyombo (Cha) record?",
               "sign": "jj6/Cw=="
            },
            "results": [
               {
                  "id": 1630347593,
                  "odds": 1.62,
                  "name": {
                     "value": "Over 0.5",
                     "sign": "XQx1MQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "0.5",
                  "numerator": 8,
                  "denominator": 13,
                  "americanOdds": -160
               },
               {
                  "id": 1630347594,
                  "odds": 2.25,
                  "name": {
                     "value": "Under 0.5",
                     "sign": "ipDnJw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "0.5",
                  "numerator": 5,
                  "denominator": 4,
                  "americanOdds": 125
               }
            ],
            "templateId": 29304,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "2RngZA=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 5,
                     "marketTabId": 14,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Bismack Biyombo (Cha)",
               "sign": "hq03eA==",
               "short": "B. Biyombo",
               "shortSign": "JDrQFA=="
            }
         },
         {
            "id": 561396438,
            "name": {
               "value": "How many rebounds will Bismack Biyombo (Cha) record?",
               "sign": "VI2Eyw=="
            },
            "results": [
               {
                  "id": 1630347595,
                  "odds": 1.9,
                  "name": {
                     "value": "Over 7.5",
                     "sign": "19YB7Q==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "7.5",
                  "numerator": 9,
                  "denominator": 10,
                  "americanOdds": -110
               },
               {
                  "id": 1630347596,
                  "odds": 1.87,
                  "name": {
                     "value": "Under 7.5",
                     "sign": "qepzSQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "7.5",
                  "numerator": 20,
                  "denominator": 23,
                  "americanOdds": -115
               }
            ],
            "templateId": 29301,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "qiDIdw=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 2,
                     "marketTabId": 10,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Bismack Biyombo (Cha)",
               "sign": "5pJWIg==",
               "short": "B. Biyombo",
               "shortSign": "3NeH5g=="
            }
         },
         {
            "id": 561396439,
            "name": {
               "value": "How many assists will Bismack Biyombo (Cha) record?",
               "sign": "n4XfXA=="
            },
            "results": [
               {
                  "id": 1630347597,
                  "odds": 1.75,
                  "name": {
                     "value": "Over 1.5",
                     "sign": "NfixWQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "1.5",
                  "numerator": 3,
                  "denominator": 4,
                  "americanOdds": -135
               },
               {
                  "id": 1630347598,
                  "odds": 2.05,
                  "name": {
                     "value": "Under 1.5",
                     "sign": "RrCY3Q==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "1.5",
                  "numerator": 21,
                  "denominator": 20,
                  "americanOdds": 105
               }
            ],
            "templateId": 29302,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "e8gveQ=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 1,
                     "marketTabId": 11,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Bismack Biyombo (Cha)",
               "sign": "+YWmog==",
               "short": "B. Biyombo",
               "shortSign": "S3GaAQ=="
            }
         },
         {
            "id": 561396440,
            "name": {
               "value": "How many turnovers will Bismack Biyombo (Cha) record?",
               "sign": "giMyoQ=="
            },
            "results": [
               {
                  "id": 1630347599,
                  "odds": 2.25,
                  "name": {
                     "value": "Over 1.5",
                     "sign": "OjSKgA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "1.5",
                  "numerator": 5,
                  "denominator": 4,
                  "americanOdds": 125
               },
               {
                  "id": 1630347600,
                  "odds": 1.65,
                  "name": {
                     "value": "Under 1.5",
                     "sign": "OWhfwg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "1.5",
                  "numerator": 13,
                  "denominator": 20,
                  "americanOdds": -155
               }
            ],
            "templateId": 12869,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "xBSnJg=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 1,
                     "subIndex": 0,
                     "marketTabId": 15,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Bismack Biyombo (Cha)",
               "sign": "WEEySg==",
               "short": "B. Biyombo",
               "shortSign": "0SdwjA=="
            }
         },
         {
            "id": 561396441,
            "name": {
               "value": "How many total rebounds and assists will Bismack Biyombo (Cha) record?",
               "sign": "Sd/41A=="
            },
            "results": [
               {
                  "id": 1630347601,
                  "odds": 1.95,
                  "name": {
                     "value": "Over 9.5",
                     "sign": "UXTCkw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "9.5",
                  "numerator": 19,
                  "denominator": 20,
                  "americanOdds": -105
               },
               {
                  "id": 1630347602,
                  "odds": 1.83,
                  "name": {
                     "value": "Under 9.5",
                     "sign": "3B8qXA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "9.5",
                  "numerator": 5,
                  "denominator": 6,
                  "americanOdds": -120
               }
            ],
            "templateId": 33718,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "FfxAKA=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 1,
                     "subIndex": 4,
                     "marketTabId": 20,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Bismack Biyombo (Cha)",
               "sign": "R1bCyg==",
               "short": "B. Biyombo",
               "shortSign": "RoFtaw=="
            }
         },
         {
            "id": 561396442,
            "name": {
               "value": "How many total points, rebounds and assists will Bismack Biyombo (Cha) record?",
               "sign": "FF7cXQ=="
            },
            "results": [
               {
                  "id": 1630347603,
                  "odds": 1.83,
                  "name": {
                     "value": "Over 18.5",
                     "sign": "UpIBfA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "18.5",
                  "numerator": 5,
                  "denominator": 6,
                  "americanOdds": -120
               },
               {
                  "id": 1630347604,
                  "odds": 1.95,
                  "name": {
                     "value": "Under 18.5",
                     "sign": "SH5/QA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "18.5",
                  "numerator": 19,
                  "denominator": 20,
                  "americanOdds": -105
               }
            ],
            "templateId": 32108,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "ZsVoOw=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 6,
                     "marketTabId": 16,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Bismack Biyombo (Cha)",
               "sign": "J2mjkA==",
               "short": "B. Biyombo",
               "shortSign": "vmw6mQ=="
            }
         },
         {
            "id": 561396449,
            "name": {
               "value": "How many points will Bismack Biyombo (Cha) score?",
               "sign": "Tx8XhA=="
            },
            "results": [
               {
                  "id": 1630347617,
                  "odds": 1.87,
                  "name": {
                     "value": "Over 9.5",
                     "sign": "FvV/Eg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "9.5",
                  "numerator": 20,
                  "denominator": 23,
                  "americanOdds": -115
               },
               {
                  "id": 1630347618,
                  "odds": 1.91,
                  "name": {
                     "value": "Under 9.5",
                     "sign": "bnYTtA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "9.5",
                  "numerator": 10,
                  "denominator": 11,
                  "americanOdds": -110
               }
            ],
            "templateId": 29300,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "7CuNtg=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 0,
                     "marketTabId": 9,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Bismack Biyombo (Cha)",
               "sign": "/h4gsA==",
               "short": "B. Biyombo",
               "shortSign": "M0fDGw=="
            }
         },
         {
            "id": 561396451,
            "name": {
               "value": "How many total points and assists will Bismack Biyombo (Cha) record?",
               "sign": "R/OhkQ=="
            },
            "results": [
               {
                  "id": 1630347621,
                  "odds": 1.91,
                  "name": {
                     "value": "Over 11.5",
                     "sign": "yawy8A==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "11.5",
                  "numerator": 10,
                  "denominator": 11,
                  "americanOdds": -110
               },
               {
                  "id": 1630347622,
                  "odds": 1.85,
                  "name": {
                     "value": "Under 11.5",
                     "sign": "YeVjfw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "11.5",
                  "numerator": 17,
                  "denominator": 20,
                  "americanOdds": -115
               }
            ],
            "templateId": 13495,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "TvpCqw=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 1,
                     "subIndex": 2,
                     "marketTabId": 23,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Bismack Biyombo (Cha)",
               "sign": "gTaxag==",
               "short": "B. Biyombo",
               "shortSign": "XAyJDg=="
            }
         },
         {
            "id": 561396452,
            "name": {
               "value": "How many steals will Bismack Biyombo (Cha) record?",
               "sign": "MCF6wA=="
            },
            "results": [
               {
                  "id": 1630347623,
                  "odds": 2.85,
                  "name": {
                     "value": "Over 0.5",
                     "sign": "yZyTrQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "0.5",
                  "numerator": 15,
                  "denominator": 8,
                  "americanOdds": 185
               },
               {
                  "id": 1630347624,
                  "odds": 1.42,
                  "name": {
                     "value": "Under 0.5",
                     "sign": "C/ivBw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "0.5",
                  "numerator": 21,
                  "denominator": 50,
                  "americanOdds": -250
               }
            ],
            "templateId": 29303,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "eWD1gw=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 3,
                     "marketTabId": 12,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Bismack Biyombo (Cha)",
               "sign": "Xl+DXg==",
               "short": "B. Biyombo",
               "shortSign": "endK1g=="
            }
         },
         {
            "id": 561396453,
            "name": {
               "value": "How many total points and rebounds will Bismack Biyombo (Cha) record?",
               "sign": "4Pp4Hw=="
            },
            "results": [
               {
                  "id": 1630347625,
                  "odds": 1.95,
                  "name": {
                     "value": "Over 17.5",
                     "sign": "NwkJIw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "17.5",
                  "numerator": 19,
                  "denominator": 20,
                  "americanOdds": -105
               },
               {
                  "id": 1630347626,
                  "odds": 1.8,
                  "name": {
                     "value": "Under 17.5",
                     "sign": "sSJTBA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "17.5",
                  "numerator": 4,
                  "denominator": 5,
                  "americanOdds": -125
               }
            ],
            "templateId": 13494,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "qIgSjQ=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 1,
                     "subIndex": 3,
                     "marketTabId": 24,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Bismack Biyombo (Cha)",
               "sign": "QUhz3g==",
               "short": "B. Biyombo",
               "shortSign": "7dFXMQ=="
            }
         },
         {
            "id": 561396454,
            "name": {
               "value": "How many turnovers will Gordon Hayward (Cha) record?",
               "sign": "MOX4Og=="
            },
            "results": [
               {
                  "id": 1630347627,
                  "odds": 1.57,
                  "name": {
                     "value": "Over 1.5",
                     "sign": "H1RaFw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "1.5",
                  "numerator": 4,
                  "denominator": 7,
                  "americanOdds": -175
               },
               {
                  "id": 1630347628,
                  "odds": 2.35,
                  "name": {
                     "value": "Under 1.5",
                     "sign": "x9jQ/Q==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "1.5",
                  "numerator": 27,
                  "denominator": 20,
                  "americanOdds": 135
               }
            ],
            "templateId": 12869,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "27E6ng=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 1,
                     "subIndex": 0,
                     "marketTabId": 15,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Gordon Hayward (Cha)",
               "sign": "a0+Ocw==",
               "short": "G. Hayward",
               "shortSign": "v2etLA=="
            }
         },
         {
            "id": 561396455,
            "name": {
               "value": "How many points will Gordon Hayward (Cha) score?",
               "sign": "qH5ZcA=="
            },
            "results": [
               {
                  "id": 1630347629,
                  "odds": 1.8,
                  "name": {
                     "value": "Over 16.5",
                     "sign": "+yl22Q==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "16.5",
                  "numerator": 4,
                  "denominator": 5,
                  "americanOdds": -125
               },
               {
                  "id": 1630347630,
                  "odds": 1.95,
                  "name": {
                     "value": "Under 16.5",
                     "sign": "WN4FLw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "16.5",
                  "numerator": 19,
                  "denominator": 20,
                  "americanOdds": -105
               }
            ],
            "templateId": 29300,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "ClndkA=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 0,
                     "marketTabId": 9,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Gordon Hayward (Cha)",
               "sign": "/pv+5g==",
               "short": "G. Hayward",
               "shortSign": "KMGwyw=="
            }
         },
         {
            "id": 561396456,
            "name": {
               "value": "How many blocks will Gordon Hayward (Cha) record?",
               "sign": "0xiTxQ=="
            },
            "results": [
               {
                  "id": 1630347631,
                  "odds": 3.0,
                  "name": {
                     "value": "Over 0.5",
                     "sign": "d6Cefw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "0.5",
                  "numerator": 2,
                  "denominator": 1,
                  "americanOdds": 200
               },
               {
                  "id": 1630347632,
                  "odds": 1.36,
                  "name": {
                     "value": "Under 0.5",
                     "sign": "0kyzcw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "0.5",
                  "numerator": 4,
                  "denominator": 11,
                  "americanOdds": -275
               }
            ],
            "templateId": 29304,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "tYVVzw=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 5,
                     "marketTabId": 14,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Gordon Hayward (Cha)",
               "sign": "uQnuyg==",
               "short": "G. Hayward",
               "shortSign": "spdaRg=="
            }
         },
         {
            "id": 561396457,
            "name": {
               "value": "How many total steals and blocks will Gordon Hayward (Cha) record?",
               "sign": "Bs21XA=="
            },
            "results": [
               {
                  "id": 1630347633,
                  "odds": 2.15,
                  "name": {
                     "value": "Over 1.5",
                     "sign": "k9sHYw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "1.5",
                  "numerator": 23,
                  "denominator": 20,
                  "americanOdds": 115
               },
               {
                  "id": 1630347634,
                  "odds": 1.7,
                  "name": {
                     "value": "Under 1.5",
                     "sign": "uAAX4g==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "1.5",
                  "numerator": 7,
                  "denominator": 10,
                  "americanOdds": -145
               }
            ],
            "templateId": 33717,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "ZG2ywQ=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 1,
                     "subIndex": 1,
                     "marketTabId": 19,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Gordon Hayward (Cha)",
               "sign": "LN2eXw==",
               "short": "G. Hayward",
               "shortSign": "JTFHoQ=="
            }
         },
         {
            "id": 561396460,
            "name": {
               "value": "How many three-pointers will Gordon Hayward (Cha) record?",
               "sign": "4epv8g=="
            },
            "results": [
               {
                  "id": 1630347639,
                  "odds": 2.0,
                  "name": {
                     "value": "Over 1.5",
                     "sign": "w4k60w==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "1.5",
                  "numerator": 1,
                  "denominator": 1,
                  "americanOdds": 100
               },
               {
                  "id": 1630347640,
                  "odds": 1.8,
                  "name": {
                     "value": "Under 1.5",
                     "sign": "UrV6Xg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "1.5",
                  "numerator": 4,
                  "denominator": 5,
                  "americanOdds": -125
               }
            ],
            "templateId": 12857,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "8SbK9A=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 4,
                     "marketTabId": 13,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Gordon Hayward (Cha)",
               "sign": "LlG+8g==",
               "short": "G. Hayward",
               "shortSign": "bAHObA=="
            }
         },
         {
            "id": 561396461,
            "name": {
               "value": "How many steals will Gordon Hayward (Cha) record?",
               "sign": "T26tqw=="
            },
            "results": [
               {
                  "id": 1630347641,
                  "odds": 1.4,
                  "name": {
                     "value": "Over 0.5",
                     "sign": "Go3IsA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "0.5",
                  "numerator": 2,
                  "denominator": 5,
                  "americanOdds": -250
               },
               {
                  "id": 1630347642,
                  "odds": 2.85,
                  "name": {
                     "value": "Under 0.5",
                     "sign": "OPnezw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "0.5",
                  "numerator": 15,
                  "denominator": 8,
                  "americanOdds": 185
               }
            ],
            "templateId": 29303,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "IM4t+g=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 3,
                     "marketTabId": 12,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Gordon Hayward (Cha)",
               "sign": "u4XOZw==",
               "short": "G. Hayward",
               "shortSign": "+6fTiw=="
            }
         },
         {
            "id": 561396462,
            "name": {
               "value": "How many total points, rebounds and assists will Gordon Hayward (Cha) record?",
               "sign": "Ro2pOg=="
            },
            "results": [
               {
                  "id": 1630347643,
                  "odds": 1.91,
                  "name": {
                     "value": "Over 27.5",
                     "sign": "6qfN+Q==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "27.5",
                  "numerator": 10,
                  "denominator": 11,
                  "americanOdds": -110
               },
               {
                  "id": 1630347644,
                  "odds": 1.85,
                  "name": {
                     "value": "Under 27.5",
                     "sign": "lgtqgw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "27.5",
                  "numerator": 17,
                  "denominator": 20,
                  "americanOdds": -115
               }
            ],
            "templateId": 32108,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "U/cF6Q=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 6,
                     "marketTabId": 16,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Gordon Hayward (Cha)",
               "sign": "Rf4uAw==",
               "short": "G. Hayward",
               "shortSign": "A0qEeQ=="
            }
         },
         {
            "id": 561396463,
            "name": {
               "value": "How many rebounds will Gordon Hayward (Cha) record?",
               "sign": "2Jc8/g=="
            },
            "results": [
               {
                  "id": 1630347645,
                  "odds": 1.78,
                  "name": {
                     "value": "Over 5.5",
                     "sign": "rtEF3w==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "5.5",
                  "numerator": 4,
                  "denominator": 5,
                  "americanOdds": -130
               },
               {
                  "id": 1630347646,
                  "odds": 2.0,
                  "name": {
                     "value": "Under 5.5",
                     "sign": "KHGoMg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "5.5",
                  "numerator": 1,
                  "denominator": 1,
                  "americanOdds": 100
               }
            ],
            "templateId": 29301,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "gh/i5w=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 2,
                     "marketTabId": 10,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Gordon Hayward (Cha)",
               "sign": "0Cpelg==",
               "short": "G. Hayward",
               "shortSign": "lOyZng=="
            }
         },
         {
            "id": 561396464,
            "name": {
               "value": "How many total points and rebounds will Gordon Hayward (Cha) record?",
               "sign": "NpcW3A=="
            },
            "results": [
               {
                  "id": 1630347647,
                  "odds": 1.83,
                  "name": {
                     "value": "Over 22.5",
                     "sign": "+i+7BA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "22.5",
                  "numerator": 5,
                  "denominator": 6,
                  "americanOdds": -120
               },
               {
                  "id": 1630347648,
                  "odds": 1.95,
                  "name": {
                     "value": "Under 22.5",
                     "sign": "8jaESQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "22.5",
                  "numerator": 19,
                  "denominator": 20,
                  "americanOdds": -105
               }
            ],
            "templateId": 13494,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "LU4UVg=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 1,
                     "subIndex": 3,
                     "marketTabId": 24,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Gordon Hayward (Cha)",
               "sign": "y9oPWw==",
               "short": "G. Hayward",
               "shortSign": "duEhuQ=="
            }
         },
         {
            "id": 561396465,
            "name": {
               "value": "How many total rebounds and assists will Gordon Hayward (Cha) record?",
               "sign": "avrVgw=="
            },
            "results": [
               {
                  "id": 1630347649,
                  "odds": 1.95,
                  "name": {
                     "value": "Over 10.5",
                     "sign": "k2hkrA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "10.5",
                  "numerator": 19,
                  "denominator": 20,
                  "americanOdds": -105
               },
               {
                  "id": 1630347650,
                  "odds": 1.8,
                  "name": {
                     "value": "Under 10.5",
                     "sign": "HQb/TQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "10.5",
                  "numerator": 4,
                  "denominator": 5,
                  "americanOdds": -125
               }
            ],
            "templateId": 33718,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "/KbzWA=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 1,
                     "subIndex": 4,
                     "marketTabId": 20,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Gordon Hayward (Cha)",
               "sign": "Xg5/zg==",
               "short": "G. Hayward",
               "shortSign": "4Uc8Xg=="
            }
         },
         {
            "id": 561396466,
            "name": {
               "value": "How many total points and assists will Gordon Hayward (Cha) record?",
               "sign": "MGhjkw=="
            },
            "results": [
               {
                  "id": 1630347651,
                  "odds": 1.91,
                  "name": {
                     "value": "Over 21.5",
                     "sign": "F4t1Lw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "21.5",
                  "numerator": 10,
                  "denominator": 11,
                  "americanOdds": -110
               },
               {
                  "id": 1630347652,
                  "odds": 1.87,
                  "name": {
                     "value": "Under 21.5",
                     "sign": "dR5WYQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "21.5",
                  "numerator": 20,
                  "denominator": 23,
                  "americanOdds": -115
               }
            ],
            "templateId": 13495,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "j5/bSw=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 1,
                     "subIndex": 2,
                     "marketTabId": 23,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Gordon Hayward (Cha)",
               "sign": "oHWfqg==",
               "short": "G. Hayward",
               "shortSign": "GaprrA=="
            }
         },
         {
            "id": 561396467,
            "name": {
               "value": "How many assists will Gordon Hayward (Cha) record?",
               "sign": "CoI/LA=="
            },
            "results": [
               {
                  "id": 1630347653,
                  "odds": 2.1,
                  "name": {
                     "value": "Over 4.5",
                     "sign": "vwkS5g==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "4.5",
                  "numerator": 11,
                  "denominator": 10,
                  "americanOdds": 110
               },
               {
                  "id": 1630347654,
                  "odds": 1.72,
                  "name": {
                     "value": "Under 4.5",
                     "sign": "UEtf4Q==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "4.5",
                  "numerator": 8,
                  "denominator": 11,
                  "americanOdds": -140
               }
            ],
            "templateId": 29302,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "Xnc8RQ=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 1,
                     "marketTabId": 11,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Gordon Hayward (Cha)",
               "sign": "NaHvPw==",
               "short": "G. Hayward",
               "shortSign": "jgx2Sw=="
            }
         },
         {
            "id": 561396470,
            "name": {
               "value": "How many rebounds will P.J. Washington (Cha) record?",
               "sign": "B3f1FA=="
            },
            "results": [
               {
                  "id": 1630347659,
                  "odds": 1.65,
                  "name": {
                     "value": "Over 4.5",
                     "sign": "UWcihA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "4.5",
                  "numerator": 13,
                  "denominator": 20,
                  "americanOdds": -155
               },
               {
                  "id": 1630347660,
                  "odds": 2.2,
                  "name": {
                     "value": "Under 4.5",
                     "sign": "uv4yXQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "4.5",
                  "numerator": 6,
                  "denominator": 5,
                  "americanOdds": 120
               }
            ],
            "templateId": 29301,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "yzxEcA=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 2,
                     "marketTabId": 10,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "P.J. Washington (Cha)",
               "sign": "6iG6xQ==",
               "short": "P.J. Washington",
               "shortSign": "uL9Jmw=="
            }
         },
         {
            "id": 561396471,
            "name": {
               "value": "How many points will P.J. Washington (Cha) score?",
               "sign": "w7ky6w=="
            },
            "results": [
               {
                  "id": 1630347661,
                  "odds": 1.95,
                  "name": {
                     "value": "Over 11.5",
                     "sign": "6NsQeg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "11.5",
                  "numerator": 19,
                  "denominator": 20,
                  "americanOdds": -105
               },
               {
                  "id": 1630347662,
                  "odds": 1.85,
                  "name": {
                     "value": "Under 11.5",
                     "sign": "SNeAMw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "11.5",
                  "numerator": 17,
                  "denominator": 20,
                  "americanOdds": -115
               }
            ],
            "templateId": 29300,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "GtSjfg=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 0,
                     "marketTabId": 9,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "P.J. Washington (Cha)",
               "sign": "9TZKRQ==",
               "short": "P.J. Washington",
               "shortSign": "aVeulQ=="
            }
         },
         {
            "id": 561396472,
            "name": {
               "value": "How many total rebounds and assists will P.J. Washington (Cha) record?",
               "sign": "ozCLug=="
            },
            "results": [
               {
                  "id": 1630347663,
                  "odds": 1.75,
                  "name": {
                     "value": "Over 6.5",
                     "sign": "YC2g7g==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "6.5",
                  "numerator": 3,
                  "denominator": 4,
                  "americanOdds": -135
               },
               {
                  "id": 1630347664,
                  "odds": 2.05,
                  "name": {
                     "value": "Under 6.5",
                     "sign": "9tQX0Q==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "6.5",
                  "numerator": 21,
                  "denominator": 20,
                  "americanOdds": 105
               }
            ],
            "templateId": 33718,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "pQgrIQ=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 1,
                     "subIndex": 4,
                     "marketTabId": 20,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "P.J. Washington (Cha)",
               "sign": "VPLerQ==",
               "short": "P.J. Washington",
               "shortSign": "1osmyg=="
            }
         },
         {
            "id": 561396473,
            "name": {
               "value": "How many total points and assists will P.J. Washington (Cha) record?",
               "sign": "jWec9g=="
            },
            "results": [
               {
                  "id": 1630347665,
                  "odds": 1.95,
                  "name": {
                     "value": "Over 13.5",
                     "sign": "pPE19g==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "13.5",
                  "numerator": 19,
                  "denominator": 20,
                  "americanOdds": -105
               },
               {
                  "id": 1630347666,
                  "odds": 1.83,
                  "name": {
                     "value": "Under 13.5",
                     "sign": "POPr5Q==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "13.5",
                  "numerator": 5,
                  "denominator": 6,
                  "americanOdds": -120
               }
            ],
            "templateId": 13495,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "dODMLw=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 1,
                     "subIndex": 2,
                     "marketTabId": 23,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "P.J. Washington (Cha)",
               "sign": "S+UuLQ==",
               "short": "P.J. Washington",
               "shortSign": "B2PBxA=="
            }
         },
         {
            "id": 561396474,
            "name": {
               "value": "How many blocks will P.J. Washington (Cha) record?",
               "sign": "nVcsSg=="
            },
            "results": [
               {
                  "id": 1630347667,
                  "odds": 2.4,
                  "name": {
                     "value": "Over 0.5",
                     "sign": "DoxNLg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "0.5",
                  "numerator": 7,
                  "denominator": 5,
                  "americanOdds": 140
               },
               {
                  "id": 1630347668,
                  "odds": 1.55,
                  "name": {
                     "value": "Under 0.5",
                     "sign": "v+InLg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "0.5",
                  "numerator": 11,
                  "denominator": 20,
                  "americanOdds": -185
               }
            ],
            "templateId": 29304,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "B9nkPA=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 5,
                     "marketTabId": 14,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "P.J. Washington (Cha)",
               "sign": "K9pPdw==",
               "short": "P.J. Washington",
               "shortSign": "dFrp1w=="
            }
         },
         {
            "id": 561396475,
            "name": {
               "value": "How many three-pointers will P.J. Washington (Cha) record?",
               "sign": "Xe+pIg=="
            },
            "results": [
               {
                  "id": 1630347669,
                  "odds": 2.4,
                  "name": {
                     "value": "Over 1.5",
                     "sign": "abSynw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "1.5",
                  "numerator": 7,
                  "denominator": 5,
                  "americanOdds": 140
               },
               {
                  "id": 1630347670,
                  "odds": 1.55,
                  "name": {
                     "value": "Under 1.5",
                     "sign": "1a6Dvw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "1.5",
                  "numerator": 11,
                  "denominator": 20,
                  "americanOdds": -185
               }
            ],
            "templateId": 12857,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "1jEDMg=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 4,
                     "marketTabId": 13,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "P.J. Washington (Cha)",
               "sign": "NM2/9w==",
               "short": "P.J. Washington",
               "shortSign": "pbIO2Q=="
            }
         },
         {
            "id": 561396476,
            "name": {
               "value": "How many total points and rebounds will P.J. Washington (Cha) record?",
               "sign": "ocwzPg=="
            },
            "results": [
               {
                  "id": 1630347671,
                  "odds": 1.9,
                  "name": {
                     "value": "Over 16.5",
                     "sign": "6V8lmw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "16.5",
                  "numerator": 9,
                  "denominator": 10,
                  "americanOdds": -110
               },
               {
                  "id": 1630347672,
                  "odds": 1.87,
                  "name": {
                     "value": "Under 16.5",
                     "sign": "BEdDow==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "16.5",
                  "numerator": 20,
                  "denominator": 23,
                  "americanOdds": -115
               }
            ],
            "templateId": 13494,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "4au0Gg=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 1,
                     "subIndex": 3,
                     "marketTabId": 24,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "P.J. Washington (Cha)",
               "sign": "66SNww==",
               "short": "P.J. Washington",
               "shortSign": "kii58Q=="
            }
         },
         {
            "id": 561396482,
            "name": {
               "value": "How many steals will P.J. Washington (Cha) record?",
               "sign": "C+vD5w=="
            },
            "results": [
               {
                  "id": 1630347683,
                  "odds": 1.62,
                  "name": {
                     "value": "Over 0.5",
                     "sign": "SQ3wrw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "0.5",
                  "numerator": 8,
                  "denominator": 13,
                  "americanOdds": -160
               },
               {
                  "id": 1630347684,
                  "odds": 2.25,
                  "name": {
                     "value": "Under 0.5",
                     "sign": "DYsexg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "0.5",
                  "numerator": 5,
                  "denominator": 4,
                  "americanOdds": 125
               }
            ],
            "templateId": 29303,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "sKRhjw=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 3,
                     "marketTabId": 12,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "P.J. Washington (Cha)",
               "sign": "2NtdbA==",
               "short": "P.J. Washington",
               "shortSign": "wydsZA=="
            }
         },
         {
            "id": 561396483,
            "name": {
               "value": "How many total steals and blocks will P.J. Washington (Cha) record?",
               "sign": "kfN1ng=="
            },
            "results": [
               {
                  "id": 1630347685,
                  "odds": 2.35,
                  "name": {
                     "value": "Over 1.5",
                     "sign": "LjUPHg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "1.5",
                  "numerator": 27,
                  "denominator": 20,
                  "americanOdds": 135
               },
               {
                  "id": 1630347686,
                  "odds": 1.6,
                  "name": {
                     "value": "Under 1.5",
                     "sign": "Z8e6Vw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "1.5",
                  "numerator": 3,
                  "denominator": 5,
                  "americanOdds": -165
               }
            ],
            "templateId": 33717,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "YUyGgQ=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 1,
                     "subIndex": 1,
                     "marketTabId": 19,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "P.J. Washington (Cha)",
               "sign": "x8yt7A==",
               "short": "P.J. Washington",
               "shortSign": "Es+Lag=="
            }
         },
         {
            "id": 561396486,
            "name": {
               "value": "How many assists will P.J. Washington (Cha) record?",
               "sign": "lZ7frw=="
            },
            "results": [
               {
                  "id": 1630347691,
                  "odds": 1.72,
                  "name": {
                     "value": "Over 1.5",
                     "sign": "wFs/fA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "1.5",
                  "numerator": 8,
                  "denominator": 11,
                  "americanOdds": -140
               },
               {
                  "id": 1630347692,
                  "odds": 2.1,
                  "name": {
                     "value": "Under 1.5",
                     "sign": "jXLX6w==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "1.5",
                  "numerator": 11,
                  "denominator": 10,
                  "americanOdds": 110
               }
            ],
            "templateId": 29302,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "9Af+tA=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 1,
                     "marketTabId": 11,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "P.J. Washington (Cha)",
               "sign": "Z40OAg==",
               "short": "P.J. Washington",
               "shortSign": "h4TzXw=="
            }
         },
         {
            "id": 561396488,
            "name": {
               "value": "How many turnovers will P.J. Washington (Cha) record?",
               "sign": "C62C8Q=="
            },
            "results": [
               {
                  "id": 1630347695,
                  "odds": 1.91,
                  "name": {
                     "value": "Over 1.5",
                     "sign": "n8U5FQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "1.5",
                  "numerator": 10,
                  "denominator": 11,
                  "americanOdds": -110
               },
               {
                  "id": 1630347696,
                  "odds": 1.85,
                  "name": {
                     "value": "Under 1.5",
                     "sign": "r4x2ZA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "1.5",
                  "numerator": 17,
                  "denominator": 20,
                  "americanOdds": -115
               }
            ],
            "templateId": 12869,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "mjOR5Q=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 1,
                     "subIndex": 0,
                     "marketTabId": 15,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "P.J. Washington (Cha)",
               "sign": "2V5qag==",
               "short": "P.J. Washington",
               "shortSign": "6bCcDg=="
            }
         },
         {
            "id": 561396489,
            "name": {
               "value": "How many total points, rebounds and assists will P.J. Washington (Cha) record?",
               "sign": "ZBJmPA=="
            },
            "results": [
               {
                  "id": 1630347697,
                  "odds": 1.91,
                  "name": {
                     "value": "Over 18.5",
                     "sign": "mVBOSg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "18.5",
                  "numerator": 10,
                  "denominator": 11,
                  "americanOdds": -110
               },
               {
                  "id": 1630347698,
                  "odds": 1.85,
                  "name": {
                     "value": "Under 18.5",
                     "sign": "bLxrZg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "18.5",
                  "numerator": 17,
                  "denominator": 20,
                  "americanOdds": -115
               }
            ],
            "templateId": 32108,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "S9t26w=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 6,
                     "marketTabId": 16,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "P.J. Washington (Cha)",
               "sign": "xkma6g==",
               "short": "P.J. Washington",
               "shortSign": "OFh7AA=="
            }
         },
         {
            "id": 561396491,
            "name": {
               "value": "How many assists will Terry Rozier (Cha) record?",
               "sign": "NnnidQ=="
            },
            "results": [
               {
                  "id": 1630347701,
                  "odds": 1.65,
                  "name": {
                     "value": "Over 2.5",
                     "sign": "SvQiYw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "2.5",
                  "numerator": 13,
                  "denominator": 20,
                  "americanOdds": -155
               },
               {
                  "id": 1630347702,
                  "odds": 2.25,
                  "name": {
                     "value": "Under 2.5",
                     "sign": "UF7rDQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "2.5",
                  "numerator": 5,
                  "denominator": 4,
                  "americanOdds": 125
               }
            ],
            "templateId": 29302,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "6Qq59g=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 1,
                     "marketTabId": 11,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Terry Rozier (Cha)",
               "sign": "dmLZFg==",
               "short": "T. Rozier",
               "shortSign": "t4nzxg=="
            }
         },
         {
            "id": 561396492,
            "name": {
               "value": "How many three-pointers will Terry Rozier (Cha) record?",
               "sign": "P4C8aQ=="
            },
            "results": [
               {
                  "id": 1630347703,
                  "odds": 1.78,
                  "name": {
                     "value": "Over 2.5",
                     "sign": "RTgZug==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "2.5",
                  "numerator": 4,
                  "denominator": 5,
                  "americanOdds": -130
               },
               {
                  "id": 1630347704,
                  "odds": 2.0,
                  "name": {
                     "value": "Under 2.5",
                     "sign": "QaE7Sg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "2.5",
                  "numerator": 1,
                  "denominator": 1,
                  "americanOdds": 100
               }
            ],
            "templateId": 12857,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "3pAO3g=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 4,
                     "marketTabId": 13,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Terry Rozier (Cha)",
               "sign": "9NVW6Q==",
               "short": "T. Rozier",
               "shortSign": "H3WjCA=="
            }
         },
         {
            "id": 561396493,
            "name": {
               "value": "How many points will Terry Rozier (Cha) score?",
               "sign": "sqF6pw=="
            },
            "results": [
               {
                  "id": 1630347705,
                  "odds": 1.9,
                  "name": {
                     "value": "Over 18.5",
                     "sign": "LsNFZg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "18.5",
                  "numerator": 9,
                  "denominator": 10,
                  "americanOdds": -110
               },
               {
                  "id": 1630347706,
                  "odds": 1.9,
                  "name": {
                     "value": "Under 18.5",
                     "sign": "0JFCMw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "18.5",
                  "numerator": 9,
                  "denominator": 10,
                  "americanOdds": -110
               }
            ],
            "templateId": 29300,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "D3jp0A=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 0,
                     "marketTabId": 9,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Terry Rozier (Cha)",
               "sign": "woTUzQ==",
               "short": "T. Rozier",
               "shortSign": "EeUorQ=="
            }
         },
         {
            "id": 561396494,
            "name": {
               "value": "How many turnovers will Terry Rozier (Cha) record?",
               "sign": "jx+SHw=="
            },
            "results": [
               {
                  "id": 1630347707,
                  "odds": 1.65,
                  "name": {
                     "value": "Over 1.5",
                     "sign": "/SRUAw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "1.5",
                  "numerator": 13,
                  "denominator": 20,
                  "americanOdds": -155
               },
               {
                  "id": 1630347708,
                  "odds": 2.2,
                  "name": {
                     "value": "Under 1.5",
                     "sign": "41XAsw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "1.5",
                  "numerator": 6,
                  "denominator": 5,
                  "americanOdds": 120
               }
            ],
            "templateId": 12869,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "fEHBww=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 1,
                     "subIndex": 0,
                     "marketTabId": 15,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Terry Rozier (Cha)",
               "sign": "mHdSoA==",
               "short": "T. Rozier",
               "shortSign": "QlPFmA=="
            }
         },
         {
            "id": 561396495,
            "name": {
               "value": "How many total rebounds and assists will Terry Rozier (Cha) record?",
               "sign": "o467/g=="
            },
            "results": [
               {
                  "id": 1630347709,
                  "odds": 1.75,
                  "name": {
                     "value": "Over 6.5",
                     "sign": "KGAmtg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "6.5",
                  "numerator": 3,
                  "denominator": 4,
                  "americanOdds": -135
               },
               {
                  "id": 1630347710,
                  "odds": 2.05,
                  "name": {
                     "value": "Under 6.5",
                     "sign": "O2XpJg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "6.5",
                  "numerator": 21,
                  "denominator": 20,
                  "americanOdds": 105
               }
            ],
            "templateId": 33718,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "rakmzQ=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 1,
                     "subIndex": 4,
                     "marketTabId": 20,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Terry Rozier (Cha)",
               "sign": "ribQhA==",
               "short": "T. Rozier",
               "shortSign": "TMNOPQ=="
            }
         },
         {
            "id": 561396496,
            "name": {
               "value": "How many total points, rebounds and assists will Terry Rozier (Cha) record?",
               "sign": "HDYahg=="
            },
            "results": [
               {
                  "id": 1630347711,
                  "odds": 1.9,
                  "name": {
                     "value": "Over 25.5",
                     "sign": "NZPzFw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "25.5",
                  "numerator": 9,
                  "denominator": 10,
                  "americanOdds": -110
               },
               {
                  "id": 1630347712,
                  "odds": 1.87,
                  "name": {
                     "value": "Under 25.5",
                     "sign": "VEcRiA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "25.5",
                  "numerator": 20,
                  "denominator": 23,
                  "americanOdds": -115
               }
            ],
            "templateId": 32108,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "AvjQfA=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 6,
                     "marketTabId": 16,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Terry Rozier (Cha)",
               "sign": "fgeKoQ==",
               "short": "T. Rozier",
               "shortSign": "PYsChw=="
            }
         },
         {
            "id": 561396497,
            "name": {
               "value": "How many steals will Terry Rozier (Cha) record?",
               "sign": "tePIng=="
            },
            "results": [
               {
                  "id": 1630347713,
                  "odds": 1.6,
                  "name": {
                     "value": "Over 0.5",
                     "sign": "icXBrg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "0.5",
                  "numerator": 3,
                  "denominator": 5,
                  "americanOdds": -165
               },
               {
                  "id": 1630347714,
                  "odds": 2.3,
                  "name": {
                     "value": "Under 0.5",
                     "sign": "jjLGpg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "0.5",
                  "numerator": 13,
                  "denominator": 10,
                  "americanOdds": 130
               }
            ],
            "templateId": 29303,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "0xA3cg=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 3,
                     "marketTabId": 12,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Terry Rozier (Cha)",
               "sign": "SFYIhQ==",
               "short": "T. Rozier",
               "shortSign": "MxuJIg=="
            }
         },
         {
            "id": 561396498,
            "name": {
               "value": "How many total steals and blocks will Terry Rozier (Cha) record?",
               "sign": "SLbSpQ=="
            },
            "results": [
               {
                  "id": 1630347715,
                  "odds": 1.45,
                  "name": {
                     "value": "Over 0.5",
                     "sign": "hgn6dw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "0.5",
                  "numerator": 5,
                  "denominator": 11,
                  "americanOdds": -225
               },
               {
                  "id": 1630347716,
                  "odds": 2.7,
                  "name": {
                     "value": "Under 0.5",
                     "sign": "KF4dzQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "0.5",
                  "numerator": 17,
                  "denominator": 10,
                  "americanOdds": 170
               }
            ],
            "templateId": 33717,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "oCkfYQ=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 1,
                     "subIndex": 1,
                     "marketTabId": 19,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Terry Rozier (Cha)",
               "sign": "EqWO6A==",
               "short": "T. Rozier",
               "shortSign": "YK1kFw=="
            }
         },
         {
            "id": 561396499,
            "name": {
               "value": "How many blocks will Terry Rozier (Cha) record?",
               "sign": "V0n8gw=="
            },
            "results": [
               {
                  "id": 1630347717,
                  "odds": 5.25,
                  "name": {
                     "value": "Over 0.5",
                     "sign": "1lvHxw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "0.5",
                  "numerator": 17,
                  "denominator": 4,
                  "americanOdds": 425
               },
               {
                  "id": 1630347718,
                  "odds": 1.14,
                  "name": {
                     "value": "Under 0.5",
                     "sign": "dXh7XQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "0.5",
                  "numerator": 1,
                  "denominator": 7,
                  "americanOdds": -700
               }
            ],
            "templateId": 29304,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "ccH4bw=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 5,
                     "marketTabId": 14,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Terry Rozier (Cha)",
               "sign": "JPQMzA==",
               "short": "T. Rozier",
               "shortSign": "bj3vsg=="
            }
         },
         {
            "id": 561396505,
            "name": {
               "value": "How many rebounds will Terry Rozier (Cha) record?",
               "sign": "WN786Q=="
            },
            "results": [
               {
                  "id": 1630347729,
                  "odds": 1.75,
                  "name": {
                     "value": "Over 3.5",
                     "sign": "7QTs0w==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "3.5",
                  "numerator": 3,
                  "denominator": 4,
                  "americanOdds": -135
               },
               {
                  "id": 1630347730,
                  "odds": 2.05,
                  "name": {
                     "value": "Under 3.5",
                     "sign": "uauX/A==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "3.5",
                  "numerator": 21,
                  "denominator": 20,
                  "americanOdds": 105
               }
            ],
            "templateId": 29301,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "W1YIBQ=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 0,
                     "subIndex": 2,
                     "marketTabId": 10,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Terry Rozier (Cha)",
               "sign": "udlreg==",
               "short": "T. Rozier",
               "shortSign": "hIiCDg=="
            }
         },
         {
            "id": 561396506,
            "name": {
               "value": "How many total points and rebounds will Terry Rozier (Cha) record?",
               "sign": "XbitXw=="
            },
            "results": [
               {
                  "id": 1630347731,
                  "odds": 1.9,
                  "name": {
                     "value": "Over 22.5",
                     "sign": "2YkJzg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "22.5",
                  "numerator": 9,
                  "denominator": 10,
                  "americanOdds": -110
               },
               {
                  "id": 1630347732,
                  "odds": 1.87,
                  "name": {
                     "value": "Under 22.5",
                     "sign": "d5yYDQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "22.5",
                  "numerator": 20,
                  "denominator": 23,
                  "americanOdds": -115
               }
            ],
            "templateId": 13494,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "KG8gFg=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 1,
                     "subIndex": 3,
                     "marketTabId": 24,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Terry Rozier (Cha)",
               "sign": "4yrtFw==",
               "short": "T. Rozier",
               "shortSign": "1z5vOw=="
            }
         },
         {
            "id": 561396508,
            "name": {
               "value": "How many total points and assists will Terry Rozier (Cha) record?",
               "sign": "FGUorg=="
            },
            "results": [
               {
                  "id": 1630347735,
                  "odds": 1.87,
                  "name": {
                     "value": "Over 21.5",
                     "sign": "e33yNw==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "21.5",
                  "numerator": 20,
                  "denominator": 23,
                  "americanOdds": -115
               },
               {
                  "id": 1630347736,
                  "odds": 1.9,
                  "name": {
                     "value": "Under 21.5",
                     "sign": "TJljcA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "attr": "21.5",
                  "numerator": 9,
                  "denominator": 10,
                  "americanOdds": -110
               }
            ],
            "templateId": 13495,
            "categoryId": 49,
            "resultOrder": "Default",
            "combo1": "NoEventCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 49,
               "name": {
                  "value": "Player Specials",
                  "sign": "zh1wMA=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 1,
                     "subIndex": 2,
                     "marketTabId": 23,
                     "name": "Player Props",
                     "displayType": "PlayerProps"
                  }
               ],
               "parameters": {}
            },
            "player1": {
               "value": "Terry Rozier (Cha)",
               "sign": "V8zgzA==",
               "short": "T. Rozier",
               "shortSign": "cVK0UA=="
            }
         },
         {
            "id": 561397715,
            "name": {
               "value": "Parlay",
               "sign": "1ZnGuA=="
            },
            "results": [
               {
                  "id": 1630350275,
                  "odds": 11.0,
                  "name": {
                     "value": "Joel Embiid & Tobias Harris both 30+ Points",
                     "sign": "DcnJLg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 10,
                  "denominator": 1,
                  "americanOdds": 1000
               },
               {
                  "id": 1630350276,
                  "odds": 9.0,
                  "name": {
                     "value": "Joel Embiid, Tobias Harris, Terry Rozier & Gordon Hayward all 20+ Points",
                     "sign": "WgbMBA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 8,
                  "denominator": 1,
                  "americanOdds": 800
               },
               {
                  "id": 1630350277,
                  "odds": 13.0,
                  "name": {
                     "value": "Joel Embiid & Tobias Harris both 25+ Points, Terry Rozier & Gordon Hayward both 20+ Points",
                     "sign": "2xg6dQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 12,
                  "denominator": 1,
                  "americanOdds": 1200
               },
               {
                  "id": 1630350278,
                  "odds": 11.0,
                  "name": {
                     "value": "Joel Embiid & Tobias Harris both 25+ Points, Joel Embiid & Bismack Biyombo both 10+ Rebounds",
                     "sign": "F6BMiQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 10,
                  "denominator": 1,
                  "americanOdds": 1000
               },
               {
                  "id": 1630350279,
                  "odds": 4.0,
                  "name": {
                     "value": "Joel Embiid to score 30+ points and record 10+ rebounds and Ben Simmons to record 7+ assists",
                     "sign": "QwRIiQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 3,
                  "denominator": 1,
                  "americanOdds": 300
               },
               {
                  "id": 1630350280,
                  "odds": 1.6,
                  "name": {
                     "value": "Joel Embiid & Bismack Biyombo both 7+ Rebounds",
                     "sign": "dkp8ig==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 3,
                  "denominator": 5,
                  "americanOdds": -165
               },
               {
                  "id": 1630350281,
                  "odds": 15.0,
                  "name": {
                     "value": "Joel Embiid, Bismack Biyombo & Ben Simmons all 2+ Blocks",
                     "sign": "8UPdIQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 14,
                  "denominator": 1,
                  "americanOdds": 1400
               },
               {
                  "id": 1630350282,
                  "odds": 19.0,
                  "name": {
                     "value": "Joel Embiid, Bismack Biyombo, Ben Simmons & Tobias Harris all 2+ Blocks",
                     "sign": "MURwxQ==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 18,
                  "denominator": 1,
                  "americanOdds": 1800
               },
               {
                  "id": 1630350283,
                  "odds": 10.0,
                  "name": {
                     "value": "Joel Embiid & Bismack Biyombo both 2+ Blocks, Terry Rozier & Seth Curry both 2+ Threes Made",
                     "sign": "tppMIA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 9,
                  "denominator": 1,
                  "americanOdds": 900
               },
               {
                  "id": 1630350284,
                  "odds": 13.0,
                  "name": {
                     "value": "Joel Embiid & Bismack Biyombo both 2+ Blocks, Terry Rozier & Seth Curry both 3+ Threes Made",
                     "sign": "J0t9tA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 12,
                  "denominator": 1,
                  "americanOdds": 1200
               },
               {
                  "id": 1630350285,
                  "odds": 4.5,
                  "name": {
                     "value": "Ben Simmons to record 10+ assists and 2+ steals",
                     "sign": "JLmZGA==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 7,
                  "denominator": 2,
                  "americanOdds": 350
               },
               {
                  "id": 1630350286,
                  "odds": 6.5,
                  "name": {
                     "value": "Ben Simmons 10+ Assists, Joel 2+ Embiid Blocks",
                     "sign": "mNoFBg==",
                     "shortSign": ""
                  },
                  "visibility": "Visible",
                  "numerator": 11,
                  "denominator": 2,
                  "americanOdds": 550
               }
            ],
            "templateId": 33919,
            "categoryId": 1382,
            "resultOrder": "Odds",
            "combo1": "NoCombo",
            "combo2": "Single",
            "visibility": "Visible",
            "category": "Specials",
            "templateCategory": {
               "id": 1382,
               "name": {
                  "value": "Price Boost",
                  "sign": "7hEkSw=="
               },
               "category": "Specials"
            },
            "isMain": false,
            "grouping": {
               "gridGroups": [],
               "detailed": [
                  {
                     "group": 6,
                     "index": 123,
                     "displayType": "Regular"
                  }
               ],
               "parameters": {}
            }
         }
      ],
      "participants": [
         {
            "participantId": 2703392,
            "name": {
               "value": "Charlotte Hornets",
               "sign": "xHQETg==",
               "short": "Hornets",
               "shortSign": "yEKBWw=="
            },
            "image": {
               "logo": "2703392_logo",
               "jersey": "2703392_1",
               "rotateJersey": true
            },
            "options": []
         },
         {
            "participantId": 2554188,
            "name": {
               "value": "Philadelphia 76ers",
               "sign": "XBhwWg==",
               "short": "76ers",
               "shortSign": "MWk5NA=="
            },
            "image": {
               "logo": "2554188_logo",
               "jersey": "2554188_1",
               "rotateJersey": true
            },
            "options": []
         }
      ],
      "id": "10974867",
      "name": {
         "value": "Charlotte Hornets at Philadelphia 76ers",
         "sign": "ZmVprw=="
      },
      "sourceId": 10974867,
      "source": "V1",
      "fixtureType": "Standard",
      "context": "v1|en-us|10974867",
      "addons": {
         "betBuilderFixture": "7653485",
         "betRadar": 24751364,
         "participantDividend": {}
      },
      "stage": "PreMatch",
      "groupId": 2346891,
      "liveType": "NotSet",
      "liveAlert": true,
      "startDate": "2021-01-05T00:05:00Z",
      "cutOffDate": "2021-01-05T00:05:00Z",
      "sport": {
         "type": "Sport",
         "id": 7,
         "name": {
            "value": "Basketball",
            "sign": "WotGFA=="
         }
      },
      "competition": {
         "parentLeagueId": 6004,
         "statistics": true,
         "sportId": 7,
         "type": "Competition",
         "id": 6004,
         "parentId": 9,
         "name": {
            "value": "NBA",
            "sign": "pL+WdA=="
         }
      },
      "region": {
         "code": "N-A",
         "sportId": 7,
         "type": "Region",
         "id": 9,
         "parentId": 7,
         "name": {
            "value": "USA",
            "sign": "ULdUtw=="
         }
      },
      "viewType": "American",
      "isOpenForBetting": true,
      "isVirtual": false,
      "taggedLocations": [],
      "totalMarketsCount": 175
   },
   "splitFixtures": [],
   "groupingVersion": "rVMxL1LtS/TBBoQh8Ivn/RmLzMs="
}
