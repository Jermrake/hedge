{
   "attributes": {
      "attrib": [
         {
            "value": "sr:match:24750896",
            "key": "betradarId"
         },
         {
            "value": "true",
            "key": "cashoutAvailable"
         }
      ]
   },
   "markets": [
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "10",
                  "frac": "9/1",
                  "rootIdx": 196
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Coby White 3+",
                  "shortName": "Coby White 3+"
               },
               "wasPrice": [],
               "id": 202519419,
               "name": "Coby White 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "5.5",
                  "frac": "9/2",
                  "rootIdx": 166
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "Coby White 2+",
                  "shortName": "Coby White 2+"
               },
               "wasPrice": [],
               "id": 202519417,
               "name": "Coby White 2+",
               "type": "2",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "30",
                  "frac": "29/1",
                  "rootIdx": 221
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Coby White 4+",
                  "shortName": "Coby White 4+"
               },
               "wasPrice": [],
               "id": 202519421,
               "name": "Coby White 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.85",
                  "frac": "37/20",
                  "rootIdx": 116
               },
               "pos": {
                  "row": 101,
                  "col": 1
               },
               "names": {
                  "longName": "Coby White 1+",
                  "shortName": "Coby White 1+"
               },
               "wasPrice": [],
               "id": 202519415,
               "name": "Coby White 1+",
               "type": "1",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Coby White blocks (incl. overtime)",
            "shortName": "Coby White blocks (incl. overtime)"
         },
         "id": 69729766,
         "name": "Coby White blocks (incl. overtime)",
         "type": "BASKETBALL:FT:PLBLOC",
         "subtype": "M#pre:playerprops:24750896:1582800",
         "displayOrder": -269,
         "columnCount": 1,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 113.5",
                  "shortName": "Under 113.5"
               },
               "wasPrice": [],
               "id": 202549657,
               "name": "Under 113.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 113.5",
                  "shortName": "Over 113.5"
               },
               "wasPrice": [],
               "id": 202549655,
               "name": "Over 113.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Half Total Points",
            "shortName": "1st Half Total Points"
         },
         "id": 69741040,
         "name": "1st Half Total Points",
         "type": "BASKETBALL:P:OU",
         "subtype": "H1#113.5",
         "line": 113.5,
         "displayOrder": -365,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "H1"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.5",
                  "frac": "3/2",
                  "rootIdx": 108
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Patrick Williams 3+",
                  "shortName": "Patrick Williams 3+"
               },
               "wasPrice": [],
               "id": 202519337,
               "name": "Patrick Williams 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.5",
                  "frac": "1/2",
                  "rootIdx": 47
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "Patrick Williams 2+",
                  "shortName": "Patrick Williams 2+"
               },
               "wasPrice": [],
               "id": 202519335,
               "name": "Patrick Williams 2+",
               "type": "2",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.1",
                  "frac": "1/10",
                  "rootIdx": 17
               },
               "pos": {
                  "row": 101,
                  "col": 1
               },
               "names": {
                  "longName": "Patrick Williams 1+",
                  "shortName": "Patrick Williams 1+"
               },
               "wasPrice": [],
               "id": 202519333,
               "name": "Patrick Williams 1+",
               "type": "1",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4.15",
                  "frac": "63/20",
                  "rootIdx": 143
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Patrick Williams 4+",
                  "shortName": "Patrick Williams 4+"
               },
               "wasPrice": [],
               "id": 202519339,
               "name": "Patrick Williams 4+",
               "type": "4",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Patrick Williams blocks (incl. overtime)",
            "shortName": "Patrick Williams blocks (incl. overtime)"
         },
         "id": 69729746,
         "name": "Patrick Williams blocks (incl. overtime)",
         "type": "BASKETBALL:FT:PLBLOC",
         "subtype": "M#pre:playerprops:24750896:1797198",
         "displayOrder": -269,
         "columnCount": 1,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.85",
                  "frac": "17/20",
                  "rootIdx": 70
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -0.5",
                  "shortName": "Dallas Mavericks -0.5"
               },
               "wasPrice": [],
               "id": 202471179,
               "name": "Dallas Mavericks -0.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.85",
                  "frac": "17/20",
                  "rootIdx": 70
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +0.5",
                  "shortName": "Chicago Bulls +0.5"
               },
               "wasPrice": [],
               "id": 202471177,
               "name": "Chicago Bulls +0.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "4th Quarter Spread",
            "shortName": "4th Quarter Spread"
         },
         "id": 69715235,
         "name": "4th Quarter Spread",
         "type": "BASKETBALL:P:PSPRD",
         "subtype": "Q4#0.5",
         "line": 0.5,
         "displayOrder": -294,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q4"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.75",
                  "frac": "3/4",
                  "rootIdx": 63
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +2.5",
                  "shortName": "Chicago Bulls +2.5"
               },
               "wasPrice": [],
               "id": 202637123,
               "name": "Chicago Bulls +2.5",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -2.5",
                  "shortName": "Dallas Mavericks -2.5"
               },
               "wasPrice": [],
               "id": 202637121,
               "name": "Dallas Mavericks -2.5",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "2nd Half Spread (Excl. OT)",
            "shortName": "2nd Half Spread (Excl. OT)"
         },
         "id": 69763710,
         "name": "2nd Half Spread (Excl. OT)",
         "type": "BASKETBALL:P:AHCP",
         "subtype": "H2#2.5",
         "line": 2.5,
         "displayOrder": -340,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "H2"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.4",
                  "frac": "7/5",
                  "rootIdx": 106
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 217.5",
                  "shortName": "Under 217.5"
               },
               "wasPrice": [],
               "id": 202471199,
               "name": "Under 217.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.53",
                  "frac": "8/15",
                  "rootIdx": 50
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 217.5",
                  "shortName": "Over 217.5"
               },
               "wasPrice": [],
               "id": 202471197,
               "name": "Over 217.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715245,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#217.5",
         "line": 217.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.05",
                  "frac": "21/20",
                  "rootIdx": 88
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 220.5",
                  "shortName": "Under 220.5"
               },
               "wasPrice": [],
               "id": 202471125,
               "name": "Under 220.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.72",
                  "frac": "8/11",
                  "rootIdx": 60
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 220.5",
                  "shortName": "Over 220.5"
               },
               "wasPrice": [],
               "id": 202471123,
               "name": "Over 220.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715209,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#220.5",
         "line": 220.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 110.5",
                  "shortName": "Under 110.5"
               },
               "wasPrice": [],
               "id": 202471347,
               "name": "Under 110.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 110.5",
                  "shortName": "Over 110.5"
               },
               "wasPrice": [],
               "id": 202471345,
               "name": "Over 110.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Chicago Bulls Total Points (Incl. OT)",
            "shortName": "Chicago Bulls Total Points (Incl. OT)"
         },
         "id": 69715297,
         "name": "Chicago Bulls Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:A:OU",
         "subtype": "M#110.5",
         "line": 110.5,
         "displayOrder": -445,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "4.3",
                  "frac": "33/10",
                  "rootIdx": 146
               },
               "pos": {
                  "row": 2,
                  "col": 2
               },
               "names": {
                  "longName": "Chicago Bulls & Over 223.5",
                  "shortName": "Chicago Bulls & Over 223.5"
               },
               "wasPrice": [],
               "id": 202638259,
               "name": "Chicago Bulls & Over 223.5",
               "type": "AOver",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.1",
                  "frac": "21/10",
                  "rootIdx": 122
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Dallas Mavericks & Under 223.5",
                  "shortName": "Dallas Mavericks & Under 223.5"
               },
               "wasPrice": [],
               "id": 202638265,
               "name": "Dallas Mavericks & Under 223.5",
               "type": "BUnder",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.05",
                  "frac": "41/20",
                  "rootIdx": 121
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks & Over 223.5",
                  "shortName": "Dallas Mavericks & Over 223.5"
               },
               "wasPrice": [],
               "id": 202638263,
               "name": "Dallas Mavericks & Over 223.5",
               "type": "BOver",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "4.3",
                  "frac": "33/10",
                  "rootIdx": 146
               },
               "pos": {
                  "row": 2,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls & Under 223.5",
                  "shortName": "Chicago Bulls & Under 223.5"
               },
               "wasPrice": [],
               "id": 202638261,
               "name": "Chicago Bulls & Under 223.5",
               "type": "AUnder",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Matchbet and Total Points (Incl. OT)",
            "shortName": "Matchbet and Total Points (Incl. OT)"
         },
         "id": 69764192,
         "name": "Matchbet and Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:AXB$OU",
         "subtype": "M#223.5",
         "line": 223.5,
         "displayOrder": -425,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.36",
                  "frac": "4/11",
                  "rootIdx": 39
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +9.5",
                  "shortName": "Chicago Bulls +9.5"
               },
               "wasPrice": [],
               "id": 202330999,
               "name": "Chicago Bulls +9.5",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3",
                  "frac": "2/1",
                  "rootIdx": 120
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -9.5",
                  "shortName": "Dallas Mavericks -9.5"
               },
               "wasPrice": [],
               "id": 202331003,
               "name": "Dallas Mavericks -9.5",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666822,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#9.5",
         "line": 9.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.85",
                  "frac": "17/20",
                  "rootIdx": 70
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 27.5",
                  "shortName": "Under 27.5"
               },
               "wasPrice": [],
               "id": 202471273,
               "name": "Under 27.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.85",
                  "frac": "17/20",
                  "rootIdx": 70
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 27.5",
                  "shortName": "Over 27.5"
               },
               "wasPrice": [],
               "id": 202471271,
               "name": "Over 27.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Quarter Chicago Bulls Total Points",
            "shortName": "1st Quarter Chicago Bulls Total Points"
         },
         "id": 69715277,
         "name": "1st Quarter Chicago Bulls Total Points",
         "type": "BASKETBALL:P:A:OU",
         "subtype": "Q1#27.5",
         "line": 27.5,
         "displayOrder": -318,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3.75",
                  "frac": "11/4",
                  "rootIdx": 135
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -11.5",
                  "shortName": "Dallas Mavericks -11.5"
               },
               "wasPrice": [],
               "id": 202330902,
               "name": "Dallas Mavericks -11.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.25",
                  "frac": "1/4",
                  "rootIdx": 32
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +11.5",
                  "shortName": "Chicago Bulls +11.5"
               },
               "wasPrice": [],
               "id": 202330896,
               "name": "Chicago Bulls +11.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666770,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#11.5",
         "line": 11.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "7",
                  "frac": "6/1",
                  "rootIdx": 184
               },
               "pos": {
                  "row": 2,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls 6-10",
                  "shortName": "Chicago Bulls 6-10"
               },
               "wasPrice": [],
               "id": 202470841,
               "name": "Chicago Bulls 6-10",
               "type": "HT 6-10",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "6.25",
                  "frac": "21/4",
                  "rootIdx": 175
               },
               "pos": {
                  "row": 3,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls 11+",
                  "shortName": "Chicago Bulls 11+"
               },
               "wasPrice": [],
               "id": 202470837,
               "name": "Chicago Bulls 11+",
               "type": "HT > 10",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "4.65",
                  "frac": "73/20",
                  "rootIdx": 154
               },
               "pos": {
                  "row": 2,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks 6-10",
                  "shortName": "Dallas Mavericks 6-10"
               },
               "wasPrice": [],
               "id": 202470835,
               "name": "Dallas Mavericks 6-10",
               "type": "AT 6-10",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "5.1",
                  "frac": "41/10",
                  "rootIdx": 161
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks 1-5",
                  "shortName": "Dallas Mavericks 1-5"
               },
               "wasPrice": [],
               "id": 202470833,
               "name": "Dallas Mavericks 1-5",
               "type": "AT 1-5",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.3",
                  "frac": "23/10",
                  "rootIdx": 126
               },
               "pos": {
                  "row": 3,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks 11+",
                  "shortName": "Dallas Mavericks 11+"
               },
               "wasPrice": [],
               "id": 202470831,
               "name": "Dallas Mavericks 11+",
               "type": "AT > 10",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "6.2",
                  "frac": "26/5",
                  "rootIdx": 174
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls 1-5",
                  "shortName": "Chicago Bulls 1-5"
               },
               "wasPrice": [],
               "id": 202470839,
               "name": "Chicago Bulls 1-5",
               "type": "HT 1-5",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Winning Margin 6-Way(Incl. OT)",
            "shortName": "Winning Margin 6-Way(Incl. OT)"
         },
         "id": 69715083,
         "name": "Winning Margin 6-Way(Incl. OT)",
         "type": "BASKETBALL:FTOT:WM",
         "subtype": "M#sr:winning_margin_no_draw:11+",
         "displayOrder": -405,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3",
                  "frac": "2/1",
                  "rootIdx": 120
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls -3.5",
                  "shortName": "Chicago Bulls -3.5"
               },
               "wasPrice": [],
               "id": 202330831,
               "name": "Chicago Bulls -3.5",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.36",
                  "frac": "4/11",
                  "rootIdx": 39
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks +3.5",
                  "shortName": "Dallas Mavericks +3.5"
               },
               "wasPrice": [],
               "id": 202330870,
               "name": "Dallas Mavericks +3.5",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666751,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-3.5",
         "line": -3.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.47",
                  "frac": "40/85",
                  "rootIdx": 45
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 229.5",
                  "shortName": "Under 229.5"
               },
               "wasPrice": [],
               "id": 202471081,
               "name": "Under 229.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.55",
                  "frac": "31/20",
                  "rootIdx": 109
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 229.5",
                  "shortName": "Over 229.5"
               },
               "wasPrice": [],
               "id": 202471079,
               "name": "Over 229.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715187,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#229.5",
         "line": 229.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3.05",
                  "frac": "41/20",
                  "rootIdx": 121
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 232.5",
                  "shortName": "Over 232.5"
               },
               "wasPrice": [],
               "id": 202470939,
               "name": "Over 232.5",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.35",
                  "frac": "7/20",
                  "rootIdx": 38
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 232.5",
                  "shortName": "Under 232.5"
               },
               "wasPrice": [],
               "id": 202470941,
               "name": "Under 232.5",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715123,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#232.5",
         "line": 232.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.57",
                  "frac": "4/7",
                  "rootIdx": 52
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -1.5",
                  "shortName": "Dallas Mavericks -1.5"
               },
               "wasPrice": [],
               "id": 202330911,
               "name": "Dallas Mavericks -1.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.3",
                  "frac": "13/10",
                  "rootIdx": 103
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +1.5",
                  "shortName": "Chicago Bulls +1.5"
               },
               "wasPrice": [],
               "id": 202330906,
               "name": "Chicago Bulls +1.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666775,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#1.5",
         "line": 1.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 56.5",
                  "shortName": "Over 56.5"
               },
               "wasPrice": [],
               "id": 202471059,
               "name": "Over 56.5",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.75",
                  "frac": "3/4",
                  "rootIdx": 63
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 56.5",
                  "shortName": "Under 56.5"
               },
               "wasPrice": [],
               "id": 202471061,
               "name": "Under 56.5",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Half Chicago Bulls Total Points",
            "shortName": "1st Half Chicago Bulls Total Points"
         },
         "id": 69715179,
         "name": "1st Half Chicago Bulls Total Points",
         "type": "BASKETBALL:P:A:OU",
         "subtype": "H1#56.5",
         "line": 56.5,
         "displayOrder": -355,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "H1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "4.2",
                  "frac": "16/5",
                  "rootIdx": 144
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -13",
                  "shortName": "Dallas Mavericks -13"
               },
               "wasPrice": [],
               "id": 202330944,
               "name": "Dallas Mavericks -13",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.2",
                  "frac": "1/5",
                  "rootIdx": 28
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +13",
                  "shortName": "Chicago Bulls +13"
               },
               "wasPrice": [],
               "id": 202330929,
               "name": "Chicago Bulls +13",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666789,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#13",
         "line": 13.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.25",
                  "frac": "1/4",
                  "rootIdx": 32
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +12",
                  "shortName": "Chicago Bulls +12"
               },
               "wasPrice": [],
               "id": 202330846,
               "name": "Chicago Bulls +12",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.75",
                  "frac": "11/4",
                  "rootIdx": 135
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -12",
                  "shortName": "Dallas Mavericks -12"
               },
               "wasPrice": [],
               "id": 202330867,
               "name": "Dallas Mavericks -12",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666756,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#12",
         "line": 12.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.15",
                  "frac": "2/13",
                  "rootIdx": 23
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +15",
                  "shortName": "Chicago Bulls +15"
               },
               "wasPrice": [],
               "id": 202331013,
               "name": "Chicago Bulls +15",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "4.8",
                  "frac": "19/5",
                  "rootIdx": 157
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -15",
                  "shortName": "Dallas Mavericks -15"
               },
               "wasPrice": [],
               "id": 202331025,
               "name": "Dallas Mavericks -15",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666829,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#15",
         "line": 15.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.85",
                  "frac": "17/20",
                  "rootIdx": 70
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 114.5",
                  "shortName": "Over 114.5"
               },
               "wasPrice": [],
               "id": 202478739,
               "name": "Over 114.5",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.85",
                  "frac": "17/20",
                  "rootIdx": 70
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 114.5",
                  "shortName": "Under 114.5"
               },
               "wasPrice": [],
               "id": 202478737,
               "name": "Under 114.5",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Half Total Points",
            "shortName": "1st Half Total Points"
         },
         "id": 69718301,
         "name": "1st Half Total Points",
         "type": "BASKETBALL:P:OU",
         "subtype": "H1#114.5",
         "line": 114.5,
         "displayOrder": -365,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "H1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.18",
                  "frac": "2/11",
                  "rootIdx": 26
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +14",
                  "shortName": "Chicago Bulls +14"
               },
               "wasPrice": [],
               "id": 202331021,
               "name": "Chicago Bulls +14",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "4.5",
                  "frac": "7/2",
                  "rootIdx": 151
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -14",
                  "shortName": "Dallas Mavericks -14"
               },
               "wasPrice": [],
               "id": 202331033,
               "name": "Dallas Mavericks -14",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666828,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#14",
         "line": 14.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.85",
                  "frac": "17/20",
                  "rootIdx": 70
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -1.5",
                  "shortName": "Dallas Mavericks -1.5"
               },
               "wasPrice": [],
               "id": 202471289,
               "name": "Dallas Mavericks -1.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.85",
                  "frac": "17/20",
                  "rootIdx": 70
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +1.5",
                  "shortName": "Chicago Bulls +1.5"
               },
               "wasPrice": [],
               "id": 202471287,
               "name": "Chicago Bulls +1.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "2nd Half Spread (Excl. OT)",
            "shortName": "2nd Half Spread (Excl. OT)"
         },
         "id": 69715285,
         "name": "2nd Half Spread (Excl. OT)",
         "type": "BASKETBALL:P:AHCP",
         "subtype": "H2#1.5",
         "line": 1.5,
         "displayOrder": -340,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "H2"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3.5",
                  "frac": "5/2",
                  "rootIdx": 130
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -11",
                  "shortName": "Dallas Mavericks -11"
               },
               "wasPrice": [],
               "id": 202330907,
               "name": "Dallas Mavericks -11",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.28",
                  "frac": "2/7",
                  "rootIdx": 34
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +11",
                  "shortName": "Chicago Bulls +11"
               },
               "wasPrice": [],
               "id": 202330901,
               "name": "Chicago Bulls +11",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666774,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#11",
         "line": 11.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3.15",
                  "frac": "43/20",
                  "rootIdx": 123
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -10",
                  "shortName": "Dallas Mavericks -10"
               },
               "wasPrice": [],
               "id": 202330940,
               "name": "Dallas Mavericks -10",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.33",
                  "frac": "1/3",
                  "rootIdx": 37
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +10",
                  "shortName": "Chicago Bulls +10"
               },
               "wasPrice": [],
               "id": 202330927,
               "name": "Chicago Bulls +10",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666782,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#10",
         "line": 10.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.47",
                  "frac": "40/85",
                  "rootIdx": 45
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Coby White 4+",
                  "shortName": "Coby White 4+"
               },
               "wasPrice": [],
               "id": 202519309,
               "name": "Coby White 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "16",
                  "frac": "15/1",
                  "rootIdx": 207
               },
               "names": {
                  "longName": "Coby White 9+",
                  "shortName": "Coby White 9+"
               },
               "wasPrice": [],
               "id": 202519319,
               "name": "Coby White 9+",
               "type": "9",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "9",
                  "frac": "8/1",
                  "rootIdx": 192
               },
               "pos": {
                  "row": 203,
                  "col": 1
               },
               "names": {
                  "longName": "Coby White 8+",
                  "shortName": "Coby White 8+"
               },
               "wasPrice": [],
               "id": 202519317,
               "name": "Coby White 8+",
               "type": "8",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "5.1",
                  "frac": "41/10",
                  "rootIdx": 161
               },
               "pos": {
                  "row": 103,
                  "col": 1
               },
               "names": {
                  "longName": "Coby White 7+",
                  "shortName": "Coby White 7+"
               },
               "wasPrice": [],
               "id": 202519315,
               "name": "Coby White 7+",
               "type": "7",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.05",
                  "frac": "41/20",
                  "rootIdx": 121
               },
               "pos": {
                  "row": 302,
                  "col": 1
               },
               "names": {
                  "longName": "Coby White 6+",
                  "shortName": "Coby White 6+"
               },
               "wasPrice": [],
               "id": 202519313,
               "name": "Coby White 6+",
               "type": "6",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.05",
                  "frac": "21/20",
                  "rootIdx": 88
               },
               "pos": {
                  "row": 202,
                  "col": 1
               },
               "names": {
                  "longName": "Coby White 5+",
                  "shortName": "Coby White 5+"
               },
               "wasPrice": [],
               "id": 202519311,
               "name": "Coby White 5+",
               "type": "5",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Coby White rebounds (incl. overtime)",
            "shortName": "Coby White rebounds (incl. overtime)"
         },
         "id": 69729742,
         "name": "Coby White rebounds (incl. overtime)",
         "type": "BASKETBALL:FT:PLREB",
         "subtype": "M#pre:playerprops:24750896:1582800",
         "displayOrder": -274,
         "columnCount": 1,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.3",
                  "frac": "13/10",
                  "rootIdx": 103
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 218.5",
                  "shortName": "Under 218.5"
               },
               "wasPrice": [],
               "id": 202470881,
               "name": "Under 218.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.57",
                  "frac": "4/7",
                  "rootIdx": 52
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 218.5",
                  "shortName": "Over 218.5"
               },
               "wasPrice": [],
               "id": 202470879,
               "name": "Over 218.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715103,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#218.5",
         "line": 218.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "9.5",
                  "frac": "17/2",
                  "rootIdx": 194
               },
               "names": {
                  "longName": "Zach LaVine 9+",
                  "shortName": "Zach LaVine 9+"
               },
               "wasPrice": [],
               "id": 202519203,
               "name": "Zach LaVine 9+",
               "type": "9",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "5.4",
                  "frac": "22/5",
                  "rootIdx": 165
               },
               "pos": {
                  "row": 203,
                  "col": 1
               },
               "names": {
                  "longName": "Zach LaVine 8+",
                  "shortName": "Zach LaVine 8+"
               },
               "wasPrice": [],
               "id": 202519201,
               "name": "Zach LaVine 8+",
               "type": "8",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.3",
                  "frac": "23/10",
                  "rootIdx": 126
               },
               "pos": {
                  "row": 103,
                  "col": 1
               },
               "names": {
                  "longName": "Zach LaVine 7+",
                  "shortName": "Zach LaVine 7+"
               },
               "wasPrice": [],
               "id": 202519199,
               "name": "Zach LaVine 7+",
               "type": "7",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.25",
                  "frac": "5/4",
                  "rootIdx": 101
               },
               "pos": {
                  "row": 302,
                  "col": 1
               },
               "names": {
                  "longName": "Zach LaVine 6+",
                  "shortName": "Zach LaVine 6+"
               },
               "wasPrice": [],
               "id": 202519197,
               "name": "Zach LaVine 6+",
               "type": "6",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.7",
                  "frac": "7/10",
                  "rootIdx": 59
               },
               "pos": {
                  "row": 202,
                  "col": 1
               },
               "names": {
                  "longName": "Zach LaVine 5+",
                  "shortName": "Zach LaVine 5+"
               },
               "wasPrice": [],
               "id": 202519195,
               "name": "Zach LaVine 5+",
               "type": "5",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.44",
                  "frac": "4/9",
                  "rootIdx": 43
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Zach LaVine 4+",
                  "shortName": "Zach LaVine 4+"
               },
               "wasPrice": [],
               "id": 202519193,
               "name": "Zach LaVine 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "22",
                  "frac": "21/1",
                  "rootIdx": 213
               },
               "names": {
                  "longName": "Zach LaVine 10+",
                  "shortName": "Zach LaVine 10+"
               },
               "wasPrice": [],
               "id": 202519191,
               "name": "Zach LaVine 10+",
               "type": "10",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Zach LaVine rebounds (incl. overtime)",
            "shortName": "Zach LaVine rebounds (incl. overtime)"
         },
         "id": 69729722,
         "name": "Zach LaVine rebounds (incl. overtime)",
         "type": "BASKETBALL:FT:PLREB",
         "subtype": "M#pre:playerprops:24750896:607400",
         "displayOrder": -274,
         "columnCount": 1,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.05",
                  "frac": "21/20",
                  "rootIdx": 88
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 225.5",
                  "shortName": "Over 225.5"
               },
               "wasPrice": [],
               "id": 202471279,
               "name": "Over 225.5",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.72",
                  "frac": "8/11",
                  "rootIdx": 60
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 225.5",
                  "shortName": "Under 225.5"
               },
               "wasPrice": [],
               "id": 202471281,
               "name": "Under 225.5",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Total Points (Excl. OT)",
            "shortName": "Total Points (Excl. OT)"
         },
         "id": 69715281,
         "name": "Total Points (Excl. OT)",
         "type": "BASKETBALL:FT:OU",
         "subtype": "M#225.5",
         "line": 225.5,
         "displayOrder": -450,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "12.5",
                  "frac": "23/2",
                  "rootIdx": 201
               },
               "names": {
                  "longName": "Coby White 10+",
                  "shortName": "Coby White 10+"
               },
               "wasPrice": [],
               "id": 202519389,
               "name": "Coby White 10+",
               "type": "10",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4.2",
                  "frac": "16/5",
                  "rootIdx": 144
               },
               "pos": {
                  "row": 203,
                  "col": 1
               },
               "names": {
                  "longName": "Coby White 8+",
                  "shortName": "Coby White 8+"
               },
               "wasPrice": [],
               "id": 202519399,
               "name": "Coby White 8+",
               "type": "8",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.75",
                  "frac": "7/4",
                  "rootIdx": 114
               },
               "pos": {
                  "row": 103,
                  "col": 1
               },
               "names": {
                  "longName": "Coby White 7+",
                  "shortName": "Coby White 7+"
               },
               "wasPrice": [],
               "id": 202519397,
               "name": "Coby White 7+",
               "type": "7",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.1",
                  "frac": "11/10",
                  "rootIdx": 93
               },
               "pos": {
                  "row": 302,
                  "col": 1
               },
               "names": {
                  "longName": "Coby White 6+",
                  "shortName": "Coby White 6+"
               },
               "wasPrice": [],
               "id": 202519395,
               "name": "Coby White 6+",
               "type": "6",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.53",
                  "frac": "8/15",
                  "rootIdx": 50
               },
               "pos": {
                  "row": 202,
                  "col": 1
               },
               "names": {
                  "longName": "Coby White 5+",
                  "shortName": "Coby White 5+"
               },
               "wasPrice": [],
               "id": 202519393,
               "name": "Coby White 5+",
               "type": "5",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.18",
                  "frac": "2/11",
                  "rootIdx": 26
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Coby White 4+",
                  "shortName": "Coby White 4+"
               },
               "wasPrice": [],
               "id": 202519391,
               "name": "Coby White 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "7",
                  "frac": "6/1",
                  "rootIdx": 184
               },
               "names": {
                  "longName": "Coby White 9+",
                  "shortName": "Coby White 9+"
               },
               "wasPrice": [],
               "id": 202519401,
               "name": "Coby White 9+",
               "type": "9",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Coby White assists (incl. overtime)",
            "shortName": "Coby White assists (incl. overtime)"
         },
         "id": 69729758,
         "name": "Coby White assists (incl. overtime)",
         "type": "BASKETBALL:FT:PLASS",
         "subtype": "M#pre:playerprops:24750896:1582800",
         "displayOrder": -273,
         "columnCount": 1,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.88",
                  "frac": "15/8",
                  "rootIdx": 117
               },
               "names": {
                  "longName": "Otto Porter 20+",
                  "shortName": "Otto Porter 20+"
               },
               "wasPrice": [],
               "id": 202631239,
               "name": "Otto Porter 20+",
               "type": "20",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.53",
                  "frac": "8/15",
                  "rootIdx": 50
               },
               "names": {
                  "longName": "Otto Porter 14+",
                  "shortName": "Otto Porter 14+"
               },
               "wasPrice": [],
               "id": 202631245,
               "name": "Otto Porter 14+",
               "type": "14",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "5",
                  "frac": "4/1",
                  "rootIdx": 160
               },
               "names": {
                  "longName": "Otto Porter 24+",
                  "shortName": "Otto Porter 24+"
               },
               "wasPrice": [],
               "id": 202631243,
               "name": "Otto Porter 24+",
               "type": "24",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.8",
                  "frac": "14/5",
                  "rootIdx": 136
               },
               "names": {
                  "longName": "Otto Porter 22+",
                  "shortName": "Otto Porter 22+"
               },
               "wasPrice": [],
               "id": 202631241,
               "name": "Otto Porter 22+",
               "type": "22",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "9.75",
                  "frac": "35/4",
                  "rootIdx": 195
               },
               "names": {
                  "longName": "Otto Porter 30+",
                  "shortName": "Otto Porter 30+"
               },
               "wasPrice": [],
               "id": 202631237,
               "name": "Otto Porter 30+",
               "type": "30",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.25",
                  "frac": "5/4",
                  "rootIdx": 101
               },
               "names": {
                  "longName": "Otto Porter 18+",
                  "shortName": "Otto Porter 18+"
               },
               "wasPrice": [],
               "id": 202631235,
               "name": "Otto Porter 18+",
               "type": "18",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "8.25",
                  "frac": "29/4",
                  "rootIdx": 189
               },
               "names": {
                  "longName": "Otto Porter 28+",
                  "shortName": "Otto Porter 28+"
               },
               "wasPrice": [],
               "id": 202631233,
               "name": "Otto Porter 28+",
               "type": "28",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "names": {
                  "longName": "Otto Porter 16+",
                  "shortName": "Otto Porter 16+"
               },
               "wasPrice": [],
               "id": 202631231,
               "name": "Otto Porter 16+",
               "type": "16",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "6.6",
                  "frac": "28/5",
                  "rootIdx": 179
               },
               "names": {
                  "longName": "Otto Porter 26+",
                  "shortName": "Otto Porter 26+"
               },
               "wasPrice": [],
               "id": 202631229,
               "name": "Otto Porter 26+",
               "type": "26",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Otto Porter points (incl. overtime)",
            "shortName": "Otto Porter points (incl. overtime)"
         },
         "id": 69761032,
         "name": "Otto Porter points (incl. overtime)",
         "type": "BASKETBALL:FT:PLPOI",
         "subtype": "M#pre:playerprops:24750896:607522",
         "displayOrder": -275,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2",
                  "frac": "1/1",
                  "rootIdx": 83
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 221.5",
                  "shortName": "Under 221.5"
               },
               "wasPrice": [],
               "id": 202471037,
               "name": "Under 221.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.75",
                  "frac": "3/4",
                  "rootIdx": 63
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 221.5",
                  "shortName": "Over 221.5"
               },
               "wasPrice": [],
               "id": 202471035,
               "name": "Over 221.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715167,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#221.5",
         "line": 221.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.25",
                  "frac": "5/4",
                  "rootIdx": 101
               },
               "names": {
                  "longName": "Zach LaVine 28+",
                  "shortName": "Zach LaVine 28+"
               },
               "wasPrice": [],
               "id": 202519373,
               "name": "Zach LaVine 28+",
               "type": "28",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2",
                  "frac": "1/1",
                  "rootIdx": 83
               },
               "names": {
                  "longName": "Zach LaVine 26+",
                  "shortName": "Zach LaVine 26+"
               },
               "wasPrice": [],
               "id": 202519371,
               "name": "Zach LaVine 26+",
               "type": "26",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.36",
                  "frac": "4/11",
                  "rootIdx": 39
               },
               "names": {
                  "longName": "Zach LaVine 22+",
                  "shortName": "Zach LaVine 22+"
               },
               "wasPrice": [],
               "id": 202519367,
               "name": "Zach LaVine 22+",
               "type": "22",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "names": {
                  "longName": "Zach LaVine 24+",
                  "shortName": "Zach LaVine 24+"
               },
               "wasPrice": [],
               "id": 202519369,
               "name": "Zach LaVine 24+",
               "type": "24",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4.75",
                  "frac": "15/4",
                  "rootIdx": 156
               },
               "names": {
                  "longName": "Zach LaVine 36+",
                  "shortName": "Zach LaVine 36+"
               },
               "wasPrice": [],
               "id": 202519381,
               "name": "Zach LaVine 36+",
               "type": "36",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "6",
                  "frac": "5/1",
                  "rootIdx": 172
               },
               "names": {
                  "longName": "Zach LaVine 38+",
                  "shortName": "Zach LaVine 38+"
               },
               "wasPrice": [],
               "id": 202519383,
               "name": "Zach LaVine 38+",
               "type": "38",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.7",
                  "frac": "27/10",
                  "rootIdx": 134
               },
               "names": {
                  "longName": "Zach LaVine 34+",
                  "shortName": "Zach LaVine 34+"
               },
               "wasPrice": [],
               "id": 202519379,
               "name": "Zach LaVine 34+",
               "type": "34",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.95",
                  "frac": "39/20",
                  "rootIdx": 119
               },
               "names": {
                  "longName": "Zach LaVine 32+",
                  "shortName": "Zach LaVine 32+"
               },
               "wasPrice": [],
               "id": 202519377,
               "name": "Zach LaVine 32+",
               "type": "32",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.55",
                  "frac": "31/20",
                  "rootIdx": 109
               },
               "names": {
                  "longName": "Zach LaVine 30+",
                  "shortName": "Zach LaVine 30+"
               },
               "wasPrice": [],
               "id": 202519375,
               "name": "Zach LaVine 30+",
               "type": "30",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Zach LaVine points (incl. overtime)",
            "shortName": "Zach LaVine points (incl. overtime)"
         },
         "id": 69729754,
         "name": "Zach LaVine points (incl. overtime)",
         "type": "BASKETBALL:FT:PLPOI",
         "subtype": "M#pre:playerprops:24750896:607400",
         "displayOrder": -275,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.72",
                  "frac": "8/11",
                  "rootIdx": 60
               },
               "pos": {
                  "row": 202,
                  "col": 1
               },
               "names": {
                  "longName": "Otto Porter 5+",
                  "shortName": "Otto Porter 5+"
               },
               "wasPrice": [],
               "id": 202631251,
               "name": "Otto Porter 5+",
               "type": "5",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "6.9",
                  "frac": "59/10",
                  "rootIdx": 183
               },
               "names": {
                  "longName": "Otto Porter 9+",
                  "shortName": "Otto Porter 9+"
               },
               "wasPrice": [],
               "id": 202631259,
               "name": "Otto Porter 9+",
               "type": "9",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4.2",
                  "frac": "16/5",
                  "rootIdx": 144
               },
               "pos": {
                  "row": 203,
                  "col": 1
               },
               "names": {
                  "longName": "Otto Porter 8+",
                  "shortName": "Otto Porter 8+"
               },
               "wasPrice": [],
               "id": 202631257,
               "name": "Otto Porter 8+",
               "type": "8",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.8",
                  "frac": "9/5",
                  "rootIdx": 115
               },
               "pos": {
                  "row": 103,
                  "col": 1
               },
               "names": {
                  "longName": "Otto Porter 7+",
                  "shortName": "Otto Porter 7+"
               },
               "wasPrice": [],
               "id": 202631255,
               "name": "Otto Porter 7+",
               "type": "7",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.05",
                  "frac": "21/20",
                  "rootIdx": 88
               },
               "pos": {
                  "row": 302,
                  "col": 1
               },
               "names": {
                  "longName": "Otto Porter 6+",
                  "shortName": "Otto Porter 6+"
               },
               "wasPrice": [],
               "id": 202631253,
               "name": "Otto Porter 6+",
               "type": "6",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.36",
                  "frac": "4/11",
                  "rootIdx": 39
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Otto Porter 4+",
                  "shortName": "Otto Porter 4+"
               },
               "wasPrice": [],
               "id": 202631249,
               "name": "Otto Porter 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "23",
                  "frac": "22/1",
                  "rootIdx": 214
               },
               "names": {
                  "longName": "Otto Porter 11+",
                  "shortName": "Otto Porter 11+"
               },
               "wasPrice": [],
               "id": 202631247,
               "name": "Otto Porter 11+",
               "type": "11",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "14",
                  "frac": "13/1",
                  "rootIdx": 204
               },
               "names": {
                  "longName": "Otto Porter 10+",
                  "shortName": "Otto Porter 10+"
               },
               "wasPrice": [],
               "id": 202631261,
               "name": "Otto Porter 10+",
               "type": "10",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Otto Porter rebounds (incl. overtime)",
            "shortName": "Otto Porter rebounds (incl. overtime)"
         },
         "id": 69761034,
         "name": "Otto Porter rebounds (incl. overtime)",
         "type": "BASKETBALL:FT:PLREB",
         "subtype": "M#pre:playerprops:24750896:607522",
         "displayOrder": -274,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.95",
                  "frac": "39/20",
                  "rootIdx": 119
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Dallas Mavericks & Under 222.5",
                  "shortName": "Dallas Mavericks & Under 222.5"
               },
               "wasPrice": [],
               "id": 202637451,
               "name": "Dallas Mavericks & Under 222.5",
               "type": "BUnder",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "4.55",
                  "frac": "71/20",
                  "rootIdx": 152
               },
               "pos": {
                  "row": 2,
                  "col": 2
               },
               "names": {
                  "longName": "Chicago Bulls & Over 222.5",
                  "shortName": "Chicago Bulls & Over 222.5"
               },
               "wasPrice": [],
               "id": 202637453,
               "name": "Chicago Bulls & Over 222.5",
               "type": "AOver",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "4.6",
                  "frac": "18/5",
                  "rootIdx": 153
               },
               "pos": {
                  "row": 2,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls & Under 222.5",
                  "shortName": "Chicago Bulls & Under 222.5"
               },
               "wasPrice": [],
               "id": 202637449,
               "name": "Chicago Bulls & Under 222.5",
               "type": "AUnder",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.9",
                  "frac": "19/10",
                  "rootIdx": 118
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks & Over 222.5",
                  "shortName": "Dallas Mavericks & Over 222.5"
               },
               "wasPrice": [],
               "id": 202637447,
               "name": "Dallas Mavericks & Over 222.5",
               "type": "BOver",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Matchbet and Total Points (Incl. OT)",
            "shortName": "Matchbet and Total Points (Incl. OT)"
         },
         "id": 69763852,
         "name": "Matchbet and Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:AXB$OU",
         "subtype": "M#222.5",
         "line": 222.5,
         "displayOrder": -425,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "19",
                  "frac": "18/1",
                  "rootIdx": 210
               },
               "names": {
                  "longName": "Patrick Williams 9+",
                  "shortName": "Patrick Williams 9+"
               },
               "wasPrice": [],
               "id": 202519189,
               "name": "Patrick Williams 9+",
               "type": "9",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.3",
                  "frac": "13/10",
                  "rootIdx": 103
               },
               "pos": {
                  "row": 202,
                  "col": 1
               },
               "names": {
                  "longName": "Patrick Williams 5+",
                  "shortName": "Patrick Williams 5+"
               },
               "wasPrice": [],
               "id": 202519181,
               "name": "Patrick Williams 5+",
               "type": "5",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "11",
                  "frac": "10/1",
                  "rootIdx": 198
               },
               "pos": {
                  "row": 203,
                  "col": 1
               },
               "names": {
                  "longName": "Patrick Williams 8+",
                  "shortName": "Patrick Williams 8+"
               },
               "wasPrice": [],
               "id": 202519187,
               "name": "Patrick Williams 8+",
               "type": "8",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "6.2",
                  "frac": "26/5",
                  "rootIdx": 174
               },
               "pos": {
                  "row": 103,
                  "col": 1
               },
               "names": {
                  "longName": "Patrick Williams 7+",
                  "shortName": "Patrick Williams 7+"
               },
               "wasPrice": [],
               "id": 202519185,
               "name": "Patrick Williams 7+",
               "type": "7",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.45",
                  "frac": "49/20",
                  "rootIdx": 129
               },
               "pos": {
                  "row": 302,
                  "col": 1
               },
               "names": {
                  "longName": "Patrick Williams 6+",
                  "shortName": "Patrick Williams 6+"
               },
               "wasPrice": [],
               "id": 202519183,
               "name": "Patrick Williams 6+",
               "type": "6",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.45",
                  "frac": "9/20",
                  "rootIdx": 44
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Patrick Williams 4+",
                  "shortName": "Patrick Williams 4+"
               },
               "wasPrice": [],
               "id": 202519179,
               "name": "Patrick Williams 4+",
               "type": "4",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Patrick Williams rebounds (incl. overtime)",
            "shortName": "Patrick Williams rebounds (incl. overtime)"
         },
         "id": 69729720,
         "name": "Patrick Williams rebounds (incl. overtime)",
         "type": "BASKETBALL:FT:PLREB",
         "subtype": "M#pre:playerprops:24750896:1797198",
         "displayOrder": -274,
         "columnCount": 1,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "5.75",
                  "frac": "19/4",
                  "rootIdx": 169
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -17",
                  "shortName": "Dallas Mavericks -17"
               },
               "wasPrice": [],
               "id": 202330979,
               "name": "Dallas Mavericks -17",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.11",
                  "frac": "1/9",
                  "rootIdx": 18
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +17",
                  "shortName": "Chicago Bulls +17"
               },
               "wasPrice": [],
               "id": 202330976,
               "name": "Chicago Bulls +17",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666804,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#17",
         "line": 17.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "5.25",
                  "frac": "17/4",
                  "rootIdx": 163
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -16",
                  "shortName": "Dallas Mavericks -16"
               },
               "wasPrice": [],
               "id": 202331020,
               "name": "Dallas Mavericks -16",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.14",
                  "frac": "1/7",
                  "rootIdx": 21
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +16",
                  "shortName": "Chicago Bulls +16"
               },
               "wasPrice": [],
               "id": 202331005,
               "name": "Chicago Bulls +16",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666826,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#16",
         "line": 16.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "6.5",
                  "frac": "11/2",
                  "rootIdx": 178
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -19",
                  "shortName": "Dallas Mavericks -19"
               },
               "wasPrice": [],
               "id": 202377521,
               "name": "Dallas Mavericks -19",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.09",
                  "frac": "1/11",
                  "rootIdx": 16
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +19",
                  "shortName": "Chicago Bulls +19"
               },
               "wasPrice": [],
               "id": 202377519,
               "name": "Chicago Bulls +19",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69680655,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#19",
         "line": 19.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.75",
                  "frac": "3/4",
                  "rootIdx": 63
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Odd",
                  "shortName": "Odd"
               },
               "wasPrice": [],
               "id": 202471089,
               "name": "Odd",
               "type": "Odd",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Even",
                  "shortName": "Even"
               },
               "wasPrice": [],
               "id": 202471087,
               "name": "Even",
               "type": "Even",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Odd/even (incl. overtime)",
            "shortName": "Odd/even (incl. overtime)"
         },
         "id": 69715191,
         "name": "Odd/even (incl. overtime)",
         "type": "BASKETBALL:FTOT:OE",
         "subtype": "M",
         "displayOrder": -259,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "6.5",
                  "frac": "11/2",
                  "rootIdx": 178
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -18",
                  "shortName": "Dallas Mavericks -18"
               },
               "wasPrice": [],
               "id": 202330975,
               "name": "Dallas Mavericks -18",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.09",
                  "frac": "1/11",
                  "rootIdx": 16
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +18",
                  "shortName": "Chicago Bulls +18"
               },
               "wasPrice": [],
               "id": 202330969,
               "name": "Chicago Bulls +18",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666803,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#18",
         "line": 18.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.57",
                  "frac": "4/7",
                  "rootIdx": 52
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 30.5",
                  "shortName": "Under 30.5"
               },
               "wasPrice": [],
               "id": 202471049,
               "name": "Under 30.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.25",
                  "frac": "5/4",
                  "rootIdx": 101
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 30.5",
                  "shortName": "Over 30.5"
               },
               "wasPrice": [],
               "id": 202471047,
               "name": "Over 30.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Quarter Dallas Mavericks Total Points",
            "shortName": "1st Quarter Dallas Mavericks Total Points"
         },
         "id": 69715173,
         "name": "1st Quarter Dallas Mavericks Total Points",
         "type": "BASKETBALL:P:B:OU",
         "subtype": "Q1#30.5",
         "line": 30.5,
         "displayOrder": -317,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3.9",
                  "frac": "29/10",
                  "rootIdx": 138
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -12.5",
                  "shortName": "Dallas Mavericks -12.5"
               },
               "wasPrice": [],
               "id": 202330948,
               "name": "Dallas Mavericks -12.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.22",
                  "frac": "2/9",
                  "rootIdx": 30
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +12.5",
                  "shortName": "Chicago Bulls +12.5"
               },
               "wasPrice": [],
               "id": 202330935,
               "name": "Chicago Bulls +12.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666781,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#12.5",
         "line": 12.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.33",
                  "frac": "1/3",
                  "rootIdx": 37
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks +4.5",
                  "shortName": "Dallas Mavericks +4.5"
               },
               "wasPrice": [],
               "id": 202330832,
               "name": "Dallas Mavericks +4.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.15",
                  "frac": "43/20",
                  "rootIdx": 123
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls -4.5",
                  "shortName": "Chicago Bulls -4.5"
               },
               "wasPrice": [],
               "id": 202330819,
               "name": "Chicago Bulls -4.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666742,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-4.5",
         "line": -4.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3.3",
                  "frac": "23/10",
                  "rootIdx": 126
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 233.5",
                  "shortName": "Over 233.5"
               },
               "wasPrice": [],
               "id": 202470863,
               "name": "Over 233.5",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.33",
                  "frac": "1/3",
                  "rootIdx": 37
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 233.5",
                  "shortName": "Under 233.5"
               },
               "wasPrice": [],
               "id": 202470865,
               "name": "Under 233.5",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715095,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#233.5",
         "line": 233.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3.75",
                  "frac": "11/4",
                  "rootIdx": 135
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 210.5",
                  "shortName": "Under 210.5"
               },
               "wasPrice": [],
               "id": 202637455,
               "name": "Under 210.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.25",
                  "frac": "1/4",
                  "rootIdx": 32
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 210.5",
                  "shortName": "Over 210.5"
               },
               "wasPrice": [],
               "id": 202637457,
               "name": "Over 210.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69763854,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#210.5",
         "line": 210.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "20",
                  "frac": "19/1",
                  "rootIdx": 211
               },
               "pos": {
                  "row": 103,
                  "col": 1
               },
               "names": {
                  "longName": "Wendell Carter 7+",
                  "shortName": "Wendell Carter 7+"
               },
               "wasPrice": [],
               "id": 202519273,
               "name": "Wendell Carter 7+",
               "type": "7",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "14",
                  "frac": "13/1",
                  "rootIdx": 204
               },
               "pos": {
                  "row": 302,
                  "col": 1
               },
               "names": {
                  "longName": "Wendell Carter 6+",
                  "shortName": "Wendell Carter 6+"
               },
               "wasPrice": [],
               "id": 202519271,
               "name": "Wendell Carter 6+",
               "type": "6",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "9.25",
                  "frac": "33/4",
                  "rootIdx": 193
               },
               "pos": {
                  "row": 202,
                  "col": 1
               },
               "names": {
                  "longName": "Wendell Carter 5+",
                  "shortName": "Wendell Carter 5+"
               },
               "wasPrice": [],
               "id": 202519269,
               "name": "Wendell Carter 5+",
               "type": "5",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4.85",
                  "frac": "77/20",
                  "rootIdx": 158
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Wendell Carter 4+",
                  "shortName": "Wendell Carter 4+"
               },
               "wasPrice": [],
               "id": 202519267,
               "name": "Wendell Carter 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.3",
                  "frac": "13/10",
                  "rootIdx": 103
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Wendell Carter 3+",
                  "shortName": "Wendell Carter 3+"
               },
               "wasPrice": [],
               "id": 202519265,
               "name": "Wendell Carter 3+",
               "type": "3",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Wendell Carter assists (incl. overtime)",
            "shortName": "Wendell Carter assists (incl. overtime)"
         },
         "id": 69729734,
         "name": "Wendell Carter assists (incl. overtime)",
         "type": "BASKETBALL:FT:PLASS",
         "subtype": "M#pre:playerprops:24750896:1304564",
         "displayOrder": -273,
         "columnCount": 1,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "20",
                  "frac": "19/1",
                  "rootIdx": 211
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Tie +4",
                  "shortName": "Tie +4"
               },
               "wasPrice": [],
               "id": 202631335,
               "name": "Tie +4",
               "type": "DH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 3
               },
               "names": {
                  "longName": "Dallas Mavericks -4",
                  "shortName": "Dallas Mavericks -4"
               },
               "wasPrice": [],
               "id": 202631333,
               "name": "Dallas Mavericks -4",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +4",
                  "shortName": "Chicago Bulls +4"
               },
               "wasPrice": [],
               "id": 202631331,
               "name": "Chicago Bulls +4",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Handicap (Excl. OT)",
            "shortName": "Handicap (Excl. OT)"
         },
         "id": 69761068,
         "name": "Handicap (Excl. OT)",
         "type": "BASKETBALL:FT:3HCP",
         "subtype": "M#+4",
         "line": 4.0,
         "displayOrder": -465,
         "columnCount": 3,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.7",
                  "frac": "7/10",
                  "rootIdx": 59
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -2.5",
                  "shortName": "Dallas Mavericks -2.5"
               },
               "wasPrice": [],
               "id": 202330998,
               "name": "Dallas Mavericks -2.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.15",
                  "frac": "23/20",
                  "rootIdx": 96
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +2.5",
                  "shortName": "Chicago Bulls +2.5"
               },
               "wasPrice": [],
               "id": 202330989,
               "name": "Chicago Bulls +2.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666820,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#2.5",
         "line": 2.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.15",
                  "frac": "23/20",
                  "rootIdx": 96
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 26.5",
                  "shortName": "Under 26.5"
               },
               "wasPrice": [],
               "id": 202474013,
               "name": "Under 26.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.62",
                  "frac": "8/13",
                  "rootIdx": 54
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 26.5",
                  "shortName": "Over 26.5"
               },
               "wasPrice": [],
               "id": 202474011,
               "name": "Over 26.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Quarter Chicago Bulls Total Points",
            "shortName": "1st Quarter Chicago Bulls Total Points"
         },
         "id": 69716447,
         "name": "1st Quarter Chicago Bulls Total Points",
         "type": "BASKETBALL:P:A:OU",
         "subtype": "Q1#26.5",
         "line": 26.5,
         "displayOrder": -318,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q1"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "10.5",
                  "frac": "19/2",
                  "rootIdx": 197
               },
               "names": {
                  "longName": "Zach LaVine 9+",
                  "shortName": "Zach LaVine 9+"
               },
               "wasPrice": [],
               "id": 202519295,
               "name": "Zach LaVine 9+",
               "type": "9",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.55",
                  "frac": "31/20",
                  "rootIdx": 109
               },
               "pos": {
                  "row": 302,
                  "col": 1
               },
               "names": {
                  "longName": "Zach LaVine 6+",
                  "shortName": "Zach LaVine 6+"
               },
               "wasPrice": [],
               "id": 202519289,
               "name": "Zach LaVine 6+",
               "type": "6",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "6",
                  "frac": "5/1",
                  "rootIdx": 172
               },
               "pos": {
                  "row": 203,
                  "col": 1
               },
               "names": {
                  "longName": "Zach LaVine 8+",
                  "shortName": "Zach LaVine 8+"
               },
               "wasPrice": [],
               "id": 202519293,
               "name": "Zach LaVine 8+",
               "type": "8",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.75",
                  "frac": "11/4",
                  "rootIdx": 135
               },
               "pos": {
                  "row": 103,
                  "col": 1
               },
               "names": {
                  "longName": "Zach LaVine 7+",
                  "shortName": "Zach LaVine 7+"
               },
               "wasPrice": [],
               "id": 202519291,
               "name": "Zach LaVine 7+",
               "type": "7",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 202,
                  "col": 1
               },
               "names": {
                  "longName": "Zach LaVine 5+",
                  "shortName": "Zach LaVine 5+"
               },
               "wasPrice": [],
               "id": 202519287,
               "name": "Zach LaVine 5+",
               "type": "5",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.53",
                  "frac": "8/15",
                  "rootIdx": 50
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Zach LaVine 4+",
                  "shortName": "Zach LaVine 4+"
               },
               "wasPrice": [],
               "id": 202519285,
               "name": "Zach LaVine 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.2",
                  "frac": "1/5",
                  "rootIdx": 28
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Zach LaVine 3+",
                  "shortName": "Zach LaVine 3+"
               },
               "wasPrice": [],
               "id": 202519283,
               "name": "Zach LaVine 3+",
               "type": "3",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Zach LaVine assists (incl. overtime)",
            "shortName": "Zach LaVine assists (incl. overtime)"
         },
         "id": 69729738,
         "name": "Zach LaVine assists (incl. overtime)",
         "type": "BASKETBALL:FT:PLASS",
         "subtype": "M#pre:playerprops:24750896:607400",
         "displayOrder": -273,
         "columnCount": 1,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 108.5",
                  "shortName": "Under 108.5"
               },
               "wasPrice": [],
               "id": 202637469,
               "name": "Under 108.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 108.5",
                  "shortName": "Over 108.5"
               },
               "wasPrice": [],
               "id": 202637467,
               "name": "Over 108.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Chicago Bulls Total Points (Incl. OT)",
            "shortName": "Chicago Bulls Total Points (Incl. OT)"
         },
         "id": 69763860,
         "name": "Chicago Bulls Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:A:OU",
         "subtype": "M#108.5",
         "line": 108.5,
         "displayOrder": -445,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "20",
                  "frac": "19/1",
                  "rootIdx": 211
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Tie +3",
                  "shortName": "Tie +3"
               },
               "wasPrice": [],
               "id": 202735533,
               "name": "Tie +3",
               "type": "DH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2",
                  "frac": "1/1",
                  "rootIdx": 83
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +3",
                  "shortName": "Chicago Bulls +3"
               },
               "wasPrice": [],
               "id": 202735535,
               "name": "Chicago Bulls +3",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 3
               },
               "names": {
                  "longName": "Dallas Mavericks -3",
                  "shortName": "Dallas Mavericks -3"
               },
               "wasPrice": [],
               "id": 202735537,
               "name": "Dallas Mavericks -3",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Handicap (Excl. OT)",
            "shortName": "Handicap (Excl. OT)"
         },
         "id": 69799572,
         "name": "Handicap (Excl. OT)",
         "type": "BASKETBALL:FT:3HCP",
         "subtype": "M#+3",
         "line": 3.0,
         "displayOrder": -465,
         "columnCount": 3,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 55.5",
                  "shortName": "Over 55.5"
               },
               "wasPrice": [],
               "id": 202478767,
               "name": "Over 55.5",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 55.5",
                  "shortName": "Under 55.5"
               },
               "wasPrice": [],
               "id": 202478765,
               "name": "Under 55.5",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Half Chicago Bulls Total Points",
            "shortName": "1st Half Chicago Bulls Total Points"
         },
         "id": 69718315,
         "name": "1st Half Chicago Bulls Total Points",
         "type": "BASKETBALL:P:A:OU",
         "subtype": "H1#55.5",
         "line": 55.5,
         "displayOrder": -355,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "H1"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.6",
                  "frac": "8/5",
                  "rootIdx": 110
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Otto Porter 3+",
                  "shortName": "Otto Porter 3+"
               },
               "wasPrice": [],
               "id": 202631211,
               "name": "Otto Porter 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "18",
                  "frac": "17/1",
                  "rootIdx": 209
               },
               "pos": {
                  "row": 302,
                  "col": 1
               },
               "names": {
                  "longName": "Otto Porter 6+",
                  "shortName": "Otto Porter 6+"
               },
               "wasPrice": [],
               "id": 202631217,
               "name": "Otto Porter 6+",
               "type": "6",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "9.5",
                  "frac": "17/2",
                  "rootIdx": 194
               },
               "pos": {
                  "row": 202,
                  "col": 1
               },
               "names": {
                  "longName": "Otto Porter 5+",
                  "shortName": "Otto Porter 5+"
               },
               "wasPrice": [],
               "id": 202631215,
               "name": "Otto Porter 5+",
               "type": "5",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4.8",
                  "frac": "19/5",
                  "rootIdx": 157
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Otto Porter 4+",
                  "shortName": "Otto Porter 4+"
               },
               "wasPrice": [],
               "id": 202631213,
               "name": "Otto Porter 4+",
               "type": "4",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Otto Porter assists (incl. overtime)",
            "shortName": "Otto Porter assists (incl. overtime)"
         },
         "id": 69761028,
         "name": "Otto Porter assists (incl. overtime)",
         "type": "BASKETBALL:FT:PLASS",
         "subtype": "M#pre:playerprops:24750896:607522",
         "displayOrder": -273,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.05",
                  "frac": "21/20",
                  "rootIdx": 88
               },
               "pos": {
                  "row": 1,
                  "col": 3
               },
               "names": {
                  "longName": "Dallas Mavericks -6",
                  "shortName": "Dallas Mavericks -6"
               },
               "wasPrice": [],
               "id": 202637117,
               "name": "Dallas Mavericks -6",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +6",
                  "shortName": "Chicago Bulls +6"
               },
               "wasPrice": [],
               "id": 202637115,
               "name": "Chicago Bulls +6",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "17",
                  "frac": "16/1",
                  "rootIdx": 208
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Tie +6",
                  "shortName": "Tie +6"
               },
               "wasPrice": [],
               "id": 202637119,
               "name": "Tie +6",
               "type": "DH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Handicap (Excl. OT)",
            "shortName": "Handicap (Excl. OT)"
         },
         "id": 69763708,
         "name": "Handicap (Excl. OT)",
         "type": "BASKETBALL:FT:3HCP",
         "subtype": "M#+6",
         "line": 6.0,
         "displayOrder": -465,
         "columnCount": 3,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2",
                  "frac": "1/1",
                  "rootIdx": 83
               },
               "pos": {
                  "row": 1,
                  "col": 3
               },
               "names": {
                  "longName": "Dallas Mavericks -5",
                  "shortName": "Dallas Mavericks -5"
               },
               "wasPrice": [],
               "id": 202470949,
               "name": "Dallas Mavericks -5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +5",
                  "shortName": "Chicago Bulls +5"
               },
               "wasPrice": [],
               "id": 202470947,
               "name": "Chicago Bulls +5",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "17",
                  "frac": "16/1",
                  "rootIdx": 208
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Tie +5",
                  "shortName": "Tie +5"
               },
               "wasPrice": [],
               "id": 202470951,
               "name": "Tie +5",
               "type": "DH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Handicap (Excl. OT)",
            "shortName": "Handicap (Excl. OT)"
         },
         "id": 69715127,
         "name": "Handicap (Excl. OT)",
         "type": "BASKETBALL:FT:3HCP",
         "subtype": "M#+5",
         "line": 5.0,
         "displayOrder": -465,
         "columnCount": 3,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under {Line}",
                  "shortName": "Under {Line}"
               },
               "wasPrice": [],
               "id": 202470869,
               "name": "Under {Line}",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over {Line}",
                  "shortName": "Over {Line}"
               },
               "wasPrice": [],
               "id": 202470867,
               "name": "Over {Line}",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Total Points",
            "shortName": "Total Points"
         },
         "id": 69715097,
         "name": "Total Points",
         "type": "BASKETBALL:FTOT:OU_MAIN",
         "subtype": "222.5",
         "line": 222.5,
         "lineType": "TOTAL",
         "displayOrder": -490,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "222.5"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 111.5",
                  "shortName": "Under 111.5"
               },
               "wasPrice": [],
               "id": 202632475,
               "name": "Under 111.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 111.5",
                  "shortName": "Over 111.5"
               },
               "wasPrice": [],
               "id": 202632473,
               "name": "Over 111.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Half Total Points",
            "shortName": "1st Half Total Points"
         },
         "id": 69761588,
         "name": "1st Half Total Points",
         "type": "BASKETBALL:P:OU",
         "subtype": "H1#111.5",
         "line": 111.5,
         "displayOrder": -365,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "H1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.15",
                  "frac": "23/20",
                  "rootIdx": 96
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 219.5",
                  "shortName": "Under 219.5"
               },
               "wasPrice": [],
               "id": 202471257,
               "name": "Under 219.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.67",
                  "frac": "4/6",
                  "rootIdx": 57
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 219.5",
                  "shortName": "Over 219.5"
               },
               "wasPrice": [],
               "id": 202471255,
               "name": "Over 219.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715269,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#219.5",
         "line": 219.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.85",
                  "frac": "17/20",
                  "rootIdx": 70
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 222.5",
                  "shortName": "Over 222.5"
               },
               "wasPrice": [],
               "id": 202470847,
               "name": "Over 222.5",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 222.5",
                  "shortName": "Under 222.5"
               },
               "wasPrice": [],
               "id": 202470849,
               "name": "Under 222.5",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715087,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#222.5",
         "line": 222.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.8",
                  "frac": "9/5",
                  "rootIdx": 115
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks & Over 225.5",
                  "shortName": "Dallas Mavericks & Over 225.5"
               },
               "wasPrice": [],
               "id": 202471019,
               "name": "Dallas Mavericks & Over 225.5",
               "type": "BOver",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "4.85",
                  "frac": "77/20",
                  "rootIdx": 158
               },
               "pos": {
                  "row": 2,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls & Under 225.5",
                  "shortName": "Chicago Bulls & Under 225.5"
               },
               "wasPrice": [],
               "id": 202471017,
               "name": "Chicago Bulls & Under 225.5",
               "type": "AUnder",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "4.85",
                  "frac": "77/20",
                  "rootIdx": 158
               },
               "pos": {
                  "row": 2,
                  "col": 2
               },
               "names": {
                  "longName": "Chicago Bulls & Over 225.5",
                  "shortName": "Chicago Bulls & Over 225.5"
               },
               "wasPrice": [],
               "id": 202471015,
               "name": "Chicago Bulls & Over 225.5",
               "type": "AOver",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.85",
                  "frac": "37/20",
                  "rootIdx": 116
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Dallas Mavericks & Under 225.5",
                  "shortName": "Dallas Mavericks & Under 225.5"
               },
               "wasPrice": [],
               "id": 202471021,
               "name": "Dallas Mavericks & Under 225.5",
               "type": "BUnder",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Matchbet and Total Points (Incl. OT)",
            "shortName": "Matchbet and Total Points (Incl. OT)"
         },
         "id": 69715159,
         "name": "Matchbet and Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:AXB$OU",
         "subtype": "M#225.5",
         "line": 225.5,
         "displayOrder": -425,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 56.5",
                  "shortName": "Over 56.5"
               },
               "wasPrice": [],
               "id": 202478735,
               "name": "Over 56.5",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.75",
                  "frac": "3/4",
                  "rootIdx": 63
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 56.5",
                  "shortName": "Under 56.5"
               },
               "wasPrice": [],
               "id": 202478733,
               "name": "Under 56.5",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Quarter Total Points",
            "shortName": "1st Quarter Total Points"
         },
         "id": 69718299,
         "name": "1st Quarter Total Points",
         "type": "BASKETBALL:P:OU",
         "subtype": "Q1#56.5",
         "line": 56.5,
         "displayOrder": -323,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "12",
                  "frac": "11/1",
                  "rootIdx": 200
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Tie",
                  "shortName": "Tie"
               },
               "wasPrice": [],
               "id": 202471005,
               "name": "Tie",
               "type": "Draw",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.62",
                  "frac": "8/13",
                  "rootIdx": 54
               },
               "pos": {
                  "row": 1,
                  "col": 3
               },
               "names": {
                  "longName": "Dallas Mavericks",
                  "shortName": "Dallas Mavericks"
               },
               "wasPrice": [],
               "id": 202471003,
               "name": "Dallas Mavericks",
               "type": "B",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.6",
                  "frac": "8/5",
                  "rootIdx": 110
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls",
                  "shortName": "Chicago Bulls"
               },
               "wasPrice": [],
               "id": 202471001,
               "name": "Chicago Bulls",
               "type": "A",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Money Line 3Way",
            "shortName": "Money Line 3Way"
         },
         "id": 69715153,
         "name": "Money Line 3Way",
         "type": "BASKETBALL:FT:AXB",
         "subtype": "M",
         "displayOrder": -481,
         "columnCount": 3,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.45",
                  "frac": "29/20",
                  "rootIdx": 107
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -7.5",
                  "shortName": "Dallas Mavericks -7.5"
               },
               "wasPrice": [],
               "id": 202330915,
               "name": "Dallas Mavericks -7.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.5",
                  "frac": "1/2",
                  "rootIdx": 47
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +7.5",
                  "shortName": "Chicago Bulls +7.5"
               },
               "wasPrice": [],
               "id": 202330913,
               "name": "Chicago Bulls +7.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666779,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#7.5",
         "line": 7.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.55",
                  "frac": "11/20",
                  "rootIdx": 51
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 29.5",
                  "shortName": "Under 29.5"
               },
               "wasPrice": [],
               "id": 202470959,
               "name": "Under 29.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.3",
                  "frac": "13/10",
                  "rootIdx": 103
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 29.5",
                  "shortName": "Over 29.5"
               },
               "wasPrice": [],
               "id": 202470957,
               "name": "Over 29.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Quarter Chicago Bulls Total Points",
            "shortName": "1st Quarter Chicago Bulls Total Points"
         },
         "id": 69715131,
         "name": "1st Quarter Chicago Bulls Total Points",
         "type": "BASKETBALL:P:A:OU",
         "subtype": "Q1#29.5",
         "line": 29.5,
         "displayOrder": -318,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -1.5",
                  "shortName": "Dallas Mavericks -1.5"
               },
               "wasPrice": [],
               "id": 202470857,
               "name": "Dallas Mavericks -1.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.75",
                  "frac": "3/4",
                  "rootIdx": 63
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +1.5",
                  "shortName": "Chicago Bulls +1.5"
               },
               "wasPrice": [],
               "id": 202470855,
               "name": "Chicago Bulls +1.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Quarter Spread",
            "shortName": "1st Quarter Spread"
         },
         "id": 69715091,
         "name": "1st Quarter Spread",
         "type": "BASKETBALL:P:PSPRD",
         "subtype": "Q1#1.5",
         "line": 1.5,
         "displayOrder": -324,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.2",
                  "frac": "1/5",
                  "rootIdx": 28
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +13.5",
                  "shortName": "Chicago Bulls +13.5"
               },
               "wasPrice": [],
               "id": 202330931,
               "name": "Chicago Bulls +13.5",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "4.2",
                  "frac": "16/5",
                  "rootIdx": 144
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -13.5",
                  "shortName": "Dallas Mavericks -13.5"
               },
               "wasPrice": [],
               "id": 202330943,
               "name": "Dallas Mavericks -13.5",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666784,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#13.5",
         "line": 13.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "25",
                  "frac": "24/1",
                  "rootIdx": 216
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Otto Porter 4+",
                  "shortName": "Otto Porter 4+"
               },
               "wasPrice": [],
               "id": 202631209,
               "name": "Otto Porter 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "7",
                  "frac": "6/1",
                  "rootIdx": 184
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Otto Porter 3+",
                  "shortName": "Otto Porter 3+"
               },
               "wasPrice": [],
               "id": 202631207,
               "name": "Otto Porter 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4.25",
                  "frac": "13/4",
                  "rootIdx": 145
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "Otto Porter 2+",
                  "shortName": "Otto Porter 2+"
               },
               "wasPrice": [],
               "id": 202631205,
               "name": "Otto Porter 2+",
               "type": "2",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.1",
                  "frac": "11/10",
                  "rootIdx": 93
               },
               "pos": {
                  "row": 101,
                  "col": 1
               },
               "names": {
                  "longName": "Otto Porter 1+",
                  "shortName": "Otto Porter 1+"
               },
               "wasPrice": [],
               "id": 202631203,
               "name": "Otto Porter 1+",
               "type": "1",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Otto Porter blocks (incl. overtime)",
            "shortName": "Otto Porter blocks (incl. overtime)"
         },
         "id": 69761026,
         "name": "Otto Porter blocks (incl. overtime)",
         "type": "BASKETBALL:FT:PLBLOC",
         "subtype": "M#pre:playerprops:24750896:607522",
         "displayOrder": -269,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "6.2",
                  "frac": "26/5",
                  "rootIdx": 174
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Zach LaVine 3+",
                  "shortName": "Zach LaVine 3+"
               },
               "wasPrice": [],
               "id": 202519157,
               "name": "Zach LaVine 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 101,
                  "col": 1
               },
               "names": {
                  "longName": "Zach LaVine 1+",
                  "shortName": "Zach LaVine 1+"
               },
               "wasPrice": [],
               "id": 202519155,
               "name": "Zach LaVine 1+",
               "type": "1",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "21",
                  "frac": "20/1",
                  "rootIdx": 212
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Zach LaVine 4+",
                  "shortName": "Zach LaVine 4+"
               },
               "wasPrice": [],
               "id": 202519153,
               "name": "Zach LaVine 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.5",
                  "frac": "5/2",
                  "rootIdx": 130
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "Zach LaVine 2+",
                  "shortName": "Zach LaVine 2+"
               },
               "wasPrice": [],
               "id": 202519151,
               "name": "Zach LaVine 2+",
               "type": "2",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Zach LaVine blocks (incl. overtime)",
            "shortName": "Zach LaVine blocks (incl. overtime)"
         },
         "id": 69729712,
         "name": "Zach LaVine blocks (incl. overtime)",
         "type": "BASKETBALL:FT:PLBLOC",
         "subtype": "M#pre:playerprops:24750896:607400",
         "displayOrder": -269,
         "columnCount": 1,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "5.5",
                  "frac": "9/2",
                  "rootIdx": 166
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Patrick Williams 3+",
                  "shortName": "Patrick Williams 3+"
               },
               "wasPrice": [],
               "id": 202519145,
               "name": "Patrick Williams 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "12",
                  "frac": "11/1",
                  "rootIdx": 200
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Patrick Williams 4+",
                  "shortName": "Patrick Williams 4+"
               },
               "wasPrice": [],
               "id": 202519149,
               "name": "Patrick Williams 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "20",
                  "frac": "19/1",
                  "rootIdx": 211
               },
               "pos": {
                  "row": 202,
                  "col": 1
               },
               "names": {
                  "longName": "Patrick Williams 5+",
                  "shortName": "Patrick Williams 5+"
               },
               "wasPrice": [],
               "id": 202519147,
               "name": "Patrick Williams 5+",
               "type": "5",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Patrick Williams assists (incl. overtime)",
            "shortName": "Patrick Williams assists (incl. overtime)"
         },
         "id": 69729710,
         "name": "Patrick Williams assists (incl. overtime)",
         "type": "BASKETBALL:FT:PLASS",
         "subtype": "M#pre:playerprops:24750896:1797198",
         "displayOrder": -273,
         "columnCount": 1,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.28",
                  "frac": "2/7",
                  "rootIdx": 34
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks +5.5",
                  "shortName": "Dallas Mavericks +5.5"
               },
               "wasPrice": [],
               "id": 202330910,
               "name": "Dallas Mavericks +5.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.5",
                  "frac": "5/2",
                  "rootIdx": 130
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls -5.5",
                  "shortName": "Chicago Bulls -5.5"
               },
               "wasPrice": [],
               "id": 202330887,
               "name": "Chicago Bulls -5.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666760,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-5.5",
         "line": -5.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.3",
                  "frac": "3/10",
                  "rootIdx": 35
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 234.5",
                  "shortName": "Under 234.5"
               },
               "wasPrice": [],
               "id": 202470877,
               "name": "Under 234.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.3",
                  "frac": "23/10",
                  "rootIdx": 126
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 234.5",
                  "shortName": "Over 234.5"
               },
               "wasPrice": [],
               "id": 202470875,
               "name": "Over 234.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715101,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#234.5",
         "line": 234.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3.4",
                  "frac": "12/5",
                  "rootIdx": 128
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 211.5",
                  "shortName": "Under 211.5"
               },
               "wasPrice": [],
               "id": 202625315,
               "name": "Under 211.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.3",
                  "frac": "3/10",
                  "rootIdx": 35
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 211.5",
                  "shortName": "Over 211.5"
               },
               "wasPrice": [],
               "id": 202625313,
               "name": "Over 211.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69758676,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#211.5",
         "line": 211.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "6.7",
                  "frac": "57/10",
                  "rootIdx": 180
               },
               "names": {
                  "longName": "Coby White 30+",
                  "shortName": "Coby White 30+"
               },
               "wasPrice": [],
               "id": 202519219,
               "name": "Coby White 30+",
               "type": "30",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "5.3",
                  "frac": "43/10",
                  "rootIdx": 164
               },
               "names": {
                  "longName": "Coby White 28+",
                  "shortName": "Coby White 28+"
               },
               "wasPrice": [],
               "id": 202519217,
               "name": "Coby White 28+",
               "type": "28",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4.15",
                  "frac": "63/20",
                  "rootIdx": 143
               },
               "names": {
                  "longName": "Coby White 26+",
                  "shortName": "Coby White 26+"
               },
               "wasPrice": [],
               "id": 202519215,
               "name": "Coby White 26+",
               "type": "26",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.25",
                  "frac": "9/4",
                  "rootIdx": 125
               },
               "names": {
                  "longName": "Coby White 24+",
                  "shortName": "Coby White 24+"
               },
               "wasPrice": [],
               "id": 202519213,
               "name": "Coby White 24+",
               "type": "24",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.55",
                  "frac": "31/20",
                  "rootIdx": 109
               },
               "names": {
                  "longName": "Coby White 22+",
                  "shortName": "Coby White 22+"
               },
               "wasPrice": [],
               "id": 202519211,
               "name": "Coby White 22+",
               "type": "22",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.05",
                  "frac": "21/20",
                  "rootIdx": 88
               },
               "names": {
                  "longName": "Coby White 20+",
                  "shortName": "Coby White 20+"
               },
               "wasPrice": [],
               "id": 202519209,
               "name": "Coby White 20+",
               "type": "20",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "names": {
                  "longName": "Coby White 18+",
                  "shortName": "Coby White 18+"
               },
               "wasPrice": [],
               "id": 202519207,
               "name": "Coby White 18+",
               "type": "18",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.55",
                  "frac": "11/20",
                  "rootIdx": 51
               },
               "names": {
                  "longName": "Coby White 16+",
                  "shortName": "Coby White 16+"
               },
               "wasPrice": [],
               "id": 202519205,
               "name": "Coby White 16+",
               "type": "16",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "8.25",
                  "frac": "29/4",
                  "rootIdx": 189
               },
               "names": {
                  "longName": "Coby White 32+",
                  "shortName": "Coby White 32+"
               },
               "wasPrice": [],
               "id": 202519221,
               "name": "Coby White 32+",
               "type": "32",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Coby White points (incl. overtime)",
            "shortName": "Coby White points (incl. overtime)"
         },
         "id": 69729724,
         "name": "Coby White points (incl. overtime)",
         "type": "BASKETBALL:FT:PLPOI",
         "subtype": "M#pre:playerprops:24750896:1582800",
         "displayOrder": -275,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.85",
                  "frac": "17/20",
                  "rootIdx": 70
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 112.5",
                  "shortName": "Under 112.5"
               },
               "wasPrice": [],
               "id": 202628381,
               "name": "Under 112.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.85",
                  "frac": "17/20",
                  "rootIdx": 70
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 112.5",
                  "shortName": "Over 112.5"
               },
               "wasPrice": [],
               "id": 202628379,
               "name": "Over 112.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Half Total Points",
            "shortName": "1st Half Total Points"
         },
         "id": 69759838,
         "name": "1st Half Total Points",
         "type": "BASKETBALL:P:OU",
         "subtype": "H1#112.5",
         "line": 112.5,
         "displayOrder": -365,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "H1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.62",
                  "frac": "8/13",
                  "rootIdx": 54
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks",
                  "shortName": "Dallas Mavericks"
               },
               "wasPrice": [],
               "id": 202471105,
               "name": "Dallas Mavericks",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.15",
                  "frac": "23/20",
                  "rootIdx": 96
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls",
                  "shortName": "Chicago Bulls"
               },
               "wasPrice": [],
               "id": 202471103,
               "name": "Chicago Bulls",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Half Money Line",
            "shortName": "1st Half Money Line"
         },
         "id": 69715199,
         "name": "1st Half Money Line",
         "type": "BASKETBALL:P:DNB",
         "subtype": "H1",
         "displayOrder": -375,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "H1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.25",
                  "frac": "1/4",
                  "rootIdx": 32
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 235.5",
                  "shortName": "Under 235.5"
               },
               "wasPrice": [],
               "id": 202470845,
               "name": "Under 235.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.75",
                  "frac": "11/4",
                  "rootIdx": 135
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 235.5",
                  "shortName": "Over 235.5"
               },
               "wasPrice": [],
               "id": 202470843,
               "name": "Over 235.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715085,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#235.5",
         "line": 235.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "27",
                  "frac": "26/1",
                  "rootIdx": 218
               },
               "pos": {
                  "row": 302,
                  "col": 1
               },
               "names": {
                  "longName": "Otto Porter 6+",
                  "shortName": "Otto Porter 6+"
               },
               "wasPrice": [],
               "id": 202631221,
               "name": "Otto Porter 6+",
               "type": "6",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "15",
                  "frac": "14/1",
                  "rootIdx": 206
               },
               "pos": {
                  "row": 202,
                  "col": 1
               },
               "names": {
                  "longName": "Otto Porter 5+",
                  "shortName": "Otto Porter 5+"
               },
               "wasPrice": [],
               "id": 202631227,
               "name": "Otto Porter 5+",
               "type": "5",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "5.7",
                  "frac": "47/10",
                  "rootIdx": 168
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Otto Porter 4+",
                  "shortName": "Otto Porter 4+"
               },
               "wasPrice": [],
               "id": 202631225,
               "name": "Otto Porter 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.55",
                  "frac": "31/20",
                  "rootIdx": 109
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Otto Porter 3+",
                  "shortName": "Otto Porter 3+"
               },
               "wasPrice": [],
               "id": 202631223,
               "name": "Otto Porter 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.57",
                  "frac": "4/7",
                  "rootIdx": 52
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "Otto Porter 2+",
                  "shortName": "Otto Porter 2+"
               },
               "wasPrice": [],
               "id": 202631219,
               "name": "Otto Porter 2+",
               "type": "2",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Otto Porter 3-point field goals (incl. overtime)",
            "shortName": "Otto Porter 3-point field goals (incl. overtime)"
         },
         "id": 69761030,
         "name": "Otto Porter 3-point field goals (incl. overtime)",
         "type": "BASKETBALL:FT:3PFG",
         "subtype": "M#pre:playerprops:24750896:607522",
         "displayOrder": -272,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.78",
                  "frac": "39/50",
                  "rootIdx": 65
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Zach LaVine 3+",
                  "shortName": "Zach LaVine 3+"
               },
               "wasPrice": [],
               "id": 202519299,
               "name": "Zach LaVine 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.47",
                  "frac": "40/85",
                  "rootIdx": 45
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "Zach LaVine 2+",
                  "shortName": "Zach LaVine 2+"
               },
               "wasPrice": [],
               "id": 202519297,
               "name": "Zach LaVine 2+",
               "type": "2",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "14",
                  "frac": "13/1",
                  "rootIdx": 204
               },
               "pos": {
                  "row": 103,
                  "col": 1
               },
               "names": {
                  "longName": "Zach LaVine 7+",
                  "shortName": "Zach LaVine 7+"
               },
               "wasPrice": [],
               "id": 202519307,
               "name": "Zach LaVine 7+",
               "type": "7",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "7.5",
                  "frac": "13/2",
                  "rootIdx": 186
               },
               "pos": {
                  "row": 302,
                  "col": 1
               },
               "names": {
                  "longName": "Zach LaVine 6+",
                  "shortName": "Zach LaVine 6+"
               },
               "wasPrice": [],
               "id": 202519305,
               "name": "Zach LaVine 6+",
               "type": "6",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4",
                  "frac": "3/1",
                  "rootIdx": 140
               },
               "pos": {
                  "row": 202,
                  "col": 1
               },
               "names": {
                  "longName": "Zach LaVine 5+",
                  "shortName": "Zach LaVine 5+"
               },
               "wasPrice": [],
               "id": 202519303,
               "name": "Zach LaVine 5+",
               "type": "5",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.45",
                  "frac": "29/20",
                  "rootIdx": 107
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Zach LaVine 4+",
                  "shortName": "Zach LaVine 4+"
               },
               "wasPrice": [],
               "id": 202519301,
               "name": "Zach LaVine 4+",
               "type": "4",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Zach LaVine 3-point field goals (incl. overtime)",
            "shortName": "Zach LaVine 3-point field goals (incl. overtime)"
         },
         "id": 69729740,
         "name": "Zach LaVine 3-point field goals (incl. overtime)",
         "type": "BASKETBALL:FT:3PFG",
         "subtype": "M#pre:playerprops:24750896:607400",
         "displayOrder": -272,
         "columnCount": 1,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 114.5",
                  "shortName": "Under 114.5"
               },
               "wasPrice": [],
               "id": 202471117,
               "name": "Under 114.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 114.5",
                  "shortName": "Over 114.5"
               },
               "wasPrice": [],
               "id": 202471115,
               "name": "Over 114.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Dallas Mavericks Total Points (Incl. OT)",
            "shortName": "Dallas Mavericks Total Points (Incl. OT)"
         },
         "id": 69715205,
         "name": "Dallas Mavericks Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:B:OU",
         "subtype": "M#114.5",
         "line": 114.5,
         "displayOrder": -440,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.67",
                  "frac": "4/6",
                  "rootIdx": 57
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks",
                  "shortName": "Dallas Mavericks"
               },
               "wasPrice": [],
               "id": 202471207,
               "name": "Dallas Mavericks",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.1",
                  "frac": "11/10",
                  "rootIdx": 93
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls",
                  "shortName": "Chicago Bulls"
               },
               "wasPrice": [],
               "id": 202471205,
               "name": "Chicago Bulls",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "2nd Half Money Line (Excl. OT)",
            "shortName": "2nd Half Money Line (Excl. OT)"
         },
         "id": 69715249,
         "name": "2nd Half Money Line (Excl. OT)",
         "type": "BASKETBALL:P:DNB",
         "subtype": "H2",
         "displayOrder": -345,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "H2"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 223.5",
                  "shortName": "Under 223.5"
               },
               "wasPrice": [],
               "id": 202471085,
               "name": "Under 223.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 223.5",
                  "shortName": "Over 223.5"
               },
               "wasPrice": [],
               "id": 202471083,
               "name": "Over 223.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715189,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#223.5",
         "line": 223.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.75",
                  "frac": "7/4",
                  "rootIdx": 114
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks & Over 224.5",
                  "shortName": "Dallas Mavericks & Over 224.5"
               },
               "wasPrice": [],
               "id": 202625309,
               "name": "Dallas Mavericks & Over 224.5",
               "type": "BOver",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.8",
                  "frac": "9/5",
                  "rootIdx": 115
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Dallas Mavericks & Under 224.5",
                  "shortName": "Dallas Mavericks & Under 224.5"
               },
               "wasPrice": [],
               "id": 202625311,
               "name": "Dallas Mavericks & Under 224.5",
               "type": "BUnder",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "5",
                  "frac": "4/1",
                  "rootIdx": 160
               },
               "pos": {
                  "row": 2,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls & Under 224.5",
                  "shortName": "Chicago Bulls & Under 224.5"
               },
               "wasPrice": [],
               "id": 202625307,
               "name": "Chicago Bulls & Under 224.5",
               "type": "AUnder",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "5",
                  "frac": "4/1",
                  "rootIdx": 160
               },
               "pos": {
                  "row": 2,
                  "col": 2
               },
               "names": {
                  "longName": "Chicago Bulls & Over 224.5",
                  "shortName": "Chicago Bulls & Over 224.5"
               },
               "wasPrice": [],
               "id": 202625305,
               "name": "Chicago Bulls & Over 224.5",
               "type": "AOver",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Matchbet and Total Points (Incl. OT)",
            "shortName": "Matchbet and Total Points (Incl. OT)"
         },
         "id": 69758674,
         "name": "Matchbet and Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:AXB$OU",
         "subtype": "M#224.5",
         "line": 224.5,
         "displayOrder": -425,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.75",
                  "frac": "3/4",
                  "rootIdx": 63
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 57.5",
                  "shortName": "Under 57.5"
               },
               "wasPrice": [],
               "id": 202471873,
               "name": "Under 57.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 57.5",
                  "shortName": "Over 57.5"
               },
               "wasPrice": [],
               "id": 202471871,
               "name": "Over 57.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Quarter Total Points",
            "shortName": "1st Quarter Total Points"
         },
         "id": 69715519,
         "name": "1st Quarter Total Points",
         "type": "BASKETBALL:P:OU",
         "subtype": "Q1#57.5",
         "line": 57.5,
         "displayOrder": -323,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "4.5",
                  "frac": "7/2",
                  "rootIdx": 151
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -14.5",
                  "shortName": "Dallas Mavericks -14.5"
               },
               "wasPrice": [],
               "id": 202330930,
               "name": "Dallas Mavericks -14.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.18",
                  "frac": "2/11",
                  "rootIdx": 26
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +14.5",
                  "shortName": "Chicago Bulls +14.5"
               },
               "wasPrice": [],
               "id": 202330921,
               "name": "Chicago Bulls +14.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666783,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#14.5",
         "line": 14.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.44",
                  "frac": "4/9",
                  "rootIdx": 43
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +8.5",
                  "shortName": "Chicago Bulls +8.5"
               },
               "wasPrice": [],
               "id": 202331018,
               "name": "Chicago Bulls +8.5",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.7",
                  "frac": "17/10",
                  "rootIdx": 113
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -8.5",
                  "shortName": "Dallas Mavericks -8.5"
               },
               "wasPrice": [],
               "id": 202331029,
               "name": "Dallas Mavericks -8.5",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666832,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#8.5",
         "line": 8.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.62",
                  "frac": "8/13",
                  "rootIdx": 54
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 28.5",
                  "shortName": "Under 28.5"
               },
               "wasPrice": [],
               "id": 202470983,
               "name": "Under 28.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.15",
                  "frac": "23/20",
                  "rootIdx": 96
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 28.5",
                  "shortName": "Over 28.5"
               },
               "wasPrice": [],
               "id": 202470981,
               "name": "Over 28.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Quarter Chicago Bulls Total Points",
            "shortName": "1st Quarter Chicago Bulls Total Points"
         },
         "id": 69715143,
         "name": "1st Quarter Chicago Bulls Total Points",
         "type": "BASKETBALL:P:A:OU",
         "subtype": "Q1#28.5",
         "line": 28.5,
         "displayOrder": -318,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.72",
                  "frac": "8/11",
                  "rootIdx": 60
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 224.5",
                  "shortName": "Under 224.5"
               },
               "wasPrice": [],
               "id": 202471161,
               "name": "Under 224.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.05",
                  "frac": "21/20",
                  "rootIdx": 88
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 224.5",
                  "shortName": "Over 224.5"
               },
               "wasPrice": [],
               "id": 202471159,
               "name": "Over 224.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715227,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#224.5",
         "line": 224.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3.9",
                  "frac": "29/10",
                  "rootIdx": 138
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls -6.5",
                  "shortName": "Chicago Bulls -6.5"
               },
               "wasPrice": [],
               "id": 202330854,
               "name": "Chicago Bulls -6.5",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.22",
                  "frac": "2/9",
                  "rootIdx": 30
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks +6.5",
                  "shortName": "Dallas Mavericks +6.5"
               },
               "wasPrice": [],
               "id": 202330872,
               "name": "Dallas Mavericks +6.5",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666754,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-6.5",
         "line": -6.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.22",
                  "frac": "2/9",
                  "rootIdx": 30
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 209.5",
                  "shortName": "Over 209.5"
               },
               "wasPrice": [],
               "id": 202637441,
               "name": "Over 209.5",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.9",
                  "frac": "29/10",
                  "rootIdx": 138
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 209.5",
                  "shortName": "Under 209.5"
               },
               "wasPrice": [],
               "id": 202637439,
               "name": "Under 209.5",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69763848,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#209.5",
         "line": 209.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "71",
                  "frac": "70/1",
                  "rootIdx": 246
               },
               "pos": {
                  "row": 2,
                  "col": 2
               },
               "names": {
                  "longName": "Tie/Tie",
                  "shortName": "Tie/Tie"
               },
               "wasPrice": [],
               "id": 202471307,
               "name": "Tie/Tie",
               "type": "X/X",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "21",
                  "frac": "20/1",
                  "rootIdx": 212
               },
               "pos": {
                  "row": 2,
                  "col": 3
               },
               "names": {
                  "longName": "Tie/Dallas Mavericks",
                  "shortName": "Tie/Dallas Mavericks"
               },
               "wasPrice": [],
               "id": 202471305,
               "name": "Tie/Dallas Mavericks",
               "type": "X/B",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "28",
                  "frac": "27/1",
                  "rootIdx": 219
               },
               "pos": {
                  "row": 2,
                  "col": 1
               },
               "names": {
                  "longName": "Tie/Chicago Bulls",
                  "shortName": "Tie/Chicago Bulls"
               },
               "wasPrice": [],
               "id": 202471303,
               "name": "Tie/Chicago Bulls",
               "type": "X/A",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "18",
                  "frac": "17/1",
                  "rootIdx": 209
               },
               "pos": {
                  "row": 3,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks/Draw",
                  "shortName": "Dallas Mavericks/Draw"
               },
               "wasPrice": [],
               "id": 202471301,
               "name": "Dallas Mavericks/Draw",
               "type": "B/X",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.05",
                  "frac": "21/20",
                  "rootIdx": 88
               },
               "pos": {
                  "row": 3,
                  "col": 3
               },
               "names": {
                  "longName": "Dallas Mavericks/Dallas Mavericks",
                  "shortName": "Dallas Mavericks/Dallas Mavericks"
               },
               "wasPrice": [],
               "id": 202471299,
               "name": "Dallas Mavericks/Dallas Mavericks",
               "type": "B/B",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "8.75",
                  "frac": "31/4",
                  "rootIdx": 191
               },
               "pos": {
                  "row": 3,
                  "col": 1
               },
               "names": {
                  "longName": "Dallas Mavericks/Chicago Bulls",
                  "shortName": "Dallas Mavericks/Chicago Bulls"
               },
               "wasPrice": [],
               "id": 202471297,
               "name": "Dallas Mavericks/Chicago Bulls",
               "type": "B/A",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "18",
                  "frac": "17/1",
                  "rootIdx": 209
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Chicago Bulls/Draw",
                  "shortName": "Chicago Bulls/Draw"
               },
               "wasPrice": [],
               "id": 202471295,
               "name": "Chicago Bulls/Draw",
               "type": "A/X",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "6.5",
                  "frac": "11/2",
                  "rootIdx": 178
               },
               "pos": {
                  "row": 1,
                  "col": 3
               },
               "names": {
                  "longName": "Chicago Bulls/Dallas Mavericks",
                  "shortName": "Chicago Bulls/Dallas Mavericks"
               },
               "wasPrice": [],
               "id": 202471293,
               "name": "Chicago Bulls/Dallas Mavericks",
               "type": "A/B",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.3",
                  "frac": "23/10",
                  "rootIdx": 126
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls/Chicago Bulls",
                  "shortName": "Chicago Bulls/Chicago Bulls"
               },
               "wasPrice": [],
               "id": 202471291,
               "name": "Chicago Bulls/Chicago Bulls",
               "type": "A/A",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Halftime/Fulltime",
            "shortName": "Halftime/Fulltime"
         },
         "id": 69715287,
         "name": "Halftime/Fulltime",
         "type": "BASKETBALL:FT:HTFT",
         "subtype": "M",
         "displayOrder": -420,
         "columnCount": 3,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3.15",
                  "frac": "43/20",
                  "rootIdx": 123
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 212.5",
                  "shortName": "Under 212.5"
               },
               "wasPrice": [],
               "id": 202470933,
               "name": "Under 212.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.33",
                  "frac": "1/3",
                  "rootIdx": 37
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 212.5",
                  "shortName": "Over 212.5"
               },
               "wasPrice": [],
               "id": 202470931,
               "name": "Over 212.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715119,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#212.5",
         "line": 212.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "26",
                  "frac": "25/1",
                  "rootIdx": 217
               },
               "pos": {
                  "row": 3,
                  "col": 1
               },
               "names": {
                  "longName": "31 plus",
                  "shortName": "31 plus"
               },
               "wasPrice": [],
               "id": 202471239,
               "name": "31 plus",
               "type": "31 plus",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "19",
                  "frac": "18/1",
                  "rootIdx": 210
               },
               "pos": {
                  "row": 2,
                  "col": 3
               },
               "names": {
                  "longName": "26 to 30",
                  "shortName": "26 to 30"
               },
               "wasPrice": [],
               "id": 202471237,
               "name": "26 to 30",
               "type": "26 to 30",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "11",
                  "frac": "10/1",
                  "rootIdx": 198
               },
               "pos": {
                  "row": 2,
                  "col": 2
               },
               "names": {
                  "longName": "21 to 25",
                  "shortName": "21 to 25"
               },
               "wasPrice": [],
               "id": 202471235,
               "name": "21 to 25",
               "type": "21 to 25",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "6.75",
                  "frac": "23/4",
                  "rootIdx": 181
               },
               "pos": {
                  "row": 2,
                  "col": 1
               },
               "names": {
                  "longName": "16 to 20",
                  "shortName": "16 to 20"
               },
               "wasPrice": [],
               "id": 202471231,
               "name": "16 to 20",
               "type": "16 to 20",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3",
                  "frac": "2/1",
                  "rootIdx": 120
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "1 to 5",
                  "shortName": "1 to 5"
               },
               "wasPrice": [],
               "id": 202471233,
               "name": "1 to 5",
               "type": "1 to 5",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "4.85",
                  "frac": "77/20",
                  "rootIdx": 158
               },
               "pos": {
                  "row": 1,
                  "col": 3
               },
               "names": {
                  "longName": "11 to 15",
                  "shortName": "11 to 15"
               },
               "wasPrice": [],
               "id": 202471229,
               "name": "11 to 15",
               "type": "11 to 15",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3",
                  "frac": "2/1",
                  "rootIdx": 120
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "6 to 10",
                  "shortName": "6 to 10"
               },
               "wasPrice": [],
               "id": 202471241,
               "name": "6 to 10",
               "type": "6 to 10",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Any team winning margin (incl. OT)",
            "shortName": "Any team winning margin (incl. OT)"
         },
         "id": 69715261,
         "name": "Any team winning margin (incl. OT)",
         "type": "BASKETBALL:FTOT:ATWM",
         "subtype": "M#sr:winning_margin_no_draw_any_team:31+",
         "displayOrder": -399,
         "columnCount": 3,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.72",
                  "frac": "8/11",
                  "rootIdx": 60
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -0.5",
                  "shortName": "Dallas Mavericks -0.5"
               },
               "wasPrice": [],
               "id": 202734805,
               "name": "Dallas Mavericks -0.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2",
                  "frac": "1/1",
                  "rootIdx": 83
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +0.5",
                  "shortName": "Chicago Bulls +0.5"
               },
               "wasPrice": [],
               "id": 202734803,
               "name": "Chicago Bulls +0.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Quarter Spread",
            "shortName": "1st Quarter Spread"
         },
         "id": 69799318,
         "name": "1st Quarter Spread",
         "type": "BASKETBALL:P:PSPRD",
         "subtype": "Q1#0.5",
         "line": 0.5,
         "displayOrder": -324,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 113.5",
                  "shortName": "Over 113.5"
               },
               "wasPrice": [],
               "id": 202632193,
               "name": "Over 113.5",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 113.5",
                  "shortName": "Under 113.5"
               },
               "wasPrice": [],
               "id": 202632195,
               "name": "Under 113.5",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Dallas Mavericks Total Points (Incl. OT)",
            "shortName": "Dallas Mavericks Total Points (Incl. OT)"
         },
         "id": 69761460,
         "name": "Dallas Mavericks Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:B:OU",
         "subtype": "M#113.5",
         "line": 113.5,
         "displayOrder": -440,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3",
                  "frac": "2/1",
                  "rootIdx": 120
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 213.5",
                  "shortName": "Under 213.5"
               },
               "wasPrice": [],
               "id": 202471219,
               "name": "Under 213.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.36",
                  "frac": "4/11",
                  "rootIdx": 39
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 213.5",
                  "shortName": "Over 213.5"
               },
               "wasPrice": [],
               "id": 202471217,
               "name": "Over 213.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715255,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#213.5",
         "line": 213.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.25",
                  "frac": "1/4",
                  "rootIdx": 32
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 236.5",
                  "shortName": "Under 236.5"
               },
               "wasPrice": [],
               "id": 202470917,
               "name": "Under 236.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.75",
                  "frac": "11/4",
                  "rootIdx": 135
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 236.5",
                  "shortName": "Over 236.5"
               },
               "wasPrice": [],
               "id": 202470915,
               "name": "Over 236.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715111,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#236.5",
         "line": 236.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "9.5",
                  "frac": "17/2",
                  "rootIdx": 194
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Yes",
                  "shortName": "Yes"
               },
               "wasPrice": [],
               "id": 202471069,
               "name": "Yes",
               "type": "Yes",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.04",
                  "frac": "1/25",
                  "rootIdx": 10
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "No",
                  "shortName": "No"
               },
               "wasPrice": [],
               "id": 202471067,
               "name": "No",
               "type": "No",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Overtime Yes/No",
            "shortName": "Overtime Yes/No"
         },
         "id": 69715183,
         "name": "Overtime Yes/No",
         "type": "BASKETBALL:FT:OTYN",
         "subtype": "M",
         "displayOrder": -395,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.65",
                  "frac": "13/20",
                  "rootIdx": 56
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 29.5",
                  "shortName": "Under 29.5"
               },
               "wasPrice": [],
               "id": 202471029,
               "name": "Under 29.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.1",
                  "frac": "11/10",
                  "rootIdx": 93
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 29.5",
                  "shortName": "Over 29.5"
               },
               "wasPrice": [],
               "id": 202471027,
               "name": "Over 29.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Quarter Dallas Mavericks Total Points",
            "shortName": "1st Quarter Dallas Mavericks Total Points"
         },
         "id": 69715163,
         "name": "1st Quarter Dallas Mavericks Total Points",
         "type": "BASKETBALL:P:B:OU",
         "subtype": "Q1#29.5",
         "line": 29.5,
         "displayOrder": -317,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2",
                  "frac": "1/1",
                  "rootIdx": 83
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 220.5",
                  "shortName": "Under 220.5"
               },
               "wasPrice": [],
               "id": 202631329,
               "name": "Under 220.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.75",
                  "frac": "3/4",
                  "rootIdx": 63
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 220.5",
                  "shortName": "Over 220.5"
               },
               "wasPrice": [],
               "id": 202631327,
               "name": "Over 220.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Total Points (Excl. OT)",
            "shortName": "Total Points (Excl. OT)"
         },
         "id": 69761066,
         "name": "Total Points (Excl. OT)",
         "type": "BASKETBALL:FT:OU",
         "subtype": "M#220.5",
         "line": 220.5,
         "displayOrder": -450,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "5",
                  "frac": "4/1",
                  "rootIdx": 160
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -15.5",
                  "shortName": "Dallas Mavericks -15.5"
               },
               "wasPrice": [],
               "id": 202330992,
               "name": "Dallas Mavericks -15.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.15",
                  "frac": "2/13",
                  "rootIdx": 23
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +15.5",
                  "shortName": "Chicago Bulls +15.5"
               },
               "wasPrice": [],
               "id": 202330984,
               "name": "Chicago Bulls +15.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666813,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#15.5",
         "line": 15.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.25",
                  "frac": "9/4",
                  "rootIdx": 125
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Zach LaVine 4+",
                  "shortName": "Zach LaVine 4+"
               },
               "wasPrice": [],
               "id": 202519133,
               "name": "Zach LaVine 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.01",
                  "frac": "1/100",
                  "rootIdx": 7
               },
               "pos": {
                  "row": 101,
                  "col": 1
               },
               "names": {
                  "longName": "Zach LaVine 1+",
                  "shortName": "Zach LaVine 1+"
               },
               "wasPrice": [],
               "id": 202519131,
               "name": "Zach LaVine 1+",
               "type": "1",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.53",
                  "frac": "8/15",
                  "rootIdx": 50
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "Zach LaVine 2+",
                  "shortName": "Zach LaVine 2+"
               },
               "wasPrice": [],
               "id": 202519129,
               "name": "Zach LaVine 2+",
               "type": "2",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.9",
                  "frac": "19/10",
                  "rootIdx": 118
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Zach LaVine 3+",
                  "shortName": "Zach LaVine 3+"
               },
               "wasPrice": [],
               "id": 202519135,
               "name": "Zach LaVine 3+",
               "type": "3",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Zach LaVine steals (incl. overtime)",
            "shortName": "Zach LaVine steals (incl. overtime)"
         },
         "id": 69729706,
         "name": "Zach LaVine steals (incl. overtime)",
         "type": "BASKETBALL:FT:PLSTA",
         "subtype": "M#pre:playerprops:24750896:607400",
         "displayOrder": -270,
         "columnCount": 1,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.33",
                  "frac": "1/3",
                  "rootIdx": 37
               },
               "pos": {
                  "row": 101,
                  "col": 1
               },
               "names": {
                  "longName": "Otto Porter 1+",
                  "shortName": "Otto Porter 1+"
               },
               "wasPrice": [],
               "id": 202631195,
               "name": "Otto Porter 1+",
               "type": "1",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "6.1",
                  "frac": "51/10",
                  "rootIdx": 173
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Otto Porter 4+",
                  "shortName": "Otto Porter 4+"
               },
               "wasPrice": [],
               "id": 202631201,
               "name": "Otto Porter 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4.75",
                  "frac": "15/4",
                  "rootIdx": 156
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Otto Porter 3+",
                  "shortName": "Otto Porter 3+"
               },
               "wasPrice": [],
               "id": 202631199,
               "name": "Otto Porter 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.2",
                  "frac": "6/5",
                  "rootIdx": 99
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "Otto Porter 2+",
                  "shortName": "Otto Porter 2+"
               },
               "wasPrice": [],
               "id": 202631197,
               "name": "Otto Porter 2+",
               "type": "2",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Otto Porter steals (incl. overtime)",
            "shortName": "Otto Porter steals (incl. overtime)"
         },
         "id": 69761024,
         "name": "Otto Porter steals (incl. overtime)",
         "type": "BASKETBALL:FT:PLSTA",
         "subtype": "M#pre:playerprops:24750896:607522",
         "displayOrder": -270,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.2",
                  "frac": "1/5",
                  "rootIdx": 28
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks +7.5",
                  "shortName": "Dallas Mavericks +7.5"
               },
               "wasPrice": [],
               "id": 202330961,
               "name": "Dallas Mavericks +7.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "4.2",
                  "frac": "16/5",
                  "rootIdx": 144
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls -7.5",
                  "shortName": "Chicago Bulls -7.5"
               },
               "wasPrice": [],
               "id": 202330952,
               "name": "Chicago Bulls -7.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666793,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-7.5",
         "line": -7.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.7",
                  "frac": "7/10",
                  "rootIdx": 59
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 225.5",
                  "shortName": "Under 225.5"
               },
               "wasPrice": [],
               "id": 202471093,
               "name": "Under 225.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.1",
                  "frac": "11/10",
                  "rootIdx": 93
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 225.5",
                  "shortName": "Over 225.5"
               },
               "wasPrice": [],
               "id": 202471091,
               "name": "Over 225.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715193,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#225.5",
         "line": 225.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2",
                  "frac": "1/1",
                  "rootIdx": 83
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -1.5",
                  "shortName": "Dallas Mavericks -1.5"
               },
               "wasPrice": [],
               "id": 202633277,
               "name": "Dallas Mavericks -1.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.72",
                  "frac": "8/11",
                  "rootIdx": 60
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +1.5",
                  "shortName": "Chicago Bulls +1.5"
               },
               "wasPrice": [],
               "id": 202633275,
               "name": "Chicago Bulls +1.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "2nd Quarter Spread",
            "shortName": "2nd Quarter Spread"
         },
         "id": 69761942,
         "name": "2nd Quarter Spread",
         "type": "BASKETBALL:P:PSPRD",
         "subtype": "Q2#1.5",
         "line": 1.5,
         "displayOrder": -314,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q2"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.75",
                  "frac": "3/4",
                  "rootIdx": 63
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 57.5",
                  "shortName": "Under 57.5"
               },
               "wasPrice": [],
               "id": 202471865,
               "name": "Under 57.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 57.5",
                  "shortName": "Over 57.5"
               },
               "wasPrice": [],
               "id": 202471863,
               "name": "Over 57.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "2nd Quarter Total Points",
            "shortName": "2nd Quarter Total Points"
         },
         "id": 69715515,
         "name": "2nd Quarter Total Points",
         "type": "BASKETBALL:P:OU",
         "subtype": "Q2#57.5",
         "line": 57.5,
         "displayOrder": -313,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q2"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +1.5",
                  "shortName": "Chicago Bulls +1.5"
               },
               "wasPrice": [],
               "id": 202731609,
               "name": "Chicago Bulls +1.5",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -1.5",
                  "shortName": "Dallas Mavericks -1.5"
               },
               "wasPrice": [],
               "id": 202731607,
               "name": "Dallas Mavericks -1.5",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Half Spread",
            "shortName": "1st Half Spread"
         },
         "id": 69798276,
         "name": "1st Half Spread",
         "type": "BASKETBALL:P:AHCP",
         "subtype": "H1#1.5",
         "line": 1.5,
         "displayOrder": -370,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "H1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 54.5",
                  "shortName": "Under 54.5"
               },
               "wasPrice": [],
               "id": 202637465,
               "name": "Under 54.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.75",
                  "frac": "3/4",
                  "rootIdx": 63
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 54.5",
                  "shortName": "Over 54.5"
               },
               "wasPrice": [],
               "id": 202637463,
               "name": "Over 54.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "3rd Quarter Total Points",
            "shortName": "3rd Quarter Total Points"
         },
         "id": 69763858,
         "name": "3rd Quarter Total Points",
         "type": "BASKETBALL:P:OU",
         "subtype": "Q3#54.5",
         "line": 54.5,
         "displayOrder": -303,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q3"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.7",
                  "frac": "7/10",
                  "rootIdx": 59
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks",
                  "shortName": "Dallas Mavericks"
               },
               "wasPrice": [],
               "id": 202471335,
               "name": "Dallas Mavericks",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.05",
                  "frac": "21/20",
                  "rootIdx": 88
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls",
                  "shortName": "Chicago Bulls"
               },
               "wasPrice": [],
               "id": 202471333,
               "name": "Chicago Bulls",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Race to 20 Points",
            "shortName": "Race to 20 Points"
         },
         "id": 69715291,
         "name": "Race to 20 Points",
         "type": "BASKETBALL:FTOT:RX",
         "subtype": "M#20",
         "line": 20.0,
         "displayOrder": -283,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.05",
                  "frac": "21/20",
                  "rootIdx": 88
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -5.5",
                  "shortName": "Dallas Mavericks -5.5"
               },
               "wasPrice": [],
               "id": 202331043,
               "name": "Dallas Mavericks -5.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.72",
                  "frac": "8/11",
                  "rootIdx": 60
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +5.5",
                  "shortName": "Chicago Bulls +5.5"
               },
               "wasPrice": [],
               "id": 202331039,
               "name": "Chicago Bulls +5.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666834,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#5.5",
         "line": 5.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "17",
                  "frac": "16/1",
                  "rootIdx": 208
               },
               "names": {
                  "longName": "Each team over 130.5 points",
                  "shortName": "Each team over 130.5 points"
               },
               "wasPrice": [],
               "id": 202514105,
               "name": "Each team over 130.5 points",
               "type": "Each team over 130.5 points",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "6.25",
                  "frac": "21/4",
                  "rootIdx": 175
               },
               "names": {
                  "longName": "Dallas Mavericks to Win and Each Team to Score Over 119.5 Points",
                  "shortName": "Dallas Mavericks to Win and Each Team to Score Over 119.5 Points"
               },
               "wasPrice": [],
               "id": 202514103,
               "name": "Dallas Mavericks to Win and Each Team to Score Over 119.5 Points",
               "type": "Dallas Mavericks to Win and Each Team to Score Over 119.5 Points",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "17",
                  "frac": "16/1",
                  "rootIdx": 208
               },
               "names": {
                  "longName": "Dallas Mavericks over 30.5 points in each quarter",
                  "shortName": "Dallas Mavericks over 30.5 points in each quarter"
               },
               "wasPrice": [],
               "id": 202514079,
               "name": "Dallas Mavericks over 30.5 points in each quarter",
               "type": "Dallas Mavericks over 30.5 points in each quarter",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.7",
                  "frac": "7/10",
                  "rootIdx": 59
               },
               "names": {
                  "longName": "Dallas Mavericks to Win and Each Team to Score Over 99.5 Points",
                  "shortName": "Dallas Mavericks to Win and Each Team to Score Over 99.5 Points"
               },
               "wasPrice": [],
               "id": 202514101,
               "name": "Dallas Mavericks to Win and Each Team to Score Over 99.5 Points",
               "type": "Dallas Mavericks to Win and Each Team to Score Over 99.5 Points",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "9.25",
                  "frac": "33/4",
                  "rootIdx": 193
               },
               "names": {
                  "longName": "Dallas Mavericks To Win the first Three Quarters and lose the Fourth",
                  "shortName": "Dallas Mavericks To Win the first Three Quarters and lose the Fourth"
               },
               "wasPrice": [],
               "id": 202514077,
               "name": "Dallas Mavericks To Win the first Three Quarters and lose the Fourth",
               "type": "Dallas Mavericks To Win the first Three Quarters and lose the Fourth",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.13",
                  "frac": "2/15",
                  "rootIdx": 20
               },
               "names": {
                  "longName": "Dallas Mavericks over 20.5 points in each quarter",
                  "shortName": "Dallas Mavericks over 20.5 points in each quarter"
               },
               "wasPrice": [],
               "id": 202514075,
               "name": "Dallas Mavericks over 20.5 points in each quarter",
               "type": "Dallas Mavericks over 20.5 points in each quarter",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.05",
                  "frac": "21/20",
                  "rootIdx": 88
               },
               "names": {
                  "longName": "Each team over 22.5 points in each quarter",
                  "shortName": "Each team over 22.5 points in each quarter"
               },
               "wasPrice": [],
               "id": 202514099,
               "name": "Each team over 22.5 points in each quarter",
               "type": "Each team over 22.5 points in each quarter",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "11.5",
                  "frac": "21/2",
                  "rootIdx": 199
               },
               "names": {
                  "longName": "Each team over 25.5 points in each quarter",
                  "shortName": "Each team over 25.5 points in each quarter"
               },
               "wasPrice": [],
               "id": 202514097,
               "name": "Each team over 25.5 points in each quarter",
               "type": "Each team over 25.5 points in each quarter",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "13",
                  "frac": "12/1",
                  "rootIdx": 202
               },
               "names": {
                  "longName": "Chicago Bulls to Win and Each Team to Score Over 119.5 Points",
                  "shortName": "Chicago Bulls to Win and Each Team to Score Over 119.5 Points"
               },
               "wasPrice": [],
               "id": 202514071,
               "name": "Chicago Bulls to Win and Each Team to Score Over 119.5 Points",
               "type": "Chicago Bulls to Win and Each Team to Score Over 119.5 Points",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.12",
                  "frac": "1/8",
                  "rootIdx": 19
               },
               "names": {
                  "longName": "Each team over 100.5 points",
                  "shortName": "Each team over 100.5 points"
               },
               "wasPrice": [],
               "id": 202514095,
               "name": "Each team over 100.5 points",
               "type": "Each team over 100.5 points",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "-",
                  "frac": "-",
                  "rootIdx": -1
               },
               "names": {
                  "longName": "Otto Porter Jr. to Record a Double Double",
                  "shortName": "Otto Porter Jr. to Record a Double Double"
               },
               "wasPrice": [],
               "id": 202717827,
               "name": "Otto Porter Jr. to Record a Double Double",
               "type": "Otto Porter Jr. to Record a Double Double",
               "suspended": true,
               "displayed": false
            },
            {
               "odds": {
                  "dec": "2.1",
                  "frac": "11/10",
                  "rootIdx": 93
               },
               "names": {
                  "longName": "Each team over 110.5 points",
                  "shortName": "Each team over 110.5 points"
               },
               "wasPrice": [],
               "id": 202514093,
               "name": "Each team over 110.5 points",
               "type": "Each team over 110.5 points",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "-",
                  "frac": "-",
                  "rootIdx": -1
               },
               "names": {
                  "longName": "Zach LaVine Over 24.5 Points and Chicago Bulls to Win",
                  "shortName": "Zach LaVine Over 24.5 Points and Chicago Bulls to Win"
               },
               "wasPrice": [],
               "id": 202717825,
               "name": "Zach LaVine Over 24.5 Points and Chicago Bulls to Win",
               "type": "Zach LaVine Over 24.5 Points and Chicago Bulls to Win",
               "suspended": true,
               "displayed": false
            },
            {
               "odds": {
                  "dec": "35",
                  "frac": "34/1",
                  "rootIdx": 226
               },
               "names": {
                  "longName": "Chicago Bulls over 30.5 points in each quarter",
                  "shortName": "Chicago Bulls over 30.5 points in each quarter"
               },
               "wasPrice": [],
               "id": 202514091,
               "name": "Chicago Bulls over 30.5 points in each quarter",
               "type": "Chicago Bulls over 30.5 points in each quarter",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.95",
                  "frac": "39/20",
                  "rootIdx": 119
               },
               "names": {
                  "longName": "Chicago Bulls to Win and Each Team to Score Over 99.5 Points",
                  "shortName": "Chicago Bulls to Win and Each Team to Score Over 99.5 Points"
               },
               "wasPrice": [],
               "id": 202514069,
               "name": "Chicago Bulls to Win and Each Team to Score Over 99.5 Points",
               "type": "Chicago Bulls to Win and Each Team to Score Over 99.5 Points",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "7.5",
                  "frac": "13/2",
                  "rootIdx": 186
               },
               "names": {
                  "longName": "Dallas Mavericks to win all 4 quarters",
                  "shortName": "Dallas Mavericks to win all 4 quarters"
               },
               "wasPrice": [],
               "id": 202514067,
               "name": "Dallas Mavericks to win all 4 quarters",
               "type": "Dallas Mavericks to win all 4 quarters",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.7",
                  "frac": "17/10",
                  "rootIdx": 113
               },
               "names": {
                  "longName": "Dallas Mavericks to win 1st and 2nd quarter",
                  "shortName": "Dallas Mavericks to win 1st and 2nd quarter"
               },
               "wasPrice": [],
               "id": 202514065,
               "name": "Dallas Mavericks to win 1st and 2nd quarter",
               "type": "Dallas Mavericks to win 1st and 2nd quarter",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "16",
                  "frac": "15/1",
                  "rootIdx": 207
               },
               "names": {
                  "longName": "Chicago Bulls To Win the first Three Quarters and lose the Fourth",
                  "shortName": "Chicago Bulls To Win the first Three Quarters and lose the Fourth"
               },
               "wasPrice": [],
               "id": 202514089,
               "name": "Chicago Bulls To Win the first Three Quarters and lose the Fourth",
               "type": "Chicago Bulls To Win the first Three Quarters and lose the Fourth",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.2",
                  "frac": "6/5",
                  "rootIdx": 99
               },
               "names": {
                  "longName": "Each quarter over 50.5 points",
                  "shortName": "Each quarter over 50.5 points"
               },
               "wasPrice": [],
               "id": 202514111,
               "name": "Each quarter over 50.5 points",
               "type": "Each quarter over 50.5 points",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.3",
                  "frac": "3/10",
                  "rootIdx": 35
               },
               "names": {
                  "longName": "Chicago Bulls over 20.5 points in each quarter",
                  "shortName": "Chicago Bulls over 20.5 points in each quarter"
               },
               "wasPrice": [],
               "id": 202514087,
               "name": "Chicago Bulls over 20.5 points in each quarter",
               "type": "Chicago Bulls over 20.5 points in each quarter",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.65",
                  "frac": "53/20",
                  "rootIdx": 133
               },
               "names": {
                  "longName": "Chicago Bulls over 25.5 points in each quarter",
                  "shortName": "Chicago Bulls over 25.5 points in each quarter"
               },
               "wasPrice": [],
               "id": 202514085,
               "name": "Chicago Bulls over 25.5 points in each quarter",
               "type": "Chicago Bulls over 25.5 points in each quarter",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "4.45",
                  "frac": "69/20",
                  "rootIdx": 150
               },
               "names": {
                  "longName": "Chicago Bulls to win 1st and 2nd quarter",
                  "shortName": "Chicago Bulls to win 1st and 2nd quarter"
               },
               "wasPrice": [],
               "id": 202514083,
               "name": "Chicago Bulls to win 1st and 2nd quarter",
               "type": "Chicago Bulls to win 1st and 2nd quarter",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "19",
                  "frac": "18/1",
                  "rootIdx": 210
               },
               "names": {
                  "longName": "Chicago Bulls to win all 4 quarters",
                  "shortName": "Chicago Bulls to win all 4 quarters"
               },
               "wasPrice": [],
               "id": 202514081,
               "name": "Chicago Bulls to win all 4 quarters",
               "type": "Chicago Bulls to win all 4 quarters",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "-",
                  "frac": "-",
                  "rootIdx": -1
               },
               "names": {
                  "longName": "Patrick Williams to Record a Double Double",
                  "shortName": "Patrick Williams to Record a Double Double"
               },
               "wasPrice": [],
               "id": 202717835,
               "name": "Patrick Williams to Record a Double Double",
               "type": "Patrick Williams to Record a Double Double",
               "suspended": true,
               "displayed": false
            },
            {
               "odds": {
                  "dec": "-",
                  "frac": "-",
                  "rootIdx": -1
               },
               "names": {
                  "longName": "Zach LaVine to Record a Double Double",
                  "shortName": "Zach LaVine to Record a Double Double"
               },
               "wasPrice": [],
               "id": 202717833,
               "name": "Zach LaVine to Record a Double Double",
               "type": "Zach LaVine to Record a Double Double",
               "suspended": true,
               "displayed": false
            },
            {
               "odds": {
                  "dec": "-",
                  "frac": "-",
                  "rootIdx": -1
               },
               "names": {
                  "longName": "Coby White Over 19.5 Points and Chicago Bulls to Win",
                  "shortName": "Coby White Over 19.5 Points and Chicago Bulls to Win"
               },
               "wasPrice": [],
               "id": 202717831,
               "name": "Coby White Over 19.5 Points and Chicago Bulls to Win",
               "type": "Coby White Over 19.5 Points and Chicago Bulls to Win",
               "suspended": true,
               "displayed": false
            },
            {
               "odds": {
                  "dec": "-",
                  "frac": "-",
                  "rootIdx": -1
               },
               "names": {
                  "longName": "Coby White to Record a Double Double",
                  "shortName": "Coby White to Record a Double Double"
               },
               "wasPrice": [],
               "id": 202717829,
               "name": "Coby White to Record a Double Double",
               "type": "Coby White to Record a Double Double",
               "suspended": true,
               "displayed": false
            },
            {
               "odds": {
                  "dec": "2.65",
                  "frac": "33/20",
                  "rootIdx": 112
               },
               "names": {
                  "longName": "Dallas Mavericks over 25.5 points in each quarter",
                  "shortName": "Dallas Mavericks over 25.5 points in each quarter"
               },
               "wasPrice": [],
               "id": 202514073,
               "name": "Dallas Mavericks over 25.5 points in each quarter",
               "type": "Dallas Mavericks over 25.5 points in each quarter",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "25",
                  "frac": "24/1",
                  "rootIdx": 216
               },
               "names": {
                  "longName": "Each quarter over 60.5 points",
                  "shortName": "Each quarter over 60.5 points"
               },
               "wasPrice": [],
               "id": 202514109,
               "name": "Each quarter over 60.5 points",
               "type": "Each quarter over 60.5 points",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "4.65",
                  "frac": "73/20",
                  "rootIdx": 154
               },
               "names": {
                  "longName": "Each team over 120.5 points",
                  "shortName": "Each team over 120.5 points"
               },
               "wasPrice": [],
               "id": 202514107,
               "name": "Each team over 120.5 points",
               "type": "Each team over 120.5 points",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "#CustomBet",
            "shortName": "",
            "veryshortName": ""
         },
         "id": 69727760,
         "name": "#CustomBet",
         "type": "BASKETBALL_RAB",
         "displayOrder": -488,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": ""
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 112.5",
                  "shortName": "Under 112.5"
               },
               "wasPrice": [],
               "id": 202751259,
               "name": "Under 112.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 112.5",
                  "shortName": "Over 112.5"
               },
               "wasPrice": [],
               "id": 202751257,
               "name": "Over 112.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Dallas Mavericks Total Points (Incl. OT)",
            "shortName": "Dallas Mavericks Total Points (Incl. OT)"
         },
         "id": 69804870,
         "name": "Dallas Mavericks Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:B:OU",
         "subtype": "M#112.5",
         "line": 112.5,
         "displayOrder": -440,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.25",
                  "frac": "1/4",
                  "rootIdx": 32
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 237.5",
                  "shortName": "Under 237.5"
               },
               "wasPrice": [],
               "id": 202471129,
               "name": "Under 237.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.75",
                  "frac": "11/4",
                  "rootIdx": 135
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 237.5",
                  "shortName": "Over 237.5"
               },
               "wasPrice": [],
               "id": 202471127,
               "name": "Over 237.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715211,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#237.5",
         "line": 237.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.85",
                  "frac": "37/20",
                  "rootIdx": 116
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 214.5",
                  "shortName": "Under 214.5"
               },
               "wasPrice": [],
               "id": 202470885,
               "name": "Under 214.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.4",
                  "frac": "2/5",
                  "rootIdx": 41
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 214.5",
                  "shortName": "Over 214.5"
               },
               "wasPrice": [],
               "id": 202470883,
               "name": "Over 214.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715105,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#214.5",
         "line": 214.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "18",
                  "frac": "17/1",
                  "rootIdx": 209
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Wendell Carter 4+",
                  "shortName": "Wendell Carter 4+"
               },
               "wasPrice": [],
               "id": 202519281,
               "name": "Wendell Carter 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "5.9",
                  "frac": "49/10",
                  "rootIdx": 171
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Wendell Carter 3+",
                  "shortName": "Wendell Carter 3+"
               },
               "wasPrice": [],
               "id": 202519279,
               "name": "Wendell Carter 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3",
                  "frac": "2/1",
                  "rootIdx": 120
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "Wendell Carter 2+",
                  "shortName": "Wendell Carter 2+"
               },
               "wasPrice": [],
               "id": 202519277,
               "name": "Wendell Carter 2+",
               "type": "2",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.5",
                  "frac": "1/2",
                  "rootIdx": 47
               },
               "pos": {
                  "row": 101,
                  "col": 1
               },
               "names": {
                  "longName": "Wendell Carter 1+",
                  "shortName": "Wendell Carter 1+"
               },
               "wasPrice": [],
               "id": 202519275,
               "name": "Wendell Carter 1+",
               "type": "1",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Wendell Carter blocks (incl. overtime)",
            "shortName": "Wendell Carter blocks (incl. overtime)"
         },
         "id": 69729736,
         "name": "Wendell Carter blocks (incl. overtime)",
         "type": "BASKETBALL:FT:PLBLOC",
         "subtype": "M#pre:playerprops:24750896:1304564",
         "displayOrder": -269,
         "columnCount": 1,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3",
                  "frac": "2/1",
                  "rootIdx": 120
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Neither Team -5.5",
                  "shortName": "Neither Team -5.5"
               },
               "wasPrice": [],
               "id": 202471171,
               "name": "Neither Team -5.5",
               "type": "Draw",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.05",
                  "frac": "21/20",
                  "rootIdx": 88
               },
               "pos": {
                  "row": 1,
                  "col": 3
               },
               "names": {
                  "longName": "Dallas Mavericks -5.5",
                  "shortName": "Dallas Mavericks -5.5"
               },
               "wasPrice": [],
               "id": 202471169,
               "name": "Dallas Mavericks -5.5",
               "type": "2",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.65",
                  "frac": "53/20",
                  "rootIdx": 133
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls -5.5",
                  "shortName": "Chicago Bulls -5.5"
               },
               "wasPrice": [],
               "id": 202471167,
               "name": "Chicago Bulls -5.5",
               "type": "1",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Winning Margin 3-Way(Incl. OT)",
            "shortName": "Winning Margin 3-Way(Incl. OT)"
         },
         "id": 69715231,
         "name": "Winning Margin 3-Way(Incl. OT)",
         "type": "BASKETBALL:FTOT:WM",
         "subtype": "M#sr:winning_margin:6+",
         "displayOrder": -410,
         "columnCount": 3,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3.4",
                  "frac": "12/5",
                  "rootIdx": 128
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Neither Team -2.5",
                  "shortName": "Neither Team -2.5"
               },
               "wasPrice": [],
               "id": 202536955,
               "name": "Neither Team -2.5",
               "type": "other",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.3",
                  "frac": "13/10",
                  "rootIdx": 103
               },
               "pos": {
                  "row": 1,
                  "col": 3
               },
               "names": {
                  "longName": "Dallas Mavericks -2.5",
                  "shortName": "Dallas Mavericks -2.5"
               },
               "wasPrice": [],
               "id": 202536953,
               "name": "Dallas Mavericks -2.5",
               "type": "B>2",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.8",
                  "frac": "9/5",
                  "rootIdx": 115
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls -2.5",
                  "shortName": "Chicago Bulls -2.5"
               },
               "wasPrice": [],
               "id": 202536951,
               "name": "Chicago Bulls -2.5",
               "type": "A>2",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "2nd Quarter Winning Margin",
            "shortName": "2nd Quarter Winning Margin"
         },
         "id": 69737208,
         "name": "2nd Quarter Winning Margin",
         "type": "BASKETBALL:P:WM",
         "subtype": "Q2",
         "displayOrder": -311,
         "columnCount": 3,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q2"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.9",
                  "frac": "19/10",
                  "rootIdx": 118
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls -2.5",
                  "shortName": "Chicago Bulls -2.5"
               },
               "wasPrice": [],
               "id": 202536941,
               "name": "Chicago Bulls -2.5",
               "type": "A>2",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.4",
                  "frac": "12/5",
                  "rootIdx": 128
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Neither Team -2.5",
                  "shortName": "Neither Team -2.5"
               },
               "wasPrice": [],
               "id": 202536939,
               "name": "Neither Team -2.5",
               "type": "other",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.2",
                  "frac": "6/5",
                  "rootIdx": 99
               },
               "pos": {
                  "row": 1,
                  "col": 3
               },
               "names": {
                  "longName": "Dallas Mavericks -2.5",
                  "shortName": "Dallas Mavericks -2.5"
               },
               "wasPrice": [],
               "id": 202536943,
               "name": "Dallas Mavericks -2.5",
               "type": "B>2",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Quarter Winning Margin",
            "shortName": "1st Quarter Winning Margin"
         },
         "id": 69737204,
         "name": "1st Quarter Winning Margin",
         "type": "BASKETBALL:P:WM",
         "subtype": "Q1",
         "displayOrder": -321,
         "columnCount": 3,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3.35",
                  "frac": "47/20",
                  "rootIdx": 127
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Neither Team -2.5",
                  "shortName": "Neither Team -2.5"
               },
               "wasPrice": [],
               "id": 202536961,
               "name": "Neither Team -2.5",
               "type": "other",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.7",
                  "frac": "17/10",
                  "rootIdx": 113
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls -2.5",
                  "shortName": "Chicago Bulls -2.5"
               },
               "wasPrice": [],
               "id": 202536959,
               "name": "Chicago Bulls -2.5",
               "type": "A>2",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.38",
                  "frac": "11/8",
                  "rootIdx": 105
               },
               "pos": {
                  "row": 1,
                  "col": 3
               },
               "names": {
                  "longName": "Dallas Mavericks -2.5",
                  "shortName": "Dallas Mavericks -2.5"
               },
               "wasPrice": [],
               "id": 202536957,
               "name": "Dallas Mavericks -2.5",
               "type": "B>2",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "4th Quarter Winning Margin (Excl. OT)",
            "shortName": "4th Quarter Winning Margin (Excl. OT)"
         },
         "id": 69737210,
         "name": "4th Quarter Winning Margin (Excl. OT)",
         "type": "BASKETBALL:P:WM",
         "subtype": "Q4",
         "displayOrder": -291,
         "columnCount": 3,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q4"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3.4",
                  "frac": "12/5",
                  "rootIdx": 128
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Neither Team -2.5",
                  "shortName": "Neither Team -2.5"
               },
               "wasPrice": [],
               "id": 202536947,
               "name": "Neither Team -2.5",
               "type": "other",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.85",
                  "frac": "37/20",
                  "rootIdx": 116
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls -2.5",
                  "shortName": "Chicago Bulls -2.5"
               },
               "wasPrice": [],
               "id": 202536945,
               "name": "Chicago Bulls -2.5",
               "type": "A>2",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.25",
                  "frac": "5/4",
                  "rootIdx": 101
               },
               "pos": {
                  "row": 1,
                  "col": 3
               },
               "names": {
                  "longName": "Dallas Mavericks -2.5",
                  "shortName": "Dallas Mavericks -2.5"
               },
               "wasPrice": [],
               "id": 202536949,
               "name": "Dallas Mavericks -2.5",
               "type": "B>2",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "3rd Quarter Winning Margin",
            "shortName": "3rd Quarter Winning Margin"
         },
         "id": 69737206,
         "name": "3rd Quarter Winning Margin",
         "type": "BASKETBALL:P:WM",
         "subtype": "Q3",
         "displayOrder": -301,
         "columnCount": 3,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q3"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 59",
                  "shortName": "Under 59"
               },
               "wasPrice": [],
               "id": 202471041,
               "name": "Under 59",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 59",
                  "shortName": "Over 59"
               },
               "wasPrice": [],
               "id": 202471039,
               "name": "Over 59",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Half Dallas Mavericks Total Points",
            "shortName": "1st Half Dallas Mavericks Total Points"
         },
         "id": 69715169,
         "name": "1st Half Dallas Mavericks Total Points",
         "type": "BASKETBALL:P:B:OU",
         "subtype": "H1#59",
         "line": 59.0,
         "displayOrder": -350,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "H1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 57",
                  "shortName": "Over 57"
               },
               "wasPrice": [],
               "id": 202632509,
               "name": "Over 57",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 57",
                  "shortName": "Under 57"
               },
               "wasPrice": [],
               "id": 202632511,
               "name": "Under 57",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Half Dallas Mavericks Total Points",
            "shortName": "1st Half Dallas Mavericks Total Points"
         },
         "id": 69761604,
         "name": "1st Half Dallas Mavericks Total Points",
         "type": "BASKETBALL:P:B:OU",
         "subtype": "H1#57",
         "line": 57.0,
         "displayOrder": -350,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "H1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 58",
                  "shortName": "Under 58"
               },
               "wasPrice": [],
               "id": 202483501,
               "name": "Under 58",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 58",
                  "shortName": "Over 58"
               },
               "wasPrice": [],
               "id": 202483499,
               "name": "Over 58",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Half Dallas Mavericks Total Points",
            "shortName": "1st Half Dallas Mavericks Total Points"
         },
         "id": 69720349,
         "name": "1st Half Dallas Mavericks Total Points",
         "type": "BASKETBALL:P:B:OU",
         "subtype": "H1#58",
         "line": 58.0,
         "displayOrder": -350,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "H1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "5.75",
                  "frac": "19/4",
                  "rootIdx": 169
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -16.5",
                  "shortName": "Dallas Mavericks -16.5"
               },
               "wasPrice": [],
               "id": 202330967,
               "name": "Dallas Mavericks -16.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.12",
                  "frac": "1/8",
                  "rootIdx": 19
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +16.5",
                  "shortName": "Chicago Bulls +16.5"
               },
               "wasPrice": [],
               "id": 202330954,
               "name": "Chicago Bulls +16.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666801,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#16.5",
         "line": 16.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "12",
                  "frac": "11/1",
                  "rootIdx": 200
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Patrick Williams 3+",
                  "shortName": "Patrick Williams 3+"
               },
               "wasPrice": [],
               "id": 202519143,
               "name": "Patrick Williams 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4.3",
                  "frac": "33/10",
                  "rootIdx": 146
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "Patrick Williams 2+",
                  "shortName": "Patrick Williams 2+"
               },
               "wasPrice": [],
               "id": 202519139,
               "name": "Patrick Williams 2+",
               "type": "2",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "46",
                  "frac": "45/1",
                  "rootIdx": 237
               },
               "pos": {
                  "row": 202,
                  "col": 1
               },
               "names": {
                  "longName": "Patrick Williams 5+",
                  "shortName": "Patrick Williams 5+"
               },
               "wasPrice": [],
               "id": 202519137,
               "name": "Patrick Williams 5+",
               "type": "5",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "22",
                  "frac": "21/1",
                  "rootIdx": 213
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Patrick Williams 4+",
                  "shortName": "Patrick Williams 4+"
               },
               "wasPrice": [],
               "id": 202519141,
               "name": "Patrick Williams 4+",
               "type": "4",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Patrick Williams 3-point field goals (incl. overtime)",
            "shortName": "Patrick Williams 3-point field goals (incl. overtime)"
         },
         "id": 69729708,
         "name": "Patrick Williams 3-point field goals (incl. overtime)",
         "type": "BASKETBALL:FT:3PFG",
         "subtype": "M#pre:playerprops:24750896:1797198",
         "displayOrder": -272,
         "columnCount": 1,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.25",
                  "frac": "5/4",
                  "rootIdx": 101
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -6.5",
                  "shortName": "Dallas Mavericks -6.5"
               },
               "wasPrice": [],
               "id": 202330947,
               "name": "Dallas Mavericks -6.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.65",
                  "frac": "13/20",
                  "rootIdx": 56
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +6.5",
                  "shortName": "Chicago Bulls +6.5"
               },
               "wasPrice": [],
               "id": 202330937,
               "name": "Chicago Bulls +6.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666794,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#6.5",
         "line": 6.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "5.75",
                  "frac": "19/4",
                  "rootIdx": 169
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls -10.5",
                  "shortName": "Chicago Bulls -10.5"
               },
               "wasPrice": [],
               "id": 202741073,
               "name": "Chicago Bulls -10.5",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.12",
                  "frac": "1/8",
                  "rootIdx": 19
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks +10.5",
                  "shortName": "Dallas Mavericks +10.5"
               },
               "wasPrice": [],
               "id": 202741071,
               "name": "Dallas Mavericks +10.5",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69801490,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-10.5",
         "line": -10.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "4.8",
                  "frac": "19/5",
                  "rootIdx": 157
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls -8.5",
                  "shortName": "Chicago Bulls -8.5"
               },
               "wasPrice": [],
               "id": 202330933,
               "name": "Chicago Bulls -8.5",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.15",
                  "frac": "2/13",
                  "rootIdx": 23
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks +8.5",
                  "shortName": "Dallas Mavericks +8.5"
               },
               "wasPrice": [],
               "id": 202330945,
               "name": "Dallas Mavericks +8.5",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666788,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-8.5",
         "line": -8.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.65",
                  "frac": "13/20",
                  "rootIdx": 56
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 226.5",
                  "shortName": "Under 226.5"
               },
               "wasPrice": [],
               "id": 202471133,
               "name": "Under 226.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.2",
                  "frac": "6/5",
                  "rootIdx": 99
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 226.5",
                  "shortName": "Over 226.5"
               },
               "wasPrice": [],
               "id": 202471131,
               "name": "Over 226.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715213,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#226.5",
         "line": 226.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -0.5",
                  "shortName": "Dallas Mavericks -0.5"
               },
               "wasPrice": [],
               "id": 202470853,
               "name": "Dallas Mavericks -0.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +0.5",
                  "shortName": "Chicago Bulls +0.5"
               },
               "wasPrice": [],
               "id": 202470851,
               "name": "Chicago Bulls +0.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "2nd Quarter Spread",
            "shortName": "2nd Quarter Spread"
         },
         "id": 69715089,
         "name": "2nd Quarter Spread",
         "type": "BASKETBALL:P:PSPRD",
         "subtype": "Q2#0.5",
         "line": 0.5,
         "displayOrder": -314,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q2"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "Coby White 2+",
                  "shortName": "Coby White 2+"
               },
               "wasPrice": [],
               "id": 202519159,
               "name": "Coby White 2+",
               "type": "2",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4.25",
                  "frac": "13/4",
                  "rootIdx": 145
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Coby White 3+",
                  "shortName": "Coby White 3+"
               },
               "wasPrice": [],
               "id": 202519165,
               "name": "Coby White 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "5.4",
                  "frac": "22/5",
                  "rootIdx": 165
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Coby White 4+",
                  "shortName": "Coby White 4+"
               },
               "wasPrice": [],
               "id": 202519163,
               "name": "Coby White 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.22",
                  "frac": "2/9",
                  "rootIdx": 30
               },
               "pos": {
                  "row": 101,
                  "col": 1
               },
               "names": {
                  "longName": "Coby White 1+",
                  "shortName": "Coby White 1+"
               },
               "wasPrice": [],
               "id": 202519161,
               "name": "Coby White 1+",
               "type": "1",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Coby White steals (incl. overtime)",
            "shortName": "Coby White steals (incl. overtime)"
         },
         "id": 69729714,
         "name": "Coby White steals (incl. overtime)",
         "type": "BASKETBALL:FT:PLSTA",
         "subtype": "M#pre:playerprops:24750896:1582800",
         "displayOrder": -270,
         "columnCount": 1,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.85",
                  "frac": "17/20",
                  "rootIdx": 70
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -2.5",
                  "shortName": "Dallas Mavericks -2.5"
               },
               "wasPrice": [],
               "id": 202471339,
               "name": "Dallas Mavericks -2.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.85",
                  "frac": "17/20",
                  "rootIdx": 70
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +2.5",
                  "shortName": "Chicago Bulls +2.5"
               },
               "wasPrice": [],
               "id": 202471337,
               "name": "Chicago Bulls +2.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Half Spread",
            "shortName": "1st Half Spread"
         },
         "id": 69715293,
         "name": "1st Half Spread",
         "type": "BASKETBALL:P:AHCP",
         "subtype": "H1#2.5",
         "line": 2.5,
         "displayOrder": -370,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "H1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 56.5",
                  "shortName": "Under 56.5"
               },
               "wasPrice": [],
               "id": 202483473,
               "name": "Under 56.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 56.5",
                  "shortName": "Over 56.5"
               },
               "wasPrice": [],
               "id": 202483471,
               "name": "Over 56.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "2nd Quarter Total Points",
            "shortName": "2nd Quarter Total Points"
         },
         "id": 69720335,
         "name": "2nd Quarter Total Points",
         "type": "BASKETBALL:P:OU",
         "subtype": "Q2#56.5",
         "line": 56.5,
         "displayOrder": -313,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q2"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "5.6",
                  "frac": "23/5",
                  "rootIdx": 167
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Wendell Carter 3+",
                  "shortName": "Wendell Carter 3+"
               },
               "wasPrice": [],
               "id": 202519175,
               "name": "Wendell Carter 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "12",
                  "frac": "11/1",
                  "rootIdx": 200
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Wendell Carter 4+",
                  "shortName": "Wendell Carter 4+"
               },
               "wasPrice": [],
               "id": 202519173,
               "name": "Wendell Carter 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.67",
                  "frac": "4/6",
                  "rootIdx": 57
               },
               "pos": {
                  "row": 101,
                  "col": 1
               },
               "names": {
                  "longName": "Wendell Carter 1+",
                  "shortName": "Wendell Carter 1+"
               },
               "wasPrice": [],
               "id": 202519171,
               "name": "Wendell Carter 1+",
               "type": "1",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.2",
                  "frac": "11/5",
                  "rootIdx": 124
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "Wendell Carter 2+",
                  "shortName": "Wendell Carter 2+"
               },
               "wasPrice": [],
               "id": 202519177,
               "name": "Wendell Carter 2+",
               "type": "2",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Wendell Carter steals (incl. overtime)",
            "shortName": "Wendell Carter steals (incl. overtime)"
         },
         "id": 69729718,
         "name": "Wendell Carter steals (incl. overtime)",
         "type": "BASKETBALL:FT:PLSTA",
         "subtype": "M#pre:playerprops:24750896:1304564",
         "displayOrder": -270,
         "columnCount": 1,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "-",
                  "frac": "-",
                  "rootIdx": -1
               },
               "names": {
                  "longName": "Coby White to Record a Double Double",
                  "shortName": "Coby White to Record a Double Double"
               },
               "wasPrice": [
                  {
                     "channel": "INTERNET",
                     "decimalOdds": 9.0,
                     "fractionalOdds": "8/1"
                  },
                  {
                     "channel": "IT",
                     "decimalOdds": 9.0,
                     "fractionalOdds": "8/1"
                  },
                  {
                     "channel": "AR",
                     "decimalOdds": 9.0,
                     "fractionalOdds": "8/1"
                  },
                  {
                     "channel": "NJ",
                     "decimalOdds": 9.0,
                     "fractionalOdds": "8/1"
                  },
                  {
                     "channel": "PA",
                     "decimalOdds": 9.0,
                     "fractionalOdds": "8/1"
                  },
                  {
                     "channel": "CO",
                     "decimalOdds": 9.0,
                     "fractionalOdds": "8/1"
                  },
                  {
                     "channel": "DE",
                     "decimalOdds": 9.0,
                     "fractionalOdds": "8/1"
                  },
                  {
                     "channel": "ES",
                     "decimalOdds": 9.0,
                     "fractionalOdds": "8/1"
                  },
                  {
                     "channel": "GR",
                     "decimalOdds": 9.0,
                     "fractionalOdds": "8/1"
                  },
                  {
                     "channel": "MI",
                     "decimalOdds": 9.0,
                     "fractionalOdds": "8/1"
                  }
               ],
               "id": 202718053,
               "name": "Coby White to Record a Double Double",
               "type": "Coby White to Record a Double Double",
               "suspended": true,
               "displayed": false
            },
            {
               "odds": {
                  "dec": "8.5",
                  "frac": "15/2",
                  "rootIdx": 190
               },
               "names": {
                  "longName": "Dallas Mavericks to win all 4 quarters",
                  "shortName": "Dallas Mavericks to win all 4 quarters"
               },
               "wasPrice": [
                  {
                     "channel": "INTERNET",
                     "decimalOdds": 7.5,
                     "fractionalOdds": "13/2"
                  },
                  {
                     "channel": "IT",
                     "decimalOdds": 7.5,
                     "fractionalOdds": "13/2"
                  },
                  {
                     "channel": "AR",
                     "decimalOdds": 7.5,
                     "fractionalOdds": "13/2"
                  },
                  {
                     "channel": "NJ",
                     "decimalOdds": 7.5,
                     "fractionalOdds": "13/2"
                  },
                  {
                     "channel": "PA",
                     "decimalOdds": 7.5,
                     "fractionalOdds": "13/2"
                  },
                  {
                     "channel": "CO",
                     "decimalOdds": 7.5,
                     "fractionalOdds": "13/2"
                  },
                  {
                     "channel": "DE",
                     "decimalOdds": 7.5,
                     "fractionalOdds": "13/2"
                  },
                  {
                     "channel": "ES",
                     "decimalOdds": 7.5,
                     "fractionalOdds": "13/2"
                  },
                  {
                     "channel": "GR",
                     "decimalOdds": 7.5,
                     "fractionalOdds": "13/2"
                  },
                  {
                     "channel": "MI",
                     "decimalOdds": 7.5,
                     "fractionalOdds": "13/2"
                  }
               ],
               "id": 202514193,
               "name": "Dallas Mavericks to win all 4 quarters",
               "type": "Dallas Mavericks to win all 4 quarters",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "-",
                  "frac": "-",
                  "rootIdx": -1
               },
               "names": {
                  "longName": "Otto Porter Jr. to Record a Double Double",
                  "shortName": "Otto Porter Jr. to Record a Double Double"
               },
               "wasPrice": [
                  {
                     "channel": "INTERNET",
                     "decimalOdds": 6.0,
                     "fractionalOdds": "5/1"
                  },
                  {
                     "channel": "IT",
                     "decimalOdds": 6.0,
                     "fractionalOdds": "5/1"
                  },
                  {
                     "channel": "AR",
                     "decimalOdds": 6.0,
                     "fractionalOdds": "5/1"
                  },
                  {
                     "channel": "NJ",
                     "decimalOdds": 6.0,
                     "fractionalOdds": "5/1"
                  },
                  {
                     "channel": "PA",
                     "decimalOdds": 6.0,
                     "fractionalOdds": "5/1"
                  },
                  {
                     "channel": "CO",
                     "decimalOdds": 6.0,
                     "fractionalOdds": "5/1"
                  },
                  {
                     "channel": "DE",
                     "decimalOdds": 6.0,
                     "fractionalOdds": "5/1"
                  },
                  {
                     "channel": "ES",
                     "decimalOdds": 6.0,
                     "fractionalOdds": "5/1"
                  },
                  {
                     "channel": "GR",
                     "decimalOdds": 6.0,
                     "fractionalOdds": "5/1"
                  },
                  {
                     "channel": "MI",
                     "decimalOdds": 6.0,
                     "fractionalOdds": "5/1"
                  }
               ],
               "id": 202718055,
               "name": "Otto Porter Jr. to Record a Double Double",
               "type": "Otto Porter Jr. to Record a Double Double",
               "suspended": true,
               "displayed": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Bet Boost",
            "shortName": "",
            "veryshortName": ""
         },
         "id": 69727790,
         "name": "Bet Boost",
         "type": "BASKETBALL_SPEC",
         "displayOrder": -489,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": ""
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.65",
                  "frac": "33/20",
                  "rootIdx": 112
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 215.5",
                  "shortName": "Under 215.5"
               },
               "wasPrice": [],
               "id": 202471211,
               "name": "Under 215.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.44",
                  "frac": "4/9",
                  "rootIdx": 43
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 215.5",
                  "shortName": "Over 215.5"
               },
               "wasPrice": [],
               "id": 202471209,
               "name": "Over 215.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715251,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#215.5",
         "line": 215.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3.75",
                  "frac": "11/4",
                  "rootIdx": 135
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 238.5",
                  "shortName": "Over 238.5"
               },
               "wasPrice": [],
               "id": 202471247,
               "name": "Over 238.5",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.25",
                  "frac": "1/4",
                  "rootIdx": 32
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 238.5",
                  "shortName": "Under 238.5"
               },
               "wasPrice": [],
               "id": 202471249,
               "name": "Under 238.5",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715265,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#238.5",
         "line": 238.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "names": {
                  "longName": "{outcomeName}",
                  "shortName": "{outcomeName}"
               },
               "wasPrice": [],
               "id": 202651079,
               "name": "{outcomeName}",
               "type": "9",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "33",
                  "frac": "32/1",
                  "rootIdx": 224
               },
               "names": {
                  "longName": "Patrick Williams 24+",
                  "shortName": "Patrick Williams 24+"
               },
               "wasPrice": [],
               "id": 202519359,
               "name": "Patrick Williams 24+",
               "type": "24",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "12",
                  "frac": "11/1",
                  "rootIdx": 200
               },
               "names": {
                  "longName": "{outcomeName}",
                  "shortName": "{outcomeName}"
               },
               "wasPrice": [],
               "id": 202651077,
               "name": "{outcomeName}",
               "type": "21",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "22",
                  "frac": "21/1",
                  "rootIdx": 213
               },
               "names": {
                  "longName": "Patrick Williams 22+",
                  "shortName": "Patrick Williams 22+"
               },
               "wasPrice": [],
               "id": 202519357,
               "name": "Patrick Williams 22+",
               "type": "22",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "10",
                  "frac": "9/1",
                  "rootIdx": 196
               },
               "names": {
                  "longName": "Patrick Williams 20+",
                  "shortName": "Patrick Williams 20+"
               },
               "wasPrice": [],
               "id": 202519355,
               "name": "Patrick Williams 20+",
               "type": "20",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "5.7",
                  "frac": "47/10",
                  "rootIdx": 168
               },
               "names": {
                  "longName": "Patrick Williams 18+",
                  "shortName": "Patrick Williams 18+"
               },
               "wasPrice": [],
               "id": 202519353,
               "name": "Patrick Williams 18+",
               "type": "18",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.9",
                  "frac": "29/10",
                  "rootIdx": 138
               },
               "names": {
                  "longName": "Patrick Williams 16+",
                  "shortName": "Patrick Williams 16+"
               },
               "wasPrice": [],
               "id": 202519351,
               "name": "Patrick Williams 16+",
               "type": "16",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.3",
                  "frac": "23/10",
                  "rootIdx": 126
               },
               "names": {
                  "longName": "{outcomeName}",
                  "shortName": "{outcomeName}"
               },
               "wasPrice": [],
               "id": 202651087,
               "name": "{outcomeName}",
               "type": "15",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4.65",
                  "frac": "73/20",
                  "rootIdx": 154
               },
               "names": {
                  "longName": "{outcomeName}",
                  "shortName": "{outcomeName}"
               },
               "wasPrice": [],
               "id": 202651085,
               "name": "{outcomeName}",
               "type": "17",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "6.7",
                  "frac": "57/10",
                  "rootIdx": 180
               },
               "names": {
                  "longName": "{outcomeName}",
                  "shortName": "{outcomeName}"
               },
               "wasPrice": [],
               "id": 202651083,
               "name": "{outcomeName}",
               "type": "19",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.33",
                  "frac": "1/3",
                  "rootIdx": 37
               },
               "names": {
                  "longName": "{outcomeName}",
                  "shortName": "{outcomeName}"
               },
               "wasPrice": [],
               "id": 202651081,
               "name": "{outcomeName}",
               "type": "7",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.44",
                  "frac": "4/9",
                  "rootIdx": 43
               },
               "pos": {
                  "row": 203,
                  "col": 1
               },
               "names": {
                  "longName": "Patrick Williams 8+",
                  "shortName": "Patrick Williams 8+"
               },
               "wasPrice": [],
               "id": 202519361,
               "name": "Patrick Williams 8+",
               "type": "8",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.2",
                  "frac": "6/5",
                  "rootIdx": 99
               },
               "names": {
                  "longName": "Patrick Williams 12+",
                  "shortName": "Patrick Williams 12+"
               },
               "wasPrice": [],
               "id": 202519347,
               "name": "Patrick Williams 12+",
               "type": "12",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "28",
                  "frac": "27/1",
                  "rootIdx": 219
               },
               "names": {
                  "longName": "{outcomeName}",
                  "shortName": "{outcomeName}"
               },
               "wasPrice": [],
               "id": 202651093,
               "name": "{outcomeName}",
               "type": "23",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.45",
                  "frac": "29/20",
                  "rootIdx": 107
               },
               "names": {
                  "longName": "{outcomeName}",
                  "shortName": "{outcomeName}"
               },
               "wasPrice": [],
               "id": 202651089,
               "name": "{outcomeName}",
               "type": "13",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.15",
                  "frac": "23/20",
                  "rootIdx": 96
               },
               "names": {
                  "longName": "{outcomeName}",
                  "shortName": "{outcomeName}"
               },
               "wasPrice": [],
               "id": 202651091,
               "name": "{outcomeName}",
               "type": "11",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.8",
                  "frac": "9/5",
                  "rootIdx": 115
               },
               "names": {
                  "longName": "Patrick Williams 14+",
                  "shortName": "Patrick Williams 14+"
               },
               "wasPrice": [],
               "id": 202519349,
               "name": "Patrick Williams 14+",
               "type": "14",
               "suspended": true,
               "displayed": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "names": {
                  "longName": "Patrick Williams 10+",
                  "shortName": "Patrick Williams 10+"
               },
               "wasPrice": [],
               "id": 202519345,
               "name": "Patrick Williams 10+",
               "type": "10",
               "suspended": true,
               "displayed": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Patrick Williams points (incl. overtime)",
            "shortName": "Patrick Williams points (incl. overtime)"
         },
         "id": 69729750,
         "name": "Patrick Williams points (incl. overtime)",
         "type": "BASKETBALL:FT:PLPOI",
         "subtype": "M#pre:playerprops:24750896:1797198",
         "displayOrder": -275,
         "columnCount": 1,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.62",
                  "frac": "8/13",
                  "rootIdx": 54
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 27.5",
                  "shortName": "Over 27.5"
               },
               "wasPrice": [],
               "id": 202549659,
               "name": "Over 27.5",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.15",
                  "frac": "23/20",
                  "rootIdx": 96
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 27.5",
                  "shortName": "Under 27.5"
               },
               "wasPrice": [],
               "id": 202549661,
               "name": "Under 27.5",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Quarter Dallas Mavericks Total Points",
            "shortName": "1st Quarter Dallas Mavericks Total Points"
         },
         "id": 69741042,
         "name": "1st Quarter Dallas Mavericks Total Points",
         "type": "BASKETBALL:P:B:OU",
         "subtype": "Q1#27.5",
         "line": 27.5,
         "displayOrder": -317,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 115.5",
                  "shortName": "Over 115.5"
               },
               "wasPrice": [],
               "id": 202471119,
               "name": "Over 115.5",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 115.5",
                  "shortName": "Under 115.5"
               },
               "wasPrice": [],
               "id": 202471121,
               "name": "Under 115.5",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Half Total Points",
            "shortName": "1st Half Total Points"
         },
         "id": 69715207,
         "name": "1st Half Total Points",
         "type": "BASKETBALL:P:OU",
         "subtype": "H1#115.5",
         "line": 115.5,
         "displayOrder": -365,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "H1"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "18",
                  "frac": "17/1",
                  "rootIdx": 209
               },
               "names": {
                  "longName": "Wendell Carter 14+",
                  "shortName": "Wendell Carter 14+"
               },
               "wasPrice": [],
               "id": 202519231,
               "name": "Wendell Carter 14+",
               "type": "14",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "5.1",
                  "frac": "41/10",
                  "rootIdx": 161
               },
               "names": {
                  "longName": "Wendell Carter 12+",
                  "shortName": "Wendell Carter 12+"
               },
               "wasPrice": [],
               "id": 202519227,
               "name": "Wendell Carter 12+",
               "type": "12",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.3",
                  "frac": "23/10",
                  "rootIdx": 126
               },
               "names": {
                  "longName": "Wendell Carter 11+",
                  "shortName": "Wendell Carter 11+"
               },
               "wasPrice": [],
               "id": 202519225,
               "name": "Wendell Carter 11+",
               "type": "11",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.35",
                  "frac": "27/20",
                  "rootIdx": 104
               },
               "names": {
                  "longName": "Wendell Carter 10+",
                  "shortName": "Wendell Carter 10+"
               },
               "wasPrice": [],
               "id": 202519223,
               "name": "Wendell Carter 10+",
               "type": "10",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "8.5",
                  "frac": "15/2",
                  "rootIdx": 190
               },
               "names": {
                  "longName": "Wendell Carter 13+",
                  "shortName": "Wendell Carter 13+"
               },
               "wasPrice": [],
               "id": 202519229,
               "name": "Wendell Carter 13+",
               "type": "13",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "names": {
                  "longName": "Wendell Carter 9+",
                  "shortName": "Wendell Carter 9+"
               },
               "wasPrice": [],
               "id": 202519237,
               "name": "Wendell Carter 9+",
               "type": "9",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.53",
                  "frac": "8/15",
                  "rootIdx": 50
               },
               "pos": {
                  "row": 203,
                  "col": 1
               },
               "names": {
                  "longName": "Wendell Carter 8+",
                  "shortName": "Wendell Carter 8+"
               },
               "wasPrice": [],
               "id": 202519235,
               "name": "Wendell Carter 8+",
               "type": "8",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "29",
                  "frac": "28/1",
                  "rootIdx": 220
               },
               "names": {
                  "longName": "Wendell Carter 15+",
                  "shortName": "Wendell Carter 15+"
               },
               "wasPrice": [],
               "id": 202519233,
               "name": "Wendell Carter 15+",
               "type": "15",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Wendell Carter rebounds (incl. overtime)",
            "shortName": "Wendell Carter rebounds (incl. overtime)"
         },
         "id": 69729726,
         "name": "Wendell Carter rebounds (incl. overtime)",
         "type": "BASKETBALL:FT:PLREB",
         "subtype": "M#pre:playerprops:24750896:1304564",
         "displayOrder": -274,
         "columnCount": 1,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 57.5",
                  "shortName": "Over 57.5"
               },
               "wasPrice": [],
               "id": 202628375,
               "name": "Over 57.5",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 57.5",
                  "shortName": "Under 57.5"
               },
               "wasPrice": [],
               "id": 202628377,
               "name": "Under 57.5",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Half Dallas Mavericks Total Points",
            "shortName": "1st Half Dallas Mavericks Total Points"
         },
         "id": 69759836,
         "name": "1st Half Dallas Mavericks Total Points",
         "type": "BASKETBALL:P:B:OU",
         "subtype": "H1#57.5",
         "line": 57.5,
         "displayOrder": -350,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "H1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.11",
                  "frac": "1/9",
                  "rootIdx": 18
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks +11",
                  "shortName": "Dallas Mavericks +11"
               },
               "wasPrice": [],
               "id": 202741075,
               "name": "Dallas Mavericks +11",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "5.75",
                  "frac": "19/4",
                  "rootIdx": 169
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls -11",
                  "shortName": "Chicago Bulls -11"
               },
               "wasPrice": [],
               "id": 202741077,
               "name": "Chicago Bulls -11",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69801492,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-11",
         "line": -11.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.65",
                  "frac": "33/20",
                  "rootIdx": 112
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 230",
                  "shortName": "Over 230"
               },
               "wasPrice": [],
               "id": 202470919,
               "name": "Over 230",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.44",
                  "frac": "4/9",
                  "rootIdx": 43
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 230",
                  "shortName": "Under 230"
               },
               "wasPrice": [],
               "id": 202470921,
               "name": "Under 230",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715113,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#230",
         "line": 230.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.4",
                  "frac": "2/5",
                  "rootIdx": 41
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 231",
                  "shortName": "Under 231"
               },
               "wasPrice": [],
               "id": 202470955,
               "name": "Under 231",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.85",
                  "frac": "37/20",
                  "rootIdx": 116
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 231",
                  "shortName": "Over 231"
               },
               "wasPrice": [],
               "id": 202470953,
               "name": "Over 231",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715129,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#231",
         "line": 231.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.36",
                  "frac": "4/11",
                  "rootIdx": 39
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 232",
                  "shortName": "Under 232"
               },
               "wasPrice": [],
               "id": 202470963,
               "name": "Under 232",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3",
                  "frac": "2/1",
                  "rootIdx": 120
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 232",
                  "shortName": "Over 232"
               },
               "wasPrice": [],
               "id": 202470961,
               "name": "Over 232",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715133,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#232",
         "line": 232.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.33",
                  "frac": "1/3",
                  "rootIdx": 37
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 233",
                  "shortName": "Under 233"
               },
               "wasPrice": [],
               "id": 202470967,
               "name": "Under 233",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.15",
                  "frac": "43/20",
                  "rootIdx": 123
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 233",
                  "shortName": "Over 233"
               },
               "wasPrice": [],
               "id": 202470965,
               "name": "Over 233",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715135,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#233",
         "line": 233.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.3",
                  "frac": "3/10",
                  "rootIdx": 35
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 234",
                  "shortName": "Under 234"
               },
               "wasPrice": [],
               "id": 202470979,
               "name": "Under 234",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.3",
                  "frac": "23/10",
                  "rootIdx": 126
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 234",
                  "shortName": "Over 234"
               },
               "wasPrice": [],
               "id": 202470977,
               "name": "Over 234",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715141,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#234",
         "line": 234.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "30",
                  "frac": "29/1",
                  "rootIdx": 221
               },
               "pos": {
                  "row": 103,
                  "col": 1
               },
               "names": {
                  "longName": "Coby White 7+",
                  "shortName": "Coby White 7+"
               },
               "wasPrice": [],
               "id": 202519331,
               "name": "Coby White 7+",
               "type": "7",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "17",
                  "frac": "16/1",
                  "rootIdx": 208
               },
               "pos": {
                  "row": 302,
                  "col": 1
               },
               "names": {
                  "longName": "Coby White 6+",
                  "shortName": "Coby White 6+"
               },
               "wasPrice": [],
               "id": 202519329,
               "name": "Coby White 6+",
               "type": "6",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "6.9",
                  "frac": "59/10",
                  "rootIdx": 183
               },
               "pos": {
                  "row": 202,
                  "col": 1
               },
               "names": {
                  "longName": "Coby White 5+",
                  "shortName": "Coby White 5+"
               },
               "wasPrice": [],
               "id": 202519327,
               "name": "Coby White 5+",
               "type": "5",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.3",
                  "frac": "23/10",
                  "rootIdx": 126
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Coby White 4+",
                  "shortName": "Coby White 4+"
               },
               "wasPrice": [],
               "id": 202519325,
               "name": "Coby White 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.87",
                  "frac": "87/100",
                  "rootIdx": 71
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Coby White 3+",
                  "shortName": "Coby White 3+"
               },
               "wasPrice": [],
               "id": 202519323,
               "name": "Coby White 3+",
               "type": "3",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.42",
                  "frac": "21/50",
                  "rootIdx": 42
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "Coby White 2+",
                  "shortName": "Coby White 2+"
               },
               "wasPrice": [],
               "id": 202519321,
               "name": "Coby White 2+",
               "type": "2",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Coby White 3-point field goals (incl. overtime)",
            "shortName": "Coby White 3-point field goals (incl. overtime)"
         },
         "id": 69729744,
         "name": "Coby White 3-point field goals (incl. overtime)",
         "type": "BASKETBALL:FT:3PFG",
         "subtype": "M#pre:playerprops:24750896:1582800",
         "displayOrder": -272,
         "columnCount": 1,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.25",
                  "frac": "1/4",
                  "rootIdx": 32
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 235",
                  "shortName": "Under 235"
               },
               "wasPrice": [],
               "id": 202470987,
               "name": "Under 235",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.75",
                  "frac": "11/4",
                  "rootIdx": 135
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 235",
                  "shortName": "Over 235"
               },
               "wasPrice": [],
               "id": 202470985,
               "name": "Over 235",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715145,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#235",
         "line": 235.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3.75",
                  "frac": "11/4",
                  "rootIdx": 135
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 236",
                  "shortName": "Over 236"
               },
               "wasPrice": [],
               "id": 202471225,
               "name": "Over 236",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.25",
                  "frac": "1/4",
                  "rootIdx": 32
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 236",
                  "shortName": "Under 236"
               },
               "wasPrice": [],
               "id": 202471227,
               "name": "Under 236",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715259,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#236",
         "line": 236.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3.75",
                  "frac": "11/4",
                  "rootIdx": 135
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 237",
                  "shortName": "Over 237"
               },
               "wasPrice": [],
               "id": 202471259,
               "name": "Over 237",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.25",
                  "frac": "1/4",
                  "rootIdx": 32
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 237",
                  "shortName": "Under 237"
               },
               "wasPrice": [],
               "id": 202471261,
               "name": "Under 237",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715271,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#237",
         "line": 237.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.25",
                  "frac": "1/4",
                  "rootIdx": 32
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 238",
                  "shortName": "Under 238"
               },
               "wasPrice": [],
               "id": 202471343,
               "name": "Under 238",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.75",
                  "frac": "11/4",
                  "rootIdx": 135
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 238",
                  "shortName": "Over 238"
               },
               "wasPrice": [],
               "id": 202471341,
               "name": "Over 238",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715295,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#238",
         "line": 238.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.25",
                  "frac": "1/4",
                  "rootIdx": 32
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 239",
                  "shortName": "Under 239"
               },
               "wasPrice": [],
               "id": 202471109,
               "name": "Under 239",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.75",
                  "frac": "11/4",
                  "rootIdx": 135
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 239",
                  "shortName": "Over 239"
               },
               "wasPrice": [],
               "id": 202471107,
               "name": "Over 239",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715201,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#239",
         "line": 239.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +0.5",
                  "shortName": "Chicago Bulls +0.5"
               },
               "wasPrice": [],
               "id": 202731613,
               "name": "Chicago Bulls +0.5",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.75",
                  "frac": "3/4",
                  "rootIdx": 63
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -0.5",
                  "shortName": "Dallas Mavericks -0.5"
               },
               "wasPrice": [],
               "id": 202731611,
               "name": "Dallas Mavericks -0.5",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "3rd Quarter Spread",
            "shortName": "3rd Quarter Spread"
         },
         "id": 69798278,
         "name": "3rd Quarter Spread",
         "type": "BASKETBALL:P:PSPRD",
         "subtype": "Q3#0.5",
         "line": 0.5,
         "displayOrder": -304,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q3"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.36",
                  "frac": "4/11",
                  "rootIdx": 39
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +9",
                  "shortName": "Chicago Bulls +9"
               },
               "wasPrice": [],
               "id": 202330957,
               "name": "Chicago Bulls +9",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.9",
                  "frac": "19/10",
                  "rootIdx": 118
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -9",
                  "shortName": "Dallas Mavericks -9"
               },
               "wasPrice": [],
               "id": 202330968,
               "name": "Dallas Mavericks -9",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666806,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#9",
         "line": 9.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.55",
                  "frac": "31/20",
                  "rootIdx": 109
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls -1.5",
                  "shortName": "Chicago Bulls -1.5"
               },
               "wasPrice": [],
               "id": 202330839,
               "name": "Chicago Bulls -1.5",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.47",
                  "frac": "40/85",
                  "rootIdx": 45
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks +1.5",
                  "shortName": "Dallas Mavericks +1.5"
               },
               "wasPrice": [],
               "id": 202330858,
               "name": "Dallas Mavericks +1.5",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666743,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-1.5",
         "line": -1.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "20",
                  "frac": "19/1",
                  "rootIdx": 211
               },
               "pos": {
                  "row": 102,
                  "col": 1
               },
               "names": {
                  "longName": "Patrick Williams 4+",
                  "shortName": "Patrick Williams 4+"
               },
               "wasPrice": [],
               "id": 202519089,
               "name": "Patrick Williams 4+",
               "type": "4",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 101,
                  "col": 1
               },
               "names": {
                  "longName": "Patrick Williams 1+",
                  "shortName": "Patrick Williams 1+"
               },
               "wasPrice": [],
               "id": 202519095,
               "name": "Patrick Williams 1+",
               "type": "1",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.5",
                  "frac": "5/2",
                  "rootIdx": 130
               },
               "pos": {
                  "row": 201,
                  "col": 1
               },
               "names": {
                  "longName": "Patrick Williams 2+",
                  "shortName": "Patrick Williams 2+"
               },
               "wasPrice": [],
               "id": 202519093,
               "name": "Patrick Williams 2+",
               "type": "2",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "6.2",
                  "frac": "26/5",
                  "rootIdx": 174
               },
               "pos": {
                  "row": 301,
                  "col": 1
               },
               "names": {
                  "longName": "Patrick Williams 3+",
                  "shortName": "Patrick Williams 3+"
               },
               "wasPrice": [],
               "id": 202519091,
               "name": "Patrick Williams 3+",
               "type": "3",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Patrick Williams steals (incl. overtime)",
            "shortName": "Patrick Williams steals (incl. overtime)"
         },
         "id": 69729690,
         "name": "Patrick Williams steals (incl. overtime)",
         "type": "BASKETBALL:FT:PLSTA",
         "subtype": "M#pre:playerprops:24750896:1797198",
         "displayOrder": -270,
         "columnCount": 1,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.15",
                  "frac": "23/20",
                  "rootIdx": 96
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -6",
                  "shortName": "Dallas Mavericks -6"
               },
               "wasPrice": [],
               "id": 202330995,
               "name": "Dallas Mavericks -6",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.67",
                  "frac": "4/6",
                  "rootIdx": 57
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +6",
                  "shortName": "Chicago Bulls +6"
               },
               "wasPrice": [],
               "id": 202330986,
               "name": "Chicago Bulls +6",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666814,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#6",
         "line": 6.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2",
                  "frac": "1/1",
                  "rootIdx": 83
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -5",
                  "shortName": "Dallas Mavericks -5"
               },
               "wasPrice": [],
               "id": 202330993,
               "name": "Dallas Mavericks -5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.75",
                  "frac": "3/4",
                  "rootIdx": 63
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +5",
                  "shortName": "Chicago Bulls +5"
               },
               "wasPrice": [],
               "id": 202330983,
               "name": "Chicago Bulls +5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666809,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#5",
         "line": 5.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.55",
                  "frac": "31/20",
                  "rootIdx": 109
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -8",
                  "shortName": "Dallas Mavericks -8"
               },
               "wasPrice": [],
               "id": 202331031,
               "name": "Dallas Mavericks -8",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.47",
                  "frac": "40/85",
                  "rootIdx": 45
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +8",
                  "shortName": "Chicago Bulls +8"
               },
               "wasPrice": [],
               "id": 202331009,
               "name": "Chicago Bulls +8",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666825,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#8",
         "line": 8.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 56.5",
                  "shortName": "Over 56.5"
               },
               "wasPrice": [],
               "id": 202549671,
               "name": "Over 56.5",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 56.5",
                  "shortName": "Under 56.5"
               },
               "wasPrice": [],
               "id": 202549673,
               "name": "Under 56.5",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "3rd Quarter Total Points",
            "shortName": "3rd Quarter Total Points"
         },
         "id": 69741046,
         "name": "3rd Quarter Total Points",
         "type": "BASKETBALL:P:OU",
         "subtype": "Q3#56.5",
         "line": 56.5,
         "displayOrder": -303,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q3"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.35",
                  "frac": "27/20",
                  "rootIdx": 104
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -7",
                  "shortName": "Dallas Mavericks -7"
               },
               "wasPrice": [],
               "id": 202331026,
               "name": "Dallas Mavericks -7",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.55",
                  "frac": "11/20",
                  "rootIdx": 51
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +7",
                  "shortName": "Chicago Bulls +7"
               },
               "wasPrice": [],
               "id": 202331011,
               "name": "Chicago Bulls +7",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666817,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#7",
         "line": 7.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.45",
                  "frac": "29/20",
                  "rootIdx": 107
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls",
                  "shortName": "Chicago Bulls"
               },
               "wasPrice": [],
               "id": 202330960,
               "name": "Chicago Bulls",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.55",
                  "frac": "11/20",
                  "rootIdx": 51
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks",
                  "shortName": "Dallas Mavericks"
               },
               "wasPrice": [],
               "id": 202330970,
               "name": "Dallas Mavericks",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Money Line (Incl. OT)",
            "shortName": "Money Line (Incl. OT)"
         },
         "id": 69666802,
         "name": "Money Line (Incl. OT)",
         "type": "BASKETBALL:FTOT:ML",
         "subtype": "M",
         "displayOrder": -500,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.2",
                  "frac": "6/5",
                  "rootIdx": 99
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +2",
                  "shortName": "Chicago Bulls +2"
               },
               "wasPrice": [],
               "id": 202331032,
               "name": "Chicago Bulls +2",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.65",
                  "frac": "13/20",
                  "rootIdx": 56
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -2",
                  "shortName": "Dallas Mavericks -2"
               },
               "wasPrice": [],
               "id": 202331037,
               "name": "Dallas Mavericks -2",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666833,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#2",
         "line": 2.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.55",
                  "frac": "11/20",
                  "rootIdx": 51
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -1",
                  "shortName": "Dallas Mavericks -1"
               },
               "wasPrice": [],
               "id": 202330826,
               "name": "Dallas Mavericks -1",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.35",
                  "frac": "27/20",
                  "rootIdx": 104
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +1",
                  "shortName": "Chicago Bulls +1"
               },
               "wasPrice": [],
               "id": 202330820,
               "name": "Chicago Bulls +1",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666746,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#1",
         "line": 1.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.57",
                  "frac": "4/7",
                  "rootIdx": 52
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 227.5",
                  "shortName": "Under 227.5"
               },
               "wasPrice": [],
               "id": 202471149,
               "name": "Under 227.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.3",
                  "frac": "13/10",
                  "rootIdx": 103
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 227.5",
                  "shortName": "Over 227.5"
               },
               "wasPrice": [],
               "id": 202471147,
               "name": "Over 227.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715221,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#227.5",
         "line": 227.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -4",
                  "shortName": "Dallas Mavericks -4"
               },
               "wasPrice": [],
               "id": 202330925,
               "name": "Dallas Mavericks -4",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +4",
                  "shortName": "Chicago Bulls +4"
               },
               "wasPrice": [],
               "id": 202330917,
               "name": "Chicago Bulls +4",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666780,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#4",
         "line": 4.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.05",
                  "frac": "21/20",
                  "rootIdx": 88
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +3",
                  "shortName": "Chicago Bulls +3"
               },
               "wasPrice": [],
               "id": 202330951,
               "name": "Chicago Bulls +3",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.72",
                  "frac": "8/11",
                  "rootIdx": 60
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -3",
                  "shortName": "Dallas Mavericks -3"
               },
               "wasPrice": [],
               "id": 202330966,
               "name": "Dallas Mavericks -3",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666798,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#3",
         "line": 3.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "5.25",
                  "frac": "17/4",
                  "rootIdx": 163
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls -9.5",
                  "shortName": "Chicago Bulls -9.5"
               },
               "wasPrice": [],
               "id": 202330835,
               "name": "Chicago Bulls -9.5",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.14",
                  "frac": "1/7",
                  "rootIdx": 21
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks +9.5",
                  "shortName": "Dallas Mavericks +9.5"
               },
               "wasPrice": [],
               "id": 202330861,
               "name": "Dallas Mavericks +9.5",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666750,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-9.5",
         "line": -9.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.4",
                  "frac": "7/5",
                  "rootIdx": 106
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls 0",
                  "shortName": "Chicago Bulls 0"
               },
               "wasPrice": [],
               "id": 202330897,
               "name": "Chicago Bulls 0",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.53",
                  "frac": "8/15",
                  "rootIdx": 50
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks 0",
                  "shortName": "Dallas Mavericks 0"
               },
               "wasPrice": [],
               "id": 202330903,
               "name": "Dallas Mavericks 0",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666773,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#0",
         "line": 0.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "5.75",
                  "frac": "19/4",
                  "rootIdx": 169
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -17.5",
                  "shortName": "Dallas Mavericks -17.5"
               },
               "wasPrice": [],
               "id": 202331004,
               "name": "Dallas Mavericks -17.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.11",
                  "frac": "1/9",
                  "rootIdx": 18
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +17.5",
                  "shortName": "Chicago Bulls +17.5"
               },
               "wasPrice": [],
               "id": 202330996,
               "name": "Chicago Bulls +17.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666819,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#17.5",
         "line": 17.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.44",
                  "frac": "4/9",
                  "rootIdx": 43
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 230.5",
                  "shortName": "Under 230.5"
               },
               "wasPrice": [],
               "id": 202470991,
               "name": "Under 230.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.7",
                  "frac": "17/10",
                  "rootIdx": 113
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 230.5",
                  "shortName": "Over 230.5"
               },
               "wasPrice": [],
               "id": 202470989,
               "name": "Over 230.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715147,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#230.5",
         "line": 230.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.75",
                  "frac": "3/4",
                  "rootIdx": 63
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 55.5",
                  "shortName": "Over 55.5"
               },
               "wasPrice": [],
               "id": 202632479,
               "name": "Over 55.5",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 55.5",
                  "shortName": "Under 55.5"
               },
               "wasPrice": [],
               "id": 202632477,
               "name": "Under 55.5",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "2nd Quarter Total Points",
            "shortName": "2nd Quarter Total Points"
         },
         "id": 69761590,
         "name": "2nd Quarter Total Points",
         "type": "BASKETBALL:P:OU",
         "subtype": "Q2#55.5",
         "line": 55.5,
         "displayOrder": -313,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q2"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.75",
                  "frac": "3/4",
                  "rootIdx": 63
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -3.5",
                  "shortName": "Dallas Mavericks -3.5"
               },
               "wasPrice": [],
               "id": 202331027,
               "name": "Dallas Mavericks -3.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2",
                  "frac": "1/1",
                  "rootIdx": 83
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +3.5",
                  "shortName": "Chicago Bulls +3.5"
               },
               "wasPrice": [],
               "id": 202331016,
               "name": "Chicago Bulls +3.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666831,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#3.5",
         "line": 3.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 109.5",
                  "shortName": "Over 109.5"
               },
               "wasPrice": [],
               "id": 202632503,
               "name": "Over 109.5",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 109.5",
                  "shortName": "Under 109.5"
               },
               "wasPrice": [],
               "id": 202632501,
               "name": "Under 109.5",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Chicago Bulls Total Points (Incl. OT)",
            "shortName": "Chicago Bulls Total Points (Incl. OT)"
         },
         "id": 69761600,
         "name": "Chicago Bulls Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:A:OU",
         "subtype": "M#109.5",
         "line": 109.5,
         "displayOrder": -445,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 54.5",
                  "shortName": "Over 54.5"
               },
               "wasPrice": [],
               "id": 202632505,
               "name": "Over 54.5",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 54.5",
                  "shortName": "Under 54.5"
               },
               "wasPrice": [],
               "id": 202632507,
               "name": "Under 54.5",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Half Chicago Bulls Total Points",
            "shortName": "1st Half Chicago Bulls Total Points"
         },
         "id": 69761602,
         "name": "1st Half Chicago Bulls Total Points",
         "type": "BASKETBALL:P:A:OU",
         "subtype": "H1#54.5",
         "line": 54.5,
         "displayOrder": -355,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "H1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.22",
                  "frac": "2/9",
                  "rootIdx": 30
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 209",
                  "shortName": "Over 209"
               },
               "wasPrice": [],
               "id": 202637445,
               "name": "Over 209",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.9",
                  "frac": "29/10",
                  "rootIdx": 138
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 209",
                  "shortName": "Under 209"
               },
               "wasPrice": [],
               "id": 202637443,
               "name": "Under 209",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69763850,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#209",
         "line": 209.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.5",
                  "frac": "1/2",
                  "rootIdx": 47
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks +1",
                  "shortName": "Dallas Mavericks +1"
               },
               "wasPrice": [],
               "id": 202330893,
               "name": "Dallas Mavericks +1",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.55",
                  "frac": "31/20",
                  "rootIdx": 109
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls -1",
                  "shortName": "Chicago Bulls -1"
               },
               "wasPrice": [],
               "id": 202330891,
               "name": "Chicago Bulls -1",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666761,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-1",
         "line": -1.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.55",
                  "frac": "31/20",
                  "rootIdx": 109
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 216.5",
                  "shortName": "Under 216.5"
               },
               "wasPrice": [],
               "id": 202471141,
               "name": "Under 216.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.47",
                  "frac": "40/85",
                  "rootIdx": 45
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 216.5",
                  "shortName": "Over 216.5"
               },
               "wasPrice": [],
               "id": 202471139,
               "name": "Over 216.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715217,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#216.5",
         "line": 216.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 58.5",
                  "shortName": "Under 58.5"
               },
               "wasPrice": [],
               "id": 202474017,
               "name": "Under 58.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 58.5",
                  "shortName": "Over 58.5"
               },
               "wasPrice": [],
               "id": 202474015,
               "name": "Over 58.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Half Dallas Mavericks Total Points",
            "shortName": "1st Half Dallas Mavericks Total Points"
         },
         "id": 69716449,
         "name": "1st Half Dallas Mavericks Total Points",
         "type": "BASKETBALL:P:B:OU",
         "subtype": "H1#58.5",
         "line": 58.5,
         "displayOrder": -350,
         "columnCount": 2,
         "suspended": true,
         "displayed": false,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "H1"
      },
      {
         "selection": [
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.47",
                  "frac": "40/85",
                  "rootIdx": 45
               },
               "names": {
                  "longName": "Wendell Carter 10+",
                  "shortName": "Wendell Carter 10+"
               },
               "wasPrice": [],
               "id": 202519243,
               "name": "Wendell Carter 10+",
               "type": "10",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "19",
                  "frac": "18/1",
                  "rootIdx": 210
               },
               "names": {
                  "longName": "Wendell Carter 26+",
                  "shortName": "Wendell Carter 26+"
               },
               "wasPrice": [],
               "id": 202519259,
               "name": "Wendell Carter 26+",
               "type": "26",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "10.5",
                  "frac": "19/2",
                  "rootIdx": 197
               },
               "names": {
                  "longName": "Wendell Carter 24+",
                  "shortName": "Wendell Carter 24+"
               },
               "wasPrice": [],
               "id": 202519257,
               "name": "Wendell Carter 24+",
               "type": "24",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "8",
                  "frac": "7/1",
                  "rootIdx": 188
               },
               "names": {
                  "longName": "Wendell Carter 22+",
                  "shortName": "Wendell Carter 22+"
               },
               "wasPrice": [],
               "id": 202519255,
               "name": "Wendell Carter 22+",
               "type": "22",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "6",
                  "frac": "5/1",
                  "rootIdx": 172
               },
               "names": {
                  "longName": "Wendell Carter 20+",
                  "shortName": "Wendell Carter 20+"
               },
               "wasPrice": [],
               "id": 202519253,
               "name": "Wendell Carter 20+",
               "type": "20",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "4.25",
                  "frac": "13/4",
                  "rootIdx": 145
               },
               "names": {
                  "longName": "Wendell Carter 18+",
                  "shortName": "Wendell Carter 18+"
               },
               "wasPrice": [],
               "id": 202519251,
               "name": "Wendell Carter 18+",
               "type": "18",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "2.25",
                  "frac": "5/4",
                  "rootIdx": 101
               },
               "names": {
                  "longName": "Wendell Carter 14+",
                  "shortName": "Wendell Carter 14+"
               },
               "wasPrice": [],
               "id": 202519247,
               "name": "Wendell Carter 14+",
               "type": "14",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "3.05",
                  "frac": "41/20",
                  "rootIdx": 121
               },
               "names": {
                  "longName": "Wendell Carter 16+",
                  "shortName": "Wendell Carter 16+"
               },
               "wasPrice": [],
               "id": 202519249,
               "name": "Wendell Carter 16+",
               "type": "16",
               "suspended": false
            },
            {
               "attributes": {
                  "attrib": [
                     {
                        "value": "HOME",
                        "key": "playerTeam"
                     }
                  ]
               },
               "odds": {
                  "dec": "1.87",
                  "frac": "87/100",
                  "rootIdx": 71
               },
               "names": {
                  "longName": "Wendell Carter 12+",
                  "shortName": "Wendell Carter 12+"
               },
               "wasPrice": [],
               "id": 202519245,
               "name": "Wendell Carter 12+",
               "type": "12",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Wendell Carter points (incl. overtime)",
            "shortName": "Wendell Carter points (incl. overtime)"
         },
         "id": 69729730,
         "name": "Wendell Carter points (incl. overtime)",
         "type": "BASKETBALL:FT:PLPOI",
         "subtype": "M#pre:playerprops:24750896:1304564",
         "displayOrder": -275,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.85",
                  "frac": "17/20",
                  "rootIdx": 70
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 28.5",
                  "shortName": "Under 28.5"
               },
               "wasPrice": [],
               "id": 202471045,
               "name": "Under 28.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.85",
                  "frac": "17/20",
                  "rootIdx": 70
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 28.5",
                  "shortName": "Over 28.5"
               },
               "wasPrice": [],
               "id": 202471043,
               "name": "Over 28.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Quarter Dallas Mavericks Total Points",
            "shortName": "1st Quarter Dallas Mavericks Total Points"
         },
         "id": 69715171,
         "name": "1st Quarter Dallas Mavericks Total Points",
         "type": "BASKETBALL:P:B:OU",
         "subtype": "Q1#28.5",
         "line": 28.5,
         "displayOrder": -317,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.25",
                  "frac": "1/4",
                  "rootIdx": 32
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 210",
                  "shortName": "Over 210"
               },
               "wasPrice": [],
               "id": 202637459,
               "name": "Over 210",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.75",
                  "frac": "11/4",
                  "rootIdx": 135
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 210",
                  "shortName": "Under 210"
               },
               "wasPrice": [],
               "id": 202637461,
               "name": "Under 210",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69763856,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#210",
         "line": 210.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3.75",
                  "frac": "11/4",
                  "rootIdx": 135
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 211",
                  "shortName": "Under 211"
               },
               "wasPrice": [],
               "id": 202625303,
               "name": "Under 211",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.25",
                  "frac": "1/4",
                  "rootIdx": 32
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 211",
                  "shortName": "Over 211"
               },
               "wasPrice": [],
               "id": 202625301,
               "name": "Over 211",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69758672,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#211",
         "line": 211.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.2",
                  "frac": "1/5",
                  "rootIdx": 28
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks +7",
                  "shortName": "Dallas Mavericks +7"
               },
               "wasPrice": [],
               "id": 202330830,
               "name": "Dallas Mavericks +7",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "4.2",
                  "frac": "16/5",
                  "rootIdx": 144
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls -7",
                  "shortName": "Chicago Bulls -7"
               },
               "wasPrice": [],
               "id": 202330821,
               "name": "Chicago Bulls -7",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666748,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-7",
         "line": -7.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "3.3",
                  "frac": "23/10",
                  "rootIdx": 126
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 212",
                  "shortName": "Under 212"
               },
               "wasPrice": [],
               "id": 202471101,
               "name": "Under 212",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.3",
                  "frac": "3/10",
                  "rootIdx": 35
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 212",
                  "shortName": "Over 212"
               },
               "wasPrice": [],
               "id": 202471099,
               "name": "Over 212",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715197,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#212",
         "line": 212.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.25",
                  "frac": "1/4",
                  "rootIdx": 32
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks +6",
                  "shortName": "Dallas Mavericks +6"
               },
               "wasPrice": [],
               "id": 202330862,
               "name": "Dallas Mavericks +6",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.75",
                  "frac": "11/4",
                  "rootIdx": 135
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls -6",
                  "shortName": "Chicago Bulls -6"
               },
               "wasPrice": [],
               "id": 202330838,
               "name": "Chicago Bulls -6",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666745,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-6",
         "line": -6.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.33",
                  "frac": "1/3",
                  "rootIdx": 37
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 213",
                  "shortName": "Over 213"
               },
               "wasPrice": [],
               "id": 202471031,
               "name": "Over 213",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.15",
                  "frac": "43/20",
                  "rootIdx": 123
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 213",
                  "shortName": "Under 213"
               },
               "wasPrice": [],
               "id": 202471033,
               "name": "Under 213",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715165,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#213",
         "line": 213.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.36",
                  "frac": "4/11",
                  "rootIdx": 39
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 214",
                  "shortName": "Over 214"
               },
               "wasPrice": [],
               "id": 202471349,
               "name": "Over 214",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3",
                  "frac": "2/1",
                  "rootIdx": 120
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 214",
                  "shortName": "Under 214"
               },
               "wasPrice": [],
               "id": 202471351,
               "name": "Under 214",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715299,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#214",
         "line": 214.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "5.25",
                  "frac": "17/4",
                  "rootIdx": 163
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls -9",
                  "shortName": "Chicago Bulls -9"
               },
               "wasPrice": [],
               "id": 202330849,
               "name": "Chicago Bulls -9",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.14",
                  "frac": "1/7",
                  "rootIdx": 21
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks +9",
                  "shortName": "Dallas Mavericks +9"
               },
               "wasPrice": [],
               "id": 202330874,
               "name": "Dallas Mavericks +9",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666752,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-9",
         "line": -9.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "4.5",
                  "frac": "7/2",
                  "rootIdx": 151
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls -8",
                  "shortName": "Chicago Bulls -8"
               },
               "wasPrice": [],
               "id": 202330847,
               "name": "Chicago Bulls -8",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.18",
                  "frac": "2/11",
                  "rootIdx": 26
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks +8",
                  "shortName": "Dallas Mavericks +8"
               },
               "wasPrice": [],
               "id": 202330869,
               "name": "Dallas Mavericks +8",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666755,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-8",
         "line": -8.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.85",
                  "frac": "37/20",
                  "rootIdx": 116
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 215",
                  "shortName": "Under 215"
               },
               "wasPrice": [],
               "id": 202471245,
               "name": "Under 215",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.4",
                  "frac": "2/5",
                  "rootIdx": 41
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 215",
                  "shortName": "Over 215"
               },
               "wasPrice": [],
               "id": 202471243,
               "name": "Over 215",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715263,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#215",
         "line": 215.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.9",
                  "frac": "19/10",
                  "rootIdx": 118
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls -3",
                  "shortName": "Chicago Bulls -3"
               },
               "wasPrice": [],
               "id": 202330852,
               "name": "Chicago Bulls -3",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.36",
                  "frac": "4/11",
                  "rootIdx": 39
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks +3",
                  "shortName": "Dallas Mavericks +3"
               },
               "wasPrice": [],
               "id": 202330873,
               "name": "Dallas Mavericks +3",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666758,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-3",
         "line": -3.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.55",
                  "frac": "31/20",
                  "rootIdx": 109
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 216",
                  "shortName": "Under 216"
               },
               "wasPrice": [],
               "id": 202471013,
               "name": "Under 216",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.47",
                  "frac": "40/85",
                  "rootIdx": 45
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 216",
                  "shortName": "Over 216"
               },
               "wasPrice": [],
               "id": 202471011,
               "name": "Over 216",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715157,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#216",
         "line": 216.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.44",
                  "frac": "4/9",
                  "rootIdx": 43
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks +2",
                  "shortName": "Dallas Mavericks +2"
               },
               "wasPrice": [],
               "id": 202330886,
               "name": "Dallas Mavericks +2",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.65",
                  "frac": "33/20",
                  "rootIdx": 112
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls -2",
                  "shortName": "Chicago Bulls -2"
               },
               "wasPrice": [],
               "id": 202330878,
               "name": "Chicago Bulls -2",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666747,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-2",
         "line": -2.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.45",
                  "frac": "29/20",
                  "rootIdx": 107
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 217",
                  "shortName": "Under 217"
               },
               "wasPrice": [],
               "id": 202471097,
               "name": "Under 217",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.5",
                  "frac": "1/2",
                  "rootIdx": 47
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 217",
                  "shortName": "Over 217"
               },
               "wasPrice": [],
               "id": 202471095,
               "name": "Over 217",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715195,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#217",
         "line": 217.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.3",
                  "frac": "3/10",
                  "rootIdx": 35
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks +5",
                  "shortName": "Dallas Mavericks +5"
               },
               "wasPrice": [],
               "id": 202330882,
               "name": "Dallas Mavericks +5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.3",
                  "frac": "23/10",
                  "rootIdx": 126
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls -5",
                  "shortName": "Chicago Bulls -5"
               },
               "wasPrice": [],
               "id": 202330875,
               "name": "Chicago Bulls -5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666744,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-5",
         "line": -5.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.35",
                  "frac": "27/20",
                  "rootIdx": 104
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 218",
                  "shortName": "Under 218"
               },
               "wasPrice": [],
               "id": 202471065,
               "name": "Under 218",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.55",
                  "frac": "11/20",
                  "rootIdx": 51
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 218",
                  "shortName": "Over 218"
               },
               "wasPrice": [],
               "id": 202471063,
               "name": "Over 218",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715181,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#218",
         "line": 218.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -1.5",
                  "shortName": "Dallas Mavericks -1.5"
               },
               "wasPrice": [],
               "id": 202471153,
               "name": "Dallas Mavericks -1.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.75",
                  "frac": "3/4",
                  "rootIdx": 63
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +1.5",
                  "shortName": "Chicago Bulls +1.5"
               },
               "wasPrice": [],
               "id": 202471151,
               "name": "Chicago Bulls +1.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "3rd Quarter Spread",
            "shortName": "3rd Quarter Spread"
         },
         "id": 69715223,
         "name": "3rd Quarter Spread",
         "type": "BASKETBALL:P:PSPRD",
         "subtype": "Q3#1.5",
         "line": 1.5,
         "displayOrder": -304,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q3"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.35",
                  "frac": "7/20",
                  "rootIdx": 38
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks +4",
                  "shortName": "Dallas Mavericks +4"
               },
               "wasPrice": [],
               "id": 202330909,
               "name": "Dallas Mavericks +4",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.05",
                  "frac": "41/20",
                  "rootIdx": 121
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls -4",
                  "shortName": "Chicago Bulls -4"
               },
               "wasPrice": [],
               "id": 202330885,
               "name": "Chicago Bulls -4",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666753,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-4",
         "line": -4.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.25",
                  "frac": "5/4",
                  "rootIdx": 101
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 219",
                  "shortName": "Under 219"
               },
               "wasPrice": [],
               "id": 202471113,
               "name": "Under 219",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.62",
                  "frac": "8/13",
                  "rootIdx": 54
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 219",
                  "shortName": "Over 219"
               },
               "wasPrice": [],
               "id": 202471111,
               "name": "Over 219",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715203,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#219",
         "line": 219.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.67",
                  "frac": "4/6",
                  "rootIdx": 57
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks",
                  "shortName": "Dallas Mavericks"
               },
               "wasPrice": [],
               "id": 202471137,
               "name": "Dallas Mavericks",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.1",
                  "frac": "11/10",
                  "rootIdx": 93
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls",
                  "shortName": "Chicago Bulls"
               },
               "wasPrice": [],
               "id": 202471135,
               "name": "Chicago Bulls",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "1st Quarter Money Line",
            "shortName": "1st Quarter Money Line"
         },
         "id": 69715215,
         "name": "1st Quarter Money Line",
         "type": "BASKETBALL:P:DNB",
         "subtype": "Q1",
         "displayOrder": -325,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q1"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.7",
                  "frac": "7/10",
                  "rootIdx": 59
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks",
                  "shortName": "Dallas Mavericks"
               },
               "wasPrice": [],
               "id": 202470873,
               "name": "Dallas Mavericks",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.05",
                  "frac": "21/20",
                  "rootIdx": 88
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls",
                  "shortName": "Chicago Bulls"
               },
               "wasPrice": [],
               "id": 202470871,
               "name": "Chicago Bulls",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "2nd Quarter Money Line",
            "shortName": "2nd Quarter Money Line"
         },
         "id": 69715099,
         "name": "2nd Quarter Money Line",
         "type": "BASKETBALL:P:DNB",
         "subtype": "Q2",
         "displayOrder": -315,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q2"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.44",
                  "frac": "4/9",
                  "rootIdx": 43
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks +2.5",
                  "shortName": "Dallas Mavericks +2.5"
               },
               "wasPrice": [],
               "id": 202330881,
               "name": "Dallas Mavericks +2.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.7",
                  "frac": "17/10",
                  "rootIdx": 113
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls -2.5",
                  "shortName": "Chicago Bulls -2.5"
               },
               "wasPrice": [],
               "id": 202330850,
               "name": "Chicago Bulls -2.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666757,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-2.5",
         "line": -2.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks {awayLine}",
                  "shortName": "Dallas Mavericks {awayLine}"
               },
               "wasPrice": [],
               "id": 202330977,
               "name": "Dallas Mavericks {awayLine}",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls {homeLine}",
                  "shortName": "Chicago Bulls {homeLine}"
               },
               "wasPrice": [],
               "id": 202330971,
               "name": "Chicago Bulls {homeLine}",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Spread",
            "shortName": "Spread"
         },
         "id": 69666807,
         "name": "Spread",
         "type": "BASKETBALL:FTOT:AHCP_MAIN",
         "subtype": "4.5",
         "line": 4.5,
         "lineType": "HANDICAP",
         "displayOrder": -495,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "4.5"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.33",
                  "frac": "1/3",
                  "rootIdx": 37
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +10.5",
                  "shortName": "Chicago Bulls +10.5"
               },
               "wasPrice": [],
               "id": 202330857,
               "name": "Chicago Bulls +10.5",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "3.3",
                  "frac": "23/10",
                  "rootIdx": 126
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -10.5",
                  "shortName": "Dallas Mavericks -10.5"
               },
               "wasPrice": [],
               "id": 202330871,
               "name": "Dallas Mavericks -10.5",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666759,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#10.5",
         "line": 10.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.7",
                  "frac": "7/10",
                  "rootIdx": 59
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks",
                  "shortName": "Dallas Mavericks"
               },
               "wasPrice": [],
               "id": 202470929,
               "name": "Dallas Mavericks",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.05",
                  "frac": "21/20",
                  "rootIdx": 88
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls",
                  "shortName": "Chicago Bulls"
               },
               "wasPrice": [],
               "id": 202470927,
               "name": "Chicago Bulls",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "3rd Quarter Money Line",
            "shortName": "3rd Quarter Money Line"
         },
         "id": 69715117,
         "name": "3rd Quarter Money Line",
         "type": "BASKETBALL:P:DNB",
         "subtype": "Q3",
         "displayOrder": -305,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q3"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.72",
                  "frac": "8/11",
                  "rootIdx": 60
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks",
                  "shortName": "Dallas Mavericks"
               },
               "wasPrice": [],
               "id": 202471183,
               "name": "Dallas Mavericks",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2",
                  "frac": "1/1",
                  "rootIdx": 83
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls",
                  "shortName": "Chicago Bulls"
               },
               "wasPrice": [],
               "id": 202471181,
               "name": "Chicago Bulls",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "4th Quarter Money Line",
            "shortName": "4th Quarter Money Line"
         },
         "id": 69715237,
         "name": "4th Quarter Money Line",
         "type": "BASKETBALL:P:DNB",
         "subtype": "Q4",
         "displayOrder": -295,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q4"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.75",
                  "frac": "3/4",
                  "rootIdx": 63
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 55.5",
                  "shortName": "Under 55.5"
               },
               "wasPrice": [],
               "id": 202471877,
               "name": "Under 55.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 55.5",
                  "shortName": "Over 55.5"
               },
               "wasPrice": [],
               "id": 202471875,
               "name": "Over 55.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "3rd Quarter Total Points",
            "shortName": "3rd Quarter Total Points"
         },
         "id": 69715521,
         "name": "3rd Quarter Total Points",
         "type": "BASKETBALL:P:OU",
         "subtype": "Q3#55.5",
         "line": 55.5,
         "displayOrder": -303,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": true,
         "period": "Q3"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "5.75",
                  "frac": "19/4",
                  "rootIdx": 169
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls -10",
                  "shortName": "Chicago Bulls -10"
               },
               "wasPrice": [],
               "id": 202330855,
               "name": "Chicago Bulls -10",
               "type": "AH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.11",
                  "frac": "1/9",
                  "rootIdx": 18
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks +10",
                  "shortName": "Dallas Mavericks +10"
               },
               "wasPrice": [],
               "id": 202330876,
               "name": "Dallas Mavericks +10",
               "type": "BH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666749,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#-10",
         "line": -10.0,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "5.75",
                  "frac": "19/4",
                  "rootIdx": 169
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -18.5",
                  "shortName": "Dallas Mavericks -18.5"
               },
               "wasPrice": [],
               "id": 202377517,
               "name": "Dallas Mavericks -18.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.11",
                  "frac": "1/9",
                  "rootIdx": 18
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +18.5",
                  "shortName": "Chicago Bulls +18.5"
               },
               "wasPrice": [],
               "id": 202377515,
               "name": "Chicago Bulls +18.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69680653,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#18.5",
         "line": 18.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.5",
                  "frac": "1/2",
                  "rootIdx": 47
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 228.5",
                  "shortName": "Under 228.5"
               },
               "wasPrice": [],
               "id": 202471145,
               "name": "Under 228.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.45",
                  "frac": "29/20",
                  "rootIdx": 107
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 228.5",
                  "shortName": "Over 228.5"
               },
               "wasPrice": [],
               "id": 202471143,
               "name": "Over 228.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715219,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#228.5",
         "line": 228.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "25",
                  "frac": "24/1",
                  "rootIdx": 216
               },
               "pos": {
                  "row": 5,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls 21-25",
                  "shortName": "Chicago Bulls 21-25"
               },
               "wasPrice": [],
               "id": 202471327,
               "name": "Chicago Bulls 21-25",
               "type": "HT 21-25",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "15",
                  "frac": "14/1",
                  "rootIdx": 206
               },
               "pos": {
                  "row": 4,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls 16-20",
                  "shortName": "Chicago Bulls 16-20"
               },
               "wasPrice": [],
               "id": 202471325,
               "name": "Chicago Bulls 16-20",
               "type": "HT 16-20",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "6",
                  "frac": "5/1",
                  "rootIdx": 172
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls 1-5",
                  "shortName": "Chicago Bulls 1-5"
               },
               "wasPrice": [],
               "id": 202471323,
               "name": "Chicago Bulls 1-5",
               "type": "HT 1-5",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "10.5",
                  "frac": "19/2",
                  "rootIdx": 197
               },
               "pos": {
                  "row": 3,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls 11-15",
                  "shortName": "Chicago Bulls 11-15"
               },
               "wasPrice": [],
               "id": 202471321,
               "name": "Chicago Bulls 11-15",
               "type": "HT 11-15",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "7.25",
                  "frac": "25/4",
                  "rootIdx": 185
               },
               "pos": {
                  "row": 3,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks 11-15",
                  "shortName": "Dallas Mavericks 11-15"
               },
               "wasPrice": [],
               "id": 202471309,
               "name": "Dallas Mavericks 11-15",
               "type": "AT 11-15",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "6.75",
                  "frac": "23/4",
                  "rootIdx": 181
               },
               "pos": {
                  "row": 2,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls 6-10",
                  "shortName": "Chicago Bulls 6-10"
               },
               "wasPrice": [],
               "id": 202471331,
               "name": "Chicago Bulls 6-10",
               "type": "HT 6-10",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "4.5",
                  "frac": "7/2",
                  "rootIdx": 151
               },
               "pos": {
                  "row": 2,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks 6-10",
                  "shortName": "Dallas Mavericks 6-10"
               },
               "wasPrice": [],
               "id": 202471319,
               "name": "Dallas Mavericks 6-10",
               "type": "AT 6-10",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "16",
                  "frac": "15/1",
                  "rootIdx": 207
               },
               "pos": {
                  "row": 6,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks 26+",
                  "shortName": "Dallas Mavericks 26+"
               },
               "wasPrice": [],
               "id": 202471317,
               "name": "Dallas Mavericks 26+",
               "type": "AT > 25",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "13.5",
                  "frac": "25/2",
                  "rootIdx": 203
               },
               "pos": {
                  "row": 5,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks 21-25",
                  "shortName": "Dallas Mavericks 21-25"
               },
               "wasPrice": [],
               "id": 202471315,
               "name": "Dallas Mavericks 21-25",
               "type": "AT 21-25",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "9",
                  "frac": "8/1",
                  "rootIdx": 192
               },
               "pos": {
                  "row": 4,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks 16-20",
                  "shortName": "Dallas Mavericks 16-20"
               },
               "wasPrice": [],
               "id": 202471313,
               "name": "Dallas Mavericks 16-20",
               "type": "AT 16-20",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "4.9",
                  "frac": "39/10",
                  "rootIdx": 159
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks 1-5",
                  "shortName": "Dallas Mavericks 1-5"
               },
               "wasPrice": [],
               "id": 202471311,
               "name": "Dallas Mavericks 1-5",
               "type": "AT 1-5",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "35",
                  "frac": "34/1",
                  "rootIdx": 226
               },
               "pos": {
                  "row": 6,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls 26+",
                  "shortName": "Chicago Bulls 26+"
               },
               "wasPrice": [],
               "id": 202471329,
               "name": "Chicago Bulls 26+",
               "type": "HT > 25",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Winning Margin 12-Way (Incl. OT)",
            "shortName": "Winning Margin 12-Way (Incl. OT)"
         },
         "id": 69715289,
         "name": "Winning Margin 12-Way (Incl. OT)",
         "type": "BASKETBALL:FTOT:WM",
         "subtype": "M#sr:winning_margin_no_draw:26+",
         "displayOrder": -400,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.1",
                  "frac": "11/10",
                  "rootIdx": 93
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 220",
                  "shortName": "Under 220"
               },
               "wasPrice": [],
               "id": 202471165,
               "name": "Under 220",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.7",
                  "frac": "7/10",
                  "rootIdx": 59
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 220",
                  "shortName": "Over 220"
               },
               "wasPrice": [],
               "id": 202471163,
               "name": "Over 220",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715229,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#220",
         "line": 220.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.05",
                  "frac": "21/20",
                  "rootIdx": 88
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 221",
                  "shortName": "Under 221"
               },
               "wasPrice": [],
               "id": 202470889,
               "name": "Under 221",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.72",
                  "frac": "8/11",
                  "rootIdx": 60
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 221",
                  "shortName": "Over 221"
               },
               "wasPrice": [],
               "id": 202470887,
               "name": "Over 221",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715107,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#221",
         "line": 221.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.36",
                  "frac": "4/11",
                  "rootIdx": 39
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 231.5",
                  "shortName": "Under 231.5"
               },
               "wasPrice": [],
               "id": 202471203,
               "name": "Under 231.5",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.9",
                  "frac": "19/10",
                  "rootIdx": 118
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 231.5",
                  "shortName": "Over 231.5"
               },
               "wasPrice": [],
               "id": 202471201,
               "name": "Over 231.5",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715247,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#231.5",
         "line": 231.5,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.95",
                  "frac": "19/20",
                  "rootIdx": 78
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 222",
                  "shortName": "Under 222"
               },
               "wasPrice": [],
               "id": 202470999,
               "name": "Under 222",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.8",
                  "frac": "4/5",
                  "rootIdx": 66
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 222",
                  "shortName": "Over 222"
               },
               "wasPrice": [],
               "id": 202470997,
               "name": "Over 222",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715151,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#222",
         "line": 222.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 223",
                  "shortName": "Over 223"
               },
               "wasPrice": [],
               "id": 202471263,
               "name": "Over 223",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.85",
                  "frac": "17/20",
                  "rootIdx": 70
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 223",
                  "shortName": "Under 223"
               },
               "wasPrice": [],
               "id": 202471265,
               "name": "Under 223",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715273,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#223",
         "line": 223.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.75",
                  "frac": "3/4",
                  "rootIdx": 63
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 224",
                  "shortName": "Under 224"
               },
               "wasPrice": [],
               "id": 202471269,
               "name": "Under 224",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2",
                  "frac": "1/1",
                  "rootIdx": 83
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 224",
                  "shortName": "Over 224"
               },
               "wasPrice": [],
               "id": 202471267,
               "name": "Over 224",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715275,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#224",
         "line": 224.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.72",
                  "frac": "8/11",
                  "rootIdx": 60
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 225",
                  "shortName": "Under 225"
               },
               "wasPrice": [],
               "id": 202471009,
               "name": "Under 225",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.05",
                  "frac": "21/20",
                  "rootIdx": 88
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 225",
                  "shortName": "Over 225"
               },
               "wasPrice": [],
               "id": 202471007,
               "name": "Over 225",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715155,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#225",
         "line": 225.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.15",
                  "frac": "23/20",
                  "rootIdx": 96
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 226",
                  "shortName": "Over 226"
               },
               "wasPrice": [],
               "id": 202470969,
               "name": "Over 226",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.67",
                  "frac": "4/6",
                  "rootIdx": 57
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 226",
                  "shortName": "Under 226"
               },
               "wasPrice": [],
               "id": 202470971,
               "name": "Under 226",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715137,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#226",
         "line": 226.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.57",
                  "frac": "4/7",
                  "rootIdx": 52
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 227",
                  "shortName": "Under 227"
               },
               "wasPrice": [],
               "id": 202471223,
               "name": "Under 227",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.3",
                  "frac": "13/10",
                  "rootIdx": 103
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 227",
                  "shortName": "Over 227"
               },
               "wasPrice": [],
               "id": 202471221,
               "name": "Over 227",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715257,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#227",
         "line": 227.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.53",
                  "frac": "8/15",
                  "rootIdx": 50
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 228",
                  "shortName": "Under 228"
               },
               "wasPrice": [],
               "id": 202471253,
               "name": "Under 228",
               "type": "Under",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "2.4",
                  "frac": "7/5",
                  "rootIdx": 106
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 228",
                  "shortName": "Over 228"
               },
               "wasPrice": [],
               "id": 202471251,
               "name": "Over 228",
               "type": "Over",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715267,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#228",
         "line": 228.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "2.55",
                  "frac": "31/20",
                  "rootIdx": 109
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Over 229",
                  "shortName": "Over 229"
               },
               "wasPrice": [],
               "id": 202471275,
               "name": "Over 229",
               "type": "Over",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.47",
                  "frac": "40/85",
                  "rootIdx": 45
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Under 229",
                  "shortName": "Under 229"
               },
               "wasPrice": [],
               "id": 202471277,
               "name": "Under 229",
               "type": "Under",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Total Points (Incl. OT)",
            "shortName": "Alternative Total Points (Incl. OT)"
         },
         "id": 69715279,
         "name": "Alternative Total Points (Incl. OT)",
         "type": "BASKETBALL:FTOT:OU",
         "subtype": "M#229",
         "line": 229.0,
         "displayOrder": -470,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      },
      {
         "selection": [
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 2
               },
               "names": {
                  "longName": "Dallas Mavericks -4.5",
                  "shortName": "Dallas Mavericks -4.5"
               },
               "wasPrice": [],
               "id": 202331042,
               "name": "Dallas Mavericks -4.5",
               "type": "BH",
               "suspended": false
            },
            {
               "odds": {
                  "dec": "1.91",
                  "frac": "10/11",
                  "rootIdx": 74
               },
               "pos": {
                  "row": 1,
                  "col": 1
               },
               "names": {
                  "longName": "Chicago Bulls +4.5",
                  "shortName": "Chicago Bulls +4.5"
               },
               "wasPrice": [],
               "id": 202331038,
               "name": "Chicago Bulls +4.5",
               "type": "AH",
               "suspended": false
            }
         ],
         "attributes": {
            "attrib": [
               {
                  "value": "false",
                  "key": "isBPG"
               },
               {
                  "value": "true",
                  "key": "cashoutAvailable"
               }
            ]
         },
         "rule4S": [],
         "names": {
            "longName": "Alternative Spread (Incl. OT)",
            "shortName": "Alternative Spread (Incl. OT)"
         },
         "id": 69666835,
         "name": "Alternative Spread (Incl. OT)",
         "type": "BASKETBALL:FTOT:AHCP",
         "subtype": "M#4.5",
         "line": 4.5,
         "displayOrder": -475,
         "columnCount": 2,
         "suspended": false,
         "displayed": true,
         "winningLegsBonus": false,
         "periodMarket": false,
         "period": "M"
      }
   ],
   "pastForm": [],
   "participants": {
      "participant": [
         {
            "names": {
               "longName": "Chicago Bulls",
               "shortName": "CHI"
            },
            "id": 4062475,
            "name": "Chicago Bulls",
            "type": "HOME"
         },
         {
            "names": {
               "longName": "Dallas Mavericks",
               "shortName": "DAL"
            },
            "id": 4062513,
            "name": "Dallas Mavericks",
            "type": "AWAY"
         }
      ]
   },
   "names": {
      "longName": "Chicago Bulls vs. Dallas Mavericks",
      "shortName": "CHI vs. DAL",
      "veryshortName": "Chicago Bulls vs. Dallas Mavericks"
   },
   "compNames": {
      "longName": "NBA",
      "shortName": "NBA"
   },
   "categoryNames": {
      "longName": "USA",
      "shortName": "USA"
   },
   "id": 8712256,
   "compId": 8370429,
   "compName": "NBA",
   "compWeighting": 100000.0,
   "categoryId": 8370427,
   "categoryName": "USA",
   "path": "226652:8370427:8370429:8712256",
   "name": "Chicago Bulls vs. Dallas Mavericks",
   "sport": "BASKETBALL",
   "state": "ACTIVE",
   "locale": "en-us",
   "displayed": true,
   "offeredInplay": true,
   "isInplay": false,
   "eventTime": 1609722000000,
   "numMarkets": 192,
   "numSpecSelections": 3,
   "numRABSelections": 30,
   "lastUpdatedTime": 1609718940258,
   "outright": false,
   "neutralVenue": false,
   "winningLegsBonus": false,
   "usDisplay": true
}
